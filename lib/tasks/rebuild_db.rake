require 'tempfile'

namespace :db do

  task :rebuild_from_remote do
    # this task can only run in a development enviroment
    return if Rails.env != 'development'

    # check that we have valid inputs from the environment varaibles
    raise Exception.new('You need to add a ENV["PRODUCTION_DATABASE_PASSWORD"] to the application.yml or the commandline') if ENV["PRODUCTION_DATABASE_PASSWORD"].blank?
    raise Exception.new('You need to add a ENV["DATABASE_DECRYPTION_KEY"] to the application.yml or the commandline') if ENV["DATABASE_DECRYPTION_KEY"].blank?

    # get the database config info
    config   = Rails.configuration.database_configuration
    dev_config = config["development"]
    prod_config = config["production"]

    database = config[Rails.env]["database"]

    dump_file_path = "#{Rails.root}/db/current.db.backup"
    dump_file_enc_path = "#{dump_file_path}.enc"

    # drop and recreate the local database
    puts 'Dropping then recreating the database'.green
    system "dropdb #{database} --if-exists"
    system "createdb #{database} -O #{dev_config["username"]}"

    # add the extentions that we are using
    system "psql -d #{database} -c \"create extension btree_gist;\" > /dev/null 2>&1"

    if (ENV["SKIP_DOWNLOAD"].try(:downcase) == 'false' || ENV["SKIP_DOWNLOAD"].blank?) || !File.exists?(dump_file_enc_path)
      # create the db dump from the remote server
      puts 'Download database dump, will take some time'.green
      system "PGPASSWORD=#{ENV["PRODUCTION_DATABASE_PASSWORD"]} pg_dump #{prod_config['database']} --no-privileges --no-owner -h db.gulfscei.tk -U #{prod_config['username']} -Z5 -Fc -b |  gpg --no-use-agent -c --batch --yes --passphrase \"#{ENV['DATABASE_DECRYPTION_KEY']}\" -o #{dump_file_enc_path}"
    end

    # restore the dump into the empty local database
    puts 'Restoring the local database'.green
    system "gpg --no-use-agent -d --yes --passphrase \"#{ENV["DATABASE_DECRYPTION_KEY"]}\" #{dump_file_enc_path} 2> /dev/null | PGPASSWORD=#{dev_config["password"]} pg_restore -h localhost -U #{dev_config["username"]} -d #{database} > /dev/null 2>&1"
  end

  task :rebuild_from_current do
    # this task can only run in a development enviroment
    return if Rails.env != 'development'

    # check that we have valid inputs from the environment varaibles
    raise Exception.new('You need to add a ENV["PRODUCTION_DATABASE_PASSWORD"] to the application.yml or the commandline') if ENV["PRODUCTION_DATABASE_PASSWORD"].blank?
    raise Exception.new('You need to add a ENV["DATABASE_DECRYPTION_KEY"] to the application.yml or the commandline') if ENV["DATABASE_DECRYPTION_KEY"].blank?

    # get the database config info
    config   = Rails.configuration.database_configuration
    dev_config = config["development"]

    database = config[Rails.env]["database"]

    dump_file_path = "#{Rails.root}/db/current.db.backup"
    dump_file_enc_path = "#{dump_file_path}.enc"

    # drop and recreate the local database
    puts 'Dropping then recreating the database'.green
    system "dropdb #{database} --if-exists"
    system "createdb #{database} -O #{dev_config["username"]}"

    # add the extentions that we are using
    system "psql -d #{database} -c \"create extension btree_gist;\" > /dev/null 2>&1"

    # restore the dump into the empty local database
    puts 'Restoring the local database'.green
    system "gpg --no-use-agent -d --yes --passphrase \"#{ENV["DATABASE_DECRYPTION_KEY"]}\" #{dump_file_enc_path} 2> /dev/null | PGPASSWORD=#{dev_config["password"]} pg_restore -h localhost -U #{dev_config["username"]} -d #{database} > /dev/null 2>&1"
  end
end

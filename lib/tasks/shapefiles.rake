task :import_shapefiles, [:env] => :environment do |t, args|
  config   = Rails.configuration.database_configuration
  database = config[Rails.env]["database"]

  # set the password for the postgres user
  sh "export PGPASSWORD=#{config[Rails.env]["password"]}"

  # drop the shape tables if they exist
  drop_table_statements = []
  drop_table_statements << 'drop table if exists basins'
  drop_table_statements << 'drop table if exists segments'
  drop_table_statements << 'drop table if exists subsegments'
  drop_table_statements << 'drop table if exists \"sub-basin\";'

  sh "psql -d #{database} -c \"#{drop_table_statements.join(';')}\" -U postgres"

  # load the shape files
  sh "shp2pgsql #{Rails.root}/import_files/shapefiles/Basins      | psql -d #{database} -U postgres"
  sh "shp2pgsql #{Rails.root}/import_files/shapefiles/Segments    | psql -d #{database} -U postgres"
  sh "shp2pgsql #{Rails.root}/import_files/shapefiles/Subsegments | psql -d #{database} -U postgres"
  sh "shp2pgsql #{Rails.root}/import_files/shapefiles/Sub-basin   | psql -d #{database} -U postgres"

  update_geom_statements = []
  update_geom_statements << 'update basins SET geom = st_transform(st_setsrid(geom,26915),4326)'
  update_geom_statements << 'update segments SET geom = st_transform(st_setsrid(geom,26915),4326)'
  update_geom_statements << 'update subsegments SET geom = st_transform(st_setsrid(geom,26915),4326)'
  update_geom_statements << 'update \"sub-basin\" SET geom = st_transform(st_setsrid(geom,26915),4326);'

  # change the srid and transform them to lat and long srid
  sh "psql -d #{database} -c \"#{update_geom_statements.join(';')}\" -U postgres"
end

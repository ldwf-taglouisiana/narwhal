require 'houston'

namespace :mobile do

  task :ios_test => :environment do

    # Environment variables are automatically read, or can be overridden by any specified options. You can also
    # conveniently use `Houston::Client.development` or `Houston::Client.production`.
    APN = Rails.env == 'production' ? Houston::Client.production : Houston::Client.development
    APN.certificate =  File.read(Rails.root + HOUSTON_CONFIG['perm_path'])
    APN.passphrase = HOUSTON_CONFIG['password']

    UserDevice.where(user_device_types_id: UserDeviceType.ios_type.id).each do |device|
      # Create a notification that alerts a message to the user, plays a sound, and sets the badge on the app
      notification = Houston::Notification.new(device: device.device_token)
      notification.alert = "Hello, World!"

      # Notifications can also change the badge count, have a custom sound, have a category identifier, indicate available Newsstand content, or pass along arbitrary data.
      # notification.badge = 57
      # notification.sound = "sosumi.aiff"
      # notification.content_available = true
      # notification.custom_data = {foo: "bar"}

      # And... sent! That's all it takes.
      APN.push(notification)
    end


  end

end

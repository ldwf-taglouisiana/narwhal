namespace :golang do

  task :build do

    commands = []

    dest_path = ENV['DESTINATION'] || Rails.root

    if Rails.env.development?
      commands << <<-SCRIPT
        mkdir -p /#{Rails.root}/opt/go
        mkdir -p /#{Rails.root}/addons/bin
        sudo chmod 755 /#{Rails.root}/opt
      SCRIPT
    end

    commands << <<-SCRIPT
      export GOPATH=#{Rails.root}/extras/golang/custom_query_report/

      go get github.com/tealeg/xlsx
      go get github.com/lib/pq

      go build github.com/dwa012/narwhal/custom_query_spreadsheet

      mv #{Rails.root}/custom_query_spreadsheet #{dest_path}/addons/bin/
    SCRIPT

    system commands.join("\n")
  end

end

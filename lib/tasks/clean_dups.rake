namespace :db do

  task :clean_dups => :environment do
    (move_dups = '') << <<-SQL
INSERT INTO captures_dups (id, capture_date, recapture, tag_id, time_of_day, angler_id, species_id, fish_condition_id, lat, long, location_description, comments, entered_gps_type, created_at, updated_at) (
  select id, capture_date, recapture, tag_id, time_of_day, angler_id, species_id, fish_condition_id, lat, long, location_description, comments, entered_gps_type, created_at, updated_at
  from captures where id in (
    select id from (
      SELECT id, ROW_NUMBER() OVER( PARTITION BY capture_date, recapture, tag_id, time_of_day, angler_id, species_id, fish_condition_id, lat, long, location_description, comments, entered_gps_type ) AS Row
      FROM captures
    ) dups
    where
    dups.Row > 1
  )
)
    SQL

    (remove_dups = '') << <<-eod
DELETE from captures where id in (
 select id from (
    SELECT id, ROW_NUMBER() OVER( PARTITION BY capture_date, recapture, tag_id, time_of_day, angler_id, species_id, fish_condition_id, lat, long, location_description, comments, entered_gps_type ) AS Row
    FROM captures
  ) dups
  where
  dups.Row > 1
)
    eod

    Capture.transaction do
      ActiveRecord::Base.connection.execute(move_dups)
      ActiveRecord::Base.connection.execute(remove_dups)
    end

  end

end

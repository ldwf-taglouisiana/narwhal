namespace :cannon do
  desc "Takes a json file and applies the updates to recaptures"
  task :fire, [:json_file] => :environment do |t,args|

    begin
      input_file = File.read(args[:json_file])
    rescue => e
      puts e
      puts 'Something went wrong with the file!'
      input_file = nil
    end

    #if everything went ok
    if input_file
      #parse the file as json
      data = JSON.parse(input_file)

      ActiveRecord::Base.transaction do

        data.each do |recapture|

          #If the log entry was for an update the tag_id field will be a tag id
          if recapture['tag_id'].to_i != 0
            update_stuff(recapture)

            #Otherwise the tag_id field will be a tag no and needs to be looked up too
          else
            tag = Tag.where(tag_no: recapture['tag_id']).first
            if tag
              recapture['tag_id'] = tag.id
                update_stuff(recapture)
            end

          end
        end
      end

    end
  end

end
def update_stuff(recapture)

  initial_entry = Recapture.where('tag_id = ? AND angler_id = ? AND species_id = ? AND capture_date::date = ? AND latitude = ? AND longitude = ?',
                                  recapture['tag_id'].to_i, recapture['angler_id'], recapture['species_id'].to_i,
                                  Chronic.parse(recapture['capture_date']).strftime('%Y-%m-%d'), recapture['latitude'].to_f.round(13), recapture['longitude'].to_f.round(13)).first


  #If we find a recapture matching our search criteria
  if initial_entry
    #need to parse some of the params because we could be going wayyyyy back
    #but in the end update the recapture in question
    if recapture.has_key?('species_length_id') and recapture.has_key?('recapture_disposition_id')
      initial_entry.update_attributes({species_length_id: recapture['species_length_id'], recapture_disposition_id: recapture['recapture_disposition_id']})

    elsif recapture.has_key?('species_length_id') and !recapture.has_key?('recapture_disposition_id')
      initial_entry.update_attributes({species_length_id: recapture['species_length_id'], recapture_disposition_id: recapture['recapture_disposition']})

    elsif !recapture.has_key?('species_length_id') and recapture.has_key?('recapture_disposition_id')
      initial_entry.update_attributes({length: recapture['length'], recapture_disposition_id: recapture['recapture_disposition_id']})

    else
      initial_entry.update_attributes({length: recapture['length'], recapture_disposition_id: recapture['recapture_disposition']})

    end
    puts initial_entry.id

  end
end

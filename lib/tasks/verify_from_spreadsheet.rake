require 'roo'

namespace :db do

  task :verify_from_spreadsheet => :environment do

    max_rows = ENV['MAX_ROWS'].to_i || nil
    target_angler_id = ENV['ANGLER_ID']

    path = Rails.root.join('static','CCA_slim.xlsx')
    xlsx = Roo::Excelx.new(path.to_s)

    count_changed = 0
    angler_count = 0
    capture_with_wrong_target_angler = 0
    unknown_capture = 0
    row_count = 0

    xlsx.each_row_streaming do |row|

     angler_id = 'LAG' + row[0].value.to_i.to_s
     tag = (row[1].value.strip + row[2].value.to_i.to_s.rjust(6, '0').strip).gsub(/\s/,'')
     capture_angler_id = Capture.joins(:tag).where( tags: { tag_no: tag.upcase }).pluck(:angler_id).first

     unknown_capture += 1 if capture_angler_id.nil?
     count_changed += 1 if capture_angler_id != nil && capture_angler_id != angler_id
     angler_count += 1 if angler_id == target_angler_id
     capture_with_wrong_target_angler += 1 if angler_id == target_angler_id && capture_angler_id != target_angler_id

     row_count += 1

     if row_count % 100 == 0
       puts '--------------------------------------------------------------------------------'
       puts "ROW #{row_count}"
       puts "Total rows that are different: #{count_changed}"
       puts "Total unknown: #{unknown_capture}"

       if target_angler_id
         puts "Total rows for angler: #{target_angler_id} - #{angler_count}"
         puts "Total rows mismatched for: #{target_angler_id} - #{capture_with_wrong_target_angler}"
         puts "Total records current in database for #{target_angler_id}: #{Capture.where(angler_id: target_angler_id).count}"
       end
     end
    end

    puts '--------------------------------------------------------------------------------'
    puts 'RESULTS'
    puts "Total rows that are different: #{count_changed}"
    puts "Total unknown: #{unknown_capture}"

    if target_angler_id
      puts "Total rows for angler: #{target_angler_id} - #{angler_count}"
      puts "Total rows mismatched for: #{target_angler_id} - #{capture_with_wrong_target_angler}"
      puts "Total records current in database for #{target_angler_id}: #{Capture.where(angler_id: target_angler_id).count}"
    end
  end
end
module SeasonsModule
  class << self

    def season_start
      last_season_end.beginning_of_day + 1.day
    end
    
    def season_end
      (season_start + 1.year).end_of_day
    end

    def last_season_start
      #1/9/2018 They changed the season start and end on us going forward
      lse = last_season_end
      if lse.year == 2017
        Date.parse('October 1, 2016').in_time_zone('Central Time (US & Canada)').beginning_of_day
      else
        (last_season_end - 1.years + 1.day).beginning_of_day
      end
    end

    def last_season_end
      if Date.today.year == 2017
        Date.parse('September 30, 2016').in_time_zone('Central Time (US & Canada)').end_of_day
      else
        Date.parse("December 31, #{Date.today.year - 1}").in_time_zone('Central Time (US & Canada)').end_of_day
      end
    end

  end
end
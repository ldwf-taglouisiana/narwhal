module CoreExtensions
  module ActiveRecord
    module IdMd5Hash

      extend ActiveSupport::Concern

      module ClassMethods

        # Creates a MD5 hash of the ids in sorted order, can take a scope with limits the id set
        # @param [Object] inner_query The scope that we will limit the set of ids that will be hashed
        # @return [String] The MD5 hash of the the ids appended to each other in order, eg .. [1,2,3] => "123" => Digest::MD5.hexdigest "123" => 202cb962ac59075b964b07152d234b70
        def ids_as_md5(inner_query = nil)
          inner = inner_query.nil? ? self.order(id: :asc).select('id::text').to_sql : inner_query.reorder(id: :asc).select('id::text').to_sql
          inner_updated_at = inner_query.nil? ? self.order(id: :asc).select('updated_at::text').to_sql : inner_query.reorder(id: :asc).select('updated_at::text').to_sql

          query = <<-SQL
            SELECT
              md5(
                concat(
                  array_to_string(array(#{inner}), '',''),
                  array_to_string(array(#{inner_updated_at}), '','')
                )
              ) as md5_hash
          SQL

          self.find_by_sql(query).first['md5_hash']
        end
      end

    end
  end
end
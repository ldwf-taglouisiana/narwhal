module CoreExtensions
  module Hash
    module SelectKeys
      def select_keys(*args)
        select {|k,v| args.map{|t| t.to_s}.include?(k) }
      end
    end
  end
end
class CustomFailure < Devise::FailureApp
  def redirect_url
    new_user_session_path
  end
  def respond
   if request.headers["Fiery-Mango"]
     http_auth
   else
     redirect
   end
  end

end
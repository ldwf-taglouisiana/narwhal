module AnglerDataMigrationModule

  class AnglerDataMigration

    def self.from_angler_to_angler!(original_angler, destination_angler, delete_original = false, whodunnit = nil)
      unless from_angler_to_angler(original_angler, destination_angler, delete_original, whodunnit)
        raise Exception.new 'There was an error migrating the data'
      end
    end
    
    def self.from_angler_to_angler(original_angler, destination_angler, delete_original = false, whodunnit = nil)

      original_angler = Angler.from_ambiguous_id!(original_angler).id
      destination_angler = Angler.from_ambiguous_id!(destination_angler).id

      result = true

      # get the list of tables that have an angler_id column, excluding the anglers table and some old data tables
      query = <<-SQL
        SELECT
          *
        FROM
          information_schema.columns
        WHERE table_schema NOT IN
          ('information_schema', 'pg_catalog')
          AND column_name = 'angler_id'
          AND table_name != 'anglers'
          AND table_name != 'captures_old'
          AND table_name != 'capture_duplicates'
          AND table_name != 'volunteer_time_logs_bad'
          AND table_name != 'versions'
          AND table_name IN ( -- only use records that are tables, ignoring views
            SELECT
              tablename
            FROM
              pg_tables
            WHERE schemaname = 'public'
           );
      SQL

      connection = ActiveRecord::Base.connection
      table_list = connection.execute(query)

      # set up the arrays to hold transient data
      selects = []
      dynamic_params = []

      # create the inner queries to get the number of rows in each column that has the given angler_id
      table_list.each do |table|

        ## -------------------------------------------------------------
        # insert the changes into the versions table before the changes.

        timestamp = Time.now # make the timestamp fixed to this point
        changes_selects = ["ARRAY[updated_at, ?] as \"updated_at\""]
        changes_selects << "ARRAY[angler_id, ?] as \"angler_id\"" if destination_angler != original_angler

        selects << <<-SQL
          INSERT into versions (item_id, item_type, event, whodunnit, created_at, object_changes) (
              SELECT
                A.id,
                ?,
                'update',
                ?,
                ?,
                (
                  select
                    to_json(t)::jsonb
                  FROM (
                    select
                      #{changes_selects.join(',')}
                  )t
                )
              FROM
                #{table["table_name"]} A
              WHERE
                angler_id = ?
            );


        SQL

        dynamic_params << [table["table_name"].classify.constantize.name, whodunnit, timestamp, timestamp]
        dynamic_params << [destination_angler] if destination_angler != original_angler
        dynamic_params << [original_angler]

        ## -------------------------------------------------------------
        # add the update statements to the query

        selects << <<-SQL
          UPDATE #{table["table_name"]} SET angler_id = ?, updated_at = ? WHERE angler_id = ?;
        SQL

        dynamic_params << [destination_angler, timestamp, original_angler]

      end # end table_list each

      if delete_original
        selects << 'DELETE FROM anglers WHERE id = ?'
        dynamic_params << original_angler
      end

      sanitized_sql = [selects.join(';')] + dynamic_params.flatten

      begin
        ActiveRecord::Base.transaction do
          sql = ActiveRecord::Base.send(:sanitize_sql_array, sanitized_sql)
          ActiveRecord::Base.connection.execute(sql)
        end

        # kick off the refresh job so we can
        RefreshArchivedViewsJob.perform_later
      rescue Exception => e
        Rails.logger.warn e.message
        result = false
      end

      result
    end

  end

end
include SeasonsModule

module DashboardModule
  class Dashboard
    class << self
      def overall_stats

        (sql_query = '') << <<-sql
          select to_json(t)
          from (
            select 
              to_json(t) as "items",
              md5(to_json(t)::text) as "version"
            from (
              select
                  (#{total_captures})                    as "total_captures",
                  (#{total_recaptures})                  as "total_recaptures",
                  (#{total_captures_season})             as "total_captures_season",
                  (#{total_recaptures_season})           as "total_recaptures_season",
                  (#{recaptures_species_30_days})        as "recaptures_by_species",
                  (#{captures_species_30_days})          as "captures_by_species"
            ) t
          ) t
        sql

        connection = ActiveRecord::Base.connection

        connection.select_value(sql_query)
      end

      def management_stats

        (sql_query = '') << <<-sql
        select to_json(t)
        from (
          select
            to_json(t) as "items",
            md5(to_json(t)::text) as "version"
          from (
            select
                (#{total_captures})                     as "total_captures",
                (#{total_recaptures})                   as "total_recaptures",

                (#{recaptures_species_30_days({target: false})})  as "recaptures_by_species",
                (#{captures_species_30_days({target: false})})    as "captures_by_species",

                (#{active_anglers_count})               as "active_anglers",
                (#{anglers_count})                      as "anglers",
                (#{participants_count})                 as "participants"
          ) t
        ) t
        sql

        connection = ActiveRecord::Base.connection

        connection.select_value(sql_query)
      end

      def angler_stats(angler_id = nil)

        # need to provide an angler_id
        # raise ArgumentError.new if angler_id.nil?

        sql_query = <<-sql
        select to_json(t)
        from (
          select
            to_json(t) as "items",
            md5(to_json(t)::text) as "version"
          from (
            select
                (#{ total_captures})                    as "total_captures",
                (#{ total_recaptures})                  as "total_recaptures",
                (#{ recaptures_species_30_days})        as "recaptures_by_species",
                (#{ captures_species_30_days})          as "captures_by_species",

                (#{ total_captures({filter_by_angler: true}) })           as "my_total_captures",
                (#{ total_recaptures({filter_by_angler: true}) })         as "my_total_recaptures",
                (#{ total_captures_season({filter_by_angler: true}) })    as "my_total_captures_this_year",
                (#{ total_recaptures_season({filter_by_angler: true}) })  as "my_total_recaptures_this_year",
                (#{ total_captures_last_season({filter_by_angler: true}) })    as "my_total_captures_last_season",
                (#{ total_recaptures_last_season({filter_by_angler: true}) })  as "my_total_recaptures_last_season",

                (#{ recaptures_species_30_days({filter_by_angler: true}) })      as "angler_recaptures_by_species",
                (#{ captures_species_30_days({filter_by_angler: true}) })        as "angler_captures_by_species"
          ) t
        ) t
        sql

        sanitized_sql = [sql_query] + ([Angler.from_ambiguous_id(angler_id).try(:id)] * 8)
        prepared_sql = ActiveRecord::Base.send('sanitize_sql_array', sanitized_sql)

        connection = ActiveRecord::Base.connection

        connection.select_value(prepared_sql)
      end



      private


      def active_anglers_count
        <<-SQL
      select to_json(d)
      from (
        select COALESCE(sum(count),0) as "count"
        from (
          select angler_id, count(*)
          from captures
          where capture_date between (now() - '30 days'::interval)::date and now()
          and angler_id !~* 'LAX'
          group by angler_id
        ) a
      ) d
        SQL
      end

      def anglers_count
        <<-SQL
      select to_json(d)
      from (
        select count(*)
        from anglers
        where angler_id !~* 'LAX'
      ) d
        SQL
      end

      def participants_count
        <<-SQL
      select to_json(d)
      from (
        select count(*)
        from anglers
        where angler_id !~* 'LAX'
      ) d
        SQL
      end

      def captures_species_30_days(params = {target: true, filter_by_angler: false})
        merged_params = private_default_params.merge(params)

        <<-SQL
        select json_agg(d)
        from (
          WITH species_list AS (
	          SELECT
              (CASE WHEN species.target = true THEN species.common_name  ELSE 'Other' END) as common_name,
              (CASE WHEN species.target = true THEN 0  ELSE 1 END) orderer
            FROM captures inner join species on captures.species_id = species.id
            WHERE capture_date BETWEEN (now() - '30 days'::interval)::date AND now()
        #{ 'and angler_id = ?' if merged_params[:filter_by_angler] }
          )
          SELECT common_name, count(*) as species_count
          FROM species_list
          GROUP BY orderer, common_name
          ORDER BY orderer, species_count DESC
        ) d
        SQL
      end

      def recaptures_species_30_days(params = {target: true, filter_by_angler: false})
        merged_params = private_default_params.merge(params)

        angler_filter = <<-SQL
          AND tag_id in (
            SELECT tag_id
            FROM captures
            WHERE angler_id = ?
          )
        SQL

        <<-SQL
        select json_agg(d)
        from (
          WITH species_list AS (
	          SELECT
              (CASE WHEN species.target = true THEN species.common_name  ELSE 'Other' END) as common_name,
              (CASE WHEN species.target = true THEN 0  ELSE 1 END) orderer
            FROM recaptures inner join species on recaptures.species_id = species.id
            WHERE capture_date BETWEEN (now() - '30 days'::interval)::date AND now()
              #{ angler_filter if merged_params[:filter_by_angler] }
          )
          SELECT common_name, count(*) as species_count
          FROM species_list
          GROUP BY orderer, common_name
          ORDER BY orderer, species_count DESC
        ) d
        SQL
      end

      def total_captures(params = {filter_by_angler: false})
        merged_params = private_default_params.merge(params)

        <<-SQL
        select to_json(d)
        from (
            SELECT COUNT(*) as count
              FROM captures
              #{ 'WHERE angler_id = ?' if merged_params[:filter_by_angler]}
        ) d
        SQL
      end

      def total_recaptures(params = {filter_by_angler: false})
        merged_params = private_default_params.merge(params)

        angler_filter = <<-SQL
      WHERE tag_id in (
        SELECT tag_id
        FROM captures
        WHERE angler_id = ?
      )
        SQL

        <<-SQL
        select to_json(d)
        from (
            SELECT COUNT(*) as count
              FROM recaptures
              #{ angler_filter if merged_params[:filter_by_angler]}
        ) d
        SQL
      end

      def total_captures_season(params = {filter_by_angler: false})
        merged_params = private_default_params.merge(params)

        <<-SQL
        select to_json(d)
        from (
          SELECT COALESCE(sum(count),0) as count
          FROM (
            SELECT COUNT(*) as count
              FROM captures
              WHERE capture_date between '#{SeasonsModule.season_start}' AND now()
                #{ 'and angler_id = ?' if merged_params[:filter_by_angler] }
              GROUP BY tag_id
          ) a
        ) d
        SQL
      end

      def total_recaptures_season(params = {filter_by_angler: false})
        merged_params = private_default_params.merge(params)

        angler_filter = <<-SQL
          AND tag_id in (
            SELECT tag_id
            FROM captures
            WHERE angler_id = ?
          )
        SQL

        <<-SQL
          select to_json(d)
          from (
            SELECT COALESCE(sum(count),0) as count
            FROM (
              SELECT COUNT(*) as count
                FROM recaptures
                WHERE capture_date between '#{SeasonsModule.season_start}' AND now()
                  #{ angler_filter if merged_params[:filter_by_angler] }
            ) a
          ) d
        SQL

      end

      def total_captures_last_season(params = {filter_by_angler: false})
        merged_params = private_default_params.merge(params)

        <<-SQL
        select to_json(d)
        from (
          SELECT COALESCE(sum(count),0) as count
          FROM (
            SELECT COUNT(*) as count
              FROM captures
              WHERE capture_date between '#{SeasonsModule.last_season_start}' AND '#{SeasonsModule.last_season_end}'
                #{ 'and angler_id = ?' if merged_params[:filter_by_angler] }
              GROUP BY tag_id
          ) a
        ) d
        SQL
      end

      def total_recaptures_last_season(params = {filter_by_angler: false})
        merged_params = private_default_params.merge(params)

        angler_filter = <<-SQL
          AND tag_id in (
            SELECT tag_id
            FROM captures
            WHERE angler_id = ?
          )
        SQL

        <<-SQL
          select to_json(d)
          from (
            SELECT COALESCE(sum(count),0) as count
            FROM (
              SELECT COUNT(*) as count
                FROM recaptures
                WHERE capture_date between '#{SeasonsModule.last_season_start}' AND '#{SeasonsModule.last_season_end}'
                  #{ angler_filter if merged_params[:filter_by_angler] }
            ) a
          ) d
        SQL

      end

      def private_default_params
        {
            target: true,
            filter_by_angler: false
        }
      end
    end
  end

end
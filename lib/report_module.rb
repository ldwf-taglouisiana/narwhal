require 'benchmark'
require 'tempfile'

module ReportModule

  # the storage path for no temporary files.
  FILE_PATH='/storage/requested_download_files/'
  GO_BIN_PATH = "#{Rails.root}/addons/bin/custom_query_spreadsheet"


  class QaQuery

    class << self

    end

  end

  class CustomizableQuery

    #{
    # Takes the following set of parameters and creates a customized query for fish events
    #
    # params = {
    #             config: {
    #               captures: boolean,
    #               recaptures: boolean,
    #               days_at_large: boolean
    #             },
    #             terms: {
    #               capture_date: {
    #                 min: Date,
    #                 max: Date
    #               },
    #               anglers: [
    #                 list of alphanumeric angler ids
    #               ],
    #               tags: [
    #                 list of alphanumeric tag numbers
    #               ],
    #               tag_range: {
    #                 min: alphanumeric tag number,
    #                 max: alphanumeric tag number
    #               },
    #               length: {
    #                 min: Number,
    #                 max: Number
    #               },
    #               time_of_day_options: [
    #                 list of time_of_day_option_ids
    #               ],
    #               species: [
    #                 list of species_ids
    #               ],
    #               fish_condition_options: [
    #                 list of fish_condition_option_ids
    #               ],
    #               location_descriptions: [
    #                 list of partial location descriptions
    #               ],
    #               comments: [
    #                 list of partial comments
    #               ],
    #             }
    #          }
    #
    # }

    # the set of default params as described above

    class << self

      DEFAULT_PARAMS = {
          config: {
              captures:      true,
              recaptures:    false,
              days_at_large: false
          },
          search: {
              captures: true,
              recaptures: false
          },
          terms:  {
              capture_date:           {
                  min:         nil,
                  max:         nil
              },
              anglers:                [],
              tags:                   [],
              tag_range:              {
                  min: nil,
                  max: nil
              },
              length:                 {
                  min: nil,
                  max: nil
              },
              time_of_day_options:    [],
              species:                [],
              basin:                  [],
              sub_basin:              [],
              fish_condition_options: [],
              location_descriptions:  [],
              comments:               []
          }
      }


=begin
 Will produce the following JSON string

      {
          overall_summary: {
              species_counts: [
                  {
                      name: string,
                      capture_count: integer,
                      recapture_count: integer
                  }
                  ...
              ],
              location_counts: [
                  {
                      species_name: string,
                      location: string,
                      date: date_string,
                      capture_count: integer,
                      recapture_count: integer
                  }
                  ...
              ]
          },
          years: [
              {
                  year: string,
                  summary: {
                      species_counts: [
                          {
                              name: string,
                              capture_count: integer,
                              recapture_count: integer
                          }
                          ...
                      ],
                      location_counts: [
                          {
                              species_name: string,
                              location: string,
                              date: date_string,
                              capture_count: integer,
                              recapture_count: integer
                          }
                          ...
                      ]
                  },
                  fish_history: {
                      max_recaptures: integer,
                      items: [
                          {
                              capture: {
                                  capture_angler_id: string,
                                  capture_angler_name: string,
                                  capture_date: datestring,
                                  location: string,
                                  time_of_day: string,
                                  length: string,
                                  fish_condition: string,
                                  latitude: double,
                                  longitude: double,
                              }
                              recaptures: [
                                  {
                                    recapture_angler_id: string,
                                    recapture_angler_name: string,
                                    recapture_date: date_string,
                                    location: string,
                                    time_of_day: string,
                                    length: string,
                                    recapture_disposition: string,
                                    fish_condition: string,
                                    latitude: double,
                                    longitude: double
                                  }
                                  ...
                              ]
                          }
                          ...
                      ]
                  }
              }
              ...
          ]
      }
=end
      def json_data(params = DEFAULT_PARAMS)
        File.write("#{Rails.root}/uggggggg.sql",build_sql(params))
        ActiveRecord::Base.connection.execute(build_sql(params))[0]['data']
      end

      # creates temp files only
      def spreadsheet(params = DEFAULT_PARAMS)
        if File.exist?(GO_BIN_PATH)
          golang_spreadsheet(configure_params(params))
        else
          axlsx_spreadsheet(configure_params(params))
        end
      end

      # this version uses the tokens to create files that will expire in 24 hours
      def spreadsheet_with_token(params = DEFAULT_PARAMS, file_token = SecureRandom.hex(40))

        # make the target directory for the files, if needed
        FileUtils.mkdir_p(File.join(Rails.root,FILE_PATH)) unless File.exists?(File.join(Rails.root,FILE_PATH))

        if File.exist?(GO_BIN_PATH)
          golang_spreadsheet(configure_params(params), file_token)
        else
          axlsx_spreadsheet(configure_params(params), file_token)
        end
      end

      private

      def axlsx_spreadsheet(params, file_token = nil)
        json_string = json_data(params)

        # the predefined header for the set of recapture columns
        recapture_header = %w(recapture_angler_id recapture_angler_name recapture_date location time_of_day length length_range recapture_disposition fish_condition latitude longitude recapture_enterer_id recapture_name_id recapture_enterer_roles)
        overall_max_recaptures = 0

        json = JSON.parse(json_string, symbolize_names: true)

        # create the work book
        p = Axlsx::Package.new
        wb = p.workbook

        # add the overall summary sheet
        wb.add_worksheet(:name => 'Overall Summary') do |sheet|

          # add the header row, use the JSON key names as the column titles
          sheet.add_row json[:overall_summary][:location_counts].first.keys.map { |t| custom_titleize(t.to_s) } unless json[:overall_summary][:location_counts].first.nil?

          # for each location count object write a row
          json[:overall_summary][:location_counts].try(:each) do |row|
            sheet.add_row row.values
          end
        end


        # for each object in the years array, create a summary and detail sheet
        json[:years].try(:each) do |data|

          # these are used for readability
          summary = data[:summary]
          fish_history = data[:fish_history]

          # update the overall max_recaptures value
          overall_max_recaptures = fish_history[:max_recaptures] if !fish_history[:max_recaptures].nil? and fish_history[:max_recaptures] > overall_max_recaptures

          worksheet_name = "#{data[:year]} Summary"

          # add the overall summary sheet
          wb.add_worksheet(:name => worksheet_name) do |sheet|

            # add the header row, use the JSON key names as the column titles
            sheet.add_row summary[:location_counts].first.keys.map { |t| custom_titleize(t.to_s) } unless summary[:location_counts].nil?

            # for each location count add a row
            summary[:location_counts].try(:each) do |row|
              sheet.add_row row.values
            end

          end # of adding year summary worksheet


          worksheet_name = "#{data[:year]} Detail"

          # add the overall fish history/detail sheet
          wb.add_worksheet(:name => worksheet_name) do |sheet|
            header = []

            # if the user requested captures to be listed, add the header
            header =  fish_history[:items].first[:capture].keys.map { |t| custom_titleize(t.to_s) } if params[:config][:captures]

            # if the user requested recaptures to be listed, add the headers
            if fish_history[:max_recaptures]
              header = header + ( recapture_header.map { |t| custom_titleize(t.to_s) } * fish_history[:max_recaptures] ) if params[:config][:recaptures]
            end

            # add the header row
            sheet.add_row header

            # for each history item
            fish_history[:items].each do |row|
              row_data = []

              # if the user requested captures to be listed, add the row portion
              row_data = row[:capture].values  if params[:config][:captures]

              # if the user requested recaptures to be listed, add the row portions
              if params[:config][:recaptures]

                # the try here is to guard when the recaptures key's value is nil
                row[:recaptures].try(:each) do |recapture|
                  row_data = row_data + recapture.values if params[:config][:recaptures]
                end

              end

              # and the compete row
              sheet.add_row row_data

            end # of history items

          end # of adding year summary worksheet

        end # of years loop

        # add the Combined Data sheet
        wb.add_worksheet(:name => 'All Data') do |sheet|

          unless json[:years].nil?

            # setup the all data header
            fish_history = json[:years].first.try(:[],:fish_history)

            if fish_history.try(:[], :items).try(:first)
              header = []

              # if the user requested captures to be listed, add the header
              header =  fish_history[:items].first[:capture].keys.map { |t| custom_titleize(t.to_s) } if params[:config][:captures]

              # if the user requested recaptures to be listed, add the headers
              if overall_max_recaptures and !fish_history[:max_recaptures].nil?
                header = header + ( recapture_header.map { |t| custom_titleize(t.to_s) } * fish_history[:max_recaptures] ) if params[:config][:recaptures]
              end

              # add the header row
              sheet.add_row header

            end

            # start adding all the rows from each year into the this worksheet
            json[:years].try(:each) do |data|

              # these are used for readability
              fish_history = data[:fish_history]

              # for each history item
              fish_history[:items].try(:each) do |row|
                row_data = []

                # if the user requested captures to be listed, add the row portion
                row_data = row[:capture].values  if params[:config][:captures]

                # if the user requested recaptures to be listed, add the row portions
                if params[:config][:recaptures]

                  # the try here is to guard when the recaptures key's value is nil
                  row[:recaptures].try(:each) do |recapture|
                    row_data = row_data + recapture.values if params[:config][:recaptures]
                  end

                end

                # and the compete row
                sheet.add_row row_data

              end # of history items

            end # of years loop

          end
        end

        # if no token was provided, then return the Workbook object
        if file_token.nil?
          p

        else
          # convert the workbook into a stream
          s = p.to_stream

          # write the stream to a file
          file_path = File.join(Rails.root, FILE_PATH, file_token)
          File.open(file_path, 'w') { |f| f.write(s.read) }


          # return the file object
          File.new(file_path)

        end
      end

      def golang_spreadsheet(params, file_token = nil)
        # create the sql file that will hold the query string
        sql_file = Tempfile.new('sql')
        sql_file.write(build_sql(params))
        sql_file.close

        # create the output file for the xlsx file
        # if no token provided use a temp file, else create a file using the token
        output_file = file_token.nil? ? Tempfile.new('output') : File.new(File.join(Rails.root, FILE_PATH, file_token), 'w')

        # if the output file is a real File we need to close it before we pass the path to the go binary
        output_file.close if output_file.instance_of?(File)

        # database connection information to pass to the go binary
        config   = Rails.configuration.database_configuration
        database = config[Rails.env]["database"]
        host     = config[Rails.env]["host"]
        username = config[Rails.env]["username"]
        password = config[Rails.env]["password"]

        # run the go binary
        system "\"#{ReportModule::GO_BIN_PATH}\" --host \"#{host}\" --database \"#{database}\" --username \"#{username}\" --password \"#{password}\" #{ '--recaptureColumns' if params[:config][:recaptures]} --query-file \"#{sql_file.path}\" --out-file \"#{output_file.path}\""

        # clean up the temp sql file
        #sql_file.unlink

        # return the file with the xlsx data in it
        output_file
      end

      def build_sql(params)
        dc_where_clause = generate_where(params, 'dc')
        rc_where_clause = generate_where(params, 'rc')

        species_recapture_where = <<-SQL
          CASE
            WHEN #{(params[:search][:recaptures] && params[:search][:recaptures]) ? 'TRUE' : 'FALSE'} THEN
              #{rc_where_clause[:where]}
              OR
              rc.capture_id = ANY (
                          (
                              SELECT
                                  array(
                                      SELECT
                                          dc.id
                                      FROM
                                          presentable_captures dc
                                      WHERE (
                                          #{dc_where_clause[:where]}
                                      )
                                  )
                          )::int[]
                      )

            WHEN #{params[:search][:recaptures] ? 'TRUE' : 'FALSE'} THEN
              #{rc_where_clause[:where]}
            ELSE
              rc.capture_id = ANY (
                          (
                              SELECT
                                  array(
                                      SELECT
                                          dc.id
                                      FROM
                                          presentable_captures dc
                                      WHERE (
                                          #{dc_where_clause[:where]}
                                      )
                                  )
                          )::int[]
                      )
          END
        SQL

        year_union = params[:search][:recaptures] ? "UNION ALL (SELECT DISTINCT ON (year_id) year_id as \"id\" FROM orphaned_recapture_tags)" : ''
        capture_list_union = params[:search][:recaptures] ? "UNION ALL (SELECT * from orphaned_recapture_tags WHERE year_id = years.id)" : ''
        max_recapture_union = params[:search][:recaptures] ? "UNION ALL (SELECT max(recapture_count) as \"recapture_count\" FROM orphaned_recapture_tags WHERE year_id = years.id)" : ''

        query = <<-SQL
----------------------------------------------------------------------------
-- this is the temp table get the distinct years that previously filtered
-- captures occur in. This allows us to create an array of events by year
-- later on

WITH orphaned_recapture_tags AS (
  SELECT
    NULL::int as "id",
    NULL::int as "species_id",
    rc.species_name::varchar(255) as "species_name",
    rc.tag_id::int,
    rc.tag_number::varchar(255) as "tag_number",
    NULL::int as "angler_id",
    'N/A'::text as "agnler_number",
    'N/A'::text as "capture_angler",
    NULL::timestamp with time zone as "capture_date",
    'N/A'::text as "date_string",
    'N/A'::text as "location",
    NULL::int as "time_of_day_option_id",
    'N/A'::varchar(255) as "time_of_day",
    NULL::numeric as "length",
    'N/A'::varchar(255) as "length_range",
    NULL::int as "fish_condition_option_id",
    'N/A'::varchar(255) as "fish_condition",
    NULL::double precision as "latitude",
    NULL::double precision as "longitude",
    'N/A'::text as "latitude_formatted",
    'N/A'::text as "longitude_formatted",
    'N/A'::varchar(255) as "entered_gps_type",
    NULL::geometry as "geom",
    NULL::int as "basin_id",
    NULL::int as "sub_basin_id",
    NULL::text as "comments",
    NULL::boolean as "user_capture",
    NULL::boolean as "verified",
    'N/A'::text as "location_description",
    NULL::bigint as "location_id",
    NULL::timestamp with time zone as "created_at",
    NULL::timestamp with time zone as "updated_at",
    (SELECT COUNT(a.tag_id) FROM presentable_recaptures a WHERE a.tag_id = rc.tag_id )::bigint as "recapture_count",
    rc.year_id::int as "year_id"
    --NULL::integer as "user_id",
    --NULL::text as "user_name",
    --NULL::text as "user_roles"
  FROM
    presentable_recaptures rc
  WHERE
      rc.capture_id IS NULL
    AND (
      #{rc_where_clause[:where]}
    )
),

versions_created AS (
	SELECT
		DISTINCT ON (item_id) *
	FROM
		versions
  WHERE
    event = 'create'
  ORDER BY
    item_id, created_at ASC
),

years AS (
    SELECT DISTINCT
      id
    FROM (
      (
        SELECT
            year_id as "id"
        FROM
            presentable_captures dc
        WHERE (
            #{dc_where_clause[:where]}
        )
      )
      #{year_union}
    ) a
    ORDER BY
      id ASC
),

----------------------------------------------------------------------------
-- this is the temp table to get the overall summary for the requested data.
-- it contains a number of inner temp tables to help make the overall SQL
-- more readable.
overall_summary AS (

-- BEGIN the species counts
-- get the species counts for captures and recaptures
    WITH species_capture_count AS (
        SELECT
            species_id        AS "species_id",
            species_name      AS "name",
            count(*)          AS "count"
        FROM
            presentable_captures dc
        WHERE (
            #{dc_where_clause[:where]}
        )
        GROUP BY
            species_id,
            species_name
    ),

    species_recapture_count AS (
        SELECT
            species_id          AS "species_id",
            species_name        AS "name",
            count(*)            AS "count"
        FROM
            presentable_recaptures rc
        WHERE
          #{species_recapture_where}
        GROUP BY
            rc.species_id,
            rc.species_name
    ),

    -- combine the previous two tables
    combined_species_count AS (
        SELECT
             COALESCE(json_agg(t), '[]') -- combine all the rows into a json array
        FROM (
             SELECT
                 CASE -- only have one name column, try to always choose a not nul column
                     WHEN cc.name IS NULL THEN
                         rc.name
                     ELSE
                         cc.name
                 END            AS "name",
                 cc.count       AS "capture_count",
                 rc.count    AS "recapture_count"
             FROM
                 species_capture_count cc
                 FULL OUTER JOIN
                 species_recapture_count rc ON cc.species_id = rc.species_id
        ) t
    ),

    -- BEGIN the event location list
    -- create the tables for event list
    event_capture_list AS (
        SELECT
            capture_date, -- only used for sorting/grouping by
            species_id           AS "species_id",
            species_name         AS "species_name",
            location_id,
            location_description AS "location",
            date_string          AS "date",
            count(*)             AS "count"
        FROM
            presentable_captures dc
        WHERE (
            #{dc_where_clause[:where]}
        )
        GROUP BY
            capture_date,
            date_string,
            location_id,
            location_description,
            species_id,
            species_name
        ORDER BY
            capture_date
    ),

    event_recapture_list AS (
        SELECT
            capture_date, -- only used for sorting/grouping by
            species_id           AS "species_id",
            species_name         AS "species_name",
            location_id,
            location_description AS "location",
            date_string          AS "date",
            count(*)             AS "count"
        FROM
            presentable_recaptures rc
        WHERE (
          #{species_recapture_where}
        )
        GROUP BY
            capture_date,
            date_string,
            location_id,
            location_description,
            species_id,
            species_name
        ORDER BY
            capture_date
    ),

    -- combine the previous two tables
    combined_event_list AS (
        SELECT
            COALESCE(json_agg(t), json'[]')
        FROM (
                -- select the
                 SELECT
                     CASE -- case is used to try to select a not null column
                         WHEN event_capture_list.species_name IS NOT NULL THEN
                             event_capture_list.species_name
                         ELSE
                             event_recapture_list.species_name
                     END                           AS "species_name",

                     CASE -- case is used to try to select a not null column
                         WHEN event_capture_list.location IS NOT NULL THEN
                             event_capture_list.location
                         ELSE
                             event_recapture_list.location
                     END                         AS "location",

                     CASE -- case is used to try to select a not null column
                         WHEN event_capture_list.date IS NOT NULL THEN
                             event_capture_list.date
                         ELSE
                             event_recapture_list.date
                     END                           AS "date",

                     event_capture_list.count     AS "capture_count",
                     event_recapture_list.count     AS "recapture_count"
                 FROM
                     event_capture_list
                     FULL OUTER JOIN event_recapture_list
                         ON (
                         event_capture_list.species_id = event_recapture_list.species_id
                         AND event_capture_list.location_id = event_recapture_list.location_id
                         AND event_capture_list.date = event_recapture_list.date
                         )
        ) t  -- end from for json_agg
    ) -- end CTEs

    SELECT
        to_json(t)  -- combine the items in the subquery into a single json object
    FROM (
         SELECT
            (SELECT * FROM combined_species_count) AS "species_counts",
            (SELECT * FROM combined_event_list)    AS "location_counts"
    ) t
),

----------------------------------------------------------------------------
-- this is the temp table to get the overall summary for the requested data.
-- it contains a number of inner temp tables to help make the overall SQL
-- more readable.
year_group as (
    SELECT
        COALESCE(json_agg(a), '[]')
    FROM (
        SELECT
            years.id::text AS "year",
            (
                WITH species_capture_count AS (
                    SELECT
                        species_id        AS "species_id",
                        species_name      AS "name",
                        count(*)          AS "count"
                    FROM
                        presentable_captures dc
                    WHERE (
                        #{dc_where_clause[:where]}
                    )
                    GROUP BY
                    species_id,
                    species_name
                ),

                species_recapture_count AS (
                    SELECT
                        species_id          AS "species_id",
                        species_name        AS "name",
                        count(*)            AS "count"
                    FROM
                        presentable_recaptures rc
                    WHERE (
                      #{species_recapture_where}
                    )
                    GROUP BY
                    species_id,
                    species_name
                ),

                -- combine the previous two tables
                combined_species_count AS (
                    SELECT
                        COALESCE(json_agg(t), '[]') -- combine all the rows into a json array
                    FROM (
                        SELECT
                            CASE -- only have one name column, try to always choose a not nul column
                                WHEN cc.name IS NULL THEN
                                    rc.name
                                ELSE
                                    cc.name
                            END            AS "name",
                            cc.count       AS "capture_count",
                            rc.count    AS "recapture_count"
                        FROM
                            species_capture_count cc
                        FULL OUTER JOIN
                            species_recapture_count rc ON cc.species_id = rc.species_id
                    ) t
                ),

                -- BEGIN the event location list
                -- create the tables for event list
                event_capture_list AS (
                    SELECT
                        capture_date, -- only used for sorting/grouping by
                        species_id           AS "species_id",
                        species_name         AS "species_name",
                        location_id,
                        location_description AS "location",
                        date_string          AS "date",
                        count(*)             AS "count"
                    FROM
                        presentable_captures dc
                    WHERE (
                        (
                            dc.year_id = years.id
                        )
                        AND
                        #{dc_where_clause[:where]}
                    )
                    GROUP BY
                        capture_date,
                        date_string,
                        location_id,
                        location_description,
                        species_id,
                        species_name
                    ORDER BY
                        capture_date
                ),

                event_recapture_list AS (
                    SELECT
                        capture_date, -- only used for sorting/grouping by
                        species_id           AS "species_id",
                        species_name         AS "species_name",
                        location_id,
                        location_description AS "location",
                        date_string          AS "date",
                        count(*)             AS "count"
                    FROM
                        presentable_recaptures rc
                    WHERE (
                      #{species_recapture_where}
                    )
                    GROUP BY
                        capture_date,
                        date_string,
                        location_id,
                        location_description,
                        species_id,
                        species_name
                    ORDER BY
                        capture_date
                ),

                -- combine the previous two tables
                combined_event_list AS (
                    SELECT
                        COALESCE(json_agg(t), '[]')
                    FROM (
                    -- select the
                        SELECT
                            CASE -- case is used to try to select a not null column
                                 WHEN event_capture_list.species_name IS NOT NULL THEN
                                     event_capture_list.species_name
                                 ELSE
                                     event_recapture_list.species_name
                             END                           AS "species_name",

                             CASE -- case is used to try to select a not null column
                                 WHEN event_capture_list.location IS NOT NULL THEN
                                     event_capture_list.location
                                 ELSE
                                     event_recapture_list.location
                            END                         AS "location",

                             CASE -- case is used to try to select a not null column
                                WHEN event_capture_list.date IS NOT NULL THEN
                                     event_capture_list.date
                                 ELSE
                                     event_recapture_list.date
                             END                           AS "date",

                             event_capture_list.count     AS "capture_count",
                             event_recapture_list.count     AS "recapture_count"
                        FROM
                            event_capture_list
                                 FULL OUTER JOIN event_recapture_list
                                     ON (
                                        event_capture_list.species_id = event_recapture_list.species_id
                                        AND event_capture_list.location_id = event_recapture_list.location_id
                                        AND event_capture_list.capture_date = event_recapture_list.capture_date
                                    )
                    ) t  -- end from for json_agg
                )
                SELECT
                    to_json(t) -- combine the items in the subquery into a single json object
                FROM (
                    SELECT
                        (SELECT * FROM combined_species_count) AS "species_counts",
                        (SELECT * FROM combined_event_list)    AS "location_counts"
                ) t
            ) as "summary",
            (
                SELECT
                    to_json(s)
                FROM (
                    SELECT
                        (
                            SELECT
                                COALESCE(json_agg(a), '[]')
                            FROM (
                                SELECT
                                    (
                                        SELECT
                                            to_json(B) -- convert a capture event to a single json object
                                        FROM (
                                            SELECT
                                                 FC.species_name,
                                                 FC.angler_number as "angler_id",
                                                 FC.capture_angler,
                                                 FC.tag_number,
                                                 FC.date_string,
                                                 FC.location,
                                                 FC.time_of_day,
                                                 FC.length,
                                                 FC.length_range,
                                                 FC.fish_condition,
                                                 FC.latitude,
                                                 FC.longitude,
                                                 versions.whodunnit as "user_id",
                                                 concat(users.first_name, ' ', users.last_name) as "user_name",
                                                 (
                                                    SELECT string_agg(roles.name, ',')
                                                    FROM
                                                      roles LEFT JOIN roles_users on roles_users.role_id = roles.id
                                                    where roles_users.user_id = versions.whodunnit
                                                 ) as "user_roles"
                                        ) B
                                    ) AS "capture",
                                    CASE
                                        WHEN FC.recapture_count > 0 THEN
                                        (
                                            SELECT
                                                COALESCE(json_agg(C), '[]')-- combine all the recapture events into a single json array
                                            FROM (
                                                SELECT
                                                    FR.recapture_angler_number as "angler_id",
                                                    FR.recapture_angler_name,
                                                    FR.date_string,
                                                    FR.location,
                                                    FR.time_of_day,
                                                    FR.length,
                                                    FR.length_range,
                                                    FR.recapture_disposition,
                                                    FR.fish_condition,
                                                    FR.latitude,
                                                    FR.longitude,
                                                    versions.whodunnit as "user_id",
                                                    concat(users.first_name, ' ', users.last_name) as "user_name",
                                                    (
                                                      SELECT string_agg(roles.name, ',')
                                                      FROM
                                                        roles LEFT JOIN roles_users on roles_users.role_id = roles.id
                                                      where roles_users.user_id = versions.whodunnit
                                                    ) as "user_roles"
                                                FROM
                                                    presentable_recaptures FR
                                                    LEFT JOIN versions_created as versions ON FR.id = versions.item_id AND versions.item_type = 'Recapture'
                                                    LEFT JOIN users ON users.id = versions.whodunnit
                                                -- only use the recapture that relate to the capture event row
                                                WHERE
                                                  case
                                                    when FC.id IS NULL THEN
                                                      FR.tag_id = FC.tag_id
                                                    ELSE
                                                      FR.capture_id = FC.id
                                                  END
                                                ORDER BY
                                                    FR.capture_date
                                                LIMIT
                                                    FC.recapture_count
                                            ) C
                                        )
                                        ELSE
                                            NULL
                                    END AS "recaptures"
                                FROM (
                                    (
                                      SELECT
                                        *
                                      FROM
                                          presentable_captures dc
                                      WHERE (
                                          (
                                              dc.year_id = years.id
                                          )
                                          AND
                                          #{dc_where_clause[:where]}
                                        )
                                    )
                                    #{capture_list_union}
                                ) FC
                                 LEFT JOIN versions_created as versions ON FC.id = versions.item_id AND versions.item_type = 'Capture'
                                 LEFT JOIN users ON users.id = versions.whodunnit
                            ) a
                        ) AS "items",
                        (
                          SELECT
                              max(recapture_count)
                          FROM (
                              (
                                SELECT
                                    max(recapture_count) as "recapture_count"
                                FROM
                                    presentable_captures dc
                                WHERE (
                                    (
                                        dc.year_id = years.id
                                    )
                                    AND
                                    #{dc_where_clause[:where]}
                                )
                              )
                              #{max_recapture_union}
                          ) a
                        ) AS "max_recaptures"
                ) s
            ) AS "fish_history"
        FROM
            years
    ) a -- end years group json_agg
)

SELECT
    to_json(t) AS "data" -- wrap up the JSON in a topmost element
FROM (
     SELECT -- combine the previous large temp tables into one json object
         (SELECT * FROM overall_summary) AS "overall_summary",
         (SELECT * FROM year_group)      AS "years"
) t
        SQL

        rbinds = rc_where_clause[:binds]
        dbinds = dc_where_clause[:binds]
        sbinds = rc_where_clause[:binds] + dc_where_clause[:binds] + rc_where_clause[:binds] + dc_where_clause[:binds]

        sql_array = [query] + rbinds + (dbinds * 2) + ((sbinds + dbinds) * 4) + dbinds

        ActiveRecord::Base.send(:sanitize_sql_array, sql_array)
      end

      # Takes the params and builds the total where statement to narrow/widen the search as specified
      # Returns the where string in [:where] and the binds in [:binds]
      def generate_where(params, table_alias)

        terms = params[:terms]

        where = ['TRUE']
        binds = []

        temp = create_angler_where(terms[:anglers], table_alias)
        where << temp[:where]
        binds = binds + temp[:binds]

        if terms[:tag_range][:min].nil? || terms[:tag_range][:min].nil?
          temp = create_tag_where(terms[:tags], table_alias)
          where << temp[:where]
          binds = binds + temp[:binds]
        else
          temp = create_tag_range_where(terms[:tag_range], table_alias)
          where << temp[:where]
          binds = binds + temp[:binds]
        end

        temp = create_location_term_where(terms[:location_descriptions], table_alias)
        where << temp[:where]
        binds = binds + temp[:binds]

        temp = create_comment_term_where(terms[:comments], table_alias)
        where << temp[:where]
        binds = binds + temp[:binds]

        temp = create_times_of_day_where(terms[:time_of_day_options], table_alias)
        where << temp[:where]
        binds = binds + temp[:binds]

        temp = create_fish_condition_where(terms[:fish_condition_options], table_alias)
        where << temp[:where]
        binds = binds + temp[:binds]

        temp = create_length_where(terms[:length], table_alias)
        where << temp[:where]
        binds = binds + temp[:binds]

        temp = create_species_where(terms[:species], table_alias)
        where << temp[:where]
        binds = binds + temp[:binds]

        # if a iteration is provided then use that year range, else use the supplied range
        capture_hash = terms[:capture_date].except(:year_ranges)

        temp = create_capture_date_where(capture_hash, table_alias)
        where << temp[:where]
        binds = binds + temp[:binds]

        temp = create_basin_where(terms[:basin], table_alias)
        where << temp[:where]
        binds = binds + temp[:binds]

        temp = create_sub_basin_where(terms[:sub_basin], table_alias)
        where << temp[:where]
        binds = binds + temp[:binds]

        {
            # remove all nil items and empty items in array before joining
            where: where.compact.reject { |t| t.empty? }.map { |t| "(#{t})" }.join("\nAND "),
            binds: binds
        }
      end

      ####################################################################################################################
      # helper functions for configuring params

      def configure_params(params)
        # merge the given params into the defined default ones
        params = params.to_hash
        params.recursive_hash_fix!

        params = DEFAULT_PARAMS.deep_merge(params)

        # update the min and max date to be used later
        #TODO
        #I think this needs to be
        #params = update_capture_date_params(params)
        #update_capture_date_params returns the param object so idk if it's being modified in there fo real
        update_capture_date_params(params)
      end

      def sym_array(array)
        array.each do |value|
          value.symbolize_keys!
        end
      end

      def update_capture_date_params(params)
        capture_date = params[:terms][:capture_date]

        params[:terms][:capture_date][:min] = Chronic.parse(capture_date[:min]) if capture_date[:min] != nil
        params[:terms][:capture_date][:max] = Chronic.parse(capture_date[:max]) if capture_date[:max] != nil

        params
      end

      ####################################################################################################################
      # functions to generate the where statements and their corresponding set of bind variables

      #angler_ids is an array of angler_ids like lax2435 or Lag3495 etc not the actual ID
      def create_angler_where(angler_ids = [], table_alias)
        wheres = []
        bind_values = []

        field = table_alias == 'dc' ? 'angler_number' : 'recapture_angler_number'

        if angler_ids.empty?
          wheres << "#{table_alias}.#{field} IS NULL OR #{table_alias}.#{field} = #{table_alias}.#{field}"
          bind_values
        else
          wheres << "#{table_alias}.#{field} SIMILAR TO ?"
          bind_values << "(#{angler_ids.collect{ |t| t[:angler_id].upcase}.join('|')})".gsub(/\(\)/, '%%')
        end

        {
            where: wheres[0],
            binds: bind_values
        }
      end

      # tag_numbers is an array of tag numbers not IDs
      # will build a hash that takes care of the ID lookups for us
      def create_tag_where(tag_numbers = [], table_alias)
        wheres = []
        bind_values = []

        if tag_numbers.empty?
          wheres << "#{table_alias}.tag_number IS NULL OR #{table_alias}.tag_number = #{table_alias}.tag_number"
        else
          wheres << "#{table_alias}.tag_number SIMILAR TO ?"
          bind_values << "(#{tag_numbers.collect{ |t| t[:tag_number].upcase}.join('|')})".gsub(/\(\)/, '%%')
        end

        {
            where: wheres[0],
            binds: bind_values
        }
      end

      def create_tag_range_where(tag_range = {min: nil, max: nil}, table_alias)
        bind_values = []

        compiled_where = ''

        if tag_range[:min].nil? || tag_range[:max].nil?
          compiled_where = "#{table_alias}.tag_number IS NULL OR #{table_alias}.tag_number = #{table_alias}.tag_number"
        else
          compiled_where = "#{table_alias}.tag_number BETWEEN ? and ?"

          bind_values << tag_range[:min].upcase
          bind_values << tag_range[:max].upcase
        end

        # For both cases
        {
            where: compiled_where,
            binds: bind_values
        }


      end

      # Takes an array of terms to search location_descriptions for
      def create_location_term_where(location_terms = [], table_alias)
        wheres = []
        bind_values = []

        if location_terms.empty?
          wheres << "#{table_alias}.location_description IS NULL OR #{table_alias}.location_description = #{table_alias}.location_description"
        else
          wheres << "#{table_alias}.location_description SIMILAR TO ?"
          bind_values << "%(#{location_terms.collect{ |t| t[:location_term]}.join('|')})%".gsub(/\(\)/, '')
        end

        {
            where: wheres[0],
            binds: bind_values
        }
      end

      # Takes an array of terms to search the comment area for
      def create_comment_term_where(comment_terms = [], table_alias)
        wheres = []
        bind_values = []

        if comment_terms.empty?
          wheres << "#{table_alias}.comments IS NULL OR #{table_alias}.comments = #{table_alias}.comments"
        else
          wheres << "#{table_alias}.comments SIMILAR TO ?"
          bind_values << "%(#{comment_terms.collect{ |t| t[:comment_term]}.join('|')})%".gsub(/\(\)/, '')
        end

        {
            where: wheres[0],
            binds: bind_values
        }
      end

      # Takes an array of times of day to search for
      def create_times_of_day_where(times_of_day = [], table_alias)
        wheres = []
        bind_values = []

        times_of_day.each do |time|
          wheres << '?'
          bind_values << time[:time_of_day].to_i
        end

        {
            where: times_of_day.empty? ? "#{table_alias}.time_of_day_option_id IS NULL OR #{table_alias}.time_of_day_option_id = #{table_alias}.time_of_day_option_id " : "#{table_alias}.time_of_day_option_id = ANY ('{#{wheres.join(',')}}'::int[])",
            binds: bind_values
        }

      end

      # Takes an array of fish_condition ids
      def create_fish_condition_where(fish_conditions = [], table_alias)
        wheres = []
        bind_values = []

        fish_conditions.each do |cond|
          wheres << '?'
          bind_values << cond[:fish_condition].to_i
        end

        {
            where: fish_conditions.empty? ? "#{table_alias}.fish_condition_option_id IS NULL OR #{table_alias}.fish_condition_option_id = #{table_alias}.fish_condition_option_id " :  "#{table_alias}.fish_condition_option_id = ANY ('{#{wheres.join(',')}}'::int[])",
            binds: bind_values
        }

      end

      # Create the where clause for length
      # Handles the case where either min or max or both are not given
      def create_length_where(length = {min: nil, max: nil}, table_alias)
        bind_values = []

        where = "CASE WHEN #{table_alias}.length IS NULL THEN 'NaN' ELSE #{table_alias}.length END <@ numrange(?,?, '[]')"
        bind_values << length[:min]
        bind_values << length[:max]

        {
            where: where,
            binds: bind_values
        }
      end

      def create_species_where(species_ids = [], table_alias)
        wheres = []
        bind_values = []

        species_ids.each do |species_id|
          wheres << '?'
          bind_values << species_id[:species_id].to_i
        end

        {
            where: species_ids.empty? ? "#{table_alias}.species_id IS NULL OR #{table_alias}.species_id = #{table_alias}.species_id" :  "#{table_alias}.species_id = ANY ('{#{wheres.join(',')}}'::int[])",
            binds: bind_values
        }

      end

      #cpature_date is a hash with the lower and upper bound of the capture dates
      def create_capture_date_where(capture_date = {min: nil, max: nil}, table_alias)
        bind_values = []

        where = "#{table_alias}.capture_date <@ tstzrange(?,?, '[]')"
        bind_values << capture_date[:min]
        bind_values << capture_date[:max]

        # For both cases
        {
            where: where,
            binds: bind_values
        }
      end

      #cpature_date is a hash with the lower and upper bound of the capture dates
      def create_basin_where(basin_ids = [], table_alias)
        wheres = []
        bind_values = []

        basin_ids.each do |basin_id|
          wheres << '?'
          bind_values << basin_id[:basin_id].to_i
        end

        {
            where: basin_ids.empty? ? "#{table_alias}.basin_id IS NULL OR #{table_alias}.basin_id = #{table_alias}.basin_id" :  "#{table_alias}.basin_id = ANY ('{#{wheres.join(',')}}'::int[])",
            binds: bind_values
        }
      end

      #cpature_date is a hash with the lower and upper bound of the capture dates
      def create_sub_basin_where(sub_basin_ids = [], table_alias)
        wheres = []
        bind_values = []

        sub_basin_ids.each do |sub_basin_id|
          wheres << '?'
          bind_values << sub_basin_id[:sub_basin_id].to_i
        end

        {
            where: sub_basin_ids.empty? ? "#{table_alias}.sub_basin_id IS NULL OR #{table_alias}.sub_basin_id = #{table_alias}.sub_basin_id" :  "#{table_alias}.sub_basin_id = ANY ('{#{wheres.join(',')}}'::int[])",
            binds: bind_values
        }
      end

      def custom_titleize(phrase_to_titleize)
        result = phrase_to_titleize
        result.tr!('_', ' ')
        result.gsub!(/\bid|\b(?<!['’`])[a-z]/) {|match| match.upcase }
        result
      end

    end # end of class method definitions

  end # end of CustomizableQuery class

end # end of ReportModule

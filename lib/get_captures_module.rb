# Module to perform query to gather data needed by Nessie to construct a spreadsheet
module GetCapturesModule

  # TODO this can be replaced, no need to use this any more, with the denormalized captures
  def module_get_angler_capture_data (angler_id)
    angler_id = Angler.from_ambiguous_id(angler_id).try(:id)
    (query = '') << <<-SQL
      select  tags.tag_no as tag_number,
              to_char(captures.capture_date, 'MM/DD/YYYY' ) as capture_date,
              species.common_name as species,
              round( CAST(captures.latitude as numeric), 6) as latitude,
              round( CAST(captures.longitude as numeric), 6) as longitude,
              -- (select captures.recapture is true) as "Recapture",
              (
                captures.tag_id in (
                                    select tag_id
                                    from recaptures
                                  )
              ) as has_recapture,
              captures.comments as comments,
              captures.length as "length",
              captures.location_description as location_description,
              fish_condition_options.fish_condition_option as fish_condition
      from captures left join species on captures.species_id = species.id
            left join tags on captures.tag_id = tags.id
            left join fish_condition_options on captures.fish_condition_option_id = fish_condition_options.id
      where captures.angler_id = ?
      order by tag_number desc
    SQL

    sanitized_sql = [query,angler_id]
    sql = ActiveRecord::Base.send(:sanitize_sql_array, sanitized_sql)

    ActiveRecord::Base.connection().execute(sql)
  end

end
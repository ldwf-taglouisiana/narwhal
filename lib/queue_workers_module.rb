module QueueWorkersModule
  include DashboardModule

  class CreateNotificationWorker

    def initialize(notification)
      @notification = notification
    end

    def run
      @notification.save!
    end
  end

  class UpdateStatsWebsocket

    def self.send
      Thread.new do
        WebsocketRails[:stats_update].trigger 'update', overall_stats
      end
    end
  end
end
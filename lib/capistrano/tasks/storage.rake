namespace :load do
  task :defaults do
    set :storage_path, -> { 'storage' }
  end
end

namespace :storage do

  desc 'Download all the files in storage'
  task :download do
    on roles(:app) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          # create the filename, using the current time
          stamp = "storage_data_#{Time.now.strftime('%Y-%m-%d_%H-%M-%S')}.txv"
          # pack the contents of the storage directory
          execute :tar, "cf - -C #{fetch(:storage_path)} . | xz -9 -c - > #{current_path}/#{stamp}"
          # download the files from the remote to the local rails root directory
          download! "#{current_path}/#{stamp}", '.'
          # extract the contents to the local storage/
          system "cd #{fetch(:storage_path)}/ && tar -xvf ../#{stamp}"
          # remove the file from the remote
          execute :rm, "#{current_path}/#{stamp}"
          # remove the file from the local
          system "rm #{stamp}"
        end
      end
    end
  end

  desc 'Upload all the files in storage'
  task :upload do
    on roles(:app) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          # create the filename, using the current time
          stamp = "storage_data_#{Time.now.strftime('%Y-%m-%d_%H-%M-%S')}.txv"
          # pack the contents of the storage directory
          system :tar, "cf - -C #{fetch(:storage_path)} . | xz -9 -c - > /tmp/#{stamp}"
          # download the files from the remote to the local rails root directory
          upload! "/tmp/#{stamp}", "#{current_path}/tmp/#{stamp}"
          # extract the contents to the local storage/
          execute "cd #{shared_path}/#{fetch(:storage_path)}/ && tar -xvf #{current_path}/tmp/#{stamp}"
          # remove the file from the remote
          system :rm, "/tmp/#{stamp}"
          # remove the file from the local
          execute "rm #{current_path}/tmp/#{stamp}"
        end
      end
    end
  end

end
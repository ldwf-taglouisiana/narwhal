namespace :load do
  task :defaults do
    set :websocket_rails_pid, -> { File.join(current_path, "tmp", "pids", "websocket_rails.pid") }
    set :websocket_rails_restart_sleep_time, 3
  end
end

namespace :websocket_rails do

  desc 'Stop the websocket rails listener process'
  task :stop do
    on roles(:app) do
      if test("[ -e #{fetch(:websocket_rails_pid)} ]")
        if test("kill -0 #{websocket_rails_pid}")
          info "stopping websocket rails..."
          execute :kill, "-s QUIT", websocket_rails_pid
        else
          info "cleaning up dead websocket rails pid..."
          execute :rm, fetch(:websocket_rails_pid)
        end
      else
        info "websocket rails is not running..."
      end
    end
  end

  desc 'Start the websocket process'
  task :start do
    on roles(:app) do
      within release_path do
        with rails_env: fetch(:rails_env) do

          if test("[ -e #{fetch(:websocket_rails_pid)} ] && kill -0 #{websocket_rails_pid}")
            info "websocket rails is running..."
          else
            execute :bundle, :exec, :rake, 'websocket_rails:start_server'
          end

        end
      end
    end
  end

  desc 'Restart the websocket rails process'
  task :restart do
    on roles(:app) do
      invoke 'websocket_rails:stop'
      execute :sleep, fetch(:websocket_rails_restart_sleep_time)
      invoke 'websocket_rails:start'
    end
  end

end

def websocket_rails_pid
  "`cat #{fetch(:websocket_rails_pid)}`"
end
namespace :task do
  desc 'Execute the specific rake task - Ex. cap staging "task:invoke[db:migrate]"'
  task :invoke, :command do |task, args|
    on roles(:app) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :bundle, :exec, :rake, args[:command], "DESTINATION=#{shared_path}"
        end
      end
    end
  end
end
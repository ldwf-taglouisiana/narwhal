namespace :eye do

  task :start do
    on roles(fetch(:eye_roles)), in: :groups, limit: 3, wait: 10 do
      within current_path do
        with fetch(:eye_env) do
          execute :bundle, "exec eye start narwhal"
        end
      end
    end
  end

  task :stop do
    on roles(fetch(:eye_roles)), in: :groups, limit: 3, wait: 10 do
      within current_path do
        with fetch(:eye_env) do
          execute :bundle, "exec eye stop narwhal"
        end
      end
    end
  end

  task :restart do
    on roles(fetch(:eye_roles)), in: :groups, limit: 3, wait: 10 do
      within current_path do
        with fetch(:eye_env) do
          execute :bundle, "exec eye restart narwhal"
        end
      end
    end
  end

  desc "Start eye with the desired configuration file"
  task :load_config do
    on roles(fetch(:eye_roles)) do
      within current_path do
        with fetch(:eye_env) do
          execute :bundle, "exec eye load #{fetch(:eye_config_file)}"
        end
      end
    end
  end

end
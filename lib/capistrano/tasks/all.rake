namespace :all do
  desc 'Stop all services'
  task :stop do
    on roles(:app) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          invoke 'unicorn:stop'
          invoke 'websocket_rails:stop'
          invoke 'resque:stop'
          invoke 'unoconv:stop'
        end
      end
    end
  end
end
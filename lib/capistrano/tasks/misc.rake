namespace :services do
  task :restart do
    invoke 'resque:pool:restart'
  end
end

namespace :app do
  task :update_rvm_key do
    on roles(:app) do
      execute "gpg --keyserver hkp://keys.gnupg.net --recv-keys D39DC0E3"
    end
  end
end

namespace :misc do
  task :install_bundler do
    on roles(:app) do
      with rails_env: fetch(:rails_env) do
      execute :gem, 'update --system'
      execute :gem, 'install bundler'
      execute :gem, 'update bundler'
    end
    end
  end
end



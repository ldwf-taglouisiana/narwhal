module FuzzyMapModule

  #takes an array of events and optionally a hash of parameters and returns parsable json and a map link for fuzzing
  #recapture locations
  #The array of events is expected to start with the original capture, order doesn't matter otherwise
  #The array MUST start with the original capture, If there is not an original capture, then the index 0 MUST BE NIL
  def get_fuzzy_map(events, params = { height: 790, width: 387, combined: false, requested_event: nil})
    result = []

    params[:requested_event] = events.first if params[:requested_event].nil?

    if params[:combined]
      result = combined(events, params)
    else
      result = non_combined(events, params)
    end

    result
  end


  private

  def combined(events, params)
    result = []
    names = []
    queries = []

    events.each_with_index do |event, index|
      names << index.to_words
      queries << event_query(names.last , params[:requested_event], event, index)
    end

    sql = final_query({names: names, event_queries: queries})

    connection = ActiveRecord::Base.connection
    geo_json = connection.execute(sql)[0]['json']
    result << static_map_from_geo_json(geo_json, params[:width], params[:height])

    result
  end

  def non_combined(events, params)
    result = []

    events.each do |event|
      queries = []
      names = %w(one two)

      queries << event_query(names.first, params[:requested_event], event, 0)
      queries << event_query(names.last , event, params[:requested_event], 1)

      sql = final_query({names: names, event_queries: queries})

      connection = ActiveRecord::Base.connection
      geo_json = connection.execute(sql)[0]['json']
      result << static_map_from_geo_json(geo_json, params[:width], params[:height])
    end

    result
  end

  def event_query(name, requested_event, other_event, index)
    <<-SQL
#{name} as (
      SELECT  #{should_show_location?(requested_event, other_event) ? 'A.geom as geom' : 'st_geometryn(st_simplify(S.geom, 0.005), 1) as geom'},
              #{other_event.instance_of?(Recapture)} as "recapture", -- indicate the type of event in SQL based on event type
              #{index} as "id"                             -- use the index as the id of this event
      from #{other_event.instance_of?(Capture) ? 'captures' : 'recaptures'} A -- pick the table based on the type of the event
            #{'inner join segments S on st_within(A.geom,S.geom)' unless should_show_location?(requested_event, other_event) } -- add the shape table if we cannot show exact location
      where A.id = #{ other_event.nil? ? 'NULL' : other_event.id} -- the NULL is a hack to always have the first event, event if the event has not location
      )
    SQL
  end

  def final_query(params = {names: [], event_queries: []})
    queries = params[:event_queries].join(',')
    unions = params[:names].map{ |m| "(select * from #{m})" }.join(' union all ')

    <<-SQL
      WITH #{ queries }, --the part built from above
      combined as (
        SELECT geom, recapture, id
        FROM (
          #{ unions }
        ) B
      )
      SELECT row_to_json(t) as "json"
      FROM (
        SELECT 'FeatureCollection' AS type,
               array_to_json(array_agg(row_to_json(m))) AS features
        FROM (
          SELECT 'Feature' AS type,
                 ST_AsGeoJSON(geom)::json AS geometry,
                 (
                   SELECT row_to_json(b)::json
                   FROM (
                     SELECT id, recapture
                     FROM combined
                     WHERE g.id = id
                   ) b
                 ) AS properties
          FROM combined g
          WHERE g.geom is not null
        ) m
      ) t;
    SQL
  end

  def static_map_from_geo_json(geo_json, width, height)
    # parse the json string
    json = JSON.parse(geo_json, :symbolize_names => true)

    # used to store the converted features to encode in the URL
    markers = ['']  # empty string added so we can prepend the group with a '&' using a join
    polygons = [''] # empty string added so we can prepend the group with a '&' using a join

    # go through each feature and encode
    result = {}

    #this guard is needed in case one or both of the captures lack location data or more specifically a capture's
    #location is in an area we don't have shape data for.
    #Case:
    # Capture in Texas
    # Recapture with no location data
    #May need to check for this method returning nil to format calling pages nicely.
    if json[:features] != nil
      json[:features].each do |feature|

        # used to determine the color properties of the feature
        # TODO need to make this a little more consistent for n events
        if feature[:properties][:recapture]
          fill_color = '0xFF1919'
          stroke_color = '0xFF1919'
          marker_color = 'red'
        else
          fill_color = '0x06ff00'
          stroke_color = '0x06ff00'
          marker_color = 'green'
        end

        # if ferature is a polygon then use the base64 encoding process
        if (feature[:geometry] != nil) and (feature[:geometry][:type] == 'Polygon')
          enc = encode_polygon(feature[:geometry][:coordinates])
          polygons << "path=fillcolor:#{fill_color}|color:#{stroke_color}|enc:#{enc}"
        else
          # the feature is a Point
          lat = feature[:geometry][:coordinates][1]
          lon = feature[:geometry][:coordinates][0]
          markers << "markers=color:#{marker_color}|#{lat},#{lon}"
        end

      end
      result[:json] = geo_json
      result[:map_url] = "http://maps.googleapis.com/maps/api/staticmap?sensor=false&size=#{width}x#{height}&scale=2#{markers.join('&')}#{polygons.join('&')}"

    end


    result
  end

  def should_show_location?(requested_event, other_event)
    !other_event.obscure_location_from?(requested_event)
  end

  # polygon encoding helpers. Created from:
  # https://developers.google.com/maps/documentation/utilities/polylinealgorithm
  # https://github.com/JasonSanford/geojson-google-maps/blob/master/GeoJSON.js

  def encode_polygon(polygon_coordinates)
    paths = []

    exterior_direction = nil
    interior_direction = nil
    polygon_coordinates.each_with_index do |i, index|

      path = []
      i.each do |j|
        c = {:lat => j[1].round(5), :lon => j[0].round(5)}
        path << c
      end

      if index == 0
        exterior_direction = _ccw(path)
        paths << path
      elsif index == 1
        interior_direction = _ccw(path)
        if exterior_direction == interior_direction
          paths << path.reverse
        else
          paths << path
        end
      else
        if exterior_direction == interior_direction
          paths + path.reverse
        else
          paths << path
        end
      end

    end

    paths = paths.flatten

    deltas = []

    # get the deltas for the points
    paths.each_with_index do |item, index|
      if index == 0
        deltas << item
      else
        deltas << {:lat => ((item[:lat] - paths[index - 1][:lat])).round(5), :lon => ((item[:lon] - paths[index - 1][:lon])).round(5)}
      end
    end

    # if we have too many points, try to reduce it
    if deltas.count > 300

      # remove the points that are too close to each other
      deltas.each_with_index do |d, index|
        if d[:lat].abs > 0.00025 and d[:lon].abs > 0.00025
          paths.delete_at(index)
        end
      end

      # recalculate the new set of deltas
      deltas = []
      paths.each_with_index do |item, index|
        if index == 0
          deltas << item
        else
          deltas << {:lat => ((item[:lat] - paths[index - 1][:lat])).round(5), :lon => ((item[:lon] - paths[index - 1][:lon])).round(5)}
        end
      end
    end



    encode_array(deltas).join('')
  end

  def encode_array(array)
    result = []

    array.each do |item|
      result << encodeCoordinates(item)
    end

    result
  end

  def encodeCoordinates(data = {:lat => 0, :lon => 0})
    if data.empty?
      return ''
    end

    encodeCoord(data[:lat]) + encodeCoord(data[:lon])
  end

  def encodeCoord(value)
    result = (value.abs * (10 ** 5)).to_i

    result = (result.to_i & 0xffffffff)

    if value < 0
      result = (result.to_i ^ 0xffffffff)
      result = result + 0x01
    end

    result = ((result << 1) & 0xffffffff)

    result = ((result ^ 0xffffffff) & 0xffffffff) if value < 0

    result = to_byte_array(result).reverse

    result = result.each_with_index.map do |item, index|
      if (result.count - 1) == index
        item
      else
        item | 0x20
      end
    end

    result = result.map{ |item| item.to_i + 63}

    result = result.map{ |item| item.chr}

    result.join('').gsub(/\\/,'\\\\')
  end

  def to_byte_array(num)
    result = []

    begin
      result << (num & 0x1f)
      num >>= 5
    end until num == 0

    result.reverse
  end

  def _ccw(path)
    a = 0

    (path.count - 2).times do |i|
      a += ((path[i+1][:lat] - path[i][:lat]) * (path[i+2][:lon] - path[i][:lon]) - (path[i+2][:lat] - path[i][:lat]) * (path[i+1][:lon] - path[i][:lon]));
    end

    return a > 0
  end
end
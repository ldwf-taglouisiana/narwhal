module CaptureSpreadsheetModule
  class CaptureSpreadsheet
    def self.export(start_date, end_date, options = {})

      start_date = start_date ? "#{Chronic.parse(start_date).beginning_of_day.iso8601}" : "1000-01-01"
      end_date = end_date ? "#{Chronic.parse(end_date).end_of_day.iso8601}" : 'now()'

      where_clause = ''

      if options[:summary]
        where_clause = "created_at between '#{start_date}' and '#{end_date}'"
      elsif options[:all]
        where_clause = "capture_date between '#{start_date}' and '#{end_date}'"
      #elsif options[:deleted]
      #  where_clause = "deleted is true and capture_date between '#{start_date}' and '#{end_date}'"
      else # all non deleted captures in that range
        where_clause = "capture_date between '#{start_date}' and '#{end_date}'"
      end

      (query = '') << <<-sql
      with caps as (
        select *
        from captures
        #{
      unless where_clause.empty?
        "where #{where_clause}"
      end
      }
      )
      select E.*, F.first_name, F.last_name
      from (
        select C.angler_id, C.capture_date, C.common_name, C.latitude, C.longitude, C.verified, D.tag_no from (
            select A.*, B.common_name
            from caps A left join species B on A.species_id = B.id
          ) C left join tags D on C.tag_id = D.id
      ) E left join anglers F on E.angler_id = F.id
      sql

      @caps = ActiveRecord::Base.connection.execute(query)

      (query = '') << <<-sql
      with caps as (
        select *
        from captures
        #{
      unless where_clause.empty?
        "where #{where_clause}"
      end
      }
      )
      select E.*, F.first_name, F.last_name
      from (
        select C.angler_id, C.capture_date, C.common_name, C.latitude, C.longitude, C.verified, D.tag_no from (
            select A.*, B.common_name
            from caps A left join species B on A.species_id = B.id
          ) C left join tags D on C.tag_id = D.id
      ) E left join anglers F on E.angler_id = F.id
      sql

      @recaps = ActiveRecord::Base.connection.execute(query)

      (query = '') << <<-sql
      with caps as (
        select *
        from captures
        #{
      unless where_clause.empty?
        "where #{where_clause}"
      end
      }
      )
      select B.common_name, count(B.common_name) as species_count
      from caps A left join species B on A.species_id = B.id
      group by B.common_name
      sql

      @species_capture_breakdown = ActiveRecord::Base.connection.execute(query)

      (query = '') << <<-sql
      with caps as (
        select *
        from captures
        #{ "where #{where_clause}" unless where_clause.empty? }
      )
      select B.common_name, count(B.common_name) as species_count
      from caps A left join species B on A.species_id = B.id
      group by B.common_name
      sql

      @species_recapture_breakdown = ActiveRecord::Base.connection.execute(query)

      p = Axlsx::Package.new
      wb = p.workbook
      wb.add_worksheet(:name => 'Captures') do |sheet|
        sheet.add_row ['Angler ID', 'Angler Name', 'Tag Number', 'Capture Date', 'Species', 'Recapture', 'Latitude', 'Longitude', 'Verified']
        @caps.each do |c|
          sheet.add_row [c['angler_id'], "#{c['first_name']} #{c['last_name']}", c['tag_no'], c['capture_date'], c['common_name'], c['recapture'], c['latitude'], c['longitude'], c['verified']]
        end
      end
      wb.add_worksheet(:name => 'Recaptures') do |sheet|
        sheet.add_row ['Angler ID', 'Angler Name', 'Tag Number', 'Capture Date', 'Species', 'Recapture', 'Latitude', 'Longitude', 'Verified']
        @recaps.each do |c|
          sheet.add_row [c['angler_id'], "#{c['first_name']} #{c['last_name']}", c['tag_no'], c['capture_date'], c['common_name'], c['recapture'], c['latitude'], c['longitude'], c['verified']]
        end
      end
      wb.add_worksheet(:name => 'Species Breakdown') do |sheet|
        sheet.add_row ['Captured Species Name', 'Number Captured']
        @species_capture_breakdown.each do |s|
          sheet.add_row [s['common_name'], s['species_count']]
        end
        sheet.add_row ['', '']
        sheet.add_row ['Recaptured Species Name', 'Number Recaptured']
        @species_recapture_breakdown.each do |s|
          sheet.add_row [s['common_name'], s['species_count']]
        end
      end

      p
    end

    def self.import(file)
      result = Hash.new
      spreadsheet = Roo::Excelx.new(file)
      header = spreadsheet.row(1)
      ActiveRecord::Base.transaction do
        # From row 2 to the last row of the spreadsheet, create a new capture from the data in each row
        2.upto(spreadsheet.last_row) do |line|
          row = Hash[[header, spreadsheet.row(line)].transpose]
          capture = Capture.new
          capture = Recapture.new if row['Recapture'] == 'TRUE'

          capture.tag_id = Tag.where(:tag_no => row['Tag Number']).first.id unless row['Tag Number'].nil? || Tag.where(:tag_no => row['Tag Number']).first.nil?
          capture.capture_date = row['Capture Date'].to_datetime unless row['Capture Date'].nil?
          capture.angler_id = row['Angler ID'] unless Angler.from_ambiguous_id(row['Angler ID']).nil?
          capture.time_of_day_option_id = row['Time of Day']
          capture.location_description = row['Location Description']
          capture.species = Species.where(:common_name => row['Species']).first unless row['Species'].nil?
          capture.length = row['Length']
          capture.fish_condition_option_id = row['Fish Condition']

          # Only record the contents of the "Recapture Disposition" column if the capture is a recapture
          capture.recapture_disposition_id = row['Recapture Disposition'] if row['Recapture'] == 'TRUE'

          capture.latitude = row['Latitude']
          capture.longitude = row['Longitude']
          capture.comments = row['Comments']

          # If everything goes cool
          if capture.save
            result[:success] = 'Captures successfully saved.'
          else
            err = []
            capture.errors.each do |e|
              err << e
            end
            err = err.join(', ')
            subbed_err = substitute(err)
            result[:error] = 'invalid data in column(s) (' + subbed_err + ') on row ' + line.to_s + '.'

            raise ActiveRecord::Rollback
          end
        end # end of row iteration
      end # end of transaction

      return result
    end

    private

    # Helper method to replace the names of capture variables in an error string with their associated
    # column names.
    def self.substitute (string)
      subbed_err = string.clone
      subbed_err.sub!('tag_id', 'Tag Number')
      subbed_err.sub!('capture_date', 'Capture Date')
      subbed_err.sub!('angler_id', 'Angler ID')

      subbed_err
    end
  end
end
package main

import (
	"database/sql"
	"encoding/json"
	"flag"
	"fmt"
	_ "github.com/lib/pq"
	"github.com/tealeg/xlsx"
	"io/ioutil"
//	"time"
)

var CAPTURE_ROW_KEYS = []string{
    "species_name",
    "angler_id",
    "capture_angler",
    "tag_number",
    "date_string",
    "location",
    "time_of_day",
    "length",
    "length_range",
    "fish_condition",
    "latitude",
    "longitude",
    "user_id",
    "user_name",
    "user_roles"}

var RECAPTURE_ROW_KEYS = []string{
    "recapture_angler_id",
    "recapture_angler_name",
    "date_string",
    "location",
    "time_of_day",
    "length",
    "length_range",
    "recapture_disposition",
    "fish_condition",
    "latitude",
    "longitude",
    "user_id",
    "user_name",
    "user_roles"}

var CAPTURE_HEADER_NAMES = []string{
    "Species Name",
    "Angler ID",
    "Capture Angler",
    "Tag Number",
    "Capture Date",
    "Location",
    "Time Of Day",
    "Length",
    "Length Range",
    "Fish Condition",
    "Latitude",
    "Longitude",
    "User ID",
    "User Name",
    "User Roles"}

var RECAPTURE_HEADER_NAMES = []string{
    "Recapture Angler ID",
    "Recapture Angler Name",
    "Recapture Date",
    "Location",
    "Time Of Day",
    "Length",
    "Length Range",
    "Recapture Disposition",
    "Fish Condition",
    "Latitude",
    "Longitude",
    "Recapture User ID",
    "Recapture User Name",
    "Recapture User Roles"}

var SUMMARY_ROW_KEYS = []string{
    "species_name",
    "location",
    "date",
    "capture_count",
    "recapture_count"}

var SUMMARY_HEADER_NAMES = []string{
    "Species Name",
    "Location",
    "Date",
    "Capture Count",
    "Recapture Count"}

var dbHost *string
var dbName *string
var dbUser *string
var dbPassword *string

var includeRecaptureColumns *bool

var inFilePath *string
var outFilePath *string

func main() {
	/*
		Get the file path arguments
	*/
	dbHost = flag.String("host", "localhost", "Input file containing the JSON string")
	dbName = flag.String("database", "", "Input file containing the JSON string")
	dbUser = flag.String("username", "", "Input file containing the JSON string")
	dbPassword = flag.String("password", "", "Input file containing the JSON string")

	includeRecaptureColumns = flag.Bool("recaptureColumns", false, "Include the recaptures columns")

	inFilePath = flag.String("query-file", "", "Input file containing the JSON string")
	outFilePath = flag.String("out-file", "", "Output file to export the spreadsheet to")

	flag.Parse()

	fmt.Println("host:", *dbHost)
	fmt.Println("database:", *dbName)
	fmt.Println("recaptureColumns:", *includeRecaptureColumns)
	fmt.Println("query-file:", *inFilePath)
	fmt.Println("out-file:", *outFilePath)

	/*
		Quit if both files are not provided
	*/
	if *inFilePath == "" || *outFilePath == "" {
		panic("must provide file names")
	}

	/*
		open the db
	*/

    // set the ssl mode
    sslMode := "require"

    // if this is on the localhost then disable the ssl
    if *dbHost == "localhost" {
        sslMode = "disable"
    }

	connString := fmt.Sprintf("host=%s dbname=%s user=%s password=%s sslmode=%s", *dbHost, *dbName, *dbUser, *dbPassword, sslMode)

	// used a connection debugging string
//	fmt.Println(connString)

	db, err := sql.Open("postgres", connString)
	check(err)
	defer db.Close()

	/*
		Read the input file containing the query string
	*/
	query, err := ioutil.ReadFile(*inFilePath)
	check(err)

	/*
		Run the query
	*/

//	begin := time.Now()
//	start := time.Now()

	var jsonData string
	err = db.QueryRow(string(query)).Scan(&jsonData)
	check(err)

//	elapsed := time.Since(start)
//	fmt.Println("running query took %s", elapsed)

//	start = time.Now()
	// convert the file to a string variable
	var jsonBytes = []byte(jsonData)

	/*
		Parse the JSON string into a map
	*/
	var f interface{}
	err = json.Unmarshal(jsonBytes, &f)
	check(err)

//	elapsed = time.Since(start)
//	fmt.Println("parsing json took %s", elapsed)

//	start = time.Now()
	/*
		Create the Spreadsheet from the JSON map
	*/
	m := f.(map[string]interface{})
	var file = createSpreadSheet(m)
	//	createSpreadSheet(m)

	err = file.Save(*outFilePath)
	check(err)

//	elapsed = time.Since(start)
//	fmt.Println("spreadsheet took %s", elapsed)

//	elapsed = time.Since(begin)
//	fmt.Println("total time %s", elapsed)
}

// ---------------------------------------------------------------------------------------------------------------------
// PRIMARY HELPERS
// ---------------------------------------------------------------------------------------------------------------------

func createSpreadSheet(f map[string]interface{}) *xlsx.File {
	file := xlsx.NewFile()

    // -----------------------------------------------------------------------------------------------------------------

    locations := f["overall_summary"].(map[string]interface{})["location_counts"].([]interface{})
    buildOverallLocationSummarySheet(file, locations)

    // -----------------------------------------------------------------------------------------------------------------

    buildYearSheets(file, f["years"].([]interface{}))

    // -----------------------------------------------------------------------------------------------------------------

    buildAllDetailSheet(file, f["years"].([]interface{}))

	return file
}


// ---------------------------------------------------------------------------------------------------------------------
// SHEET HELPERS
// ---------------------------------------------------------------------------------------------------------------------

func buildOverallLocationSummarySheet(file *xlsx.File, locations []interface{}) {
    sheet, _ := file.AddSheet("Overall Summary")

    buildSummaryHeaderRow(sheet.AddRow())

    for _, data := range locations {
        row := sheet.AddRow()
        buildSummaryRow(data.(map[string]interface{}), row)
    }

}

func buildYearSheets(file *xlsx.File, years []interface{}) {
    for _, year := range years {

        // the event list for the current year
        yearData := year.(map[string]interface{})

        yearString  := yearData["year"].(string)
        maxRecaptureCount := yearData["fish_history"].(map[string]interface{})["max_recaptures"].(float64)
        eventList   := yearData["fish_history"].(map[string]interface{})["items"].([]interface{})
        summaryList := yearData["summary"].(map[string]interface{})["location_counts"].([]interface{})

        // -------------------------------------------------------------------------------------------------------------

        sheet, _ := file.AddSheet(fmt.Sprintf("%s Summary", yearString))
        buildSummaryHeaderRow(sheet.AddRow())

        for _, data := range summaryList {
            buildSummaryRow(data.(map[string]interface{}), sheet.AddRow())
        }

        // -------------------------------------------------------------------------------------------------------------

        sheet, _ = file.AddSheet(fmt.Sprintf("%s Detail", yearString))
        headerRow := sheet.AddRow()

        buildCaptureHeaderRow(headerRow)

		if *includeRecaptureColumns {
			buildRecaptureHeaderRow(headerRow, int(maxRecaptureCount))
		}

        for _, data := range eventList {
            buildEventRow(sheet, data.(map[string]interface{}))
        }

    }
}

func buildAllDetailSheet(file *xlsx.File, years []interface{}) {
    maxRecapturesOverall := 0.0

    for _, year := range years {
        yearData := year.(map[string]interface{})

        maxRecaptureCount := yearData["fish_history"].(map[string]interface{})["max_recaptures"].(float64)

        if (maxRecaptureCount > maxRecaptureCount) {
            maxRecapturesOverall = maxRecaptureCount
        }
    }

    sheet, _ := file.AddSheet("All Detail")
    headerRow := sheet.AddRow()

    buildCaptureHeaderRow(headerRow)

	if *includeRecaptureColumns {
		buildRecaptureHeaderRow(headerRow, int(maxRecapturesOverall))
	}

    for _, year := range years {
        // the event list for the current year
        yearData := year.(map[string]interface{})

        eventList   := yearData["fish_history"].(map[string]interface{})["items"].([]interface{})

        for _, data := range eventList {
            buildEventRow(sheet, data.(map[string]interface{}))
        }

    }
}

func buildEventRow(sheet *xlsx.Sheet, event map[string]interface{}) {
    row := sheet.AddRow()

    /*
        Loop through the capture
    */

    capture := event["capture"].(map[string]interface{})
    buildCaptureRow(capture, row)

    /*
        Loop through the recaptures list
    */

    if event["recaptures"] != nil && *includeRecaptureColumns {
        for _, recapture := range event["recaptures"].([]interface{}) {
            buildRecaptureRow(recapture.(map[string]interface{}), row)
        }
    }
}

// ---------------------------------------------------------------------------------------------------------------------
// HEADER HELPERS
// ---------------------------------------------------------------------------------------------------------------------

func buildSummaryHeaderRow(row *xlsx.Row) {
    buildHeaderRow(row, SUMMARY_HEADER_NAMES, 1)
}

func buildCaptureHeaderRow(row *xlsx.Row) {
	buildHeaderRow(row, CAPTURE_HEADER_NAMES, 1)
}

func buildRecaptureHeaderRow(row *xlsx.Row, timesToRepeat int){
    buildHeaderRow(row, RECAPTURE_HEADER_NAMES, timesToRepeat)
}

func buildHeaderRow(row *xlsx.Row, names []string, timesToRepeat int) {
	var cell *xlsx.Cell

	for index := 0; index < timesToRepeat; index++ {
		for _, item := range names {
			cell = row.AddCell()
			cell.Value = item
		}
	}
}

// ---------------------------------------------------------------------------------------------------------------------
// ROW HELPERS
// ---------------------------------------------------------------------------------------------------------------------

func buildSummaryRow(item map[string]interface{}, row *xlsx.Row) {
    buildRow(item, SUMMARY_ROW_KEYS, row)
}

func buildCaptureRow(item map[string]interface{}, row *xlsx.Row) {
    buildRow(item, CAPTURE_ROW_KEYS, row)
}

func buildRecaptureRow(item map[string]interface{}, row *xlsx.Row) {
    buildRow(item, RECAPTURE_ROW_KEYS, row)
}

func buildRow(item map[string]interface{}, keys []string, row *xlsx.Row) {
	for _, key := range keys {
		cell := row.AddCell()
        value := item[key]

		switch value.(type) {
            case string:
                cell.SetString(value.(string))
            case float64:
                cell.SetFloat(value.(float64))
            case bool:
                cell.SetBool(value.(bool))
            default:
                cell.Value = ""
		}
	}
}

// ---------------------------------------------------------------------------------------------------------------------
// MISC HELPERS
// ---------------------------------------------------------------------------------------------------------------------

func check(e error) {
	if e != nil {
		panic(e)
	}
}

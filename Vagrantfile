# -*- mode: ruby -*-
# vi: set ft=ruby :

require 'yaml'

Vagrant.require_version '>= 1.8.0'

dir = File.dirname(File.expand_path(__FILE__))
data = YAML.load_file("#{dir}/config/vagrant/config.yaml")['vagrantfile-local']
database_config = YAML.load_file("#{dir}/config/database.yml")

required_plugins = %w(vagrant-triggers vagrant-env )
required_plugins.each do |plugin|
  exec "vagrant plugin install #{plugin};vagrant #{ARGV.join(" ")}" unless Vagrant.has_plugin? plugin || ARGV[0] == 'plugin'
end

# install the roles via ansible galaxy
unless File.exist?("#{dir}/.ansible-roles")
  puts 'Installing ansible roles'
  `ansible-galaxy install -i -f -r #{dir}/config/ansible/requirements.yml 2> /dev/null`
  `touch #{dir}/.ansible-roles`
end

Vagrant.configure('2') do |config|

  config.env.enable

  #--------------------------------------------------------------
  #
  #   Config the box
  #
  #--------------------------------------------------------------
  # config.vm.box = "#{data['box']}"
  # config.vm.box_url = "#{data['box_url']}" unless data['box_url'].nil?

  config.vm.box = "trusty64"
  config.vm.box_url = "http://cloud-images.ubuntu.com/vagrant/trusty/current/trusty-server-cloudimg-amd64-vagrant-disk1.box"

  if data['hostname'].to_s.strip.length != 0
    config.vm.hostname = "#{data['hostname']}"
  end

  config.vm.network 'private_network', ip: data['network']['private_network']

  data['network']['forwarded_port'].each do |port|
    if port['guest'] != '' && port['host'] != ''
      config.vm.network :forwarded_port, guest: port['guest'].to_i, host: port['host'].to_i
    end
  end

  unless data['post_up_message'].nil?
    config.vm.post_up_message = "#{data['post_up_message']}"
  end

  # enable NF, will work on *nix and OSX without a problem
    config.vm.synced_folder '.', '/vagrant', nfs: true

  #--------------------------------------------------------------
  #
  #   Provider
  #
  #--------------------------------------------------------------

  config.vm.provider :virtualbox do |v, override|
    v.gui = false

    # Linked clones for extremely fast import of machines
    v.linked_clone = true

    v.memory = data['memory']
    v.cpus = data['cpus']

    # box customizations for speed
    v.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
    v.customize ["modifyvm", :id, "--audio", "none"]
    v.customize ["modifyvm", :id, "--clipboard", "bidirectional"]
    v.customize ["modifyvm", :id, "--usb", "off"]
    v.customize ["modifyvm", :id, "--ioapic", "on"]
  end

  #--------------------------------------------------------------
  #
  #   Install Ansible on the VM
  #
  #--------------------------------------------------------------
  config.vm.provision :shell do |shell|
    shell.args = ['startup-once']
    shell.privileged = true
    shell.inline = <<-SCRIPT
      if [ ! -x /usr/bin/ansible ]; then
        echo "Installing Ansible on the Vagrant machine"
        sudo apt-get update &> /dev/null
        sudo apt-get install software-properties-common  &> /dev/null
        sudo apt-add-repository ppa:ansible/ansible  &> /dev/null
        sudo apt-get update &> /dev/null
        sudo apt-get install -y ansible &> /dev/null
      fi
    SCRIPT
  end

  #--------------------------------------------------------------
  #
  #   user Ansible to configure the machine
  #
  #--------------------------------------------------------------
  config.vm.provision :ansible do |ansible|
    ansible.inventory_path = 'config/ansible/hosts'
    ansible.playbook = "config/ansible/vagrant.yml"
    ansible.limit = 'vagrant' # you can use this value with the reply, to pick up where it left off before
    ansible.vault_password_file = '.ansible_vault_password' # you can use this value with the reply, to pick up where it left off before
    # ansible.tags = 'ssh'
    # ansible.raw_arguments = '-v'
  end

  #--------------------------------------------------------------
  #
  #   Startup Scripts
  #
  #--------------------------------------------------------------
  config.vm.provision :shell, privileged: false, run: 'once' do |s|
    s.path = "#{dir}/config/vagrant/scripts/setup.sh"
    s.args = ['startup-once']
    s.env = {
        'VAGRANT_RUBY_VERSION' => '2.3.0',
        'DEVELOPMENT_DATABASE_NAME' => database_config['development']['database'],
        'STAGING_DATABASE_NAME' => database_config['staging']['database']
    }
  end

  config.vm.provision :shell, privileged: false, run: 'always' do |s|
    s.path = "#{dir}/config/vagrant/scripts/startup.sh"
    s.args = ['startup-always']
    s.env = {
        'VAGRANT_RUBY_VERSION' => '2.3.0',
        'RAILS_ENV' => 'development'
    }
  end

  config.trigger.after [:provision, :up, :reload] do
    system('echo "
        rdr pass on lo0 inet proto tcp from any to 127.0.0.1 port 80 -> 127.0.0.1 port 8080
        rdr pass on lo0 inet proto tcp from any to 127.0.0.1 port 443 -> 127.0.0.1 port 4443
      " | sudo pfctl -ef - > /dev/null 2>&1; echo "==> Fowarding Ports: 80 -> 8080, 443 -> 4443 & Enabling pf"')
  end

  config.trigger.after [:halt, :destroy] do
    system("sudo pfctl -df /etc/pf.conf > /dev/null 2>&1; echo '==> Removing Port Forwarding & Disabling pf'")
  end
end
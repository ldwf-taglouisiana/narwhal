## Narwhal management server for TagLouisiana

This is the server that manages all the data for the LDWF fish tagging program in partnership with CCA.

Traditional fish tagging: The tradition fish tagging effort is a voluntary program aimed at a better understanding of the movement patterns and habitat use of targeted fish species. This STIS includes a geospatial portal that enables LDWF to perform data entry, content management and report generation. The portal enables anglers to perform on- line submission and manage their own captures/recaptures. Submission can also be performed via mobile app while the angler is still on the water.

### Checking out project

You will need to clone with the git submodules

```
git clone --recurse-submodules git@gitlab.com:ldwf-taglouisiana/narwhal.git
git pull --recurse-submodules
git submodule update --init --recursive
```

To get the latest changes for the submodules

```
git submodule foreach git pull origin master
```

### Development

The project is designed to work with a Vagrant VM. All the tools have been tested in the VM as well as configured to be
similar to the deployment server

> You will need ansible on the host machin in order to provision the new vagrant VM
>
> If you are on OSX you can install with `homebrew`
> 
> ```
> brew install ansible
> ```

To get started for the first time you need to do an up with a provision.

```
vagrant up --provision
```

The provisioner is setup to use Ansible. The playbook and roles can be found in `config/ansible`.
After the first time upping the box, you no longer need to use the provision flag.

The system is setup to install all need gems on `provision` and each `up`.
The development `Puma` rails servers is designed to start on up as well and can be accessed at `http://localhost:3000`

The VM also has the `mailcatcher` gem configured to start on boot. This serves as the SMTP server for the box.
All `development` and `staging` email will be sent there.

> ##### Email
> You can access the local SMTP webserver at `http://localhost:8081`. Here you can check that the emails being sent by the server are correct. Attachments can also be visible from here as well.

### Deployment

We are using `capistrano` as the deployment tool for the rails application.

To deploy to the production server you can call, **within the VM**

```
cap production deploy
```

If you need to see a list of other capistrano tasks that are avaialble run `cap --tasks`


#### Staging Environment

The VM is designed to be the recipent of the `staging` environment, if you so choose to use it.
The `webapps` user is the target of the deployment 

> It is recommended to deploy to `staging`, before deploying to `production`

Run `cap staging deploy` to do the same deployment process. This will create the needed source directories and start the rails server.

The `staging` server is set to use a `Nginx` webserver with a unix socket to the puma rails server.
Nginx will only server SSL connections. 

> There is a local self signed certificate that will prompt a warning in most browsers

OSX should have ports `80` and `443` forwarded from the host to the indicated guest ports.

You can go to `https//localhost` or `https://lcoalhost:443` to access the staging server.

To view the logs and control the server, you will need to invoke `capistrano` tasks.


### Remote server configuration

If you need to updated the remote server (or the vagrant VM) you can use ansible to do so.

Ansible is installed in the Vagrant VM, but can be installe on the host if desired. You may want to install it on the host, if using SSH keys to access the remote.

#### Webserver

To execute the remote server configurations invoke (assumes shared SSH keys).

```
ansible-playbook config/ansible/webserver.yml --inventory config/ansible/hosts --user <ssh_user> --ask-sudo-pass
```

#### Rackspace DNS

To execute the remote server configurations invoke (assumes shared SSH keys).
There is a secrets.yml at `config/ansibe/roles/rackspace/vars/secrets.yml` that needs to contain a defined variable for `rackspace_api_key`. It is best to use `anisble-vault` to encrypt the secrets file before commiting to to the repo

```
ansible-playbook config/ansible/rackspace.yml --inventory config/ansible/hosts --user <ssh_user> --ask-sudo-pass --ask-vault-pass
```
root_dir = File.expand_path("../..", __FILE__)

workers 0
threads 0, 1

rackup      DefaultRackup
port        3000
environment ENV['RACK_ENV'] || 'development'

pidfile "#{root_dir}/tmp/pids/puma.pid"

state_path "#{root_dir}/tmp/pids/puma.state"

stdout_redirect "#{root_dir}/log/puma.stdout.log", "#{root_dir}/log/puma.stderr.log"

preload_app!

on_worker_boot do
  # Worker specific setup for Rails 4.1+
  # See: https://devcenter.heroku.com/articles/deploying-rails-applications-with-the-puma-web-server#on-worker-boot
  ActiveRecord::Base.establish_connection
end

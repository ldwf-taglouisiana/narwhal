FishTagger::Application.configure do
  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # enable the webconsole for the vagrant vm
  config.web_console.whitelisted_ips = '10.0.2.0/16'

  config.active_job.queue_adapter = :resque

  # Show full error reports and disable caching
  config.consider_all_requests_local = true
  config.action_controller.perform_caching = false

  # Don't care if the mailer can't send
  config.action_mailer.raise_delivery_errors = false

  # Print deprecation notices to the Rails logger
  config.active_support.deprecation = :log

  # Only use best-standards-support built into browsers
  config.action_dispatch.best_standards_support = :builtin

  # Do not compress assets
  config.assets.compress = false

  config.serve_static_files = true

  config.eager_load = false

  # Expands the lines which load the assets
  config.assets.debug = true

  # enable cross origin access for Angular
  config.middleware.insert_before 0, 'Rack::Cors' do
    allow do
      origins '*'
      resource '*', :headers => :any, :methods => [:get, :post, :options]
    end
  end

  #SMTP
  ActionMailer::Base.default :from => '"Tag Louisiana" <no-reply@taglouisiana.com>'
  ActionMailer::Base.default :to => 'nobody@yomama.net'

  config.action_mailer.default_url_options = {:protocol => 'http', :host => 'localhost:3000'}
  config.action_mailer.delivery_method = :smtp
  config.action_mailer.smtp_settings = { :address => "localhost", :port => 1025 }

  # imagemagick path
  Paperclip.options[:command_path] = "/usr/local/bin/"
end

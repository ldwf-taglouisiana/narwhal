FishTagger::Application.configure do
  # Code is not reloaded between requests
  config.cache_classes = true

  # set the active job adaper
  config.active_job.queue_adapter = :resque

  # Full error reports are disabled and caching is turned on
  config.consider_all_requests_local = false

  # enable caching
  config.action_controller.perform_caching = true

  # Disable Rails's static asset server (Apache or nginx will already do this)
  config.serve_static_files = false

  # Compress JavaScripts and CSS
  config.assets.compress = true

  # Don't fallback to assets pipeline if a precompiled asset is missed
  config.assets.compile = true

  # Generate digests for assets URLs
  config.assets.digest = true

  config.eager_load = true

  # See everything in the log (default is :info)
  config.log_level = :info

  # Enable locale fallbacks for I18n (makes lookups for any locale fall back to
  # the I18n.default_locale when a translation can not be found)
  config.i18n.fallbacks = true

  # Send deprecation notices to registered listeners
  config.active_support.deprecation = :notify

  # used for sending static files to Nginx
  config.action_dispatch.x_sendfile_header = 'X-Accel-Redirect'

  config.action_mailer.default_url_options = {:protocol => 'https', :host => 'narwhal.taglouisiana.com'}
  config.action_mailer.delivery_method = :mailgun
  config.action_mailer.mailgun_settings = {
      api_key: ENV['MAILGUN_API_KEY'],
      domain: 'taglouisiana.com'
  }

  # enable cross origin access for Angular
  config.middleware.insert_before 0, 'Rack::Cors' do
    allow do
      origins 'taglouisiana.com'
      resource '*', :headers => :any, :methods => [:get, :post, :options]
    end
  end

  ActionMailer::Base.default :from => '"Tag Louisiana" <no-reply@taglouisiana.com>'

  # imagemagick path
  Paperclip.options[:command_path] = "/usr/local/bin/"

  # exception mailer
  config.middleware.use ExceptionNotification::Rack,
                        :ignore_exceptions => ['ActionController::RoutingError'] + ExceptionNotifier.ignored_exceptions,
                        # :ignore_exceptions => ['ActionView::TemplateError'] + ExceptionNotifier.ignored_exceptions,
                        :email => {
                            :email_prefix => "[Narwhal Error] ",
                            :sender_address => %{"notifier" <notifier@taglouisiana.com>},
                            :exception_recipients => %w{errors@taglouisiana.com }
                        }
end

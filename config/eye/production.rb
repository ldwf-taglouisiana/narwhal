Eye.config do
  logger "/home/narwhal/apps/narwhal_production/shared/log/eye.log"
end

Eye.application("narwhal") do |app|
  rails_env = 'production'
  gemset    = 'ruby-2.3.0@narwhal'
  app_path  = '/home/narwhal/apps/narwhal_production'

  uid "narwhal" # run app as a user_name (optional) - available only on ruby >= 2.0
  gid "narwhal" # run app as a group_name (optional) - available only on ruby >= 2.0

  working_dir "#{app_path}/current"
  env 'RAILS_ENV' => rails_env # global env for each processes

  group "services" do

    process :resque_pool do
      pid_file "#{app_path}/shared/tmp/pids/resque-pool.pid"
      stdall "#{app_path}/shared/log/eye-resque-pool.log"
      start_command   "#{app_path}/rvm1scripts/rvm-auto.sh #{gemset} bundle exec resque-pool --daemon --environment #{rails_env}"

      stop_signals [:TERM, 5.seconds, :KILL]
      restart_command 'kill -HUP {PID}' # for rolling restarts

      monitor_children do
        stop_command 'kill -QUIT {PID}'
        check :cpu, every: 30, below: 80, times: 3
        check :memory, every: 30, below: 150.megabytes, times: [3, 5]
      end
    end

    process :puma do
      pid_file "#{app_path}/shared/tmp/pids/puma.pid"
      stdall "#{app_path}/shared/log/eye-puma.log"
      start_command   "/#{app_path}/rvm1scripts/rvm-auto.sh #{gemset} bundle exec puma -C #{app_path}/shared/puma.rb --daemon"

      stop_signals [:TERM, 5.seconds, :KILL]
      restart_command 'kill -USR2 {PID}'

      restart_grace 10.seconds

      check :cpu, every: 30, below: 80, times: 3
      check :memory, every: 30, below: 100.megabytes, times: [3, 5]
    end

    process :resque_scheduler do
      pid_file "#{app_path}/shared/tmp/pids/scheduler.pid"
      stdall "#{app_path}/shared/log/eye-resque-scheduler.log"
      start_command   "#{app_path}/rvm1scripts/rvm-auto.sh #{gemset} bundle exec rake RAILS_ENV=#{rails_env} PIDFILE=#{app_path}/shared/tmp/pids/scheduler.pid BACKGROUND=yes VERBOSE=1 MUTE=1 environment resque:scheduler "
      stop_command    "kill -s QUIT "
      restart_command "kill -s USR2 " # for rolling restarts
    end
  end
end
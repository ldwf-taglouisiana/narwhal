The `vars/secrets.yml` contains a `ansible vault` with the following variables

The Rackspace API key, if needed

```
rackspace_api_key
```

> If the vault is not unlocked the SSH keys will not be used, and may error out.

The SSH key for the vagrant box to log into the remote server, for capistrano deployments

```
vagrant_server_ssh_key
```

The SSH key for the remote server to pull the changes from the git repo.

```
server_gitlab_deploy_ssh_key
```
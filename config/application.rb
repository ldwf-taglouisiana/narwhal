require File.expand_path('../boot', __FILE__)

require 'rails/all'

Bundler.require(*Rails.groups)

module FishTagger
  class Application < Rails::Application
    config.autoload_paths += %W(
      #{config.root}/lib
      #{config.root}/app/models/views
    )

    config.middleware.insert_before ActionDispatch::ParamsParser, "CompressedRequests"

    config.active_record.raise_in_transactional_callbacks = true

    # Configure the default encoding used in templates for Ruby 1.9.
    config.encoding = "utf-8"

    # Configure sensitive parameters which will be filtered from the log file.
    config.filter_parameters += [:password, :raw_data]

    # Enable escaping HTML in JSON.
    config.active_support.escape_html_entities_in_json = true

    # Use SQL instead of Active Record's schema dumper when creating the database.
    # This is necessary if your schema can't be completely dumped by the schema dumper,
    # like if you have constraints or database-specific column types
    config.active_record.schema_format = :sql

    # Enable the asset pipeline
    config.assets.enabled = true

    # Version of your assets, change this if you want to expire all your assets
    config.assets.version = '2.0'

    config.lograge.enabled = true
    config.lograge.keep_original_rails_log = true
    config.lograge.logger = ActiveSupport::Logger.new "#{Rails.root}/log/lograge_#{Rails.env}.log"
    config.lograge.formatter = Lograge::Formatters::Logstash.new
    config.lograge.custom_options = lambda do |event|
      params = event.payload[:params].reject do |k|
        ['controller', 'action', 'format', 'utf8'].include? k
      end

      {
          remote_ip: event.payload[:remote_ip],
          user_id: event.payload[:user_id],
          device:  event.payload[:device],
          params: params
      }
    end

    config.to_prepare do
      Devise::SessionsController.layout "plain"
      Devise::RegistrationsController.layout proc{ |controller| user_signed_in? ? "application"   : "plain" }
      Devise::ConfirmationsController.layout "plain"
      Devise::UnlocksController.layout "plain"            
      Devise::PasswordsController.layout "plain"        
    end
  end
end

set :application, 'narwhal'
set :repo_url, 'git@gitlab.com:ldwf-taglouisiana/narwhal.git'

set :workers, { query: 1, email: 1, notifications: 1, refresh_view: 1, download: 1}

# need to set this so the puma plugin will work properly
set :rvm1_map_bins,   -> { %w{rake gem bundle ruby} }

set :use_sudo, false
set :bundle_bins, %w{rake rails}
set :bundle_binstubs, nil
set :bundle_jobs, 2
set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system', 'storage', 'addons')

# prevent pushign of the local database to the remote
set :disallow_pushing, true

# used for database tools
# if you prefer bzip2/unbzip2 instead of gzip
set :compressor, :bzip2

# keep only 5 backups on the remote
set :postgres_keep_local_dumps, 2 # Will keep 5 last dump files.
set :postgres_backup_compression_level, 6

### puma options
set :puma_workers, 0
set :puma_threads, [0, 1]


### eye config
set :eye_roles, -> { :app }
set :eye_executable, '/usr/local/rvm/wrappers/ruby-2.3.0@eye/eye'

# create a database backup on the remote before running a deploy, just in case a migration goes bad
# before 'deploy:starting', 'postgres:backup:create'

# before we restart the server, load the new eye config
before 'deploy:restart', 'eye:load'
# restart any services that we need
after 'deploy:finished', 'services:restart'
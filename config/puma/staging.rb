# config/puma.rb

# set path to application
app_dir = File.expand_path("../../..", __FILE__)
shared_dir = File.expand_path("../shared", app_dir)
rails_env = File.basename(__FILE__)

scheduler_pid = "#{shared_dir}/tmp/pids/scheduler.pid"

# Change to match your CPU core count
workers 1

# Min and Max threads per worker
threads 1, 6

# use the file name to get the environment
environment rails_env

# Set up socket location
bind "unix://#{shared_dir}/sockets/puma.sock"

# Logging
stdout_redirect "#{shared_dir}/log/puma.stdout.log", "#{shared_dir}/log/puma.stderr.log", true

# Set master PID and state locations
pidfile "#{shared_dir}/tmp/pids/puma.pid"
state_path "#{shared_dir}/tmp/pids/puma.state"
activate_control_app

on_worker_boot do
  ActiveRecord::Base.establish_connection

  if defined?(Resque)
    Resque.redis = ENV["REDISCLOUD_URL"] || "redis://127.0.0.1:6379/#{GlobalConstants::REDIS_DATABASE_NUMBER}"
  end

  Scheduler::start_unless_running scheduler_pid
end
# config/unicorn.rb
# Set environment to development unless something else is specified

DEFAULT_ENV = 'production'

env = ENV["RAILS_ENV"] || DEFAULT_ENV

USER_NAME = 'webapps'
USER_GROUP = 'webapps'

APP_NAME = 'narwhal'
APP_DIR = '/home/webapps/apps/narwhal_production/current/'

# See http://unicorn.bogomips.org/Unicorn/Configurator.html for complete
# documentation.
worker_processes 1

# listen on both a Unix domain socket and a TCP port,
# we use a shorter backlog for quicker failover when busy
#listen PORT # or "/tmp/#{ APP_NAME }.socket", :backlog => 64
listen "/tmp/#{ APP_NAME }.sock", :backlog => 1024 # SET ME!

# Preload our app for more speed
preload_app true

# nuke workers after 30 seconds instead of 60 seconds (the default)
timeout 120

pid APP_DIR + "/tmp/pids/unicorn.pid"

stderr_path APP_DIR + "/log/unicorn.stderr.log"
stdout_path APP_DIR + "/log/unicorn.stdout.log"

# Production specific settings
if env == "production"
  # Help ensure your application will always spawn in the symlinked
  # "current" directory that Capistrano sets up.
  working_directory APP_DIR

  # feel free to point this anywhere accessible on the filesystem
  user USER_NAME, USER_GROUP
end

before_fork do |server, worker|
  # the following is highly recomended for Rails + "preload_app true"
  # as there's no need for the master process to hold a connection
  if defined?(ActiveRecord::Base)
    ActiveRecord::Base.connection.disconnect!
  end

  # Before forking, kill the master process that belongs to the .oldbin PID.
  # This enables 0 downtime deploys.
  old_pid = APP_DIR + "/tmp/pids/unicorn.pid.oldbin"
  if File.exists?(old_pid) && server.pid != old_pid
    begin
      Process.kill("QUIT", File.read(old_pid).to_i)
    rescue Errno::ENOENT, Errno::ESRCH
      # someone else did our job for us
    end
  end
end

# load the scheduler init script
require '/home/webapps/apps/narwhal_production/current/lib/scheduler'

# path to the scheduler pid file
scheduler_pid_file = File.join(APP_DIR, "tmp/pids/scheduler.pid").to_s

after_fork do |server, worker|
  # the following is *required* for Rails + "preload_app true",
  if defined?(ActiveRecord::Base)
    ActiveRecord::Base.establish_connection
  end

  # run scheduler initialization
  Scheduler::start_unless_running scheduler_pid_file
  
  # if preload_app is true, then you may also want to check and
  # restart any other shared sockets/descriptors such as Memcached,
  # and Redis.  TokyoCabinet file handles are safe to reuse
  # between any number of forked children (assuming your kernel
  # correctly implements pread()/pwrite() system calls)
end
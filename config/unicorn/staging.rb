# config/unicorn.rb
# Set environment to development unless something else is specified

# set path to application
app_dir = File.expand_path("../../..", __FILE__)
shared_dir = File.expand_path("../shared", app_dir)
working_directory app_dir

# load the scheduler init script
require "#{app_dir}/lib/scheduler"

# Set unicorn options
worker_processes 1
preload_app true
timeout 120

# Set up socket location
listen "#{shared_dir}/sockets/unicorn.sock", :backlog => 64

# Logging
stderr_path "#{shared_dir}/log/unicorn.stderr.log"
stdout_path "#{shared_dir}/log/unicorn.stdout.log"

# Set master PID location
pid "#{shared_dir}/tmp/pids/unicorn.pid"
scheduler_pid = "#{shared_dir}/tmp/pids/scheduler.pid"

before_fork do |server, worker|
  Signal.trap 'TERM' do
    puts 'Unicorn master intercepting TERM and sending myself QUIT instead'
    Process.kill 'QUIT', Process.pid
  end

  # establish a disconenct the conncetion to the database, for the master process
  defined?(ActiveRecord::Base) and
      ActiveRecord::Base.connection.disconnect!
end

after_fork do |server, worker|
  Signal.trap 'TERM' do
    puts 'Unicorn worker intercepting TERM and doing nothing. Wait for master to send QUIT'
  end

  # establish a connection to the database
  defined?(ActiveRecord::Base) and
      ActiveRecord::Base.establish_connection

  # run scheduler initialization
  Scheduler::start_unless_running scheduler_pid
end

# Force the bundler gemfile environment variable to
# reference the capistrano "current" symlink
before_exec do |_|
  ENV['BUNDLE_GEMFILE'] = File.join(app_dir, 'Gemfile')
end
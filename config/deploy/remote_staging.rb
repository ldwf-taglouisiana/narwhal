set :stage, :remote_staging

set :host_ip, 'taglouisiana.com'
set :port, 22
set :user, 'webapps'
set :deploy_via, :remote_cache
set :use_sudo, false

ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }.call

set :rails_env, :remote_staging
set :conditionally_migrate, true

set :rvm1_ruby_version, 'ruby-2.2.3@moby'

role :resque_worker, fetch(:host_ip)
role :resque_scheduler, fetch(:host_ip)

set :keep_releases, 2

# configure the eye components
set :eye_env, -> { {rails_env: fetch(:rails_env)} }
set :eye_config, -> { "#{current_path}/config/eye/#{fetch :rails_env}.rb" }

server fetch(:host_ip),
       roles: [:web, :app, :db],
       port: fetch(:port),
       user: fetch(:user),
       primary: true

set :deploy_to, "/home/#{fetch(:user)}/apps/#{fetch(:application)}_#{fetch(:rails_env)}"

set :ssh_options, {
                    forward_agent: true,
                    auth_methods: %w(publickey),
                    user: 'webapps',
                }
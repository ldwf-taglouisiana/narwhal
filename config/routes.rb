FishTagger::Application.routes.draw do
  # GEM FILE ROUTES
  apipie

  use_doorkeeper

  devise_for :users, :controllers => { registrations: :registrations }

  devise_scope :users do
    get '/users/sign_out' => 'devise/sessions#destroy'
  end

  # APPLICATION ROUTES

  scope '/admin' do
    resources :users do
      get :autocomplete_angler_angler_id, :on => :collection
      post :generate_new_password_email
    end

    resque_web_constraint = lambda do |request|
      current_user = request.env['warden'].user
      current_user.present? && current_user.has_role?(:admin)
    end

    constraints resque_web_constraint do
      mount ResqueWeb::Engine => "/resque_web"
      # mount RailsAdmin::Engine => '/rails'
    end

  end

  resources :angler_items do
    collection do
      post 'mark_fulfilled', action: :mark_fulfilled
    end
  end

  resources :about_tagging_programs

  resources :anglers do
    member do
      get 'data_count', action: :get_data_count
    end

    collection do
      get :autocomplete_angler_angler_id
      get 'search', action: :search
      get :download, defaults: { format: 'json' }

      scope :duplicates, as: :duplicates do
        get '/', action: :find_duplicates
        get :with_name, action: :duplicate_anglers
        get :with_ids, action: :duplicate_anglers
      end

      scope :merge, as: :merge do
        get '/', action: :merge_anglers
        get :canonical_angler, action: :canonical_angler
        post :perform, action: :perform_merge
      end

      post 'change_angler_id', action: :change_angler_id
      post 'transfer_data', action: :transfer_data
    end
  end

  resources :announcements

  controller :basin_sub_basin, as: 'basin_sub_basin',  path: 'basin_sub_basin' do
    get 'get_sub_basins', action: :get_sub_basins
    get 'basins', action: :basins
  end

  resources :blurbs

  resources :captures do
    collection do
      get :search
      get :import
      get :autocomplete_angler_angler_id
      get :autocomplete_tag_tag_no
      get :download, defaults: { format: 'json' }

      post :verify
      post :import, action: :import_commit
    end

    member do
      post :verify
      get :report
      get :partials
      get :convert_to_other
      get :geo_json, defaults: { format: 'json' }
    end
  end

  controller :docs, as: :docs, path: 'docs' do
    get '/', to: redirect( '/docs/index.html' )
    get '*id', action: :show
  end

  resources :draft_captures do
    member do
      get 'status', action: :get_status
      get 'move_to_capture', action: :move_to_capture
      get :geo_json, defaults: { format: 'json' }
    end

    collection do
      get :download, defaults: { format: 'json' }
      post :move_to_captures
      delete :multi_delete
      post :refresh_errors
    end
  end
  
  controller :search do
      get 'get_search_data', :action => :get_search_data
  end

  resources :scratch_captures do
    member do
      post :convert_to_recapture, action: :convert_to_recapture
      get :geo_json, defaults: { format: 'json' }
    end
  end

  controller :fish_entry_photos, as: :fish_entry_photos, path: :fish_entry_photos do
    get '/image/:id/:style/*filename', action: :image
    delete '/image/:id/:style/*filename', action: :destroy
  end

  resources :front_page_photo_stream_items

  resources :how_to_tags

  resources :news_items

  resources :notifications do
    collection do
      delete :destroy_all, action: :destroy_all
    end
  end

  resources :photos do
    member do
      get ':image_name', action: :show
    end
  end

  resources :recaptures do
    collection do
      get :download, defaults: { format: 'json' }
      post :verify
    end

    member do
      post :verify
      get :report
      get :partials
      get :convert_to_other
      get :geo_json, defaults: { format: 'json' }
    end
  end

  controller :report, as: :report, path: 'reports' do
    get '/', action: :reports
    match :angler_info, via: [:get, :post]
    match :tagged_report, via: [:get, :post]
    match :days_at_large, via: [:get, :post]
    match :random_event, via: [:get, :post]
    match :data_check, via: [:get, :post]
    match :saltwater, via: [:get, :post]
    match :custom, via: [:get, :post]

    scope :sql, as: :sql do
      match :dump, action: :sql_dump, via: [:get, :post]
      match :csv, action: :sql_dump_csv, via: [:get, :post]
    end

    scope :angler, as: :angler do
      match :captures, action: :angler_captures, via: [:get, :post]
      match :recaptures, action: :angler_recaptures, via: [:get, :post]
      match :detail, action: :angler_events, via: [:get, :post]
      match :unused_kits, action: :angler_unused_kits, via: [:get, :post]
      match :banquet, action: :banquet_anglers, via: [:get, :post]
    end
  end

  controller :request_downloads, as: 'request_download', path: 'request_download' do
    get 'get', action: 'get'
    get 'file_is_ready', action: 'get_file_is_ready', defaults: { format: 'json' }
    post 'should_email', action: 'post_should_email', defaults: { format: 'json' }
  end

  controller :search, as: :search, path: :search do
    get '/', action: :index
  end

  resources :species do
    member do
      post :update_published
      post :update_target
    end

    collection do
      get '/image/:id/:image_name', action: :image
      post :sort
    end
  end

  resources :species_lengths
  
  scope '/stats', controller: :stats, as: :stats, defaults: { format: 'json' } do
    get '/', action: :index, defaults: { format: 'html' }
    get '/active_anglers', action: :active_anglers
    get '/total_anglers', action: :total_anglers
    get '/total_captures', action: :total_captures
    get '/total_recaptures', action: :total_recaptures
    get '/fish_event_points', action: :fish_event_points
    get '/fish_event_basins', action: :fish_event_basins
    get '/fish_event_heatmap', action: :fish_event_heatmap
    get '/species_counts', action: :species_counts
  end

  resources :tags, except: [:show, :new, :edit, :update, :create, :destroy] do
    get :autocomplete_angler_angler_id, :on => :collection
    get :autocomplete_tag_tag_no, :on => :collection

    collection do
      get :autocomplete_tag_number
      get :autocomplete_tag_number_any

      get '/search', action: :search
      get '/assign', action: :assign
      get '/unassign', action: :unassign
      get '/assigned', action: :assigned
      get '/deactivate', action: :deactivate

      post '/deactivate', action: :commit_deactivate
      post '/assign', action: :commit_assignment
      post '/unassign', action: :commit_unassignment
    end
  end

  resources :tag_lots do
    collection do
      get 'merge', action: :merge_tag_lots
    end
  end

  resources :user_tag_requests do
    member do
      post :update_fulfilled
    end
  end

  resources :volunteer_time_logs do
    member do
      get 'captures', action: :captures
    end
  end

  root to: 'welcome#index'
end

FishTagger::Application.routes.append do
  namespace :api, defaults: { format: 'json' } do

    # DEPRECATION on October 1, 2015
    namespace :v1 do
      resources :tokens, :only => [:create, :destroy]

      get '/me' => 'protected_api#me'

      # public api actions
      get '/combined_data' => 'public_api#get_combined_data'
      post '/combined_data' => 'public_api#get_combined_data'
      get '/species' => 'public_api#get_species'
      get '/species_lengths' => 'public_api#get_species_lengths'
      get '/about_tagging_program' => 'public_api#get_about_tagging_program'
      get '/how_to_tag' => 'public_api#get_how_to_tag'
      get '/time_of_day_options' => 'public_api#get_time_of_day_options'
      get '/fish_condition_options' => 'public_api#get_fish_condition_options'
      get '/recapture_disposition_options' => 'public_api#get_recapture_disposition_options'
      get '/shirt_size_options' => 'public_api#get_shirt_size_options'
      get '/blurbs' => 'public_api#get_blurbs'
      post '/add_unregistered_capture' => 'public_api#post_non_registered_capture'
      post '/create_unregistered_angler' => 'public_api#create_anon_cap_angler'
      get '/tag_history' => 'public_api#get_tag_history'
      get '/check_for_duplicate_captures' => 'public_api#check_for_duplicate_caps'
      get '/check_tag_exists' => 'public_api#check_tag_exists'
      get '/public_check_tag_exists' => 'public_api#public_check_tag_exists'
      get '/ajaxEmailAlreadyExists' => 'public_api#check_if_email_exists'
      post '/register' => 'public_api#create' #this is not behind authentication
      post '/reset_password' => 'public_api#reset_user_password'
      get '/capture_map_data' => 'public_api#capture_map_data'
      get '/news' => 'public_api#get_news_feed'
      get '/photo_stream' => 'public_api#get_photo_stream'
      get '/photos/:id/:image_name' => 'public_api#get_photos'
      get '/public_dashboard' => 'public_api#get_public_dashboard'
      get '/new_draft_capture' => 'public_api#get_new_draft_capture'
      get '/announcements' => 'public_api#get_announcements'

      # protected api actions
      get '/check_tag_exists_for_angler' => 'protected_api#check_tag_exists'
      get '/tags' => 'protected_api#get_tags'
      post '/tag_request' => 'protected_api#post_tag_request'
      get '/captures' => 'protected_api#get_captures'
      post '/captures' => 'protected_api#post_captures'
      post '/change_draft_capture_should_save' => 'protected_api#change_draft_capture_should_save'
      get '/draft_capture' => 'protected_api#get_draft_capture'
      get '/draft_captures' => 'protected_api#get_draft_captures'
      get '/draft_capture_status' => 'protected_api#get_status_for_draft'
      post '/draft_captures' => 'protected_api#post_draft_captures'
      post '/update_draft_capture' => 'protected_api#update_draft_capture'
      post '/volunteer_hours' => 'protected_api#post_volunteer_hours'
      get '/captures_dashboard' => 'protected_api#get_dashboard'
      post '/get_angler_capture_report' => 'protected_api#get_angler_capture_report'
      post '/update_me' => 'protected_api#update_me'
      post '/ios_device_registration' => 'protected_api#post_ios_device_registration'
    end

    namespace :v2 do

      namespace :public do

        controller :info, as: :info, path: :info do
          get :about
          get :how_to
          get :blurbs
          get :news
          get :dashboard
          get :announcements
        end

        controller :options, as: :options, path: :options do
          match :combined, via: [:get, :post]
          get :species
          get :species_lengths
          get :time_of_days
          get :fish_conditions
          get :dispositions
          get :shirt_sizes
        end

        controller :fish_event, as: :fish_event, path: :fish_event do
          get :draft_capture
          get :map_data
          post :recapture
        end

        controller :photos, as: :photos, path: :photos do
          get :stream
          get '/:id/:image_name', action: :send_photo
        end

        controller :validate, as: :validate, path: :validate do
          get :new_capture
          get :tag_exists
          get :email_exists
        end

        controller :account, as: :account, path: :account do
          post :register
          post :reset_password
        end
      end

      namespace :protected do

        controller :combined_data, as: :combined_data, path: :combined_data do
          get '/', action: :index
          post '/', action: :create
        end

        controller :volunteer_time_logs, as: :volunteer_time_logs, path: :volunteer_time_logs do
          get '/', action: :index
          post '/', action: :create
        end

        controller :tags, as: :tags, path: :tags do
          get '/',         action: :index
          post :request
          get '/request',  action: :get_requests
        end

        controller :captures, as: :captures, path: :captures do
          get '/', action: :index
          get '/:id/history', action: :history
        end

        controller :draft_captures, as: :draft_captures, path: :draft_captures do
          get '/',                  action: :index
          post '/',                 action: :create
          get :status
          post :toggle_saved_at
        end

        controller :fish_entry_photos, as: :fish_entry_photos, path: :fish_entry_photos do
          get '/:id/*filename',  action: :show, defaults: { format: nil }
        end

        controller :account, as: :account, path: :account do
          get :me
          post '/me',       action: :update_me
          get :dashboard
        end

        controller :validate, as: :validate, path: :validate do
          get :tag_exists
        end

        controller :mobile, as: :mobile, path: :mobile do
          scope :notifications, as: :notifications, path: :notifications do
            scope :registrations, as: :registrations, path: :registrations do
              post '/ios', action: :ios_device_registration
            end
          end
        end
      end

      controller :internal_api, as: 'internal', path: 'internal' do
        get :angler_from_name
        get :angler_from_angler_id
        get :angler_from_tag_number
        get :validate_tag
        get :validate_angler_id_available
      end

      controller :ldwf_api, as: 'ldwf', path: 'ldwf' do
        get :captures
        get :species
        get 'species/:id', action: :species_id
        get :times
        get 'times/:id', action: :times_id
        get :basins
        get 'basins/:id', action: :basins_id
        get :subbasins
        get 'subbasins/:id', action: :subbasins_id
      end
    end
  end
end

# Load the rails application
require File.expand_path('../application', __FILE__)

# Initialize the rails application
FishTagger::Application.initialize!

#This is here so we only have to compute the list of app id + secret hashes once
ids_and_stuff = ActiveRecord::Base.connection().execute("select * from oauth_applications")
hashed_auth_stuff = []
ids_and_stuff.each do |auth|
  sha256 = OpenSSL::Digest::SHA256.new
  sha256 << auth['uid']
  sha256 << auth['secret']

  hashed_auth_stuff << sha256.to_s
end
#This set of hashes is stored in this variable for everyone to access anywhere they need it
APPLICATION_HASHES = hashed_auth_stuff

NativeDbTypesOverride.configure({
                                    postgres: {
                                        datetime: { name: "timestamptz" },
                                        timestamp: { name: "timestamptz" }
                                    }
                                })
class DateTimeValidator < Apipie::Validator::BaseValidator

  DATE_REGEX = /\A(\d{4})-(\d{2})-(\d{2})T(\d{2})\:(\d{2})\:(\d{2})Z\Z/
  INTEGER_REGEX = /\A\d+\z/

  def initialize(param_description, argument)
    super(param_description)
    @type = argument
  end

  def validate(value)
    return false if value.nil?

    if value.is_a?(Numeric) || (value.is_a?(String) && value =~ INTEGER_REGEX)
      value.to_i > 0

    elsif value.is_a?(String)
      value =~ DATE_REGEX

    else
      false

    end
  end

  def self.build(param_description, argument, options, block)
    if argument == DateTime || argument == Time
      self.new(param_description, argument)
    end
  end

  def description
    "Must be Epoch value or match the regex #{DATE_REGEX.to_s}."
  end

  def process_value(value)
    if value.is_a?(Numeric) || (value.is_a?(String) && value =~ INTEGER_REGEX)
      Time.at(value.to_i)

    elsif value.is_a?(String)
      Chronic.parse(value)

    else
      nil

    end
  end
end

class ArrayValidator < Apipie::Validator::ArrayValidator
  def process_value(value)
    value ? value.split(',') : []
  end
end


rails_root = ENV['RAILS_ROOT'] ||  File.expand_path("../../../", __FILE__)
rails_env = ENV['RAILS_ENV'] || 'development'
resque_config = YAML.load_file(rails_root + '/config/redis.yml')
app_name = Rails.application.class.parent_name
redis = Redis.new(host: resque_config[Rails.env], driver: :hiredis)
namespace = "#{app_name}_#{Rails.env}".downcase

$redis_store =  Redis::Namespace.new(namespace, redis: redis)

Resque.redis = $redis_store

if Rails.env.development?
  $redis_store.set GlobalConstants::REDIS_CAPTURE_UPDATING_KEY, 0
  $redis_store.set GlobalConstants::REDIS_RECAPTURE_UPDATING_KEY, 0
  $redis_store.set GlobalConstants::REDIS_VOLUNTEER_HOURS_UPDATING_KEY, 0
  $redis_store.set GlobalConstants::REDIS_TAG_UPDATING_KEY, 0
end

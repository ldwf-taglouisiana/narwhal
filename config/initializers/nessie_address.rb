config = YAML.load_file(File.join(Rails.root, 'config', 'nessie_address.yml'))[Rails.env]

NESSIE_HOST = config['host']

nessie_application = Doorkeeper::Application.where(name: config['oauth_application_name']).first
sha256 = OpenSSL::Digest::SHA256.new
sha256 << nessie_application.try(:uid)
sha256 << nessie_application.try(:secret)

NESSIE_OAUTH_HASH = sha256.to_s
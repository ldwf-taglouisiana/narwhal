Rails.application.config.assets.paths += ["#{Rails.root}/app/assets/fonts"]
Rails.application.config.assets.precompile += %w(plain.css New_Capture_Spreadsheet.xlsx)
Rails.application.config.assets.precompile += %w(*.js *.ico *.png *.jpg)
Rails.application.config.assets.precompile += %w(application-report.js)
Rails.application.config.assets.precompile += %w(application-report.scss application-report-print.scss)
Apipie.configure do |config|
  config.app_name                = "Narwhal"
  config.api_base_url            = "/api"
  config.doc_base_url            = "/apipie"
  config.process_params          = true
  config.default_version         = "2.0"
  # where is your API defined?
  config.api_controllers_matcher = "#{Rails.root}/app/controllers/api/**/*.rb"
end

module GlobalConstants
  REDIS_DATABASE_NUMBER = Rails.env.production? ? 1 : 2
  REDIS_TAG_UPDATING_KEY = "tags_updateing_key_counter"
  REDIS_CAPTURE_UPDATING_KEY = "capture_updateing_key_counter"
  REDIS_RECAPTURE_UPDATING_KEY = "recapture_updateing_key_counter"
  REDIS_VOLUNTEER_HOURS_UPDATING_KEY = "volunteer_updateing_key_counter"
end
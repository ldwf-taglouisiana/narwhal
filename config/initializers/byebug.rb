if Rails.env.development?
  require 'byebug/core'

  begin
    Byebug.start_server 'localhost', ENV.fetch("BYEBUG_SERVER_PORT", 1048).to_i
  rescue Errno::EADDRINUSE => ex
    puts "Byebug Server already bound to port #{ENV.fetch("BYEBUG_SERVER_PORT", 1048)}".red
  end

end
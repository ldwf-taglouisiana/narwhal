

class Hash

  def recursive_hash_fix!
    self.recursive_symbolize_keys!
    self.recursive_array_fix!
  end


  def recursive_symbolize_keys!
    self.symbolize_keys!

    self.values.select{ |v| v.is_a?(String) }.each do |h|
      h.strip!
      self.delete(self.key(h)) if h.empty?
    end

    self.values.select{ |v| v.is_a?(Hash) }.each{ |h| h.recursive_symbolize_keys!}

    self.values.select{ |v| v.is_a?(Array) }.flatten.each do |h|
      h.recursive_symbolize_keys! if h.is_a?(Hash)
    end


    self
  end

  def recursive_array_fix!
    self.values.each do |value|
      if value.is_a?(Hash)
        value.recursive_array_fix!
      elsif value.is_a?(Array)
        rec_help(value)
      end
    end


  end

  def rec_help(array)
    array.each do |inner|
      if inner.empty?
        array.delete(inner)
      elsif inner.is_a?(Array)
        rec_help(inner)
      else
        inner.recursive_array_fix!
      end
    end
  end
end
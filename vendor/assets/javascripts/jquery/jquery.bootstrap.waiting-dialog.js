/**
 * Module for displaying "Waiting for..." dialog using Bootstrap
 *
 * http://bootsnipp.com/snippets/featured/quotwaiting-forquot-modal-dialog
 * @author Eugene Maslovich <ehpc@em42.ru>
 *
 *     Modified by Daniel Ward <dwa012@gmail.com>
 *     Will take a file token, so if can show the send email checkbox
 */

var downloadWaitingDialog = (function ($) {

    // Creating modal dialog's DOM
    var $dialog = $(
        '<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
        '<div class="modal-dialog">' +
        '<div class="modal-content">' +
        '<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><h3 style="margin:0;"></h3></div>' +
        '<div class="modal-body">' +
        '<div class="progress progress-striped active" style="margin-bottom:0;"><div class="progress-bar" style="width: 100%"></div></div>' +
        '<div class="checkbox" id="email_status" style="display: none;"><label><input type="checkbox"> Email when ready?</label></div>' +
        '<span class="label label-success" style="display: none;" id="email_message_span">Will send email when ready. You can close this page if you wish.</span>' +
        '</div>' +
        '</div></div></div>');

    var $settings;

    return {
        /**
         * Opens our dialog
         * @param message Custom message
         * @param options Custom options:
         * 				  options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
         * 				  options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
         */
        show: function (message, options) {
            // Assigning defaults
            var settings = $.extend({
                //dialogSize: 'm',
                progressType: '',
                onModalClose: function(){},
                token: ''
            }, options);
            if (typeof message === 'undefined') {
                message = 'Loading';
            }
            if (typeof options === 'undefined') {
                options = {};
            }
            // Configuring dialog
            $dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
            $dialog.find('.progress-bar').attr('class', 'progress-bar');
            if (settings.progressType) {
                $dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
            }
            $dialog.find('h3').text(message);

            $dialog.off('hidden.bs.modal');

            $dialog.on('hidden.bs.modal', function() {
                settings.onModalClose();
            });

            // if a token was porvided
            if (settings.token != '') {
                // reset the checkbox
                $dialog.find('#email_status input').removeAttr('checked');

                // show the checkbox to the user
                $dialog.find('#email_status').show();

                // reset the message span as well
                $dialog.find('#email_message_span').hide();

                // when the checkbox state is changed
                $dialog.find('#email_status input').change(function(){
                    // send the post request to indicate the user wants the file link emailed to them.
                    $.ajax({
                        type: 'POST',
                        url: '/request_download/should_email',
                        dataType: 'json',
                        data: {
                            send_email: true,
                            token: settings.token
                        }
                    }).done(function() {
                        // when the change is made then hide the checkbox and show the confirmation message
                        $dialog.find('#email_status').hide();
                        $dialog.find('#email_message_span').show();
                    }).fail(function() {
                        // if the update failed then reset the checkbox
                        $dialog.find('#email_status input').removeAttr('checked');
                    });
                });
            }

            // Opening dialog
            $dialog.modal();
        },
        /**
         * Closes dialog
         */
        hide: function () {
            $dialog.modal('hide');
        }
    }

})(jQuery);
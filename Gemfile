source 'https://rubygems.org'

gem 'rails', '4.2.5'

# database
gem 'pg'
gem 'activerecord-native_db_types_override'

# authentication
gem 'devise'
gem 'cancan'
gem 'rolify'
gem 'doorkeeper', '~> 1.4.0'

# utils
gem 'chronic', github: 'mojombo/chronic', branch: 'master' # date time parsing
gem 'mailgun_rails' # mail api interface
gem 'jbuilder' # creating json objects
gem 'yajl-ruby', require: 'yajl' # faster json building
gem 'colorize' # adds colorizing print options to strings
gem 'rack-cors'

# front end utils
gem 'jquery-rails' # jquery
gem 'jquery-ui-rails' # the UJS components for rails
gem 'jquery-fileupload-rails' # enables async file uploads
gem 'wicked_pdf' # wicked pdf
gem 'wkhtmltopdf-binary-edge', '~> 0.12.3.0'
gem 'unobtrusive_flash', git: 'https://github.com/dwa012/unobtrusive_flash.git', branch: 'dwa012-patch2'

#gem 'wkhtmltopdf-binary', git: 'https://github.com/dwa012/wkhtmltopdf-binary.git' # the binary for wicked pdf
gem 'googlestaticmap' # creates static maps
gem 'font-awesome-rails' # fontawesome
gem 'kaminari' # pagination
gem 'therubyracer', :platforms => :ruby # JS runtime
gem 'rails-jquery-autocomplete', github: 'dwa012/rails-jquery-autocomplete', branch: 'dwa012-patch-1' # allows for autocompletion of fields
gem 'numbers_and_words' #
gem 'haml-rails', '~> 0.9'
gem 'turbolinks', github: 'rails/turbolinks'
gem 'nprogress-rails'
gem 'js-routes'

# spreadsheets
gem 'roo', '~> 2.1.0' # , '< 2.0.0' # reading spreadsheets
gem 'spreadsheet' # exporting spreadsheets
gem 'axlsx', '2.1.0.pre' #github: 'randym/axlsx' #, '~> 2.0' # creates spreadsheets
gem 'axlsx_rails' #adds xlsx templates to views

# active record
gem 'annotate' # annotates model related classes with schema info
gem 'acts_as_list' # allows a set of models to be orderable like a list
gem 'paperclip' # allows for file attachments
gem 'geocoder' # converts an address to a geocoded entity
gem 'area' # geocoding TODO may need to remove
gem 'custom_error_message' # create better valdiation messages
gem 'textacular' #allows for full text searching in the db
gem 'strip_attributes' #Removes leading and trailing white spaces and converts empty entries to nil
gem 'validates_overlap' # time overlap
gem 'paper_trail', '~> 4.0.0' # audit loggin for model changes
gem 'scenic' # used to better manage SQL views
gem 'pg_search' # better text searching with postgres
gem 'hairtrigger' # allows for trigger definitions to live in the models

# backend
gem 'exception_notification', git: 'https://github.com/smartinez87/exception_notification.git' # notifies when there are exceptions raised
gem 'websocket-rails' # the websocket engine
gem 'rufus-scheduler' # CRON scheduler
gem 'houston' # Push notificaitons for Mobile devices
gem 'resque' # for queuing jobs
gem 'resque-pool' # for faster reque upstarts
gem 'resque-web', require: 'resque_web' # web interface for managing jobs
gem 'apipie-rails', '~> 0.3.0' # documentation and validation for API routes
gem 'lograge' # use a different logging tool
gem 'logstash-event' # the logstash JSON formatter helper
gem 'device_detector' # user agent parser

gem 'oink'

gem 'hiredis', '~> 0.6.0'
gem 'redis', '>= 3.2.0', :require => %w(redis redis/connection/hiredis)
gem 'active_scheduler'
gem 'resque-scheduler-web'

# assets
gem 'sass-rails'
gem 'coffee-rails'
gem 'uglifier', '>= 1.0.3'

gem 'puma'
gem 'figaro'

group :development do
  gem 'byebug'
  gem 'spring'
  gem 'web-console', '~> 2.0'
  gem 'yard'
end

group :development, :test do
  gem 'rspec-rails'
  gem 'factory_girl_rails'
end

group :development, :deploy do
  gem 'capistrano' # used to help automate deployments
  gem 'capistrano-rails-console'
  gem 'capistrano-rails' # rails specific stuff
  gem 'rvm1-capistrano3', require: false # adds RVM to capistrano
  gem 'capistrano3-unicorn' # allows us to start and stop the unicorn process
  gem 'capistrano-resque', require: false # can control resque
  gem 'capistrano3-puma'
  gem 'capistrano3-postgres', require: false
  gem 'capistrano-db-tasks', require: false
  gem 'capistrano-resque-pool', require: false
  gem 'capistrano-eye', git: 'https://github.com/dwa012/capistrano-eye.git', branch: 'dwa012-patch-1'
  gem 'capistrano-figaro-yml'
  gem 'capistrano-scm-copy', git: 'https://github.com/dwa012/capistrano-scm-copy.git', branch: 'patch-1'
end

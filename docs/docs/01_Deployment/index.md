### The web application is written with Ruby on Rails

You will need a rails enviroment to get the application ready to run.

We suggest using something like [RVM](https://rvm.io/)

	\curl -sSL https://get.rvm.io | bash -s stable

RVM works best on **UNIX** based systems such as Linux and OSX. If you want to use **Windows**, you may have to use [this](http://blog.developwithpassion.com/2012/03/30/installing-rvm-with-cygwin-on-windows/) guide to get started.

	rvm install [RUBY VERSION]

It is best to use gemsets to keep projects seperated. Navigate to the project directory and run the following.

	rvm use [RUBY VERSION]@telemetry --create --ruby-version

Install the gems needed for the app to run.

	bundle install
	
Start the server locally with the following

	rails s


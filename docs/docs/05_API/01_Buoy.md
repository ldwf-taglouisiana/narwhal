###GET


Get the buoys list

**Params**

<table>
<tr class="tableizer-firstrow"><th>Name</th><th>Type</th><th>Description</th></tr>
 <tr><td>id </td><td>int</td><td>The ID</td></tr>
 <tr><td>active </td><td>boolean</td><td>Is this buoy active</td></tr>
 <tr><td>anchorline </td><td>integer</td><td>The weight of the anchor line</td></tr>
 <tr><td>attachment </td><td>string</td><td>What the buoy is attached to</td></tr>
 <tr><td>bottom_type </td><td>string</td><td>Describes what the kind of bottom the anchor is in</td></tr>
 <tr><td>buoy_id </td><td>integer</td><td>The ID of the Buoy</td></tr>
 <tr><td>depth </td><td>integer</td><td>How deep the buoy is</td></tr>
 <tr><td>downline </td><td>integer</td><td>????</td></tr>
 <tr><td>notes </td><td>string</td><td>Any notes about the buoy</td></tr>
 <tr><td>site_id </td><td>integer</td><td>The site ID that the buoy is attached to</td></tr>
 <tr><td>updated_at</td><td>date string</td><td>The last time this buoy was updated, in ISO8661 format</td></tr>
</table>

**Response**


	{
		errors: [ ],
		count: 113,
		version: "2014-02-17T22:57:31Z",
		items: [
			{
				active: null,
				anchorline: 1,
				attachment: "1",
				bottom_type: "1",
				buoy_id: "Z1",
				depth: "1.0",
				downline: 1,
				id: 678,
				notes: null,
				site_id: 571,
				updated_at: "2014-02-14T09:38:54.195-06:00"
			}
		]
	}

###POST

**Params**

<table>
<tr class="tableizer-firstrow"><th>Name</th><th>Type</th><th>Description</th></tr>
 <tr><td>active </td><td>boolean</td><td>Is this buoy active</td></tr>
 <tr><td>anchorline </td><td>integer</td><td>The weight of the anchor line</td></tr>
 <tr><td>attachment </td><td>string</td><td>What the buoy is attached to</td></tr>
 <tr><td>bottom_type </td><td>string</td><td>Describes what the kind of bottom the anchor is in</td></tr>
 <tr><td>buoy_id </td><td>integer</td><td>The ID of the Buoy</td></tr>
 <tr><td>depth </td><td>integer</td><td>How deep the buoy is</td></tr>
 <tr><td>downline </td><td>integer</td><td>????</td></tr>
 <tr><td>notes </td><td>string</td><td>Any notes about the buoy</td></tr>
 <tr><td>site_id </td><td>integer</td><td>The site ID that the buoy is attached to</td></tr>
</table>

**Response**

	{ 
		"errors": [],
		"messages": []
	}

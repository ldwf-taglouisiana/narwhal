## Purpose

* To store detection information from a set of radio recievers spread across Lake Ponchatrain

## Documentaion

* Contains the basic setup to redeploy the server if necessary.
* Basic API documentation as needed.
* Location of relative config files to make changes to the server.


###Git Repo:
The main source repo is housed at UNO in the Computer Science department.

You can find it [here](https://gitlab.cs.uno.edu/telemetry/telemetry-rails). You may need to contact [Daniel Ward](mailto:drward3@uno.edu) for access.
The server that **Telemetry** is running on is housed at [www.louisianafisheries.net](https://www.louisianafisheries.net/telemetry) and is hosted on Rackspace.

The webserver is an [nginx](http://nginx.org/) server. Nginx handles the SSL connection information.

**SSL**

The SSL certificate and the doamin name is managed through [GoDaddy](https://www.godaddy.com/). 

The SSL key and certificate are here

	/etc/ssl/private/
	
**Rails Web Engine Socket**

The rails app communicates with the webserver through a UNIX socket via the [Unicorn](https://github.com/defunkt/unicorn) gem.

Most of the instructions are based of the this [Gist](https://gist.github.com/killercup/2049606).

The primary Unicorn deamon script is here:

	/etc/init.d/unicorn
	
It reads config files for each rails server from:

	/etc/unicorn/
	
You can change the **telemetry** config by editing:

	/etc/unicorn/telemetry.conf
	
Example:

	RAILS_ROOT=/home/webapps/telemetry/
	RAILS_ENV=production
	UNICORN=/home/webapps/.rvm/wrappers/ruby-2.1.3@telemetry/unicorn_rails

* The app root is the project location
* The Rails Env is set here
* The location of the Unicorn executable

Unicorn needs to have a wrapper binary when using gemsets.
	
	rvm wrapper [RUBY VERSION]@telemetry telemetry unicorn_rails
	
Restart unicorn to get the Rails app started.

	service start telemetry
	
**Nginx**

The config files for Nginx are in 

	/etc/nginx/

The server is setup up with multiple virtual hosts that are addressed at subURIs.

There is one **server** block in

	/etc/nginxsites-available/default 

Each rails app has a Unix socjet connection defined with the server block.

Telemetry's socket is defined as:

	# the main rails app 
	upstream telemetry_server {
    	server unix:/tmp/telemetry.sock fail_timeout=0;
	}

	# The websocket that runs in parrallel to the main server
	upstream telemetry_websocket {
    	server localhost:3245;
	}
	
To accomadate the subURI configurations, each Rails app configuration is in:

	/etc/nginx/locations-available/

	
Telemetry is configured in:

	/etc/nginx/locations-available/telemetry
	
**Example**

	location ~ ^/telemetry(/.*|$) {
		rewrite /telemetry(.*) $1 break;
	
		# websocket route for the map information
		location ~ /websocket {
			# switch off logging
	        	access_log off;
	
			rewrite ^(.*)/websocket/(.*) /$2  break;
	
	       	 	# redirect all HTTP traffic to localhost:8080
	        	proxy_pass http://telemetry_websocket;
	        	proxy_set_header X-Real-IP $remote_addr;
	        	proxy_set_header Host $host;
	        	proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
	
	        	# WebSocket support (nginx 1.4)
	        	proxy_http_version 1.1;
	        	proxy_set_header Upgrade $http_upgrade;
	       	 	proxy_set_header Connection "upgrade";
		}
		
		# the main app root
		location ~ /  {
	    		# path for static files
	    		root /home/webapps/telemetry/public;
	    		
	    		# this sets the static doc files to be served via nginx, but still
	    		# be behind Rails authentication
	    		proxy_set_header  X-Sendfile-Type  X-Accel-Redirect;
	    		proxy_set_header  X-Accel-Mapping  /home/webapps/telemetry/=/docs/;
	
	    		# Prefer to serve static files directly from nginx to avoid unnecessary
	    		# data copies from the application server.
	    		#
	    		# try_files directive appeared in in nginx 0.7.27 and has stabilized
	    		# over time.  Older versions of nginx (e.g. 0.6.x) requires
	    		# "if (!-f $request_filename)" which was less efficient:
	    		# http://bogomips.org/unicorn.git/tree/examples/nginx.conf?id=v3.3.1#n127
	    		try_files $uri/index.html $uri.html $uri @telemetry;
		}
	
	  	# Rails error pages
		error_page 500 502 503 504 /500.html;
	        location ~ /500.html {
	        	root /home/webapps/telemetry/public;
	        }
	}
	
	# the main app connection is done through a UNIX socket
	location @telemetry {
		# an HTTP header important enough to have its own Wikipedia entry:
		#   http://en.wikipedia.org/wiki/X-Forwarded-For
		proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
	
		# enable this if you forward HTTPS traffic to unicorn,
		# this helps Rack set the proper URL scheme for doing redirects:
		# proxy_set_header X-Forwarded-Proto $scheme;
	
		# pass the Host: header from the client right along so redirects
		# can be set properly within the Rack application
		proxy_set_header Host $http_host;
	
		# we don't want nginx trying to do something clever with
		# redirects, we set the Host: header above already.
		proxy_redirect off;
	
		# set "proxy_buffering off" *only* for Rainbows! when doing
		# Comet/long-poll/streaming.  It's also safe to set if you're using
		# only serving fast clients with Unicorn + nginx, but not slow
		# clients.  You normally want nginx to buffer responses to slow
		# clients, even with Rails 3.1 streaming because otherwise a slow
		# client can become a bottleneck of Unicorn.
		#
		# The Rack application may also set "X-Accel-Buffering (yes|no)"
		# in the response headers do disable/enable buffering on a
		# per-response basis.
		# proxy_buffering off;
	
		proxy_pass http://telemetry_server;
	}

**Wrap Up**

After the configuration is finished, you can restart the Unicorn daemon and the Nginx server in that order.

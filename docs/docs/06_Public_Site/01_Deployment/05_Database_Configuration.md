The database for all of the LDWF projects is PostgreSQL.

There is a central database server that all the projects tie into.
The server is at **10.223.208.130** and can only be accessed from the **louisianafisheries.net** server.

The database server uses PostGIS for the telemetry project.

**Create database**

To create the database, it may be best to use the PostgreSQL tools vs. the Rails database tools.

	## if user exists skip the first line
	psql -d postgres -c "CREATE USER telemetry WITH password '8RN1hGbOsyd3CN8BnMWotF6sPVchwf' LOGIN"
	createdb telemetry_development -O telemetry
	psql -d telemetry_developement -c "create extension postgis;"

**Database dump**

To get a database dump, you must first SSH into the louisianafisheries.net server.

	ssh [USER]@louisianafisheries.net
	
Then run the following to get a database dump:

	pg_dump -h 10.223.208.130 -p 5432 -U telemetry -Fc -b -v -f telemetry.backup [telemetry_staging || telemetry_production]
	
You can then use SCP to transfer telemetry.backup to your development machine.

**Restore dump**

To restore the data base use the following:

	postgis_restore.pl "/somepath/telemetry.backup" | psql -h localhost -p 5432 -U telemetry telemetry_development 2> errors.txt
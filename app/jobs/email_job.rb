class EmailJob < ActiveJob::Base
  queue_as :email

  def perform(notification_topic, notification_item)
    # Create the base email
    default_email = nil
    # Determine which type of email to make based on the notification topic
    if notification_topic.capture?
      default_email = AdminNotifyMailer.new_capture_email(notification_item)
    elsif notification_topic.recapture?
      default_email = AdminNotifyMailer.new_recapture_email(notification_item)
    elsif notification_topic.user_tag_request?
      default_email = AdminNotifyMailer.new_tag_request_email(notification_item)
    elsif  notification_topic.user?
      default_email = AdminNotifyMailer.new_app_user_email(notification_item)
    end

    # Create an empty array for recipients of the email
    recipients = []
    notification_topic.users.each do |user|
      # Get the user's courier for this notification topic
      courier = user.user_couriers.where(:notification_topic_id => notification_topic.id).first
      unless courier.nil?
        # TODO notify user based on courier type
        unless courier.notification_courier.nil?
          # if they requested email notifications
          if courier.notification_courier.email?
            unless courier.daily_digest?
              # add this user to the list of recipients
              recipients << user.email
            end
          end # end of email courier
        end # end of unless
      end
    end
    # send each recipient a copy of the email
    recipients = recipients.join(',')
    default_email.bcc = recipients
    default_email.deliver
  end
end
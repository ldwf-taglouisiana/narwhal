class UserReportEmailJob < ActiveJob::Base

  queue_as :email

  def perform(recapture)
    UserNotifyMailer.notify_release_angler_email(recapture.capture).deliver if recapture.capture

    Recapture.where(tag_id: recapture.tag_id).order(capture_date: :desc).each do |recap|
      need_reward = recapture == recap
      UserNotifyMailer.notify_reward_sender_email(recap).deliver if need_reward
      UserNotifyMailer.notify_recapture_angler_email(recap, need_reward ).deliver
    end

  end

end


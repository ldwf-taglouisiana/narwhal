class ConvertScratchCaptureJob < ActiveJob::Base
  queue_as :convert_drafts

  def perform
    ScratchCapture.check_for_expiration
  end
  
end


class RefreshArchivedViewsJob < ActiveJob::Base
  queue_as :refresh_view

  def perform
    RefreshArchivedCaptureJob.perform_if_needed
    RefreshArchivedRecaptureJob.perform_if_needed
    RefreshArchivedVolunteerHoursJob.perform_if_needed
    RefreshArchivedTagsJob.perform_if_needed
  end
end

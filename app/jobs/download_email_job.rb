class DownloadEmailJob < ActiveJob::Base
  queue_as :email

  def perform(requested_download)
    # send the email to download the file.
    DownloadReadyMailer.download_ready_email(requested_download).deliver
  end
end
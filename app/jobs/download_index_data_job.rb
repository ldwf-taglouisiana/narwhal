class DownloadIndexDataJob < ActiveJob::Base
  queue_as :download

  include ReportModule

  REPORT_TYPES = {
      angler: 1,
      capture: 2,
      recapture: 3,
      volunteer_time_logs: 4,
      tag_assignments: 5,
      draft_capture: 6
  }

  def perform(type, params, token)

    params = ActionController::Parameters.new(params) unless params.class <= ActionController::Parameters

    # make the target directory for the files, if needed
    FileUtils.mkdir_p(File.join(Rails.root, FILE_PATH)) unless File.exists?(File.join(Rails.root, FILE_PATH))

    xlsx = nil
    verified = params[:verified].present?

    # check the type of report being requested
    case type
      when REPORT_TYPES[:angler]
        items = params[:select_all] == 'true' ? Angler.includes(:shirt_size_option) : Angler.includes(:shirt_size_option).where(angler_id: params[:options])
        xlsx = anglers_to_xlsx(items)

      when REPORT_TYPES[:capture]
        items = params[:select_all] == 'true' ? PresentableCapture.where(verified: verified) : PresentableCapture.where(id: params[:options], verified: verified)
        xlsx = captures_to_xlsx(items)

      when REPORT_TYPES[:recapture]
        items = params[:select_all] == 'true' ? PresentableRecapture.where(verified: verified) : PresentableRecapture.where(id: params[:options], verified: verified)
        xlsx = recaptures_to_xlsx(items)

      when REPORT_TYPES[:draft_capture]
        items = params[:select_all] == 'true' ? DraftCapture.includes(:species, :angler, :species_length, :time_of_day_option) : DraftCapture.includes(:species, :angler, :species_length, :time_of_day_option).where(id: params[:options])
        xlsx = draft_captures_to_xlsx(items)

      when REPORT_TYPES[:volunteer_time_logs]
        items = params[:select_all] == 'true' ? PresentableVolunteerHour.all : PresentableVolunteerHour.where(id: params[:options])
        xlsx = hours_to_xlsx(items)

      when REPORT_TYPES[:tag_assignments]
        if params[:options][:date][:month].presence
          date = Date.parse("1-#{params[:options][:date][:month]}-#{params[:options][:date][:year].presence || Time.now.year}")
          where = 'assignment_at <@ daterange(?,?, \'[]\')'
          binds = [date.beginning_of_month, date.end_of_month]
        end

        items = params[:select_all] == 'true' ? TagAssignment.where(where, binds) : TagAssignment.where(id: params[:options][:ids])
        xlsx = assignments_to_xlsx(items)

      else
        raise ArgumentError.new('Must use a type from DownloadIndexDataJob::REPORT_TYPES')
    end

    # write the workbook out to the proper file
    path = File.join(Rails.root, FILE_PATH, token)
    xlsx.serialize path

    # update the download item with the correct path
    download = RequestedDownload.where(token: token).first
    download.update_attributes({filepath: path})

    # send email if needed
    download.send_email
  end

  private

  def anglers_to_xlsx(items)
    Axlsx::Package.new do |p|
      wb = p.workbook
      wb.add_worksheet(name: 'Anglers') do |sheet|
        sheet.add_row ['Angler Id', 'First Name', 'Last Name', 'Street', 'Suite', 'City', 'State', 'Zip', 'Phone Number', 'Email', 'Shirt Size']
        items.each do |angler|
          row = angler.attributes.values_at('angler_id', 'first_name', 'last_name', 'street', 'suite', 'city', 'state', 'zip', 'phone_number_1', 'email', 'shirt_size_option_name') + [angler.shirt_size.try(:attributes).try(:values_at, 'shirt_size_option')]
          sheet.add_row row.flatten
        end
      end
    end
  end

  def captures_to_xlsx(items)
    Axlsx::Package.new do |p|
      wb = p.workbook
      wb.add_worksheet(name: 'Captures') do |sheet|
        sheet.add_row ['Angler ID', 'Angler Name', 'Tag Number', 'Date', 'Species', 'Latitude', 'Longitude', 'Length','Length Range', 'Time of Day', 'Fish Condition', 'Verified', 'Location Description', 'Comments']
        items.each do |capture|
          sheet.add_row capture.attributes.values_at('angler_id', 'capture_angler', 'tag_number', 'capture_date', 'species_name', 'latitude_formatted', 'longitude_formatted', 'length', 'length_range', 'time_of_day', 'fish_condition', 'verified', 'location_description', 'comments')
        end
      end
    end
  end

  def recaptures_to_xlsx(items)
    Axlsx::Package.new do |p|
      wb = p.workbook
      wb.add_worksheet(name: 'Recaptures') do |sheet|
        sheet.add_row ['Recapture Angler ID', 'Recapture Angler Name', 'Tag Number', 'Date', 'Species', 'Latitude', 'Longitude', 'Length', 'Length Range', 'Time of Day', 'Fish Condition', 'Verified', 'Location Description', 'Comments']
        items.each do |recapture|
          sheet.add_row recapture.attributes.values_at('recapture_angler_id', 'recapture_angler_name', 'tag_number', 'capture_date', 'species_name', 'latitude_formatted', 'longitude_formatted', 'length', 'length_range', 'time_of_day', 'fish_condition', 'verified', 'location_description', 'comments')
        end
      end
    end
  end

  def draft_captures_to_xlsx(items)
    Axlsx::Package.new do |p|
      wb = p.workbook
      wb.add_worksheet(name: 'Draft Captures') do |sheet|
        sheet.add_row ['Angler ID', 'Angler Name', 'Tag Number', 'Date', 'Species', 'Latitude', 'Longitude', 'Length', 'Length Range', 'Time of Day', 'Fish Condition', 'Recapture', 'Location Description', 'Comments']
        items.each do |draft_capture|
          sheet.add_row [draft_capture.angler.try(:angler_id), draft_capture.angler.try(:first_name) + draft_capture.angler.try(:last_name), draft_capture.tag_number,
                         draft_capture.capture_date, draft_capture.species.try(:common_name), draft_capture.latitude_string, draft_capture.longitude_string, draft_capture.length, draft_capture.species_length.try(:description),
                         draft_capture.time_of_day_option.try(:time_of_day_option), draft_capture.fish_condition_id, draft_capture.recapture, draft_capture.location_description, draft_capture.comments]
        end
      end
    end
  end

  def assignments_to_xlsx(items)
    Axlsx::Package.new do |p|
      wb = p.workbook
      wb.add_worksheet(name: 'Tag Assignments') do |sheet|
        sheet.add_row ['Angler ID', 'Angler Name', 'Tag Prefix', 'Assignment Date', 'Start Tag', 'End Tag', 'Quantity', 'Last Used Tag']
        items.each do |assignment|
          sheet.add_row assignment.attributes.values_at('angler_id', 'angler_name', 'tag_prefix', 'assignment_at', 'start_tag', 'end_tag', 'number_of_tags', 'last_tag_used')
        end
      end
    end
  end

  def hours_to_xlsx(items)
    Axlsx::Package.new do |p|
      wb = p.workbook
      wb.add_worksheet(name: 'Volunteer Hours') do |sheet|
        sheet.add_row ['Date Entered', 'Angler ID', 'Angler Name', 'Start Date', 'End Date', 'Total Time', 'Number Capture', 'Verified', 'LDWF Entered', 'LDWF Edited']

        items.each do |time_log|
          sheet.add_row time_log.attributes.values_at('created_at', 'angler_id', 'angler_name', 'start', 'end', 'total_time', 'number_of_captures', 'verified', 'ldwf_entered', 'ldwf_edited')
        end
      end
    end
  end
end
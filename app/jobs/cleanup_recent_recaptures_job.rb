class CleanupRecentRecapturesJob < ActiveJob::Base
  queue_as :refresh_view

  TIME_CUTOFF_DAYS = 30

  rescue_from(StandardError) do |exception|
    ExceptionNotifier.notify_exception(exception, :env => Rails.env, :data => {:message => "was doing something wrong"})
  end

  def perform
    ActiveRecord::Base.connection_pool.with_connection do |conn|
      query = <<-SQL
        BEGIN;
        UPDATE #{Recapture.table_name} SET recent = FALSE WHERE recent = true AND verified = TRUE AND created_at < '#{TIME_CUTOFF_DAYS.days.ago.iso8601}';
        REFRESH MATERIALIZED VIEW CONCURRENTLY #{ArchivedRecapture.table_name};
        END;
      SQL
      conn.execute(query)
    end
  end
end
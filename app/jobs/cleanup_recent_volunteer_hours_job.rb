class CleanupRecentVolunteerHoursJob < ActiveJob::Base
  queue_as :refresh_view

  TIME_CUTOFF_DAYS = 30

  rescue_from(StandardError) do |exception|
    ExceptionNotifier.notify_exception(exception, :env => Rails.env, :data => {:message => "was doing something wrong"})
  end

  after_perform do |job|
    # todo the delay should based on the number of captures, not the number of
    CleanupRecentVolunteerHoursJob.set(wait: TIME_CUTOFF_DAYS.days).perform_later
  end

  def perform
    ActiveRecord::Base.connection_pool.with_connection do |conn|
      query = <<-SQL
        BEGIN;
        UPDATE #{VolunteerTimeLog.table_name} SET recent = false WHERE recent = TRUE AND created_at < '#{TIME_CUTOFF_DAYS.days.ago.iso8601}';
        REFRESH MATERIALIZED VIEW CONCURRENTLY #{ArchivedVolunteerHour.table_name};
        END;
      SQL
      conn.execute(query)
    end
  end
end
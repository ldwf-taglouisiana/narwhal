class RefreshArchivedTagsJob < ActiveJob::Base
  queue_as :refresh_view

  REDIS_KEY = GlobalConstants::REDIS_TAG_UPDATING_KEY

  rescue_from(StandardError) do |exception|
    # decrement the counter
    $redis_store.decr REDIS_KEY

    # ensure that the key never falls below zero
    $redis_store.set(REDIS_KEY, 0) if $redis_store.get(REDIS_KEY).to_i < 0

    ExceptionNotifier.notify_exception(exception, :env => Rails.env, :data => {:message => "was doing something wrong"})
  end

  after_enqueue do |job|
    # increment the counter
    $redis_store.incr REDIS_KEY
  end

  after_perform do |job|
    # decrement the counter
    $redis_store.decr REDIS_KEY

    # ensure that the key never falls below zero
    $redis_store.set(REDIS_KEY, 0) if $redis_store.get(REDIS_KEY).to_i < 0
  end

  def perform
    # only do the work, if there are fewer than 3 jobs enqueued, else do a noop
    if $redis_store.get(REDIS_KEY).to_i < 3

      ActiveRecord::Base.connection_pool.with_connection do |conn|
        query = <<-SQL
        UPDATE tags
        SET recent = FALSE
        FROM recent_tags
        WHERE tags.id = recent_tags.id
          AND (tags.angler_id IS NOT NULL AND tags.updated_at < (now() - '90 days' :: INTERVAL))
          AND tags.tag_lot_id NOT IN (SELECT
                                         id
                                       FROM tag_lots
                                       ORDER BY
                                         created_at DESC
                                       LIMIT 3);

          REFRESH MATERIALIZED VIEW CONCURRENTLY #{ArchivedTag.table_name};
        SQL
        conn.execute(query)
      end

    end
  end

  def self.perform_if_needed
    if $redis_store.get(REDIS_KEY).to_i < 3
      self.perform_later
    end
  end
end

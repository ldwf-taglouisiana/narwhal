class NotificationJob < ActiveJob::Base
  queue_as :notifications

  def perform(topic, notification_item)
    users = topic.users

    users.each do |user|
      Notification.create(:notification_item => notification_item, :user => user, :notification_topic => topic, :checked => false)
    end

    EmailJob.perform_later(topic, notification_item)
  end
end
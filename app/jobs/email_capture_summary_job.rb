class EmailCaptureSummaryJob < ActiveJob::Base
  queue_as :email

  def perform()
    spreadsheet = CaptureSpreadsheetModule::CaptureSpreadsheet.export(
        Chronic.parse('yesterday').beginning_of_day.strftime('%m/%d/%Y').to_s,
        Chronic.parse('yesterday').end_of_day.strftime('%m/%d/%Y').to_s,
        {:summary => true}
    ).to_stream.read

    recipient_list = User.emails_for_daily_capture_summary
    AdminNotifyMailer.capture_summary_email(recipient_list, spreadsheet).deliver
  end
end
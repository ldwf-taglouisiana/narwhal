class DownloadReportJob< ActiveJob::Base
  queue_as :download

  include ReportModule
  include ReportDataHelper

  REPORT_TYPES = {
      :tagged_fish => 1,
      :recaptured_fish => 2,
      :captures_by_angler => 3,
      :recaptures_by_angler => 4,
      :days_at_large => 5,
      :data_check => 6,
      :banquet => 7,
      :unused_kits => 8,
      :sql_dump => 9,
      :sql_dump_csv => 10,
      :angler_info => 11
  }

  def perform(type, params, token)

    params = ActionController::Parameters.new(params) unless params.class <= ActionController::Parameters

    # make the target directory for the files, if needed
    FileUtils.mkdir_p(File.join(Rails.root,FILE_PATH)) unless File.exists?(File.join(Rails.root,FILE_PATH))
    path = File.join(Rails.root, FILE_PATH, token)
    worksheet = nil

    # check the type of report being requested
    case type
      when REPORT_TYPES[:tagged_fish]
        worksheet = tagged_fish_list_to_xlsx(params)

      when REPORT_TYPES[:recaptured_fish]
        worksheet = tagged_fish_list_to_xlsx(params)

      when REPORT_TYPES[:captures_by_angler]
        worksheet = captures_by_angler_list_to_xlsx(params)

      when REPORT_TYPES[:recaptures_by_angler]
        worksheet = recaptures_by_angler_list_to_xlsx(params)

      when REPORT_TYPES[:days_at_large]
        worksheet = days_at_large_list_to_xlsx(params)

      when REPORT_TYPES[:data_check]
        worksheet = data_check_list_to_xlsx(params)

      when REPORT_TYPES[:banquet]
        worksheet = banquet_to_xlsx(params)

      when REPORT_TYPES[:unused_kits]
        worksheet = unused_kits_list_to_xlsx(params)

      when REPORT_TYPES[:sql_dump]
        db_config = Rails.configuration.database_configuration[Rails.env]
        cmd = "PGPASSWORD=\"#{db_config['password']}\" pg_dump \"#{db_config['database']}\" -h \"#{db_config['host']}\" -U \"#{db_config['username']}\" | 7za a -si -t7z -m0=lzma -mx=9 -mfb=64 -md=32m -ms=on \"#{path}\"."
        system cmd

      when REPORT_TYPES[:sql_dump_csv]
        temp_dir_path = Dir.mktmpdir('sql_db_dump_csv')
        FileUtils.chmod 0777, temp_dir_path

        begin
          db_config = Rails.configuration.database_configuration[Rails.env]


          tables = ActiveRecord::Base.connection.execute <<-SQL
            SELECT
              table_schema,
              (table_schema || '."' || table_name || '"') AS schema_table,
              table_name
            FROM information_schema.tables t INNER JOIN information_schema.schemata s
            ON s.schema_name = t.table_schema
            WHERE t.table_schema NOT IN ('pg_catalog', 'information_schema', 'configuration')
              AND t.table_type = 'BASE TABLE'
              AND ( NOT table_name ~* 'oauth')
            ORDER BY schema_table
          SQL

          tables.each do |item|
            output_path = File.join(temp_dir_path, item["table_name"] + '.csv')
            cmd = "PGPASSWORD=\"#{db_config['password']}\" psql -d \"#{db_config['database']}\" -h \"#{db_config['host']}\" -U \"#{db_config['username']}\" -c \"\\COPY #{item["table_schema"]}.\"#{item["table_name"]}\" TO STDOUT  DELIMITER ',' CSV HEADER\" > \"#{output_path}\""
            system cmd
          end

          system "7za a -t7z -m0=lzma -mx=9 -mfb=64 -md=32m -ms=on \"#{path}\". \"#{temp_dir_path}\""
        ensure
          FileUtils.remove_entry temp_dir_path
        end

      when REPORT_TYPES[:angler_info]
        worksheet = angler_info_xlsx(params)
      else
        raise ArgumentError.new('Must use a type from DownloadReportJob::REPORT_TYPES')

    end

    worksheet.serialize path unless worksheet.nil?

    # update the download item with the correct path
    download = RequestedDownload.where(token: token).first
    download.update_attributes({filepath: path})

    # send email if needed
    download.send_email
  end
end
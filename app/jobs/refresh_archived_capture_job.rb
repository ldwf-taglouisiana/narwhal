class RefreshArchivedCaptureJob < ActiveJob::Base
  queue_as :refresh_view

  REDIS_KEY = GlobalConstants::REDIS_CAPTURE_UPDATING_KEY

  rescue_from(StandardError) do |exception|
    # decrement the counter
    $redis_store.decr REDIS_KEY

    # ensure that the key never falls below zero
    $redis_store.set(REDIS_KEY, 0) if $redis_store.get(REDIS_KEY).to_i < 0

    ExceptionNotifier.notify_exception(exception, :env => Rails.env, :data => {:message => "was doing something wrong"})
  end

  after_enqueue do |job|
    # increment the counter
    $redis_store.incr REDIS_KEY
  end

  after_perform do |job|
    # decrement the counter
    $redis_store.decr REDIS_KEY

    # ensure that the key never falls below zero
    $redis_store.set(REDIS_KEY, 0) if $redis_store.get(REDIS_KEY).to_i < 0

    # call the other enqueue callbacks
    if job.arguments.first.try(:[], :with_callbacks) != false
      RefreshArchivedRecaptureJob.perform_if_needed(with_callbacks: false)
      RefreshArchivedTagsJob.perform_if_needed
    end
  end

  def perform(options = {})
    # only do the work, if there are fewer than 3 jobs enqueued, else do a noop
    if $redis_store.get(REDIS_KEY).to_i < 3

      ActiveRecord::Base.connection_pool.with_connection do |conn|
        query = <<-SQL
          REFRESH MATERIALIZED VIEW CONCURRENTLY #{ArchivedCapture.table_name};
        SQL
        conn.execute(query)
      end

    end
  end

  def self.perform_if_needed(options = {})
    if $redis_store.get(REDIS_KEY).to_i < 3
      self.perform_later(options)
    end
  end
end

class AnonUserReportEmailJob < ActiveJob::Base

  queue_as :email

  def perform(recapture)
    UserNotifyMailer.notify_anon_recap_angler_email(recapture).deliver
    UserNotifyMailer.notify_reward_sender_email(recapture).deliver
  end

end
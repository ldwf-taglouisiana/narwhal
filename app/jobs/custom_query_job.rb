class CustomQueryJob < ActiveJob::Base
  queue_as :query

  include ReportModule

  def perform(params, token)
    ActiveRecord::Base.connection_pool.with_connection do |conn|
      # get the spreadsheet File object
      spreadsheet = CustomizableQuery.spreadsheet_with_token(params, token)

      # update the download item with the correct path
      download = RequestedDownload.where(token: token).first
      download.update_attributes({filepath: spreadsheet.path})

      # send email if needed
      download.send_email
    end
  end
end
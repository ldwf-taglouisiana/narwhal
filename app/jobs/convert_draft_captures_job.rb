class ConvertDraftCapturesJob < ActiveJob::Base
  queue_as :convert_drafts

  def perform
    DraftCapture.check_for_expiration
  end

end
class UserEmailJob < ActiveJob::Base

  queue_as :email

  def perform(user_id)
    user = User.where(:id => user_id).first
    unless user.nil?
      UserNotifyMailer.new_user_email(user).deliver if Rails.env == 'production'
    end
  end

end
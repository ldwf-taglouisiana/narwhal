json.data do
  # going to return the whole list as a feature collection that leaflet.js can render
  json.type 'FeatureCollection'

  # we need to go through each items and add it as a feature
  json.features @basins do |item|
    json.type 'Feature'

    # we got the GeoJSON from the PostGIS extension
    json.geometry item.geo_json

    # add any specific properties about the item.
    # these are usually used for coloring or a popup
    json.properties do
      json.id item.gid
      json.name item.name
    end
  end
end

json.current number_with_delimiter(Angler.with_kit.count, delimiter: ',')
json.labels @data[:labels].reverse
json.current_year @data[:current_year].reverse
json.last_year @data[:last_year].reverse
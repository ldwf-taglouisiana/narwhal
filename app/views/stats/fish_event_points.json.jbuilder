json.data do
  # going to return the whole list as a feature collection that leaflet.js can render
  json.type 'FeatureCollection'

  # we need to go through each items and add it as a feature
  json.features @items do |item|
    json.type 'Feature'

    # we got the GeoJSON from the PostGIS extension
    json.geometry item.geo_json

    # add any specific properties about the item.
    # these are usually used for coloring or a popup
    json.properties do
      json.id item.id
      json.url url_for(item) # the path for the item, in case we need to navigate to it.
      json.angler item.angler_name
      json.angler_id item.angler_number
      json.tag_number item.tag_number
      json.capture_date item.capture_date
    end
  end
end

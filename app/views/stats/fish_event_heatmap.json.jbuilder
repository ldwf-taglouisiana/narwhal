json.data do
  json.max @items.max_by(&:event_count).event_count
  json.data do
    json.array!(@items) do |item|
      json.lat item.latitude
      json.lng item.longitude
      json.count item.event_count
    end
  end
end
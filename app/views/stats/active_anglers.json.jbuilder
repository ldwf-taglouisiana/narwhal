json.current number_with_delimiter(Angler.currently_active.count, delimiter: ',') + ' (last 30 days) '
json.labels @data[:labels].reverse
json.current_year @data[:current_year].reverse
json.last_year @data[:last_year].reverse
json.data do
  json.title format_title_for_species_table(@params)
  json.total number_with_delimiter(@items.collect {|item| item.species_count}.inject{|sum,x| sum + x }, delimiter: ',')
  json.items(@items) do |item|
    json.name item.species_name
    json.count number_with_delimiter(item.species_count, delimiter: ',')
  end
end
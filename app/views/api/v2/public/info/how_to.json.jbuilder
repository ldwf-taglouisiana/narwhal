json.partial! 'api/v2/public/options/partials/common', common: @items
json.items @items[:items] do |item|
  json.text item.description
end
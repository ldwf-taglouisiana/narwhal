json.partial! 'api/v2/public/options/partials/common', common: @items
json.items @items[:items] do |item|
  json.user item.user.full_name
  json.title item.title
  json.content item.content
  json.published_at item.should_publish_at.iso8601
end
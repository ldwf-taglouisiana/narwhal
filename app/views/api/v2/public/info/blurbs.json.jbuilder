json.partial! 'api/v2/public/options/partials/common', common: @items
@items[:items].first.attributes.select{|k,v| k.to_sym != :id}.each do |k,v|
  json.set!(k.to_sym, v);
end
json
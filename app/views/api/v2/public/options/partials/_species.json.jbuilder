json.partial! 'api/v2/public/options/partials/common', common: data

if data[:ids]
  json.ids data[:ids]

else
  json.items data[:items] do |item|
    json.(item, :id, :common_name, :position, :proper_name, :other_name, :family, :habitat, :description, :size, :food_value, :target, :publish)
    json.image_url "#{request.protocol}#{request.host_with_port}#{item.photo.url}"
    json.image_url_path "#{item.photo.url}"
    json.updated_at item.updated_at.iso8601
    json.updated_at_epoch item.updated_at.to_i
  end

end
if common[:errors]
  json.errors   common[:errors]
end

if common[:id_hash]
  json.id_hash  common[:id_hash].downcase
end

if common[:version]
  json.version  common[:version].iso8601
end

if common[:count]
  json.count    common[:count]
end
json.version @items[:version]
json.hash @items[:hash]

json.species do
  json.partial! 'api/v2/public/options/partials/species', data: @items[:species]
end

json.species_lengths do
  json.partial! 'api/v2/public/options/partials/species_lengths', data: @items[:species_lengths]
end

json.time_of_days do
  json.partial! 'api/v2/public/options/partials/time_of_days', data: @items[:time_of_days]
end

json.fish_conditions do
  json.partial! 'api/v2/public/options/partials/fish_conditions', data: @items[:fish_conditions]
end

json.dispositions do
  json.partial! 'api/v2/public/options/partials/dispositions', data: @items[:dispositions]
end

json.shirt_sizes do
  json.partial! 'api/v2/public/options/partials/shirt_sizes', data: @items[:shirt_sizes]
end
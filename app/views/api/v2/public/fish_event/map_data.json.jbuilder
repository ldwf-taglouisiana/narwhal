if params[:map_type] == 'Basin'

  json.data do
    # going to return the whole list as a feature collection that leaflet.js can render
    json.type 'FeatureCollection'

    # we need to go through each items and add it as a feature
    json.features @items do |item|
      json.type 'Feature'

      # we got the GeoJSON from the PostGIS extension
      json.geometry item.geo_json

      # add any specific properties about the item.
      # these are usually used for coloring or a popup
      json.properties do
        json.id item.id
        json.name item.name
        json.event_count item.event_count
      end
    end
  end

else

  json.data do
    json.max @items.max_by(&:event_count).event_count
    json.data do
      json.array!(@items) do |item|
        json.lat item.latitude
        json.lng item.longitude
        json.count item.event_count
      end
    end
  end

end
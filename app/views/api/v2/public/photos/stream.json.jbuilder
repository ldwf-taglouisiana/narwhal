json.partial! 'api/v2/public/options/partials/common', common: @items
json.items @items[:items] do |item|
  json.(item, :id, :featured)
  json.updated_at item.created_at.iso8601
  json.image_name item.image_name
  json.api_image_url "/api/v2/public#{item.photo.image.url.gsub(/%.*$/, '')}"
end
json.version @items[:version].try(:iso8601)
json.hash @items[:hash]

json.captures do
  json.partial! 'api/v2/protected/partials/captures', data: @items[:captures]
end

json.tags do
  json.partial! 'api/v2/protected/partials/tags', data: @items[:tags]
end

json.draft_captures do
  json.partial! 'api/v2/protected/partials/draft_capture', data: @items[:draft_captures]
end

json.time_logs do
  json.partial! 'api/v2/protected/partials/time_logs', data: @items[:time_logs]
end

json.tag_requests do
  json.partial! 'api/v2/protected/partials/tag_requests', data: @items[:tag_requests]
end

json.me do
  json.partial! 'api/v2/protected/partials/me', data: @items[:me]
end
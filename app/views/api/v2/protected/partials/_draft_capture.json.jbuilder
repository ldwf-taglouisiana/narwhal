json.partial! 'api/v2/public/options/partials/common', common: data
if data[:ids]
  json.ids data[:ids]

else
  json.items data[:items] do |item|
    json.(item, :id, :tag_number, :species_id, :length, :location_description, :latitude, :longitude, :fish_condition_id, :weight, :comments, :recapture, :recapture_disposition_id, :time_of_day_id, :entered_gps_type, :error_json, :should_save, :species_length_id, :uuid)
    json.angler_id item.angler.angler_id
    json.saved_at item.saved_at.try(:iso8601)
    json.saved_at_epoch item.saved_at.try(:to_i)
    json.expires_at (item.created_at + 24.hours).iso8601
    json.expires_at_epoch (item.created_at + 24.hours).to_i
    json.capture_date item.capture_date.iso8601
    json.capture_date_epoch item.capture_date.to_i
    json.updated_at item.updated_at.iso8601
    json.updated_at_epoch item.updated_at.to_i
    json.latitude_string item.latitude_string
    json.longitude_string item.longitude_string
    json.errors JSON.parse(item.error_json)
    json.images item.images do |image|
      json.id     image.id
      json.full   api_v2_protected_fish_entry_photos_path(image.id, image.image_file_name, stamp: image.updated_at.to_i)
      json.medium api_v2_protected_fish_entry_photos_path(image.id, image.image_file_name, style: :medium, stamp: image.updated_at.to_i)
      json.thumb  api_v2_protected_fish_entry_photos_path(image.id, image.image_file_name, style: :thumb, stamp: image.updated_at.to_i)
    end
    json.status item.status
  end
end
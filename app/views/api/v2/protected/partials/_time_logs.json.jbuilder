json.partial! 'api/v2/public/options/partials/common', common: data
if data[:ids]
  json.ids data[:ids]

else
  json.items data[:items] do |item|
    json.(item, :id, :verified)
    json.start_date item.start.iso8601
    json.start_date_epoch item.start.to_i
    json.end_date item.end.iso8601
    json.end_date_epoch item.end.to_i
    json.updated_at item.updated_at.iso8601
    json.updated_at_epoch item.updated_at.to_i
  end
end
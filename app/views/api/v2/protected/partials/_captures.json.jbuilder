json.partial! 'api/v2/public/options/partials/common', common: data
if data[:ids]
  json.ids data[:ids]

else
  json.items data[:items] do |item|
    json.partial! 'api/v2/protected/partials/capture', data: item
  end
end
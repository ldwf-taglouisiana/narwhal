json.partial! 'api/v2/public/options/partials/common', common: data
if data[:ids]
  json.ids data[:ids]

else
  json.items data[:items] do |item|
    json.(item, :id, :number_of_tags, :tag_type, :fulfilled)
    json.updated_at item.updated_at.iso8601
    json.updated_at_epoch item.updated_at.to_i
  end
end
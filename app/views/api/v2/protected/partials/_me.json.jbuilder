json.partial! 'api/v2/public/options/partials/common', common: data

json.user do
  json.(data[:user], :id, :first_name, :last_name, :email)
  json.angler_id data[:user].angler.try(:angler_id)
  json.updated_at data[:user].updated_at.iso8601
  json.updated_at_epoch data[:user].updated_at.to_i
end

json.angler do
  json.(data[:angler], :first_name, :last_name, :street, :city, :suite, :state, :zip, :email, :phone_number_1, :angler_id)
  json.id 0
  json.updated_at data[:angler].updated_at.iso8601
  json.updated_at_epoch data[:angler].updated_at.to_i
end
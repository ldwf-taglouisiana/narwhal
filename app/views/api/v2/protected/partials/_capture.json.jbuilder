json.(data, :id, :species_id, :tag_id, :tag_number, :location, :time_of_day_option_id, :length, :length_range, :entered_gps_type, :fish_condition_option_id, :latitude, :longitude, :latitude_formatted, :longitude_formatted, :basin_id, :sub_basin_id, :comments, :verified, :location_description)

json.recapture_count data.recaptures.where(verified: true).count

json.angler_id data.angler_number
json.images data.images do |image|
  json.id     image.id
  json.full   api_v2_protected_fish_entry_photos_path(image.id, image.image_file_name, stamp: image.updated_at.to_i)
  json.medium api_v2_protected_fish_entry_photos_path(image.id, image.image_file_name, style: :medium, stamp: image.updated_at.to_i)
  json.thumb  api_v2_protected_fish_entry_photos_path(image.id, image.image_file_name, style: :thumb, stamp: image.updated_at.to_i)
end

json.updated_at data.updated_at.iso8601
json.updated_at_epoch data.updated_at.to_i
json.capture_date data.capture_date.iso8601
json.capture_date_epoch data.capture_date.to_i
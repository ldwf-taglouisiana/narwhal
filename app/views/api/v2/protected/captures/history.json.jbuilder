json.capture do
  json.partial! 'api/v2/protected/partials/capture', data: @item[:capture]
  json.geo_json @item[:capture].geo_json(as_hash: true)
end

json.recaptures @item[:recaptures] do |item|
  json.partial! 'api/v2/protected/partials/recapture', data: item
  json.geo_json item.geo_json(obscured: item.obscure_location_from?(@item[:capture]), as_hash: true)
end
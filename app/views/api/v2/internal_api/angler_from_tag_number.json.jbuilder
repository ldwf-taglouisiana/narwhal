json.angler @angler
json.tag do
  if Tag.with_number(params[:tag_number]).first
    json.extract!(Tag.with_number(params[:tag_number]).first, :id, :tag_no)
  else
    json.id nil
    json.tag_no nil
  end
end
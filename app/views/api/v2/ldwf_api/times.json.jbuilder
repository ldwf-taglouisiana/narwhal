json.version @times.max_by(&:updated_at).try(:updated_at)
json.times @times, partial: 'api/v2/ldwf_api/partials/time', as: :time
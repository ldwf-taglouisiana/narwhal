json.version @species.max_by(&:updated_at).try(:updated_at)
json.species @species, partial: 'api/v2/ldwf_api/partials/species', as: :species
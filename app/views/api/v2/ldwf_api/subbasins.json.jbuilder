json.version "#{@subbasins.max_by(&:updated_at).updated_at}"
json.sub_basins @subbasins, partial: 'api/v2/ldwf_api/partials/sub_basin', as: :sub_basin
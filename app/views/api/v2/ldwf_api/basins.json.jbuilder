json.version "#{@basins.max_by(&:updated_at).updated_at}"
json.basins @basins, partial: 'api/v2/ldwf_api/partials/basin', as: :basin
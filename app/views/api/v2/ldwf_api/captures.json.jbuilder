json.all_records @results[:count]
json.version @results[:version]
json.count @results[:items].to_a.count
json.limit @results[:limit]
json.offset @results[:offset]
json.captures @results[:items] do |item|
  json.(item, :species_id, :capture_date, :time_of_day_option_id, :length, :length_range, :fish_condition_option_id, :latitude, :longitude)

  if includes = params[:include]
    json.basin item.basin, partial: 'api/v2/ldwf_api/partials/basin', as: :basin if includes.include?('basin')
    json.sub_basin item.sub_basin, partial: 'api/v2/ldwf_api/partials/sub_basin', as: :basin if includes.include?('sub_basin')
    json.species item.species, partial: 'api/v2/ldwf_api/partials/species', as: :species if includes.include?('species')
    json.time_of_day item.time_of_day_option, partial: 'api/v2/ldwf_api/partials/time', as: :basin if includes.include?('time_of_day')
  end

end
// TODO use the path helpers, instead of hard coding the routes

onReady($('body.volunteer-time-logs .index'), function (selector) {

    function initTable(selector, all) {
        selector.dataTable({
            searching:    false,
            lengthChange: true,
            autoWidth:    true,
            processing: true,
            serverSide: true,
            order: [ 7, 'desc' ],
            ajax: {
                url : Routes.volunteer_time_logs_path({format: 'json'}),
                data: function ( d ) {
                    d.all = all
                }
            },
            columns: [
                { data: "angler_number" },
                { data: "angler_name" },
                { data: "start_time",
                  render: function(data, type, full, meta){
                    return dateFormatfull(data);
                  }
                },
                { data: "end_time",
                  render: function(data, type, full, meta){
                    return dateFormatfull(data);
                  }
                },
                { data: "total_time"},
                {
                    data: "verified",
                    className: 'text-center',
                    render: function (data, type, full, meta) {
                        return data ? '<i class="fa fa-check center-table-icon"></i>' : '';
                    }
                },
                { data: "entered",
                  render: function(data, type, full, meta){
                    return dateFormatfull(data);
                  } },
                {
                    data: "number_of_captures",
                    className: 'text-center',
                    render: function (data, type, full, meta) {
                        if (data == 0) {
                            return '0';
                        } else {
                            return '<button onClick="timeLogsShowCaptureModal(\'' + Routes.captures_volunteer_time_log_path(full.id) +  '\')" class="num-capture-button btn btn-default" style="min-width: 80px" >' + data + '</button>';
                        }

                    }
                },
                {
                    data: "id",
                    orderable: false,
                    className: 'text-center',
                    render: function (data, type, full, meta) {
                        return '<a href="' + Routes.edit_volunteer_time_log_path(data) + '" target="_blank"><i class="fa fa-external-link"></i></a>';
                    }
                }
            ]
        });
    }

    initTable(selector.find('#withCapturesTimeLogIndexList'), false);
    initTable(selector.find('#allTimeLogIndexList'), true);
});

/**
 * Show the detail table for the time logs.
 * Will fetch the list of captures from the server and show them as a DataTable.
 *
 * This function is called with the onclick for the index table rows.
 *
 * @param {String} url to fetch the capture data
 */
function timeLogsShowCaptureModal(url) {

    var selector = $("#myModal table");

    if ($.fn.dataTable.isDataTable('#myModal table')) {
        table = selector.DataTable();
        table.destroy();
    }

    selector.dataTable({
        searching:    false,
        lengthChange: true,
        autoWidth:    true,
        processing: true,
        serverSide: true,
        deferRender: true,
        order: [ 11, 'desc' ],
        ajax: {
            url : url
        },
        columns: [
            { data: "angler_id" },
            { data: "capture_angler" },
            { data: "tag_number" },
            { data: "date_string"},
            { data: "species_name" },
            { data: "length" },
            { data: "latitude_formatted" },
            { data: "longitude_formatted" },
            { data: "fish_condition_option_id" },
            {
              data: "recapture_count",
              className : 'text-center',
              render: function ( data, type, full, meta ) {
                return data > 0 ? '<i class="fa fa-check"></i>' : '';
              }
              },
            {
              data: "time_of_day",
              render: function (data, type, full, meta) {
                return data ? data.replace(/\s\(.*/g, "") : '';
              }
            },
              { data: "entered",
                render: function (data, type, full, meta) {
                  return dateFormatMDY(data);
                }
              },
              {
                  data: "id",
                  orderable: false,
                  className : 'text-center',
                  render: function ( data, type, full, meta ) {
                      return '<a href="' + Routes.capture_path(data) + '" target="_blank"><i class="fa fa-external-link"></i></a>';
                  }
              }
        ]
    });

    $('#myModal').modal('show')
}
// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require application

$(document).ready(function () {
    $(".user-angler-select").change(function () {
        $('#new_angler_id').val($(this).find(":selected").val());
    });

    $('#user_angler_id').focusout(function () {
        this.value = this.value.toUpperCase().replace(' ', '');
    });

    $('#delete_unused_angler').change(function () {
        if (($(this).attr("checked") == "checked")){
            alert("When update is clicked, the angler with ID " + $('#old-angler-id').val() + " will be deleted.");
            $('#update_assoc_data').attr("checked", true);
        }
        else{
            $('#update_assoc_data').attr("checked", false);

        }
    });

    $('.notification-checkbox').change(function () {
        if (($(this).attr('checked') != 'checked')) {
            $(this).closest('.notification-group').find('.courier-checkbox').each(function () {
                $(this).removeAttr('checked');
            });
        }
    });

    $('.courier-checkbox').change(function () {
        if (($(this).attr('checked') == 'checked')) {
            $(this).closest('.notification-group').find('.notification-checkbox').attr('checked', 'checked')
        }
    });

    $('.notification-checkbox').each(function () {
        if (($(this).attr('checked') != 'checked')) {
            $(this).closest('.notification-group').find('.courier-checkbox').each(function () {
                $(this).removeAttr('checked');
            });
        }
    });

    $('.courier-checkbox').each(function () {
        if (($(this).attr('checked') == 'checked')) {
            $(this).closest('.notification-group').find('.notification-checkbox').attr('checked', 'checked')
        }
    });

    $('#myTab a').click(function (e) {
        e.preventDefault()
        $(this).tab('show')
    })

});
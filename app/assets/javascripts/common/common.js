$(document).on('ready page:load', function () {

    $('.nav.nav-tabs li:first-child').addClass('active');
    $('.tab-content').children('.tab-pane:first-child').addClass('active');

    $('[data-toggle="tooltip"]').tooltip('destroy').tooltip();

    $('a[disabled=disabled]').click(function (event) {
        event.preventDefault(); // Prevent link from following its href
    });

    $(".navbar li.dropdown").each(function(){
       var links = $(this).find("li a");
       var separators = $(this).find("li.divider ~ li.divider").each(function(){
           // Remove adjacent dividers from the menu.
          $(this).remove();
       });
       $(this).find("li.divider:first-child").remove(); // Remove first divider.
       $(this).find("li.divider:last-child").remove(); // Remove last divider.
       if (links.length == 0){
           $(this).remove();
       }
    });

    $('.summernote').summernote();

    $('.notification-anchor[data-toggle="popover"]').popover({
            html: true,
            content: $('#notification_content'),
            template: '<div class="popover notifications" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
        }
    );

    $('body').on('click', function (e) {
        //only buttons
        //if ($(e.target).data('toggle') !== 'popover'
        //    && $(e.target).parents('.popover.in').length === 0) {
        //    $('[data-toggle="popover"]').popover('hide');
        //}
        //buttons and icons within buttons

        if ($(e.target).data('toggle') !== 'popover'
            && $(e.target).parents('[data-toggle="popover"]').length === 0
            && $(e.target).parents('.popover.in').length === 0) {
            $('[data-toggle="popover"]').popover('hide');
        }

    });

    $.fn.dataTable.ext.errMode = 'none';

    $('#example').on('error.dt',function (e, settings, techNote, message) {
        console.log('An error has been reported by DataTables: ', message);
    });

});
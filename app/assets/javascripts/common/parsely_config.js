PARSLEY_CONFIG = {
    excluded: "input[type=button], input[type=submit], input[type=reset], input[type=hidden], :hidden:not(.chosen-select)",
    focus: 'none',
    errorClass: 'has-error',
    successClass: 'has-success',
    classHandler: function (ParsleyField) {
        return ParsleyField.$element.closest(".form-group");
    },
    errorsContainer: function (ParsleyField) {
        return ParsleyField.$element.closest(".form-group");
    },
    errorsWrapper: '<ul class="help-block"></ul>',
    errorTemplate: '<li></li>'
};

/*
    the date field mask used in the date picker.
    fields with this, can be tested for equality with the mask when empty.
*/
DATE_FIELD_MASK = '__/__/____';

$(document).ready(function () {
    window.Parsley.on('field:validated', function (fieldInstance) {
        // if this is an excluded field, then hide the error block, if any
        if (fieldInstance.$element.is(PARSLEY_CONFIG['excluded'])) {
            // hide the message wrapper
            fieldInstance._ui.$errorsWrapper.css('display', 'none');
            // set validation result to true
            fieldInstance.validationResult = true;
            return true;
        }
    });

    /*
        Check the tag number field against the internal API.
     */
    Parsley.addAsyncValidator('tag_number', function (xhr) {
        var element = this.$element.parsley();
        var errors = xhr.responseJSON.errors;

        element.removeError('remote');
        element.removeError('remotevalidator');

        $.each(errors, function (index, error) {
            window.ParsleyUI.addError(element, 'remotevalidator', error);
        });

        return 200 === xhr.status;

    }, Routes.api_v2_internal_validate_tag_path(), { dataType: "json", data: { angler_id: $('form.fish-entry .angler-id-field').val(), recapture: $('body').hasClass('recaptures') } });

    /*
     Check the tag number field against the internal API.
     */
    Parsley.addAsyncValidator('angler-id-available', function (xhr) {
        var element = this.$element.parsley();

        element.removeError('remote');
        element.removeError('remotevalidator');

        if (200 !== xhr.status) {
            window.ParsleyUI.addError(element, 'remotevalidator', xhr.responseJSON.error);
        }

        return 200 === xhr.status;

    }, Routes.api_v2_internal_validate_angler_id_available_path(), { field_param_name: 'angler_id', dataType: "json" });


    // set the param for the mailgun remote validation
    $('[data-parsley-remote-validator="mailgun-email"]').attr('remote-validation-param', 'address');
    /*
        Adds an email validator that uses the Mailgun api to check whether an email is correct or not.
        There is a limitation at the moment the only lets you have one mailgun-email-field on the page at a time.
     */
    Parsley.addAsyncValidator('mailgun-email', function (xhr) {
        var element = this.$element.parsley();
        var json = xhr.responseJSON;

        element.removeError('remote');
        element.removeError('remotevalidator');

        if (json.hasOwnProperty('is_valid')){
            if (json.is_valid == false) {
                if (json.did_you_mean == null) {
                    window.ParsleyUI.addError(element, 'remotevalidator', 'Email is incorrect');
                } else {
                    window.ParsleyUI.addError(element, 'remotevalidator', 'Did you mean: ' + json.did_you_mean);
                }
            }
            return json.is_valid;
        }
        else {
            // Fallback to true if the mailgun validator failed.
            return true;
        }

    }, 'https://api.mailgun.net/v2/address/validate', { dataType: "jsonp", crossDomain: true, data: { api_key: 'pubkey-0cc0c14f3749f6f8b9c4ff67d762eb1b' }});

    /*
        A date range validator for capture dates.
     */
    Parsley.addValidator('daterange', {
            requirementType: 'string',
            validateString: function (value, requirement) {
                return Date.create(value).isBetween(Date.create('6 years ago'), Date.create());
            },
            messages: {
                en: 'This value should be between ' + Date.create('6 years ago').short() + ' and ' + Date.create().short()
            }
        }
    );

    Parsley.addValidator('beforetoday', {
            requirementType: 'string',
            validateString: function (value, requirement) {
                return Date.create(value).isPast();
            },
            messages: {
                en: 'This value should be on or before today'
            }
        }
    );

    Parsley.addValidator('greaterthandatefield', {
            validateString: function (value, refOrValue) {
                var $reference = $(refOrValue);
                return value == DATE_FIELD_MASK || Date.create(value).isAfter(Date.create($reference.val()));
            },
            messages: {
                en: 'This value should be on or before today'
            }
        }
    );

    Parsley.addValidator('greaterthanfield', {
        validateString: function (value, refOrValue) {
            var $reference = $(refOrValue);

            // if there is a selector, then
            if ($reference.length) {

                // if the values can be converted to numbers, then compare them as numbers
                if (!isNaN(parseFloat(value)) && !isNaN(parseFloat($reference.val()))) {
                    return parseFloat(value) > parseFloat($reference.val());

                } else {
                    // if they cannot be compared as numbers, then treat them as strings, the default comparison
                    return value > $reference.val();
                }
            }
            else
                return value > refOrValue;
        }
        }
    );

    Parsley.addValidator('greaterthanequalfield', {
            validateString: function (value, refOrValue) {
                var $reference = $(refOrValue);

                // if there is a selector, then
                if ($reference.length) {

                    // if the values can be converted to numbers, then compare them as numbers
                    if (!isNaN(parseFloat(value)) && !isNaN(parseFloat($reference.val()))) {
                        return parseFloat(value) > parseFloat($reference.val()) || parseFloat(value) == parseFloat($reference.val());

                    } else {
                        // if they cannot be compared as numbers, then treat them as strings, the default comparison
                        return value > $reference.val() || value == $reference.val();
                    }
                }
                else
                    return value > refOrValue;
            }
        }
    );
});
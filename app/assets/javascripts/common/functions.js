/**
 * Creates a document on page:load callback with
 * a selector and a function to execute. The function will
 * be called with the jQuery object that contains the
 * selector.
 *
 * @param {String|jQuery} selector A string CSS selector or jQuery object
 * @param {Function} block The function to execute, will be called with the selector as a parameter
 */
function onReady(selector, block) {

    var targetSelector = selector;

    if (typeof selector == "string") {
        targetSelector = $(targetSelector);
    } else if (targetSelector instanceof jQuery) {
        // already a jQuery selector, we can use it as is
    } else {
        throw "onReady: fist argument must be a CSS string selector or a jQuery selector";
    }

    if (typeof block == "undefined" || typeof block != 'function') {
        throw "onReady: second argument must be a function";
    }

    $(document).on('page:change', function() {
        targetSelector.refresh();

        if (targetSelector.length > 0) {
            block(targetSelector);
        }
    });
}

/**
 * Creates a document on page:before-unload callback with
 * a selector and a function to execute. The function will
 * be called with the jQuery object that contains the
 * selector.
 *
 * @param {String|jQuery} selector A string CSS selector or jQuery object
 * @param {Function} block The function to execute, will be called with the selector as a parameter
 */
function onUnload(selector, block) {

    var targetSelector = selector;

    if (typeof selector == "string") {
        targetSelector = $(targetSelector);
    } else if (targetSelector instanceof jQuery) {
        // already a jQuery selector, we can use it as is
    } else {
        throw "onUnload: fist argument must be a CSS string selector or a jQuery selector";
    }

    if (typeof block == "undefined" || typeof block != 'function') {
        throw "onUnload: second argument must be a function";
    }

    $(document).on('page:before-unload', function() {
        targetSelector.refresh();

        if (targetSelector.length > 0) {
            block(targetSelector);
        }
    });
}

/**
 * Refreshes a jQuery selector
 *
 * http://stackoverflow.com/a/11868946
 *
 * @returns {jQuery}
 */
$.fn.refresh = function() {
    var elems = $(this.selector);
    this.splice(0, this.length);
    this.push.apply( this, elems );
    return this;
};

/**
 * Adds a truncate function to {String} allowing for strings that exceeds a ceratin length to be
 *
 * truncation and ellipsis added to the end.
 * @type {Function|*}
 */
String.prototype.trunc = String.prototype.trunc ||
    function (n) {
        return this.length > n ? this.substr(0, n - 1) + '&hellip;' : this;
    };


function dateFormatMDY(date){
  if (typeof date !== 'undefined' && date != null ) {
    return Date.create(date).format('{MM}/{dd}/{yyyy}');
  }
  return null;
}

function dateFormatfull(date){
  if (typeof date !== 'undefined' && date != null ) {
    return Date.create(date).format('{MM}/{dd}/{yyyy} {hh}:{mm} {TT}');
  }
  return null;
}
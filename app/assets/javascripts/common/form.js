$(document).on('page:change reportsChange', function() {
    var selector = $('form');

    // init the date pickers
    formInitDatePickers(selector);

    // initialize the file uploader
    formInitFileUpload(selector);

    // init the phone number masks
    selector.find('.phone-field').inputmask("(999) 999-9999");

    // init the zipcode input mask
    selector.find('.zip-field').inputmask("99999");

    // on uppercase fields we will auto capitalize them
    selector.find('.uppercase-field').unbind('keyup').keyup(function(){
        this.value = this.value.toUpperCase();
    });

    selector.find(".chosen-select").chosen();

    selector.find('button[type="reset"]').click(function() {
        selector.find(".chosen-select").val(null).trigger("chosen:updated");
    });

    selector.find('.uppercase-field').on('focusout', function() {
        $(this).val($(this).val().toUpperCase());
    });

    selector.find('.word-field').keyup(function () {
        this.value = this.value.toLowerCase().replace(/\b[a-z]/g, function (letter) {
            return letter.toUpperCase();
        });
    });
});

function formInitFileUpload(selector) {
    // http://www.abeautifulsite.net/whipping-file-inputs-into-shape-with-bootstrap-3/

    var updateFileName = function() {
        var input = $(this),
            numFiles = input.get(0).files ? input.get(0).files.length : 1,
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [numFiles, label]);
    };

    var fileSelected = function(event, numFiles, label) {
        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;

        if( input.length ) {
            input.val(log);
        } else {
            if( log ) alert(log);
        }
    };

    selector.find('.btn-file :file')
        .unbind('change', updateFileName)
        .on('change', updateFileName)
        .unbind('fileselect', fileSelected)
        .on('fileselect', fileSelected);
    // ----------------------------
}

function formInitDatePickers(selector) {
    var DATE_WRAPPER_FIELD_SELECTOR     = 'div.date-wrapper, div.datetime-wrapper';
    var DATE_VALUE_FIELD_SELECTOR       = 'input.date-value, input.datetime-value';
    var DATE_INPUT_FIELD_SELECTOR       = 'input.date-field';
    var DATE_TIME_INPUT_FIELD_SELECTOR  = 'input.datetime-field';

    if (selector.find(DATE_WRAPPER_FIELD_SELECTOR).length == 0){
        //Don't do anything if there are no dates.
        return;
    }

    function formDatePickerDateChanged(datePicker, input) {
        var val = input.val();

        if (val.length > 0) {
            input.closest(DATE_WRAPPER_FIELD_SELECTOR).find(DATE_VALUE_FIELD_SELECTOR).val(Date.create(val).toISOString());
        }
    }

    function formDatePickerFocusOut() {
        if ($(this).val() == '' || $(this).val() == '__/__/____' || $(this).val() == '__/__/____ h:__ A') {
            $(this).closest(DATE_WRAPPER_FIELD_SELECTOR).find(DATE_VALUE_FIELD_SELECTOR).val('');
        } else {
            timeString = Date.create($(this).val()).toISOString();
            $(this).closest(DATE_WRAPPER_FIELD_SELECTOR).find(DATE_VALUE_FIELD_SELECTOR).val(timeString);
        }
    }

    // unbind the pickers first
    selector.find(DATE_INPUT_FIELD_SELECTOR).datetimepicker('destroy');
    selector.find(DATE_TIME_INPUT_FIELD_SELECTOR).datetimepicker('destroy');

    // set up the date pickers so the can use just the calendar
    selector.find(DATE_INPUT_FIELD_SELECTOR).each(function() {
        var tempThis = $(this);
        var timeValue = $(this).closest(DATE_WRAPPER_FIELD_SELECTOR).find(DATE_VALUE_FIELD_SELECTOR).val();
        var timeString = null;

        if (timeValue) {
            timeString  = Date.create($(this).closest(DATE_WRAPPER_FIELD_SELECTOR).find(DATE_VALUE_FIELD_SELECTOR).val()).format('{MM}/{dd}/{yyyy}');
        }

        $(this).datetimepicker({
            value: timeString,
            timepicker: false,
            mask: true,
            format: 'm/d/Y',
            formatDate: 'm/d/Y',
            scrollInput: false,
            allowBlank: tempThis.data('autofillonblank') === undefined,
            onChangeDateTime: formDatePickerDateChanged
        });

        $(this).focusout(formDatePickerFocusOut);
    });

    // set up the date time pickers so the can use the calendar and time picker
    selector.find(DATE_TIME_INPUT_FIELD_SELECTOR).each(function() {
      var tempThis = $(this);

        var timeValue = $(this).closest(DATE_WRAPPER_FIELD_SELECTOR).find(DATE_VALUE_FIELD_SELECTOR).val();
        var timeString = '';

        if (timeValue) {
            timeString = Date.create(timeValue).format('{MM}/{dd}/{yyyy} {h}:{mm} {TT}');
        }

        $(this).datetimepicker({
            value: timeString,
            timepicker: true,
            mask: true,
            format: 'm/d/Y h:i A',
            formatDate: 'm/d/Y',
            formatTime: 'h:i A',
            scrollInput: false,
            allowBlank: tempThis.data('autofillonblank') === undefined,
            onChangeDateTime: formDatePickerDateChanged
        });

        $(this).focusout(formDatePickerFocusOut);
    });
}

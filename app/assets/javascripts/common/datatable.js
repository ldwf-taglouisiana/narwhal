/**
 * http://jsfiddle.net/gyrocode/5bmx7ejw/
 */
function dataTableUpdateSelectAllCtrl(table){
    var $table                   = table.table().node();
    var $chkbox_all              = $('tbody input[type="checkbox"]', $table);
    var $chkbox_checked          = $('tbody input[type="checkbox"]:checked', $table);
    var chkbox_select_all        = $('thead input[name="all"]', $table).get(0);
    var chkbox_select_all_param  = $('thead input[name="select_all"]', $table).get(0);

    // If none of the checkboxes are checked
    if($chkbox_checked.length === 0){
        chkbox_select_all.checked = false;
        chkbox_select_all_param.checked = false;

        if('indeterminate' in chkbox_select_all){
            chkbox_select_all.indeterminate = false;
        }

        // If all of the checkboxes are checked
    } else if ($chkbox_checked.length === $chkbox_all.length){
        chkbox_select_all.checked = true;
        chkbox_select_all_param.checked = true;

        if('indeterminate' in chkbox_select_all){
            chkbox_select_all.indeterminate = false;
        }

        // If some of the checkboxes are checked
    } else {
        //chkbox_select_all.checked = true;
        //chkbox_select_all_param.checked = false;
        //
        //if('indeterminate' in chkbox_select_all){
        //    chkbox_select_all.indeterminate = true;
        //}
    }
}
/**
 *
 * @param selector
 * @param table [DataTable]
 * @param selected_array
 */
function dataTableInitCheckBoxes(selector, table, selected_array) {
    // Handle click on checkbox
    selector.find('tbody').on('click', 'input[type="checkbox"]', function(e){
        var $row = $(this).closest('tr');

        // Get row data
        var data = table.row($row).data();

        // Get row ID
        var rowId = data.id;
        // Determine whether row ID is in the list of selected row IDs
        var index = $.inArray(rowId, selected_array);

        // If checkbox is checked and row ID is not in list of selected row IDs
        if(this.checked && index === -1){
            selected_array.push(rowId);

            // Otherwise, if checkbox is not checked and row ID is in list of selected row IDs
        } else if (!this.checked && index !== -1){
            selected_array.splice(index, 1);
        }

        if(this.checked){
            $row.addClass('selected');
        } else {
            $row.removeClass('selected');
        }

        // Update state of "Select all" control
        dataTableUpdateSelectAllCtrl(table);

        // Prevent click event from propagating to parent
        e.stopPropagation();
    });

    // Handle click on table cells with checkboxes
    selector.on('click', 'tbody td, thead th:first-child', function(e){
        $(this).parent().find('input[type="checkbox"]').trigger('click');
    });

    // Handle click on "Select all" control
    selector.find('thead input[name="all"]').on('click', function(e){
        if(this.checked){
            selector.find('tbody input[type="checkbox"]:not(:checked)').trigger('click');
        } else {
            selector.find('tbody input[type="checkbox"]:checked').trigger('click');
        }

        // Prevent click event from propagating to parent
        e.stopPropagation();
    });

    // Handle table draw event
    table.on('draw', function(){
        // Update state of "Select all" control
        dataTableUpdateSelectAllCtrl(table);
    });
}

function dataTableOptions(selected_array) {
    return {
        'columnDefs': [{
        'targets': 0,
        'searchable':false,
        'orderable':false,
        'className': 'dt-body-center',
        'render': function (data, type, full, meta){
            return '<input name="options[]" value="' + data + '" type="checkbox">';
        }
    }],
        'rowCallback': function(row, data, dataIndex){
        // Get row ID
        var rowId = data.id;

        // If row ID is in the list of selected row IDs
        if($.inArray(rowId, selected_array) !== -1){
            $(row).find('input[type="checkbox"]').prop('checked', true);
            $(row).addClass('selected');
        }
    }
    }
}
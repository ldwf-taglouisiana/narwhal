/**
 * Creates a Leaflet.js map object. The map is centered around (29.692825, -90.044769).
 * There are 2 map layer options as well as a Map Markings overlay.
 * The user can choose between a satellite map or a ocean base map.
 * Maps are provided by the ESRI free map tile service.
 *
 * @param {jQuery} selector The selector that will be used to create the map.
 * @returns {L.map} The map object that was created
 */
function createLeafletMap(selector) {
    var latitude = 29.692825;
    var longitude = -90.044769;

    var worldMap = L.esri.tiledMapLayer("https://services.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer", {
        detectRetina: true
        //,
        //attribution: "Sources: Esri, DeLorme, GEBCO, NOAA NGDC, and other contributors"
    });

    var oceanMap = L.esri.tiledMapLayer("https://services.arcgisonline.com/arcgis/rest/services/Ocean/World_Ocean_Base/MapServer", {
        detectRetina: true
        //,
        //attribution: "Sources: Esri, DigitalGlobe, Earthstar Geographics, CNES/Airbus DS, GeoEye, USDA FSA, USGS, Getmapping, Aerogrid, IGN, IGP, swisstopo, and the GIS User Community"
    });

    var mapMarkings = L.esri.tiledMapLayer("https://services.arcgisonline.com/arcgis/rest/services/Ocean/World_Ocean_Reference/MapServer", {
        detectRetina: true
        //,
        //attribution: "Sources: Esri, GEBCO, NOAA, National Geographic, DeLorme, HERE, Geonames.org, and other contributors"
    });

    // create the map that we will return
    var map = new L.map(selector[0], {
        center: [latitude, longitude],
        zoom: 8,
        layers: [worldMap,mapMarkings]
    });

    // get the base maps
    var baseMaps = {
        "World Map": worldMap,
        "Ocean Map": oceanMap
    };

    // get the overlays
    var overlayMaps = {
        "Markings" : mapMarkings
    };

    // add the layer control to the map.
    L.control.layers(baseMaps, overlayMaps).addTo(map);

    return map;
}
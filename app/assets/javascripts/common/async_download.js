/**
 * Creates a dialog for doing the async downloads of data.
 * Will poll every 3 seconds to check if a file is ready.
 * Once the file is ready, the download will start
 *
 * @param {String} The url of to post the data to. The route should return as JSON response of { token: String }
 */
function startAsyncDownload(url, title) {
    // the time in ms to check if the file is ready
    var POLL_TIME_MS = 3000;

    $.getJSON(url, function (data){
        // parse the file token from the request
        var token = data.token;

        // flag for controlling if a download should be started.
        var shouldDownloadWhenReady = true;

        // the timeout id
        var timeoutID;

        // show the modal
        downloadWaitingDialog.show(title, {
            token: token,
            onModalClose: function(){
                // when the modal is closed, stop the interval that checks for when a download is ready
                shouldDownloadWhenReady = false;
                window.clearInterval(timeoutID);
                return false;
            }
        });

        // this is the finction that will repeat until the file is downloaded
        var checkForDownloadReadyFunction = function() {

            // call the file ready JSON route to determine if the file ready
            $.getJSON('request_download/file_is_ready', {token: token}, function (data) {
                if (data.ready) {
                    window.clearInterval(timeoutID);

                    if (shouldDownloadWhenReady) {
                        // if the file is ready then use the ajax file downloader
                        $.fileDownload('/request_download/get?token=' + token, {
                            successCallback: function (url) {
                                // the download was started, hide the modal
                                downloadWaitingDialog.hide();
                            }
                        });
                    }
                }
            });
            // download the file without opening a new window

        };

        // start the download file loop
        timeoutID = setInterval(checkForDownloadReadyFunction, POLL_TIME_MS);
    });
}

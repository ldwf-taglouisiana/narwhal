onReady($('.scratch-captures .index'), function(selector){
  //selector.find('.table').dataTable();





  // the angler index DataTable params
  var dataTableParams = {
    searching:    false,
    lengthChange: true,
    autoWidth:    true,
    processing: true,
    serverSide: true,
    order: [ 3, 'desc' ],
    ajax: {
      url : "/scratch_captures.json",
      data: function(d){
        d.scope = "displayables";
      }
    },
    columns: [
      { data: "angler_number" },
      { data: "tag_number" },
      { data: "angler_name" },
      { data: "created_at",
        render: function( data, type, full, meta){
          return dateFormatMDY(data);
        }
      },
      { // assign tags
        data: "id",
        orderable: false,
        className : 'text-center',
        render: function ( data, type, row, meta ) {
          return '<a href="/scratch_captures/' + data + '"><i class="fa fa-tags center-table-icon"></i></a>';
        }
      },
      { // open
        data: "id",
        orderable: false,
        className : 'text-center center-table-icon',
        render: function ( data, type, row, meta ) {
          return '<a href="/scratch_captures/' + data + '/edit"><i class="fa fa-external-link center-table-icon"></i></a>';
        },
      },
      { // assign tags
        data: "id",
        orderable: false,
        className : 'text-center',
        render: function ( data, type, row, meta ) {
          return '<a href="/scratch_captures/' + data + '/convert_to_recapture" data-method="post"><i class="fa fa-tags center-table-icon"></i></a>';
        }
      }
    ]
  };

  // create the datatable object
  selector.find('.scratch-captures').DataTable(dataTableParams);



});

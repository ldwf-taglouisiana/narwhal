onReady($('.scratch-captures .show'), function(selector) {
  var map = selector.find('.map');
  var newMap = createLeafletMap(map);
  $.getJSON(map.data('path'), function(json){
    // ignore if the data property is null
    if (json.data != null) {
      // create the layer,
      var layer = L.geoJson(json.data, {});
      // add the layer to the map
      newMap.addLayer(layer);
      // center the map to the center of the layer
      newMap.panTo(layer.getBounds().getCenter());
    }
  });
});

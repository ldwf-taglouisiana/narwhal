onReady($('.scratch-captures form.scratch-entry'), function(selector){
  capturesFormInit(selector,false);
});

onUnload($('.scratch-captures form.scratch-entry'), function(selector){
  selector.parsley('destroy');
});
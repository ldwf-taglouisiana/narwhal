onReady($('body.stats .index'), function (selector) {
    // global variables for this closure
    var map;
    var layer;
    var geojson;
    var controls = [];

    // set all charts to be responsive
    Chart.defaults.global.responsive = true;

    /**
     * We are using an each due to a bug where the jS was being called twice.
     *
     * create a new map object for each target DOM element.
     */
    selector.find('#dashboard_map_canvas').not('.leaflet-container').each(function () {
        map = createLeafletMap(selector.find('#dashboard_map_canvas').not('.leaflet-container'));
    });

    /**
     * For each card-value container, we use the dat-url attribute to
     * Fetch the data need to build the graphs.
     */
    selector.find('.card-value[data-url]').each(function () {
        var self = $(this);
        var url = self.data('url');

        // follow the url to get the JSON data
        $.getJSON(url, function (json) {
            // use the returned JSON data to create the Chart.js data hash
            var data = {
                labels: json.labels,
                datasets: [
                    {
                        label: "Last 6 months",
                        fillColor: "rgba(67, 120, 158, 0.5)",
                        strokeColor: "rgb(67, 120, 158)",
                        pointColor: "rgb(67, 120, 158)",
                        pointStrokeColor: "#64723e",
                        pointHighlightFill: "#64723e",
                        pointHighlightStroke: "rgba(220,220,220,1)",
                        data: json.current_year
                    },
                    {
                        label: "Same Time Last Year",
                        fillColor: "rgba(176, 223, 243, 0.2)",
                        strokeColor: "rgb(176, 223, 243)",
                        pointColor: "rgb(176, 223, 243)",
                        pointStrokeColor: "#64723e",
                        pointHighlightFill: "#64723e",
                        pointHighlightStroke: "rgba(151,187,205,1)",
                        data: json.last_year
                    }
                ]
            };

            // get the canvas element in the current card
            var canvas = self.find('canvas');

            // show the canavs element and remove the loading indicator
            canvas.show();
            self.find('.loading').remove();

            // Get the context of the canvas element we want to select
            var ctx = canvas[0].getContext("2d");
            // create the chart object
            new Chart(ctx).Line(data);

            // update the text below the header with current number
            self.find('.text').text(json.current);
        });
    });

    /**
     * Create a layer with points for the give JSON of fish events
     *
     * @param {JSON} json The JSON that we will build the layer with.
     */
    var createPointsLayer = function (json) {
        /*
            For each marker pin, we will create a popup with some info about the event
            when clicked
         */
        function onEachFeature(feature, layer) {
            feature.properties.popupContent = 'Angler: ' + feature.properties.angler + '<br> Angler ID: ' + feature.properties.angler_id + '<br> Tag: ' + feature.properties.tag_number + '<br> Date: ' + Date.create(feature.properties.capture_date).long();
            layer.bindPopup(feature.properties.popupContent, {className: 'leaflet-popup-content-wrapper-black-text'});
        }

        /*
            Empty style for now
         */
        function style(feature) {
            return {};
        }

        // create the GeoJSON layer from the passed in json data
        layer = L.geoJson(json.data, {
            style: style,
            onEachFeature: onEachFeature
        });

        // add the layer to the map
        layer.addTo(map);

        // adjust the map bounds to fit the layer
        map.fitBounds(geojson.layer(), {padding: [10, 10]});
    };

    /**
     * Creates a layer with the basin shapes with a color to indicate the amount of events
     * in that shape.
     *
     * Base code was used from here http://leafletjs.com/examples/choropleth.html.
     *
     * @param {JSON} json The JSON that we will build the layer with.
     */
    var createBasinLayer = function (json) {

        /*
            The color gradient for the event counts
         */
        function getColor(d) {
            if (d > 1000) {
                return '#800026';
            } else if (d > 500) {
                return '#BD0026';
            } else if (d > 200) {
                return '#E31A1C';
            } else if (d > 100) {
                return '#FC4E2A';
            } else if (d > 50) {
                return '#FD8D3C';
            } else if (d > 20) {
                return '#FEB24C';
            } else if (d > 10) {
                return '#FED976';
            } else {
                return '#FFEDA0';
            }
        }

        /*
            Used as the mouseover for the basins in this layer.
            Updates the style of the shape, as well as the info control
         */
        function highlightFeature(e) {
            var layer = e.target;

            layer.setStyle({
                weight: 5,
                color: '#666',
                dashArray: '',
                fillOpacity: 0.7
            });

            if (!L.Browser.ie && !L.Browser.opera) {
                layer.bringToFront();
            }

            controls[0].update(layer.feature.properties);
        }

        /*
            Reset the shape styles when you mouseout
         */
        function resetHighlight(e) {
            geojson.resetStyle(e.target);
            controls[0].update();
        }

        /*
            Used to zoom and center the shape when clicked on
         */
        function zoomToFeature(e) {
            map.fitBounds(e.target.getBounds());
        }

        /*
            This is the each feature function that will be called
            on each GeoJSON feature.
         */
        function onEachFeature(feature, layer) {
            layer.on({
                mouseover: highlightFeature,
                mouseout: resetHighlight,
                click: zoomToFeature
            });
        }

        /*
            The style function that will be called on each GeoJSON feature.
            The fill color is dependant on the number of events for each
            feature.
         */
        function style(feature) {
            return {
                fillColor: getColor(feature.properties.event_count),
                weight: 2,
                opacity: 1,
                color: 'white',
                dashArray: '3',
                fillOpacity: 0.7
            };
        }

        /*
            Create the layer using the JSON data returned from the server
         */
        geojson = L.geoJson(json.data, {
            style: style,
            onEachFeature: onEachFeature
        });

        // save this as the global layer
        layer = geojson;

        /*
            Add the info box. This wil be used to show the basin name and events count
         */
        var info = L.control({position: 'bottomleft'});

        // on control added
        info.onAdd = function (map) {
            this._div = L.DomUtil.create('div', 'info'); // create a div with a class "info"
            this.update();
            return this._div;
        };
        // method that we will use to update the control based on feature properties passed
        info.update = function (props) {
            this._div.innerHTML = '<h2>Basin</h2>   ' + (props ?
                '<b>' + props.name + '</b><br />' + props.event_count + ' events'
                    : 'Hover over a basin');
        };

        /*
            Add the legend box. This wil be used to show color gradient with the corresponding ranges
        */
        var legend = L.control({position: 'bottomright'});

        // on control added
        legend.onAdd = function (map) {

            var div = L.DomUtil.create('div', 'info legend'),
                grades = [0, 10, 20, 50, 100, 200, 500, 1000],
                labels = [];

            // loop through our density intervals and generate a label with a colored square for each interval
            for (var i = 0; i < grades.length; i++) {
                m = L.DomUtil.create('div', 'gradient', div);
                m.innerHTML +=
                    '<i style="background:' + getColor(grades[i] + 1) + '"></i> ' +
                    grades[i] + (grades[i + 1] ? '&ndash;' + grades[i + 1] : '+');

                if (grades[i + 1]) {
                    div.innerHTML += '<br>'
                }
            }
            return div;
        };

        // add the controls to the array
        controls.push(info);
        controls.push(legend);

        // add the controls and layer to the map
        info.addTo(map);
        legend.addTo(map);
        layer.addTo(map);

        // adjust the map bounds to fit the layer
        map.fitBounds(layer.getBounds(), {padding: [10, 10]});
    };

    /**
     * Creates a HeatMap layer using Heatmap.js.
     *
     * Not fully usable since the spreading of the colors isn't quite correct
     *
     * @param {JSON} json The JSON that we will build the layer with.
     */
    var createHeatMapLayer = function(json) {
        var config = {
            // radius should be small ONLY if scaleRadius is true (or small radius is intended)
            // if scaleRadius is false it will be the constant radius used in pixels
            "radius": 2,
            "maxOpacity": .6,
            //// scales the radius based on map zoom
            "scaleRadius": true,
            // if set to false the heatmap uses the global maximum for colorization
            // if activated: uses the data maximum within the current map boundaries
            //   (there will always be a red spot with useLocalExtremas true)
            "useLocalExtrema": true,
            // which field name in your data represents the latitude - default "lat"
            latField: 'lat',
            // which field name in your data represents the longitude - default "lng"
            lngField: 'lng',
            // which field name in your data represents the data value - default "value"
            valueField: 'count'
        };

        // create the heatmap layer
        layer = new HeatmapOverlay(config);

        // add the layer to the map
        map.addLayer(layer);

        // set the data for the heatmap layer, must be done after being added to the map
        layer.setData(json.data);
    };

    /**
     * Callback function for updating the map based on the values of the form.
     */
    var updateMap = function () {
        var form = selector.find('.card-map-form form');
        var url = form.find('select[name="map_type"] option:selected').data('url');
        var type = form.find('select[name="map_type"] option:selected').val();

        $.getJSON(url, form.serialize(), function (json) {
            // remove the existing map layer, if any
            if (typeof layer !== 'undefined') {
                map.removeLayer(layer);
            }

            // remove any control layers
            $.each(controls, function (index, element) {
                map.removeControl(element);
            });

            // empty the controls array
            controls = [];

            // create the appropriate layers based on the map type selected
            if (type == 'Basins') {
                createBasinLayer(json);
            } else if (type == 'Points') {
                createPointsLayer(json);
            } else if (type == 'Heatmap') {
                createHeatMapLayer(json);
            }
        });
    };

    /**
     * We are intercepting the submission of the form.
     *
     * We are going to go the submitting and handling the response in the updateMap() function.
     */
    selector.find('.card-map-form form').on('submit', function (event) {
        event.preventDefault();
        updateMap();
    });

    /**
     * Fetched the species count information via JSON and display the results in
     * the existing table.
     *
     * Will show a default message if there is not information for the selected
     * time period.
     *
     * @param {jQuery} selector The selector that contains the form as a child element
     */
    var updateSpeciesTable = function (selector) {
        var self = selector.closest('.card-species');
        var form = self.find('form');
        var url = self.data('url');

        $.getJSON(url, form.serialize(), function (json) {
            self.find('h3').text(json.data.title);
            var tbody = self.find('tbody');

            tbody.empty();

            if (json.data.items.length > 0) {

                $.each(json.data.items, function (index, element) {
                    var tr = $('<tr>');
                    tr.append($('<td>', {html: element.name}));
                    tr.append($('<td>', {html: element.count}));
                    tbody.append(tr);
                });

                var tr = $('<tr>', {class: 'total'});
                tr.append($('<td>', {html: 'Total'}));
                tr.append($('<td>', {html: json.data.total}));
                tbody.append(tr);

                self.find('table').show('fade');
            } else {
                self.find('.placeholder').show('fade');
            }

            self.find('.loading').removeClass('animate').hide();

        });
    };

    /**
     * We are intercepting the submission of the form.
     *
     * We are going to go the submitting and handling the response with the
     * updateSpeciesTable() function.
     */
    selector.find('.card-species form').on('submit', function (event) {
        event.preventDefault();

        var card = $(this).closest('.card-species');

        card.find('table').hide();
        card.find('.placeholder').hide();
        card.find('.loading').addClass('animate').show();

        updateSpeciesTable($(this));
    });

    /**
     * Adding a 'change' listener for the date selects in the species card.
     * There are hidden form field that get updated with the correct time
     * allowing the form to submit with the correct data
     */
    selector.find('.card-species form select').on('change', function (event) {
        var form = $(this).closest('form');
        var year = form.find('.year-field').val();
        var month = form.find('.month-field').val();
        var date = Date.create(month + ' ' + year);

        // use Sugar.js to create the begging and ending datetimes for the selected
        // month and year combination.
        form.find('.start-date-field').val(date.beginningOfDay().toISOString());
        form.find('.end-date-field').val(date.endOfDay().toISOString());
    });

    /**
     * Add a 'click' event listener to the reste button on the species count card forms.
     *
     * Updates the hidden inputs back to the initial values and resubmits the form.
     */
    selector.find(".card-species form button[type='reset']").on("click", function(event){
        var form = $(this).closest('form');

        form.find('input[data-default]').each(function(){
            $(this).val($(this).data('default'));
        });

        form.submit();
    });

    /*
        Submit the species counts forms so they will have their intial data.
     */
    selector.find('.card-species form').submit();
});
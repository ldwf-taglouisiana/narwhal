/**
 * Coordinate Conversion Object
 * @typedef {Object} Narwhal.Coordinate
 * @property {Object} 'D' The coordinate converted to degrees
 * @property {Number} 'D'.degrees The numerical version of the value rounded to 4 decimals
 * @property {String} 'D'.string The string representation of the conversion type
 * @property {Object} 'DM' The coordinate converted to degrees minutes
 * @property {Number} 'DM'.degrees The numerical version of the degrees rounded to 0 decimals
 * @property {Number} 'DM'.minutes The numerical version of the minutes rounded to 4 decimals
 * @property {String} 'DM'.string The string representation of the conversion type
 * @property {Object} 'DMS' The coordinate converted to degrees seconds
 * @property {Number} 'DMS'.degrees The numerical version of the degrees rounded to 0 decimals
 * @property {Number} 'DMS'.minutes The numerical version of the minutes rounded to 0 decimals
 * @property {Number} 'DMS'.seconds The numerical version of the seconds rounded to 2 decimals
 * @property {String} 'DMS'.string The string representation of the conversion type
 * @property {String} 'NOGPS' An empty string representing no value
 */

/**
 * Takes a coordinate and create the needed formats in:
 *  Decimal
 *  Degrees Minutes
 *  Degrees Minutes Seconds
 *  NOGPS
 *
 * We can then use these values to update the form elements.
 *
 * @param {String|Number} value The coordinate to be converted
 * @returns {Narwhal.Coordinate} The results object containing the various forms of the inputed coordinate
 */
function capturesFromConvertCoordinates(value) {
    value = parseFloat(value);

    /*
        Working variables for the computations later on
     */
    var decimal = 0;
    var minutes = 0;
    var seconds = 0;
    var string = '';

    var result = {};

    /*
        Create the Decimal portion of the result hash
     */
    decimal = value.round(4);

    decimal = (Math.abs(decimal) * (decimal / Math.abs(decimal))) || null;

    string = decimal;

    result['D'] = {
        degrees: isNaN(decimal) ? '' : decimal,
        string: string
    };

    /*
        Create the Decimal Minutes portion of the result hash
     */
    decimal = Math.floor(Math.abs(value));
    minutes = ( Math.abs(value) - Math.abs(decimal) ) * 60;

    decimal = Math.abs(decimal) * (value / Math.abs(value));
    minutes = minutes.round(4);

    string = decimal + "° " + minutes + "'";

    result['DM'] = {
        degrees: isNaN(decimal) ? '' : decimal,
        minutes: isNaN(minutes) ? '' : minutes,
        string: string
    };

    /*
        Creates the Decimal Minutes Seconds portion of the result hash
    */
    decimal = Math.floor(Math.abs(value));
    minutes = ( Math.abs(value) - Math.abs(decimal) ) * 60;
    seconds = ( Math.abs(minutes) - Math.floor(minutes) ) * 60;

    decimal = Math.abs(decimal) * (value / Math.abs(value));
    minutes = Math.floor(minutes);
    seconds = seconds.round(2);

    /*
     * This is a check to prevent the seconds being equal to 60.
     * Which is equivalent to an extra minute.
     *
     * Best I can figure is this is due to a rounding error.
     * Ex. 29° 31" -> 29° 30" 60'
     */
    if (seconds == 60.0) {
        minutes += 1;
        seconds = 0;
    }

    string = decimal + "° " + minutes + "' " + seconds + "''";

    result['DMS'] = {
        degrees: isNaN(decimal) ? '' : decimal,
        minutes: isNaN(minutes) ? '' : minutes,
        seconds: isNaN(seconds) ? '' : seconds,
        string: string
    };

    /*
        Creates the NOGPS portion of the result hash
     */
    result['NOGPS'] = {
        string: ""
    };

    return result;
}

/**
 * Update the coordinate fields when the coordinate type is updated.
 * Hides and shows the correct number of fields based on the selection.
 *
 * @param {jQuery} selector The selector in the form that the change in being called on.
 */
function capturesFormUpdateCoordinateFields(selector) {
    var form = selector.closest('form');
    var GPStype = form.find('.coordinate-type-fields input:checked').val();

    if (GPStype == "D") {
        form.find(".coordinate-field-group").find('.minutes, .seconds').hide();
    }

    if (GPStype == "DM") {
        form.find(".coordinate-field-group").find('.minutes').show();
        form.find(".coordinate-field-group").find('.seconds').hide();
    }

    if (GPStype == "DMS") {
        form.find(".coordinate-field-group").find('.minutes, .seconds').show();
    }

    form.find(".coordinate-field-group").show();
    form.find(".basin-field-group").hide();

    if (GPStype == "NOGPS") {
        form.find('.degrees, .minutes, .seconds, .coordinate-input').val('');

        form.find(".coordinate-field-group").hide();
        form.find(".basin-field-group").show();
        //var markerLayer = mapObjects[$(this).closest('.card').attr('id')].layer;
        //markerLayer.clearLayers();
    }

    var latitudeField = $('.latitude-group .coordinate-input');
    var longitudeField = $('.longitude-group .coordinate-input');
    capturesFormUpdateCoordinateFieldValues(latitudeField, parseFloat(latitudeField.val()));
    capturesFormUpdateCoordinateFieldValues(longitudeField, parseFloat(longitudeField.val()));
}

function capturesFormUpdateCoordinateFieldValues(field, value) {
    var form = field.closest('form');
    var group = field.closest('.coordinate-field-group');
    var parsedCoordinates = capturesFromConvertCoordinates(value);
    var gpsType = form.find('.coordinate-type-fields input:checked').val();


    group.find('.coordinate-input').val(value);
    group.find('.degrees').val(parsedCoordinates[gpsType].degrees);
    group.find('.minutes').val(parsedCoordinates[gpsType].minutes);
    group.find('.seconds').val(parsedCoordinates[gpsType].seconds);
}

function capturesFormFieldsToDegrees(selector) {
    var d = parseFloat(selector.find('.degrees').val()) || 0;
    var m = parseFloat(selector.find('.minutes').val()) || 0;
    var s = parseFloat(selector.find('.seconds').val()) || 0;

    var fraction = ( (m * 60) + s ) / 3600;
    var value = ((Math.abs(d) + fraction) * (d / Math.abs(d)));

    return isNaN(value) ? '' : value;
}
onReady($('.captures .show'), function(selector){
    capturesShowInit(selector);
});

function capturesShowInit(selector) {
    // This will hold all the map objects for each of the tab panes
    var maps = [];

    /*
     When we click on a new tab, we hook into the event so we can tell
     the map to redraw, since there is an issue with rendering the map
     when the map is not visible.
     */
    selector.find('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
        // use the target tab to look up the tab pane
        var tab = selector.find('div' + $(e.target).attr('href') + '.tab-pane');
        // get the map form the array
        var map = maps[tab.find('.id').val()];

        // if map that we pulled out of the array us is defined, then tell it to redraw.
        if (typeof map !== 'undefined') {
            map._onResize();
        }
    });

    /*
     For each of the tabs we need to create the leaflet map,
     and to add the map features.
     */
    selector.find('.tab-pane').each(function(){
        var tab = $(this);
        var map = createLeafletMap(tab.find('.map'));

        /*
         Hit the route in the data-path attribute to fetch the
         geoJSON elements to render.
         */
        $.getJSON(tab.data('path'), function(json){
            // ignore if the data property is null
            if (json.data != null) {
                // create the layer, TODO add the right coloring for the shape
                var layer = L.geoJson(json.data, {});
                // add the layer to the map
                map.addLayer(layer);
                // center the map to the center of the layer
                map.panTo(layer.getBounds().getCenter());
            }
        });

        // add the map object to teh
        maps[tab.find('.id').val()] = map;
    });
}
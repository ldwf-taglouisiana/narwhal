onReady($('body.captures .index'), function(selector){

  capturesIndexInitCapturesTable(selector);
  capturesIndexInitUnverifiedCapturesTable(selector, '/captures');

  capturesIndexInitRecapturesTable(selector);
  capturesIndexInitUnverifiedRecapturesTable(selector, '/captures');

  capturesIndexInitDraftCapturesTable(selector,'/draft_captures.json', '/captures');

  selector.find('.check-all-button').click(function () {
      $(this).closest('.tab-pane').find('.verify-capture-checkbox').prop('checked', true);
  });

  selector.find('.unCheck-all-button').click(function () {
      $(this).closest('.tab-pane').find('.verify-capture-checkbox').prop('checked', false);
  });

});

function capturesIndexInitCapturesTable(selector) {
    var rows_selected = [];

    var dataTableParams = {
        searching: true,
        lengthChange: true,
        autoWidth: true,
        processing: true,
        serverSide: true,
        order: [12, 'desc'],
        ajax: {
            url: "/captures.json",
            data: function (d) {
                d.verified = true;
            }
        },
        columns: [
            { // verify checkbox
                data: "id",
                orderable: false,
                className: 'text-center',
                searchable: false
            },
            {data: "angler_number"},
            {data: "capture_angler"},
            {data: "tag_number"},
            {
                data: "date_string",
                searchable: false
            },
            {
                data: "species_name",
                searchable: false
            },
            {
                data: "length",
                searchable: false
            },
            {
                data: "latitude_formatted",
                searchable: false
            },
            {
                data: "longitude_formatted",
                searchable: false
            },
            {
                data: "recapture_count",
                className: 'text-center',
                searchable: false,
                render: function (data, type, full, meta) {
                    return data > 0 ? '<i class="fa fa-check"></i>' : '';
                }
            },
            {
                data: "time_of_day",
                searchable: false,
                render: function (data, type, full, meta) {
                    return data ? data.replace(/\s\(.*/g, "") : '';
                }
            },
            {
                data: "entered",
                searchable: false,
                render: function (data, type, full, meta) {
                    return dateFormatfull(data);
                }
            },
            {
                data: "has_images",
                className: 'text-center',
                searchable: false,
                render: function (data, type, full, meta) {
                    return data ? '<i class="fa fa-picture-o"></i>' : '';
                }
            },
            {
                data: "id",
                orderable: false,
                searchable: false,
                className: 'text-center',
                render: function (data, type, full, meta) {
                    return '<a href="/captures/' + data + '" target="_blank"><i class="fa fa-external-link"></i></a>';
                }
            }
        ]
    };

  var tabSelector = selector.find('#captureIndexTable').closest('.tab-pane');

  // combine the index params and the params for the datatable check box
  var settings = $.extend({}, dataTableParams, dataTableOptions(rows_selected));

  // create the datatable object
  var newSelect = selector.find('#captureIndexTable');
  var dataTable = newSelect.DataTable(settings);

  // initialize the checkboxes for the datatable
  dataTableInitCheckBoxes(newSelect, dataTable, rows_selected);

  // adds a click listener to the checkboxes so we can show/hide the actions
  tabSelector.on('change', 'tbody input[type="checkbox"]', function() {
    if (tabSelector.find('tbody input[type="checkbox"]:checked').length > 0) {
      tabSelector.find('.table-actions button, .table-actions input').show();
    } else {
      tabSelector.find('.table-actions button, .table-actions input').hide();
    }
  });

  // the listener to start the download
  tabSelector.find('.table-actions button.btn-download').on('click', function(){
    var all = newSelect.find('#all').is(':checked');
    startAsyncDownload(Routes.download_captures_path({options: rows_selected, select_all: all, verified: 'true'}))
  });

}

function capturesIndexInitUnverifiedCapturesTable(selector, redirectUrl) {
  var rows_selected = [];

  var dataTableParams = {
        searching: true,
        destroy: true,
        lengthChange: true,
        autoWidth: true,
        processing: true,
        serverSide: true,
        order: [ 13, 'asc' ],
        ajax: {
            url: "/captures.json",
            data: function ( d ) {
                d.verified = false;
            }
        },
        columns: [
            { // verify checkbox
              data: "id",
              orderable: false,
              className : 'text-center'
            },
            { data: "angler_number" },
            { data: "capture_angler" },
            { data: "tag_number" },
            { data: "date_string" },
            { data: "species_name" },
            { data: "length" },
            { data: "latitude_formatted" },
            { data: "longitude_formatted" },
            {
                data: "recapture_count",
                className : 'text-center',
                render: function ( data, type, full, meta ) {
                    return data > 0 ? '<i class="fa fa-check"></i>' : '';
                }
            },
            {
                data: "time_of_day",
                render: function (data, type, full, meta) {
                    return data ? data.replace(/\s\(.*/g, "") : '';
                }
            },
            { data: "entered",
              render: function( data, type, full, meta){
                return dateFormatfull(data);
              }
            },
            {
                data: "has_images",
                className: 'text-center',
                searchable: false,
                render: function (data, type, full, meta) {
                    return data ? '<i class="fa fa-picture-o"></i>' : '';
                }
            },
            {
                data: "id",
                orderable: false,
                className : 'text-center',
                render: function ( data, type, full, meta ) {
                    return '<a href="/captures/' + data + '" target="_blank"><i class="fa fa-external-link"></i></a>';
                }
            }
        ]
    };

  var tabSelector = selector.find('#unverifiedCaptureIndexTable').closest('.tab-pane');


  // combine the index params and the params for th datatable check box
  var settings = $.extend({}, dataTableParams, dataTableOptions(rows_selected));

  // create the datatable object
  var newSelect = selector.find('#unverifiedCaptureIndexTable');
  var dataTable = newSelect.DataTable(settings);

  // initialize the checkboxes for the datatable
  dataTableInitCheckBoxes(newSelect, dataTable, rows_selected);

  capturesIndexInitVerifyCheckbox(tabSelector, rows_selected, dataTable, redirectUrl);

  // adds a click listener to the checkboxes so we can show/hide the actions
  tabSelector.on('change', 'tbody input[type="checkbox"]', function() {
    if (tabSelector.find('tbody input[type="checkbox"]:checked').length > 0) {
      tabSelector.find('.table-actions button, .table-actions input').show();
    } else {
      tabSelector.find('.table-actions button, .table-actions input').hide();
    }
  });

  // the listener to start the download
  tabSelector.find('.table-actions button.btn-download').on('click', function(){
    var all = newSelect.find('#all').is(':checked');
    startAsyncDownload(Routes.download_captures_path({options: rows_selected, select_all: all}))
  });

}

function capturesIndexInitRecapturesTable(selector) {
  var rows_selected = [];

  var dataTableParams = {
        searching:    true,
        lengthChange: true,
        autoWidth:    true,
        processing: true,
        serverSide: true,
        order: [ 12, 'desc' ],
        ajax: {
            url : "/recaptures.json",
            data: function ( d ) {
                d.verified = true;
            }
        },
        columns: [
            { // verify checkbox
              data: "id",
              orderable: false,
              className : 'text-center'
            },
            { data: "recapture_angler_number" },
            { data: "recapture_angler_name" },
            { data: "tag_number" },
            { data: "date_string" },
            { data: "species_name" },
            { data: "length" },
            { data: "latitude" },
            { data: "longitude" },
            { data: "recapture_disposition" },
            {
              data: "time_of_day",
              render: function (data, type, full, meta) {
                return data ? data.replace(/\s\(.*/g, "") : '';
              }
            },
            { data: "entered",
              render: function( data, type, full, meta){
                return dateFormatfull(data);
              }
            },
            {
                data: "has_images",
                className: 'text-center',
                searchable: false,
                render: function (data, type, full, meta) {
                    return data ? '<i class="fa fa-picture-o"></i>' : '';
                }
            },
            {
                data: "id",
                orderable: false,
                className : 'text-center',
                render: function ( data, type, full, meta ) {
                    return '<a href="/recaptures/' + data + '" target="_blank"><i class="fa fa-external-link"></i></a>';
                }
            }
        ]
    };
  var tabSelector = selector.find('#recaptureIndexTable').closest('.tab-pane');


  // combine the index params and the params for th datatable check box
  var settings = $.extend({}, dataTableParams, dataTableOptions(rows_selected));

  // create the datatable object
  var dataTable = selector.find('#recaptureIndexTable').DataTable(settings);
  var newSelect = selector.find('#recaptureIndexTable');

  // initialize the checkboxes for the datatable
  dataTableInitCheckBoxes(newSelect, dataTable, rows_selected);

  // adds a click listener to the checkboxes so we can show/hide the actions
  tabSelector.on('change', 'tbody input[type="checkbox"]', function() {
    if (tabSelector.find('tbody input[type="checkbox"]:checked').length > 0) {
      tabSelector.find('.table-actions button, .table-actions input').show();
    } else {
      tabSelector.find('.table-actions button, .table-actions input').hide();
    }
  });

  // the listener to start the download
  tabSelector.find('.table-actions button.btn-download').on('click', function(){
    var all = newSelect.find('#all').is(':checked');
    startAsyncDownload(Routes.download_recaptures_path({options: rows_selected, select_all: all, verified: 'true'}))
  });
}

function capturesIndexInitUnverifiedRecapturesTable(selector, redirectUrl) {

  var rows_selected = [];

  var dataTableParams = {
      searching:    true,
      lengthChange: true,
      autoWidth: true,
      processing: true,
      serverSide: true,
      order: [ 12, 'desc' ],
      ajax: {
          url: "/recaptures.json",
          data: function ( d ) {
              d.verified = false;
          }
      },
      columns: [
          { // verify checkbox
            data: "id",
            orderable: false,
            className : 'text-center'
          },
          { data: "recapture_angler_number" },
          { data: "recapture_angler_name" },
          { data: "tag_number" },
          { data: "date_string" },
          { data: "species_name" },
          { data: "length" },
          { data: "latitude" },
          { data: "longitude" },
          { data: "recapture_disposition" },
          {
            data: "time_of_day",
            render: function (data, type, full, meta) {
              return data ? data.replace(/\s\(.*/g, "") : '';
            }
          },
          { data: "entered",
            render: function( data, type, full, meta){
              return dateFormatfull(data);
            }
          },
          {
              data: "has_images",
              className: 'text-center',
              searchable: false,
              render: function (data, type, full, meta) {
                  return data ? '<i class="fa fa-picture-o"></i>' : '';
              }
          },
          {
              data: "id",
              orderable: false,
              className : 'text-center',
              render: function ( data, type, full, meta ) {
                  return '<a href="/recaptures/' + data + '" target="_blank"><i class="fa fa-external-link"></i></a>';
              }
          }
      ]
  };

  var tabSelector = selector.find('#unverifiedRecaptureIndexTable').closest('.tab-pane');


  // combine the index params and the params for th datatable check box
  var settings = $.extend({}, dataTableParams, dataTableOptions(rows_selected));

  // create the datatable object
  var newSelect = selector.find('#unverifiedRecaptureIndexTable');
  var dataTable = newSelect.DataTable(settings);

  // initialize the checkboxes for the datatable
  dataTableInitCheckBoxes(newSelect, dataTable, rows_selected);

  capturesIndexInitVerifyCheckbox(tabSelector, rows_selected, dataTable, redirectUrl);


  // adds a click listener to the checkboxes so we can show/hide the actions
  tabSelector.on('change', 'tbody input[type="checkbox"]', function() {
    if (tabSelector.find('tbody input[type="checkbox"]:checked').length > 0) {
      tabSelector.find('.table-actions button, .table-actions input').show();
    } else {
      tabSelector.find('.table-actions button, .table-actions input').hide();
    }
  });

  // the listener to start the download
  tabSelector.find('.table-actions button.btn-download').on('click', function(){
    var all = newSelect.find('#all').is(':checked');
    startAsyncDownload(Routes.download_recaptures_path({options: rows_selected, select_all: all}))
  });
}

function capturesIndexInitDraftCapturesTable(selector,url,redirectUrl) {
  var rows_selected = [];

  var dataTableParams = {
        searching:    true,
        lengthChange: true,
        autoWidth:    true,
        processing:   true,
        serverSide:   true,
        deferRender:  true,
        order: [ 12, 'desc' ],
        ajax: {
            url : url
        },
        columns: [
            { // verify checkbox
              data: "id",
              orderable: false,
              className : 'text-center'
            },
            { data: "angler_number" },
            { data: "angler_full_name" },
            { data: "tag_number" },
            { data: "capture_date",
                render: function( data, type, full, meta){
                    return dateFormatMDY(data);
                }
            },
            { data: "species_name" },
            { data: "species_length" },
            { data: "latitude" },
            { data: "longitude" },
            {
                data: "recapture",
                className : 'text-center',
                render: function ( data, type, full, meta ) {
                    return data ? '<i class="fa fa-check"></i>' : '';
                }
            },
            { data: "recapture_disposition_id", className : 'text-center' },
            { data: "created_at",
              render: function( data, type, full, meta){
                return dateFormatfull(data);
              }
            },
            { // status
                data: "status",
                orderable: false,
                className : 'text-center',
                defaultContent: 'Loading' ,
                render: function ( data, type, full, meta ) {

                    if (full.errors.length > 0) {
                        data.message = data.message + '<br><span style=\'color: red\'>' + full.errors.join('<br>') + '<span>'
                    }

                    return '<i class="fa ' + data.icon + '" data-toggle="popover" data-container="body" data-placement="left" data-html="true" data-content="' + data.message + '" style="cursor: pointer;"></i>'
                }
            },
            {
                data: "has_image",
                className: 'text-center',
                searchable: false,
                render: function (data, type, full, meta) {
                    return data ? '<i class="fa fa-picture-o"></i>' : '';
                }
            },
            { // open link
                data: "id",
                orderable: false,
                className : 'text-center',
                render: function ( data, type, full, meta ) {
                    return '<a href="' + Routes.draft_capture_path(data) + '" target="_blank"><i class="fa fa-external-link"></i></a>';
                }
            }
        ]
    };

    selector.find('#draftCaptureIndexTable').on( 'draw.dt', function () {
        $(this).find('[data-toggle="popover"]').popover({
            trigger: 'hover'
        });
    });


  var tabSelector = selector.find('#draftCaptureIndexTable').closest('.tab-pane');


  // combine the index params and the params for th datatable check box
  var settings = $.extend({}, dataTableParams, dataTableOptions(rows_selected));

  // create the datatable object
  var newSelect = selector.find('#draftCaptureIndexTable');
  var dataTable = newSelect.DataTable(settings);

  // initialize the checkboxes for the datatable
  dataTableInitCheckBoxes(newSelect, dataTable, rows_selected );

  //init the click listeners for the move to capture and recapture and delete buttons
  capturesIndexInitMoveAndDelteCheckbox(tabSelector, rows_selected, dataTable, redirectUrl);

  // adds a click listener to the checkboxes so we can show/hide the actions
  tabSelector.on('change', 'tbody input[type="checkbox"]', function() {
    if (tabSelector.find('tbody input[type="checkbox"]:checked').length > 0) {
      tabSelector.find('.table-actions button').show();
    } else {
      tabSelector.find('.table-actions button').hide();
    }
  });

  // the listener to start the download
  tabSelector.find('.table-actions button.btn-download').on('click', function(){
    var all = newSelect.find('#all').is(':checked');
    startAsyncDownload(Routes.download_draft_captures_path({options: rows_selected, select_all: all}))
  });
}

/*
  This creates click listeners for the verify buttons on the unverified pages
 */
function capturesIndexInitVerifyCheckbox(selector, selected_rows, dataTable, redirectUrl){
  selector.find('button.post-button').on('click', function(){
    $.post($(this).data('path'), {options : selected_rows, redirect_url: redirectUrl}, function(){
      reloadTable(selector,dataTable,selected_rows);
      reloadAllTable();
    });
  });

}

/*
  Initializes the click events for each of the move and delete buttons on the index pages
 */
function capturesIndexInitMoveAndDelteCheckbox(selector, selected_rows, dataTable, redirectUrl){
  //find the button to bind the click event to
  //This covers the move button
  selector.find('button.post-button').on('click', function(){

    $.post($(this).data('path'), {options : selected_rows, redirectUrl: redirectUrl})

        .done(function(){
          reloadTable(selector,dataTable,selected_rows);
          reloadAllTable();
        })
        .fail(function(){

        });
  });

  //the delete button's listener
  selector.find('button.delete-button').on('click', function(){
    $.ajax({
      method: "DELETE",
      url: $(this).data('path'),
      data: {options : selected_rows, redirectUrl: redirectUrl}
    })
        .done(function(){
          reloadTable(selector,dataTable,selected_rows);
        })
        .fail(function(){
        });
  })

}

/*
Function that is called whenever an ajax action succeeds that wouldn't cause a redirect
Hides the buttons, empties the array of selected items and refreshes the dataTable view
 */
function reloadTable(selector, dataTable, selected_rows){
  selector.find('.table-actions button, .table-actions input').hide();
  selected_rows.splice(0,selected_rows.length);
  dataTable.draw();
}

function reloadAllTable() {
    $('.tab-pane table').each(function(){ $(this).dataTable().fnDraw() });
}
onReady($('.captures form.fish-entry'), function(selector){
    capturesFormInit(selector);

    /*
        If th recapture box is clicked then package up the form values
        and navigate to the recapture form.
     */
    selector.find('input[type=checkbox]#recapture_box').change(function(){
        selector.find('input[type=hidden]#recapture_box').val($(this).prop('checked') ? '1' : '0');

        if ($(this).prop('checked')) {
            var params = { fish_entry: selector.serializeJSON().capture };
            Turbolinks.visit(Routes.new_recapture_path(params));
        }
    });
});

onUnload($('.captures form.fish-entry'), function(selector){
    selector.parsley('destroy');
});

function capturesFormInit(selector,blankAnglerName){
    // create the map
    var map = createLeafletMap(selector.find('.map'));
    // create the working layer for the map
    var markerLayer = new L.LayerGroup();

    /**
     * A click event handler for the event.
     * Will add a pin to the map at the click point.
     * Will update the coordinate values and fields with the LatLng from the click event.
     *
     * @param [MouseEvent] e A leaflet.js mouse event.
     */
    var onMapClick = function(e) {
        markerLayer.clearLayers();

        markerLayer.addLayer(new L.Marker(e.latlng));
        map.panTo(e.latlng);

        capturesFormUpdateCoordinateFieldValues(selector.find('.latitude-group .coordinate-input'), e.latlng.lat);
        capturesFormUpdateCoordinateFieldValues(selector.find('.longitude-group .coordinate-input'), e.latlng.lng);
    };

    /**
     * Reads the current latitude and longitude values on the form and updates the
     * map with the correct marker if the longitude and latitude values are not empty.
     */
    var adjustMapToEnteredLocation = function() {
        var latitude = selector.find('.latitude-group .coordinate-input').val();
        var longitude = selector.find('.longitude-group .coordinate-input').val();

        // if the latitude and longitude are parsabel into floats then update the map
        if (!isNaN(parseFloat(latitude)) && !isNaN(parseFloat(longitude))) {
            var latlng = new L.LatLng(latitude, longitude);
            markerLayer.clearLayers();
            markerLayer.addLayer(new L.Marker(latlng));
            map.panTo(latlng);
        }
    };

    /**
     * Create the shape layer with the given geoJSON object and the fieldSelector.
     * Updates the map with the created shape layer.
     *
     * @param [geoJSON] json The geoJSON containing the shape information
     * @param [jQuery] fieldSelector The jQuery selector for the basin/subbasin selector
     */
    var createShapeLayer = function (json, fieldSelector) {
        fieldSelector.refresh();
        markerLayer.clearLayers();
        var shapeLayer;

        /*
             Used as the mouseover for the basins in this layer.
             Updates the style of the shape, as well as the info control
         */
        function highlightFeature(e) {
            if (fieldSelector.val() != e.target.feature.properties.id.toString()) {
                var layer = e.target;

                layer.setStyle({
                    weight: 5,
                    color: '#FF7800',
                    dashArray: '',
                    fillOpacity: 0.7
                });

                if (!L.Browser.ie && !L.Browser.opera) {
                    layer.bringToFront();
                }
            }
        }

        /*
            Reset the shape styles when you mouseout, unless the feature is
            the one currently selected.
         */
        function resetHighlight(e) {
            if (fieldSelector.val() != e.target.feature.properties.id.toString()) {
                shapeLayer.resetStyle(e.target);
            }
        }

        /*
            When a layer is selected, then we set the values of the fieldSelector,
            then trigger the change event for the fieldSelector.
         */
        function onClickFeature(e) {
            shapeLayer.eachLayer(function(l){shapeLayer.resetStyle(l);});

            var layer = e.target;
            var id = layer.feature.properties.id;

            fieldSelector.val(id).trigger("chosen:updated").trigger('change');

            layer.setStyle({
                fillColor: '#C74736',
                color: '#DB331D',
                dashArray: '',
                fillOpacity: 0.7
            });
        }

        /*
             This is the each feature function that will be called
             on each GeoJSON feature.
         */
        function onEachFeature(feature, layer) {
            layer.on({
                mouseover: highlightFeature,
                mouseout: resetHighlight,
                click: onClickFeature
            }).bindLabel(feature.properties.name);
        }

        /*
             The style function that will be called on each GeoJSON feature.
             The fill color is dependant on the number of events for each
             feature.
         */
        function style(feature) {
            return {
                fillColor: '#ff7800',
                weight: 2,
                opacity: 1,
                color: 'white',
                dashArray: '3',
                fillOpacity: 0.5
            };
        }

        /*
            Create the layer using the JSON data returned from the server
         */
        shapeLayer = L.geoJson(json.data, {
            style: style,
            onEachFeature: onEachFeature
        });

        // add the shape layer to the  working layer
        markerLayer.addLayer(shapeLayer);

        // adjust the map bounds to fit the layer
        map.fitBounds(shapeLayer.getBounds(), {padding: [10, 10]});
    };

    // add the working layer to the map
    map.addLayer(markerLayer);

    // set the click action up for the map
    map.on('click', onMapClick);

    /*
        Bind a change listener to the coordinate type radio buttons.

        NOGPS option will enable the basin/subbasin selects, as well as clear
        out any coordinate values. The map will be updated to show the basin
        shapes.

        The D/DM/DMS options will show the appropriate coordinate components for
        the selected type. the entered coordinates will be broken/assembled into
        the right fields on change.

        On init, we trigger the change on the field to do the initial configuration.
     */
    selector.find('.coordinate-type-fields input[type=radio]').change(function(){
        // update the coordinate fields
        capturesFormUpdateCoordinateFields(selector);

        // empty the map layer
        markerLayer.clearLayers();

        // if the checked value is 'NOGPS', then enable the basin/subbasin selects
        if (selector.find('.coordinate-type-fields input:checked').val() == 'NOGPS') {
            // disable the map click listener
            map.off('click', onMapClick);
            // clear the map
            markerLayer.clearLayers();

            // get the basin shape geoJSON and create the shape layer
            $.getJSON(Routes.basin_sub_basin_basins_path(), function(json) {
                createShapeLayer(json, selector.find('.basin-field'));
            });

        } else { // if the checked value is D/DM/DNS, then rebind the map click listener
            // unbind
            map.off('click', onMapClick);
            // rebind
            map.on('click', onMapClick);
        }

        // set the select values to null, and tell chosen to update with the values
        selector.find('.basin-field').val(null).trigger("chosen:updated");
        selector.find('.subbasin-field').val(null).trigger("chosen:updated");

    }).trigger('change');

    /*
        This will flip the degree of the longitude degrees field to negative
     */
    selector.find('.longitude-group .degrees').on('focusout', function(){
        if (!isNaN(parseFloat($(this).val()))) {
            var value = parseFloat($(this).val());

            if (value > 0.0) {
                $(this).val(value * -1);
            }
        }
    });

    /*
        Adding a focusout to all coordinate fields to update the hidden
        value field, which stores the coordinate in full decimal.

        Also after the focusout, the map will be updated to the coordinates.
     */
    selector.find('.coordinate-group .field').on('focusout', function(){
        var value = capturesFormFieldsToDegrees($(this).closest('.coordinate-field-group'));
        capturesFormUpdateCoordinateFieldValues($(this), value);

        adjustMapToEnteredLocation();
    });

    /*
        When the species value changes, we need to update the species
        length options to those that match the selected species.

        On init, trigger the change to enable the species lengths if any.
     */
    selector.find('.species-field').on('change', function(){
        var val = $(this).val();
        var selectField = selector.find('.species-length-field');

        // enable all the options, then disable the options that do not correspond the selected species
        selectField.val(null)
            .find('option')
            .removeAttr('disabled')
            .not('[data-species="' + val + '"]')
            .prop('disabled', 'disabled');

        // update chosen to reflect the changes
        selectField.trigger("chosen:updated");
    }).trigger('change');

    /*
        When the basin drop down is changed, we will enable the subbasin options
        and update the map with the shapes for the appropriate subbasins.
     */
    selector.find('.basin-field').on('change', function() {
        var val = $(this).val();
        var selectField = selector.find('.subbasin-field');

        if (val.length > 0) {
            // enable all options, then disable the options,that do not have the right basin id.
            selectField.find('option')
                .removeAttr('disabled')
                .not('[data-basin="' + val + '"]')
                .prop('disabled', 'disabled');
        }

        // tell chosen to update with the new values
        selectField.trigger("chosen:updated");

        // redraw the shape layer using the correct subbasin shapes
        $.getJSON(Routes.basin_sub_basin_get_sub_basins_path({basin_gid: val}), function(json) {
            createShapeLayer(json, selector.find('.subbasin-field'));
        });
    });

    /*
        We are binding a focusout to the angler id field, so we can update the angler name,
        when we leave the field. We will trigger this on init as well.
     */
    selector.find('.angler-id-field').on('focusout', function(){
        var self = $(this);
        $.getJSON(Routes.api_v2_internal_angler_from_angler_id_path(), { angler_id: self.val() }, function(json) {
            // if the angler is null, then clear the field
            if (json.angler == null && blankAnglerName) {
                self.closest('form').find('.angler-name-field').val('');
            } else { // if we got an angler back, update the name field
                self.closest('form').find('.angler-name-field').val(json.angler.first_name + " " + json.angler.last_name);
                self.closest('form').find('.hidden-angler-id').val(json.angler.id);
            }
        });
    }).focusout();

    /*
        We are binding a focusout so when the tag number is updated, we can update
        the angler id/name with the angler that is assigned to the given tag.
     */
    selector.find('.tag-number-field').on('focusout', function() {
        var self = $(this);

        $.getJSON(Routes.api_v2_internal_angler_from_tag_number_path(), {tag_number: self.val()}, function (json) {
            // if the angler is null, then clear the angler id/name fields

            if (json.tag.id != null) {
                self.closest('form').find('.tag-id-field').val(json.tag.id);
                self.closest('form').find('.tag-number-field').val(json.tag.tag_no);
            }

            // if we are not on an editing page, then we can autocomplete the angler id
          //TODO 11/14/17 This is garbage and needs to be changed why are we checking the url? just use a hidden field in the view
            if (window.location.href.indexOf("edit") == -1) {
              if (json.angler != null && selector.find('.angler-id-field').val().length == 0 && !$("#recapture_box.checkbox").prop('checked'))  {
                  self.closest('form').find('.angler-id-field').val(json.angler.angler_id);
                  self.closest('form').find('.hidden-angler-id').val(json.angler.id);
                  self.closest('form').find('.angler-name-field').val(json.angler.first_name + " " + json.angler.last_name);
              }
            }
        });
    });

    /*
        We need to set the angler id/name based on the tag value, but we should only
        do this on init, if the angler id is empty
     */
    if (selector.find('.angler-id-field').val().length == 0) {
        selector.find('.tag-number-field').focusout();
    }

    /*
        initialize parsley with the default config, defined in common/parsley.js
     */
    selector.parsley(PARSLEY_CONFIG);

    /*
        Trigger the map update, if any values are prefilled
     */
    adjustMapToEnteredLocation();
}
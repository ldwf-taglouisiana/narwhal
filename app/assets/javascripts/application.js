// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
// Gem libs
//= require turbolinks
//= require js-routes
//= require jquery_ujs
//= require autocomplete-rails
//= require websocket_rails/main
//= require nprogress
//= require nprogress-turbolinks
//= require unobtrusive_flash
//= require unobtrusive_flash_bootstrap
// jQuery libs
//= require jquery/jquery.bootstrap.waiting-dialog
//= require jquery/jquery.datetimepicker
//= require jquery/jquery.fileDownload
//= require jquery/jquery.inputmask
//= require jquery/jquery.placeholder.min
//= require jquery/jquery.redirect
// Leaflet libs
//= require leaflet/leaflet.label
//= require leaflet/esri-leaflet
//= require leaflet/heatmap.min
//= require leaflet/leaflet-heatmap
// Darkroom libs
//=# require darkroom/fabric
//=# require darkroom/darkroom.min
// Parsley libs
//= require parsley/parsley
// Project js files
//= require_tree ./common
//= require ./anglers/index
//= require ./anglers/form
//= require ./anglers/new
//= require ./anglers/show
//= require_tree ./anglers/modal
//= require ./anglers/duplicate-anglers
//= require ./captures/functions
//= require ./captures/form
//= require ./captures/index
//= require ./captures/show
//= require_tree ./draft_captures
//= require_tree ./photos
//= require_tree ./reports
//= require_tree ./recaptures
//= require_tree ./scratch_captures
//= require_tree ./search
//= require_tree ./stats
//= require_tree ./tag_lots
//= require_tree ./tags
//= require_tree ./user_tag_requests
//= require_tree ./users
//= require_tree ./volunteer_time_logs
//= require_tree ./welcome
//= require_tree ./angler_items
//= require_self
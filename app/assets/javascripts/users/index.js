onReady($('body.users .index'), function (selector) {
  selector.find('.ldwf').DataTable();
  selector.find('.anglers').DataTable();
  //the unverified column should be sorted by newest anglers first
  selector.find('.unverified').DataTable({
    'order': [[4, 'desc']]
  });
  selector.find('.admin').DataTable();
});

function initUnverifiedUsersTable(selector) {

    var dataTableParams = {
        searching: true,
        lengthChange: true,
        autoWidth: true,
        processing: true,
        serverSide: true,
        order: [5, 'desc'],
        ajax: {
            url: "/admin/users.json",
            data: function (d) {
                d.scope = "visible_app";
                d.verified = false;
            }
        },
        columns: [
            {data: "first_name"},
            {data: "last_name"},
            {
                data: "email"
            },
            {
                data: "angler_number"
            },
            {
                data: "created_at",
                render: function (data, type, full, meta) {
                    return dateFormatfull(data);
                }
            },
            {
                data: "contacted",
                searchable: false,
                render: function (data, type, full, meta) {
                    return data ? '<i class="fa fa-check"></i>' : '';
                }
            },
            {
                data: "edit",
                className: 'text-center',
                searchable: false,
                sortable: false,
                render: function (data, type, row, meta) {
                  if (row['user_id']) {
                    return '<a class="btn btn-default fa fa-pencil" href="/admin/users/' + row['user_id'] + '/edit" target="_blank class ="></a>';

                  }
                }
            },
            {
                data: "delete",
                searchable: false,
                sortable: false,
                render: function (data, type, row, meta) {
                  if (row['user_id']) {
                    return '<a data-confirm="Are you sure you want to delete this item?" class="btn btn-default fa fa-trash-o" rel="nofollow" data-method="delete" href="/admin/users/"'+ row['user_id'] +' data-unsp-sanitized="clean"></a>'
                  }
                }
            },
            {
                data: "reset_password",
                searchable: false,
                sortable: false,
                render: function (data, type, row, meta) {
                  if (row['user_id']) {
                    return '<a class="btn btn-default fa fa-unlock" rel="nofollow" data-method="post" href="/admin/users/'+row['user_id']+'/generate_new_password_email" data-unsp-sanitized="clean"></a>';

                  }
                }
            }
        ]
    };

  var tabSelector = selector.find('#unverifiedUsers').closest('.tab-pane');


  // create the datatable object
  var newSelect = selector.find('#unverifiedUsers');
  var dataTable = newSelect.DataTable(dataTableParams);

}

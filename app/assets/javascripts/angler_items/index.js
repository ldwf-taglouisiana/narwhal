onReady($('body.angler-items .index'), function(selector) {
  anglerItemsIndexInitItemsTable(selector, false, '/angler_items');



});


function anglerItemsIndexInitItemsTable(selector, onlyUnsent, redirectUrl) {
  var rows_selected = [];

  var dataTableParams = {
    searching: false,
    lengthChange: true,
    autoWidth: true,
    processing: true,
    serverSide: true,
    order: [5, 'asc'],
    ajax: {
      url: "/angler_items.json",
      data: function ( d ) {
        d.unsent = onlyUnsent;
      }
    },
    columns: [
      { // verify checkbox
        data: "id",
        orderable: false,
        className: 'text-center',
        searchable: false
      },
      {
        data: "angler_number",
        render: function (data, type, row, meta) {
          if (row['user_id']) {
            return '<a href="/admin/users/' + row['user_id'] + '/edit" target="_blank">'+data+'</a>';

          } else {
            return data;
          }
        }
      },
      {
        data: "angler_name",
        render: function (data, type, row, meta) {
          return '<a href="/anglers/' + row['angler_id'] + '" target="_blank">'+data+'</a>';
        }
      },
      {data: "item_name"},
      {data: "item_type"},
      {
        data: "requested_at",
        searchable: false,
        render: function (data, type, row, meta) {
          return dateFormatfull(data);
        }
      },
      {
        data: "fulfilled_at",
        searchable: false,
        render: function (data, type, row, meta) {
          return dateFormatfull(data);
        }
      }
    ]
  };

  var tabSelector = selector.find('#anglerItemsIndexTable').closest('.tab-pane');

  // combine the index params and the params for the datatable check box
  var settings = $.extend({}, dataTableParams, dataTableOptions(rows_selected));

  // create the datatable object
  var newSelect = selector.find('#anglerItemsIndexTable');
  var dataTable = newSelect.DataTable(settings);

  // initialize the checkboxes for the datatable
  dataTableInitCheckBoxes(newSelect, dataTable, rows_selected);
  itemsMarkFulfilled(tabSelector, rows_selected, dataTable, redirectUrl);

  // adds a click listener to the checkboxes so we can show/hide the actions
  tabSelector.on('change', 'tbody input[type="checkbox"]', function() {
    if (tabSelector.find('tbody input[type="checkbox"]:checked').length > 0) {
      tabSelector.find('.table-actions button, .table-actions input').show();
    } else {
      tabSelector.find('.table-actions button, .table-actions input').hide();
    }
  });
}



function itemsMarkFulfilled(selector, selected_rows, dataTable, redirectUrl){

  selector.find('button.post-button').on('click', function(){
    $.post($(this).data('path'), {options : selected_rows, redirect_url: redirectUrl}, function(){
      reloadTable(selector,dataTable,selected_rows);
      reloadAllTable();
    });
  });

}
onReady($('.draft-captures form.fish-entry'), function(selector){
    capturesFormInit(selector);
});

onUnload($('.draft-captures form.fish-entry'), function(selector){
    selector.parsley('destroy');
});

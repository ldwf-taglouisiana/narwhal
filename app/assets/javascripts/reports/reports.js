onReady($('body.report .reports'), function (selector) {
    reportsInitNav(selector);
    reportsInit(selector);

    selector.find('form').parsley(PARSLEY_CONFIG);

    selector.find('form').parsley().on('form:submit', function() {
        downloadWaitingDialog.show("Loading");
    });
});

function reportsInit(selector) {
    reportsInitCustomQueryBinds(selector);

    selector.find('.download').click(function() {
        var form = $(this).closest('form') ;
        if (form.parsley().validate()){
            startAsyncDownload(form.attr('action') + '?' + form.serialize(), "Building report");
        }
    });
}

function reportsShowDetail(element, isRecapture) {
    var item = $(element);
    var detailModal = $('#report_container .modal');

    detailModal.find('.modal-title').text(item.data('angler'));

    detailModal.find('table').DataTable({
        destroy : true,
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url" : Routes.report_angler_detail_path(),
            "data": function ( d ) {
                d.recaptures = isRecapture;
                d.angler_id = item.data('angler');
                d.start_date = detailModal.closest('#report_container').find('form input[name="start_date"]').val();
                d.end_date = detailModal.closest('#report_container').find('form input[name="end_date"]').val();
                d.count = detailModal.closest('#report_container').find('form input[name="count"]').val();
            }
        },
        "columns": [
            { "data": "angler_id" },
            { "data": "angler_name" },
            { "data": "tag_number" },
            { "data": "capture_date" },
            { "data": "species" },
            { "data": "length" },
            { "data": "length_range" },
            { "data": "time_of_day" },
            { "data": "fish_condition" },
            { "data": "latitude" },
            { "data": "longitude" }
        ]
    });

    detailModal.modal('show');
}

function reportsInitNav(selector) {
    selector.find('.list-group-item').unbind('click').on('click',function(e){
        Turbolinks.visit($(this).data('path'), { change: ['report_container'] });
    });
}

function reportsInitCustomQueryBinds(selector) {
    //this adds more input fields when the + buttons are pushed
    selector.find(".btn-group button.add").click(function () {

        var fieldset = $(this).closest(".field-set");
        var inputGroup = fieldset.find('.input-group').first();

        inputGroup.find("input").first().clone().appendTo(inputGroup);
        inputGroup.find('.input-group-addon').removeClass('hidden');
        inputGroup.find('.input-group-addon').first().clone().appendTo(inputGroup);
        inputGroup.find("input").last().val(null);

        fieldset.find('.remove').show();

        inputGroup.find('.input-group-addon').last().addClass('hidden');
        inputGroup.find('input[data-autocomplete]').railsAutocomplete();
    });

    selector.find('.btn-group button.remove').click(function () {

        var fieldset = $(this).closest(".field-set");
        var inputGroup = fieldset.find('.row.multiple .input-group');

        inputGroup.find('.input-group-addon').last().remove();
        inputGroup.find("input").last().remove();


        if (fieldset.find('input:visible').length == 1) {
            inputGroup.find('.input-group-addon').addClass('hidden');
            $(this).hide();
        }
    });

    //Show the other method of tag searching and clear the form for the method that was switched out
    selector.find('.btn-group button.range').click(function () {
        var fieldSet = $(this).closest('.field-set');
        fieldSet.find('.row.numbers').toggle();
        fieldSet.find('.row.range').toggle();

        if (fieldSet.find('.row.numbers').is(':visible')) {
            $(this).text('range');
            fieldSet.find('button.add').show();

            if (fieldSet.find('.row.multiple input').length > 1) {
                fieldSet.find('button.remove').show();
            } else {
                fieldSet.find('button.remove').hide();
            }

        } else {
            $(this).text('single');
            fieldSet.find('button.add, button.remove').hide();
        }
    });

    /*
        Adapted from http://jsfiddle.net/vpanga/ep37owwr/
     */
    selector.find("#basins_basin").on('change', function (e) {
        var data = $(this).data('sub-basin').data;

        var $SubItems = [],
            values = $(this).val();

        if (values) {
            $.each(values, function (i, c) {
                $.each(data[c], function (index, item) {
                    $SubItems.push($("<option/>", {
                        value: item.gid,
                        text: item.name
                    }));
                });
            });
        }

        selector.find("#sub_basins_sub_basin").empty().append($SubItems).trigger("chosen:updated");
    });

    selector.find('input[data-autocomplete]').railsAutocomplete();
}
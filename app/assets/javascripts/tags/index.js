onReady($('body.tags .index'), function (selector) {
    selector.find('table').dataTable({
        searching:    false,
        lengthChange: true,
        autoWidth:    true,
        processing: true,
        serverSide: true,
        order: [ 4, 'desc' ],
        ajax: {
            url : "/tags.json"
        },
        columns: [
            { data: "tag_no" },
            { data: "angler_number" },
            { data: "angler_name" },
            { data: "created_at",
                render: function(data, type, full, meta) {
                    return dateFormatfull(data);
                }
            },
            { data: "assigned_at",
              render: function(data, type, full, meta) {
                return dateFormatfull(data);
              }
            },
            {
                data: "used",
                orderable: false,
                className: 'text-center',
                render: function (data, type, full, meta) {
                    return data ? '<i class="fa fa-check"></i>' : '';
                }
            }
        ]
    });
});
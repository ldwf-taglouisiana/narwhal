/**
 * Created by danielward on 9/29/15.
 */
onReady($('body.tags .deactivate'), function (selector) {
    selector.find('form').parsley(PARSLEY_CONFIG);
});

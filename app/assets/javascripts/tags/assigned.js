onReady($('body.tags .assigned'), function (selector) {

    var table = selector.find('table').DataTable({
        searching:    false,
        lengthChange: true,
        autoWidth:    true,
        processing: true,
        serverSide: true,
        order: [ 2, 'desc' ],
        ajax: {
            url : Routes.assigned_tags_path({ format: 'json' }),
            data: function ( d ) {
                d.date = {
                    month: selector.find('form.month-filter select.month').val(),
                    year: selector.find('form.month-filter select.year').val()
                };
            }
        },
        columns: [
            { data: "angler_number" },
            { data: "angler_name" },
            { data: "assignment_at",
                render: function( data, type, full, meta){
                    return dateFormatMDY(data);
                } },
            { data: "start_tag" },
            { data: "end_tag" },
            { data: "number_of_tags" },
            { data: "last_tag_used"}

        ]
    });

    selector.find('form.month-filter select').change(function(){
        table.draw();
    });

    selector.find('form.month-filter button').click(function(){
        selector.find('form.month-filter select').val('');
        selector.find('form.month-filter select').find('option').removeAttr('selected');

        table.draw();
    });
});
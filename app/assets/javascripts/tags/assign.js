onReady($('body.tags .assign'), function (selector) {
    var form = selector.find('form');

    form.find("#angler_id").val(form.find("#passed_angler_id").val());


    form.find(".text-input").keyup(function () {
        this.value = this.value.toUpperCase();
    });

    form.find("#start_tag_number").focusout(function () {
        //$('.end-tag').val($(this).val());
    });

    form.find('#number_tags').focusout(function () {
        var value = $(this).val();
        if (!isNaN(parseFloat(value)) && parseFloat(value) > 0) {
            var prefix = form.find("#start_tag_number").val().replace(/[0-9]/g, '');
            var numbers = form.find('#start_tag_number').val().replace(/[A-Z]/g, '');
            numbers = parseFloat(numbers) + parseFloat($(this).val()) - 1;
            form.find('#end_tag_number').val(prefix + numbers);
        }
    });

    form.find('#end_tag_number, #start_tag_number').focusout(function() {
        var startNumbers = parseFloat(form.find('#start_tag_number').val().replace(/[A-Z]/g, ''));
        var endNumbers   = parseFloat(form.find('#end_tag_number').val().replace(/[A-Z]/g, ''));

        if (!isNaN(startNumbers) && !isNaN(endNumbers)) {
            form.find('#number_tags').val(endNumbers - startNumbers + 1);
        }
    });

    form.find('.clear-button').click(function () {
        form.find(":input[type=text], textarea").val("");
    });

    form.parsley(PARSLEY_CONFIG);
});


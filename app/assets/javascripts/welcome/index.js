onReady($('body.welcome .index'), function(selector){

  initUnverifiedUsersTable(selector);

  capturesIndexInitUnverifiedCapturesTable(selector,'/');
  capturesIndexInitUnverifiedRecapturesTable(selector,'/');
  capturesIndexInitDraftCapturesTable(selector,'/draft_captures.json?only_errors=true','/');
  initUserTagTable(selector.find('#unFulfilledRequestsList'), false);
  anglerItemsIndexInitItemsTable(selector, true, '/');

  selector.find('.check-all-button').click(function () {
    $(this).closest('.tab-pane').find('.verify-capture-checkbox').prop('checked', true);
  });

  selector.find('.unCheck-all-button').click(function () {
    $(this).closest('.tab-pane').find('.verify-capture-checkbox').prop('checked', false);
  });


});

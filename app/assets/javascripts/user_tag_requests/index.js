onReady($('body.user-tag-requests .index'), function (selector) {



  initUserTagTable(selector.find('table#unFulfilledRequestsList'), false);
  initUserTagTable(selector.find('table#fulfilledRequestsList'), true);
});

function initUserTagTable(selector, fulfilled) {
  selector.dataTable({
    searching:    false,
    lengthChange: true,
    autoWidth:    true,
    processing:   true,
    serverSide:   true,
    order: [ 4, 'desc' ],
    ajax: {
      url : "/user_tag_requests.json",
      data: function ( d ) {
        d.fulfilled = fulfilled
      }
    },
    columns: [
      { data: "angler_number" },
      { data: "angler_name" },
      { data: "tag_type" },
      { data: "number_of_tags" },
      { data: "created_at",
        render: function(data, type, full, meta) {
          return dateFormatMDY(data);
        }
      },
      {
        data: null,
        className: 'text-center',
        orderable: false,
        render: function (data, type, full, meta) {
          return '<a href="/user_tag_requests/' + full.id + '/edit" target="_blank"><i class="fa fa-pencil center-table-icon"></i></a>';
        }
      },
      {
        data: null,
        className: 'text-center',
        orderable: false,
        render: function (data, type, full, meta) {
          return '<a href="/user_tag_requests/' + full.id + '" data-confirm="Are you sure you want to delete this item?" data-method="delete" rel="nofollow"><i class="fa fa-trash-o center-table-icon"></i></a>';
        }
      }
    ]
  });
}



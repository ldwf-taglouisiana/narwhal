onReady($('.recaptures form.fish-entry'), function(selector){
    capturesFormInit(selector);

    selector.find('input[type=checkbox]#recapture_box').change(function(){
        selector.find('input[type=hidden]#recapture_box').val($(this).prop('checked') ? '1' : '0');

        /*
            If the box is unchecked then we ned to go back to the captures form
         */
        if (!$(this).prop('checked')) {
            var params = { fish_entry: selector.serializeJSON().recapture };
            Turbolinks.visit(Routes.new_capture_path(params));
        }
    });
});

onUnload($('.recaptures form.fish-entry'), function(selector){
    selector.parsley('destroy');
});

onReady($('body.tag-lots .index'), function (selector) {
    selector.find('table').dataTable({
        searching:    false,
        lengthChange: true,
        autoWidth:    true,
        processing: true,
        serverSide: true,
        order: [ 6, 'desc' ],
        ajax: {
            url : "/tag_lots.json"
        },
        columns: [
            { data: "name1" },
            { data: "prefix" },
            { data: "start_no" },
            { data: "end_no" },
            { data: "name2"},
            { data: "color_name" },
            { data: "created_at",
              render: function (data, type, full, meta){
                return dateFormatfull(data);
              }
            },
            {
                data: "ordered",
                className: 'text-center',
                render: function (data, type, full, meta) {
                    return data ? '<i class="fa fa-check"></i>' : '';
                }
            },
            { data: "order_date",
              render: function (data, type, full, meta){
                return dateFormatfull();
              }
            },
            {
                data: "received",
                className: 'text-center',
                render: function (data, type, full, meta) {
                    return data ? '<i class="fa fa-check"></i>' : '';
                }
            },
            { data: "received_date",
              render: function (data, type, full, meta){
                return dateFormatfull();
              }
            }
        ]
    });
});
onReady($('body.photos .index'), function (selector) {

    function handleImage(e){
        var reader = new FileReader();  //the file reader to handle the file that was uploaded
        var darkroom;    //croping utility
        var attachment = selector.find('#front_page_photo_stream_item_attachment64');   //selector to find the field we need

        //when the file reader loads
        reader.onload = function(event){
            //place the uploaded image into the img tag so darkroom can manipulate it
            selecto.find('#photo').attr('src', event.target.result);
            //start darkroom
            darkroom = new Darkroom('#photo', {
                plugins:{
                    crop: {
                        //this is the ratio to achieve the perfect image size for nessie
                        ratio: 1.3856
                    }
                },
                onSelfDestroy: function(){
                    //when darkroom is done we need to put the image back into the correct dom object for submission
                    //we can't submit image tags so we put it into the hidden field so it can be submitted
                    attachment.attr('value', selector.find('#image_holder img').attr('src'));
                    //and submit the form
                    attachment.closest('form').submit();
                }
            });
        };


        reader.readAsDataURL(e.target.files[0]);

    }

    // Set up the file loader to fire the handleImage function when the file begins to upload
    selector.find('.photo_field').on('change', handleImage);
});
onReady($('body.species .index'), function (selector) {
    selector.find('#myTab a').click(function (e) {
        e.preventDefault()
        $(this).tab('show')
    });

    selector.find('#species-order').sortable({
        axis: 'y',
        dropOnEmpty: false,
        handle: '.handle',
        cursor: 'crosshair',
        items: 'li',
        opacity: 0.4,
        scroll: true,
        update: function(){
            $.ajax({
                type: 'post',
                data: selector.find('#species-order').sortable('serialize'),
                dataType: 'script',
                complete: function(request){
                    selector.find('#species-order').effect('highlight');
                },
                url: '/species/sort?position=1'})
        }
    });

    selector.find('#target-order').sortable({
        axis: 'y',
        dropOnEmpty: false,
        handle: '.handle',
        cursor: 'crosshair',
        items: 'li',
        opacity: 0.4,
        scroll: true,
        update: function(){
            $.ajax({
                type: 'post',
                data: selector.find('#target-order').sortable('serialize'),
                dataType: 'script',
                complete: function(request){
                    selector.find('#target-order').effect('highlight');
                },
                url: '/species/sort?target=1'})
        }
    });
});

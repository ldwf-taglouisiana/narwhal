onReady($('body.anglers .index'), function (selector) {
    // used as the handler to indicate which rows have been selected.
    // allows the change of pages to keep the rows selected
    var rows_selected = [];

    // the angler index DataTable params
    var dataTableParams = {
        searching:    false,
        lengthChange: true,
        autoWidth:    true,
        processing: true,
        serverSide: true,
        order: [ 11, 'desc' ],
        ajax: {
            url : "/anglers.json",
            data: function ( d ) {
                d.verified = true;
            }
        },
        columns: [
            { data: "angler_id" },
            { data: "angler_id" },
            { data: "first_name" },
            { data: "last_name" },
            { data: "street" },
            { data: "suite" },
            { data: "city" },
            { data: "state" },
            { data: "zip" },
            { data: "phone" },
            { data: "email" },
            { data: "entered",
                render: function( data, type, full, meta){
                    return dateFormatMDY(data);
                }
            },
            { // assign tags
                data: "angler_id",
                orderable: false,
                className : 'text-center',
                render: function ( data, type, full, meta ) {
                    return '<a href="/tags/assign?angler_id' + data + '" target="_blank"><i class="fa fa-tags center-table-icon"></i></a>';
                }
            },
            { // open
                data: "id",
                orderable: false,
                className : 'text-center center-table-icon',
                render: function ( data, type, full, meta ) {
                    return '<a href="/anglers/' + data + '" target="_blank"><i class="fa fa-external-link center-table-icon"></i></a>';
                }
            }
        ]
    };

    // combine the index params and the params for th datatable check box
    var settings = $.extend({}, dataTableParams, dataTableOptions(rows_selected));

    // create the datatable object
    var dataTable = selector.find('#anglersIndexList').DataTable(settings);

    // initialize the checkboxes for the datatable
    dataTableInitCheckBoxes(selector.find('#anglersIndexList'), dataTable, rows_selected);

    // adds a click listener to the checkboxes so we can show/hide the actions
    selector.on('change', 'tbody input[type="checkbox"]', function() {
       if (selector.find('tbody input[type="checkbox"]:checked').length > 0) {
           selector.find('.table-actions button').show();
       } else {
           selector.find('.table-actions button').hide();
       }
    });

    // the listener to start the download
    selector.find('.table-actions button.btn-download').on('click', function() {
        var params = {
          options: rows_selected,
          select_all: selector.find('.table-container form #select_all').is(':checked')
        };

        startAsyncDownload(Routes.download_anglers_path(params))
    });
});
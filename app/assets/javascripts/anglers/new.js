onReady($('body.anglers .new form'), function (selector) {
    var skipDuplicatesCheck = false;
    var anglers = [];

    selector.find("#angler_first_name, #angler_last_name").on('change', function(){
        var params = { last_name: selector.find('#angler_last_name').val(), first_name: selector.find('#angler_first_name').val() };

        $.getJSON(Routes.api_v2_internal_angler_from_name_path(), params, function(json) {
            anglers = json.anglers;

            $('#duplicate_angler_modal tbody').empty();
            var html = '';

            $(anglers).each(function (index, item) {
                html += "<tr>";
                html += "<td>" + item.angler_id + "</td>";
                html += "<td>" + item.first_name + "</td>";
                html += "<td>" + item.last_name + "</td>";
                html += "<td>" + item.street + "</td>";
                html += "<td>" + item.suite + "</td>";
                html += "<td>" + item.city + "</td>";
                html += "<td>" + item.state + "</td>";
                html += "<td>" + item.zip + "</td>";
                html += "<td>" + item.phone_number_1 + "</td>";
                html += "<td>" + item.phone_number_2 + "</td>";
                html += "<td>" + item.email + "</td>";
                html += "<td>" + item.email_2 + "</td>";
                html += "</tr>";
            });

            $('#duplicate_angler_modal tbody').append(html);
        });
    });

    $('#duplicate_angler_modal .continue-button').click(function () {
        $(this).closest('#duplicate_angler_modal').modal('hide');
        skipDuplicatesCheck = true;
        selector.submit();
    });

    selector.parsley().on('form:submit', function (formInstance) {
        if (anglers.length > 0 && !skipDuplicatesCheck) {
            $('#duplicate_angler_modal').modal('show');

            formInstance.submitEvent.preventDefault();
            return false;

        } else if ($.trim(formInstance.$element.find('#angler_angler_id').val()).length == 0) {
            if (confirm('Are you sure you want to leave the Angler ID blank?\nA generated value will be used if you continue')) {
                // Save it!
            } else {
                formInstance.submitEvent.preventDefault();
                return false;
            }
        }
    });
});


onReady($('#changeAnglerID'), function (selector) {

    selector.find('.angler-id-field').on('change', function(){
        $.getJSON(Routes.api_v2_internal_validate_angler_id_available_path(), { angler_id: $(this).val()}, function() {
            selector.find('.merge-info').hide();
            selector.find('input[type="submit"]').removeAttr('disabled');
        }).fail(function() {
            selector.find('.merge-info').show();
            selector.find('input[type="submit"]').removeAttr('disabled').attr('disabled', 'disabled');
        });
    });

    selector.find('.merge-info button').on('click', function(){
        var params = {
            angler_ids: [
                selector.find('#original_angler_id').val(),
                selector.find('.angler-id-field').val()
            ]
        };

        Turbolinks.visit(Routes.with_ids_duplicates_anglers_path(params))
    });
});
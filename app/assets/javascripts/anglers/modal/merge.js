onReady($('#anglerMergeModal'), function (selector) {

    ////When merge is clicked on the modal, grab the canonical angler and send it to anglers_controller
    selector.find("button.submit").click(function () {
        if (confirm('If you continue, this will transfer all data to the destination angler.')) {
            var params = anglerMergeExtractAnglerParams(selector);
            $.redirect(Routes.perform_merge_anglers_path(), params);
        }
    });

    //When a cell in the modal duplicates table is clicked, the cell is highlighted
    //and all other columns that may have been selected will be deselected.
    selector.on('click', 'div.modal-body table.duplicates tbody tr td', function () {

        // get the index of the column
        var col = $(this).parent().children().index($(this));

        selector.find("table.duplicates tbody tr").each(function (index) {
            selector.find(this).children('td').each(function (index) {
                if (index == col) {
                    $(this).removeClass('item-selected');
                }
            });
        });

        //Highlight the recently clicked row.
        $(this).toggleClass("item-selected");

        var selected_cell = selector.find(this);
        selector.find("table.canonical tbody tr td").each(function (index) {
            if (index == col) {
                $(this).text(selected_cell.html());
                $(this).effect("highlight", {}, 1000);
            }

        });
    });
});

/**
 * Will create the needed params for performing the merging of anglers.
 * Looks at the canonical angler table and extract each of the 'td' elements.
 * The params will also include the value of the deletion checkbox and the duplicate angler ids
 *
 * @param selector The merge angler modal selector
 * @returns [Object] A dictionary of parameter values, which represent the destination angler
 */
function anglerMergeExtractAnglerParams(selector) {
    var data = {};
    data['angler'] = {}; // the angler params
    data['delete_duplicates'] = selector.find('.modal-body .actions input#delete_anglers').is(":checked"); // the checkbox val for the deletion of the duplicate anglers
    data['duplicate_ids'] = []; // the list of duplicate angler ids

    // get the values from the canonical table
    selector.find("table.canonical tbody tr td").each(function () {
        data['angler'][$(this).data('key')] = $(this).text();
    });

    // get the duplicate angler ids from the dupicates table
    selector.find("table.duplicates .angler-id").each(function () {
        var angler_id = $(this).text();

        /*
            If the angler id column is not the same as the canonical/destination
            angler id then add it to the list.
         */
        if (angler_id != selector.find('table.canonical td[data-key="angler_id"]').text()) {
            data['duplicate_ids'].push($(this).text());
        }
    });

    return data;
}

/**
 * This is a call back for when the the merge modal is shown. It
 * handles populating the tables with the appropriate data.
 *
 * @param selector [jQuery] The modal selector that contains the _merge_angler modal
 * @param source_table [jQuery] The selector that contains the source duplicates table.
 * @param ids [Array] List of the angler_ids that are going to use for the merge modal
 */
function anglerMergeModalShown(selector, source_table, ids) {
    var rows = [];

    //return the canonical angler from JSON route in anglers_controller.
    $.getJSON(Routes.canonical_angler_merge_anglers_path(), { angler_ids: ids }, function (data) {
        // create the tr that will be the canonical angler
        var canonical = '<tr>' +
//        '<td class="id hidden">'+ data.id + '</td>' +
            '<td data-key="angler_id">' + data.angler_id + '</td>' +
            '<td data-key="first_name">' + data.first_name + '</td>' +
            '<td data-key="last_name">' + data.last_name + '</td>' +
            '<td data-key="street">' + data.street + '</td>' +
            '<td data-key="suite">' + data.suite + '</td>' +
            '<td data-key="city">' + data.city + '</td>' +
            '<td data-key="state">' + data.state + '</td>' +
            '<td data-key="zip">' + data.zip + '</td>' +
            '<td data-key="phone_number_1">' + data.phone_number_1 + '</td>' +
            '<td data-key="phone_number_2">' + data.phone_number_2 + '</td>' +
            '<td data-key="email">' + data.email + '</td>' +
            '<td data-key="email_2">' + data.email_2 + '</td>' +
            '<td data-key="shirt_size">' + data.shirt_size + '</td>' +
            '</tr>';


        //Add the duplicate anglers to the duplicates table
        source_table.find("input:checked").each(function (index) {
            // clon the row on the page, then remove the checkbox td
            var clonedRow = $(this).closest('tr').clone();
            clonedRow.find('td:first-child').remove();
            // add the new row to the array
            rows.push('<tr class="dup-row">' + clonedRow.html() + '<tr>');
        });


        //Add canonical angler to the canonical table
        selector.find("table.canonical tbody").append(canonical);
        selector.find("table.duplicates tbody").append(rows.join(''));

        //Remove null values from the table for empty fields.
        selector.find("table.canonical tbody tr td").each(function (index) {
            if ($(this).html().match(/null/)) {
                $(this).text("");
            }
        });

        // remove the empty rows if any.
        selector.find('table.duplicates tbody tr').each(function(){
            if($(this).html().trim() == "") {
                $(this).remove();
            }
        });

        selector.find("#candidatesList").dataTable({
            destroy: true,
            lengthChange: false,
            autoWidth:    true,
            searching: false
        });
    });
}
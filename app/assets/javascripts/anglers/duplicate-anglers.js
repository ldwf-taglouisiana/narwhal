onReady($('body.anglers .duplicate-anglers'), function (selector) {
    selector.find("#duplicateList").dataTable({
        lengthChange: false,
        autoWidth:    true
    });

    selector.find('#anglerMergeModal').on('show.bs.modal', function (e) {
        anglerMergeModalShown(selector.find('#anglerMergeModal'), $('#duplicateList'), getDuplicateIds());
    });

    function getDuplicateIds() {
        var ids = [];

        $("#duplicateList input:checked").each(function () {
            ids.push($(this).data('id'));
        });

        return ids
    }
});
class AdminNotifyMailer < ActionMailer::Base
  include CaptureSpreadsheetModule
  default :to => '"Narwhal Admins" <all.admins@taglouisiana.com>'

  def new_app_user_email(user)
    @user = user
    @angler = user.angler
    mail(:subject => "New App user #{@user.first_name} #{@user.last_name} registered")
  end

  def new_capture_email(capture)
    @capture = capture
    @angler = capture.angler
    @tag = Tag.find(@capture.tag_id)
    @species = Species.find(@capture.species_id)
    map = GoogleStaticMap.new(:scale => 1, :height => 300, :width => 300)
    map.markers << MapMarker.new(:color => 'red', :location => MapLocation.new(:latitude => @capture.latitude, :longitude => @capture.longitude))

    map.zoom = 9
    @map = map.url
    mail(:subject => 'New Capture')
  end

  def new_recapture_email(capture)
    @capture = capture
    @angler =  capture.angler
    @tag = Tag.find(@capture.tag_id)
    @species = Species.find(@capture.species_id)
    map = GoogleStaticMap.new(:scale => 1, :height => 300, :width => 300)
    #raise map.inspect
    map.markers << MapMarker.new(:color => 'red', :location => MapLocation.new(:latitude => @capture.latitude, :longitude => @capture.longitude))

    map.zoom = 9
    @map = map.url
    mail(:subject => 'New Recapture')
  end

  def new_tag_request_email(tag_request)
    @tag_request = tag_request
    @angler = tag_request.angler
    mail(:subject => 'New Tag Request')
  end


  # Method to send a summary email about all user-made captures yesterday.
  def capture_summary_email(emails, attachment)
    attachments['report.xlsx'] = {
        mime_type: Mime::XLSX,
        content: attachment
    }

    mail(subject: 'Capture Summary for ' + Chronic.parse('yesterday').strftime("%m/%d/%Y").to_s, to: 'NO REPLY <narwhal@taglouisiana.com>', bcc: emails)
  end
end

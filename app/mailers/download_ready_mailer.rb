class DownloadReadyMailer < ActionMailer::Base

  def download_ready_email(requested_download)
    user = User.find(requested_download.user_id)
    @download = requested_download

    mail(:subject => "Your file '#{requested_download.filename}' is ready", to: user.email )
  end

end

class UserNotifyMailer < ActionMailer::Base

  default :from => '"Tag Louisiana" <no-reply@taglouisiana.com>'

  def notify_release_angler_email(capture)
    return if capture.angler.nil?
    return unless capture.angler.has_valid_email?
    @angler = capture.angler
    @capture = capture
    mail(:to => @angler.email, :subject => 'Your fish has been recaptured!')
  end

  def notify_recapture_angler_email(recapture,reward)
    return if recapture.angler.nil?
    return unless recapture.angler.has_valid_email?
    @recapture = recapture
    @angler = recapture.angler
    @reward = reward
    mail(:to => @angler.email, :subject => 'Your submitted recapture')
  end

  def notify_reward_sender_email(recapture)
    return if recapture.angler.nil?
    return unless recapture.angler.has_valid_email?
    @angler = recapture.angler
    mail(:to => 'mike@aftco.com', :subject => 'Reward Request')
  end

  def notify_anon_recap_angler_email(recapture)
    return if recapture.angler.nil?
    return unless recapture.angler.has_valid_email?
    @recapture = recapture
    @angler = recapture.angler
    @reward = true
    mail(:to => @angler.email, :subject => 'Your submitted recapture')
  end

  def new_user_email(user)
    @user = user
    mail(:to => @user.email, :subject => 'Welcome to TAG Louisiana!')
  end

  def notify_user_verified_email(user)
    @user = user
    mail(:to => user.email, :subject => 'You\'ve been approved!')
  end

end
# == Schema Information
# Schema version: 20160405165101
#
# Table name: sub-basin
#
#  gid        :integer          not null, primary key
#  objectid   :decimal(10, )
#  area       :decimal(, )
#  perimeter  :decimal(, )
#  subseg02   :decimal(10, )
#  subseg02id :decimal(10, )
#  subsegment :string(15)
#  name       :string(71)
#  descriptio :string(140)
#  basin      :string(18)
#  shape_area :decimal(, )
#  shape_len  :decimal(, )
#  geom       :geometry(MultiPo
#  created_at :datetime         default(Tue, 01 Dec 2015 03:29:39 UTC +00:00), not null
#  updated_at :datetime         default(Tue, 01 Dec 2015 03:29:39 UTC +00:00), not null
#
# Indexes
#
#  idx_sub_basins_geom  (geom)
#

module SubBasinsHelper
  def sub_basin_grouped_options(object)
    options = SubBasin.joins(:basin).select('"sub-basin".*, "basins".name as basin_name, "basins".gid as basin_gid').group_by(&:basin_name).each{ |_,v| v.replace(v.map{ |t| [t.name, t.id, { data: { basin: t.basin_gid } }]})}
    grouped_options_for_select(options, object.sub_basin_id)
  end
end

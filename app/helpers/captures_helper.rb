# == Schema Information
# Schema version: 20160405165101
#
# Table name: captures
#
#  id                                 :integer          not null, primary key
#  tag_id                             :integer
#  capture_date                       :datetime         not null
#  species_id                         :integer
#  length                             :decimal(, )
#  location_description               :text
#  latitude                           :float
#  longitude                          :float
#  fish_condition_option_id           :integer
#  weight                             :float
#  comments                           :text
#  confirmed                          :boolean          default(FALSE)
#  verified                           :boolean          default(FALSE)
#  deleted                            :boolean
#  mailed_map                         :boolean
#  time_of_day_option_id              :integer
#  created_at                         :datetime         not null
#  updated_at                         :datetime         not null
#  geom                               :geometry
#  entered_gps_type                   :string(255)
#  user_capture                       :boolean          default(FALSE)
#  species_length_id                  :integer
#  basin_id                           :integer
#  sub_basin_id                       :integer
#  gps_format_id                      :integer
#  recent                             :boolean          default(FALSE), not null
#  search_vector                      :tsvector         not null
#  fish_event_location_description_id :integer
#  angler_id                          :integer          not null
#
# Indexes
#
#  cap_tag_fk                                  (tag_id) UNIQUE
#  captures_length_idx                         (length)
#  captures_tag_idx                            (tag_id) UNIQUE
#  idx_captures_created_at                     (created_at)
#  index_captures_geom                         (geom)
#  index_captures_on_fish_condition_option_id  (fish_condition_option_id)
#  index_captures_on_gps_format_id             (gps_format_id)
#  index_captures_on_recent                    (recent)
#  index_captures_on_search_vector             (search_vector)
#  index_captures_on_species_id                (species_id)
#  index_captures_on_species_length_id         (species_length_id)
#  index_captures_on_time_of_day_option_id     (time_of_day_option_id)
#
# Foreign Keys
#
#  cap_fishcond_fk                  (fish_condition_option_id => fish_condition_options.id)
#  cap_species_fk                   (species_id => species.id)
#  cap_time_fk                      (time_of_day_option_id => time_of_day_options.id)
#  captures_basin_id_fk             (basin_id => basins.gid)
#  captures_species_length_id_fkey  (species_length_id => species_lengths.id)
#  captures_sub_basin_id_fk         (sub_basin_id => "sub-basin".gid)
#  fk_captures_gps_type             (entered_gps_type => gps_formats.format_type)
#  fk_rails_64432fd973              (gps_format_id => gps_formats.id)
#  fk_rails_906df65cfe              (angler_id => anglers.id)
#

module CapturesHelper

  def history_modal_name(item)
    "#{item.class.name.downcase}_#{item.id.to_words.gsub(/\s/,'_')}_history"
  end

  def capture_images_modal_name(item)
    "#{item.class.name.downcase}_#{item.id.to_words.gsub(/\s/,'_')}_images"
  end

  def capture_form_active_gps_class(item, format)
    if (item.entered_gps_type == format.format_type) || (item.entered_gps_type.nil? && format.format_type == 'D')
      'active'
    else
      nil
    end
  end

  def capture_form_active_gps_checked(item, format)
    if (item.entered_gps_type == format.format_type) || (item.entered_gps_type.nil? && format.format_type == 'D')
      true
    else
      false
    end
  end

  def format_capture_tab(item)
    content_tag :div do
      content_tag :span do
        (item.class.name + '<br/>' + item.capture_date.strftime('%m/%d/%Y')).html_safe
      end.html_safe
    end
  end

  class ActionView::Helpers::FormBuilder
    include ActionView::Helpers::TagHelper
    include ActionView::Helpers::FormTagHelper
    include ActionView::Helpers::FormOptionsHelper
    include ActionView::Helpers::CaptureHelper
    include ActionView::Helpers::AssetTagHelper

    # Accepts an int and displays a smiley based on >, <, or = 0
    def coordinate_field_bootstrap(method, options = {})
      content = [
          hidden_field(method, options.string_merge({class: 'coordinate-input'})),
          text_field_tag(:degrees, nil, options.string_merge({class: 'field degrees form-control'}.merge({placeholder: 'Deg.'}))),
          content_tag(:span, '°', class: 'input-group-addon degrees'),
          text_field_tag(:minutes, nil, options.string_merge({class: 'field minutes form-control'}.deep_merge({placeholder: 'Min.', 'data-parsley-min' => '0', 'data-parsley-max' => '59.99' }))),
          content_tag(:span, '′′', class: 'input-group-addon minutes'),
          text_field_tag(:seconds, nil, options.string_merge({class: 'field seconds form-control'}.deep_merge({placeholder: 'Sec.', 'data-parsley-min' => '0', 'data-parsley-max' => '59.99' }))),
          content_tag(:span, '′', class: 'input-group-addon seconds'),
      ].join.html_safe

      content_tag :div, content, class: "coordinate-group input-group #{method.to_s}-group"
    end

    def field_name(label,index=nil)
      output = index ? "[#{index}]" : ''
      return @object_name + output + "[#{label}]"
    end

    def field_id(label,index=nil)
      output = index ? "_#{index}" : ''
      return @object_name + output + "_#{label}"
    end

  end
end

# == Schema Information
# Schema version: 20160405165101
#
# Table name: user_tag_requests
#
#  id             :integer          not null, primary key
#  number_of_tags :integer          not null
#  fulfilled      :boolean          default(FALSE)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  tag_type       :string(255)      not null
#  uuid           :string
#  angler_id      :integer          not null
#
# Foreign Keys
#
#  fk_rails_e812ee6b36  (angler_id => anglers.id)
#

module UserTagRequestsHelper

end

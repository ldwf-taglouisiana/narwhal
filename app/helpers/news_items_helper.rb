# == Schema Information
# Schema version: 20160405165101
#
# Table name: news_items
#
#  id                :integer          not null, primary key
#  user_id           :integer          not null
#  content           :text             not null
#  should_publish    :boolean          default(FALSE)
#  should_publish_at :datetime
#  title             :string(255)      not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#
# Foreign Keys
#
#  newsitem_user_fk  (user_id => users.id)
#

module NewsItemsHelper
end

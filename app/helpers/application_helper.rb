module ApplicationHelper

  DEFAULT_DATE_FORMAT = '%m/%d/%Y'

  def bootstrap_class_for flash_type
    case flash_type.to_sym
      when :success
        'alert alert-success alert-dismissible'
      when :error
        'alert alert-danger alert-dismissible'
      when :alert
        'alert alert-warning alert-dismissible'
      when :notice
        'alert alert-info alert-dismissible'
      else
        flash_type.to_s
    end
  end

  def time_to_cst time
    time = Chronic.parse(time) if time.is_a?(String)
    time.try(:in_time_zone, 'Central Time (US & Canada)').try(:strftime, '%m/%d/%Y %l:%M %p')
  end

  def format_with_placeholder(item)
    item || content_tag(:span, 'N/A', class: 'placeholder')
  end

  def datatable_checkbox
    content_tag :div, class:'checkbox datatable-all-checkbox' do
      label = label_tag :all, class: '' do
        check_box_tag(:all, '1', false, class: 'checkbox')
      end

      label + check_box_tag(:select_all, '1', false, class: 'hidden')
    end
  end

  def font_awesome_icon(fa_class, options = {})
    options = options.string_merge({class: "fa #{fa_class}" })
    html = content_tag(:i, nil, options)
    html += ' ' + options[:text] unless options[:text].nil?
    html.html_safe
  end

  class ActionView::Helpers::FormBuilder
    include ActionView::Helpers::TagHelper
    include ActionView::Helpers::FormTagHelper
    include ActionView::Helpers::FormOptionsHelper
    include ActionView::Helpers::CaptureHelper
    include ActionView::Helpers::AssetTagHelper

    def datetime_picker_field(method, options = {}, outer_options = {})
      date_value = nil

      if @object.send(method).present?
        date_value = options[:date_format].present? ? @object.send(method).try(:strftime, options[:date_format]) : @object.send(method).try(:strftime, DEFAULT_DATE_FORMAT)
      end

      content = [
          hidden_field(method, options.string_merge({class: 'datetime-value'}).merge({value: @object.send(method).try(:iso8601)}).except(:data)),
          text_field_tag(method, date_value, options.string_merge({class: 'datetime-input datetime-field'}).except(:name)),
      ].join

      content_tag :div, content.html_safe, outer_options.string_merge({class: 'datetime-wrapper'})
    end

    def date_picker_field(method, options = {}, outer_options = {})
      date_value = nil

      if @object.send(method).present?
        date_value = options[:date_format].present? ? @object.send(method).try(:strftime, options[:date_format]) : @object.send(method).try(:strftime, DEFAULT_DATE_FORMAT)
      end

      content = [
          hidden_field(method, options.string_merge({class: 'date-value'}).merge({value: @object.send(method).try(:iso8601)}).except(:data)),
          text_field_tag(method, date_value, options.string_merge({class: 'date-input date-field'}).except(:name)),
      ].join

      content_tag :div, content.html_safe, outer_options.string_merge({class: 'date-wrapper'})
    end

    def submit_with_input_field(value, input_name, options = {})
      content = [
          button_tag(value, options.merge({ onclick: "$(this).closest('div').find('input').val('#{input_name}');$(this).closest('form').submit()" })),
          hidden_field_tag(input_name, nil)
      ].join

      content_tag :div, content.html_safe
    end

    def field_name(label,index=nil)
      output = index ? "[#{index}]" : ''
      return @object_name + output + "[#{label}]"
    end

    def field_id(label,index=nil)
      output = index ? "_#{index}" : ''
      return @object_name + output + "_#{label}"
    end

  end

  def datetime_picker_field_tag(method, value = nil, options = {}, outer_options = {})
    content = [
        hidden_field_tag(method, value, options.string_merge({class: 'datetime-value'}).except(:data)),
        text_field_tag(method, nil, options.string_merge({class: 'datetime-input datetime-field'}).except(:name)),
    ].join

    content_tag :div, content.html_safe, outer_options.string_merge({class: 'datetime-wrapper'})
  end

  def date_picker_field_tag(method, value = nil, options = {}, outer_options = {})
    content = [
        hidden_field_tag(method, value, options.string_merge({class: 'date-value'}).except(:data)),
        text_field_tag(method, nil, options.string_merge({class: 'date-input date-field'}).except(:name)),
    ].join

    content_tag :div, content.html_safe, outer_options.string_merge({class: 'date-wrapper'})
  end

  def submit_with_input_field_tag(value, input_name, options = {})
    content = [
        button_tag(value, options.merge({ onclick: "$(this).closest('div').find('input').val('#{input_name}');$(this).closest('form').submit()" })),
        hidden_field_tag(input_name, nil)
    ].join

    content_tag :div, content.html_safe
  end
end

# == Schema Information
# Schema version: 20160405165101
#
# Table name: notifications
#
#  id                     :integer          not null, primary key
#  initiator_id           :integer
#  checked                :boolean          default(FALSE)
#  notification_item_id   :integer          not null
#  notification_item_type :string(255)      not null
#  notification_topic_id  :integer          not null
#  user_id                :integer          not null
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#
# Foreign Keys
#
#  notification_initiator_fk  (initiator_id => users.id)
#  notification_topic_fk      (notification_topic_id => notification_topics.id)
#  notification_user_fk       (user_id => users.id)
#

module NotificationsHelper
  def notification_title(notification)
    content_tag :div, notification_title_text(notification), class: "notification-title #{notification_title_class(notification)}"
  end

  def notification_content(notification)
    content_tag :div, notification_info_text(notification), class: "notification-info #{notification_title_class(notification)}"
  end

  def notification_time(notification)
    content_tag :div, "#{time_ago_in_words(notification.created_at, include_seconds: true)} ago", class: 'notification-time'
  end

  def notification_icon(notification)
    content_tag :div, notification_icon_text(notification), class: "notification-icon #{notification_title_class(notification)}"
  end

  def notification_title_class(notification)
    case notification.kind
      when :capture
        'green'
      when :recapture
        'purple'
      when :unregistered_recapture
        'pink'
      when :user_tag_request
        'blue'
      when :user
        'orange'
      when :unknown
        'grey'
    end
  end

  def notification_title_text(notification)
    case notification.kind
      when :capture
        'Capture'
      when :recapture
        'Recapture'
      when :unregistered_recapture
        'Unregistered Recapture'
      when :user_tag_request
        'User Tag Request'
      when :user
        'New User'
      when :unknown
        'Unknown'
    end
  end

  def notification_icon_text(notification)
    case notification.kind
      when :capture
        'C'
      when :recapture
        'R'
      when :unregistered_recapture
        'UR'
      when :user_tag_request
        'T'
      when :user
        'U'
      when :unknown
        '?'
    end
  end

  def notification_info_text(notification)
    case notification.kind
      when :capture, :recapture, :unregistered_recapture
        "#{notification.notification_item.try(:tag).try(:tag_no)} from #{notification.notification_item.try(:angler).try(:full_name)}"
      when :user_tag_request
        "#{notification.notification_item.number_of_tags} #{notification.notification_item.tag_type} tags from #{notification.notification_item.try(:angler).try(:full_name)}"
      when :user
        "#{notification.notification_item.try(:angler).try(:full_name)} has registered"
      when :unknown
        "We can't seem to locate the information on this item"
    end
  end
end

module ReportHelper

  def nav_link(name, section, active = false)
    link_to name, '#', class: "list-group-item#{' active' if active}", 'data-path' => report_path(section: section), 'data-section' => section
  end
end

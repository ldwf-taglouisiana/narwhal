# == Schema Information
# Schema version: 20160405165101
#
# Table name: blurbs
#
#  id                     :integer          not null, primary key
#  capture_dashboard      :text
#  new_capture            :text
#  my_tags                :text
#  order_tags             :text
#  find_a_spot            :text
#  common_fish            :text
#  target_species         :text
#  about_program          :text
#  sign_up                :text
#  how_to                 :text
#  licensing              :text
#  volunteer_hours        :text
#  contact_info           :text
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  non_registered_capture :text
#  login                  :text
#  my_captures            :text
#  draft_captures         :text
#  account_info           :text
#  contact_us             :text
#  privacy_policy         :text
#  faq                    :text
#

module BlurbsHelper
end

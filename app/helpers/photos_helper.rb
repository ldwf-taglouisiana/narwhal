# == Schema Information
# Schema version: 20160405165101
#
# Table name: photos
#
#  id                 :integer          not null, primary key
#  image_file_name    :string(255)
#  image_content_type :string(255)
#  image_file_size    :integer
#  image_updated_at   :datetime
#  imageable_id       :integer
#  imageable_type     :string(255)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

module PhotosHelper
end

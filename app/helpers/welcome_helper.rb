module WelcomeHelper
  def format_title_for_species_table(params)
    class_name = params[:recapture] ? 'Recaptures' : 'Captures'

    if (5.minutes.ago..Time.now).cover?(params[:end_date])
      days = (params[:end_date] - params[:start_date]).to_i / 1.day
      "#{class_name} in the last #{days} days"
    else
      "#{class_name} for #{params[:start_date].strftime('%B %Y')}"
    end
  end
end

# == Schema Information
# Schema version: 20160405165101
#
# Table name: marinas
#
#  id                      :integer          not null, primary key
#  site_no                 :integer
#  name                    :string(255)
#  closed                  :boolean          default(FALSE)
#  city                    :string(255)
#  parish                  :string(255)
#  publish                 :boolean          default(FALSE)
#  address                 :string(255)
#  latitude                :float
#  longitude               :float
#  web_address             :text
#  phone                   :string(255)
#  hours_of_operation      :string(255)
#  can_get_fishing_license :boolean
#  fee_amount              :string(255)
#  has_bait                :boolean          default(FALSE)
#  has_ice                 :boolean          default(FALSE)
#  has_food                :boolean          default(FALSE)
#  has_restrooms           :boolean          default(FALSE)
#  has_sewage_pump         :boolean          default(FALSE)
#  has_cert_scales         :boolean          default(FALSE)
#  facility_type           :string(255)
#  has_cleaning_station    :boolean          default(FALSE)
#  fuel_type               :string(255)
#  location_description    :text
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  deleted                 :boolean          default(FALSE)
#  photo_file_name         :string(255)
#  photo_content_type      :string(255)
#  photo_file_size         :integer
#  photo_updated_at        :datetime
#  comments                :text
#

module MarinasHelper
end

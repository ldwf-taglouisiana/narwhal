# == Schema Information
# Schema version: 20160405165101
#
# Table name: species_lengths
#
#  id          :integer          not null, primary key
#  description :string(255)      not null
#  species_id  :integer          not null
#  position    :integer          not null
#  created_at  :datetime
#  updated_at  :datetime
#  min         :float
#  max         :float
#
# Indexes
#
#  idx_species_length_min_max  (min,max)
#
# Foreign Keys
#
#  species_lengths_species_fk  (species_id => species.id)
#

module SpeciesLengthsHelper
  def species_length_grouped_options(object)
    options = SpeciesLength.grouped_select.group_by(&:species_name).each{ |_,v| v.replace(v.map{ |t| [t.description, t.id, { data: { species: t.species_id } }]})}
    grouped_options_for_select(options, object.species_length_id)
  end
end

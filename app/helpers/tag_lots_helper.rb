# == Schema Information
# Schema version: 20160405165101
#
# Table name: tag_lots
#
#  id              :integer          not null, primary key
#  tag_lot_number  :integer
#  manufacturer_id :integer
#  prefix          :string(255)
#  start_no        :integer
#  end_no          :integer
#  end_user_type   :integer
#  cost            :integer
#  date_entered    :datetime
#  ordered         :boolean
#  order_date      :datetime
#  received        :boolean
#  received_date   :datetime
#  deleted         :boolean
#  color           :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  user_id         :integer
#
# Foreign Keys
#
#  tl_enduser_fk  (end_user_type => tag_end_user_types.id)
#  tl_man_fk      (manufacturer_id => manufacturers.id)
#

module TagLotsHelper

  # Helper method to return the manufacturer of a given tag lot.
  def manufacturer (tag_lot)
    Manufacturer.find(tag_lot.manufacturer_id).name if tag_lot.manufacturer_id
  end

  # Helper method to return the manufacturer of a given tag lot, excluding a manufacturer with an ID of 0.
  def manufacturer_excluding_id_0 (tag_lot)
    Manufacturer.find(tag_lot.manufacturer_id).name if tag_lot.manufacturer_id and tag_lot.manufacturer_id > 0
  end

  # Helper method to return the end user type of a given tag lot.
  def end_user_type (tag_lot)
    TagEndUserType.find(tag_lot.end_user_type).name if tag_lot.end_user_type
  end

  # Helper method to return the date a tag lot was entered to the app.
  def date_entered (tag_lot)
    tag_lot.date_entered.strftime "%m/%d/%Y" if tag_lot.date_entered
  end

  # Helper method to return the date a tag lot was ordered.
  def order_date (tag_lot)
    tag_lot.order_date.strftime "%m/%d/%Y" if tag_lot.order_date
  end

  # Helper method to return the date a tag lot was received.
  def received_date (tag_lot)
    tag_lot.received_date.strftime "%m/%d/%Y" if tag_lot.received_date
  end

  # Helper method to return the color of a given tag lot.
  def color (tag_lot)
    TagLotColor.find(tag_lot.color).color if tag_lot.color
  end
end

# == Schema Information
# Schema version: 20160405165101
#
# Table name: species
#
#  id                 :integer          not null, primary key
#  common_name        :string(255)
#  species_code       :string(255)
#  position           :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  photo_file_name    :string(255)
#  photo_content_type :string(255)
#  photo_file_size    :integer
#  photo_updated_at   :datetime
#  proper_name        :string(255)
#  other_name         :string(255)
#  family             :string(255)
#  habitat            :text
#  description        :text
#  size               :text
#  food_value         :text
#  name               :string(255)
#  publish            :boolean          default(FALSE)
#  target             :boolean          default(FALSE)
#  ordinal_position   :integer
#

module SpeciesHelper
  def species_grouped_options(object)
    options = {
        'Target' => Species.target.map {|t| [t.common_name, t.id]},
        'Other' => Species.not_target.map {|t| [t.common_name, t.id]}
    }
    grouped_options_for_select(options, object.species_id)
  end
end

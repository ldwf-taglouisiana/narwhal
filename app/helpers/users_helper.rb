# == Schema Information
# Schema version: 20170724200101
#
# Table name: users
#
#  id                                  :integer          not null, primary key
#  email                               :string(255)      default(""), not null
#  encrypted_password                  :string(255)      default(""), not null
#  reset_password_token                :string(255)
#  reset_password_sent_at              :datetime
#  remember_created_at                 :datetime
#  sign_in_count                       :integer          default(0)
#  current_sign_in_at                  :datetime
#  last_sign_in_at                     :datetime
#  current_sign_in_ip                  :string(255)
#  last_sign_in_ip                     :string(255)
#  created_at                          :datetime         not null
#  updated_at                          :datetime         not null
#  first_name                          :string(255)      not null
#  last_name                           :string(255)      not null
#  authentication_token                :string(255)
#  verified                            :boolean          default(FALSE)
#  receive_daily_capture_email_summary :boolean          default(FALSE)
#  contacted                           :boolean          default(FALSE)
#  hidden                              :boolean          default(FALSE)
#  comments                            :text
#  search_vector                       :tsvector         not null
#  angler_id                           :integer
#  is_existing_angler                  :boolean
#
# Indexes
#
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#  index_users_on_search_vector         (search_vector)
#
# Foreign Keys
#
#  fk_rails_c2453f5ff0  (angler_id => anglers.id)
#

module UsersHelper
  # Generates a set of checkboxes for a HABTM assignment in a form.  Labels are properly assigned
  # to the checkboxes and markup is valid HTML.
  # 
  #   habtm_checkboxes(@organizer, :event_ids, @events, :name)
  #
  #   <%= hidden_field_tag "organizer[event_ids][]", "" %>
  #   <% @events.each do |event| -%>
  #     <%= check_box_tag "organizer[event_ids][]", event.id, @organizer.event_ids.include?(event.id), :id => "organizer_events_id_#{event.id}" %>
  #     <%= label_tag "organizer_events_id_#{event.id}", h(event.name) %>
  #   <% end -%>
  def habtm_checkboxes(obj, column, assignment_objects, assignment_object_display_column)
    obj_to_s    = obj.class.to_s.split("::").last.underscore
    field_name  = "#{obj_to_s}[#{column}][]"
    
    html = hidden_field_tag(field_name, "")
    assignment_objects.each do |assignment_obj|
      cbx_id = "#{obj_to_s}_#{column}_#{assignment_obj.id}"
      html += check_box_tag field_name, assignment_obj.id, obj.send(column).include?(assignment_obj.id), :id => cbx_id
      html += label_tag cbx_id, h(assignment_obj.send(assignment_object_display_column))
      html += content_tag(:br)
    end
    html
  end

end

# == Schema Information
# Schema version: 20170724200101
#
# Table name: users
#
#  id                                  :integer          not null, primary key
#  email                               :string(255)      default(""), not null
#  encrypted_password                  :string(255)      default(""), not null
#  reset_password_token                :string(255)
#  reset_password_sent_at              :datetime
#  remember_created_at                 :datetime
#  sign_in_count                       :integer          default(0)
#  current_sign_in_at                  :datetime
#  last_sign_in_at                     :datetime
#  current_sign_in_ip                  :string(255)
#  last_sign_in_ip                     :string(255)
#  created_at                          :datetime         not null
#  updated_at                          :datetime         not null
#  first_name                          :string(255)      not null
#  last_name                           :string(255)      not null
#  authentication_token                :string(255)
#  verified                            :boolean          default(FALSE)
#  receive_daily_capture_email_summary :boolean          default(FALSE)
#  contacted                           :boolean          default(FALSE)
#  hidden                              :boolean          default(FALSE)
#  comments                            :text
#  search_vector                       :tsvector         not null
#  angler_id                           :integer
#  is_existing_angler                  :boolean
#
# Indexes
#
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#  index_users_on_search_vector         (search_vector)
#
# Foreign Keys
#
#  fk_rails_c2453f5ff0  (angler_id => anglers.id)
#

class UsersController < ApplicationController

  include AnglerDataMigrationModule, DatatableActionable

  before_action :set_user, except: [:index, :new, :create]

  def index
    authorize! :read, User

    respond_to do |format|
      format.html {
        results = User.index_list(current_user)
        @index_list = {
          ldwf: {
            users: results[:ldwf],
            active: true,
            title: 'LDWF Users',
            class: 'ldwf'
          },
          anglers: {
            users: results[:anglers],
            active: false,
            title: 'Anglers',
            class: 'anglers'
          },
          unverified: {
            users: results[:unverified],
            active: false,
            title: 'Unverified App Users',
            class: 'unverified'
          },
          admin: {
            users: results[:admin],
            active: false,
            title: 'Admin Users',
            class: 'admin'
          }
        }

      }
      format.json { render json: get_user_json_list }
    end
  end

  def show
    authorize! :read, @user
  end

  def edit
    authorize! :update, @user
    @anglers = @user.angler.try :possible_duplicate_anglers
    prep_user
  end

  def new
    authorize! :create, User
    @user = User.new
    prep_user
  end

  def create
    authorize! :create, User,
    authorize!(:assign_roles, User) if user_params[:roles_users_attributes]
    authorize!(:assign_administrator_role,@user) if user_params[:roles_users_attributes] && user_params[:roles_users_attributes].values.select{|a| a[:role_id] == Role.admin_role.id }.any?

    clean_params = user_params

    temp_password = Devise.friendly_token.first(8)
    clean_params[:password] = temp_password
    clean_params[:password_confirmation] = temp_password

    @user = User.new(clean_params)

    respond_to do |format|
      format.html {
        if @user.save
          @user.send_reset_password_instructions
          redirect_to action: :index
        else
          render action: :new, status: :unprocessable_entity
        end
      }
    end
end

  def update
    authorize! :update, @user
    authorize!(:assign_roles, @user) if user_params[:roles_users_attributes]
    authorize!(:assign_administrator_role,@user) if user_params[:roles_users_attributes] && user_params[:roles_users_attributes].values.select{|a| a[:role_id] == Role.admin_role.id }.any?

    @user.assign_attributes(user_params)
    deliver = @user.verified && @user.changed.include?('verified')
    if @user.save
      if deliver
        UserNotifyMailer.notify_user_verified_email(@user).deliver
      end

      redirect_to users_path, notice: 'User updated.'
    else
      redirect_to edit_user_path(@user), alert: "User cannot be updated<br>#{ @user.errors.full_messages.join("<br>")}.".html_safe
    end
  end

  def destroy
    authorize! :destroy, @user

    if @user == current_user
      redirect_to users_path, notice: 'Can\'t delete yourself.'
    else
      @user.destroy
      redirect_to users_path, notice: 'User deleted.'
    end
  end

  def generate_new_password_email
    authorize! :assist, @user
    if @user.has_role?(:admin) || @user.has_role?(:ldwf)
      @user.send_reset_password_instructions

      flash[:notice] = "Reset password instructions have been sent to #{@user.email}."

    else
      data = { token_override: NESSIE_OAUTH_HASH, email: @user.email, format: :json}.to_json
      url = NESSIE_HOST + '/users/request_reset.json'

      request = make_request_from_url(url, { method: :post, body: data, headers: {'Content-Type' => 'application/json'} })

      if request.code.to_i == 200
        flash[:notice] = "Reset password instructions have been sent to #{@user.email}."
      else
        flash[:error] = "Could not send password reset instructions to #{@user.email}. "
      end
    end

    redirect_to users_path
  end

  private

  def set_user
    @user = User.find(params[:id] || params[:user_id])
  end

  def prep_user
    (Role.all - @user.roles).each do |role|
      @user.roles_users.build(role: role, user: @user)
    end

    NotificationTopic.all.each do |topic|
      known_topic = @user.notification_topics_users.where(notification_topic: topic).first
      @user.notification_topics_users.build(notification_topic: topic, user: @user) if known_topic.nil?

      NotificationCourier.all.each do |courier|
        known_courier = @user.user_couriers.where(notification_topic: topic, notification_courier: courier ).first
        @user.user_couriers.build(notification_topic: topic, notification_courier: courier, user: @user) if known_courier.nil?
      end
    end
  end

  def user_params
    params.require(:user).permit(:id, :first_name, :last_name, :email, :receive_daily_capture_email_summary, :verified, :contacted, :hidden, :comments,
                                 :roles_users_attributes => [:id, :_destroy, :role_id],
                                 :notification_topics_users_attributes => [:id, :_destroy, :notification_topic_id],
                                 :user_couriers_attributes => [:id, :_destroy, :notification_courier_id, :notification_topic_id])
  end

  def get_user_json_list
    data_params = data_table_params
    data_params[:verified] = (params[:verified] == 'true')

    # get the data from the capture model
    User.get_data_tables_list(data_params)
  end

end

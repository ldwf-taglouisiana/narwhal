# == Schema Information
# Schema version: 20160405165101
#
# Table name: how_to_tags
#
#  id          :integer          not null, primary key
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class HowToTagsController < ApplicationController

  before_action :set_how_to, only: [:show, :edit, :update, :destroy]

  # GET /how_to_tags
  def index
    authorize! :read, HowToTag
    @how_to_tags = HowToTag.all
  end

  # GET /how_to_tags/1
  def show
    authorize! :read, @how_to_tag
  end

  # GET /how_to_tags/new
  def new
    authorize! :create, HowToTag
    @how_to_tag = HowToTag.new
  end

  # GET /how_to_tags/1/edit
  def edit
    authorize! :update, @how_to_tag
  end

  # POST /how_to_tags
  def create
    authorize! :create, HowToTag
    @how_to_tag = HowToTag.new(how_to_params)

    respond_to do |format|
      if @how_to_tag.save
        flash[:notice] = 'How to was successfully updated.'
        format.html { redirect_to @how_to_tag, notice: 'How to tag was successfully created.' }
      else
        flash[:error] = 'Could not update.'
        format.html { render action: "new" }
      end
    end
  end

  # PUT /how_to_tags/1
  def update
    authorize! :update, @how_to_tag
    respond_to do |format|
      if @how_to_tag.update_attributes(how_to_params)
        flash.now[:notice] = 'How to was successfully updated.'
        format.html { render action: "edit" }
      else
        flash.now[:error] = 'Could not update.'
        format.html { render action: "edit" }
      end
    end
  end

  # DELETE /how_to_tags/1
  def destroy
    authorize! :destroy, @how_to_tag
    @how_to_tag.destroy
    respond_to do |format|
      format.html { redirect_to how_to_tags_url }
      format.json { head :no_content }
    end
  end

  private

  def set_how_to
    @how_to_tag = HowToTag.find(params[:id])
  end

  def how_to_params
    params.require(:how_to_tag).permit(:description)
  end
end

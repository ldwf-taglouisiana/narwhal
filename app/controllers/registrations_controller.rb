class RegistrationsController < Devise::RegistrationsController

  skip_authorization_check

  # We are overriding this so we can remove the password params if thery are
  # blank.
  def update
    # required for settings form to submit when password is left blank
    if params[:user][:password].blank?
      params[:user].delete(:password)
      params[:user].delete(:password_confirmation)
    end

    if current_user.update_attributes(params[:user])
      set_flash_message :notice, :updated

      # Sign in the user bypassing validation in case his password changed
      sign_in @user, :bypass => true
      redirect_to after_update_path_for(@user)
    else
      render 'edit'
    end
  end
end

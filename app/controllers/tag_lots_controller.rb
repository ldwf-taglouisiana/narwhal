# == Schema Information
# Schema version: 20160405165101
#
# Table name: tag_lots
#
#  id              :integer          not null, primary key
#  tag_lot_number  :integer
#  manufacturer_id :integer
#  prefix          :string(255)
#  start_no        :integer
#  end_no          :integer
#  end_user_type   :integer
#  cost            :integer
#  date_entered    :datetime
#  ordered         :boolean
#  order_date      :datetime
#  received        :boolean
#  received_date   :datetime
#  deleted         :boolean
#  color           :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  user_id         :integer
#
# Foreign Keys
#
#  tl_enduser_fk  (end_user_type => tag_end_user_types.id)
#  tl_man_fk      (manufacturer_id => manufacturers.id)
#

class TagLotsController < ApplicationController

  include DatatableActionable

  before_action :set_tag_lot, :only => [:show,:edit,:update,:destroy]

  # GET /tag_lots
  # GET /tag_lots.json
  def index
    authorize! :read, TagLot
    @tag_lots = TagLot.accessible_by_user(current_user).where('manufacturer_id != 0').order('created_at desc').page params[:page]

    respond_to do |format|
      format.html
      format.json { render json: get_tag_lot_json_list }
    end
  end

  # GET /tag_lots/1
  # GET /tag_lots/1.json
  def show
    authorize! :read, @tag_lot
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @tag_lot }
    end
  end

  # GET /tag_lots/new
  # GET /tag_lots/new.json
  def new
    authorize! :create, TagLot
    @tag_lot = TagLot.new
    @tag_lot.tag_lot_number = TagLot.count + 1

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @tag_lot }
    end
  end

  # GET /tag_lots/1/edit
  def edit
    authorize! :update, @tag_lot
  end

  # POST /tag_lots
  # POST /tag_lots.json
  def create
    authorize! :create, TagLot

    @tag_lot = TagLot.new(tag_lot_params)
    @tag_lot.user = current_user

    respond_to do |format|
      format.html {
        if @tag_lot.save
          redirect_to @tag_lot, notice: 'Tag lot was successfully created.'
        else
          render action: :new, error: 'There was an error saving the tag lot.'
        end
      }
    end
  end

  # PUT /tag_lots/1
  # PUT /tag_lots/1.json
  def update
    authorize :update, @tag_lot
    respond_to do |format|
      if @tag_lot.update_attributes(tag_lot_params)
        format.html { redirect_to @tag_lot, notice: 'Tag lot was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @tag_lot.errors, status: :unprocessable_entity }
      end
    end
  end

  def merge_tag_lots
    authorize! :update, TagLot
    @tag_lots = TagLot.order('id').page params[:page]
    respond_to do |format|
      format.html
    end
  end

  # DELETE /tag_lots/1
  # DELETE /tag_lots/1.json
  def destroy
    authorize! :destroy, @tag_lot
    @tag_lot = TagLot.find(params[:id])

    respond_to do |format|
      format.html { redirect_to tag_lots_url }
      format.json { head :no_content }
    end
  end

  private

  def set_tag_lot
    @tag_lot = TagLot.find(params[:id])
  end

  def tag_lot_params
    p = params.require(:tag_lot).permit(:tag_lot_number, :manufacturer_id, :prefix, :start_no, :end_no, :end_user_type,
                                    :cost, :color, :ordered, :received, :date_entered, :received_date, :order_date, :date_entered)

    p[:date_entered] = Chronic.parse(p[:date_entered])
    p[:order_date] = Chronic.parse(p[:order_date])
    p[:received_date] = Chronic.parse(p[:received_date])

    p
  end

  def get_tag_lot_json_list
    TagLot.get_data_tables_list(data_table_params)
  end
end

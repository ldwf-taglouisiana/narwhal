# == Schema Information
# Schema version: 20160405165101
#
# Table name: volunteer_time_logs
#
#  id            :integer          not null, primary key
#  start         :datetime         not null
#  end           :datetime         not null
#  verified      :boolean          default(FALSE)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  ldwf_entered  :boolean          default(FALSE)
#  ldwf_edited   :boolean          default(FALSE)
#  uuid          :string
#  recent        :boolean          default(TRUE), not null
#  search_vector :tsvector         not null
#  angler_id     :integer          not null
#
# Indexes
#
#  index_volunteer_time_logs_on_recent         (recent)
#  index_volunteer_time_logs_on_search_vector  (search_vector)
#
# Foreign Keys
#
#  fk_rails_b894ce2542  (angler_id => anglers.id)
#

class VolunteerTimeLogsController < ApplicationController

  include DatatableActionable

  before_action :set_volunteer_time_log, only: [:show, :edit, :update, :destroy, :captures]

  # GET /volunteer_time_logs
  # GET /volunteer_time_logs.json
  def index
    authorize! :read, VolunteerTimeLog
    respond_to do |format|
      format.html # index.html.erb
      format.json {
        @items = get_data_table_list(params)
        render json: @items
      }
      format.xlsx {
        @items = get_data_table_list(params)
        @items = JSON.parse(@items)
      }
    end
  end

  def show
    authorize! :read, @volunteer_time_log
  end

  def new
    authorize! :create, VolunteerTimeLog
    @volunteer_time_log = VolunteerTimeLog.new
  end

  def edit
    authorize! :update, @volunteer_time_log
  end

  def create
    authorize! :create, VolunteerTimeLog
    @volunteer_time_log = VolunteerTimeLog.new(time_safe_params)
    @volunteer_time_log.ldwf_entered = true

    respond_to do |format|
      if @volunteer_time_log.save
        format.html { redirect_to volunteer_time_logs_path, notice: 'Volunteer time log was successfully created.' }
      else
        format.html {
          render action: :new, error: 'Could not create time log'
        }
      end
    end
  end

  def update
    authorize! :update, @volunteer_time_log
    @volunteer_time_log.ldwf_edited = true

    respond_to do |format|
      if @volunteer_time_log.update_attributes(time_safe_params)
        format.html { redirect_to volunteer_time_logs_url, notice: 'Volunteer time log was successfully updated.' }
      else
        format.html {
          render action: :edit, error: 'Could not update time log'
        }
      end
    end
  end

  def destroy
    authorize! :destroy, @volunteer_time_log
    @volunteer_time_log.destroy

    respond_to do |format|
      format.html { redirect_to volunteer_time_logs_url }
    end
  end

  #This view shows captures associated with a given volunteer time log
  def captures
    authorize! :read, @volunteer_time_log
    render :json => get_captures_data_table_list(params, @volunteer_time_log)
  end

  private

  def set_volunteer_time_log
    @volunteer_time_log = VolunteerTimeLog.find(params[:id])
  end

  def volunteer_time_params
    params.require(:volunteer_time_log).permit(:angler_id, :end, :start, :verified)
  end

  def time_safe_params
    fixed_params = volunteer_time_params
    fixed_params[:end]         = Chronic.parse(fixed_params[:end])
    fixed_params[:start]       = Chronic.parse(fixed_params[:start])
    fixed_params[:ldwf_edited] = true
    fixed_params
  end

  def get_data_table_list(params)
    data_params = data_table_params
    data_params[:all] = params[:all] == 'true'
    data_params[:min_captures] = 1

    VolunteerTimeLog.data_tables_json(data_params)

  end

  def get_captures_data_table_list(params, volunteer_time_log)
    data_params = data_table_params
    data_params[:verified] = true
    data_params[:where] = 'capture_date <@ tstzrange(?,?,\'[]\') AND angler_id = ?'
    data_params[:binds] = [volunteer_time_log.start.iso8601, volunteer_time_log.end.iso8601, volunteer_time_log.angler_id]
    Capture.datatable_list(data_params)
  end


end

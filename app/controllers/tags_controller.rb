# == Schema Information
# Schema version: 20160405165101
#
# Table name: tags
#
#  id              :integer          not null, primary key
#  tag_no          :string(255)
#  tag_lot_id      :integer
#  tag_request_id  :integer
#  deleted         :boolean
#  unassign_option :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  old_anlger_id   :integer
#  prefix          :string(255)
#  active          :boolean          default(TRUE)
#  assigned_at     :datetime
#  recent          :boolean          default(TRUE), not null
#  search_vector   :tsvector         not null
#  angler_id       :integer
#
# Indexes
#
#  index_tags_on_angler_id      (angler_id)
#  index_tags_on_recent         (recent)
#  index_tags_on_search_vector  (search_vector)
#  index_tags_on_tag_lot_id     (tag_lot_id)
#  index_tags_on_tag_no         (tag_no) UNIQUE
#  tags_prefix_idx              (prefix)
#
# Foreign Keys
#
#  fk_rails_e4069f0b05  (angler_id => anglers.id)
#  tag_lot_fk           (tag_lot_id => tag_lots.id)
#

class TagsController < ApplicationController

  include DatatableActionable

  before_action :auth_autocomplete, only: [:autocomplete_tag_number, :autocomplete_tag_number_any]


  autocomplete :tag, :number, :display_value => :tag_no, :selected_value => :tag_no, extra_data: [:angler_id, :angler_name], :cascade => true, :search_action => :autocomplete
  autocomplete :tag, :number_any, :display_value => :tag_no, :selected_value => :tag_no, extra_data: [:angler_id, :angler_name], :cascade => true, :search_action => :autocomplete_any

  # GET /tags
  # GET /tags.json
  def index
    authorize! :read, Tag
    #@tags = Tag.where('tag_no is not null and deleted is not true').order(:id).page params[:page]

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: Tag.get_data_tables_list(data_table_params) }
    end
  end

  def deactivate
    authorize! :update, Tag
    # this is empty because there is nothing to do to prep this view. The router requires a controller method to route correctly
  end

  def assign
    authorize! :update_assignment, Tag
    @angler_id = params[:angler_id]
  end

  #this is empty because there is nothing to do to prep this view. The router requires a controller method to route correctly
  def unassign
    authorize! :update_assignment, Tag

  end

  def commit_assignment
    authorize! :update_assignment, Tag
    success = false
    error_message = nil

    begin

      # prevent too many tags from being changed if the range does not match the requested number
      if Tag.with_number_range(params[:start_tag_number], params[:end_tag_number]).count > params[:number_tags].to_i
        raise StandardError.new("Tag range #{params[:start_tag_number]} - #{params[:end_tag_number]} exceeds number of tags requested to be changed: #{params[:number_tags]}");
      end

      Tag.assign_tags(params[:angler_id], params[:start_tag_number], params[:end_tag_number], current_user)
      success = true
    rescue Exception => e
      error_message = e.message
    end

    respond_to do |format|
      format.html {
        if success
          flash[:success] = 'Tags have been successfully assigned.'
        else
          flash[:error] = error_message
        end

        render action: :assign
      }
    end
  end

  def commit_unassignment
    authorize! :update_assignment, Tag
    success = false
    error_message = nil

    begin

      # prevent too many tags from being changed if the range does not match the requested number
      if Tag.with_number_range(params[:start_tag_number], params[:end_tag_number]).count > params[:number_tags].to_i
        raise StandardError.new("Tag range #{params[:start_tag_number]} - #{params[:end_tag_number]} exceeds number of tags requested to be changed: #{params[:number_tags]}");
      end

      Tag.unassign_tags(params[:start_tag_number], params[:end_tag_number], current_user)
      success = true
    rescue => e
      error_message = e.message
    end

    respond_to do |format|
      format.html {
        if success
          flash[:success] = 'Tags have been successfully unassigned.'
        else
          flash[:error] = error_message
        end

        render action: :unassign
      }
    end
  end


  def commit_deactivate
    authorize! :update, Tag
    tag = Tag.where(tag_no: params[:tag_number]).first

    respond_to do |format|
      if tag.nil?
        flash[:error] = 'Tag does not exist.'

      elsif tag.update_attribute(:active, false)
        flash[:notice] = 'Tag was successfully deactivated.'

      else
        flash[:error] = 'Tag could not be deactivated.'
      end

      format.html { redirect_to deactivate_tags_path }
    end
  end

  def search
    authorize! :read, Tag
    @tags = Tag.where("tag_no ~* '#{params[:tag][:tag_no]}' and angler_id ~* '#{params[:tag][:angler_id]}'").order('tag_no')
    @search_count =@tags.count
    @tags = @tags.page params[:page]
    respond_to do |format|
      format.html { render action: "index" }
    end
  end

  def assigned
    authorize! :read, Tag
    respond_to do |format|
      format.html
      format.json {
        data_parms = data_table_params
        data_parms[:date] = params[:date]
        render json: TagAssignment.get_data_tables_list(data_parms)
      }
    end
  end

  def auth_autocomplete
    authorize! :read, Tag
  end
end

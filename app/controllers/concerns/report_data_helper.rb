module ReportDataHelper
  extend ActiveSupport::Concern

  # the whitelist of group by options
  GROUP_BY_WHITELIST = {
      :species => 'species_id',
      :angler => 'angler_id'
  }

  ####################################################
  # tagged fish report
  # Returns the species and count of each of those species caught with ther params conditions (angler date etc)
  def get_tagged_fish_list(params)

    # setup the SQL params
    params = data_helper_param_cleaner(params)
    angler_id = Angler.from_ambiguous_id(params[:angler_id]).try(:id)
    start_date = params[:start_date]
    end_date = params[:end_date]
    group_by = GROUP_BY_WHITELIST[params[:group_by].downcase.to_sym]
    table = params[:recapture] == 'true' ? 'recaptures' : 'captures'

    # determine if we are grouping by an angler
    is_angler_group_by = (group_by == GROUP_BY_WHITELIST[:angler])
    # we join on a final table depending on the group by
    final_table = is_angler_group_by ? 'anglers' : 'species'
    # the column that is returned as the "title" for that row
    final_table_title_column = is_angler_group_by ? 'anglers.angler_id' : 'species.common_name'

    # build the query string
    query = <<-SQL
          WITH items (object_id, count) AS (
            SELECT
              #{group_by},
              COUNT(*)
            FROM
              #{table}
            WHERE
              angler_id = COALESCE(?,angler_id)
              AND
              capture_date <@ tstzrange(?,?,'[]')
            GROUP BY #{group_by}
          ),
          total(total_count) as (
            select
              sum(count)
            from
              items
          )
          SELECT
            #{final_table_title_column} as "title",
            count,
            total_count
          FROM
            #{final_table} inner join items ON items.object_id = #{final_table}.id
            CROSS JOIN total
          ORDER BY
            count desc
    SQL
    # execute and return the query results
    ActiveRecord::Base.connection.execute(ActiveRecord::Base.send(:sanitize_sql_array, [query, angler_id, start_date, end_date])).to_a

  end

  def download_tagged_fish_list(params)
    clean_params = params.permit(:start_date, :end_date, :count)
    params = data_helper_param_cleaner(params)

    # create the file name
    min = clean_params[:start_date].blank? ? 'First' : clean_params[:start_date]
    max = clean_params[:end_date].blank?  ? 'Last' : clean_params[:end_date]
    filename = "Tagged_fish_from_#{min}_to_#{max}.xlsx"

    # create the token
    file_token = generate_download_token

    # create the download item with the token, the filename, and the if of the user that created it
    RequestedDownload.create!(token: file_token, filename: filename, user_id: current_user.id)

    DownloadReportJob.perform_later(DownloadReportJob::REPORT_TYPES[:tagged_fish], params, file_token)

    file_token
  end

  def tagged_fish_list_to_xlsx(params)
    is_angler_group_by = (params[:group_by].downcase.to_sym == :angler)

    items = get_tagged_fish_list(params)

    Axlsx::Package.new do |p|
      p.workbook.add_worksheet(name: 'Tagged Fish') do |sheet|
        sheet.add_row [is_angler_group_by ? 'Angler ID' : 'Fish Species', 'Count']
        items.each do |datum|
          sheet.add_row [datum['title'], datum['count']]
        end
      end
    end
  end

  ####################################################
  # captures by angler

  def get_captures_by_angler_list(params)
    get_events_by_angler_list_helper(params)
  end

  def download_captures_by_angler_list(params)
    download_events_by_angler_list_helper(params)
  end

  def captures_by_angler_list_to_xlsx(params)
    events_by_angler_list_to_xlsx_helper(params)
  end

  ####################################################
  # recaptures by angler

  #TODO this seems redundant with line 102, why not just pass the parameter of t/f and only have 1 call?
  def get_recaptures_by_angler_list(params)
    get_events_by_angler_list_helper(params, true)
  end

  def download_recaptures_by_angler_list(params)
    download_events_by_angler_list_helper(params, true)
  end

  def recaptures_by_angler_list_to_xlsx(params)
    events_by_angler_list_to_xlsx_helper(params, true)
  end

  ####################################################
  # unused kits

  def get_unused_kits_list(params)
    TagAssignment
        .includes(:angler)
        .unused_kits
        .where('assignment_at <@ daterange(?,?,\'[]\')', params[:start_date], params[:end_date])
        .where('angler_number = COALESCE(?,angler_number)', params[:angler_id].presence)
  end

  def download_unused_kits_list(params)
    start_date = params[:start_date].try(:gsub, /__\/__\/____/, '').blank? ? Time.now.beginning_of_day.iso8601 : Chronic.parse(params[:start_date]).try(:beginning_of_day).try(:iso8601)
    end_date   = params[:end_date].try(:gsub, /__\/__\/____/, '').blank? ? Time.now.end_of_day.iso8601 : Chronic.parse(params[:end_date]).try(:end_of_day).try(:iso8601)
    filename   = "Unused_kits_from_#{start_date}_to_#{end_date}.xlsx"

    # create the token
    file_token = generate_download_token

    # create the download item with the token, the filename, and the if of the user that created it
    RequestedDownload.create!(token: file_token, filename: filename, user_id: current_user.id)

    DownloadReportJob.perform_later(DownloadReportJob::REPORT_TYPES[:unused_kits], params, file_token)

    file_token
  end

  def unused_kits_list_to_xlsx(params)
    items = get_unused_kits_list(params)

    # build the workbook
    Axlsx::Package.new do |p|
      p.workbook.add_worksheet(name: "Unused kits") do |sheet|
        sheet.add_row ['Angler ID', 'First Name', 'Last Name', 'Assignment At', 'Start Tag', 'End Tag', '# of Tags']

        items.each do |assignment|
          angler = assignment.angler
          sheet.add_row [angler.angler_id, angler.first_name, angler.last_name, assignment.assignment_at.strftime('%m/%d/%Y'), assignment.start_tag, assignment.end_tag, assignment.number_of_tags]
        end
      end
    end
  end

  ####################################################
  # days at large

  def get_days_at_large_list(params)
    # permit the params that we are looking for
    params = params.permit(:angler_id, :species_id, :time_frame, :start_date, :end_date)
    params = data_helper_param_cleaner(params)

    # initialize the params
    angler_id   = params[:angler_id]
    species_id  = params[:species_id].blank?  ? nil : params[:species_id].to_i
    time_frame  = params[:time_frame].blank?  ? 365 : params[:time_frame]
    start_date  = params[:start_date]
    end_date    = params[:end_date]

    # setup the SQL query
    query = <<-SQL
      WITH items as (
        SELECT
          A.id    as "id",
          anglers.angler_id       as "angler_id",
          anglers.first_name      as "first_name",
          anglers.last_name       as "last_name" ,
          tags.tag_no             as "tag_number",
          A.capture_date as "capture_date",
          B.capture_date as "recapture_date",
          species.common_name     as "species_name",
          A.species_id   as "species_id",
          (B.capture_date::date - A.capture_date::date) as "days_at_large"
        FROM
          captures A
            LEFT JOIN anglers on A.angler_id = anglers.id
            LEFT JOIN species on A.species_id = species.id
            LEFT JOIN tags on A.tag_id = tags.id
            LEFT JOIN (
            -- get the first recapture for a given tag.
              select
                *
              from (
                select *, row_number() over (partition BY recaptures.tag_id ORDER BY recaptures.capture_date ASC) AS row_num from recaptures
              ) C
              where C.row_num = 1
            ) B on A.tag_id = B.tag_id
        WHERE
          B.id IS NOT NULL
          AND A.angler_id = COALESCE(?, A.angler_id)
          AND A.capture_date <@ tstzrange(?,?,'[]')
          #{'AND A.species_id = ?' unless species_id.nil?} -- we are going to ignore this where if there was not species_is given
      )
      SELECT
        id,
        angler_id,
        first_name,
        last_name,
        tag_number,
        species_name,
        to_char(capture_date,'MM/DD/YYYY') as "capture_date", -- convert to a normal date for US users
        extract(epoch from capture_date) as "epoch_time", -- used for sorting in the JS tables
        to_char(recapture_date,'MM/DD/YYYY') as "recapture_date", -- convert to a normal date for US users
        extract(epoch from recapture_date) as "recapture_epoch_time", -- used for sorting in the JS tables
        days_at_large
      FROM
        items
      WHERE
        days_at_large > 0 AND days_at_large < ?
    SQL

    # combine the sql and the bind values
    sql_params = [query, angler_id, start_date, end_date, time_frame]

    # add the species_id if one was provided
    sql_params = sql_params + [species_id] unless species_id.nil?

    ActiveRecord::Base.connection.execute(ActiveRecord::Base.send(:sanitize_sql_array, sql_params)).to_a
  end

  def download_days_at_large_list(params)
    # create the file name
    filename = 'Days_At_Large.xlsx'

    # create the token
    file_token = generate_download_token

    # create the download item with the token, the filename, and the if of the user that created it
    RequestedDownload.create!(token: file_token, filename: filename, user_id: current_user.id)

    DownloadReportJob.perform_later(DownloadReportJob::REPORT_TYPES[:days_at_large], params, file_token)

    file_token
  end

  def days_at_large_list_to_xlsx(params)
    items = get_days_at_large_list(params)

    # build the workbook
    Axlsx::Package.new do |p|
      p.workbook.add_worksheet(name: "Days_At_Large") do |sheet|
        sheet.add_row ['Angler ID', 'First Name', 'Last Name', 'Tag Number', 'Species', 'Capture Date', 'Days At Large']

        items.each do |datum|
          sheet.add_row [datum['angler_id'], datum['first_name'], datum['last_name'], datum['tag_number'], datum['species_name'], datum['capture_date'], datum['days_at_large']]
        end
      end
    end
  end

  ####################################################
  # get random fish event

  def get_random_event_list(params)
    params = params.permit(:start_date, :end_date, :capture_type, :limit)
    params = data_helper_param_cleaner(params)

    # setup the sql params
    limit       = params[:limit].blank? ? 1 : params[:limit]
    start_date  = params[:start_date]
    end_date    = params[:end_date]

    # get the table that we are going to look at
    table       = (params[:capture_type] == 'Recaptures') ? 'recaptures' : 'captures'

    # build the SQL query
    query = <<-SQL
      SELECT
        #{table}.id         AS "id",
        anglers.angler_id  AS "angler_id",
        anglers.first_name  AS "first_name",
        anglers.last_name   AS "last_name",
        species.common_name AS "species_name",
        tags.tag_no         AS "tag_number",
        to_char(#{table}.capture_date,'MM/DD/YYYY') as "capture_date", -- convert to a normal date for US users
        extract(epoch from #{table}.capture_date) as "epoch_time" -- used for sorting in the JS tables
      FROM
        #{table}
          LEFT JOIN anglers on #{table}.angler_id = anglers.id
          LEFT JOIN species on #{table}.species_id = species.id
          LEFT JOIN tags on #{table}.tag_id = tags.id
      WHERE
        #{table}.capture_date <@ tstzrange(?,?,'[]')
      ORDER BY
        random()
      LIMIT
        ?
    SQL

    ActiveRecord::Base.connection.execute(ActiveRecord::Base.send(:sanitize_sql_array, [query, start_date, end_date, limit])).to_a
  end

  ####################################################
  # get data check

  def get_data_check_list(params, xlsx = false)
    params = data_helper_param_cleaner(params)

    # setup the sql params
    user_id     = params[:user_id].blank? ? nil : params[:user_id].to_i
    start_date  = params[:start_date]
    end_date    = params[:end_date]

    # generate the smaller sql sets
    capture_activity_query = data_check_fish_event_sql(false, xlsx)
    recapture_activity_query = data_check_fish_event_sql(true, xlsx)
    tag_activity_query = data_check_tag_sql
    angler_activity_query = data_check_angler_sql

    # combine the smaller queries into one
    query = <<-SQL
      SELECT
        to_json(t) AS "data"
      FROM (
        SELECT
              (#{capture_activity_query}) AS "captures",
              (#{recapture_activity_query}) AS "recaptures",
              (#{tag_activity_query}) AS "tags",
              (#{angler_activity_query}) AS "anglers"
      ) AS t
    SQL

    # build the array to sanitize and execute
    sanitized_sql = [query, start_date, end_date, user_id, start_date, end_date, user_id, start_date, end_date, user_id, start_date, end_date, user_id]
    json = ActiveRecord::Base.connection.execute(ActiveRecord::Base.send(:sanitize_sql_array, sanitized_sql))[0]['data']

    # the sql will return a json string that needs to be parsed
    JSON.parse(json)
  end

  def download_data_check_list(params)
    clean_params = params.permit(:start_date, :end_date)

    # create the file name
    start_date = clean_params[:start_date].try(:gsub, /__\/__\/____/, '').blank? ? Time.now.beginning_of_day.iso8601 : Chronic.parse(clean_params[:start_date]).try(:beginning_of_day).try(:iso8601)
    end_date   = clean_params[:end_date].try(:gsub, /__\/__\/____/, '').blank? ? Time.now.end_of_day.iso8601 : Chronic.parse(clean_params[:end_date]).try(:end_of_day).try(:iso8601)
    filename   = "Data_check_from_#{start_date}_to_#{end_date}.xlsx"

    # create the token
    file_token = generate_download_token

    # create the download item with the token, the filename, and the if of the user that created it
    RequestedDownload.create!(token: file_token, filename: filename, user_id: current_user.id)

    DownloadReportJob.perform_later(DownloadReportJob::REPORT_TYPES[:data_check], params, file_token)

    file_token
  end

  def data_check_list_to_xlsx(params)
    data = get_data_check_list(params, true)

    # build the workbook
    Axlsx::Package.new do |p|

      #######################################
      ## Build the captures worksheet
      p.workbook.add_worksheet(name: 'Captures') do |sheet|
        sheet.add_row ['User Name', 'Action', 'Activity Date' 'Species Name', 'Angler ID', 'Angler Name', 'Tag Number', 'Capture Date', 'Location', 'Time Of Day', 'Length', 'Length Range', 'Fish Condition', 'Latitude', 'Longitude']

        data['captures'].each do |datum|
          sheet.add_row [datum['user_name'], datum['activity_type'], datum['activity_date'], datum['species'], datum['angler_id'], datum['angler_name'], datum['tag_number'], datum['capture_date'], datum['location'], datum['time_of_day'], datum['length'], datum['length_range'], datum['fish_condition'], datum['latitude'], datum['longitude']]
        end
      end

      #######################################
      ## Build the recaptures worksheet
      p.workbook.add_worksheet(name: 'Recaptures') do |sheet|
        sheet.add_row ['User Name', 'Action', 'Activity Date' 'Species Name', 'Angler ID', 'Angler Name', 'Tag Number', 'Capture Date', 'Location', 'Time Of Day', 'Length', 'Length Range', 'Fish Condition', 'Latitude', 'Longitude']

        data['recaptures'].each do |datum|
          sheet.add_row [datum['user_name'], datum['activity_type'], datum['activity_date'], datum['species'], datum['angler_id'], datum['angler_name'], datum['tag_number'], datum['capture_date'], datum['location'], datum['time_of_day'], datum['length'], datum['length_range'], datum['fish_condition'], datum['latitude'], datum['longitude']]
        end
      end

      #######################################
      ## Build the tags worksheet
      p.workbook.add_worksheet(name: 'Tags') do |sheet|
        sheet.add_row ['User Name', 'Action', 'Activity Date', 'Angler ID', 'Angler Name', 'Start Tag', 'End Tag', 'Number Of Tags']

        data['tags'].each do |datum|
          sheet.add_row [datum['user_name'], datum['activity_type'], datum['activity_date'], datum['angler_id'], datum['angler_name'], datum['start_tag'], datum['end_tag'], datum['number_of_tags']]
        end
      end

      #######################################
      ## Build the anglers worksheet
      p.workbook.add_worksheet(name: 'Anglers') do |sheet|
        sheet.add_row ['User Name', 'Action', 'Activity Date', 'Angler ID', 'Angler Name', 'Angler Email']

        data['anglers'].each do |datum|
          sheet.add_row [datum['user_name'], datum['activity_type'], datum['activity_date'], datum['angler_id_string'], datum['angler_name'], datum['angler_email']]
        end
      end

    end
  end

  ####################################################
  # sql_dump

  def download_sql_dump(params)
    filename   = "#{Time.now.strftime('%Y_%m_%d')}_narwhal_sql_dump.7z"

    # create the token
    file_token = generate_download_token

    # create the download item with the token, the filename, and the if of the user that created it
    RequestedDownload.create!(token: file_token, filename: filename, user_id: current_user.id)

    if params[:csv]
      DownloadReportJob.perform_later(DownloadReportJob::REPORT_TYPES[:sql_dump_csv], params, file_token)
    else
      DownloadReportJob.perform_later(DownloadReportJob::REPORT_TYPES[:sql_dump], params, file_token)
    end

    file_token
  end

  ####################################################
  # banquet

  def get_banquet_list(params)
    params = data_helper_param_cleaner params
    result = []
    Angler.banquet(start_date: params[:start_date], end_date: params[:end_date]).each do |angler|
      captures = Capture.in_time_range(params[:start_date] || Capture.this_season.first, params[:end_date]).where(angler_id: angler.id)

      item = {
          angler_id: angler.angler_id,
          capture_count: captures.count,
          recapture_count: Recapture.where("tag_id in (#{captures.select(:tag_id).to_sql})").count,
          species: captures.target_species_counts_h,
          contact: {
              first_name: angler.first_name.presence || 'NA',
              last_name: angler.last_name.presence || 'NA',
              street: angler.street.presence || 'NA',
              suite: angler.suite.presence || 'NA',
              city: angler.city.presence || 'NA',
              state: angler.state.presence || 'NA',
              zip: angler.zip.presence || 'NA',
              phone_1: angler.phone_number_1.presence || 'NA',
              phone_2: angler.phone_number_2.presence || 'NA',
              email: angler.email.presence || 'NA',
              shirt_size: angler.shirt_size_name.presence || 'NA'
          }
      }

      result << item
    end

    result
  end

  def download_banquet_list(params)
    start_date = params[:start_date].blank? ? Time.now.beginning_of_day.iso8601 : Chronic.parse(params[:start_date]).try(:beginning_of_day).try(:iso8601)
    end_date   = params[:end_date].blank? ? Time.now.end_of_day.iso8601 : Chronic.parse(params[:end_date]).try(:end_of_day).try(:iso8601)
    filename   = "banquet_anglers_from_#{start_date}_to_#{end_date}.xlsx"

    # create the token
    file_token = generate_download_token

    # create the download item with the token, the filename, and the if of the user that created it
    RequestedDownload.create!(token: file_token, filename: filename, user_id: current_user.id)

    DownloadReportJob.perform_later(DownloadReportJob::REPORT_TYPES[:banquet], params, file_token)

    file_token
  end

  def banquet_to_xlsx(params)
    params = data_helper_param_cleaner(params)
    header = %w(Angler.ID	Captures Recaptures Redfish	Red.Snapper Speckled.Trout Yellowfin.Tuna First.Name Last.Name Address.1 Address.2 City State Zip Phone.1	Phone.2	Email	Tshirt.Size)

    Axlsx::Package.new do |p|
      wb = p.workbook
      wb.add_worksheet(:name => 'Anglers') do |sheet|
        sheet.add_row header

        get_banquet_list(params).each do |angler_hash|
          sheet.add_row [
                            angler_hash[:angler_id],
                            angler_hash[:capture_count],
                            angler_hash[:recapture_count],

                            (angler_hash[:species].select{|s| s[:name] == 'Redfish' }.try(:[],0).try(:[],:count) || 0),
                            (angler_hash[:species].select{|s| s[:name] == 'Red Snapper' }.try(:[],0).try(:[],:count) || 0),
                            (angler_hash[:species].select{|s| s[:name] == 'Speckled Trout' }.try(:[],0).try(:[],:count) || 0),
                            (angler_hash[:species].select{|s| s[:name] == 'Yellowfin Tuna' }.try(:[],0).try(:[],:count) || 0),

                            angler_hash[:contact][:first_name],
                            angler_hash[:contact][:last_name],
                            angler_hash[:contact][:street],
                            angler_hash[:contact][:suite],
                            angler_hash[:contact][:city],
                            angler_hash[:contact][:zip],
                            angler_hash[:contact][:phone_1],
                            angler_hash[:contact][:phone_2],
                            angler_hash[:contact][:email],
                            angler_hash[:contact][:shirt_size],
                        ]
        end
      end
    end
  end

  def download_angler_info(params)
    clean_params = params.permit(:start_date, :end_date, :minimum_captures)
    params = data_helper_param_cleaner(params)

    # create the file name
    min = clean_params[:start_date].blank? ? 'First' : clean_params[:start_date]
    max = clean_params[:end_date].blank?  ? 'Last' : clean_params[:end_date]
    filename = "Angler_info_from_#{min}_to_#{max}.xlsx"

    # create the token
    file_token = generate_download_token

    # create the download item with the token, the filename, and the if of the user that created it
    RequestedDownload.create!(token: file_token, filename: filename, user_id: current_user.id)

    DownloadReportJob.perform_later(DownloadReportJob::REPORT_TYPES[:angler_info], params, file_token)

    file_token
  end

  def angler_info_xlsx(params)
    query = <<-SQL
      SELECT * from anglers
      WHERE id IN (
        SELECT angler_id
        FROM captures
        WHERE capture_date >= ? AND capture_date < ?
        GROUP BY angler_id HAVING COUNT(*) >= ?
      );
    SQL
    results = ActiveRecord::Base.connection.execute(ActiveRecord::Base.send(:sanitize_sql_array, [query, params[:start_date], params[:end_date], params[:minimum_captures]])).to_a
    header = ['Angler Id', 'First Name', 'Last Name', 'Street', 'City', 'State', 'Zip', 'Phone 1', 'Phone 2', 'email']


    Axlsx::Package.new do |p|
      wb = p.workbook
      wb.add_worksheet(:name => 'Anglers') do |sheet|
        sheet.add_row header
        results.each do |angler|
          sheet.add_row [
                          angler['angler_id'],
                          angler['first_name'],
                          angler['last_name'],
                          angler['street'],
                          angler['city'],
                          angler['state'],
                          angler['zip'],
                          angler['phone_number_1'],
                          angler['phone_number_2'],
                          angler['email']
                        ]
        end
      end
    end

  end

  private

  ######################################################################################################################
  ## HELPERS
  ######################################################################################################################

  ####################################################
  # captures and recaptures by angler helpers

  def get_events_by_angler_list_helper(params, recapture = false)
    params = params.permit(:start_date, :end_date, :count)

    query = events_by_angler_sql(params, recapture, nil)

    ActiveRecord::Base.connection.execute(query).to_a
  end

  def get_events_detail_by_angler_list_helper(params, recapture = false)
    # ensure the order, limit, and offset params are in the params hash
    params = {
        order: {
            "0" => {
                column: 0,
                dir: 'asc'
            }
        },
        length: nil,
        start: 0
    }.deep_merge(params).deep_symbolize_keys

    # a whitelist hash for the sort orders, to prevent SQL injections
    sort_orders = {
        asc: 'asc',
        desc: 'desc'
    }

    # a list of sort columns that will correspond with the column index provided by the DataTable JS library
    columns = %w(angler_id angler_name tag_number epoch_time species length length_range time_of_day fish_condition captures.latitude captureslongitude)

    # determine which table we are going to pull data from
    table = recapture ? 'recaptures' : 'captures'

    # correlate the order column and direction from the params. We are only going to do single column sorting for now
    order_column = columns[params[:order].first.last[:column].to_i]
    order_direction = sort_orders[params[:order].first.last[:dir].to_sym]

    # get the SQL string that will get the ids for the items to return
    inner_query = events_by_angler_sql(ActionController::Parameters.new(params), recapture, params[:angler_id])

    query = <<-SQL
      WITH all_items as (
        SELECT
          *
        FROM
          #{table}
        WHERE
          -- get the records whose ids are in the sub query
          #{table}.id IN (
            SELECT
              unnest(ids) -- we do a unnest to convert the array of ids to table rows
            FROM (
              #{inner_query}
            ) t
          )
      ),
      --------------
      -- if we were to implement searching we would have another CTE here
      -- that would apply the search terms
      ----------------
      paged_items as (
        SELECT
          COALESCE(json_agg(t), json'[]')
        FROM (
          SELECT
            anglers.angler_id                                     AS "angler_id",
            concat(anglers.first_name, ' ', anglers.last_name)    AS "angler_name",
            to_char(A.capture_date,'MM/DD/YYYY')                  AS "capture_date",
            extract(epoch from A.capture_date)                    AS "epoch_time",
            tags.tag_no                                           AS "tag_number",
            species.common_name                                   AS "species",
            A.length                                    		      AS "length",
            species_lengths.description                           AS "length_range",
            format_latitude(A.geom, gps_formats.format_string)      AS "latitude",
            format_longitude(A.geom, gps_formats.format_string)     AS "longitude",
            A.comments                      						          AS "comments",
            location_description_cleaner(a.location_description)	AS "location",
            A.fish_condition_option_id                            AS "fish_condition",
            -- the regex string will be SQL bind, there is an issue with having the `question mark` in the regex when using bind variables
            regexp_replace(time_of_day_options.time_of_day_option, ?, '') AS "time_of_day"
          FROM  all_items A
            LEFT JOIN tags on A.tag_id = tags.id
            LEFT JOIN species on A.species_id = species.id
            LEFT JOIN anglers on A.angler_id = anglers.id
            LEFT JOIN time_of_day_options ON A.time_of_day_option_id = time_of_day_options.id
            LEFT JOIN species_lengths ON A.species_length_id = species_lengths.id
            LEFT OUTER JOIN gps_formats ON A.entered_gps_type = gps_formats.format_type
          ORDER BY
            #{order_column} #{order_direction}
          LIMIT ?
          OFFSET ?
        ) t
      )
      -- combine the results into one json object with the totalRecords count and the data
      SELECT
        to_json(t) as "data"
      FROM (
        SELECT
          (SELECT count(*) FROM all_items) as "totalRecords",
          (SELECT * FROM paged_items) as "data"
      ) t
    SQL

    # build the array to sanitize and execute
    sanitized_sql = [query, '\\s\\(.+?\\)$' , params[:length], params[:start]]
    json = ActiveRecord::Base.connection.execute(ActiveRecord::Base.send(:sanitize_sql_array, sanitized_sql))[0]['data']

    # the sql will return a json string that needs to be parsed
    json = JSON.parse(json)

    {
        draw: params[:draw],
        data: json['data'],
        recordsTotal: json['totalRecords'],
        recordsFiltered: json['totalRecords']
    }
  end

  def download_events_by_angler_list_helper(params, recapture = false)
    clean_params = params.permit(:start_date, :end_date, :count)
    clean_params = data_helper_param_cleaner(clean_params)

    # create the file name
    min = clean_params[:start_date].blank? ? 'First' : clean_params[:start_date]
    max = clean_params[:end_date].blank?  ? 'Last' : clean_params[:end_date]
    filename = "#{recapture ? 'Recaptures' : 'Captures'}_By_Angler_from_#{min}_to_#{max}.xlsx"

    # create the token
    file_token = generate_download_token

    # create the download item with the token, the filename, and the if of the user that created it
    RequestedDownload.create!(token: file_token, filename: filename, user_id: current_user.id)

    job = recapture ? DownloadReportJob::REPORT_TYPES[:recaptures_by_angler] : DownloadReportJob::REPORT_TYPES[:captures_by_angler]
    DownloadReportJob.perform_later(job, params, file_token)

    file_token
  end

  def events_by_angler_list_to_xlsx_helper(params, recapture = false)
    if recapture
      items = get_recaptures_by_angler_list(params)
    else
      items = get_captures_by_angler_list(params)
    end

    details = []

    # build the workbook
    Axlsx::Package.new do |p|

      # and the summary count list worksheet
      p.workbook.add_worksheet(name: "#{recapture ? 'Recaptures' : 'Captures'} By Angler Summary") do |sheet|
        sheet.add_row ['Angler ID', 'First Name', 'Last Name', 'Capture Count']

        items.each do |datum|
          sheet.add_row [datum['angler_id'], datum['first_name'], datum['last_name'], datum['capture_count']]
        end
      end

      # for each item add a new worksheet, we are going to collect the detailed rows into one sheet.
      p.workbook.add_worksheet(name: 'All Data') do |sheet|
        sheet.add_row ['Angler ID', 'Angler Name', 'Capture Date', 'Tag Number', 'Species', 'Length', 'Length Range', 'Location', 'Comments', 'Time Of Day', 'Fish Condition', 'Latitude', 'Longitude']

        # for each item we need to ge the detail list
        items.each do |item|

          # adjusts the params to include the angler_id
          detail_params = params
          detail_params[:angler_id] = item['angler_id']

          # get the detail list for the current angler_id
          detail_info = get_events_detail_by_angler_list_helper(params, recapture)[:data]

          detail_info.each do |info|
            sheet.add_row [info['angler_id'], info['angler_name'], info['capture_date'], info['tag_number'], info['species'], info['length'], info['length_range'], info['location'], info['commets'], info['time_of_day'], info['fish_condition'], info['latitude'], info['longitude']]
          end

          details << { angler_id: item['angler_id'], data: detail_info}

        end # end items each

      end # end worksheet

      # each angler will get their own sheet as well.
      details.each do |item|

        p.workbook.add_worksheet(name: item[:angler_id]) do |sheet|
          item[:data].each do |info|
            sheet.add_row [info['angler_id'], info['angler_name'], info['capture_date'], info['tag_number'], info['species'], info['length'], info['length_range'], info['location'], info['commets'], info['time_of_day'], info['fish_condition'], info['latitude'], info['longitude']]
          end
        end

      end # end details each

    end # end workbook
  end

  def events_by_angler_sql(params, recapture = false, angler_id = nil)
    params = params.permit(:start_date, :end_date, :count)
    params = data_helper_param_cleaner(params)
    angler_id = Angler.from_ambiguous_id(angler_id).try(:id)

    # prep the params
    end_date = params[:end_date]
    start_date = params[:start_date]
    min_captures = params[:count].blank? ? 1 : params[:count]

    # setup the table and alias values
    table = recapture ? 'recaptures A INNER JOIN captures B ON A.tag_id = B.tag_id' : 'captures B'
    table_alias = recapture ? 'A' : 'B'

    # create the SQL string
    query = <<-SQL
      SELECT
        anglers.angler_id,
        anglers.first_name,
        anglers.last_name,
        count(*) AS "capture_count",
        array_agg(#{table_alias}.id) AS ids
      FROM
        #{table}
          INNER JOIN anglers ON B.angler_id = anglers.id
      WHERE
        #{table_alias}.capture_date <@ tstzrange(?,?,'[]')
        AND
         B.angler_id = coalesce(?, B.angler_id)
      GROUP BY
        anglers.angler_id,
        anglers.first_name,
        anglers.last_name
      HAVING
        count(*) >= COAlESCE(?,1)
      ORDER BY
        capture_count DESC
    SQL

    # return the sql
    ActiveRecord::Base.send(:sanitize_sql_array, [query, start_date, end_date,  angler_id, min_captures])
  end

  ####################################################
  # data check helpers

  def data_check_fish_event_sql(recapture = false, spreadsheet = false)
    table = recapture ? PresentableRecapture.table_name : PresentableCapture.table_name
    activity_type = recapture ? Recapture.name : Capture.name
    angler_name_attribute = recapture ?  'recapture_angler_name' : 'capture_angler'
    angler_id_attribute = recapture ? 'recapture_angler_number' : 'angler_number'

    spreadsheet_select_sql = ''
    spreadsheet_join_sql = ''

    # sql insertions for spreadsheet building
    if spreadsheet
      spreadsheet_select_sql = <<-SQL
         -- for spreadsheet generation
        , -- the extra comma that we need
        A.length                                    		      	AS "length",
        A.length_range                                          AS "length_range",
        A.latitude_formatted                                    AS "latitude",
        A.longitude_formatted                                   AS "longitude",
        A.comments                      						            AS "comments",
        location_description_cleaner(A.location_description)	  AS "location",
        A.time_of_day                                           AS "time_of_day",
        A.fish_condition                                        AS "fish_condition"
      SQL
    end

    <<-SQL
      SELECT
        COALESCE(json_agg(t), json'[]')
      FROM (
        SELECT
          A.id as "id",
          -- user info
          users.id as "user_id",
          concat(users.first_name, ' ', users.last_name) as "user_name",
          -- angler info
          A.#{angler_id_attribute} as "angler_id",
          A.#{angler_name_attribute} as "angler_name",
          -- capture info
          A.date_string AS "capture_date",
          extract(epoch from A.capture_date) as "capture_epoch_time", -- used for sorting
          -- tag info
          A.tag_number as "tag_number",
          -- species info
          A.species_name as "species",
          -- activity info
          CASE
            WHEN versions.event = 'create' THEN
              'Created'
            WHEN versions.event = 'update' THEN
              'Updated'
           WHEN versions.event = 'delete' THEN
              'Deleted'
            ELSE
              'N/A'
          END AS "activity_type",
          to_char(versions.created_at,'MM/DD/YYYY') AS "activity_date",
          extract(epoch from versions.created_at) as "epoch_time" -- used for sorting
          #{spreadsheet_select_sql}
        FROM versions
          INNER JOIN #{table} A on versions.item_id = A.id
          INNER JOIN users on versions.whodunnit = users.id
          #{spreadsheet_join_sql}
        WHERE
            versions.item_type = '#{activity_type}'
          AND
            versions.created_at <@ tstzrange(?,?,'[]')
          AND
            versions.whodunnit = coalesce(?, versions.whodunnit)
      ) t
    SQL
  end

  def data_check_tag_sql
    # todo need to write a migration that fixed the version.object changes
    <<-SQL
      SELECT
	      COALESCE(json_agg(t), JSON '[]')
      FROM (
           WITH tag_set AS (
             SELECT
               versions.*,
               (versions.object_changes #>> '{angler_id,1}')::bigint AS "angler_id",
               dense_rank()
               OVER (
                 ORDER BY versions.created_at::DATE, versions.activity_type, versions.whodunnit, tags.seq_id,
                   (versions.object_changes #>> '{angler_id,1}')::bigint
               )                                                                                  AS seq_id
             FROM
               versions
               LEFT JOIN #{PresentableTag.table_name} tags ON tags.id = versions.item_id AND versions.item_type = 'Tag'
             WHERE
               versions.item_type = 'Tag'
               AND
               versions.created_at <@ TSTZRANGE(?, ?, '[]')
               AND
               versions.whodunnit = COALESCE(?, versions.whodunnit)
           )
           SELECT
             min(tags.tag_no)                               AS "start_tag",
             max(tags.tag_no)                               AS "end_tag",
             count(tags.tag_no)                             AS "tag_quantity",
             versions.activity_type                         AS "activity_type",
             versions.created_at :: DATE                    AS "activity_date",
             max(versions.created_at)                       AS "activity_datetime",
             CASE
             WHEN max(versions.event) = 'create'
               THEN
                 NULL
             WHEN versions.angler_id IS NULL
               THEN
                 concat(max(anglers.angler_id), '*')
             ELSE
               anglers.angler_id
             END                                            AS "angler_id",
             min(anglers.email)                             AS "angler_email",
             CASE
             WHEN max(versions.event) = 'create'
               THEN
                 NULL
             WHEN versions.angler_id IS NULL
               THEN
                 (SELECT
                    concat(anglers.first_name, ' ', anglers.last_name, '*')
                  FROM anglers
                  WHERE id = max(tags.angler_id)
                  LIMIT 1)
             ELSE
               concat(min(anglers.first_name), ' ', min(anglers.last_name))
             END                                            AS "angler_name",
             users.id                                       AS "user_id",
             concat(users.first_name, ' ', users.last_name) AS "user_name",
             extract(EPOCH FROM max(versions.created_at))   AS "epoch_time"
           FROM
             tag_set AS versions
             LEFT JOIN #{PresentableTag.table_name} tags ON tags.id = versions.item_id AND versions.item_type = 'Tag'
             LEFT JOIN users ON versions.whodunnit = users.id
             LEFT JOIN anglers ON versions.angler_id = anglers.id
           GROUP BY
             versions.seq_id, versions.activity_type, activity_date, users.id, versions.angler_id, anglers.angler_id
           ORDER BY
             epoch_time DESC
         ) t
    SQL
  end

  def data_check_angler_sql
    <<-SQL
      SELECT
        COALESCE(json_agg(t), json'[]')
      FROM (
        SELECT
          versions.id       AS "id",
          anglers.id        AS "angler_id",
          anglers.angler_id AS "angler_id_string",
          anglers.email     AS "angler_email",
          users.id          AS "user_id",
          concat(users.first_name, ' ', users.last_name) AS "user_name",
          concat(anglers.first_name, ' ', anglers.last_name) AS "angler_name",
          CASE
            WHEN versions.event = 'create' THEN
              'Created'
            WHEN versions.event = 'update' THEN
              'Updated'
            ELSE
              'N/A'
          END AS "activity_type",
          to_char(versions.created_at,'MM/DD/YYYY') AS "activity_date",
          extract(epoch from versions.created_at) as "epoch_time" -- used for sorting
        FROM
          versions
            INNER JOIN anglers on versions.item_id = anglers.id AND versions.item_type = 'Angler'
            INNER JOIN users on versions.whodunnit = users.id
        WHERE
            versions.item_type = 'Angler'
          AND
            versions.created_at <@ tstzrange(?,?,'[]')
          AND
            versions.whodunnit = coalesce(?, versions.whodunnit)
      ) t
    SQL
  end

  def data_helper_param_cleaner(params)
    params[:angler_id] = params[:angler_id].blank? ? nil : Angler.from_ambiguous_id(params[:angler_id]).try(:id)
    params[:start_date] = params[:start_date].try(:gsub, /__\/__\/____/, '').blank? ? nil: Chronic.parse(params[:start_date]).try(:beginning_of_day).try(:iso8601)
    params[:end_date] = params[:end_date].try(:gsub, /__\/__\/____/, '').blank? ? nil : Chronic.parse(params[:end_date]).try(:end_of_day).try(:iso8601)

    params
  end

end
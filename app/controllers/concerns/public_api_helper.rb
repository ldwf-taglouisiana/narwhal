module PublicApiHelper
  extend ActiveSupport::Concern

  def combined_public_data(params)
    updated_params = {
        species: {},
        species_lengths: {},
        news_feed: {},
        photo_stream: {},
        fish_condition_options: {},
        time_of_day_options: {},
        recapture_disposition_options: {},
        shirt_size_options: {}
    }

    updated_params.deep_merge(params.try(:deep_symbolize_keys)) unless params.nil?

    {
        species: species(updated_params[:species]),
        species_lengths: species_lengths(updated_params[:species_lengths]),
        news_feed: news_feed(updated_params[:news_feed]),
        photo_stream: photo_stream(updated_params[:photo_stream]),
        time_of_day_options: time_of_day_options(updated_params[:time_of_day_options]),
        fish_condition_options: fish_condition_options(updated_params[:fish_condition_options]),
        recapture_disposition_options: recapture_disposition_options(updated_params[:recapture_disposition_options]),
        shirt_size_options: shirt_size_options(updated_params[:shirt_size_options])
    }
  end

  def species_lengths(params)
    data = Hash.new

    includes = ['id', 'description', 'species_id', 'position', 'to_char(updated_at, \'yyyy-MM-ddThh24:MI:SS\' ) AS version']

    if params[:version]
      results = SpeciesLength.where("date_trunc('second', updated_at) > ?", params[:version]).order('updated_at ASC').select(includes)
    else
      results = SpeciesLength.order('updated_at ASC').select(includes)
    end

    data[:errors] =[]
    data[:count] = results.to_a.count
    data[:items] = results

    if  params[:get_id_list] == true or params[:get_id_list].to_i == 1
      data[:ids] = SpeciesLength.all.collect {|t| t.id}
    end

    data
  end

  def species(params)
    default_includes = %w(id common_name position proper_name other_name family habitat description size food_value target publish)
    image_styles = %w(medium feature index_list thumb )

    image_size = image_styles.include?(params[:image_size]) ? params[:image_size] : 'medium'
    limit = params[:limit]
    offset = params[:offset]
    includes = params[:includes] ? params[:includes].split(/,/) & default_includes : default_includes
    target = params[:target]

    includes = includes.empty? ? default_includes : includes
    includes = includes + ['to_char(updated_at, \'yyyy-MM-ddThh24:MI:SS\' ) AS version']

    # GET THE PHOTO URL, THE PATH IS RIPPED FROM THE DEFINITION IN THE SPECIES MODEL
    (photo_url = '') << <<-SQL
        CASE
            WHEN photo_file_name IS NULL
                THEN ''
                ELSE concat('#{request.protocol}#{request.host_with_port}','/species/image/', id, '/', photo_file_name, '?style=#{image_size}' )
        END AS image_url
    SQL

    includes = includes + [photo_url]
    includes = includes.join(',')

    id = params[:id] ? params[:id] : nil
    term = params[:term]

    species = {}

    if id
      current_version = Species.find(id).updated_at.to_i
      species = Species.where('id = ?', [id]).select(includes.empty? ? default_includes : includes)

    elsif term
      where_statement = []
      where_statement << '(common_name ilike ? OR proper_name ilike ? OR other_name ilike ?)'
      where_statement << " date_trunc('second', updated_at) > #{ params[:version].present? ? '?' : '(select min(updated_at) - interval \'1 min\' from species)'}"

      if params[:version].present?
        species = Species.where(where_statement.join(' AND '), "%#{term}%", "%#{term}%", "%#{term}%", params[:version])
                      .select(includes.empty? ? default_includes : includes)
                      .order('common_name')
                      .offset(offset ? offset : 0)
                      .limit(limit ? limit : nil)

      else
        species = Species.where(where_statement.join(' AND '), "%#{term}%", "%#{term}%", "%#{term}%")
                      .select(includes.empty? ? default_includes : includes)
                      .order('common_name')
                      .offset(offset ? offset : 0)
                      .limit(limit ? limit : nil)

      end


    elsif target
      species = Species.where('target = true')
                    .order('position asc')
                    .select(includes.empty? ? default_includes : includes)
    else
      current_version = Species.order('updated_at desc').first.updated_at.to_i
      species = Species.where("common_name != 'Unknown' and date_trunc('second', updated_at) > #{ params[:version].present? ? '?' : '(select min(updated_at) - interval \'1 min\' from species)'}", [params[:version]]).select(includes.empty? ? default_includes : includes)
                    .order('common_name')
                    .select(includes.empty? ? default_includes : includes)
                    .offset(offset ? offset : 0)
                    .limit(limit ? limit : nil)
    end

    data = Hash.new

    data[:errors] = []
    data[:count] = species.to_a.count
    data[:version] = current_version
    data[:items] = species.to_a.count > 0 ? species : []

    if  params[:get_id_list] == true or params[:get_id_list].to_i == 1
      data[:ids] = Species.all.collect {|t| t.id}
    end

    data
  end

  def marinas(params)
    default_includes = %w(name latitude longitude hours_of_operation parish id address web_address phone publish closed)

    zip_code = params[:zip_code] ? params[:zip_code] : nil
    dis = params[:dis] ? params[:dis] : 20
    limit = params[:limit] ? limit : nil
    offset = params[:offset] ? offset : 0
    parish = params[:parish] ? params[:parish] : nil
    includes = params[:includes] ? params[:includes].split(/,/) & default_includes : default_includes
    includes = includes.empty? ? default_includes : includes
    includes = includes + ['to_char(updated_at, \'yyyy-MM-ddThh24:MI:SS\' ) AS version']
    includes = includes.join(',');

    current_version = Marina.order('updated_at desc')
                          .offset(offset)
                          .limit(limit)
                          .first.updated_at

    marinas = {}

    wheres = []
    binds = []

    if params[:version].nil? or params[:version].empty?
      wheres << 'updated_at > (select min(updated_at) - interval \'1 minute\' from marinas)'
    else
      wheres << "date_trunc('second', updated_at) > ?"
      binds << Chronic.parse(params[:version]).to_s
    end


    if zip_code
      marinas = Marina.near(zip_code.to_region, dis)
                    .where(wheres.join(' AND '), binds)
                    .select(includes)
                    .offset(offset)
                    .limit(limit)
    else
      marinas = Marina.where(wheres.join(' AND '), binds)
                    .order("#{ parish ? 'parish,' : '' } name ")
                    .select(includes)
                    .offset(offset)
                    .limit(limit)
    end

    #marinas.each do |m|
    #  begin
    #    m[:photo_url] = "#{request.protocol}#{request.host_with_port}#{m.photo_url}"
    #  rescue
    #    raise m.photo.inspect
    #  end
    #end

    data = Hash.new
    data['count'] = marinas.to_a.count

    result = Hash.new

    if parish
      marinas.each do |m|
        unless result[m.parish.to_s]
          result[m.parish.to_s] = []
        end

        result[m.parish.to_s] << m

        result[m.parish.to_s].sort
      end

      result
    else
      result = marinas
    end


    data[:errors] = []

    data[:version] = current_version
    data[:items] = result

    data
  end

  def blurbs(params)
    default_includes = %w(about_program capture_dashboard common_fish contact_info draft_captures find_a_spot how_to licensing my_captures my_tags new_capture order_tags sign_up target_species volunteer_hours contact_us account_info faq privacy_policy)

    version = params[:version] ? params[:version].to_i : 0
    includes = params[:includes] ? params[:includes].split(/,/) & default_includes : default_includes

    current_version = Blurb.order('updated_at desc').first.updated_at.to_i

    blurbs = current_version != version ? Blurb.select(includes) : []

    data = Hash.new
    data['count'] = blurbs.to_a.count
    data['errors'] = ''
    data['version'] = current_version
    data['items'] = blurbs

    data
  end

  def time_of_day_options(params)
    data = Hash.new

    includes = ['id', 'time_of_day_option', 'to_char(updated_at, \'yyyy-MM-ddThh24:MI:SS\' ) AS version']

    if params[:version]
      results = TimeOfDayOption.where('date_trunc(\'second\', updated_at) > ?', params[:version]).order('updated_at desc').select(includes)
    else
      results = TimeOfDayOption.order('updated_at desc').select(includes)
    end

    data[:errors] =[]
    data[:count] = results.to_a.count
    data[:items] = results

    if  params[:get_id_list] == true or params[:get_id_list].to_i == 1
      data[:ids] = TimeOfDayOption.all.collect {|t| t.id}
    end

    data
  end

  def fish_condition_options(params)
    data = Hash.new

    includes = ['id', 'fish_condition_option', 'to_char(updated_at, \'yyyy-MM-ddThh24:MI:SS\' ) AS version']

    if params[:version]
      results = FishConditionOption.where('date_trunc(\'second\', updated_at) > ?', params[:version]).order('updated_at desc').select(includes)
    else
      results = FishConditionOption.order('updated_at desc').select(includes)
    end

    data[:errors] =[]
    data[:count] = results.to_a.count
    data[:items] = results

    if  params[:get_id_list] == true or params[:get_id_list].to_i == 1
      data[:ids] = FishConditionOption.all.collect {|t| t.id}
    end

    data
  end

  def recapture_disposition_options(params)

    data = Hash.new

    includes = ['id', 'recapture_disposition', 'to_char(updated_at, \'yyyy-MM-ddThh24:MI:SS\' ) AS version']

    if params[:version]
      results = RecaptureDisposition.where('date_trunc(\'second\', updated_at) > ?', params[:version]).order('updated_at desc').select(includes)
    else
      results = RecaptureDisposition.order('updated_at desc').select(includes)
    end

    data[:errors] =[]
    data[:count] = results.to_a.count
    data[:items] = results

    if  params[:get_id_list] == true or params[:get_id_list].to_i == 1
      data[:ids] = RecaptureDisposition.all.collect {|t| t.id}
    end

    data
  end

  def shirt_size_options(params)

    data = Hash.new

    includes = ['id', 'shirt_size_option', 'to_char(updated_at, \'yyyy-MM-ddThh24:MI:SS\' ) AS version']

    if params[:version]
      results = ShirtSizeOption.where('date_trunc(\'second\', updated_at) > ?', params[:version]).order('updated_at desc').select(includes)
    else
      results = ShirtSizeOption.order('updated_at desc').select(includes)
    end

    data[:errors] =[]
    data[:count] = results.to_a.count
    data[:items] = results

    if  params[:get_id_list] == true or params[:get_id_list].to_i == 1
      data[:ids] = ShirtSizeOption.all.collect {|t| t.id}
    end

    data
  end

  def news_feed(params)
    limit = params[:limit].present? ? params[:limit] : 25
    offset = params[:offset].present? ? params[:offset] : 0

    (query = '') << <<-SQL
        with items as (
          select  id,
                  title,
                  content,
                  to_char(created_at, 'yyyy-MM-ddThh24:MI:SS' ) as version
          from news_items
          where should_publish is true
            #{ 'AND date_trunc(\'second\', updated_at) > ?' if params[:version]}
          order by created_at DESC
          limit ?
          offset ?
        )
        SELECT row_to_json(t) as "json"
        FROM (
          SELECT  array_to_json(array_agg(row_to_json(m))) as "items",
                  (SELECT count(*) FROM items) as "count",
                  ? as "limit",
                  ? as "offset"
          FROM items m
        )t;
    SQL

    sanitized_sql = params[:version] ? ([query] + [params[:version]]) : [query]
    sanitized_sql = sanitized_sql + [limit, offset, limit, offset]

    sql = ActiveRecord::Base.send(:sanitize_sql_array, sanitized_sql)
    json = ActiveRecord::Base.connection().execute(sql)[0]['json']

    result = JSON.parse(json, symbolize_name: true)
  end

  def photo_stream(params)

    if params[:version]
      results = FrontPagePhotoStreamItem.where('date_trunc(\'second\', updated_at) > ?', params[:version]).order('created_at desc')
    else
      results = FrontPagePhotoStreamItem.order('created_at desc')
    end

    data = Hash.new
    data[:items] = results.as_json(:only => [:id, :featured], :methods => [:version, :api_image_url, :image_name])
    data[:count] = results.to_a.count

    data
  end

end
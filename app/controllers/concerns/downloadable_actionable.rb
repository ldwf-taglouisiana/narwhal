module DownloadableActionable
  extend ActiveSupport::Concern

  def generate_download_token
    # create the token
    token = SecureRandom.hex(32)

    # if the token was already taken, then regenerate
    while RequestedDownload.where(token: token).count > 0
      token = SecureRandom.hex(32)
    end

    token
  end
end
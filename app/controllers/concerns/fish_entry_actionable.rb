module FishEntryActionable
  extend ActiveSupport::Concern
  include FuzzyMapModule

  def verify
    authorize! :update, self.resource_class
    entries = controller_name.singularize.classify.constantize.where(id: params[:id] || params[:options])
    entries.each { |item| item.update_attributes({ verified: true}) }

    respond_to do |format|
      format.html {
        if entries.count == 1
          redirect_to entries.first
        else
          redirect_to_url captures_path
        end
      }
      format.js {
        'refreshTables();``'
      }
    end

  end

  def convert_to_other
    authorize! :update, Capture
    authorize! :update, Recapture
    authorize! :update, DraftCapture
    klass = controller_name.classify.constantize
    result = klass.find(params[:id]).convert_to_other

    if result.errors.any?
      flash[:error] = result.errors.full_messages.map {|t| "• #{t}"}.join('<br>').html_safe
      redirect_to (klass == Recapture ? recapture_path(params[:id]) : capture_path(params[:id]))
    else
      flash[:success] = 'Entry successfully converted'
      redirect_to (klass == Recapture ? capture_path(result) : recapture_path(result))
    end
  end

  def geo_json
    authorize! :index, self.resource_class
    @items = controller_name.classify.constantize
                 .where(id: params[:id])
                 .where.not(geom: nil)

    # if there are any items then render the geo json response
    if @items.any?
      render template: 'fish_entry_common/geo_json'

    else # if there were no items, then return a null data element
      render json: { data: nil }

    end

  end

  def report
    # model_key = (controller_name.singularize + '_id').to_sym

    @events = []

    @requested_event = controller_name.singularize.classify.constantize.find(params[:id])

    # this is the original capture
    @events << Capture.where(:tag_id => @requested_event.tag_id).first

    # these are the recaptures
    @events = @events + Recapture.where(tag_id: @requested_event.tag_id).order('capture_date')

    fuzzy_data_hash = get_fuzzy_map(@events.compact, {height: 350, width: 700, combined: true, requested_event: @requested_event})
    @geo_json = fuzzy_data_hash.first[:json]
    @map = fuzzy_data_hash.first[:map_url]

    if @events.last.instance_of? Recapture
      @release = @events.last.recapture_disposition.nil?  ? 'N/A' : @events.last.recapture_disposition.recapture_disposition
    else
      @release = ''
    end

    respond_to do |f|
      f.html {
        render template: 'fish_entry_common/report', layout: 'capture_report.html'
      }

      f.pdf {
        render :pdf => 'capture-report',
               :layout => 'capture_report_pdf',
               :template => 'fish_entry_common/report_pdf.html',
               :print_media_type => true,
               :page_size => 'Letter',
               :javascript_delay => 8000,
               :show_as_html => params[:debug].present?, # renders html version if you set debug=true in URL,
               :extra => '--debug-javascript'
      }
    end
  end

  # Only allow a trusted parameter "white list" through.
  def fish_entry_params
    model_key = controller_name.singularize.to_sym

    clean_params = params.require(model_key).permit(
        :angler_id, :tag_id, :capture_date, :species_id, :length, :location_description,
        :latitude, :longitude, :basin_id, :sub_basin_id, :fish_condition_option_id, :comments, :verified,
        :recapture_disposition_id, :time_of_day_option_id, :entered_gps_type, :species_length_id
    )

    clean_params[:user_capture] = false

    clean_params
  end

  def params_from_form_redirect
    params.permit(:fish_entry => [
                      :angler_id, :tag_id, :capture_date, :species_id, :length, :location_description, :latitude,
                      :longitude, :basin_id, :sub_basin_id, :fish_condition_option_id, :comments, :verified,
                      :recapture_disposition_id, :time_of_day_option_id, :entered_gps_type, :species_length_id
                  ])[:fish_entry]
  end
end
module DatatableActionable
  extend ActiveSupport::Concern

  def data_table_params
    # whitelist the params
    clean_params = params.permit(:start, :length, :draw, :only_errors, :scope, :order => [:column, :dir], :search => [:value])

    # merge a default set of params to ensure they are present
    clean_params = {
        order: {
            "0" => {
                column: 0,
                dir: 'asc'
            }
        },
        length: nil,
        start: 0
    }.deep_merge(clean_params).deep_symbolize_keys

    # pull out the params, just for ease of use
    limit = clean_params[:length].try(:to_i)
    offset = clean_params[:start].to_i
    scope = clean_params[:scope]
    column = clean_params[:order].first.last[:column].to_i
    direction = clean_params[:order].first.last[:dir]
    only_errors = clean_params[:only_errors].present? ? true : false
    {
        for_user: defined?(current_user) ? current_user : nil,
        limit: limit,
        offset: offset,
        order_column_index: column,
        order_direction: direction,
        draw: clean_params[:draw],
        search_term: clean_params[:search].try(:[], :value).presence,
        only_errors: only_errors,
        scope: scope
    }
  end
end
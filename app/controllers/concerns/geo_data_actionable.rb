module GeoDataActionable
  extend ActiveSupport::Concern

  def helper_event_basin_data(params)
    klass = params[:recapture] ? Recapture : Capture
    table_name = Basin.table_name
    @items = Basin.joins(klass.name.downcase.pluralize.to_sym)
                 .merge(klass.in_time_range(params[:start_date], params[:end_date]))
                 .group(:gid)
                 .select([
                             "\"#{table_name}\".*",
                             "ST_AsGeoJSON(\"#{table_name}\".geom)::json as geo_json",
                             'count(gid) as event_count'
                         ])
  end

  def helper_event_heatmap(params)
    klass = params[:recapture] ? Recapture : Capture

    @items = klass.in_time_range(params[:start_date], params[:end_date])
                 .where.not(geom: nil)
                 .group(:latitude, :longitude)
                 .select([
                             :latitude,
                             :longitude,
                             'count(*) as event_count'
                         ])
  end

  def geo_json_params(controller_params)
    controller_params[:number_of_days] = 30 if controller_params[:number_of_days].blank?
    controller_params[:start_date] = controller_params[:start_date].blank? ? (Time.now - controller_params[:number_of_days].to_i.days).beginning_of_day : Chronic.parse(controller_params[:start_date])
    controller_params[:end_date]   = controller_params[:end_date].blank? ? Time.now : Chronic.parse(controller_params[:end_date])
    controller_params[:recapture]  = controller_params[:recapture] == 'true'

    controller_params
  end
end
module ProtectedApiHelper
  extend ActiveSupport::Concern

  def tags(params)
    # The allowed attributes that can be retrieved for this query
    default_includes = %w(id tag_number used)

    ################################################################################################
    # Parameters for API call
    #
    # limit     - Used to limit the amount of entries returned. By default you will get all records.
    # offset    - The position of the first entry returned from 0.
    # version   - The version string of the data that you currently have. If given, will only return
    #             records if given version is older than current updated at version.
    # includes  - Can be used to delimit the number of attributes for the returend entries.
    ################################################################################################

    limit = params[:limit]
    version = params[:version]
    offset = params[:offset] ? params[:offset] : 0
    includes = params[:includes] ? params[:includes].split(/,/) & default_includes : default_includes
    includes = includes.empty? ? default_includes : includes
    includes = includes + ['version']
    includes = includes.join(',')

    (tags_query = '') << <<-SQL
          with
          user_tags as (
            select distinct on (tag_no)
              tags.id as id,
              tags.tag_no as tag_number,
              to_char(tags.updated_at, 'yyyy-MM-ddThh24:MI:SS' ) as version,
              (captures.id IS NOT NULL) as used
            from
              tags left join captures on tags.id = captures.tag_id
            where
              tags.angler_id = ?
              and
              tags.updated_at <@ tstzrange(?,NULL,'(]')
            order by
              tag_no ASC
            limit ?
            offset ?
          )
          select COALESCE(json_agg(t), json'[]') as "data"
          from (
            select
              #{includes}
            from
              user_tags
          ) t
    SQL

    sanitized_sql = [tags_query, @user.angler_id, params[:version], limit, offset]
    sql = ActiveRecord::Base.send(:sanitize_sql_array, sanitized_sql)
    tags = ActiveRecord::Base.connection().execute(sql)

    tags = tags[0]['data']
    tags = tags.nil? ? JSON.parse('[]') : JSON.parse(tags)

    data = Hash.new

    data[:errors] = []
    data[:count] = tags.count
    data[:version] = nil
    data[:items] = tags

    if  params[:get_id_list] == true or params[:get_id_list].to_i == 1
      data[:ids] = Tag.where(:angler_id => @user.angler_id).collect {|t| t.id}
    end

    data
  end

  def captures(params)
# The allowed attributes that can be retrieved for this query
    default_includes = %w(id capture_date tag_number species_id species_name species_length_id latitude latitude_string longitude longitude_string has_recapture comments location_description length fish_condition_option_id fish_condition entered_gps_type time_of_day_option_id verified photo_url range)

    ####################################################################################################
    # Parameters for API call
    #
    # limit     - Used to limit the amount of entries returned. By default you will get all records.
    # offset    - The position of the first entry returned from 0.
    # zipcode   - The starting zip code to search from. REQUIRES distance be set
    # distance  - The distance in miles to search out from the given zipcode. REQUIRES zipcode to be set
    # version   - The version string of the data that you currently have. If given, will only return
    #             records if given version is older than current updated at version.
    # includes  - Can be used to delimit the number of attributes for the returend entries.
    ####################################################################################################

    dynamic_sql_binds = []

    limit = params[:limit]
    offset = params[:offset] ? params[:offset] : 0

    includes = params[:includes] ? params[:includes].split(/,/) & default_includes : default_includes
    includes = includes.empty? ? default_includes : includes
    includes = includes + ['version']
    includes = includes.join(',')

    species = params[:species] ? params[:species] : ''

    if params[:start_date].present?
      start_date = '?'
      dynamic_sql_binds << Chronic.parse(params[:start_date]).utc.to_s
    else
      start_date = '(select min(capture_date) from captures where angler_id = ?)'
      dynamic_sql_binds << @user.angler_id
    end

    if params[:end_date].present?
      end_date = '?'
      dynamic_sql_binds << Chronic.parse(params[:end_date]).utc.to_s
    else
      end_date = 'now()'
    end

    if params[:version].present?
      version = '?'
      dynamic_sql_binds << Chronic.parse(params[:version]).utc.to_s
    else
      version = '(select min(updated_at) - interval \'1 min\' from captures where angler_id = ? )'
      dynamic_sql_binds << @user.angler_id
    end

    # if the user wants the captures by distance from a zipcode
    distance_where = nil
    if params[:zipcode].present? and params[:distance].present?
      distance_where = 'AND ST_DWithin((select geom from zipcodes where id = ?), captures.geom, (? * 1609.34) )'
      dynamic_sql_binds << params[:zipcode]
      dynamic_sql_binds << params[:distance]
    end

    # get the max version of the requested set of data
    (current_version_query = '') << <<-SQL
        select max (updated_at) from (
          select to_char(captures.updated_at, 'yyyy-MM-ddThh24:MI:SS' ) as updated_at,
                 captures.capture_date
          from captures left join species on captures.species_id = species.id
                left join tags on captures.tag_id = tags.id
                left join fish_condition_options on captures.fish_condition_option_id = fish_condition_options.id
          where captures.angler_id = ?
                and captures.capture_date between #{start_date} and #{end_date}
                and date_trunc('second', captures.updated_at) > #{version}
                #{distance_where}
                and species.common_name ~* ?
          order by captures.capture_date desc
          limit ?
          offset ?
        ) t
    SQL

    sanitized_sql = [current_version_query, @user.angler_id] + dynamic_sql_binds + [species, limit, offset]
    sql = ActiveRecord::Base.send(:sanitize_sql_array, sanitized_sql)
    current_version = ActiveRecord::Base.connection().execute(sql)[0]['max']

    (captures_query = '') << <<-SQL
          with
          caps as (
            select  captures.id,
                    captures.tag_id,
                    to_char(captures.capture_date, 'yyyy-MM-ddThh24:MI:SS' ) as capture_date,
                    captures.species_id,
                    round( CAST(captures.latitude as numeric), 4) as latitude,
                    round( CAST(captures.longitude as numeric), 4) as longitude,
                    format_latitude(geom, gps_formats.format_string)            AS "latitude_string",
                    format_longitude(geom, gps_formats.format_string)           AS "longitude_string",
                    (captures.tag_id in (select tag_id from recaptures)) as has_recapture,
                    captures.comments,
                    captures.length,
                    captures.species_length_id,
                    captures.fish_condition_option_id,
                    captures.entered_gps_type,
                    captures.time_of_day_option_id,
                    captures.location_description,
                    species.common_name as species_name,
                    captures.verified as verified,
                    species_lengths.description as range,
                    (CASE
                      WHEN photo_file_name IS NULL
                        THEN ''
                        ELSE concat('#{request.protocol}#{request.host_with_port}','/species/image/', species.id, '/', photo_file_name)
                    END) AS photo_url,
                    tags.tag_no as tag_number,
                    fish_condition_options.fish_condition_option as fish_condition,
                    to_char(captures.updated_at, 'yyyy-MM-ddThh24:MI:SS' ) as version
            from captures
                  left join species on captures.species_id = species.id
                  left join tags on captures.tag_id = tags.id
                  left join fish_condition_options on captures.fish_condition_option_id = fish_condition_options.id
                  left join species_lengths on captures.species_length_id = species_lengths.id
                  LEFT OUTER JOIN gps_formats ON captures.entered_gps_type = gps_formats.format_type
            where captures.angler_id = ?
                  and captures.capture_date between #{start_date} and #{end_date}
                  and date_trunc('second', captures.updated_at) > #{version}
                  -- and captures.verified = true
      #{distance_where}
                  and species.common_name ~* ?
            order by tag_number desc
            limit ?
            offset ?
          )
          select array_to_json(array_agg(row_to_json(t))) as "data"
          from (
            select #{includes}
            from caps
          ) t
    SQL

    sanitized_sql = [captures_query, @user.angler_id] + dynamic_sql_binds + [species, limit, offset]
    sql = ActiveRecord::Base.send(:sanitize_sql_array, sanitized_sql)
    captures = ActiveRecord::Base.connection().execute(sql)

    captures = captures[0]['data']
    captures = captures.nil? ? JSON.parse('[]') : JSON.parse(captures)

    data = Hash.new
    data[:errors] = []
    data[:count] = captures.to_a.count
    data[:items] = captures
    data[:version] = current_version

    if  params[:get_id_list] == true or params[:get_id_list].to_i == 1
      data[:ids] = Capture.where(:angler_id => @user.angler_id).collect {|t| t.id}
    end

    data
  end

  def draft_captures(params)
    dynamic_sql_binds = []

    includes = DraftCapture.attribute_names - %w(created_at updated_at capture_date saved_at, geom)
    includes = includes + ['latitude_string', 'longitude_string', 'common_name as species_name', 'to_char(capture_date, \'yyyy-MM-ddThh24:MI:SS\' ) AS capture_date', 'to_char(updated_at, \'yyyy-MM-ddThh24:MI:SS\' ) AS version', 'to_char(saved_at, \'yyyy-MM-ddThh24:MI:SS\' ) AS saved_at', 'uuid']

    #drafts = user.angler.draft_captures.order('created_at').select(includes)

    species = params[:species] ? params[:species] : ''

    if params[:start_date].present?
      start_date = '?'
      dynamic_sql_binds << Chronic.parse(params[:start_date]).utc.to_s
    else
      start_date = '(select min(capture_date) from draft_captures where angler_id = ?)'
      dynamic_sql_binds << @user.angler_id
    end

    if params[:end_date].present?
      end_date = '?'
      dynamic_sql_binds << Chronic.parse(params[:end_date]).utc.to_s
    else
      end_date = 'now()'
    end

    (draft_captures_query = '') << <<-SQL
          with
          caps as (
            select
              draft_captures.*,
              format_latitude(geom, gps_formats.format_string) as "latitude_string",
              format_longitude(geom, gps_formats.format_string) as "longitude_string",
              common_name
            from draft_captures
              left join species on draft_captures.species_id = species.id
              LEFT OUTER JOIN gps_formats ON draft_captures.entered_gps_type = gps_formats.format_type
            where draft_captures.angler_id = ?
              and draft_captures.capture_date between #{start_date} and #{end_date}
              and species.common_name ~* ?
          )
          select array_to_json(array_agg(row_to_json(t))) as "data"
          from (
            select #{includes.join(',')}
            from caps
          ) t
    SQL

    sanitized_sql = [draft_captures_query, @user.angler_id] + dynamic_sql_binds + [species]
    sql = ActiveRecord::Base.send(:sanitize_sql_array, sanitized_sql)
    drafts = ActiveRecord::Base.connection.execute(sql)

    drafts = drafts[0]['data']
    drafts = drafts.nil? ? JSON.parse('[]') : JSON.parse(drafts)

    drafts.each do |draft|
      draft['status'] = DraftCapture.where(:id => draft['id']).first.status
    end

    data = Hash.new
    data[:errors] = []
    data[:count] = drafts.to_a.count
    data[:items] = drafts
    data[:version] = Time.now.to_s
    data[:ids] = DraftCapture.where(:angler_id => @user.angler_id).collect {|t| t.id}

    data
  end

  def post_draft_captures_helper(params)
    data = Hash.new
    data[:errors] = []
    data[:messages] = []
    data[:count] = 0
    data[:items] = []
    data[:status] = :ok

    clean_params = params.permit(:items => [:id, :uuid, :should_delete, :capture_date, :comments, :entered_gps_type, :fish_condition_id, :latitude, :longitude, :recapture, :recapture_disposition_id, :species_id, :length, :species_length_id, :tag_number, :time_of_day_id, :should_save, :location_description])

    begin
      ActiveRecord::Base.transaction do
        clean_params[:items].try(:each) do |item|

          #Here the id is the rails id, if this param is nil or 0 then that means we're making a new capture
          if item[:id].nil? or item[:id] <= 0
            c = DraftCapture.new(item.except(:id, :should_delete))
            c.angler_id = @user.angler_id
            c.save!
          else
            #otherwise we're either updating or deleting
            c = DraftCapture.where(id: item[:id])
            c.each do |temp|
              if item[:should_delete] == 1 or item[:should_delete] == true
                temp.destroy
              else
                temp.update_attributes(item.except(:id, :uuid, :should_delete))
              end # end should_delete if
            end

          end # end do loop

          data[:items] << item[:uuid] unless item[:uuid].blank?
        end
      end

      data[:count] = data[:items].count

    rescue Exception => exp
      logger.fatal 'Error Occurred'
      logger.fatal exp

      data[:errors] << 'Could not save some or all of the captures'
      data[:items] = []
      data[:count] = 0

      data[:status] = :internal_server_error

    ensure
      return data
    end
  end

  def post_tag_requests_helper(params)
    data = Hash.new
    data[:errors] = []
    data[:status] = :internal_server_error
    data[:items] = []

    p =  params.permit(:items => [:tag_type, :number_of_tags, :uuid])
    items = p[:items].nil? ? [] : p[:items]

    request = Hash.new
    request[:angler_id] = @user.angler_id
    request[:tag_types] = Hash.new

    requests = []

    begin
      ActiveRecord::Base.transaction do
        items.each do |item|
          request[:tag_types][item['tag_type']] += item['number_of_tags'] if request[:tag_types].key?(item['tag_type'])
          request[:tag_types][item['tag_type']] = item['number_of_tags']

          m = UserTagRequest.new(item.except(:uuid))
          m.angler_id = @user.angler_id
          m.fulfilled = false
          m.save!

          requests << m

          data[:items] << item[:uuid] unless item[:uuid].blank?
        end
      end
    rescue Exception => exp
      logger.fatal 'Error Occurred'
      logger.fatal exp
      data[:errors] << 'Could not create request'
      data[:status] = :internal_server_error
    else
      requests.each do |r|
        Notification.build_user_tag_request(r)
      end

      data[:status] = :ok
    ensure
      return data
    end
  end

  def post_time_logs_helper(params)
    # Times must be in UTC
    #{
    #    "auth_token" : "fdsfsfgdsff",
    #    "items" : [
    #       {
    #         "start_date" : "2012-31-1T15:34:00",
    #         "end_date" : "2012-31-1T17:34:00",
    #         "verified" : true
    #       }, ...
    #    ]
    #}

    data = Hash.new
    data[:errors] = []
    data[:messages] = []
    data[:status] = :internal_server_error
    data[:items] = []

    params_clean = params.permit(:items => [:start_date, :end_date, :verified, :uuid])
    items = params_clean[:items].nil? ? [] : params_clean[:items]

    begin
      ActiveRecord::Base.transaction do
        items.each do |item|
          data[:items] << item[:uuid] unless item[:uuid].blank?

          v = VolunteerTimeLog.new(:start => item[:start_date], :end => item[:end_date], :verified => item[:verified], :angler_id => @user.angler_id)
          v.save!
        end

      end

    rescue ActiveRecord::StatementInvalid => exp
      logger.fatal 'Error Occurred'
      logger.fatal exp
      data[:errors] << 'Could not add hours because hours overlap with previous entered time on water. <a href="/my_captures/new" style="color:blue">Click here to enter captures.</a>'
      data[:status] = :unprocessable_entity

    rescue Exception => exp
      logger.fatal 'Error Occurred'
      logger.fatal exp
      data[:errors] << 'Could not add hours'
      data[:status] = :internal_server_error

    else
      data[:status] = :ok

    ensure
      return data

    end

  end
end
# == Schema Information
# Schema version: 20160405165101
#
# Table name: about_tagging_programs
#
#  id          :integer          not null, primary key
#  description :text             not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class AboutTaggingProgramsController < ApplicationController

  before_action :set_about_tagging_program, only: [:show, :edit, :update, :destroy]

  # GET /about_tagging_programs
  def index
    authorize! :read, AboutTaggingProgram
    @about_tagging_programs = AboutTaggingProgram.all
  end

  # GET /about_tagging_programs/1
  def show
    authorize! :read, @about_tagging_program
  end

  # GET /about_tagging_programs/new
  def new
    authorize! :create, AboutTaggingProgram
    @about_tagging_program = AboutTaggingProgram.new
  end

  # GET /about_tagging_programs/1/edit
  def edit
    authorize! :update, @about_tagging_program
  end

  # POST /about_tagging_programs
  def create
    authorize! :create, AboutTaggingProgram
    @about_tagging_program = AboutTaggingProgram.new(about_params)

    respond_to do |format|
      if @about_tagging_program.save
        format.html { redirect_to @about_tagging_program, notice: 'About tagging program was successfully created.' }
      else
        format.html { render action: 'new' }
      end
    end
  end

  # PUT /about_tagging_programs/1
  def update
    authorize! :update, @about_tagging_program
    respond_to do |format|
      if @about_tagging_program.update_attributes(about_params)
        flash.now[:notice] = 'About tagging program was successfully updated.'
        format.html { render action: 'edit' }
      else
        flash.now[:error] = 'Could not update.'
        format.html { render action: 'edit' }
      end
    end
  end

  # DELETE /about_tagging_programs/1
  def destroy
    authorize! :destroy, @about_tagging_program
    @about_tagging_program.destroy

    respond_to do |format|
      format.html { redirect_to about_tagging_programs_url }
    end
  end

  private

  def set_about_tagging_program
    @about_tagging_program = AboutTaggingProgram.find(params[:id])
  end

  def about_params
    params.require(:about_tagging_program).permit(:description)
  end
end

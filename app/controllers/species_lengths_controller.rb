# == Schema Information
# Schema version: 20160405165101
#
# Table name: species_lengths
#
#  id          :integer          not null, primary key
#  description :string(255)      not null
#  species_id  :integer          not null
#  position    :integer          not null
#  created_at  :datetime
#  updated_at  :datetime
#  min         :float
#  max         :float
#
# Indexes
#
#  idx_species_length_min_max  (min,max)
#
# Foreign Keys
#
#  species_lengths_species_fk  (species_id => species.id)
#

class SpeciesLengthsController < ApplicationController

  before_action :set_species_length, only: [:show, :edit, :update, :destroy]

  # GET /species_lengths
  def index
    authorize! :read, SpeciesLength
    @species_lengths = SpeciesLength.all
  end

  # GET /species_lengths/1
  def show
    authorize! :read, @species_length
  end

  # GET /species_lengths/new
  def new
    authorize! :create, SpeciesLength
    @species_length = SpeciesLength.new
  end

  # GET /species_lengths/1/edit
  def edit
    authorize! :update, @species_length
  end

  # POST /species_lengths
  def create
    authorize! :create, SpeciesLength
    @species_length = SpeciesLength.new(species_length_params)

    if @species_length.save
      redirect_to @species_length, notice: 'Species length was successfully created.'
    else
      render action: 'new'
    end
  end

  # PATCH/PUT /species_lengths/1
  def update
    authorize :update, @species_length
    if @species_length.update(species_length_params)
      redirect_to @species_length, notice: 'Species length was successfully updated.'
    else
      render action: 'edit'
    end
  end

  # DELETE /species_lengths/1
  def destroy
    authorize! :destroy, @species_length
    @species_length.destroy
    redirect_to species_lengths_url, notice: 'Species length was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_species_length
      @species_length = SpeciesLength.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def species_length_params
      params.require(:species_length).permit(:description, :species_id, :position)
    end
end

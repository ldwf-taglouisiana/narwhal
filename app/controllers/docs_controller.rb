class DocsController < ApplicationController

  protect_from_forgery  except: [:show]

  def show

    begin
    # get the file path requested
    file_path = "#{Rails.root}/static/docs/#{doc_params}"

    # if there was no ID then redirect to the index page
    if params[:id].nil?
      redirect_to '/docs/index.html'
    end

    ## if we are behind a Nginx server, just send the files
    if Rails.env == 'production' || Rails.env == 'staging'
      send_file(file_path, disposition: 'inline')

    # if on localhost
    else

      # render html files
      if file_path.include? '.html' or params[:format].downcase == 'html'
        render :file => file_path, :status => :ok, :layout => false

      # send non html files
      else
        send_file(file_path, disposition: 'inline')
      end

    end # end if

    rescue
      raise ActionController::RoutingError.new('Not Found')
    end


  end # end show

  private

  def doc_params
    path = params[:id]

    # if there was a format, then append it to file requested
    if params[:format] != nil
      path = path + '.' + params[:format]
    end

    return path
  end

end
class SearchController < ApplicationController

  skip_authorization_check

  # GET /search
  # GET /search.json
  def index
    # this scan/map combo allows us to search for either multiple space seperated terms or multiple quoted space seperated terms
    # in the everything box
    # bob smith returns all bobs and all smiths
    # "bob smith" would return all bob smiths
    @term =  ([params[:term].scan(/"[^"]*"|'[^']*'|[^"'\s]+/).map{ |m| m.gsub('\'','').gsub('"','')}] + [params[:angler_id_search].gsub(/\s+/, '')]).join(' ').strip
    @results = {}
  end

  def get_search_data
    @term = params[:term]
    @results = do_search(@term,params)[params[:q].to_sym]
    @row = ROW_MAP[params[:q].to_sym]
    render 'search/search_data', :layout => false
  end

  SEARCH_METHODS = {
      :captures => ->(term) { PresentableCapture.search(term, verified: true) },
      :draft_captures => ->(term) { DenormalizedDraftCapture.search(term).includes(:draft_capture) },
      :recaptures => ->(term) { PresentableRecapture.search(term, verified: true) },
      :unverified_recaptures => ->(term) { PresentableRecapture.search(term, verified: false) },
      :unverified_captures => ->(term) { PresentableCapture.search(term, verified: false) },
      :tags => ->(term) { PresentableTag.search(term) },
      :anglers => -> (term){ Angler.search(term) },
      :volunteer_time_logs => ->(term) { PresentableVolunteerHour.search(term) },
      :users => ->(term) { User.includes(:angler).search(term).with_role_list(with_search: true) }
  }

  ROW_MAP = {
    :captures => 'capture',
    :draft_captures => 'draft_capture',
    :recaptures => 'recapture',
    :unverified_recaptures => 'recapture',
    :unverified_captures => 'capture',
    :tags => 'tag',
    :anglers => 'angler',
    :volunteer_time_logs => 'volunteer_hour',
    :users => 'user'
  }

  private
    def do_search(term,params)
      q = params[:q].try(:to_sym)
      if q
        { q => SEARCH_METHODS[q].call(term) }
      else
        Hash[SEARCH_METHODS.map{ |k,v| [k,v.call(term)] }]
      end
    end
end

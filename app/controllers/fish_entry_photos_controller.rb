# == Schema Information
# Schema version: 20170724200101
#
# Table name: fish_entry_photos
#
#  id                        :integer          not null, primary key
#  fish_entry_photoable_type :string           not null
#  fish_entry_photoable_id   :integer          not null
#  image_file_name           :string
#  image_content_type        :string
#  image_file_size           :integer
#  image_updated_at          :datetime
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#

class FishEntryPhotosController < ApplicationController

  before_action :set_fish_entry_photo, :only => [:image,:destroy]

  # GET /blurbs
  def image
    authorize! :read, @fish_entry_photo
    style = params[:style] ? params[:style] : 'original'
    send_file(@fish_entry_photo.image.path(style), {type: @fish_entry_photo.image_content_type, disposition: 'inline'})
  end

  def destroy
    authorize! :destroy, @fish_entry_photo
    @fish_entry_photo.destroy!
    redirect_to @fish_entry_photo.fish_entry_photoable
  end

  private

  def set_fish_entry_photo
    @fish_entry_photo = FishEntryPhoto.find(params[:id])
  end

end

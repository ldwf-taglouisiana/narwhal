# == Schema Information
# Schema version: 20160405165101
#
# Table name: draft_captures
#
#  id                       :integer          not null, primary key
#  tag_number               :string(255)      not null
#  capture_date             :datetime         not null
#  species_id               :integer
#  length                   :float
#  location_description     :text
#  latitude                 :float
#  longitude                :float
#  fish_condition_id        :integer
#  weight                   :float
#  comments                 :text
#  recapture                :boolean          default(FALSE)
#  recapture_disposition_id :integer
#  time_of_day_id           :integer
#  entered_gps_type         :string(255)
#  error_json               :string(255)
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  should_save              :boolean          default(FALSE)
#  saved_at                 :datetime
#  species_length_id        :integer
#  geom                     :geometry(Point,4
#  uuid                     :string(255)
#  search_vector            :tsvector         not null
#  angler_id                :integer          not null
#
# Indexes
#
#  index_draft_captures_on_search_vector  (search_vector)
#  index_draft_captures_on_uuid           (uuid) UNIQUE
#
# Foreign Keys
#
#  draft_captures_species_length_id_fkey  (species_length_id => species_lengths.id)
#  fk_draft_captures_gps_type             (entered_gps_type => gps_formats.format_type)
#  fk_part_captures_fish_cond             (fish_condition_id => fish_condition_options.id)
#  fk_part_captures_recap_disp            (recapture_disposition_id => recapture_dispositions.id)
#  fk_part_captures_species_id            (species_id => species.id)
#  fk_part_captures_time_of_day           (time_of_day_id => time_of_day_options.id)
#  fk_rails_18ffac8d12                    (angler_id => anglers.id)
#

class DraftCapturesController < ApplicationController
  include FishEntryActionable, DownloadableActionable, DatatableActionable

  before_action :set_draft_capture, only: [:show, :edit, :get_status, :update, :destroy, :move_to_capture]


  def resource_class
    DraftCapture
  end

  # GET /draft_captures
  # GET /draft_captures.json
  def index
    authorize! :read, DraftCapture
    respond_to do |format|
      format.html {
        @draft_captures = DraftCapture.all
      }
      format.json {
        render json: DraftCapture.datatable_list(data_table_params)
      }
    end
  end

  # GET /draft_captures/1
  # GET /draft_captures/1.json
  def show
    authorize! :read, @draft_capture
    errors = JSON.parse(@draft_capture.error_json, symbolize_names: true).try(:[], :errors)
    if errors
      errors = ['This draft capture has the following errors', errors.map{|e| "• #{e}"} ].flatten
      flash[:error] = errors.join('</br>').html_safe
    end
  end

  # GET /draft_captures/new
  # GET /draft_captures/new.json
  def new
    authorize! :create, DraftCapture
    @draft_capture = DraftCapture.new
  end

  # GET /draft_captures/1/edit
  def edit
    authorize! :create, DraftCapture
  end

  # POST /draft_captures
  # POST /draft_captures.json
  def create
    authorize! :create, DraftCapture
    @draft_capture = DraftCapture.new(draft_capture_params)

    respond_to do |format|
      format.html {
        if @draft_capture.save
          redirect_to @draft_capture, notice: 'Draft capture was successfully created.'
        else
          render action: :new
        end
      }
    end
  end

  # PUT /draft_captures/1
  # PUT /draft_captures/1.json
  def update
    authorize! :update, @draft_capture
    respond_to do |format|
      format.html {
        if @draft_capture.update_attributes(draft_capture_params)
          redirect_to @draft_capture, notice: 'Draft capture was successfully updated.'
        else
          render action: :edit
        end
      }
    end
  end

  # DELETE /draft_captures/1
  # DELETE /draft_captures/1.json
  def destroy
    authorize! :destroy, @draft_capture
    @draft_capture.destroy!

    respond_to do |format|
      format.html { redirect_to captures_path }
      format.json { head :no_content }
    end
  end

  def get_status
    authorize! :read, @draft_capture

    render json: @draft_capture.status
  end

  def move_to_capture
    authorize! :update, @draft_capture
    fish_event = @draft_capture.try(:move_to_capture, params[:admin].present?)

    respond_to do |format|
      format.html {
        if fish_event.nil?
          redirect_to captures_path, notice: 'Draft Capture is no longer available'
        elsif fish_event
          redirect_to fish_event, notice: 'Draft capture converted successfully.'
        else
          redirect_to captures_path, notice: 'Error converting draft capture.'
        end
      }
    end
  end

  #Take the id's of many draft captures and delete all of them
  def multi_delete
    authorize! :destroy, DraftCapture
    result = {status: :ok}

    begin
      @draft_captures = DraftCapture.find(params[:options])

      @draft_captures.each { |cap| cap.destroy! }

      #if anything went wrong, build the error message
    rescue
      result[:error] = 'Some or all Draft Captures could not be deleted. Try refreshing the page.'
      result[:status] = :unprocessable_entity
      flash[:error] = result[:error]
    end

    respond_to do |format|
      #send the result to the user
      format.html { redirect_to_url captures_path}
      format.json { render json: result, status: result[:status] }
    end

  end

  def move_to_captures
    authorize! :update, DraftCapture
    result = {status: :ok}
    begin
      @draft_captures = DraftCapture.find(params[:options])

      @draft_captures.each { |cap| cap.move_to_capture(true) }

    rescue
      result[:error] = 'Some or all Draft Captures could not be converted. Try refreshing the page.'
      result[:status] = :unprocessable_entity
      flash[:error] = result[:error]
    end

    respond_to do |format|
      format.html { redirect_to_url captures_path}
      format.json { render json: result, status: result[:status] }
    end
  end

  def download
    authorize! :report, DraftCapture
    # create the token
    file_token = generate_download_token

    # create the download item with the token, the filename, and the id of the user that created it
    RequestedDownload.create!(token: file_token, filename: 'draft_captures.xlsx', user_id: current_user.id)
    DownloadIndexDataJob.perform_later(DownloadIndexDataJob::REPORT_TYPES[:draft_capture], params, file_token)

    render json: { token: file_token }
  end

  def refresh_errors
    authorize! :update, DraftCapture
    DraftCapture.refresh_errors
    redirect_to :back
  end

  private

  def set_draft_capture
    @draft_capture = DraftCapture.find(params[:id])
  end

  def draft_capture_params
    params.require(:draft_capture).permit(:recapture, :tag_number, :capture_date, :time_of_day_id, :angler_id, :species_id,
                                          :species_length_id, :length, :fish_condition_id, :recapture_disposition_id,
                                          :entered_gps_type, :latitude, :longitude, :location_description, :comments, :id,
                                          :tag_number, :error_json, :created_at, :updated_at, :should_save, :saved_at, :uuid)
  end
end

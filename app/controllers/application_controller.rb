class ApplicationController < ActionController::Base

  protect_from_forgery

  before_action :authenticate_user!
  before_action :validate_proper_roles
  after_action :prepare_unobtrusive_flash

  after_action :set_updating_message
  check_authorization unless: :devise_controller?

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_path, :alert => exception.message
  end

  def append_info_to_payload(payload)
    super
    client = DeviceDetector.new(request.user_agent)

    payload[:remote_ip] = request.remote_ip
    payload[:user_id] = current_user.try(:id)
    payload[:device] = {
        device_type: client.device_type,
        device_name: client.device_name,
        os_name: client.os_name,
        name: client.name
    }

  end

  def info_for_paper_trail
    client_device = DeviceDetector.new(request.user_agent)

    {
        meta: {
            client_info: {
                user_agent: request.user_agent,
                ip: request.remote_ip,
                name: client_device.name,
                version: client_device.full_version,
                os: client_device.os_name,
                os_version: client_device.os_full_version,
                device: client_device.device_name,
                device_type: client_device.device_type
            }
        }
    }
  end

  def make_request_from_url(url, options = {})
    user_agent = request.user_agent

    uri = URI.parse(url)

    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = uri.scheme == 'https'

    # turn off cert validation when in staging mode
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE if Rails.env.staging?

    case options[:method]
      when :post
        request =  Net::HTTP::Post.new(uri.request_uri)
      else
        request =  Net::HTTP::Get.new(uri.request_uri)
    end

    request.body = options[:body] if options[:body].present?

    # forward the user agento the api server
    request.add_field('User-Agent', user_agent)

    options[:headers].try(:each) do |k, v|
      request.add_field(k,v)
    end

    http.request(request)
  end

  def validate_proper_roles
    # Regular users shuld not be able to view pages (CURRENTLY TURNED OFF).
    unless self.is_a?(Devise::SessionsController) || self.is_a?(Api::V1::TokensController)
      if defined?(current_user) && !current_user.nil? && (current_user.roles.to_a & [Role.ldwf_role,Role.admin_role,Role.volunteer_role]).count  == 0
        sign_out(current_user)
        flash.clear
        flash[:error] = 'You do not have permission to view this page.'
        redirect_to root_path
      end
    end
  end

  def redirect_to_url(default_url)
    params[:redirect_url].nil? ? redirect_to(default_url) : redirect_to(params[:redirect_url])
  end

  private

  def set_updating_message
    if request.format.html?
      message = []

      if $redis_store.get(GlobalConstants::REDIS_VOLUNTEER_HOURS_UPDATING_KEY).to_i > 0
        message << '• We are updating the volunteer hours dataset, some items may not be searchable at this time.'
      end

      if $redis_store.get(GlobalConstants::REDIS_CAPTURE_UPDATING_KEY).to_i > 0
        message << '• We are updating the captures dataset, some items may not be available at this time.'
      end

      if $redis_store.get(GlobalConstants::REDIS_RECAPTURE_UPDATING_KEY).to_i > 0
        message << '• We are updating the recaptures dataset, some items may not be available at this time.'
      end

      if $redis_store.get(GlobalConstants::REDIS_TAG_UPDATING_KEY).to_i > 0
        message << '• We are updating the tags dataset, some items may not be available at this time.'
      end

      flash[:sys_info] = message.join('<br>').html_safe if message.any?
    end
  end
end

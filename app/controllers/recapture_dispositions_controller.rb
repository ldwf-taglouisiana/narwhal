# == Schema Information
# Schema version: 20160405165101
#
# Table name: recapture_dispositions
#
#  id                    :integer          not null, primary key
#  recapture_disposition :string(255)
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#

class RecaptureDispositionsController < ApplicationController

  before_action :set_recapture_dispostion, only: [:show, :edit, :update, :destroy]

  # GET /recapture_disposition_ids
  def index
    authorize! :read, RecaptureDisposition
    @recapture_dispositions = RecaptureDisposition.all
  end

  # GET /recapture_dispositions/1
  def show
    authorize! :read, @recapture_disposition
  end

  # GET /recapture_dispositions/new
  def new
    authorize! :create, RecaptureDisposition
    @recapture_disposition = RecaptureDisposition.new
  end

  # GET /recapture_dispositions/1/edit
  def edit
    authorize! :update, @recapture_disposition
  end

  # POST /recapture_dispositions
  def create
    authorize! :create, RecaptureDisposition
    @recapture_disposition = RecaptureDisposition.new(params[:recapture_disposition])

    respond_to do |format|
      if @recapture_disposition.save
        format.html { redirect_to @recapture_disposition, notice: 'Recapture disposition was successfully created.' }
      else
        format.html { render action: 'new' }
      end
    end
  end

  # PUT /recapture_dispositions/1
  def update
    authorize! :update, @recapture_disposition
    respond_to do |format|
      if @recapture_disposition.update_attributes(params[:recapture_disposition])
        format.html { redirect_to @recapture_disposition, notice: 'Recapture disposition was successfully updated.' }
      else
        format.html { render action: 'edit' }
      end
    end
  end

  # DELETE /recapture_dispositions/1
  def destroy
    authorize! :destroy, @recapture_disposition
    @recapture_disposition.destroy

    respond_to do |format|
      format.html { redirect_to recapture_dispositions_url }
      format.json { head :no_content }
    end
  end

  private

  def set_recapture_dispostion
    @recapture_disposition = RecaptureDisposition.find(params[:id])
  end
end

# == Schema Information
# Schema version: 20160405165101
#
# Table name: announcements
#
#  id              :integer          not null, primary key
#  content         :text
#  created_at      :datetime
#  updated_at      :datetime
#  expiration_date :datetime
#

class AnnouncementsController < ApplicationController
  before_action :set_announcement, only: [:show, :edit, :update, :destroy]

  # GET /announcements
  def index
    authorize! :read, Announcement
    @announcements = Announcement.all
  end

  # GET /announcements/1
  def show
    authorize! :read, @announcement
  end

  # GET /announcements/new
  def new
    authorize! :create, Announcement
    @announcement = Announcement.new
  end

  # GET /announcements/1/edit
  def edit
    authorize! :update, @announcement
  end

  # POST /announcements
  def create
    authorize! :create, Announcement
    @announcement = Announcement.new(announcement_params)

    if @announcement.save
      redirect_to @announcement, notice: 'Announcement was successfully created.'
    else
      render action: 'new'
    end
  end

  # PATCH/PUT /announcements/1
  def update
    if @announcement.update(announcement_params)
      flash.now[:notice] = 'Announcement updated.'
      render action: 'edit'
    else
      flash.now[:error] = 'Could not update.'
      render action: 'edit'
    end
  end

  # DELETE /announcements/1
  def destroy
    authorize! :destroy, @announcement
    @announcement.destroy
    redirect_to announcements_url, notice: 'Announcement was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_announcement
      @announcement = Announcement.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def announcement_params
      params.require(:announcement).permit(:content,:expiration_date)
    end
end

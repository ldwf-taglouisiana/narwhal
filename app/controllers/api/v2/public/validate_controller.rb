module Api::V2::Public
  class ValidateController < ApiController

    before_filter :authorize_app!

    # checks to see if a given tag number is already associated with a capture
    def new_capture
      data = Hash.new
      result = Capture.joins(:tag).where( tags: {tag_no: params[:tag_number]}).empty?
      data[:result] = result
      render :json => data
    end

    #Used by the general public (nessie and apps) to determine if a tag is usable in context
    def tag_exists
      tag = Tag.where(:tag_no => params[:fieldValue]).first

      answer = []
      answer << params[:fieldId]

      # if the field is empty
      if params[:fieldValue].empty?
        answer << true

      # no tag exists
      elsif tag.nil?
        answer << false
        answer << 'Tag does not exist'

      # the tag hasn't been assigned to anyone
      elsif tag.assigned_at
        answer << false
        answer << 'Tag is not available for use'

      # The tag has been marked as lost or broken (deactivated)
      elsif !tag.active
        answer << false
        answer << 'This tag has been deactivated'

      # a previous capture exists
      elsif Capture.where('tag_id = ?', tag.id).to_a.length > 0 and (params[:secret_recap] == 'false' or params[:secret_recap] == '')
        answer << false
        answer << 'A capture already exists for this tag'

      # default to valid
      else
        answer << true

      end

      render :json => answer

    end

    def email_exists
      answer = []
      answer << params[:fieldId]

      if User.where(email: params[:fieldValue]).any?
        answer << false
      else
        answer << true
      end

      respond_to do |format|
        format.json { render json: answer }
      end
    end
  end
end
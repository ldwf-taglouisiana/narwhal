module Api::V2::Public
  class InfoController < ApiController
    include DashboardModule

    resource_description do
      name 'Public Information API'
      short 'LDWF Public Information API'
      api_base_url "/api/v2/public/info"
      error code: 422, desc: 'Missing or malformatted parameters'
      error code: 500, desc: 'Server had an unexpected issue processing your request'
      desc <<-EOS
        == Long description
        This API provides some general information that can be posted via the Narwhal CMS
      EOS
    end

    def_param_group :basic do
      param :limit, :number, desc: 'The number of records to return must be <= 200'
      param :offset, :number, desc: 'The offset of the items to fetch, Must be >= 0'
    end

    api :GET, '/about'
    description <<-EOS
      Get announcements for front facing applications
    EOS
    param_group :basic
    def about
      @items = basic_data_helper(params, nil, AboutTaggingProgram)
    end

    api :GET, '/announcements'
    description <<-EOS
      Get announcements for front facing applications
    EOS
    param_group :basic
    def announcements
      processed_params = preprocess_params(params)

      query = Announcement
      query = query.where('expiration_date > ?', Time.now)
      query = query.reorder(updated_at: :desc).limit(processed_params[:limit]).offset(processed_params[:offset] || 0)

      @items = {
          errors: [],
          id_hash: Announcement.ids_as_md5,
          version: query.max_by(&:updated_at).try(:updated_at),
          count: query.count,
          items: query
      }
    end

    api :GET, '/blurbs'
    description <<-EOS
    Get the current list of blurbs
    EOS
    param :includes, %w(about_program capture_dashboard common_fish contact_info draft_captures find_a_spot how_to licensing my_captures my_tags new_capture order_tags sign_up target_species volunteer_hours contact_us account_info faq privacy_policy)
    def blurbs
      default_includes = %w(about_program capture_dashboard common_fish contact_info draft_captures find_a_spot how_to licensing my_captures my_tags new_capture order_tags sign_up target_species volunteer_hours contact_us account_info faq privacy_policy)
      @items = basic_data_helper(params, default_includes, Blurb)
    end

    api :GET, '/dashboard'
    description <<-EOS
    Get the public stats dashboard for the program
    EOS
    def dashboard
      items = {
          captures: {
              total: Capture.count,
              species: Capture.in_time_range(30.days.ago, Time.now).target_species_counts_h,
              season: {
                  current: Capture.in_current_season.count,
                  previous: Capture.in_last_season.count
              }
          },

          recaptures: {
              total: Recapture.count,
              species: Recapture.in_time_range(30.days.ago, Time.now).target_species_counts_h,
              season: {
                  current: Recapture.in_current_season.count,
                  previous: Recapture.in_last_season.count
              }
          },

          season: {
              previous: {
                  start: {
                      epoch: Capture.last_season.first.to_i,
                      iso: Capture.last_season.first.iso8601,
                  },
                  end: {
                      epoch:  Capture.last_season.last.to_i,
                      iso:  Capture.last_season.last.iso8601
                  }
              },
              current: {
                  start: {
                      epoch: Capture.this_season.first.to_i,
                      iso: Capture.this_season.first.iso8601
                  },
                  end: {
                      epoch: Capture.this_season.last.to_i,
                      iso: Capture.this_season.last.iso8601
                  }
              }
          }
      }

      # get a MD5 sum of the result, so we can fingerprint it with the current state of the dashboard
      md5 = Digest::MD5.new
      md5.update items.to_json

      items[:hash] = md5.hexdigest

      render json: items
    end

    api :GET, '/how_to'
    description <<-EOS
    Get the "How to Tag" information. Tnstructs the anglers how to properly tag a fish.
    EOS
    param_group :basic
    def how_to
      @items = basic_data_helper(params, nil, HowToTag)
    end

    api :GET, '/news'
    description <<-EOS
    Get any news items that may have been published
    EOS
    param_group :basic
    def news
      processed_params = preprocess_params(params)

      query = NewsItem
      query = query.where(should_publish: true).where('should_publish_at < ?', Time.now)
      query = query.includes(:user).reorder(should_publish_at: :desc).limit(processed_params[:limit]).offset(processed_params[:offset] || 0)

      @items = {
          errors: [],
          id_hash: NewsItem.ids_as_md5,
          version: query.max_by(&:updated_at).try(:updated_at),
          count: query.count,
          items: query
      }
    end
  end
end
module Api::V2::Public
  class PhotosController < ApiController

    resource_description do
      name 'Public Photos API'
      short 'LDWF Public Photos API'
      api_base_url "/api/v2/public/photos"
      error code: 422, desc: 'Missing or malformatted parameters'
      error code: 500, desc: 'Server had an unexpected issue processing your request'
      desc <<-EOS
        == Long description
        This API provides endpoints to get photos
      EOS
    end

    skip_after_action :cache_buster
    skip_after_action :set_headers

    api :GET, '/stream'
    description <<-EOS
      Get a list of photos that are set as the photo stream for the front page
    EOS
    param :updated_after, Time, desc: 'The version of the last image fetched, will filter the images to only ones with a newer version value'
    def stream
      @items = basic_data_helper(@api_params, [], FrontPagePhotoStreamItem)
    end

    api :GET, '/:id/:image_name'
    description <<-EOS
      Get an image buy its id
    EOS
    param :id, :number, desc: 'The id of the requested image, must be a positive integer'
    param :style, String, desc: 'The style of the image, corresponds to the image size'
    def send_photo
      photo = Photo.find(@api_params[:id])
      style = params[:style] ? @api_params[:style] : 'original'
      send_file(photo.image.path(style), {type: photo.image_content_type, disposition: 'inline'})
    end

  end
end
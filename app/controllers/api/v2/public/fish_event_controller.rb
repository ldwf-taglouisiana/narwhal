module Api::V2::Public
  class FishEventController < ApiController
    include DashboardModule, FuzzyMapModule, GeoDataActionable

    before_filter :authorize_app!, only: [:recapture]

    resource_description do
      name 'Public Fish Event API'
      short 'LDWF Public Account API'
      api_base_url "/api/v2/public/fish_event"
      error code: 401, desc: 'The application is not authorized to use these resources'
      error code: 422, desc: 'Missing or malformatted parameters'
      error code: 500, desc: 'Server had an unexpected issue processing your request'
      desc <<-EOS
        == Long description
        This API provides tools to manage som account that are stored on Narwhal.
        Mainly for registering new accounts and resetting passwords.
      EOS
    end

    api :GET, '/map_data'
    description <<-EOS
      Get map data to show events, in either a basin map, or a heat map
    EOS
    param :start_date, Time, desc: 'The start date to look at. Must be an ISO8601 date string. '
    param :end_date, Time, desc: 'The end date to look at. Must be an ISO8601 date string. '
    param :number_of_days, :number, desc: 'The number of day previous to look at'                 #seems a bit hacky the param can't be an integer... it must be a string need to make sure this still works as intended
    param :recapture, :bool, desc: 'Should we look at recaptures or not', required: true
    param :map_type, ['Heat Map', 'Basin'], desc: 'The type of map data to return', required: true
    def map_data
      case params[:map_type]
        when 'Basin'
          @items = helper_event_basin_data(geo_json_params(@api_params))
        when 'Heat Map'
          @items = helper_event_heatmap(geo_json_params(@api_params))
        else
          # do nothing
      end
    end

    api :GET, '/draft_capture'
    description <<-EOS
      Get an empty draft capture object
    EOS
    def draft_capture
      data = Hash.new
      data['draft_capture'] = DraftCapture.new
      render :json => data
    end

    api :POST, '/recapture'
    description <<-EOS
      Allows the reporting of a non registered recapture.
    EOS
    param :recapture, Hash, required: true, desc: 'The recapture data' do
      param :capture_date, Time, required: true, desc: 'The date of the capture, as an iso string '
      param :comments, String, desc: 'Any comments about the recapture'
      param :entered_gps_type, GPSFormat.pluck(:format_type).sort, required: true, desc: 'The latitude. Must be a value from 0.0 to 90.0'
      param :fish_condition_id, lambda { |val|  FishConditionOption.where(id: val).any? }, as: :fish_condition_option_id, desc: 'Condition of the fish upon capture'
      param :latitude, Api::V2::ApiController::LATITUDE_LAMBDA, required: true, desc: 'The latitude. Must be a value from 0.0 to 90.0'
      param :length, Float, desc: 'The numerical length of the recapture'
      param :location_description, String, desc: 'The location description'
      param :longitude, Api::V2::ApiController::LONGITUDE_LAMBDA, required: true, desc: 'The longitude. Must be a value from -180.0 to 0.0'
      param :recapture_disposition_id, lambda { |val| RecaptureDisposition.where(id: val).any? }, desc: 'The recapture disposition'
      param :species_id, lambda { |val| Species.where(id: val).any? }, required: true, desc: 'The species '
      param :species_length_id, lambda { |val| SpeciesLength.where(id: val).any? }, desc: 'The species length option'
      param :tag_number, Api::V2::ApiController::TAG_REGEX, required: true, desc: 'The tag number, must be in the format of a alpha prefix followed by a six digit number'
      param :time_of_day_id, lambda { |val| TimeOfDayOption.where(id: val).any? }, as: :time_of_day_option_id, required: true, desc: 'The time of day'
      param :images, Array, allow_nil: true do
        param :raw_data, String, required: true
      end
    end
    param :angler, Hash, required: true, desc: 'The angler data' do
      param :email, Api::V2::ApiController::EMAIL_REGEX, required: true, desc: 'The email address, must be in the correct format'
      param :first_name, String, required: true, desc: 'The first name'
      param :last_name, String, required: true, desc: 'The last name'
      param :shirt_size_id, lambda { |val| ShirtSizeOption.where(id: val).any? }, as: :shirt_size, desc: 'The angler\'s shirt size'
      param :phone_number, Api::V2::ApiController::PHONE_REGEX, as: :phone_number_1, required: true, desc: 'The phone number, has to be 10 digits'
    end
    def recapture
      status = :internal_server_error

      data = Hash.new
      data[:errors] = []
      event = nil

      begin
        Recapture.transaction do
          images= @api_params[:recapture][:images]

          angler = Angler.where('first_name ILIKE ? AND last_name ILIKE ? AND email ILIKE ?', @api_params[:angler][:first_name], @api_params[:angler][:last_name],  @api_params[:angler][:email]).first
          angler = Angler.new_app_angler(@api_params[:angler]) unless angler
          angler.save! unless angler.persisted?

          event = Recapture.create!(@api_params[:recapture].except(:images).merge({angler: angler, user_capture: true, verified: false}))
          event

          if images.present?
            images.each do |image|
              photo = FishEntryPhoto.new(fish_entry_photoable: event)
              photo.image = StringIO.new(Base64.decode64(image[:raw_data]))
              photo.save!
            end
          end
        end # end transaction
      rescue => error
        logger.fatal 'Error occurred'
        logger.fatal error
        data[:errors] << 'Could not save some or all of the captures'
        status = :unprocessable_entity
      else
        Notification.build_unregistered_recapture(event)
        status = :ok
      end

      render json: data, status: status
    end
  end
end
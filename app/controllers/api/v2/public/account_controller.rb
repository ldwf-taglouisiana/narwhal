module Api::V2::Public
  class AccountController < ApiController

    before_filter :authorize_app!, only: [:register, :reset_password]

    resource_description do
      name 'Public Account API'
      short 'LDWF Public Account API'
      api_base_url "/api/v2/public/account"
      error code: 401, desc: 'The application is not authorized to use these resources'
      error code: 422, desc: 'Missing or malformatted parameters'
      error code: 500, desc: 'Server had an unexpected issue processing your request'
      desc <<-EOS
        == Long description
        This API provides tools to manage som account that are stored on Narwhal.
        Mainly for registering new accounts and resetting passwords.
      EOS
    end

    api :GET, '/register'
    description <<-EOS
    Register a new account
    EOS
    param :user, Hash, required: true do
      param :email, Api::V2::ApiController::EMAIL_REGEX, desc: 'Email', required: true
      param :password, Api::V2::ApiController::PASSWORD_REGEX, desc: 'Password, min 8 characters', required: true
      param :password_confirmation, Api::V2::ApiController::PASSWORD_REGEX, desc: 'Password confirmation, min 8 characters', required: true
      param :is_existing_angler, [true,false], desc: 'True or false', required: false
    end
    param :angler, Hash, required: true do
      param :first_name, String, desc: 'First name', required: true
      param :last_name, String, desc: 'Last name', required: true
      param :street, String, desc: 'Street address'
      param :suite, String, desc: 'Suite'
      param :city, String, desc: 'City'
      param :state, Api::V2::ApiController::STATE_REGEX, desc: 'State abbreviation'
      param :zip, Api::V2::ApiController::ZIP_CODE_REGEX, desc: 'Zip code, has to contain 5 numbers'
      param :phone_number, Api::V2::ApiController::PHONE_REGEX, as: :phone_number_1, required: true, desc: 'The phone number, has to be 10 digits'
      param :shirt_size_id, lambda { |val| ShirtSizeOption.where(id: val).any? }, as: :shirt_size, desc: 'The angler\'s shirt size'
    end
    def register
      data = {
          errors: [],
          messages: []
      }
      status = :internal_server_error

      #We only want to service apps that have been registered with the system, so see if they stack up
      begin
        User.transaction do

          user = User.new(@api_params[:user])
          angler = Angler.where('first_name ILIKE ? AND last_name ILIKE ? AND email ILIKE ?', @api_params[:angler][:first_name], @api_params[:angler][:last_name],  @api_params[:angler][:email]).first
          angler = Angler.new_app_angler(@api_params[:angler]) unless angler
          user.angler = angler
          angler.save! unless angler.persisted?

          user.first_name = angler.first_name
          user.last_name = angler.last_name
          user.angler_id = angler.angler_id

          user.save
          user.add_role Role.angler_role.name

          UserNotifyMailer.new_user_email(user).deliver if Rails.env == 'production'
          Notification.build_user(user)
        end
      rescue => error
        logger.fatal 'Error Occurred'
        logger.fatal error
        data[:errors] << error.message #'Could not register the user'
        status = :internal_server_error
      else
        data[:messages] << 'Successfully created the user'
        status = :ok
      end

      render :json => data, :status => status
    end


    api :GET, '/reset_password'
    description <<-EOS
      Reset the password for the account that has the provided email
    EOS
    param :email, Api::V2::ApiController::EMAIL_REGEX, desc: 'Email', required: true
    param :password, Api::V2::ApiController::PASSWORD_REGEX, desc: 'Password, min 8 characters', required: true
    param :password_confirmation, Api::V2::ApiController::PASSWORD_REGEX, desc: 'Password confirmation, min 8 characters', required: true
    def reset_password
      data = Hash.new

      user = User.where(email: @api_params[:email]).first
      user.update_attributes(:password => @api_params[:password], :password_confirmation => @api_params[:password_confirmation])
      data[:change_confirmation] = true

      render :json => data
    end
  end
end

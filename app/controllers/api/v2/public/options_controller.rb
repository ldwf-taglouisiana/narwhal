module Api::V2::Public
  class OptionsController < ApiController

    resource_description do
      name 'Public Options API'
      short 'LDWF Public Options API'
      api_base_url "/api/v2/public/options"
      formats %w(json jsonp)
      error code: 422, desc: 'Missing or malformatted parameters'
      error code: 500, desc: 'Server had an unexpected issue processing your request'
      desc <<-EOS
        == Long description
        This API provides some of the core relational data used in createing and viewing fish events.
        the <tt>/combined</tt> action is a "big cookie" route for getting all the data in one request.
      EOS
    end

    def_param_group :basic do
      param :only_id_list, :bool, desc: 'Send the id list back to the client'
      param :updated_after, Time, desc: 'Return items greater than the provide date string'
      param :limit, :number, desc: 'The number of records to return must be <= 200'
      param :offset, :number, desc: 'The offset of the items to fetch, Must be >= 0'
    end

    # ---------------------------------------------------------------------------------
    # OPTIONS ROUTES

    api :GET, '/combined'
    description <<-EOS
      Will get all of the other <tt>/api/v2/public/options/*</tt> in one API call.
    EOS
    param :species, Hash do
      param :id, lambda { |val| Species.where(id: val).any? }, desc: 'Species to fetch'
      param :only_id_list, :bool, desc: 'Send the id list back to the client'
      param :term, String, desc: 'A search term to find species that has attributes common_name, proper_name, other_name that match the given term'
      param :target, :bool, desc: 'Only get the target species'
      param :image_size, %w(medium feature index_list thumb )
      param :includes, Array, of: %w(id common_name position proper_name other_name family habitat description size food_value target publish)
      param :updated_after, Time, desc: 'Return items greater than the provide version date string'
      param :limit, Integer, desc: 'The number of records to return must be <= 200'
      param :offset, Integer, desc: 'The offset of the items to fetch, Must be >= 0'
    end
    param :species_lengths, Hash do
      param :id, lambda { |val| SpeciesLength.where(id: val).any? }, desc: 'Species id to fetch'
      param :only_id_list, :bool, desc: 'Send the id list back to the client'
      param :includes, Array, of: %w(id description species_id position version)
      param :updated_after, Time, desc: 'Return items greater than the provide version date string'
      param :limit, Integer, desc: 'The number of records to return must be <= 200'
      param :offset, Integer, desc: 'The offset of the items to fetch, Must be >= 0'
    end
    param :time_of_days, Hash do
      param :id, lambda { |val| TimeOfDayOption.where(id: val).any? }, desc: 'Time of day to fetch'
      param :only_id_list, :bool, desc: 'Send the id list back to the client'
      param :includes, Array, of: %w(id description species_id position version)
      param :updated_after, Time, desc: 'Return items greater than the provide version date string'
      param :limit, Integer, desc: 'The number of records to return must be <= 200'
      param :offset, Integer, desc: 'The offset of the items to fetch, Must be >= 0'
    end
    param :fish_conditions, Hash do
      param :id, lambda { |val| FishConditionOption.where(id: val).any? }, desc: 'Fish condition to fetch'
      param :only_id_list, :bool, desc: 'Send the id list back to the client'
      param :includes, Array, of: %w(id fish_condition_option version)
      param :updated_after, Time, desc: 'Return items greater than the provide version date string'
      param :limit, Integer, desc: 'The number of records to return must be <= 200'
      param :offset, Integer, desc: 'The offset of the items to fetch, Must be >= 0'
    end
    param :dispositions, Hash do
      param :id, lambda { |val| RecaptureDisposition.where(id: val).any? }, desc: 'Recapture disposition to fetch'
      param :only_id_list, :bool, desc: 'Send the id list back to the client'
      param :includes, Array, of: %w(id recapture_disposition version)
      param :updated_after, Time, desc: 'Return items greater than the provide version date string'
      param :limit, :number, desc: 'The number of records to return must be <= 200'
      param :offset, Integer, desc: 'The offset of the items to fetch, Must be >= 0'
    end
    param :shirt_sizes, Hash do
      param :id, lambda { |val| ShirtSizeOption.where(id: val).any? }, desc: 'Shirt size to fetch'
      param :only_id_list, :bool, desc: 'Send the id list back to the client'
      param :includes, Array, of: %w(id shirt_size_option version)
      param :updated_after, Time, desc: 'Return items greater than the provide version date string'
      param :limit, Integer, desc: 'The number of records to return must be <= 200'
      param :offset, Integer, desc: 'The offset of the items to fetch, Must be >= 0'
    end
    # param :includes, Array, in: [:species, :species_lengths, :time_of_days, :fish_conditions, :dispositions, :shirt_sizes]
    def combined
      updated_params = {
          species: {},
          species_lengths: {},
          fish_conditions: {},
          time_of_days: {},
          dispositions: {},
          shirt_sizes: {}
      }

      updated_params = updated_params.deep_merge(@api_params.try(:deep_symbolize_keys)) unless @api_params.nil?

      # get the items from the params and store them locally
      species = helper_find_species(updated_params[:species])
      species_lengths = helper_find_species_lengths(updated_params[:species_lengths])
      time_of_days = helper_time_of_days(updated_params[:time_of_days])
      fish_conditions = helper_fish_conditions(updated_params[:fish_conditions])
      dispositions = helper_dispositions(updated_params[:dispositions])
      shirt_sizes = helper_shirt_sizes(updated_params[:shirt_sizes])

      # find the max version from all the queried items
      version = [
          species[:version], species_lengths[:version], time_of_days[:version],
          fish_conditions[:version], dispositions[:version], shirt_sizes[:version]
      ].max

      hashes = [
          species[:id_hash], species_lengths[:id_hash], time_of_days[:id_hash],
          fish_conditions[:id_hash], dispositions[:id_hash], shirt_sizes[:id_hash]
      ].join

      # build the result hash
      @items =  {
          hash: Digest::MD5.hexdigest(version.iso8601 + hashes),
          version: version,
          species: species,
          species_lengths:species_lengths,
          time_of_days: time_of_days,
          fish_conditions: fish_conditions,
          dispositions: dispositions,
          shirt_sizes: shirt_sizes
      }
    end

    api :GET, '/time_of_days'
    description <<-EOS
    Get a list of species_lengths
    EOS
    param :id, lambda { |val| TimeOfDayOption.where(id: val).any? }, desc: 'Time of day to fetch'
    param :includes, Array, of: %w(id description species_id position version)
    param_group :basic
    def time_of_days
      @items = helper_time_of_days(@api_params)
    end

    api :GET, '/fish_conditions'
    description <<-EOS
    Get a list of fish conditions
    EOS
    param :id, lambda { |val| FishConditionOption.where(id: val).any? }, desc: 'Fish condition to fetch'
    param :includes, Array, of: %w(id fish_condition_option version)
    param_group :basic
    def fish_conditions
      @items = helper_fish_conditions(@api_params)
    end

    api :GET, '/dispositions'
    description <<-EOS
    Get a list of recapture dispositions
    EOS
    param :id, lambda { |val| RecaptureDisposition.where(id: val).any? }, desc: 'Recapture disposition to fetch'
    param :includes, Array, of: %w(id recapture_disposition version)
    param_group :basic
    def dispositions
      render :json => helper_dispositions(@api_params)
    end

    api :GET, '/shirt_sizes'
    description <<-EOS
    Get the list of shirt size options
    EOS
    param :id, lambda { |val| ShirtSizeOption.where(id: val).any? }, desc: 'Shirt size to fetch'
    param :includes, Array, of: %w(id shirt_size_option version)
    param_group :basic
    def shirt_sizes
      @items = helper_shirt_sizes(@api_params)
    end

    api :GET, '/species'
    description <<-EOS
    Get a list of species
    EOS
    param :id, lambda { |val| Species.where(id: val).any? }, desc: 'Species id to fetch'
    param :term, String, desc: 'A search term to find species that has attributes common_name, proper_name, other_name that match the given term'
    param :target, :bool, desc: 'Only get the target species'
    param :image_size, %w(medium feature index_list thumb )
    param :includes, Array, of: %w(id common_name position proper_name other_name family habitat description size food_value target publish)
    param_group :basic
    def species
      @items = helper_find_species(@api_params)
    end

    api :GET, '/species_lengths'
    description <<-EOS
    Get a list of species_lengths
    EOS
    param :id, lambda { |val| SpeciesLength.where(id: val).any? }, desc: 'Species id to fetch'
    param :includes, Array, of: %w(id description species_id position version)
    param_group :basic
    def species_lengths
      @items = helper_find_species_lengths params
    end

    private

    def helper_time_of_days(params)
      default_includes = %w(id time_of_day_option updated_at)
      basic_data_helper(params, default_includes, TimeOfDayOption)
    end

    def helper_fish_conditions(params)
      default_includes = %w(id fish_condition_option updated_at)
      basic_data_helper(params, default_includes, FishConditionOption)
    end

    def helper_dispositions(params)
      default_includes = %w(id recapture_disposition updated_at)
      basic_data_helper(params, default_includes, RecaptureDisposition)
    end

    def helper_shirt_sizes(params)
      default_includes = %w(id shirt_size_option updated_at)
      basic_data_helper(params, default_includes, ShirtSizeOption)
    end

    def helper_find_species_lengths(params)
      default_includes = %w(id description species_id position updated_at)
      basic_data_helper(params, default_includes, SpeciesLength)
    end

    def helper_find_species(params)
      params = preprocess_params(params)
      default_includes = %w(id common_name position proper_name other_name family habitat description size food_value target publish updated_at photo_file_name photo_content_type photo_file_size photo_updated_at)

      query = Species

      if params[:only_id_list]
        only_id_list_response(query, Species)

      else
        query = query.where(id: params[:id]) if params[:id] && params[:get_id_list].nil?
        query = query.where(id: params[:get_id_list]) if params[:get_id_list] && params[:id].nil?
        query = query.target if params[:target]
        query = query.where('date_trunc(\'second\', updated_at) > ?', params[:updated_after]) if params[:updated_after]
        query = query.where('common_name ILIKE ? OR proper_name ILIKE ? OR other_name ILIKE ?', "%#{params[:term]}%", "%#{params[:term]}%", "%#{params[:term]}%") if params[:term]

        query = query.reorder(updated_at: :asc).limit(params[:limit]).offset(params[:offset] || 0)

        {
            errors: [],
            id_hash: Species.ids_as_md5,
            version: query.max_by(&:updated_at).try(:updated_at) || params[:updated_after],
            count: query.count,
            items: query.select(params[:includes] || default_includes)
        }
      end
    end

  end
end
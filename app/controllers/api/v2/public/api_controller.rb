module Api::V2::Public
  class ApiController < Api::V2::ApiController

    after_action :set_headers

    def basic_data_helper(params, includes, klass)
      params = preprocess_params(params)

      if params[:only_id_list]
        only_id_list_response(klass.all,klass)

      else
        query = klass
        query = query.where(id: params[:id]) if params[:id]
        query = query.where('date_trunc(\'second\', updated_at) > ?', params[:updated_after]) if params[:updated_after]
        query = query.reorder(updated_at: :asc).limit(params[:limit]).offset(params[:offset] || 0)

        {
            errors: [],
            id_hash: klass.ids_as_md5,
            version: query.max_by(&:updated_at).try(:updated_at) || params[:updated_after],
            count: query.count,
            items: query.select(params[:includes] || includes)
        }
      end

    end

    def preprocess_params(params)
      unless params.nil?
        params[:limit] = (params[:limit] > 200 ? 200 : params[:limit]) if params[:limit]
        params[:offset] = (params[:offset] < 0 ? 0 : params[:offset]) if params[:offset]
      end

      params
    end

    protected

    def authorize_app!
      # check the headers for the token first, then check the params
      app_token = request.headers['Api-Access-Token'] || params[:app_token]
      # if the suplpied hash in not in the application oauth hashes, then we need to raise a 401 error
      raise AccessDenied if APPLICATION_HASHES.exclude?(app_token)
    end

    private

    def set_headers

      version = JSON.parse(response.body)["version"]
      hash    = JSON.parse(response.body)["hash"]
      id_hash = JSON.parse(response.body)["id_hash"]

      cache = Hash.new

      if hash
        cache[:etag] = hash
      elsif id_hash
        cache[:etag] = id_hash
      elsif version
        cache[:etag] = version
      end

      cache[:date] = version ? Date.parse(version) : Time.now

      if cache[:etag]
        expires_in 5.minutes, public: true
        fresh_when etag: cache[:etag], last_modified: cache[:date]
      end

    end

  end
end
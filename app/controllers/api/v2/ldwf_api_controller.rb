module Api::V2
  class LdwfApiController < ApiController

    # doorkeeper_for :all
    # before_action :check_for_role

    resource_description do
      name 'LDWF API'
      short 'LDWF External API'
      api_base_url "/api/v2/ldwf"
      formats %w(json jsonp)
      error code: 401, desc: 'Authorization is required or user does not have the proper access role'
      error code: 422, desc: 'Missing or malformatted parameters'
      error code: 500, desc: 'Server had an unexpected issue processing your request'
      desc <<-EOS
        == Long description
        This API provides routes and data for the use in 3rd party
        LDWF applications.
        <tt>https://narwhal.taglouisiana.com</tt>

        == Authentication Required
        To get access, you will need an OAuth2 token.
        <tt>POST /oauth/token?username=example@example.com&password=xxxxxx</tt>.
        Include the token in the standard OAuth2 way, using the "Authorization"
        header with value of "Bearer xxxxxxxxxxxx", where "xxxxxxxxxxxx" is
        your token.
      EOS
    end

    api :GET, '/captures'
    description <<-EOS
      == Long description
      Get a list of captures that are in the given set of parameters
      your token.

      Location filters can be used together
    EOS
    example <<-EOS
      {
          "all_records": 205114,
          "count": 1,
          "limit": 1,
          "offset": 0,
          "captures": [
              {
                  "species_id": 2,
                  "capture_date": "2015-10-05T05:00:00.000Z",
                  "time_of_day_option_id": null,
                  "length": null,
                  "length_range": "12\" - 15\"",
                  "fish_condition_option_id": null,
                  "latitude": 29.7100020542008,
                  "longitude": -90.2362060546875,
                  "species": {
                      "id": 2,
                      "name": "Speckled Trout"
                  }
              },
              ...
          ]
      }
    EOS
    param :date, Hash, desc: 'Date filter params' do
      param :min, String, desc: 'Minimum date (can be null to let the min be unbounded)'
      param :max, String, desc: 'Maximum date (can be null to let the max be unbounded)'
      param :ranges, Array, desc: 'Sets of date ranges to filter by. Can be disjoint ranges or overlapping' do
        param :min, String, desc: 'Minimum date (can be null to let the min be unbounded)', required: true
        param :max, String, desc: 'Maximum date (can be null to let the max be unbounded)', required: true
      end
    end
    param :location, Hash, desc: 'Location filter params' do
      param :basins, Array, in: -> { Basin.pluck(:gid) }, desc: 'The set of basin ids to search within'
      param :sub_basins, Array, in: -> { SubBasin.pluck(:gid) }, desc: 'The set of sub-basin ids to search within'
      param :regions, Array, desc: 'Set of location regions to filter by. Can either be bounding boxes or radi around a center point, can mix and match range types' do
        param :latitude, /(\-)?\d+(\.\d{1,})?/, desc: 'The latitude for the center of the region. String with a decimal number. i.e. "20.432442"', required: true
        param :longitude, /(\-)?\d+(\.\d{1,})?/, desc: 'The longitude for the center of the region. String with a decimal number. i.e. "-20.432442"', required: true
        param :shape, %w(square circle), desc: 'The shape around the center', required: true
        param :range, Integer, desc: 'The range around the center latitude and longitude to include, in meters', required: true
      end
    end
    param :species, Array, in: -> { Species.target.pluck(:id) }, desc: 'Species ids'
    param :times, Array, in: -> { TimeOfDayOption.pluck(:id) }, desc: 'Time ids'
    param :limit, Integer, desc: 'The limit number of records to return, must be <= 200'
    param :offset, Integer, desc: 'The offset of the records to fetch, must be >= 0'
    param :include, Array, in: -> { %w(species time_of_day basin sub_basin) }, desc: 'Include the relations values in the capture hash objects, themselves.'
    def captures
      query = PresentableCapture
      # include the related items, so we can add them to the json result
      # don't include captures in the future
      query = query.includes(:species, :time_of_day_option, :basin, :sub_basin).where('capture_date < ?', Time.now)

      # ------------------------------------
      # process the date params
      if params[:date]

        date_wheres = ['capture_date <@ tstzrange(?,?, \'[]\')']
        date_binds = [params[:date].try(:fetch, :min), params[:date].try(:fetch, :max)]

        params[:date].try(:fetch, :ranges).try(:each) do |range|
          date_wheres += ['capture_date <@ tstzrange(?,?, \'[]\')']
          date_binds += [range.try(:fetch, :min), range.try(:fetch, :max)]
        end

        # add the new where to the query builder
        query = query.where([date_wheres.join(' OR ')] + date_binds)
      end

      # ------------------------------------
      # process the location params
      if location = params[:location]
        region_wheres = []
        region_binds = []

        if basin = location.try(:[], :basins)
          region_wheres += ['basin_id IN (?)']
          region_binds += [basin]
        end

        if subbasin = location.try(:[], :subbasins)
          region_wheres += ['sub_basin_id IN (?)']
          region_binds += [subbasin]
        end

        # if there are any regions provided, then add the region
        # filters to the query builder
        location.try(:[], :regions).try(:each) do |region|

          # check which region shape
          case region[:shape]
            # SRID of 2163 is a meter spatial system, which will allow us to us the
            # range param as is.

            # SRID of 4326 id the spatial system for latitude and longitude values.

            when 'square'
              # create a bounding box and check which items intersect(contained in the box)
              region_wheres += ['ST_Intersects(ST_Transform(presentable_captures.geom,2163), ST_Expand(ST_Transform(ST_SetSRID(ST_MakePoint(?, ?),4326),2163), ?))']
              region_binds += [region[:longitude], region[:latitude], region[:range]]
            when 'circle'
              # check that the items geom is within a circular region surround the given center point
              region_wheres += ['ST_DWithin(ST_Transform(presentable_captures.geom,2163), ST_Transform(ST_SetSRID(ST_MakePoint(?, ?),4326),2163), ?)']
              region_binds += [region[:longitude], region[:latitude], region[:range]]
          end
        end

        # add the new where to the query builder
        query = query.where([region_wheres.join(' OR ')] + region_binds)
      end

      # set the species where if provided
      query = query.where(species_id: params[:species]) if params[:species]
      # set the times where if provided
      query = query.where(time_of_day_option_id: params[:times]) if params[:times]

      # set the limit, and if it exceeds the max then reduce it to the max
      limit = params[:limit] ? params[:limit].to_i : 200
      limit = 200 if limit > 200

      # set the offset, ensure it is greater the 0
      offset = params[:offset] ? params[:offset].to_i : 0
      offset = 0 if offset < 0

      @results = {
          version: query.max_by(&:updated_at).try(:updated_at),
          #return the limit
          limit: limit,
          # return the offset
          offset: offset,
          # get total records count
          count: query.count,
          # run the query with the right order, limit and offset
          items: query.order(capture_date: :desc).limit(limit).offset(offset)
      }
    end

    api :GET, '/species'
    description 'Get a list of species'
    example <<-EOS
      {
        "species": [
          {
            "id": 1,
            "name": "Redfish"
          },
          ...
        ]
      }
    EOS

    def species
      @species = Species.target
    end

    api :GET, '/species/:id'
    description 'Get a specific species based on the given id'
    param :id, Species.target.pluck(:id).map(&:to_s), desc: 'Species id', :required => true
    example <<-EOS
      {
        "id": 1,
        "name": "Redfish"
      }
    EOS

    def species_id
      @species = Species.find(params[:id])
    end

    api :GET, '/times'
    description 'Get a list of species'
    example <<-EOS
      {
         "times":[
            {
               "id":1,
               "description":"Early Morning (12:01 AM - 3:00 AM)"
            },
            ...
         ]
      }
    EOS

    def times
      @times = TimeOfDayOption.all
    end

    api :GET, '/times/:id'
    description 'Get a specific time based on the given id'
    param :id, TimeOfDayOption.pluck(:id).map(&:to_s), desc: 'Time id', :required => true
    example <<-EOS
      {
         "id": 1,
         "description": "Early Morning (12:01 AM - 3:00 AM)"
      }
    EOS

    def times_id
      @time = TimeOfDayOption.find(params[:id])
    end

    api :GET, '/basins'
    description 'Get a list of basins'
    example <<-EOS
      {
         "basins":[
            {
               "id": 1,
               "name": "Mississippi River"
            },
            ...
         ]
      }
    EOS

    def basins
      @basins = Basin.all
    end

    api :GET, '/basins/:id'
    description 'Get a specific basin based on the given id'
    param :id, Basin.pluck(:gid).map(&:to_s), desc: 'Basin id', :required => true
    example <<-EOS
      {
         "id": 1,
         "name": "Mississippi River"
      }
    EOS

    def basins_id
      @basin = Basin.find(params[:id])
    end

    api :GET, '/subbasins'
    description 'Get a list of sub-basins'
    example <<-EOS
      {
         "sub_basins":[
            {
               "id":1,
               "name":"Caddo Lake and James Bayou",
               "basin_id":4
            },
            ...
         ]
      }
    EOS

    def subbasins
      @subbasins = SubBasin.includes(:basin).where.not(basins: {gid: nil})
    end

    api :GET, '/subbasins/:id'
    description 'Get a specific sub-basin based on the given id'
    param :id, SubBasin.joins(:basin).where.not(basins: {gid: nil}).pluck(:gid).map(&:to_s), desc: 'Sub-basin id', :required => true
    example <<-EOS
      {
         "id":1,
         "name":"Caddo Lake and James Bayou",
         "basin_id":4
      }
    EOS
    def subbasins_id
      @subbasin = SubBasin.includes(:basin).where.not(id: params[:id], basins: {gid: nil}).first
    end

    private

    def check_for_role
      render json: {error: 'Not authorized'}, status: :unauthorized if @user.has_role?(:ldwf_api)
    end

  end
end
module Api::V2::Protected
  class ApiController < Api::V2::ApiController

    resource_description do
      name 'Protected API'
      short 'LDWF Protected API'
      api_base_url "/api/v2/protected"
      formats %w(json jsonp)
      error code: 401, desc: 'Authorization is required or user does not have the proper access role'
      error code: 422, desc: 'Missing or malformatted parameters'
      error code: 500, desc: 'Server had an unexpected issue processing your request'
      desc <<-EOS
        == Long description
        This API provides some public data to be used in front facing applications.
        Most of the data provided here is mainly relational lookup data.

        No authentication is required. All data at this endpoint is for public consumption.
      EOS
    end

    doorkeeper_for :all

    before_action :get_user
    before_action :check_for_valid_user_id

    after_action :set_headers

    private

    def get_user
      @user = User.find(doorkeeper_token.resource_owner_id) if doorkeeper_token
    end

    def check_for_valid_user_id

      if  @user.angler_id.nil?
        # the hash to return to the client
        data = Hash.new
        data[:count] = 0
        data[:items] = {}
        data[:messages] = []
        data[:errors] = []

        # check for that the signed in user has a
        data[:errors] << 'No Angler ID associated with this user.'

        render :json => data, :status => :internal_server_error

        false
      end
    end

    def set_headers

      version = JSON.parse(response.body)["version"]
      hash    = JSON.parse(response.body)["hash"]
      id_hash = JSON.parse(response.body)["id_hash"]

      cache = Hash.new

      if hash
        cache[:etag] = hash
      elsif id_hash
        cache[:etag] = id_hash
      elsif version
        cache[:etag] = version
      end

      cache[:date] = version ? Date.parse(version) : Time.now

      if cache[:etag]
        expires_in 20.seconds
        fresh_when etag: cache[:etag], last_modified: cache[:date]
      end

    end

  end
end
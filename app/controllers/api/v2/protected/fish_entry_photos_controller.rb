module Api::V2::Protected
  class FishEntryPhotosController < ApiController

    resource_description do
      name 'Protected Fish Entry Controller API'
      short 'LDWF Protected Mobile API'
      api_base_url "/api/v2/protected/fish_entry_photos"
      formats %w(json jsonp)
      error code: 401, desc: 'Authorization is required or user does not have the proper access role'
      error code: 422, desc: 'Missing or malformatted parameters'
      error code: 500, desc: 'Server had an unexpected issue processing your request'
      desc <<-EOS
        == Long description
        This API provides some public data to be used in front facing applications.
        Most of the data provided here is mainly relational lookup data.

        No authentication is required. All data at this endpoint is for public consumption.
      EOS
    end

    skip_after_action :cache_buster
    skip_after_action :set_headers

    api :GET, '/:id/filename'
    param :id, lambda { |val| FishEntryPhoto.where(id: val).any? }, required: true, desc: 'The id of the photo to retrieve'
    param :style, /original|thumb|medium/, required: false, desc: 'They size/style of the photo to return'
    def show
      photo = FishEntryPhoto.find(params[:id])
      if photo.fish_entry_photoable.angler_id == @user.angler_id
        style = params[:style] ? params[:style] : 'original'
        send_file(photo.image.path(style), {type: photo.image_content_type, disposition: 'inline'})
      else
        render nothing: true, status: :unauthorized
      end

    end
  end
end
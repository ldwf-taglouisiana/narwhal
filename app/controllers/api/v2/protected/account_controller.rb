module Api::V2::Protected
  class AccountController < ApiController
    include Api::V2::Protected::Concerns::QueryHelper

    resource_description do
      name 'Protected Capture API'
      short 'LDWF Protected Capture API'
      api_base_url "/api/v2/protected/account"
      formats %w(json jsonp)
      error code: 401, desc: 'Authorization is required or user does not have the proper access role'
      error code: 422, desc: 'Missing or malformatted parameters'
      error code: 500, desc: 'Server had an unexpected issue processing your request'
      desc <<-EOS
        == Long description
        This API provides some public data to be used in front facing applications.
        Most of the data provided here is mainly relational lookup data.

        No authentication is required. All data at this endpoint is for public consumption.
      EOS
    end

    api :GET, '/me'
    def me
      @items = query_me(@user)
    end

    api :POST, '/me'
    param :angler, Hash do
      param :first_name, String, required: true
      param :last_name, String, required: true
      param :street, String, required: true
      param :city, String, required: true
      param :state, Api::V2::ApiController::STATE_REGEX, required: true
      param :zip, Api::V2::ApiController::ZIP_CODE_REGEX, required: true
      param :email, Api::V2::ApiController::EMAIL_REGEX, required: true
      param :phone_number, Api::V2::ApiController::PHONE_REGEX, as: :phone_number_1, required: true
    end
    param :user, Hash do
      param :email, Api::V2::ApiController::EMAIL_REGEX, required: true
      param :old_pass, Api::V2::ApiController::PASSWORD_REGEX
      param :password, Api::V2::ApiController::PASSWORD_REGEX
      param :password_confirmation, Api::V2::ApiController::PASSWORD_REGEX
    end
    def update_me
      data = {
          satus: :ok,
          errors: [],
          messages: []
      }

      begin
        User.transaction do
          @user.update_attributes!(@api_params[:user]) if @user.valid_password?(@api_params[:user][:old_pass])
          @user.angler.update_attributes!(@api_params[:angler])
        end

      rescue Exception => e
        data[:status] = :unprocessable_entity
        data[:errors] << 'Could not update, check that your old password is correct'

      end

      render json: data, status: data[:status]
    end

    api :GET, '/dashboard'
    def dashboard
      angler_id = @user.angler_id

      items = {
          angler: {
              captures: {
                  total: Capture.where(angler_id: angler_id).count,
                  species: Capture.where(angler_id: angler_id).in_time_range(30.days.ago, Time.now).target_species_counts_h,
                  season: {
                      current: Capture.where(angler_id: angler_id).in_current_season.count,
                      previous: Capture.where(angler_id: angler_id).in_last_season.count
                  }
              },

              recaptures: {
                  total: Recapture.where(angler_id: angler_id).count,
                  species: Recapture.where(angler_id: angler_id).in_time_range(30.days.ago, Time.now).target_species_counts_h,
                  season: {
                      current: Recapture.where(angler_id: angler_id).in_current_season.count,
                      previous: Recapture.where(angler_id: angler_id).in_last_season.count
                  }
              }
          },

          program: {
              season: {
                  previous: {
                      start: {
                        epoch: Capture.last_season.first.to_i,
                        iso: Capture.last_season.first.iso8601,
                      },
                      end: {
                        epoch:  Capture.last_season.last.to_i,
                        iso:  Capture.last_season.last.iso8601
                      }
                  },
                  current: {
                      start: {
                        epoch: Capture.this_season.first.to_i,
                        iso: Capture.this_season.first.iso8601
                      },
                      end: {
                        epoch: Capture.this_season.last.to_i,
                        iso: Capture.this_season.last.iso8601
                      }
                  },
              },

              captures: {
                  total: Capture.count,
                  species: Capture.in_time_range(30.days.ago, Time.now).target_species_counts_h,
                  season: {
                      current: Capture.in_current_season.count,
                      previous: Capture.in_last_season.count
                  }
              },

              recaptures: {
                  total: Recapture.count,
                  species: Recapture.in_time_range(30.days.ago, Time.now).target_species_counts_h,
                  season: {
                      current: Recapture.in_current_season.count,
                      previous: Recapture.in_last_season.count
                  }
              }
          }
      }

      # get a MD5 sum of the result, so we can fingerprint it with the current state of the dashboard
      md5 = Digest::MD5.new
      md5.update items.to_json

      items[:hash] = md5.hexdigest

      render json: items
    end
  end
end
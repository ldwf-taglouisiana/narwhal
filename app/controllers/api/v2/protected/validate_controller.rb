module Api::V2::Protected
  class ValidateController < ApiController

    resource_description do
      name 'Protected Validate API'
      short 'LDWF Protected Validate API'
      api_base_url "/api/v2/protected/validate"
      formats %w(json jsonp)
      error code: 401, desc: 'Authorization is required or user does not have the proper access role'
      error code: 422, desc: 'Missing or malformatted parameters'
      error code: 500, desc: 'Server had an unexpected issue processing your request'
      desc <<-EOS
        == Long description
        This API provides some public data to be used in front facing applications.
        Most of the data provided here is mainly relational lookup data.

        No authentication is required. All data at this endpoint is for public consumption.
      EOS
    end

    api :GET, '/tag_exists'
    def tag_exists
      tag = Tag.where(:tag_no => params[:fieldValue]).first

      answer = []
      answer << params[:fieldId]

      # if the field is empty
      if params[:fieldValue].empty?
        answer << true

        # no tag exists
      elsif tag.nil?
        answer << false
        answer << 'Tag does not exist'

        # the tag hasn't been assigned to anyone
      elsif !tag.angler and (params[:secret_recap] == 'false' or params[:secret_recap] == '')
        answer << false
        answer << 'Tag is not assigned to an angler'

        # The tag has been marked as lost or broken (deactivated)
      elsif !tag.active
        answer << false
        answer << 'This tag has been deactivated'

        # the tag hasn't been assigned to this angler
      elsif tag.angler_id != @user.angler_id and (params[:secret_recap] == 'false' or params[:secret_recap] == '')
        answer << false
        answer << 'Tag is not available for use'

        # a previous capture exists
      elsif Capture.where("tag_id = ?", tag.id).to_a.length > 0 and (params[:secret_recap] == 'false' or params[:secret_recap] == '')
        answer << false
        answer << 'A capture already exists for this tag'

        # default to valid
      else
        answer << true

      end

      render :json => answer
    end
  end
end
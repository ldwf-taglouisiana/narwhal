module Api::V2::Protected
  class VolunteerTimeLogsController < ApiController
    include Api::V2::Protected::Concerns::QueryHelper

    resource_description do
      name 'Protected Volunteer Time Logs API'
      short 'LDWF Protected Volunteer Time Logs API'
      api_base_url "/api/v2/protected/volunteer_time_logs"
      formats %w(json jsonp)
      error code: 401, desc: 'Authorization is required or user does not have the proper access role'
      error code: 422, desc: 'Missing or malformatted parameters'
      error code: 500, desc: 'Server had an unexpected issue processing your request'
      desc <<-EOS
        == Long description
        This API provides some public data to be used in front facing applications.
        Most of the data provided here is mainly relational lookup data.

        No authentication is required. All data at this endpoint is for public consumption.
      EOS
    end

    api :GET, '/'
    param :id, Integer
    param :only_id_list, :bool, desc: 'Send the id list back to the client'
    param :updated_after, Time
    param :limit, :number
    param :offset, :number
    def index
      @items = query_volunteer_time_logs(params, @user.angler_id)
    end

    api :POST, '/'
    param :items, Array, required: true do
      param :start_date, Time, required: true
      param :end_date, Time, required: true
      param :verified, :bool, required: true
      param :uuid, Api::V2::ApiController::UUID_REGEX, required: true
    end
    def create
      data = insert_volunteer_time_logs(params, @user.angler_id)
      render json: data, status: data[:status]
    end

  end
end
module Api::V2::Protected
  class CombinedDataController < ApiController
    include Api::V2::Protected::Concerns::QueryHelper
    include Api::V2::Protected::Concerns::InsertHelper

    skip_before_action :set_headers

    resource_description do
      name 'Protected Combined Data API'
      short 'LDWF Protected Combined Data API'
      api_base_url "/api/v2/protected/combined_data"
      formats %w(json jsonp)
      error code: 401, desc: 'Authorization is required or user does not have the proper access role'
      error code: 422, desc: 'Missing or malformatted parameters'
      error code: 500, desc: 'Server had an unexpected issue processing your request'
      desc <<-EOS
        == Long description
        This API provides some public data to be used in front facing applications.
        Most of the data provided here is mainly relational lookup data.

      EOS
    end

    api :GET, '/'
    param :tags, Hash do
      param :id, lambda { |val| Tag.where(id: val).any? }
      param :only_id_list, :bool, desc: 'Send the id list back to the client'
      param :updated_after, Time
      param :limit, :number
      param :offset, :number
    end
    param :captures, Hash do
      param :id, lambda { |val| PresentableCapture.where(id: val).any? }, desc: 'The id the requested capture'
      param :only_id_list, :bool, desc: 'Send the id list back to the client'
      param :species_id, lambda { |val| Species.where(id: val).any? }, desc: 'Find catures with a specific species id'
      param :date, Hash do
        param :min, Time
        param :max, Time
      end
      param :updated_after, Time
      param :limit, :number
      param :offset, :number
    end
    param :draft_captures, Hash do
      param :id, lambda { |val| DraftCapture.where(id: val).any? }, desc: 'The draft capture with the given id'
      param :only_id_list, :bool, desc: 'Send the id list back to the client'
      param :species_id, lambda { |val| Species.where(id: val) }, desc: 'Draft captures that match the given species id'
      param :date, Hash do
        param :min, Time
        param :max, Time
      end
      param :updated_after, Time
      param :limit, :number
      param :offset, :number
    end
    param :time_logs, Hash do
      param :id, lambda { |val| VolunteerTimeLog.where(id: val).any? }
      param :only_id_list, :bool, desc: 'Send the id list back to the client'
      param :updated_after, Time
      param :limit, :number
      param :offset, :number
    end
    param :tag_requests, Hash do
      param :id, lambda { |val| UserTagRequest.where(id: val).any? }
      param :only_id_list, :bool, desc: 'Send the id list back to the client'
      param :updated_after, Time
      param :limit, :number
      param :offset, :number
    end
    def index
      updated_params = {
          tags: {},
          captures: {},
          draft_captures: {},
          time_logs: {},
          tag_requests: {}
      }

      updated_params = updated_params.deep_merge(@api_params.deep_symbolize_keys) unless @api_params.nil?

      # save the queries locally so we can pull out the version information
      tags = query_tags(updated_params[:tags], @user.angler_id)
      captures = query_captures(updated_params[:captures], @user.angler_id)
      draft_captures = query_draft_captures(updated_params[:draft_captures], @user.angler_id)
      time_logs = query_volunteer_time_logs(updated_params[:time_logs], @user.angler_id)
      tag_requests = query_tag_requests(updated_params[:tag_requests], @user.angler_id)
      me = query_me(@user)

      version = [
          tags[:version],
          captures[:version],
          draft_captures[:version],
          time_logs[:version],
          tag_requests[:version],
          me[:version]
      ].compact.max

      hashes = [
          tags[:id_hash],
          captures[:id_hash],
          draft_captures[:id_hash],
          time_logs[:id_hash],
          tag_requests[:id_hash],
          me[:id_hash]
      ].compact.join

      @items = {
          version: version,
          hash: Digest::SHA1.hexdigest(version.iso8601 + hashes),
          tags: tags,
          captures: captures,
          draft_captures: draft_captures,
          time_logs: time_logs,
          tag_requests: tag_requests,
          me: me
      }
    end

    api :POST, '/'
    param :tag_requests, Hash do
      param :items, Array, required: true do
        param :tag_type, ['Tuna Tags', 'Redfish/Trout Tags'], required: true
        param :number_of_tags, :number, required: true
        param :uuid, Api::V2::ApiController::UUID_REGEX, required: true
      end
    end
    param :draft_captures, Hash do
      param :items, Array, required: true do
        param :id, lambda { |val| val.to_i == -1 || DraftCapture.where(id: val).any? }, desc: 'The draft capture with the given id, if -1 then this is a new draft capture'
        param :tag_number, Api::V2::ApiController::TAG_REGEX, required: true
        param :capture_date, Time, required: true
        param :species_id, lambda { |val| Species.where(id: val).any? },  required: true
        param :length, Api::V2::ApiController::LENGTH_LAMBDA, desc: 'Length must be greater than 0'
        param :location_description, String
        param :latitude, Api::V2::ApiController::LATITUDE_LAMBDA, required: true, desc: 'Latitude must be between -120 and -50'
        param :longitude, Api::V2::ApiController::LONGITUDE_LAMBDA, required: true, desc: 'Longitude must be between 20 and 50'
        param :fish_condition_id, lambda { |val| FishConditionOption.where(id: val).any? }
        param :weight, Api::V2::ApiController::WEIGHT_LAMBDA, desc: 'Weight must be greater than 0'
        param :comments, String
        param :recapture, :bool, required: true
        param :recapture_disposition_id, lambda { |val| RecaptureDisposition.where(id: val).any? }
        param :time_of_day_id, lambda { |val| TimeOfDayOption.where(id: val).any? }, required: true
        param :entered_gps_type, lambda { |val| GPSFormat.where(format_type: val).any? }, required: true
        param :should_save, :bool, required: true
        param :species_length_id, lambda { |val| SpeciesLength.where(id: val).any? }
        param :uuid, Api::V2::ApiController::UUID_REGEX, required: true
        param :images, Array, allow_nil: true do
          param :raw_data, String, required: true
        end
      end
    end
    param :time_logs, Hash do
      param :items, Array, required: true do
        param :start_date, Time, required: true
        param :end_date, Time, required: true
        param :verified, :bool, required: true
        param :uuid, Api::V2::ApiController::UUID_REGEX, required: true
      end
    end
    def create
      data = Hash.new.tap do |data_hash|
        data_hash[:tag_requests]    = insert_tag_requests(@api_params[:tag_requests], @user.angler_id) if @api_params[:tag_requests]
        data_hash[:draft_captures]  = insert_draft_captures(@api_params[:draft_captures], @user.angler_id) if @api_params[:draft_captures]
        data_hash[:time_logs]       = insert_volunteer_time_logs(@api_params[:time_logs], @user.angler_id) if @api_params[:time_logs]
      end

      render json: data
    end

  end
end
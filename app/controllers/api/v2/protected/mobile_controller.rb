module Api::V2::Protected
  class MobileController < ApiController

    IOS_APNS_KEY_REGEX = /\A<[a-z0-9]{8}(\s[a-z0-9]{8}){7}>\z/

    resource_description do
      name 'Protected Mobile API'
      short 'LDWF Protected Mobile API'
      api_base_url "/api/v2/protected/mobile"
      formats %w(json jsonp)
      error code: 401, desc: 'Authorization is required or user does not have the proper access role'
      error code: 422, desc: 'Missing or malformatted parameters'
      error code: 500, desc: 'Server had an unexpected issue processing your request'
      desc <<-EOS
        == Long description
        This API provides some public data to be used in front facing applications.
        Most of the data provided here is mainly relational lookup data.

        No authentication is required. All data at this endpoint is for public consumption.
      EOS
    end

    api :GET, '/notifications/registrations/ios'
    param :device_token, IOS_APNS_KEY_REGEX, required: true, desc: 'The iOS device push notification forma. Must be in form "<xxxxxxxx xxxxxxxx xxxxxxxx xxxxxxxx xxxxxxxx xxxxxxxx xxxxxxxx xxxxxxxx>"'
    def ios_device_registration
      device = UserDevice.new(
          {
              device_token: params[:device_token],
              user: @user,
              user_device_types_id: UserDeviceType.ios_type.id
          }
      )

      if device.save
        render json: {message: 'success'}, status: :ok
      else
        render json: {message: 'error'}, status: :internal_server_error
      end

    end
  end
end
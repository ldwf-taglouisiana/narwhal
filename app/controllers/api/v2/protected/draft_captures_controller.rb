module Api::V2::Protected
  class DraftCapturesController < ApiController
    include Api::V2::Protected::Concerns::QueryHelper
    include Api::V2::Protected::Concerns::InsertHelper

    resource_description do
      name 'Protected Draft Capture API'
      short 'LDWF Protected Draft Capture API'
      api_base_url "/api/v2/protected/draft_captures"
      formats %w(json jsonp)
      error code: 401, desc: 'Authorization is required or user does not have the proper access role'
      error code: 422, desc: 'Missing or malformatted parameters'
      error code: 500, desc: 'Server had an unexpected issue processing your request'
      desc <<-EOS
        == Long description
        This API provides some public data to be used in front facing applications.
        Most of the data provided here is mainly relational lookup data.

        No authentication is required. All data at this endpoint is for public consumption.
      EOS
    end

    api :GET, '/'
    param :id, lambda { |val| DraftCapture.where(id: val).any? }, desc: 'The draft capture with the given id'
    param :only_id_list, :bool, desc: 'Send the id list back to the client'
    param :species_id, lambda { |val| Species.where(id: val) }, desc: 'Draft captures that match the given species id'
    param :date, Hash do
      param :min, Time
      param :max, Time
    end
    param :updated_after, Time
    param :limit, :number
    param :offset, :number
    def index
      @items = query_draft_captures(params, @user.angler_id)
    end

    api :GET, '/status'
    param :id, lambda { |val| DraftCapture.where(id: val).any? }, desc: 'The status for a draft capture with the given id'
    def status
      render json: {
                 status: DraftCapture.find(params[:id]).try(:status)
             }
    end

    api :POST, '/'
    param :items, Array, required: true do
      param :id, lambda { |val| val == -1 || DraftCapture.where(id: val).any? }, desc: 'The draft capture with the given id, if -1 then this is a new draft capture'
      param :tag_number, Api::V2::ApiController::TAG_REGEX, required: true
      param :capture_date, Time, required: true
      param :species_id, lambda { |val| Species.where(id: val).any? },  required: true
      param :length, Api::V2::ApiController::LENGTH_LAMBDA, desc: 'Length must be greater than 0'
      param :location_description, String
      param :latitude, Api::V2::ApiController::LATITUDE_LAMBDA, required: true, desc: 'Latitude must be between -120 and -50'
      param :longitude, Api::V2::ApiController::LONGITUDE_LAMBDA, required: true, desc: 'Longitude must be between 20 and 50'
      param :fish_condition_id, lambda { |val| FishConditionOption.where(id: val).any? }, required: true
      param :weight, Api::V2::ApiController::WEIGHT_LAMBDA, desc: 'Weight must be greater than 0'
      param :comments, String
      param :recapture, :bool, required: true
      param :recapture_disposition_id, lambda { |val| RecaptureDisposition..where(id: val).any? }
      param :time_of_day_id, lambda { |val| TimeOfDayOption.where(id: val).any? }, required: true
      param :entered_gps_type, lambda { |val| GPSFormat.where(format_type: val).any? }, required: true
      param :should_save, :bool, required: true
      param :species_length_id, lambda { |val| SpeciesLength.where(id: val).any? }
      param :uuid, Api::V2::ApiController::UUID_REGEX, required: true
      param :images, Array, allow_nil: true do
        param :raw_data, String, required: true
      end
    end
    def create
      data = insert_draft_captures(params, @user.angler_id)
      render json: data, status: data[:status]
    end

    api :GET, '/toggle_should_save'
    param :id, lambda { |val| DraftCapture.where(id: val).any? }, desc: 'The id of a draft capture'
    def toggle_should_save
      data = {
          status: :ok,
          uuids: [],
          errors: []
      }

      begin
        DraftCapture.transaction do
          draft_capture = DraftCapture.where(id: params[:id], angler_id: @user.angler_id).first
          draft_capture.should_save = !draft_capture.should_save
          draft_capture.saved_at = Time.now
          draft_capture.save!
        end

      rescue Exception => exp
        logger.fatal 'Error Occurred'
        logger.fatal exp

        data[:errors] << {
            message: 'Could not create volunteer time log',
            uuid: item[:uuid],
            status: :unprocessable_entity,
            exception: exp.message
        }

        data[:status] = :unprocessable_entity

      end

      render json: data, status: data[:status]
    end
  end
end
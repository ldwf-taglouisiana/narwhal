module Api::V2::Protected
  class TagsController < ApiController
    include Api::V2::Protected::Concerns::QueryHelper
    include Api::V2::Protected::Concerns::InsertHelper

    resource_description do
      name 'Protected Tag API'
      short 'LDWF Protected Tag API'
      api_base_url "/api/v2/protected/tags"
      formats %w(json jsonp)
      error code: 401, desc: 'Authorization is required or user does not have the proper access role'
      error code: 422, desc: 'Missing or malformatted parameters'
      error code: 500, desc: 'Server had an unexpected issue processing your request'
      desc <<-EOS
        == Long description
        This API provides some public data to be used in front facing applications.
        Most of the data provided here is mainly relational lookup data.

        No authentication is required. All data at this endpoint is for public consumption.
      EOS
    end

    api :GET, '/'
    param :id, lambda { |val| Tag.where(id: val).any? }
    param :only_id_list, :bool, desc: 'Send the id list back to the client'
    param :updated_after, Time
    param :limit, :number
    param :offset, :number
    def index
      @items = query_tags(params, @user.angler_id)
    end

    api :GET, '/request'
    param :id, lambda { |val| UserTagRequest.where(id: val).any? }
    param :only_id_list, :bool, desc: 'Send the id list back to the client'
    param :updated_after, Time
    param :limit, :number
    param :offset, :number
    def get_requests
      @items = query_tag_requests(params, @user.angler_id)
    end

    api :POST, '/request'
    param :items, Array, required: true do
      param :tag_type, ['Redfish/Trout Tags', 'Inshore Tags'], required: true
      param :number_of_tags, :number, required: true
      param :uuid, Api::V2::ApiController::UUID_REGEX, required: true
    end
    def request
      #requests are only valid if the user is verified
      if @user.angler.try(:is_app_angler?) && !@user.verified
        render :json => { :errors => ['You are not yet verified please allow a few days for verification before requesting additional tags'],
                          :status => :unprocessable_entity } and return
      end
      data = insert_tag_requests(params, @user.angler_id)
      render json: data, status: data[:status]
    end
  end
end
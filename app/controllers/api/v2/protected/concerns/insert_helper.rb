module Api::V2::Protected::Concerns::InsertHelper
  extend ActiveSupport::Concern

  OK                   = Rack::Utils::SYMBOL_TO_STATUS_CODE[:ok]
  UNPROCESSABLE_ENTITY = Rack::Utils::SYMBOL_TO_STATUS_CODE[:unprocessable_entity]

  def insert_draft_captures(controller_params, angler_id)
    data = {
        status: OK,
        uuids: [],
        errors: []
    }

    controller_params[:items].try(:each) do |item|
      begin
        DraftCapture.transaction do

          if item[:id].to_i == -1
            images = item[:images]

            draft = DraftCapture.create! item.except(:id, :should_delete, :images).merge({angler_id: angler_id})

            if images.present?
              images.each do |image|
                photo = FishEntryPhoto.new(fish_entry_photoable: draft)
                photo.image = StringIO.new(Base64.decode64(image[:raw_data]))
                photo.save!
              end
            end
          else
            draft = DraftCapture.where(id: item[:id], angler_id: @user.angler_id).first
            item[:should_delete] ? draft.destroy! : draft.update_attributes!(item.except(:id, :uuid, :should_delete))
          end

          data[:uuids] << item[:uuid]
        end

      rescue Exception => exp
        logger.fatal 'Error Occurred'
        logger.fatal exp

        data[:errors] << {
            message: 'Could not create draft capture',
            uuid: item[:uuid],
            status: UNPROCESSABLE_ENTITY,
            exception: exp.message
        }
      end
    end

    data[:status] = UNPROCESSABLE_ENTITY if data[:uuids].empty?

    data
  end

  def insert_tag_requests(controller_params, angler_id)
    data = {
        errors: [],
        status: OK,
        uuids: []
    }

    controller_params[:items].each do |item|
      begin

        ActiveRecord::Base.transaction do
          entity = UserTagRequest.create!(tag_type: item[:tag_type], number_of_tags: item[:number_of_tags], angler_id: angler_id, uuid: item[:uuid])
          data[:uuids] << item[:uuid] unless item[:uuid].blank?
          Notification.build_user_tag_request(entity)
        end

      rescue Exception => exp
        logger.fatal 'Error Occurred'
        logger.fatal exp

        data[:errors] << {
            message: 'Could not create tag request',
            uuid: item[:uuid],
            status: UNPROCESSABLE_ENTITY,
            exception: exp.message
        }
      end
    end

    data[:status] = UNPROCESSABLE_ENTITY if data[:uuids].empty?

    data
  end

  def insert_volunteer_time_logs(controller_params, angler_id)
    data = {
        status: OK,
        errors: [],
        uuids: []
    }

    controller_params[:items].each do |item|
      begin
        ActiveRecord::Base.transaction do
          VolunteerTimeLog.create!(start: item[:start_date], end: item[:end_date], verified: item[:verified], angler_id: angler_id)
          data[:uuids] << item[:uuid]
        end

      rescue Exception => exp
        logger.fatal 'Error Occurred'
        logger.fatal exp

        data[:errors] << {
            message: 'Could not create volunteer time log',
            uuid: item[:uuid],
            status: UNPROCESSABLE_ENTITY,
            exception: exp.message
        }

      end
    end

    data[:status] = UNPROCESSABLE_ENTITY if data[:uuids].empty?

    data
  end
end

module Api::V2::Protected::Concerns::QueryHelper
  extend ActiveSupport::Concern

  def query_captures(controller_params, angler_id)
    query_fish_event_helper(controller_params, angler_id, PresentableCapture)
  end

  def query_draft_captures(controller_params, angler_id)
    selects = DraftCapture.attribute_names + ['format_latitude(geom, gps_formats.format_string) as "latitude_string", format_longitude(geom, gps_formats.format_string) as "longitude_string"']
    query = DraftCapture.includes(:species, :gps_format).joins(:gps_format).select(selects)

    query_fish_event_helper(controller_params, angler_id, DraftCapture, query)
  end

  def query_tags(controller_params, angler_id)
    query_helper(controller_params, angler_id, PresentableTag, PresentableTag.where(active: true))
  end

  def query_tag_requests(controller_params, angler_id)
    query_helper(controller_params, angler_id, UserTagRequest)
  end

  def query_volunteer_time_logs(controller_params, angler_id)
    query_helper(controller_params, angler_id, VolunteerTimeLog)
  end

  def query_me(user)
    {
        version: [user.updated_at, user.angler.updated_at].compact.max,
        id_hash: Digest::MD5.hexdigest([user.id,user.angler.id].join),
        user: user,
        angler: user.angler
    }
  end

  def preprocess_params(params)
    unless params.nil?
      params[:limit] = (params[:limit] > 200 ? 200 : params[:limit]) if params[:limit]
      params[:offset] = (params[:offset] < 0 ? 0 : params[:offset]) if params[:offset]
    end

    params
  end

  private

  def query_fish_event_helper(controller_params, angler_id, klass, query = nil)
    params = preprocess_params(controller_params)
    query = query || klass.where(angler_id: angler_id)

    unless params[:only_id_list]
      if (date = params[:date])
        query = query.where('capture_date <@ tstzrange(?,?, \'[]\')', date[:min], date[:min])
      end

      query = query.where(species_id: params[:species_id]) if params[:species_id]
    end

    query_helper(params, angler_id, klass, query)
  end

  def query_helper(controller_params, angler_id, klass, query = nil)
    params = preprocess_params(controller_params)
    query = query || klass
    query = query.where(angler_id: angler_id)

    if params[:only_id_list]
      only_id_list_response(query,klass)

    else
      query = query.where(id: params[:id]) if params[:id]

      if klass.attribute_names.include?("used_at") && params[:updated_after]
        query = query.where("date_trunc('second', #{klass.table_name}.updated_at) > ? OR date_trunc('second', #{klass.table_name}.used_at) > ?", params[:updated_after], params[:updated_after])
      elsif params[:updated_after]
        query = query.where("date_trunc('second', #{klass.table_name}.updated_at) > ?", params[:updated_after])
      end

      query = query.reorder(updated_at: :asc).limit(params[:limit]).offset(params[:offset] || 0)

      {
          errors: [],
          id_hash: klass.ids_as_md5(klass.where(angler_id: angler_id)),
          version: query.max_by(&:updated_at).try(:updated_at),
          count: query.except(:select).count,
          items: query
      }
    end
  end

end

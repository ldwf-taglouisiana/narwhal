module Api::V2::Protected
  class CapturesController < ApiController
    include Api::V2::Protected::Concerns::QueryHelper

    resource_description do
      name 'Protected Capture API'
      short 'LDWF Protected Capture API'
      api_base_url "/api/v2/protected/captures"
      formats %w(json jsonp)
      error code: 401, desc: 'Authorization is required or user does not have the proper access role'
      error code: 422, desc: 'Missing or malformatted parameters'
      error code: 500, desc: 'Server had an unexpected issue processing your request'
      desc <<-EOS
        == Long description
        This API provides some public data to be used in front facing applications.
        Most of the data provided here is mainly relational lookup data.

        No authentication is required. All data at this endpoint is for public consumption.
      EOS
    end

    api :GET, '/'
    param :id, lambda { |val| PresentableCapture.where(id: val).any? || val == -1 }, desc: 'The id the requested capture'
    param :only_id_list, :bool, desc: 'Send the id list back to the client'
    param :species_id, lambda { |val| Species.where(id: val).any? }, desc: 'Find catures with a specific species id'
    param :date, Hash do
      param :min, Time
      param :max, Time
    end
    param :updated_after, Time
    param :limit, :number
    param :offset, :number
    def index
      @items = query_captures(params, @user.angler_id)
    end

    api :GET, '/:id/history'
    param :id, lambda { |val| PresentableCapture.where(id: val).any? }, desc: 'The id the requested capture'
    def history
      capture_item = PresentableCapture.where(id: params[:id], angler_id: @user.angler_id).first

      # if the capture is nil, then the user did not have access to it.
      if capture_item.nil?
        render json: {error: "Authenticated user does not have access to capture: #{params[:id]}"}, status: 401
        return
      end

      table_name = PresentableRecapture.table_name
      distance_select = <<-SQL
        (
          SELECT
            ST_Distance(ST_Transform(A.geom, 2163), ST_Transform(#{table_name}.geom,2163))
            FROM captures A
            WHERE A.id = #{table_name}.capture_id
        ) AS distance_traveled_meters
      SQL

      @item = {
          capture: capture_item,
          recaptures: PresentableRecapture
                          .where(capture_id: capture_item.id, verified: true)
                          .order(recapture_number: :asc)
                          .select([
                                      "#{table_name}.*",
                                      distance_select
                          ])

      }
    end
  end
end
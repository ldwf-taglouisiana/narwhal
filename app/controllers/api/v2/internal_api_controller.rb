module Api::V2
  class InternalApiController < ApiController

    resource_description do
      name 'Inernal API'
      short 'LDWF Internal API'
      api_base_url "/api/v2/internal"
      formats %w(json jsonp)
      error code: 401, desc: 'Authorization is required or user does not have the proper access role'
      error code: 422, desc: 'Missing or malformatted parameters'
      error code: 500, desc: 'Server had an unexpected issue processing your request'
      desc <<-EOS
        == Long description
        This API provides some public data to be used in front facing applications.
        Most of the data provided here is mainly relational lookup data.

        No authentication is required. All data at this endpoint is for public consumption.
      EOS
    end

    before_action :authenticate_user!

    respond_to :json

    def angler_from_name
      #pure sql here needs scrubbing with name like Oi[ldif it crashes
      anglers = Angler.where('first_name ~* ? AND last_name ~* ?', params[:first_name], params[:last_name])

      render json: { anglers: anglers }
    end

    #Used when signed in on nessie and apps to check if a tag is usable
    def angler_from_angler_id
      @angler = Angler.from_ambiguous_id(params[:angler_id])
    end

    #Used when signed in on nessie and apps to check if a tag is usable
    def angler_from_tag_number
      @angler = Angler.joins(:tags).where(tags: { tag_no: params[:tag_number].try(:upcase) }).first
    end

    #Used when signed in on nessie and apps to check if a tag is usable
    def validate_angler_id_available
      if Angler.from_ambiguous_id(params[:angler_id]).nil?
        render json: {}, status: :ok
      else
        render json: { error: 'Angler ID already taken' }, status: :not_acceptable
      end
    end

    #used by Narwhal to check if the tag exists
    def validate_tag
      tag_number = params[:tag_number].try(:upcase)
      recapture  = params[:recapture] == '1'
      angler_id  = Angler.from_ambiguous_id(params[:angler_id]).try(:id)

      tag = Tag.where(tag_no: tag_number).first

      status = :ok
      @error_messages = []

      # no tag exists
      if tag.nil?
        status = :not_found
        @error_messages << 'Tag does not exist'

        # The tag has been marked as lost or broken (deactivated)
      elsif !tag.active
        status = :not_found
        @error_messages << 'This tag has been deactivated'

        # the tag has been previously assigned, from the assignment page
      elsif (params[:start_tag_no] or params[:end_tag_no]) and tag.angler
        status = :not_found
        @error_messages << 'Tag is already assigned'

        # the tag hasn't been assigned to anyone
      elsif tag.angler.nil? and recapture == true
        status = :not_found
        @error_messages << 'Tag is not assigned to an angler'

        # The tag was not assigned to the entered angler
      elsif !recapture && angler_id.present? && tag.angler_id != angler_id
        status = :not_found
        @error_messages << "This tag is assigned to #{tag.angler.try(:angler_id)} not #{params[:angler_id].try(:upcase)}"

        # a previous capture exists
      elsif Capture.joins(:tag).where( tags: { tag_no: tag_number }).first and !recapture
        status = :not_found
        @error_messages << 'A capture already exists for this tag'

      end

      # have to specify the template to use the status code.
      render template: 'api/v2/internal_api/check_tag_valid', status: status
    end

  end
end
module Api::V2
  class ApiController < ActionController::Base

    LATITUDE_LAMBDA = lambda { |val| val.to_f.between?(20, 50) }
    LONGITUDE_LAMBDA = lambda { |val| val.to_f.between?(-120, -50) }
    LENGTH_LAMBDA = lambda { |val| val.to_f > 0 }
    WEIGHT_LAMBDA = lambda { |val| val.to_f > 0 }

    TAG_REGEX = /\A[A-Za-z]+[\d\-]{6,7}\z/ # /\A[A-Z]+\d{6}\z/
    DATE_REGEX = /(\d{4})-(\d{2})-(\d{2})T(\d{2})\:(\d{2})\:(\d{2})Z/
    UUID_REGEX = /\A(\w{8}(-\w{4}){3}-\w{12}?)\z/
    EMAIL_REGEX = /\A[^@]+@[^@]+\z/
    PASSWORD_REGEX = /\A.{8,128}\z/
    STATE_REGEX = /\A[a-zA-Z]{2}\z/
    ZIP_CODE_REGEX = /\A\d{5}\z/
    PHONE_REGEX = /\A((\d{3})(-)?(\d{3})(-)?(\d{4}))|((1\s)(\(\d{3}\)\s)(\d{3})(-)?(\d{4}))\z/ # 123-456-7890 or 1 (123) 456-7890

    # This is the rescue for when a param is malformed an does not
    # pass the validation
    rescue_from Apipie::ParamError do |e|
      render json: {error: e.message}, status: :unprocessable_entity
    end

    # This is the rescue for when a 'required' param is missing
    rescue_from Apipie::ParamMissing do |e|
      render json: {error: e.message}, status: :unprocessable_entity
    end

    # all APIs respond with JSON
    respond_to :json

    # wea re skipping the devise authentication for the API
    # authentication will be done via OAuth, using the Doorkeeper gem
    skip_before_action :verify_authenticity_token

    def only_id_list_response(query, klass)
      ids = query.except(:joins, :select, :order).reorder(id: :asc).pluck(:id)

      {
          errors: [],
          id_hash: klass.ids_as_md5(query.clone.except(:joins, :select, :order)),
          version: query.except(:joins, :select, :order).reorder(updated_at: :desc).limit(1).first.try(:updated_at),
          count: ids.count,
          ids: ids
      }
    end

    def append_info_to_payload(payload)
      super
      client = DeviceDetector.new(request.user_agent)

      payload[:remote_ip] = request.remote_ip
      payload[:user_id] = doorkeeper_token.try(:resource_owner_id)
      payload[:device] = {
          device_type: client.device_type,
          device_name: client.device_name,
          os_name: client.os_name,
          name: client.name
      }
    end

    def info_for_paper_trail
      client_device = DeviceDetector.new(request.user_agent)

      {
          meta: {
              client_info: {
                  user_agent: request.user_agent,
                  ip: request.remote_ip,
                  name: client_device.name,
                  version: client_device.full_version,
                  os: client_device.os_name,
                  os_version: client_device.os_full_version,
                  device: client_device.device_name,
                  device_type: client_device.device_type
              }
          }
      }
    end

    private

    # This is used to create a helper to find a user that
    # is associated with the OAuth2 token
    def current_resource_owner
      User.where(id: doorkeeper_token.resource_owner_id) if doorkeeper_token
    end

  end
end
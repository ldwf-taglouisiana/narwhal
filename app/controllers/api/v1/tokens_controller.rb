class Api::V1::TokensController  < ApplicationController

  skip_before_filter :verify_authenticity_token

  skip_authorization_check

  # this removes the need to pre-authenticate to get a token
  before_filter :authenticate_user!, :except => [:create, :destroy]

  respond_to :json

  def create

    email = params[:email]
    password = params[:password]

    if request.format != :json
      render :status=>406, :json=>{:message=> 'The request must be json'}
      return
    end

    if email.nil? or password.nil?
      render :status=>400,
             :json=>{:message=> 'The request must contain the user email and password.'}
      return
    end

    @user=User.find_by_email(email.downcase)

    if @user.nil?
      logger.info("User #{email} failed signin, user cannot be found.")
      render :status=>401, :json=>{:message=> 'Invalid email or passoword.'}
      return
    end

    # http://rdoc.info/github/plataformatec/devise/master/Devise/Models/TokenAuthenticatable
    @user.ensure_authentication_token!

    if not @user.valid_password?(password)
      logger.info("User #{email} failed signin, password \"#{password}\" is invalid")
      render :status => 401, :json => {:message => 'Invalid email or password.'}
      # If the user does not have an Angler ID, prevent him/her from logging in and
      # log the error.
    elsif @user.angler_id.nil? or @user.angler_id.empty?
      logger.info("User #{email} failed signin, has no angler ID.")
      render :status => 401, :json => {:message => 'Login failure, user has no Angler ID'}
    else
      result = Hash.new
      result['items'] = Hash.new
      result['items']['token'] = @user.authentication_token
      result['items']['first_name']=@user.first_name
      result['items']['last_name']=@user.last_name


      if request.headers['mobile-app'].present?
        device = Device.new
        device.user = @user
        device.save!

        result['items']['device_id'] = device.identifier
      end

      render :status=>200, :json=>result.to_json
    end

    # Token creation sign a user in automatically, this will sign them out after token creation
    sign_out(@user)
  end

  def destroy
    @user=User.find_by_authentication_token(params[:id])
    if @user.nil?
      logger.info('Token not found.')
      render :status=>404, :json=>{:message=> 'Invalid token.'}
    else
      @user.reset_authentication_token!
      render :status=>200, :json=>{:token=>params[:id]}
    end
  end

end

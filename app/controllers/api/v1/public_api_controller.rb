module Api::V1
  class PublicApiController < ApiController
    include DashboardModule
    include FuzzyMapModule

    skip_after_filter :set_headers, only: [:get_photo_stream, :get_photos]

    #######################################################################################
    ## GET methods for general data
    def announcements
      data = Hash.new

      items = Announcement.where('now() < expiration_date').order('expiration_date DESC').limit(1)

      data['errors'] = []
      data['count'] = 1
      data['version'] = items.empty? ? 0 : items[0].updated_at.to_i
      data['items'] = items

      data
    end

    def get_announcements
      render :json => announcements
    end


    def get_combined_data
      updated_params = {
          species: {},
          species_lengths: {},
          news_feed: {},
          photo_stream: {},
          fish_condition_options: {},
          time_of_day_options: {},
          recapture_disposition_options: {},
          shirt_size_options: {}
      }.deep_merge(params.deep_symbolize_keys)

      data = {
          species: species(updated_params[:species]),
          species_lengths: species_lengths(updated_params[:species_lengths]),
          news_feed: news_feed(updated_params[:news_feed]),
          photo_stream: photo_stream(updated_params[:photo_stream]),
          time_of_day_options: time_of_day_options(updated_params[:time_of_day_options]),
          fish_condition_options: fish_condition_options(updated_params[:fish_condition_options]),
          recapture_disposition_options: recapture_disposition_options(updated_params[:recapture_disposition_options]),
          shirt_size_options: shirt_size_options(updated_params[:shirt_size_options])
      }

      render json: data
    end

    def get_species
      render :json => species(params)
    end

    def get_species_lengths
      render :json => species_lengths(params)
    end

    def get_marinas
      render :json => marinas(params)
    end

    def get_blurbs
      render :json => blurbs(params)
    end

    def get_about_tagging_program
      data = Hash.new

      items = []
      items << AboutTaggingProgram.all.first ? AboutTaggingProgram.all.first : {}

      data['errors'] = []
      data['count'] = 1
      data['version'] = AboutTaggingProgram.all.first ? AboutTaggingProgram.all.first.updated_at.to_i : 0
      data['items'] = items

      render :json => data
    end

    def get_how_to_tag
      data = Hash.new

      items = []
      items << HowToTag.all.first ? HowToTag.all.first : {}

      data['errors'] = []
      data['count'] = 1
      data['version'] = HowToTag.all.first ? HowToTag.all.first.updated_at.to_i : 0
      data['items'] = items

      render :json => data
    end

    def get_tag_history
      tag_no = params[:tag_number].upcase

      (captures_query = '') << <<-SQL
          with
          caps as (
            select  recaptures.id,
                    recaptures.tag_id,
                    to_char(recaptures.capture_date, 'yyyy-MM-ddThh24:MI:SS' ) as capture_date,
                    recaptures.species_id,
                    round( CAST(recaptures.latitude as numeric), 4) as latitude,
                    round( CAST(recaptures.longitude as numeric), 4) as longitude,
                    format_latitude(geom, gps_formats.format_string) as "latitude_string",
                    format_longitude(geom, gps_formats.format_string) as "longitude_string",
                    recaptures.comments,
                    recaptures.length,
                    recaptures.fish_condition_option_id,
                    recaptures.entered_gps_type,
                    recaptures.time_of_day_option_id,
                    species.common_name as species_name,
                    tags.tag_no as tag_number,
                    fish_condition_options.fish_condition_option as fish_condition,
                    to_char(recaptures.updated_at, 'yyyy-MM-ddThh24:MI:SS' ) as version,
                    recaptures.location_description,
                    time_of_day_options.time_of_day_option as time_of_day_option
            from recaptures left join species on recaptures.species_id = species.id
                  left join tags on recaptures.tag_id = tags.id
                  left join fish_condition_options on recaptures.fish_condition_option_id = fish_condition_options.id
                  left join time_of_day_options on recaptures.time_of_day_option_id = time_of_day_options.id
                  LEFT OUTER JOIN gps_formats ON recaptures.entered_gps_type = gps_formats.format_type
            where tags.tag_no ILIKE ?
            order by recaptures.capture_date ASC
          )
          select array_to_json(array_agg(row_to_json(t))) as "data"
          from (
            select *
            from caps
          ) t
      SQL

      sanitized_sql = [captures_query, tag_no]
      sql = ActiveRecord::Base.send(:sanitize_sql_array, sanitized_sql)
      captures = ActiveRecord::Base.connection().execute(sql)

      captures = captures[0]['data']
      captures = captures.nil? ? JSON.parse('[]') : JSON.parse(captures, :symbolize_names => true)
      #this look up is needed because the get_fuzzy_map method expects capture objects not an array of capture hashes
      capArray = Array.new
      captures.each do |cap|
        capArray << Capture.find(cap[:id])
      end
      params[:width] = 300
      params[:height] = 200
      mapArray = get_fuzzy_map(capArray, params)
      #after getting the map data, add it to the the correct captures
      mapArray.each_with_index do |cap, index|
        captures[index][:map_url] = cap[:map_url]
        if !/enc/.match(cap[:map_url]).nil?
          captures[index][:latitude] = nil
          captures[index][:longitude] = nil
        end

      end

      data = Hash.new
      data[:errors] = []
      data[:count] = captures.count(:all)
      data[:items] = captures
      data[:version] = ''

      render :json => data
    end

    def get_time_of_day_options
      render :json => time_of_day_options(params)
    end

    def get_fish_condition_options
      render :json => fish_condition_options(params)
    end

    def get_recapture_disposition_options
      render :json => recapture_disposition_options(params)
    end

    def get_shirt_size_options
      render :json => shirt_size_options(params)
    end

    def get_news_feed
      render :json => news_feed(params)
    end

    def get_photo_stream
      render :json => photo_stream(params)
    end

    def capture_map_data
      clean_params = params.permit(:map_type, :event_type, :number_of_days, :start_date, :end_date)

      json = '{}'

      if clean_params[:map_type] == 'Basin'
        json = get_capture_shapes(clean_params)
      elsif clean_params[:map_type] == 'Heat Map'
        json = get_capture_points(clean_params)
      elsif clean_params[:map_type] == 'Points'
        json = get_capture_points(clean_params)
      end

      render :json => json
    end

    def get_photos
      photo = Photo.find(params[:id])
      style = params[:style] ? params[:style] : 'original'
      send_file(photo.image.path(style), {:type => photo.image_content_type, :disposition => 'inline'})
    end

    def get_public_dashboard
      json = Dashboard.overall_stats

      if JSON.parse(json)["version"] == params[:version]

        json = {
            items: nil,
            version: params[:version]
        }

      end

      render :json => json
    end


    def get_new_draft_capture
      data = Hash.new
      data['draft_capture'] = DraftCapture.new
      render :json => data
    end


    def get_angler_capture_report
      data = Hash.new
      user = User.find(doorkeeper_token.resource_owner_id) if doorkeeper_token
      angler_id = user.angler_id
      data[:captures] = module_get_angler_capture_data(angler_id)

      render :json => data
    end


    #######################################################################################
    ## Actionable methods

    def create
      #Expected JSON Object
      #
      #{
      #    "user" : {
      #      "email" : "testt.com",
      #      "password" : "xxxxxx".
      #      "password_confirmation" : "xxxxxx"
      #    },
      #    "angler" : {
      #      "first_name" : "bob",
      #      "last_name" : "smith",
      #      "street" : "fdsfdsf",
      #      "suit" : "dfdff",
      #      "city" : "fdsfds",
      #      "state" : "LA",
      #      "zip" : "90008",
      #      "phone_number" : "504-555-5555",
      #      "shirt_size" : "M"
      #    }
      #}
      status = :internal_server_error
      data = Hash.new
      errors = []
      messages = []

      #We only want to service apps that have been registered with the system, so see if they stack up
      if APPLICATION_HASHES.include?(params[:token])
        clean_params = params.permit(:angler => [:first_name, :last_name, :street, :suite, :city, :state, :zip, :phone_number_1, :phone_number_2, :phone_number_3, :email, :email_2, :shirt_size, :comments], :user => [:email, :password, :password_confirmation, :is_existing_angler])

        begin
          ActiveRecord::Base.transaction do


            angler = Angler.new_app_angler(clean_params[:angler])
            user = User.new(clean_params[:user])

            user.first_name = angler.first_name
            user.last_name = angler.last_name
            user.angler = angler

            user.save!
            angler.save!

            if user.persisted?
              user.add_role Role.find(3).name
              Notification.build_user(user)
              #AdminNotifyMailer.new_app_user_email(user).deliver if Rails.env == 'production'
              UserEmailJob.perform_later(user.id)
            end

          end
        rescue => error
          logger.fatal 'Error Occurred'
          logger.fatal error
          errors << error.message #'Could not register the user'
          status = :internal_server_error
        else
          messages << 'Successfully created the user'
          status = :ok
        end
      else
        errors << 'You are not authorized'
        status = :unauthorized
      end


      data[:errors] = errors
      data[:messages] = messages
      render :json => data, :status => status
    end


    def post_non_registered_capture
      status = :internal_server_error
      data = Hash.new
      new_angler_token = get_random_token(25)
      if APPLICATION_HASHES.include?(params[:token])
        data[:errors] = []
        clean_params = params.permit(:items => [:capture => [:capture_date, :comments, :entered_gps_type, :time_of_day,
                                                             :fish_condition_id, :location_description, :latitude,
                                                             :longitude, :time_of_day_id, :recapture_disposition_id,
                                                             :species_id, :species_length_id, :tag_number, :length,
                                                             :entered_gps_type]
                                     ])
        items = clean_params[:items]
        captures = []

        #We need to catch any error that occurs so that the system keeps running
        begin
          ActiveRecord::Base.transaction do
            #amke a new capture and assign a temp angler to it, they may not enter their info at the same time
            items.each do |item|
              a = Angler.TEMP_ANGLER
              event_params = item[:capture]
              event_params[:tag_id] = Tag.where(:tag_no => event_params.delete(:tag_number)).first.id
              event_params[:capture_date] = Chronic.parse(event_params[:capture_date])
              event_params[:time_of_day_option_id] = event_params.delete(:time_of_day_id)
              event_params[:fish_condition_option_id] = event_params.delete(:fish_condition_id)

              event = Recapture.new(event_params)

              event.angler = a
              event.user_capture = true
              event.verified = false
              event.angler_creation_token = new_angler_token
              
              data[:new_angler_token] = new_angler_token

              event.save!

              captures << event
            end
          end
            #something goof'd so now we log the error and report to the user what happened
        rescue => error
          logger.fatal 'Error occurred'
          logger.fatal error
          data[:errors] << 'Could not save some or all of the captures'
          status = :internal_server_error
            #everything went fine do this stuff now
        else
          captures.each do |capture|
            if capture.instance_of?(Recapture)
              Notification.build_recapture(capture)
            end
          end
          status = :ok
        end
      else
        status = :unauthorized
        data[:errors] << 'You are not authorized'
      end

      render :json => data, :status => status

    end

    def create_anon_cap_angler
      status = :internal_server_error
      data = Hash.new
      if APPLICATION_HASHES.include?(params[:token])
        data[:errors] = []
        clean_params = params.permit(:items => [:angler => [:first_name, :last_name, :phone_number_1, :email]])
        items = clean_params[:items]

        #We need to catch any error that occurs so that the system keeps running
        begin
          ActiveRecord::Base.transaction do
            #amke a new capture and assign a temp angler to it, they may not enter their info at the same time
            items.each do |item|
              a = Angler.where('first_name ILIKE ? AND last_name ILIKE ? AND regexp_replace(phone_number_1,\'[^\d]\',\'\') = ? AND email ILIKE ? ',
                               item[:angler][:first_name], item[:angler][:last_name], item[:angler][:phone_number_1], item[:angler][:email]).first
              if a.nil?
                a = Angler.new_app_angler(item[:angler])
                a.save!
              end

              c = Recapture.where(angler_creation_token: params[:angler_creation_token]).first

              unless c.nil?
                c.angler = a
                c.save!
                AnonUserReportEmailJob.perform_later(c)
              end
            end
          end
            #something goof'd so now we log the error and report to the user what happened
        rescue => error
          logger.fatal 'Error occurred'
          logger.fatal error
          data[:errors] << 'Could not save your contact info'
          status = :internal_server_error
            #everything went fine do this stuff now
        else
          status = :ok
        end
      else
        status = :unauthorized
        data[:errors] << 'You are not authorized'
      end

      render :json => data, :status => status

    end


    def reset_user_password
      data = Hash.new

      if APPLICATION_HASHES.include?(params[:app_token])
        user = User.where('email = ?', params[:email]).first

        if user.try(:update_attributes, { :password => params[:password], :password_confirmation => params[:password_confirmation] })
          data[:change_confirmation] = true
          status = :ok
        else
          data[:change_confirmation] = false
          status = :unauthorized
        end

      else
        data[:change_confirmation] = false
        status = :unauthorized
      end

      render :json => data, :status => status
    end

    #######################################################################################
    ## Data check methods

    # checks to see if a given tag number is already associated with a capture
    def check_for_duplicate_caps
      data = Hash.new
      result = Capture.where(:tag_id => Tag.where(:tag_no => params[:tag_number]).first.id).to_a.length == 0
      data[:result] = result
      render :json => data
    end

    #used by Narwhal to check if the tag exists
    def check_tag_exists
      tag = Tag.where(:tag_no => params[:fieldValue]).first

      answer = []
      answer << params[:fieldId]

      # if the field is empty
      if params[:fieldValue].empty?
        answer << true

        # no tag exists
      elsif tag.nil?
        answer << false
        answer << 'Tag does not exist'

        # The tag has been marked as lost or broken (deactivated)
      elsif !tag.active
        answer << false
        answer << 'This tag has been deactivated'

        # the tag has been previously assigned, from the assignment page
      elsif (params[:fieldId] == 'start_tag_no' or params[:fieldId] == 'end_tag_no') and tag.angler
        answer << false
        answer << 'Tag is already assigned'

        # the tag has not been previously assigned, from the assignment page
      elsif (params[:fieldId] == 'start_tag_no' or params[:fieldId] == 'end_tag_no') and !tag.angler
        answer << true

        # the tag hasn't been assigned to anyone
      elsif !tag.angler and (params[:secret_recap] == 'false' or params[:secret_recap] == '')
        answer << false
        answer << 'Tag is not assigned to an angler'

        # a previous capture exists
      elsif Capture.where("tag_id = ?", tag.id).to_a.length > 0 and (params[:secret_recap] == 'false' or params[:secret_recap] == '')
        answer << false
        answer << 'A capture already exists for this tag'

        # default to valid
      else
        answer << true

      end

      render :json => answer

    end

    #Used by the general public (nessie and apps) to determine if a tag is usable in context
    def public_check_tag_exists
      tag = Tag.where(:tag_no => params[:fieldValue]).first

      answer = []
      answer << params[:fieldId]

      # if the field is empty
      if params[:fieldValue].empty?
        answer << true

        # no tag exists
      elsif tag.nil?
        answer << false
        answer << 'Tag does not exist'

        # the tag hasn't been assigned to anyone
      elsif !tag.angler
        answer << false
        answer << 'Tag is not available for use'

        # The tag has been marked as lost or broken (deactivated)
      elsif !tag.active
        answer << false
        answer << 'This tag has been deactivated'

        # a previous capture exists
      elsif Capture.where("tag_id = ?", tag.id).to_a.length > 0 and (params[:secret_recap] == 'false' or params[:secret_recap] == '')
        answer << false
        answer << 'A capture already exists for this tag'

        # default to valid
      else
        answer << true

      end

      render :json => answer

    end

    def check_if_email_exists
      answer = []
      answer << params[:fieldId]

      if User.where(:email => params[:fieldValue]).first
        answer << false
      else
        answer << true
      end

      respond_to do |format|
        format.json { render json: answer }
      end
    end

    private

    def get_capture_points(params)

      table = 'captures'
      kind = 'points'

      if params[:map_type] == 'Points'
        kind = 'points'
      elsif params[:map_type] == 'Heat Map'
        kind = 'heatmap'
      end

      if params[:event_type] == 'Captures'
        table = 'captures'
      elsif params[:event_type] == 'Recaptures'
        table = 'recaptures'
      end

      if params[:number_of_days].present?
        date_where = "C.capture_date::date between (now() - '#{params[:number_of_days].to_i} days'::interval)::date and now()::date"
      elsif params[:start_date].present? and params[:end_date].present?
        date_where = 'C.capture_date::date between ?::date and ?::date'
      else
        date_where = "C.capture_date::date between (now() - '30 days'::interval)::date and now()::date"
      end

      connection = ActiveRecord::Base.connection()

      (query = '') << <<-SQL
          with combined as (
            SELECT C.id, C.geom as geom
            from #{table} C
            where C.geom is not null AND
                  #{date_where}
          )
          SELECT row_to_json(t) as "json"
          FROM (
            SELECT row_to_json(t) as "json", '#{kind}' as "kind"
            FROM (
              SELECT 'FeatureCollection' AS type,
                   array_to_json(array_agg(row_to_json(m))) AS features
              FROM (
                SELECT 'Feature' AS type,
                   ST_AsGeoJSON(geom)::json AS geometry,
                   (
                     SELECT row_to_json(b)::json
                     FROM (
                       SELECT id
                       FROM combined
                       WHERE g.id = id
                     ) b
                   ) AS properties
                FROM combined g
                WHERE g.geom is not null
              ) m
            ) t
          )t;
      SQL

      sanitized_sql = [query]

      if params[:start_date].present? and params[:end_date].present?
        sanitized_sql = sanitized_sql + [Chronic.parse(params[:start_date]), Chronic.parse(params[:end_date])]
      end

      sql = ActiveRecord::Base.send(:sanitize_sql_array, sanitized_sql)

      json = connection.execute(sql)[0]['json']

      return json
    end

    def get_capture_shapes(params)

      table = 'captures'
      kind = 'shapes'

      if params[:event_type] == 'Captures'
        table = 'captures'
      elsif params[:event_type] == 'Recaptures'
        table = 'recaptures'
      end

      if params[:number_of_days].present?
        date_where = "C.capture_date::date between (now() - '#{params[:number_of_days].to_i} days'::interval)::date and now()::date"
      elsif params[:start_date].present? and params[:end_date].present?
        date_where = 'C.capture_date::date between ?::date and ?::date'
      else
        date_where = "C.capture_date::date between (now() - '30 days'::interval)::date and now()::date"
      end

      connection = ActiveRecord::Base.connection()

      (query = '') << <<-SQL
          with combined as (
            SELECT  B.gid, B.name, count(*), st_geometryn(B.geom, 1) as geom
            from #{table} C inner join basins B on st_within(C.geom,B.geom)
            where C.geom is not null AND
                  #{date_where}
            group by B.gid, B.name, B.geom
          )
          SELECT row_to_json(t) as "json"
          FROM (
            SELECT row_to_json(t) as "json", '#{kind}' as "kind"
            FROM (
              SELECT 'FeatureCollection' AS type,
                   array_to_json(array_agg(row_to_json(m))) AS features
              FROM (
                SELECT 'Feature' AS type,
                   ST_AsGeoJSON(geom)::json AS geometry,
                  (
                     SELECT row_to_json(b)::json
                     FROM (
                       SELECT count, name
                       FROM combined
                       WHERE g.gid = gid
                     ) b
                   ) AS properties
                FROM combined g
                WHERE g.geom is not null
              ) m
            ) t
          )t;
      SQL

      sanitized_sql = [query]

      if params[:start_date].present? and params[:end_date].present?
        sanitized_sql = sanitized_sql + [Chronic.parse(params[:start_date]), Chronic.parse(params[:end_date])]
      end

      sql = ActiveRecord::Base.send(:sanitize_sql_array, sanitized_sql)

      json = connection.execute(sql)[0]['json']

      json
    end

    # fetch methods for some common api get methods.

    def species_lengths(params)
      data = Hash.new

      includes = ['id', 'description', 'species_id', 'position', 'to_char(updated_at, \'yyyy-MM-ddThh24:MI:SS\' ) AS version'];

      if params[:version]
        results = SpeciesLength.where("date_trunc('second', updated_at) > ?", params[:version]).order('updated_at ASC').select(includes)
      else
        results = SpeciesLength.order('updated_at ASC').select(includes)
      end

      data[:errors] =[]
      data[:count] = results.count(:all)
      data[:items] = results

      data
    end

    def species(params)
      default_includes = %w(id common_name position proper_name other_name family habitat description size food_value target publish photo_file_name)
      image_styles = %w(medium feature index_list thumb )

      image_size = image_styles.include?(params[:image_size]) ? params[:image_size] : 'medium'
      limit = params[:limit]
      offset = params[:offset]
      includes = params[:includes] ? params[:includes].split(/,/) & default_includes : default_includes
      target = params[:target]

      includes = includes.empty? ? default_includes : includes
      includes = includes + ['to_char(updated_at, \'yyyy-MM-ddThh24:MI:SS\' ) AS version']

      # GET THE PHOTO URL, THE PATH IS RIPPED FROM THE DEFINITION IN THE SPECIES MODEL
      (photo_url = '') << <<-SQL
        CASE
            WHEN photo_file_name IS NULL
                THEN NULL
                ELSE concat('#{request.protocol}#{request.host_with_port}','/species/image/', id, '/', photo_file_name, '?style=#{image_size}&',  EXTRACT(EPOCH FROM photo_updated_at))
        END AS image_url
      SQL

      includes = includes + [photo_url]
      includes = includes.join(',')

      id = params[:id] ? params[:id] : nil
      term = params[:term]

      species = {}

      if id
        current_version = Species.find(id).updated_at.to_i
        species = Species.where('id = ?', [id]).select(includes.empty? ? default_includes : includes)

      elsif term
        where_statement = []
        where_statement << '(common_name ilike ? OR proper_name ilike ? OR other_name ilike ?)'
        where_statement << " date_trunc('second', updated_at) > #{ params[:version].present? ? '?' : '(select min(updated_at) - interval \'1 min\' from species)'}"

        if params[:version].present?
          species = Species.where(where_statement.join(' AND '), "%#{term}%", "%#{term}%", "%#{term}%", params[:version])
                        .select(includes.empty? ? default_includes : includes)
                        .order('common_name')
                        .offset(offset ? offset : 0)
                        .limit(limit ? limit : nil)

        else
          species = Species.where(where_statement.join(' AND '), "%#{term}%", "%#{term}%", "%#{term}%")
                        .select(includes.empty? ? default_includes : includes)
                        .order('common_name')
                        .offset(offset ? offset : 0)
                        .limit(limit ? limit : nil)

        end


      elsif target
        species = Species.where('target = true')
                      .order('position asc')
                      .select(includes.empty? ? default_includes : includes)
      else
        current_version = Species.order('updated_at desc').first.updated_at.to_i
        species = Species.where("common_name != 'Unknown' and date_trunc('second', updated_at) > #{ params[:version].present? ? '?' : '(select min(updated_at) - interval \'1 min\' from species)'}", [params[:version]]).select(includes.empty? ? default_includes : includes)
                      .order('common_name')
                      .select(includes.empty? ? default_includes : includes)
                      .offset(offset ? offset : 0)
                      .limit(limit ? limit : nil)
      end

      data = Hash.new

      data[:errors] = []
      data[:count] = species.count(:all)
      data[:version] = current_version
      data[:items] = species.count(:all) > 0 ? species : []

      data
    end

    def marinas(params)
      default_includes = %w(name latitude longitude hours_of_operation parish id address web_address phone publish closed)

      zip_code = params[:zip_code] ? params[:zip_code] : nil
      dis = params[:dis] ? params[:dis] : 20
      limit = params[:limit] ? limit : nil
      offset = params[:offset] ? offset : 0
      parish = params[:parish] ? params[:parish] : nil
      includes = params[:includes] ? params[:includes].split(/,/) & default_includes : default_includes
      includes = includes.empty? ? default_includes : includes
      includes = includes + ['to_char(updated_at, \'yyyy-MM-ddThh24:MI:SS\' ) AS version']
      includes = includes.join(',');

      current_version = Marina.order('updated_at desc')
                            .offset(offset)
                            .limit(limit)
                            .first.updated_at

      marinas = {}

      wheres = []
      binds = []

      if params[:version].nil? or params[:version].empty?
        wheres << 'updated_at > (select min(updated_at) - interval \'1 minute\' from marinas)'
      else
        wheres << "date_trunc('second', updated_at) > ?"
        binds << Chronic.parse(params[:version]).to_s
      end


      if zip_code
        marinas = Marina.near(zip_code.to_region, dis)
                      .where(wheres.join(' AND '), binds)
                      .select(includes)
                      .offset(offset)
                      .limit(limit)
      else
        marinas = Marina.where(wheres.join(' AND '), binds)
                      .order("#{ parish ? 'parish,' : '' } name ")
                      .select(includes)
                      .offset(offset)
                      .limit(limit)
      end

      #marinas.each do |m|
      #  begin
      #    m[:photo_url] = "#{request.protocol}#{request.host_with_port}#{m.photo_url}"
      #  rescue
      #    raise m.photo.inspect
      #  end
      #end

      data = Hash.new
      data['count'] = marinas.count(:all)

      result = Hash.new

      if parish
        marinas.each do |m|
          unless result[m.parish.to_s]
            result[m.parish.to_s] = []
          end

          result[m.parish.to_s] << m

          result[m.parish.to_s].sort
        end

        result
      else
        result = marinas
      end


      data[:errors] = []

      data[:version] = current_version
      data[:items] = result

      data
    end

    def blurbs(params)
      default_includes = %w(about_program capture_dashboard common_fish contact_info draft_captures find_a_spot how_to licensing my_captures my_tags new_capture order_tags sign_up target_species volunteer_hours contact_us account_info faq privacy_policy)

      version = params[:version] ? params[:version].to_i : 0
      includes = params[:includes] ? params[:includes].split(/,/) & default_includes : default_includes

      current_version = Blurb.order('updated_at desc').first.updated_at.to_i

      blurbs = current_version != version ? Blurb.select(includes) : []

      data = Hash.new
      data['count'] = blurbs.count(:all)
      data['errors'] = ''
      data['version'] = current_version
      data['items'] = blurbs

      data
    end

    def time_of_day_options(params)
      data = Hash.new

      includes = ['id', 'time_of_day_option', 'to_char(updated_at, \'yyyy-MM-ddThh24:MI:SS\' ) AS version']

      if params[:version]
        results = TimeOfDayOption.where('date_trunc(\'second\', updated_at) > ?', params[:version]).order('updated_at desc').select(includes)
      else
        results = TimeOfDayOption.order('updated_at desc').select(includes)
      end

      data[:errors] =[]
      data[:count] = results.count(:all)
      data[:items] = results

      data
    end

    def fish_condition_options(params)
      data = Hash.new

      includes = ['id', 'fish_condition_option', 'to_char(updated_at, \'yyyy-MM-ddThh24:MI:SS\' ) AS version']

      if params[:version]
        results = FishConditionOption.where('date_trunc(\'second\', updated_at) > ?', params[:version]).order('updated_at desc').select(includes)
      else
        results = FishConditionOption.order('updated_at desc').select(includes)
      end

      data[:errors] =[]
      data[:count] = results.count(:all)
      data[:items] = results

      data
    end

    def recapture_disposition_options(params)

      data = Hash.new

      includes = ['id', 'recapture_disposition', 'to_char(updated_at, \'yyyy-MM-ddThh24:MI:SS\' ) AS version']

      if params[:version]
        results = RecaptureDisposition.where('date_trunc(\'second\', updated_at) > ?', params[:version]).order('updated_at desc').select(includes)
      else
        results = RecaptureDisposition.order('updated_at desc').select(includes)
      end

      data[:errors] =[]
      data[:count] = results.count(:all)
      data[:items] = results

      data
    end

    def shirt_size_options(params)

      data = Hash.new

      includes = ['id', 'shirt_size_option', 'to_char(updated_at, \'yyyy-MM-ddThh24:MI:SS\' ) AS version']

      if params[:version]
        results = ShirtSizeOption.where('date_trunc(\'second\', updated_at) > ?', params[:version]).order('updated_at desc').select(includes)
      else
        results = ShirtSizeOption.order('updated_at desc').select(includes)
      end

      data[:errors] =[]
      data[:count] = results.count(:all)
      data[:items] = results

      data
    end

    def news_feed(params)
      limit = params[:limit].present? ? params[:limit] : 25
      offset = params[:offset].present? ? params[:offset] : 0

      (query = '') << <<-SQL
        with items as (
          select  id,
                  title,
                  content,
                  to_char(created_at, 'yyyy-MM-ddThh24:MI:SS' ) as version
          from news_items
          where should_publish is true
            #{ 'AND date_trunc(\'second\', updated_at) > ?' if params[:version]}
          order by created_at DESC
          limit ?
          offset ?
        )
        SELECT row_to_json(t) as "json"
        FROM (
          SELECT  array_to_json(array_agg(row_to_json(m))) as "items",
                  (SELECT count(*) FROM items) as "count",
                  ? as "limit",
                  ? as "offset"
          FROM items m
        )t;
      SQL

      sanitized_sql = params[:version] ? ([query] + [params[:version]]) : [query]
      sanitized_sql = sanitized_sql + [limit, offset, limit, offset]

      sql = ActiveRecord::Base.send(:sanitize_sql_array, sanitized_sql)
      json = ActiveRecord::Base.connection().execute(sql)[0]['json']

      result = JSON.parse(json, symbolize_name: true)
    end

    def photo_stream(params)

      if params[:version]
        results = FrontPagePhotoStreamItem.where('date_trunc(\'second\', updated_at) > ?', params[:version]).order('created_at desc')
      else
        results = FrontPagePhotoStreamItem.order('created_at desc')
      end

      data = Hash.new
      data[:items] = results.as_json(:only => [:id, :featured], :methods => [:version, :api_image_url, :image_name])
      data[:count] = results.count(:all)

      data
    end

  end
end

module Api::V1
  class ApiController < ActionController::Base

      skip_before_filter :verify_authenticity_token

      after_filter :set_headers

      #################################################################################


      def append_info_to_payload(payload)
        super
        client = DeviceDetector.new(request.user_agent)

        payload[:remote_ip] = request.remote_ip
        payload[:user_id] = doorkeeper_token.try(:resource_owner_id)
        payload[:device] = {
            device_type: client.device_type,
            device_name: client.device_name,
            os_name: client.os_name,
            name: client.name
        }
      end

      def info_for_paper_trail
        client_device = DeviceDetector.new(request.user_agent)

        {
            meta: {
                client_info: {
                    user_agent: request.user_agent,
                    ip: request.remote_ip,
                    name: client_device.name,
                    version: client_device.full_version,
                    os: client_device.os_name,
                    os_version: client_device.os_full_version,
                    device: client_device.device_name,
                    device_type: client_device.device_type
                }
            }
        }
      end

      private

      # Find the user that owns the access token
      def current_resource_owner
        User.where(:id => doorkeeper_token.resource_owner_id).select("id, first_name, last_name, email, concat(first_name, ' ', last_name) as name").first if doorkeeper_token
      end

      def set_headers
        response.headers["X-Accel-Buffering"] = "no"
      end
    end
end
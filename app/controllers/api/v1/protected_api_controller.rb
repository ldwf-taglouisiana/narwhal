module Api::V1
  class ProtectedApiController < ApiController
    include GetCapturesModule
    include DashboardModule

    doorkeeper_for :all, :except => [:create]

    before_filter :get_user, :get_angler
    before_filter :check_for_valid_user_id, :except => [:create]

    respond_to :json

    #Used when signed in on nessie and apps to check if a tag is usable
    def check_tag_exists
      tag = Tag.where(:tag_no => params[:fieldValue]).first

      answer = []
      answer << params[:fieldId]

      # if the field is empty
      if params[:fieldValue].empty?
        answer << true

        # no tag exists
      elsif tag.nil?
        answer << false
        answer << 'Tag does not exist'

        # the tag hasn't been assigned to anyone
      elsif !tag.angler and (params[:secret_recap] == 'false' or params[:secret_recap] == '')
        answer << false
        answer << 'Tag is not assigned to an angler'

        # The tag has been marked as lost or broken (deactivated)
      elsif !tag.active
        answer << false
        answer << 'This tag has been deactivated'

        # a previous capture exists
      elsif Capture.where("tag_id = ?", tag.id).to_a.length > 0 and (params[:secret_recap] == 'false' or params[:secret_recap] == '')
        answer << false
        answer << 'A capture already exists for this tag'

        # default to valid
      else
        answer << true

      end

      render :json => answer
    end

    def get_tags
      # The allowed attributes that can be retrieved for this query
      default_includes = %w(id tag_number used)

      ################################################################################################
      # Parameters for API call
      #
      # limit     - Used to limit the amount of entries returned. By default you will get all records.
      # offset    - The position of the first entry returned from 0.
      # version   - The version string of the data that you currently have. If given, will only return
      #             records if given version is older than current updated at version.
      # includes  - Can be used to delimit the number of attributes for the returend entries.
      ################################################################################################

      dynamic_sql_binds = []

      limit = params[:limit]
      offset = params[:offset] ? params[:offset] : 0
      includes = params[:includes] ? params[:includes].split(/,/) & default_includes : default_includes
      includes = includes.empty? ? default_includes : includes
      includes = includes + ['version']
      includes = includes.join(',');

      if params[:version].present?
        version = '?'
        dynamic_sql_binds << params[:version]
      else
        version = '(select min(updated_at) - interval \'1 min\' from tags where angler_id = ? )'
        dynamic_sql_binds << @user.angler_id
      end

      # get the max version of the requested set of data
      (current_version_query = '') << <<-SQL
        select max (updated_at) from (
          select to_char(tags.updated_at, 'yyyy-MM-ddThh24:MI:SS' ) as updated_at
          from tags
          where tags.angler_id = ?
                and date_trunc('second', tags.updated_at) > #{version}
          limit ?
          offset ?
        ) t
      SQL

      sanitized_sql = [current_version_query, @user.angler_id] + dynamic_sql_binds + [limit, offset]
      sql = ActiveRecord::Base.send(:sanitize_sql_array, sanitized_sql)
      current_version = ActiveRecord::Base.connection().execute(sql)[0]['max']

      (tags_query = '') << <<-SQL
          with
          user_tags as (
            select  tags.id,
                    tags.tag_no as tag_number,
                    to_char(tags.updated_at, 'yyyy-MM-ddThh24:MI:SS' ) as version,
                    (tags.id in (select distinct tag_id from captures)) as used
            from tags
            where tags.angler_id = ?
                  and date_trunc('second', tags.updated_at) > #{version}
            order by tag_no ASC
            limit ?
            offset ?
          )
          select array_to_json(array_agg(row_to_json(t))) as "data"
          from (
            select #{includes}
            from user_tags
          ) t
      SQL

      sanitized_sql = [tags_query, @user.angler_id,] + dynamic_sql_binds + [limit, offset]
      sql = ActiveRecord::Base.send(:sanitize_sql_array, sanitized_sql)
      tags = ActiveRecord::Base.connection().execute(sql)

      tags = tags[0]['data']
      tags = tags.nil? ? JSON.parse('[]') : JSON.parse(tags)

      data = Hash.new

      data[:errors] = []
      data[:count] = tags.count(:all)
      data[:version] = current_version
      data[:items] = tags

      render :json => data
    end

    def get_captures
      # The allowed attributes that can be retrieved for this query
      default_includes = %w(id capture_date tag_number species_id species_name species_length_id latitude latitude_string longitude longitude_string has_recapture comments location_description length fish_condition_option_id fish_condition entered_gps_type time_of_day_option_id photo_url range)

      ####################################################################################################
      # Parameters for API call
      #
      # limit     - Used to limit the amount of entries returned. By default you will get all records.
      # offset    - The position of the first entry returned from 0.
      # zipcode   - The starting zip code to search from. REQUIRES distance be set
      # distance  - The distance in miles to search out from the given zipcode. REQUIRES zipcode to be set
      # version   - The version string of the data that you currently have. If given, will only return
      #             records if given version is older than current updated at version.
      # includes  - Can be used to delimit the number of attributes for the returend entries.
      ####################################################################################################

      dynamic_sql_binds = []

      limit = params[:limit]
      offset = params[:offset] ? params[:offset] : 0

      includes = params[:includes] ? params[:includes].split(/,/) & default_includes : default_includes
      includes = includes.empty? ? default_includes : includes
      includes = includes + ['version']
      includes = includes.join(',')

      species = params[:species] ? params[:species] : ''

      if params[:start_date].present?
        start_date = '?'
        dynamic_sql_binds << Chronic.parse(params[:start_date]).utc.to_s
      else
        start_date = '(select min(capture_date) from captures where angler_id = ?)'
        dynamic_sql_binds << @user.angler_id
      end

      if params[:end_date].present?
        end_date = '?'
        dynamic_sql_binds << Chronic.parse(params[:end_date]).utc.to_s
      else
        end_date = 'now()'
      end

      if params[:version].present?
        version = '?'
        dynamic_sql_binds << Chronic.parse(params[:version]).utc.to_s
      else
        version = '(select min(updated_at) - interval \'1 min\' from captures where angler_id = ? )'
        dynamic_sql_binds << @user.angler_id
      end

      # if the user wants the captures by distance from a zipcode
      distance_where = nil
      if params[:zipcode].present? and params[:distance].present?
        distance_where = 'AND ST_DWithin((select geom from zipcodes where id = ?), captures.geom, (? * 1609.34) )'
        dynamic_sql_binds << params[:zipcode]
        dynamic_sql_binds << params[:distance]
      end

      # get the max version of the requested set of data
      (current_version_query = '') << <<-SQL
        select max (updated_at) from (
          select to_char(captures.updated_at, 'yyyy-MM-ddThh24:MI:SS' ) as updated_at,
                 captures.capture_date
          from captures left join species on captures.species_id = species.id
                left join tags on captures.tag_id = tags.id
                left join fish_condition_options on captures.fish_condition_option_id = fish_condition_options.id
          where captures.angler_id = ?
                and captures.capture_date between #{start_date} and #{end_date}
                and date_trunc('second', captures.updated_at) > #{version}
                #{distance_where}
                and species.common_name ~* ?
          order by captures.capture_date desc
          limit ?
          offset ?
        ) t
      SQL

      sanitized_sql = [current_version_query, @user.angler_id] + dynamic_sql_binds + [species, limit, offset]
      sql = ActiveRecord::Base.send(:sanitize_sql_array, sanitized_sql)
      current_version = ActiveRecord::Base.connection().execute(sql)[0]['max']

      (captures_query = '') << <<-SQL
          with
          caps as (
            select  captures.id,
                    captures.tag_id,
                    to_char(captures.capture_date, 'yyyy-MM-ddThh24:MI:SS' ) as capture_date,
                    captures.species_id,
                    round( CAST(captures.latitude as numeric), 4) as latitude,
                    round( CAST(captures.longitude as numeric), 4) as longitude,
                    format_latitude(geom, gps_formats.format_string)            AS "latitude_string",
                    format_longitude(geom, gps_formats.format_string)           AS "longitude_string",
                    (captures.tag_id in (select tag_id from recaptures)) as has_recapture,
                    captures.comments,
                    captures.length,
                    captures.species_length_id,
                    captures.fish_condition_option_id,
                    captures.entered_gps_type,
                    captures.time_of_day_option_id,
                    captures.location_description,
                    species.common_name as species_name,
                    species_lengths.description as range,
                    (CASE
                      WHEN species.photo_file_name IS NULL
                        THEN ''
                        ELSE concat('#{request.protocol}#{request.host_with_port}','/species/image/', species.id, '/', species.photo_file_name, '::question_mark::style=original&',  EXTRACT(EPOCH FROM species.photo_updated_at))
                    END) AS photo_url,
                    tags.tag_no as tag_number,
                    fish_condition_options.fish_condition_option as fish_condition,
                    to_char(captures.updated_at, 'yyyy-MM-ddThh24:MI:SS' ) as version
            from captures
                  left join species on captures.species_id = species.id
                  left join tags on captures.tag_id = tags.id
                  left join fish_condition_options on captures.fish_condition_option_id = fish_condition_options.id
                  left join species_lengths on captures.species_length_id = species_lengths.id
                  LEFT OUTER JOIN gps_formats ON captures.entered_gps_type = gps_formats.format_type
            where captures.angler_id = ?
                  and captures.capture_date between #{start_date} and #{end_date}
                  and date_trunc('second', captures.updated_at) > #{version}
                  and captures.verified = true
                  #{distance_where}
                  and species.common_name ~* ?
            order by tag_number desc
            limit ?
            offset ?
          )
          select array_to_json(array_agg(row_to_json(t))) as "data"
          from (
            select #{includes}
            from caps
          ) t
      SQL

      sanitized_sql = [captures_query, @user.angler_id, ] + dynamic_sql_binds + [species, limit, offset]
      sql = ActiveRecord::Base.send(:sanitize_sql_array, sanitized_sql)
      captures = ActiveRecord::Base.connection().execute(sql)

      # there is an issue with having a non sanitized '?' in the prepared statement, I am using a substitioun flag add the '?' afterwords.
      captures = captures[0]['data'].try(:gsub, /::question_mark::/, '?')
      captures = captures.nil? ? JSON.parse('[]') : JSON.parse(captures)

      data = Hash.new
      data[:errors] = []
      data[:count] = captures.count(:all)
      data[:items] = captures
      data[:version] = current_version

      render :json => data
    end

    def get_angler_capture_report
      data = Hash.new
      user = User.find(doorkeeper_token.resource_owner_id) if doorkeeper_token
      angler_id = user.angler_id
      data[:captures] = module_get_angler_capture_data(angler_id)

      render :json => data
    end

    def get_draft_capture
      data = Hash.new
      data['draft_capture'] = DraftCapture.find(params[:id])
      render :json => data
    end

    def get_draft_captures
      dynamic_sql_binds = []

      includes = DraftCapture.attribute_names - %w(created_at updated_at capture_date saved_at)
      includes = includes + ['latitude_string', 'longitude_string', 'common_name as species_name', 'to_char(capture_date, \'yyyy-MM-ddThh24:MI:SS\' ) AS capture_date', 'to_char(updated_at, \'yyyy-MM-ddThh24:MI:SS\' ) AS version', 'to_char(saved_at, \'yyyy-MM-ddThh24:MI:SS\' ) AS saved_at']

      #drafts = user.angler.draft_captures.order('created_at').select(includes)

      species = params[:species] ? params[:species] : ''

      if params[:start_date].present?
        start_date = '?'
        dynamic_sql_binds << Chronic.parse(params[:start_date]).utc.to_s
      else
        start_date = '(select min(capture_date) from draft_captures where angler_id = ?)'
        dynamic_sql_binds << @user.angler_id
      end

      if params[:end_date].present?
        end_date = '?'
        dynamic_sql_binds << Chronic.parse(params[:end_date]).utc.to_s
      else
        end_date = 'now()'
      end

      (draft_captures_query = '') << <<-SQL
          with
          caps as (
            select
              draft_captures.*,
              format_latitude(geom, gps_formats.format_string) as "latitude_string",
              format_longitude(geom, gps_formats.format_string) as "longitude_string",
              common_name
            from draft_captures
              left join species on draft_captures.species_id = species.id
              LEFT OUTER JOIN gps_formats ON draft_captures.entered_gps_type = gps_formats.format_type
            where draft_captures.angler_id = ?
              and draft_captures.capture_date between #{start_date} and #{end_date}
              and species.common_name ~* ?
          )
          select array_to_json(array_agg(row_to_json(t))) as "data"
          from (
            select #{includes.join(',')}
            from caps
          ) t
      SQL

      sanitized_sql = [draft_captures_query, @user.angler_id] + dynamic_sql_binds + [species]
      sql = ActiveRecord::Base.send(:sanitize_sql_array, sanitized_sql)
      drafts = ActiveRecord::Base.connection().execute(sql)

      drafts = drafts[0]['data']
      drafts = drafts.nil? ? JSON.parse('[]') : JSON.parse(drafts)

      drafts.each do |draft|
        draft['status'] = DraftCapture.where(:id => draft['id']).first.status
      end

      data = Hash.new
      data[:errors] = []
      data[:count] = drafts.count(:all)
      data[:items] = drafts
      data[:version] = Time.now.to_s

      render :json => data
    end

    def get_status_for_draft
      d = DraftCapture.find(params[:id])
      render :json => d.nil? ? {} : d.status
    end

    def post_draft_captures

      data = Hash.new
      data[:errors] = []
      data[:messages] = []
      data[:count] = 0
      data[:items] = []

      status = :ok

      clean_params = params.permit(:items => [:id, :uuid, :should_delete, :capture_date, :comments, :entered_gps_type, :fish_condition_id, :latitude, :longitude, :recapture, :recapture_disposition_id, :species_id, :length, :species_length_id, :tag_number, :time_of_day_id, :should_save, :location_description])

      begin
        ActiveRecord::Base.transaction do
          clean_params[:items].try(:each) do |item|

            #Here the id is the rails id, if this param is nil or 0 then that means we're making a new capture
            if item[:id].nil? or item[:id] == 0
              c = DraftCapture.new(item.except(:id, :should_delete))
              c.angler_id = @user.angler_id
              c.save!
            else
              #otherwise we're either updating or deleting
              c = DraftCapture.where(id: item[:id])
              c.each do |temp|
                if item[:should_delete]
                  temp.destroy
                else
                  temp.update_attributes(item.except(:id, :uuid, :should_delete))
                end # end should_delete if
              end

            end # end do loop

            data[:items] << item[:uuid] unless item[:uuid].blank?
          end
        end

        data[:count] = data[:items].count

      rescue Exception => exp
        logger.fatal 'Error Occurred'
        logger.fatal exp

        data[:errors] << 'Could not save some or all of the captures'
        data[:items] = []
        data[:count] = 0

        status = :internal_server_error

      ensure
        render :json => data, :status => status
      end

    end

    # Method to update a draft capture
    def update_draft_capture
      status = :ok
      data = {
          errors: []
      }

      clean_params = params.except('protected_api').permit(items: [
            :id, :should_delete, :capture_date, :comments, :entered_gps_type,
            :fish_condition_id, :latitude, :longitude, :recapture, :recapture_disposition_id,
            :species_id, :species_length_id, :tag_number, :time_of_day_id, :length, :should_save,
            :location_description, :error_json, :uuid
          ]
      )

      logger.info(clean_params)

      if clean_params[:items].is_a? Array

        clean_params[:items].each do |draft_capture_attributes|
          draft_capture = DraftCapture.where(id: draft_capture_attributes[:id]).first

          begin
            if draft_capture
              ActiveRecord::Base.transaction do
                draft_capture.update_attributes(draft_capture_attributes)
              end
            else
              status = :unprocessable_entity
            end
          rescue StandardError => exp
            logger.fatal 'Error Occurred'
            logger.fatal exp
            data[:errors] << 'Could not update the draft capture'
            status = :internal_server_error
          end

        end

      elsif clean_params[:items].is_a? Hash

          draft_capture = DraftCapture.where(id: clean_params[:items][:id]).first

          begin
            if draft_capture
              ActiveRecord::Base.transaction do
                draft_capture.update_attributes(clean_params[:items])
              end
            else
              status = :unprocessable_entity
            end
          rescue StandardError => exp
            logger.fatal 'Error Occurred'
            logger.fatal exp
            data[:errors] << 'Could not update the draft capture'
            status = :internal_server_error
          end

      else
        data[:errors] << 'Params were not in a format that could be read'
        status = :internal_server_error

      end

      render :json => data, :status => status
    end

    # Method to switch a draft capture's should_save attribute
    def change_draft_capture_should_save
      draft_capture = DraftCapture.find(params[:id])
      draft_capture.should_save = !draft_capture.should_save
      draft_capture.saved_at = Time.now.utc
      draft_capture.save

      data = Hash.new
      render :json => data
    end

    def get_dashboard
      json = Dashboard.angler_stats(@user.angler_id)

      if JSON.parse(json)["version"] == params[:version]

        json = {
            items: nil,
            version: params[:version]
        }

      end

      render :json => json
    end

    def post_tag_request
      #requests are only valid if the user is verified
      if @user.angler.try(:is_app_angler?) && !@user.verified
        render :json => { :errors => ['You are not yet verified please allow a few days for verification before requesting additional tags'],
                          :status => :unprocessable_entity } and return
      end
      tags_available = true

      if tags_available
        data = Hash.new
        data[:errors] = []

        p =  params.permit(:items => [:tag_type, :number_of_tags])
        items = p[:items].nil? ? [] : p[:items]

        status = :internal_server_error
        request = Hash.new
        request[:angler_id] = @user.angler_id
        request[:tag_types] = Hash.new

        requests = []
        begin
          ActiveRecord::Base.transaction do
            items.each do |item|
              request[:tag_types][item['tag_type']] += item['number_of_tags'] if request[:tag_types].key?(item['tag_type'])
              request[:tag_types][item['tag_type']] = item['number_of_tags']

              m = UserTagRequest.new(item)
              m.angler_id = @user.angler_id
              m.fulfilled = false
              m.save

              requests << m
            end
          end
        rescue Exception => exp
          logger.fatal 'Error Occurred'
          logger.fatal exp
          data[:errors] << 'Could not create request'
          status = :internal_server_error
        else
          requests.each do |r|
            Notification.build_user_tag_request(r)
          end
          status = :ok
        ensure
          render :json => data, :status => status
        end
      else
        # No tags are available.
        render :json => {:errors => ['No tags are available at this time.']}, :status => :unprocessable_entity
      end
    end

    def post_volunteer_hours

      # Times must be in UTC
      #{
      #    "auth_token" : "fdsfsfgdsff",
      #    "items" : [
      #       {
      #         "start_date" : "2012-31-1T15:34:00",
      #         "end_date" : "2012-31-1T17:34:00",
      #         "verified" : true
      #       }, ...
      #    ]
      #}
      status = :internal_server_error
      data = Hash.new
      data[:errors] = []
      data[:messages] = []

      params_clean = params.permit(:items => [:start_date, :end_date, :verified])
      items = params_clean[:items].nil? ? [] : params_clean[:items]

      begin
        ActiveRecord::Base.transaction do
          items.each do |item|
            v = VolunteerTimeLog.new(:start => item[:start_date], :end => item[:end_date], :verified => item[:verified], :angler_id => @user.angler_id)
            v.save! if v.valid?
          end

        end

      rescue ActiveRecord::StatementInvalid => exp
        logger.fatal 'Error Occurred'
        logger.fatal exp
        data = 'Could not add hours because hours overlap with previous entered time on water. <a href="/my_captures/new" style="color:blue">Click here to enter captures.</a>'
        status = 422
      rescue Exception => exp
        logger.fatal 'Error Occurred'
        logger.fatal exp
        data = 'Could not add hours'
        status = :internal_server_error
      else
        status = :ok
      ensure
        render :json => data, :status => status
      end
    end

    def post_ios_device_registration

      device = UserDevice.new(
          {
              device_token: params[:device_token],
              user: @user,
              user_device_types_id: UserDeviceType.ios_type.id
          }
      )

      if device.save
        render json: {message: 'success'}, status: :ok
      else
        render json: {message: 'error'}, status: :internal_server_error
      end

    end

    def me
      Hash data = Hash.new

      data[:user] = @user.as_json(only: [:id, :first_name, :last_name, :email])
      data[:angler] = @angler.as_json(only: [:id, :first_name, :last_name, :street, :city, :state, :zip, :email, :phone_number_1, :phone_number_2, :angler_id])
      data[:version] = Digest::MD5.hexdigest(data[:user].to_json + data[:angler].to_json)

      render json: data
    end

    def update_me
      incorrect_password = false
      clean_params = params.permit(:first_name, :last_name, :street, :city, :state, :zip, :email, :phone_number_1, :phone_number_2, :old_pass, :password, :password_confirmation)
      if !clean_params[:old_pass].blank? && clean_params[:password] == clean_params[:password_confirmation]
        user = User.find_by_email(clean_params[:email])

        if user.valid_password?(clean_params[:old_pass])
          clean_params = clean_params.except(:old_pass)
        else
          clean_params = clean_params.except(:old_pass,:password,:password_confirmation)
          incorrect_password = true
        end
      else
        clean_params = clean_params.except(:old_pass,:password,:password_confirmation)
      end

      status = 500
      Hash data = Hash.new
      data[:message] = 'Could not save changes'

      if @angler.update_attributes(clean_params.except(:password,:password_confirmation)) && @user.update_attributes(clean_params.except(:street,:city,:state,:zip,:phone_number_1,:phone_number_2)) && !incorrect_password
        status = 200

        data[:message] = 'Updated successfully'
      elsif incorrect_password
        status = 500

        data[:message] = 'Incorrect current password'
      end

      render json: data, status: status
    end

    private

    def get_user
      @user = User.find(doorkeeper_token.resource_owner_id) if doorkeeper_token
    end

    def get_angler
      @angler = User.find(doorkeeper_token.resource_owner_id).angler if doorkeeper_token
    end

    def check_for_valid_user_id

      if  @user.angler_id.nil?
        # the hash to return to the client
        data = Hash.new
        data[:count] = 0
        data[:items] = {}
        data[:messages] = []
        data[:errors] = []

        # check for that the signed in user has a
        data[:errors] << 'No Angler ID associated with this user.'

        render :json => data, :status => :internal_server_error

        false
      end
    end

    def get_partial_capture (params)
      partial = DraftCapture.new
      partial.attributes = params[:capture].reject { |k, v| !partial.attributes.keys.member?(k.to_s) }
    end

    def get_capture (params)
      capture = Capture.new
      capture.attributes = params[:capture].reject { |k, v| !capture.attributes.keys.member?(k.to_s) }

      capture.angler_id = @user.angler_id
      capture.verified = false
      capture.user_capture = true
    end

    def get_unregistered_angler (params)
      a = Angler.where('angler_id ~* \'APP\'').collect { |e| e.angler_id }
      a.each do |e|
        e.gsub!(/[A-Z]/, '')
      end

      angler = Angler.new(params[:angler])
      angler.angler_id = "APP#{ a.to_a.length > 0 ? a.map(&:to_i).max + 1 : 00001}"
    end

    def get_user_from_angler(angler)

    end
  end
end

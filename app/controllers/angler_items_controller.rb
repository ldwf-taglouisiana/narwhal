# == Schema Information
# Schema version: 20180301154137
#
# Table name: angler_items
#
#  id                  :integer          not null, primary key
#  angler_id           :integer          not null
#  item_id             :integer          not null
#  angler_item_type_id :integer          not null
#  requested_at        :datetime
#  fulfilled_at        :datetime
#  comment             :text
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#
# Indexes
#
#  idx_angler_items_angler_id            (angler_id)
#  idx_angler_items_angler_item_type_id  (angler_item_type_id)
#  idx_angler_items_item_id              (item_id)
#
# Foreign Keys
#
#  angler_items_angler_id_fkey            (angler_id => anglers.id)
#  angler_items_angler_item_type_id_fkey  (angler_item_type_id => angler_item_types.id)
#  angler_items_item_id_fkey              (item_id => items.id)
#

class AnglerItemsController < ApplicationController

  include DatatableActionable

  before_action :get_angler_item, :only => [:edit, :update]

  def edit
    authorize! :update, @angler_item
  end

  def update
    authorize! :update, @angler_item
    respond_to do |format|

      if @angler_item.update_attributes(angler_item_params)
        #format.html { redirect_to @angler, notice: 'Angler was successfully updated.' }
      else
        #format.html { redirect_to @angler, error: 'Angler was was not updated.' }
      end
    end
  end

  def index
    authorize! :read, AnglerItem
    respond_to do |format|
      format.html # index.html.erb
      format.js # show.js.erb
      format.json {
        render json: get_angler_item_json_list(params)
      }
    end
  end

  def mark_fulfilled
    authorize! :mark_fulfilled, AnglerItem
    entries = AnglerItem.where(id: params[:id] || params[:options])
    entries.each { |item| item.update_attributes({ fulfilled_at: Time.now}) }
    respond_to do |format|
      format.html {
        redirect_to_url angler_items_path
      }
    end

  end

  private

  def get_angler_item
    @angler_item = AnglerItem.find(params[:id])
  end

  def angler_item_params
    params.require(:angler_item).permit(:angler_id, :item_id, :angler_item_type, :requested_at, :fulfilled_at, :comment)
  end

  def get_angler_item_json_list(params)
    data_params = data_table_params
    data_params[:unsent] = params[:unsent] == 'true'
    AnglerItem.datatable_list(data_params)
  end
end

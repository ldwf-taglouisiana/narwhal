# == Schema Information
# Schema version: 20160405165101
#
# Table name: news_items
#
#  id                :integer          not null, primary key
#  user_id           :integer          not null
#  content           :text             not null
#  should_publish    :boolean          default(FALSE)
#  should_publish_at :datetime
#  title             :string(255)      not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#
# Foreign Keys
#
#  newsitem_user_fk  (user_id => users.id)
#

class NewsItemsController < ApplicationController

  before_action :set_news_item, only: [:show, :edit, :update, :destroy]

  # GET /news_items
  def index
    authorize! :read, NewsItem
    @news_items = NewsItem.all
  end

  # GET /news_items/1
  def show
    authorize! :read, @news_item
  end

  # GET /news_items/new
  def new
    authorize! :create, NewsItem
    @news_item = NewsItem.new
  end

  # GET /news_items/1/edit
  def edit
    authorize! :update, @news_item
  end

  # POST /news_items
  def create
    authorize! :create, NewsItem
    @news_item = NewsItem.new(news_item_params)
    @news_item.user = current_user

    if @news_item.save
      redirect_to news_items_path, success: 'News item was successfully created.'
    else
      render action: 'new'
    end
  end

  # PATCH/PUT /news_items/1
  def update
    authorize! :update, @news_item
    if @news_item.update(news_item_params)
      redirect_to edit_news_item_path(@news_item), success: 'News item was successfully updated.'
    else
      render action: 'edit'
    end
  end

  # DELETE /news_items/1
  def destroy
    authorize! :destroy, @news_item
    @news_item.destroy
    redirect_to news_items_url, success: 'News item was successfully destroyed.'
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_news_item
    @news_item = NewsItem.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def news_item_params
    params.require(:news_item).permit(:content, :title, :should_publish, :should_publish_at)
  end

end

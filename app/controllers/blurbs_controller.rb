# == Schema Information
# Schema version: 20160405165101
#
# Table name: blurbs
#
#  id                     :integer          not null, primary key
#  capture_dashboard      :text
#  new_capture            :text
#  my_tags                :text
#  order_tags             :text
#  find_a_spot            :text
#  common_fish            :text
#  target_species         :text
#  about_program          :text
#  sign_up                :text
#  how_to                 :text
#  licensing              :text
#  volunteer_hours        :text
#  contact_info           :text
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  non_registered_capture :text
#  login                  :text
#  my_captures            :text
#  draft_captures         :text
#  account_info           :text
#  contact_us             :text
#  privacy_policy         :text
#  faq                    :text
#

class BlurbsController < ApplicationController

  before_action :set_blurb, only: [:show, :edit, :update, :destroy]

  # GET /blurbs
  def index
    authorize! :read, Blurb
    @blurbs = Blurb.all
  end

  # GET /blurbs/1
  def show
    authorize! :read, @blurb
  end

  # GET /blurbs/new
  def new
    authorize! :create, Blurb
    @blurb = Blurb.new
  end

  # GET /blurbs/1/edit
  def edit
    authorize! :update, @blurb
  end

  # POST /blurbs
  def create
    authorize! :create, Blurb
    @blurb = Blurb.new(blurbs_params)

    respond_to do |format|
      if @blurb.save
        format.html { redirect_to @blurb, notice: 'Blurb was successfully created.' }
      else
        format.html { render action: "new" }
      end
    end
  end

  # PUT /blurbs/1
  def update
    authorize! :update, @blurb
    respond_to do |format|
      if @blurb.update_attributes(blurbs_params)
        format.html { render action: 'edit' }
      else
        format.html { render action: 'edit' }
      end
    end
  end

  # DELETE /blurbs/1
  def destroy
    authorize! :destroy, @blurb
    @blurb.destroy
    respond_to do |format|
      format.html { redirect_to blurbs_url }
    end
  end

  private

  def set_blurb
    @blurb = Blurb.find(params[:id])
  end

  def blurbs_params
    params.require(:blurb).permit(:about_program, :capture_dashboard, :common_fish, :contact_info, :find_a_spot, :non_registered_capture,
                                   :how_to, :licensing, :my_tags, :new_capture, :order_tags, :sign_up, :target_species,
                                   :volunteer_hours, :login, :my_captures, :draft_captures, :account_info, :contact_us, :faq, :privacy_policy)
  end
end

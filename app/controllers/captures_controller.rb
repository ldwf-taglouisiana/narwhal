# == Schema Information
# Schema version: 20160405165101
#
# Table name: captures
#
#  id                                 :integer          not null, primary key
#  tag_id                             :integer
#  capture_date                       :datetime         not null
#  species_id                         :integer
#  length                             :decimal(, )
#  location_description               :text
#  latitude                           :float
#  longitude                          :float
#  fish_condition_option_id           :integer
#  weight                             :float
#  comments                           :text
#  confirmed                          :boolean          default(FALSE)
#  verified                           :boolean          default(FALSE)
#  deleted                            :boolean
#  mailed_map                         :boolean
#  time_of_day_option_id              :integer
#  created_at                         :datetime         not null
#  updated_at                         :datetime         not null
#  geom                               :geometry
#  entered_gps_type                   :string(255)
#  user_capture                       :boolean          default(FALSE)
#  species_length_id                  :integer
#  basin_id                           :integer
#  sub_basin_id                       :integer
#  gps_format_id                      :integer
#  recent                             :boolean          default(FALSE), not null
#  search_vector                      :tsvector         not null
#  fish_event_location_description_id :integer
#  angler_id                          :integer          not null
#
# Indexes
#
#  cap_tag_fk                                  (tag_id) UNIQUE
#  captures_length_idx                         (length)
#  captures_tag_idx                            (tag_id) UNIQUE
#  idx_captures_created_at                     (created_at)
#  index_captures_geom                         (geom)
#  index_captures_on_fish_condition_option_id  (fish_condition_option_id)
#  index_captures_on_gps_format_id             (gps_format_id)
#  index_captures_on_recent                    (recent)
#  index_captures_on_search_vector             (search_vector)
#  index_captures_on_species_id                (species_id)
#  index_captures_on_species_length_id         (species_length_id)
#  index_captures_on_time_of_day_option_id     (time_of_day_option_id)
#
# Foreign Keys
#
#  cap_fishcond_fk                  (fish_condition_option_id => fish_condition_options.id)
#  cap_species_fk                   (species_id => species.id)
#  cap_time_fk                      (time_of_day_option_id => time_of_day_options.id)
#  captures_basin_id_fk             (basin_id => basins.gid)
#  captures_species_length_id_fkey  (species_length_id => species_lengths.id)
#  captures_sub_basin_id_fk         (sub_basin_id => "sub-basin".gid)
#  fk_captures_gps_type             (entered_gps_type => gps_formats.format_type)
#  fk_rails_64432fd973              (gps_format_id => gps_formats.id)
#  fk_rails_906df65cfe              (angler_id => anglers.id)
#

require 'fileutils'

class CapturesController < ApplicationController
  include QueueWorkersModule, FuzzyMapModule, CaptureSpreadsheetModule,
          DatatableActionable, FishEntryActionable, DownloadableActionable

  skip_before_filter :authenticate_user!, :only => :report
  skip_authorization_check :only => [:report]


  before_action :set_capture, only: [:show, :edit, :update, :destroy]

  # GET /captures
  # GET /captures.json
  def index
    authorize! :read, Capture
    respond_to do |format|
      format.html # index.html.erb
      format.js # show.js.erb
      format.json {
        render json: get_capture_json_list(params)
      }
    end
  end

  def resource_class
    Capture
  end

  # GET /captures/1
  # GET /captures/1.json
  def show
    authorize! :read, @capture
  end

  # GET /captures/new
  # GET /captures/new.json
  def new
    authorize! :create, Capture
    @capture = Capture.new(params_from_form_redirect.try(:except, :recapture_disposition_id))
  end

  # GET /captures/1/edit
  def edit
    authorize! :update, @capture
  end

  # POST /captures
  # POST /captures.json
  def create
    authorize! :create, Capture
    @capture = Capture.new(fish_entry_params)

    respond_to do |format|
      format.html {
        if @capture.save
          warn = nil

          # if the create is called, then we redirect to the show page
          if params[:create].present?

            flash[:warn] = 'There are existing recaptures for this new capture. Please send new recapture reports' if @capture.recaptures.any?
            flash[:success] = 'Capture was created'
            redirect_to capture_path(@capture)

          elsif params[:continue].present?
            flash[:warn] = "There are existing recaptures for this new capture. Please send new recapture #{view_context.link_to 'reports', capture_path(@capture), target: :blank}".html_safe  if @capture.recaptures.any?
            flash[:success] = "Capture for #{@capture.tag.tag_no} created!<br>Can be updated #{view_context.link_to 'here', capture_path(@capture), target: :blank}.".html_safe

            # create and continue was selected
            @capture = Capture.new(@capture.attributes.select_keys(:capture_date, :tag_id, :time_of_day_option_id, :fish_condition_option_id, :latitude, :longitude, :length, :species_length_id, :comments, :location_description, :entered_gps_type))
            @capture.tag = Tag.for_capture_use.where('tag_no > ?', @capture.tag.tag_no).order_by_number.limit(1).first
            @capture.angler_id = @capture.tag.angler_id

            render action: :new
          else
            raise 'Unexpected "submit" parameter'
          end

        else
          render action: :new
        end
      }
    end

  end

  # PUT /captures/1
  # PUT /captures/1.json
  def update
    authorize! :update, @capture
    respond_to do |format|
      format.html {
        if @capture.update_attributes(fish_entry_params)
          redirect_to @capture, notice: 'Capture was successfully updated.'
        else
          render action: 'edit'
        end
      }
    end
  end

  # DELETE /captures/1
  # DELETE /captures/1.json
  def destroy
    authorize! :destroy, @capture
    @capture.destroy!

    redirect_to captures_path, notice: 'Capture was successfully destroyed.'
  end


  def import
    authorize! :create, Capture
  end

  # Method to format an uploaded spreadsheet to prepare it to be uploaded via Capture's import method
  def import_commit
    authorize! :create, Capture
    flash_result = {}

    # If the file given is not nil, and it has a .xlsx extension, import the captures from that file
    if params[:file].present? && File.extname(params[:file].original_filename).eql?('.xlsx')
      tmp = params[:file].tempfile
      file = File.join("public", params[:file].original_filename)
      FileUtils.cp tmp.path, file
      result = CaptureSpreadsheet.import(file)
      FileUtils.rm file

      result[:error].nil? ?
          flash_result[:success] = 'Captures were imported successfully' :
          flash_result[:error] = 'Error: ' + result[:error]

    else
      flash_result[:error] = 'Spreadsheet import failure. Please enter a .xlsx file to import.'
    end

    respond_to do |format|
      format.html {
        redirect_to import_captures_path, flash_result
      }
    end
  end

  def should_send_report
    authorize! :read, Capture
    tag = Tag.where('tag_no = ?', [params[:tag_no]]).first
    result = Hash.new

    if (tag != nil) and Recapture.where('tag_id = ?', tag.id).first
      result[:result] = true
    else
      result[:result] = false
    end

    render :json => result
  end


  def download
    # create the token
    authorize! :report, Capture
    file_token = generate_download_token

    # create the download item with the token, the filename, and the id of the user that created it
    RequestedDownload.create!(token: file_token, filename: 'captures.xlsx', user_id: current_user.id)
    DownloadIndexDataJob.perform_later(DownloadIndexDataJob::REPORT_TYPES[:capture], params, file_token)

    render json: { token: file_token }
  end

  private

  def set_capture
    @capture = Capture.find(params[:id])
  end

  def get_capture_json_list(params)
    data_params = data_table_params
    data_params[:verified] = (params[:verified] == 'true')

    # get the data from the capture model
    Capture.datatable_list(data_params)
  end
end

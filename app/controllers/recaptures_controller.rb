# == Schema Information
# Schema version: 20160405165101
#
# Table name: recaptures
#
#  id                                 :integer          not null, primary key
#  capture_date                       :datetime         not null
#  length                             :decimal(, )
#  location_description               :text
#  latitude                           :float
#  longitude                          :float
#  comments                           :text
#  verified                           :boolean          default(FALSE)
#  user_capture                       :boolean          default(FALSE)
#  entered_gps_type                   :string(255)
#  tag_id                             :integer
#  species_id                         :integer
#  fish_condition_option_id           :integer
#  recapture_disposition_id           :integer
#  time_of_day_option_id              :integer
#  created_at                         :datetime         not null
#  updated_at                         :datetime         not null
#  geom                               :geometry(Point,4
#  species_length_id                  :integer
#  basin_id                           :integer
#  sub_basin_id                       :integer
#  gps_format_id                      :integer
#  capture_id                         :integer
#  recent                             :boolean          default(TRUE), not null
#  search_vector                      :tsvector         not null
#  fish_event_location_description_id :integer
#  angler_id                          :integer          not null
#
# Indexes
#
#  idx_recaptures_created_at                     (created_at)
#  index_recaptures_on_capture_id                (capture_id)
#  index_recaptures_on_fish_condition_option_id  (fish_condition_option_id)
#  index_recaptures_on_gps_format_id             (gps_format_id)
#  index_recaptures_on_recent                    (recent)
#  index_recaptures_on_search_vector             (search_vector)
#  index_recaptures_on_species_id                (species_id)
#  index_recaptures_on_species_length_id         (species_length_id)
#  index_recaptures_on_tag_id                    (tag_id)
#  index_recaptures_on_time_of_day_option_id     (time_of_day_option_id)
#  recaptures_date_idx                           (capture_date)
#  recaptures_geom_idx                           (geom)
#  recaptures_length_idx                         (length)
#  recaptures_tag_idx                            (tag_id)
#
# Foreign Keys
#
#  fk_rails_24510c6ae4         (capture_id => captures.id)
#  fk_rails_45858b5865         (gps_format_id => gps_formats.id)
#  fk_rails_782717f62d         (angler_id => anglers.id)
#  fk_recaptures_gps_type      (entered_gps_type => gps_formats.format_type)
#  recap_fish_cond_fk          (fish_condition_option_id => fish_condition_options.id)
#  recap_recap_dis_fk          (recapture_disposition_id => recapture_dispositions.id)
#  recap_species_fk            (species_id => species.id)
#  recap_tag_fk                (tag_id => tags.id)
#  recap_time_day_fk           (time_of_day_option_id => time_of_day_options.id)
#  recaptures_basin_id_fk      (basin_id => basins.gid)
#  recaptures_sub_basin_id_fk  (sub_basin_id => "sub-basin".gid)
#

class RecapturesController < ApplicationController
  include QueueWorkersModule, DatatableActionable, FishEntryActionable, DownloadableActionable

  before_action :set_recapture, only: [:show, :edit, :update, :destroy]

  skip_before_filter :authenticate_user!, :only => :report
  skip_authorization_check :only => [:report]


  def resource_class
    Recapture
  end

  # GET /recaptures
  def index
    authorize! :read, Recapture
    respond_to do |format|
      format.html {
        @recaptures = Recapture.all
      }
      format.js # index.html.erb
      format.json {
        render json: get_recapture_json_list(params)
      }
    end
  end

  # GET /recaptures/1
  def show
    authorize! :read, @recapture
  end

  # GET /recaptures/new
  def new
    authorize! :create, Recapture
    @recapture = Recapture.new(params_from_form_redirect)
  end

  # GET /recaptures/1/edit
  def edit
    authorize! :update, @recapture
  end

  # POST /recaptures
  def create
    authorize! :create, Recapture
    @recapture = Recapture.new(fish_entry_params)

    respond_to do |format|
      format.html {
        if @recapture.save
          warn = nil
          warn = "There is an existing capture for this new recapture. Please send a new recapture #{view_context.link_to 'report', capture_path(@recapture.capture), target: :blank}".html_safe  if @recapture.capture

          # if the create is called, then we redirect to the show page
          if params[:create].present?
            flash[:warn] = warn unless warn.nil?
            flash[:success] = 'Recapture was created'
            redirect_to @recapture

          elsif params[:continue].present?
            flash[:success] = "Recapture for #{@recapture.tag.tag_no} created!<br>Can be updated #{view_context.link_to 'here', recapture_path(@recapture), target: :blank}.".html_safe

            redirect_to new_capture_path

          else
            raise 'Unexpected "submit" parameter'

          end

        else
          render action: :new
        end
      }
    end
  end

  # PATCH/PUT /recaptures/1
  def update
    authorize! :update, @recapture
    if @recapture.update(fish_entry_params)
      redirect_to @recapture, notice: 'Recapture was successfully updated.'
    else
      render action: 'edit'
    end
  end

  # DELETE /recaptures/1
  def destroy
    authorize! :destroy, @recapture
    @recapture.destroy!
    redirect_to captures_path, notice: 'Recapture was successfully destroyed.'
  end

  def download
    authorize! :report, Recapture
    # create the token
    file_token = generate_download_token

    # create the download item with the token, the filename, and the id of the user that created it
    RequestedDownload.create!(token: file_token, filename: 'recaptures.xlsx', user_id: current_user.id)
    DownloadIndexDataJob.perform_later(DownloadIndexDataJob::REPORT_TYPES[:recapture], params, file_token)

    render json: { token: file_token }
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_recapture
    @recapture = Recapture.find(params[:id])
  end

  def get_recapture_json_list(params)
    data_params = data_table_params
    data_params[:verified] = (params[:verified] == 'true')

    # get the data from the capture model
    Recapture.datatable_list(data_params)
  end
end

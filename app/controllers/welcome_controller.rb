class WelcomeController < ApplicationController
  include DashboardModule

  before_filter :authenticate_user!
  skip_authorization_check

  def index
    @data = {}
    #@data[:users] = User.accessible_by_user(current_user,:read).includes(:angler).visible_app.where(verified: false)
    @data[:items] = AnglerItem.accessible_by_user(current_user,:read)

  end

end

# == Schema Information
# Schema version: 20160405165101
#
# Table name: user_tag_requests
#
#  id             :integer          not null, primary key
#  number_of_tags :integer          not null
#  fulfilled      :boolean          default(FALSE)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  tag_type       :string(255)      not null
#  uuid           :string
#  angler_id      :integer          not null
#
# Foreign Keys
#
#  fk_rails_e812ee6b36  (angler_id => anglers.id)
#

class UserTagRequestsController < ApplicationController

  before_action :set_user_tag_request, :only => [:edit,:update,:show,:destroy]

  include DatatableActionable
  # GET /user_tag_requests
  # GET /user_tag_requests.json
  def index
    authorize! :read, UserTagRequest
    respond_to do |format|
      format.html # index.html.erb
      format.js
      format.json { render json: get_tag_lot_json_list(params) }
    end
  end

  # GET /user_tag_requests/1
  # GET /user_tag_requests/1.json
  def show
    authorize! :read, @user_tag_request
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @user_tag_request }
    end
  end

  # GET /user_tag_requests/new
  # GET /user_tag_requests/new.json
  def new
    authorize! :create, UserTagRequest
    @user_tag_request = UserTagRequest.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @user_tag_request }
    end
  end

  # GET /user_tag_requests/1/edit
  def edit
    authorize! :update, @user_tag_request
  end

  # POST /user_tag_requests
  # POST /user_tag_requests.json
  def create
    authorize! :create, UserTagRequest
    @user_tag_request = UserTagRequest.new(user_tag_requests_params)

    respond_to do |format|
      if @user_tag_request.save
        format.html { redirect_to @user_tag_request, notice: 'User tag request was successfully created.' }
        format.json { render json: @user_tag_request, status: :created, location: @user_tag_request }
      else
        format.html { render action: "new" }
        format.json { render json: @user_tag_request.errors, status: :unprocessable_entity }
      end
    end
  end

  def update_fulfilled
    @tag_request = UserTagRequest.find(params[:id])

    authorize! :update, @tag_request
    params[:fulfilled] = params[:fulfilled].nil? ? false : params[:fulfilled]


    respond_to do |format|
      if @tag_request.update_attributes(:fulfilled => params[:fulfilled])
        format.js
      else
        format.js
      end
    end
  end

  # PUT /user_tag_requests/1
  # PUT /user_tag_requests/1.json
  def update
    authorize! :update, @user_tag_request
    respond_to do |format|
      if @user_tag_request.update_attributes(user_tag_requests_params)
        format.html { redirect_to @user_tag_request, notice: 'User tag request was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @user_tag_request.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /user_tag_requests/1
  # DELETE /user_tag_requests/1.json
  def destroy
    authorize! :destroy, @user_tag_request
    @user_tag_request.destroy

    respond_to do |format|
      format.html { redirect_to user_tag_requests_url }
      format.json { head :no_content }
    end
  end

  private

  def user_tag_requests_params
    params.require(:user_tag_request).permit(:angler_id, :number_of_tags, :fulfilled, :tag_type)
  end

  def set_user_tag_request
    @user_tag_request = UserTagRequest.find(params[:id])
  end

  def get_tag_lot_json_list(params)
    util_params = {where: nil}
    if params[:fulfilled] == 'true'
      util_params[:where] = 'fulfilled is true'
    else
      util_params[:where] = 'fulfilled is not true'
    end


    # get the data from the capture model
    UserTagRequest.get_data_tables_list(data_table_params.merge(util_params))

  end

end

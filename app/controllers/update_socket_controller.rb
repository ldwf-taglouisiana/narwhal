class UpdateSocketController < WebsocketRails::BaseController

  def update

    (total_captures_query = '') << <<-sql
        select row_to_json(d)
        from (
          SELECT COUNT(count) as count
          FROM (
            SELECT distinct on (tag_id) COUNT(*) as count
              FROM captures
              WHERE recapture is not true
              GROUP BY tag_id
          ) a
        ) d
    sql

    (total_recaptures_query = '') << <<-sql
        select row_to_json(d)
        from (
          SELECT COUNT(count) as count
          FROM (
            SELECT distinct on (tag_id) COUNT(*) as count
              FROM captures
              WHERE recapture is true
              GROUP BY tag_id
          ) a
        ) d
    sql

    (total_captures_season_query = '') << <<-sql
        select row_to_json(d)
        from (
          SELECT COUNT(count) as count
          FROM (
            SELECT distinct on (tag_id) COUNT(*) as count
              FROM captures
              WHERE recapture is not true
              AND capture_date < now() AND capture_date >= '#{Chronic.parse("last July 1").beginning_of_day.utc}'
              GROUP BY tag_id
          ) a
        ) d
    sql

    (total_recaptures_season_query = '') << <<-sql
        select row_to_json(d)
        from (
          SELECT COUNT(count) as count
          FROM (
            SELECT distinct on (tag_id) COUNT(*) as count
              FROM captures
              WHERE recapture is true
              AND capture_date < now() AND capture_date >= '#{Chronic.parse("last July 1").beginning_of_day.utc}'
              GROUP BY tag_id
          ) a
        ) d
    sql

    (recaptures_by_species_query = '') << <<-sql
        select array_to_json(array_agg(row_to_json(d)))
        from (
          WITH species_list AS (
	          SELECT (CASE WHEN species.target = true THEN species.common_name  ELSE 'Other' END) as common_name
            FROM captures inner join species on captures.species_id = species.id
            WHERE capture_date BETWEEN (now() - '30 days'::interval)::date AND now()
              AND recapture is true
          )
          SELECT common_name, count(*) as species_count
          FROM species_list
          GROUP BY common_name
          ORDER BY species_count DESC
        ) d
    sql

    (captures_by_species_query = '') << <<-sql
        select array_to_json(array_agg(row_to_json(d)))
        from (
          WITH species_list AS (
	          SELECT (CASE WHEN species.target = true THEN species.common_name  ELSE 'Other' END) as common_name
            FROM captures inner join species on captures.species_id = species.id
            WHERE capture_date BETWEEN (now() - '30 days'::interval)::date AND now()
              AND recapture is not true
          )
          SELECT common_name, count(*) as species_count
          FROM species_list
          GROUP BY common_name
          ORDER BY species_count DESC
        ) d
    sql

    (sql_query = '') << <<-sql
        select row_to_json(t) as "data"
        from (
          select
              (#{total_captures_query})                    as "total_captures",
              (#{total_recaptures_query})                  as "total_recaptures",
              (#{total_captures_season_query})             as "total_captures_season",
              (#{total_recaptures_season_query})           as "total_recaptures_season",
              (#{recaptures_by_species_query})             as "recaptures_by_species",
              (#{captures_by_species_query})               as "captures_by_species"
        ) t
    sql

    connection = ActiveRecord::Base.connection()

    send_message :data, connection.select_value(sql_query).to_json
  end
end

# == Schema Information
# Schema version: 20180301154137
#
# Table name: scratch_captures
#
#  id                       :integer          not null, primary key
#  capture_date             :datetime
#  length                   :decimal(, )
#  location_description     :text
#  latitude                 :float
#  longitude                :float
#  comments                 :text
#  tag_id                   :integer
#  tag_number               :text
#  species_id               :integer
#  fish_condition_option_id :integer
#  recapture_disposition_id :integer
#  time_of_day_option_id    :integer
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  geom                     :geometry(Point,4
#  basin_id                 :integer
#  sub_basin_id             :integer
#  gps_format_id            :integer
#  angler_name              :text
#  angler_number            :text
#  angler_id                :integer
#  scratch_capture_type_id  :integer          not null
#  recapture_id             :integer
#
# Foreign Keys
#
#  scratch_captures_angler_id_fkey                 (angler_id => anglers.id)
#  scratch_captures_basin_id_fkey                  (basin_id => basins.gid)
#  scratch_captures_fish_condition_option_id_fkey  (fish_condition_option_id => fish_condition_options.id)
#  scratch_captures_gps_format_id_fkey             (gps_format_id => gps_formats.id)
#  scratch_captures_recapture_disposition_id_fkey  (recapture_disposition_id => recapture_dispositions.id)
#  scratch_captures_recapture_id_fkey              (recapture_id => recaptures.id)
#  scratch_captures_scratch_capture_type_id_fkey   (scratch_capture_type_id => scratch_capture_types.id)
#  scratch_captures_species_id_fkey                (species_id => species.id)
#  scratch_captures_sub_basin_id_fkey              (sub_basin_id => "sub-basin".gid)
#  scratch_captures_tag_id_fkey                    (tag_id => tags.id)
#  scratch_captures_time_of_day_option_id_fkey     (time_of_day_option_id => time_of_day_options.id)
#

class ScratchCapturesController < ApplicationController
  include DatatableActionable

  before_action :set_scratch_capture, except: [:index, :new, :create]

  def index
    authorize! :read, ScratchCapture


    respond_to do |format|
      format.xlsx
      format.html { @scratch_captures = ScratchCapture.accessible_by_user(current_user).displayables }
      format.json { render json: get_scratch_capture_json }
    end
  end

  def show
    authorize! :read, @scratch_capture
  end

  def new
    authorize! :create, ScratchCapture
    @scratch_capture = ScratchCapture.new
  end

  def create
    authorize! :create, ScratchCapture
    @scratch_capture = ScratchCapture.new(scratch_capture_params)
    if @scratch_capture.save
      redirect_to @scratch_capture
    else
      render action: :new, error: 'There was an error saving the scratch capture.'
    end

  end

  def edit
    authorize! :update, @scratch_capture
  end

  def update
    authorize! :update, @scratch_capture
    if @scratch_capture.update_attributes(scratch_capture_params)
      redirect_to @scratch_capture, notice: 'Changes Saved'
    else
      redirect_to @scratch_capture, error: 'Error Occurred'
    end

  end

  def convert_to_recapture
    authorize! :update, @scratch_capture
    if @scratch_capture.manual_conversion[:errors].nil?
      redirect_to scratch_captures_path, notice: 'Changes Saved'
    else
      redirect_to edit_scratch_capture_path(@scratch_capture), notice: @scratch_capture.manual_conversion[:errors].join('<br/>').html_safe
    end
  end

  def resource_class
    ScratchCapture
  end

  def geo_json
    authorize! :index, self.resource_class
    @items = controller_name.classify.constantize
               .where(id: params[:id])
               .where.not(geom: nil)

    # if there are any items then render the geo json response
    if @items.any?
      render template: 'fish_entry_common/geo_json'

    else # if there were no items, then return a null data element
      render json: { data: nil }

    end

  end

  private

  def set_scratch_capture
    @scratch_capture = ScratchCapture.find(params[:id])
  end

  def scratch_capture_params
    new_params = params.require(:scratch_capture).permit(:id, :capture_date, :length, :location_description,
                                            :latitude, :longitude, :comments, :tag_id, :tag_number, :species_id,
                                            :fish_condition_option_id, :recapture_disposition_id,
                                            :time_of_day_option_id, :geom, :basin_id, :sub_basin_id,
                                            :gps_format, :angler_number, :angler_name)

    new_params[:gps_format] = GPSFormat.where(format_type: new_params[:gps_format]).first
    new_params
  end

  def get_scratch_capture_json
    # get the data from the capture model
    ScratchCapture.datatable_list(data_table_params)
  end

end

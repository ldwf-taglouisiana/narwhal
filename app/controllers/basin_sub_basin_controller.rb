class BasinSubBasinController < ApplicationController

  def get_sub_basins
    authorize! :read, SubBasin
    @sub_basins = SubBasin.joins(:basin)
                      .where(basins: { gid: params[:basin_gid] })
                      .select(['"sub-basin".*', 'basins.gid as basin_gid', 'ST_AsGeoJSON("sub-basin".geom)::json AS geo_json'])
  end

  def basins
    authorize! :read, Basin
    @basins = Basin.all.select(['"basins".*','ST_AsGeoJSON("basins".geom)::json AS geo_json'])
  end

end
# == Schema Information
# Schema version: 20160405165101
#
# Table name: front_page_photo_stream_items
#
#  id         :integer          not null, primary key
#  featured   :boolean          default(FALSE)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class FrontPagePhotoStreamItemsController < ApplicationController

  before_action :set_front_page_photo_stream_item, only: [:show, :edit, :update, :destroy]

  # GET /front_page_photo_stream_items
  def index
    authorize! :read, FrontPagePhotoStreamItem
    @front_page_photo_stream_items = FrontPagePhotoStreamItem.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @front_page_photo_stream_items.map{|item| item.to_jq_upload } }
    end
  end

  # GET /front_page_photo_stream_items/1
  def show
    authorize! :read, @front_page_photo_stream_item
  end

  # GET /front_page_photo_stream_items/new
  def new
    authorize! :create, FrontPagePhotoStreamItem
    @front_page_photo_stream_item = FrontPagePhotoStreamItem.new
  end

  # GET /front_page_photo_stream_items/1/edit
  def edit
    authorize! :update, @front_page_photo_stream_item
  end

  # POST /front_page_photo_stream_items
  def create
    authorize! :create, FrontPagePhotoStreamItem
    saved = false
    ActiveRecord::Base.transaction do
      photo = Photo.create!(:image => front_page_photo_stream_item_params[:attachment64])
      @upload = FrontPagePhotoStreamItem.new(front_page_photo_stream_item_params.except(:attachment64))
      @upload.photo = photo
      saved = @upload.save!
    end

    respond_to do |format|
      if saved
        format.html {
          #can this be done better? Maybe make the form remote true and refresh the relevant info on the page?
          redirect_to '/photos'
          # render :json => [@upload.to_jq_upload].to_json,
          #        :content_type => 'text/html',
          #        :layout => false
        }
        format.json { render json: {files: [@upload.to_jq_upload]}, status: :created, location: @upload.photo.image.url }
      else
        format.html { render action: 'new' }
        format.json { render json: @upload.errors, status: :unprocessable_entity }
      end
    end

  end

  # PATCH/PUT /front_page_photo_stream_items/1
  def update
    authorize! :update, FrontPagePhotoStreamItem
    if @front_page_photo_stream_item.update(front_page_photo_stream_item_params)
      redirect_to @front_page_photo_stream_item, notice: 'Front page photo stream item was successfully updated.'
    else
      render action: 'edit'
    end
  end

  # DELETE /front_page_photo_stream_items/1
  def destroy
    authorize! :destroy, FrontPagePhotoStreamItem
    @front_page_photo_stream_item.destroy

    respond_to do |format|
      format.html {
        redirect_to front_page_photo_stream_items_url, notice: 'Front page photo stream item was successfully destroyed.'
      }
      format.json {
        render json: '{}'
      }
      format.js {
        render  :js => "$('form##{@front_page_photo_stream_item.id}').remove();"
      }
    end

  end

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_front_page_photo_stream_item
      begin
        @front_page_photo_stream_item = FrontPagePhotoStreamItem.find(params[:id])
      rescue ActiveRecord::RecordNotFound
        @front_page_photo_stream_item = FrontPagePhotoStreamItem.new
      end
    end

    # Only allow a trusted parameter "white list" through.
    def front_page_photo_stream_item_params
      params[:front_page_photo_stream_item].permit(:featured, :attachment64)
    end
end

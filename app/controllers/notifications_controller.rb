# == Schema Information
# Schema version: 20160405165101
#
# Table name: notifications
#
#  id                     :integer          not null, primary key
#  initiator_id           :integer
#  checked                :boolean          default(FALSE)
#  notification_item_id   :integer          not null
#  notification_item_type :string(255)      not null
#  notification_topic_id  :integer          not null
#  user_id                :integer          not null
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#
# Foreign Keys
#
#  notification_initiator_fk  (initiator_id => users.id)
#  notification_topic_fk      (notification_topic_id => notification_topics.id)
#  notification_user_fk       (user_id => users.id)
#

class NotificationsController < ApplicationController

  before_action :set_notification, :only => [:show,:edit,:update,:destroy]

  # GET /notifications
  # GET /notifications.json
  def index
    authorize! :read, Notification
    @notifications = current_user.notifications.active.page params[:page]

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @notifications }
      format.js
    end
  end

  # GET /notifications/1
  # GET /notifications/1.json
  def show
    authorize! :read, @notification

    # mark the @notification as checked
    @notification.checked = true
    @notification.save!

    @notifications = current_user.notifications.active.page params[:page]
    @path = nil

    case @notification.kind
      when :capture
        @path = capture_path(@notification.notification_item)
      when :unregistered_recapture, :recapture
        @path = recapture_path(@notification.notification_item)
      when :user_tag_request
        @path = edit_user_tag_request(@notification.notification_item)
      when :user
        @path = edit_user_path(@notification.notification_item_id)
    end

    respond_to do |format|
      format.js
      format.html {
        redirect_to @path
      }
      format.json {
        render json: @notification
      }
    end
  end

  # GET /notifications/new
  # GET /notifications/new.json
  def new
    authorize! :create, Notification
    @notification = Notification.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @notification }
    end
  end

  # GET /notifications/1/edit
  def edit
    authorize! :update, @notification
  end

  # POST /notifications
  # POST /notifications.json
  def create
    authorize! :create, Notification
    @notification = Notification.new(params[:notification])

    respond_to do |format|
      if @notification.save
        format.html { redirect_to @notification, notice: 'Notification was successfully created.' }
        format.json { render json: @notification, status: :created, location: @notification }
      else
        format.html { render action: "new" }
        format.json { render json: @notification.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /notifications/1
  # PUT /notifications/1.json
  def update
    authorize! :update, @notification
    respond_to do |format|
      if @notification.update_attributes(params[:notification])
        format.html { redirect_to @notification, notice: 'Notification was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @notification.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /notifications/1
  # DELETE /notifications/1.json
  def destroy
    authorize! :destroy, @notification
    @notification.checked = true
    @notification.save!

    @notifications = current_user.notifications.active.page params[:page]

    respond_to do |format|
      format.html { redirect_to notifications_url }
      format.json { head :no_content }
      format.js
    end
  end

  def destroy_all
    authorize! :destroy, Notification
    current_user.notifications.active.update_all(checked: true)
    @notifications = current_user.notifications.active.page params[:page]
  end

  private

    def set_notification
      @notification = Notification.find(params[:id])
    end

end

# == Schema Information
# Schema version: 20160405165101
#
# Table name: photos
#
#  id                 :integer          not null, primary key
#  image_file_name    :string(255)
#  image_content_type :string(255)
#  image_file_size    :integer
#  image_updated_at   :datetime
#  imageable_id       :integer
#  imageable_type     :string(255)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

class PhotosController < ApplicationController

  before_action :set_photo, only: [:show, :edit, :update, :destroy]

  # GET /photos
  def index
    authorize! :read, Photo
    @front_page_photo_stream_items = FrontPagePhotoStreamItem.all
    @photos = Photo.all
  end

  # GET /photos/1
  def show
    authorize! :read, @photo
    style = params[:style] ? params[:style] : 'original'
    send_file @photo.image.path(style), type: @photo.image_content_type, disposition: 'inline'
  end

  # GET /photos/new
  def new
    authorize! :create, Photo
    @photo = Photo.new
  end

  # GET /photos/1/edit
  def edit
    authorize! :update, @photo
  end

  # POST /photos
  def create
    authorize! :create, Photo
    @photo = Photo.new(photo_params)

    if @photo.save
      redirect_to @photo, notice: 'Photo was successfully created.'
    else
      render action: 'new'
    end
  end

  # PATCH/PUT /photos/1
  def update
    authorize! :update, @photo
    if @photo.update(photo_params)
      redirect_to @photo, notice: 'Photo was successfully updated.'
    else
      render action: 'edit'
    end
  end

  # DELETE /photos/1
  def destroy
    authorize! :destroy, @photo
    @photo.destroy
    redirect_to photos_url, notice: 'Photo was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_photo
      @photo = Photo.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def photo_params
      params[:photo]
    end
end

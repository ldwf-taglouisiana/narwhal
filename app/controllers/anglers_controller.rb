# == Schema Information
# Schema version: 20170724200101
#
# Table name: anglers
#
#  id                         :integer          not null, primary key
#  lag_no                     :integer
#  law_no                     :integer
#  first_name                 :string(255)
#  last_name                  :string(255)
#  street                     :string(255)
#  suite                      :string(255)
#  city                       :string(255)
#  state                      :string(255)
#  zip                        :string(255)
#  phone_number_1             :string(255)
#  phone_number_1_type        :string(255)
#  phone_number_2             :string(255)
#  phone_number_2_type        :string(255)
#  phone_number_3             :string(255)
#  phone_number_3_type        :string(255)
#  phone_number_4             :string(255)
#  phone_number_4_type        :string(255)
#  email                      :string(255)
#  tag_end_user_type          :integer
#  shirt_size                 :integer
#  deleted                    :boolean
#  lax_no                     :integer
#  email_2                    :string(255)
#  user_name                  :string(255)
#  comments                   :text
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  angler_id                  :string(20)       not null
#  search_vector              :tsvector         not null
#  prefered_incentive_item_id :integer
#
# Indexes
#
#  angler_id_uniq                                           (id) UNIQUE
#  index_anglers_on_angler_id_and_first_name_and_last_name  (angler_id,first_name,last_name)
#  index_anglers_on_search_vector                           (search_vector)
#
# Foreign Keys
#
#  ang_shirt_fk                             (shirt_size => shirt_size_options.id)
#  ang_tag_fk                               (tag_end_user_type => tag_end_user_types.id)
#  anglers_prefered_incentive_item_id_fkey  (prefered_incentive_item_id => items.id)
#

class AnglersController < ApplicationController
  include AnglerDataMigrationModule, DatatableActionable, DownloadableActionable
  #gross thing we need to do to authorize the autocomplete actions
  before_action :auth_autocomplete, only: [:autocomplete_angler_angler_id]

  autocomplete :angler, :angler_id, :display_value => :autocomplete_display, :selected_value => :angler_id, extra_data: [:full_name, :id], :cascade => true, :search_action => :autocomplete

  before_action :set_angler, only: [:show, :edit, :update, :destroy]
  skip_before_action :verify_authenticity_token, only: [:perform_merge]

  # GET /anglers
  # GET /anglers.json
  # GET /anglers.xlsx
  def index
    authorize! :read, Angler


    respond_to do |format|
      format.xlsx
      format.html {@anglers = params[:all].present? ? Angler.all : Angler.order(:id).page(params[:page])}
      format.json { render json: get_angler_json_list }
    end
  end

  # GET /anglers/1
  def show
    authorize! :read, @angler
  end

  # GET /anglers/new
  def new
    authorize! :create, Angler
    @angler = Angler.new
  end

  # GET /anglers/1/edit
  def edit
    authorize! :update, @angler
  end

  # POST /anglers
  def create
    need_welcome_kit = ActiveRecord::Type::Boolean.new.type_cast_from_database(params[:should_create_welcome_kit])
    authorize! :create, Angler
    @angler = Angler.builder(angler_params)

    respond_to do |format|
      if @angler.save
        AnglerItem.create(angler: @angler, requested_at: Time.now, item: Item.tagging_kit, angler_item_type: AnglerItemType.welcome_package) if need_welcome_kit
        format.html { redirect_to @angler, notice: 'Angler was successfully created.' }
        format.js
      else
        format.html { render action: :new }
        format.js
      end
    end
  end

  # PUT /anglers/1
  def update
    authorize! :update, @angler
    respond_to do |format|
      # delete the old angler id from the session
      session.delete(:old_angler_id)
        if @angler.update_attributes(angler_params)
          format.html { redirect_to @angler, notice: 'Angler was successfully updated.' }
        else
          format.html { redirect_to @angler, error: 'Angler was was not updated.' }
        end
    end
  end

  # DELETE /anglers/1
  def destroy
    authorize! :destroy, @angler
    respond_to do |format|
      if @angler.destroy
        format.html { redirect_to anglers_path }
      else
        format.html { redirect_to angler_path(@angler), error: 'Could not delete angler' }
      end
    end
  end

  def search
    authorize! :read, Angler
    @anglers = Angler.where('first_name ~* ? AND last_name ~* ? and angler_id ~* ?', params[:angler][:first_name], params[:angler][:last_name], params[:angler][:angler_id]).order('angler_id')
    @search_count = @anglers.count
    @anglers = @anglers.page params[:page]
    respond_to do |format|
      format.html { render action: :index }
    end
  end

  # GET /anglers/download.json
  def download
    authorize! :report, Angler
    # create the token
    file_token = generate_download_token

    # create the download item with the token, the filename, and the id of the user that created it
    RequestedDownload.create!(token: file_token, filename: 'anglers.xlsx', user_id: current_user.id)
    DownloadIndexDataJob.perform_later(DownloadIndexDataJob::REPORT_TYPES[:angler], params, file_token)

    render json: { token: file_token }
  end

  # GET /anglers/duplicates
  def find_duplicates
    authorize! :read, Angler
    @anglers = Angler.duplicates
  end

  # GET /anglers/duplicates/with_name
  # GET /anglers/duplicates/with_ids
  def duplicate_anglers
    authorize! :read, Angler
    if params[:angler_ids]
      @anglers = Angler.where(angler_id: params[:angler_ids])
    else
      @anglers = Angler.where('first_name ~* ? AND last_name ~* ?', params[:first_name], params[:last_name])
    end
  end

  # POST /anglers/change_angler_id
  def change_angler_id
    authorize! :update, Angler
    success = true
    Angler.transaction do
      begin
        angler = Angler.from_ambiguous_id(params[:original_angler_id])
        angler = Angler.new(angler.attributes.except('id'))
        angler.angler_id = params[:new_angler_id].upcase
        angler.save!

        AnglerDataMigration.from_angler_to_angler!(params[:original_angler_id], params[:new_angler_id], true, current_user.id)
      rescue Exception => e
        success = false
        puts e.message
      end
    end

    respond_to do |format|
      format.html {
        if success
          flash[:notice] = 'Angler updated successfully'
          redirect_to Angler.from_ambiguous_id(params[:new_angler_id])
        else
          flash[:error] = 'There was a problem updating the Angler.'
          redirect_to Angler.from_ambiguous_id(params[:original_angler_id])
        end
      }
    end

  end

  # Returns the canonical angler that will be the remaining angler after a merge is performed and
  # duplicates are deleted.
  # GET /anglers/merge/canonical_angler
  def canonical_angler
    authorize! :read, Angler
    anglers = params[:angler_ids].map { |a| Angler.from_ambiguous_id(a) }
    canonical_angler = anglers.first

    #this just loops through the other anglers associated with this guy and fills his info with the other duplicates' info
    # if he is missing that information
    anglers.each do |angler|
      angler.attributes.each do |key, value|
        # if the canonical angler attribute that matched the key is nil and the value of the current angler
        # is not nil, then set the attribute if the canonical angler
        if canonical_angler.send(key.to_sym) == nil && value
          canonical_angler.send("#{key}=".to_sym, value)
        end
      end
    end

    render json: canonical_angler
  end

  #considers the first duplicate angler record that comes across as the canonical angler
  #takes nil attrs from the first angler and inserts values from subsequent anglers into them
  #deletes duplicate anglers and makes entries into the temp table
  # POST /anglers/merge/perform
  def perform_merge
    authorize! :update, Angler
    success = true

    Angler.transaction do
      begin
        Angler.where(angler_id: params[:angler][:angler_id]).first.update_attributes!(angler_params)

        params[:duplicate_ids].each do |angler_id|
          AnglerDataMigration.from_angler_to_angler!(angler_id, params[:angler][:angler_id], params['delete_duplicates'] == 'true', current_user.id)
        end
      rescue
        success = false
      end
    end

    respond_to do |format|
        format.html {
          if success
            redirect_to angler_path(Angler.from_ambiguous_id(params[:angler][:angler_id])), notice: 'Angler data was successfully merged'
          else
            angler_ids  = [params[:angler][:angler_id]] + params[:duplicate_ids]
            redirect_to with_ids_duplicates_anglers_path(angler_ids: angler_ids), error: 'There was errors migrating the data'
          end
        }
    end
  end

  # POST /anglers/transfer_data
  def transfer_data
    authorize! :read, Angler
    authorize! :destroy, Angler
    should_delete = params[:delete_angler] == '1'
    success = AnglerDataMigration.from_angler_to_angler(params[:original_angler], params[:destination_angler], should_delete, current_user.id)

    respond_to do |format|
      if success && should_delete
        format.html { redirect_to angler_path(Angler.from_ambiguous_id(params[:destination_angler])), notice: 'Data was transferred anf the angler was deleted' }
      elsif success
        format.html { redirect_to angler_path(Angler.from_ambiguous_id(params[:original_angler])), notice: 'Angler\'s data was transferred'  }
      else
        format.html { redirect_to angler_path(Angler.from_ambiguous_id(params[:original_angler])), error: 'There was an error transferring the data' }
      end
    end
  end

  private

  def set_angler
    @angler = Angler.find(params[:id])
    @angler.current_user = current_user
  end

  def angler_params
    params.require(:angler).permit(:angler_id, :first_name, :last_name, :street, :suite, :city, :state, :zip,
                                   :phone_number_1, :phone_number_2, :phone_number_3, :email, :email_2, :tag_end_user_type,
                                   :shirt_size, :comments)
  end

  def get_angler_json_list
    # get the data from the capture model
    Angler.get_data_tables_list(data_table_params)
  end

  def auth_autocomplete
    authorize! :read, Angler
  end

end

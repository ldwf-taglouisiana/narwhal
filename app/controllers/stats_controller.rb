class StatsController < ApplicationController

  before_action do
    authorize! :read, :stats
  end

  include GeoDataActionable

  def index
    session.delete(:new_angler)
    session.delete(:capture_info)
    session.delete(:recapture_info)
  end

  def active_anglers
    @data = {
        labels: [],
        current_year: [],
        last_year: []
    }

    6.times do  |iteration|
      month = Time.now - iteration.months
      @data[:labels] << month.strftime('%B')
      @data[:current_year] << Angler.active_during(month.beginning_of_month, month.end_of_month).count
      @data[:last_year] << Angler.active_during((month - 1.years).beginning_of_month, iteration == 0 ? (Time.now - 1.years) : (month - 1.years).end_of_month).count
    end

  end

  def total_anglers
    @data = {
        labels: [],
        current_year: [],
        last_year: []
    }

    6.times do  |iteration|
      month = Time.now - iteration.months
      @data[:labels] << month.strftime('%B')
      @data[:current_year] << Angler.with_kit.where('created_at <@ tstzrange(NULL,?,\'[]\')', month.end_of_month).count
      @data[:last_year] << Angler.with_kit.where('created_at <@ tstzrange(NULL,?,\'[]\')', iteration == 0 ? (Time.now - 1.years) : (month - 1.years).end_of_month).count
    end

  end

  def total_captures
    @data = {
        labels: [],
        current_year: [],
        last_year: []
    }

    6.times do  |iteration|
      month = Time.now - iteration.months
      @data[:labels] << month.strftime('%B')
      @data[:current_year] << Capture.where('created_at <@ tstzrange(NULL,?,\'[]\')', month.end_of_month).count
      @data[:last_year] << Capture.where('created_at <@ tstzrange(NULL,?,\'[]\')', iteration == 0 ? (Time.now - 1.years) : (month - 1.years).end_of_month).count
    end

  end

  def total_recaptures
    @data = {
        labels: [],
        current_year: [],
        last_year: []
    }

    6.times do  |iteration|
      month = Time.now - iteration.months
      @data[:labels] << month.strftime('%B')
      @data[:current_year] << Recapture.where('created_at <@ tstzrange(NULL,?,\'[]\')', month.end_of_month).count
      @data[:last_year] << Recapture.where('created_at <@ tstzrange(NULL,?,\'[]\')', iteration == 0 ? (Time.now - 1.years) : (month - 1.years).end_of_month).count
    end

  end

  def fish_event_points
    clean_params = geo_json_params(params)

    klass = clean_params[:recapture] ? Recapture : Capture
    @items = klass.in_time_range(clean_params[:start_date], clean_params[:end_date])
                 .joins(:angler, :tag)
                 .where.not(geom: nil)
                 .select([
                             "DISTINCT ON (\"#{klass.table_name}\".id) \"#{klass.table_name}\".*",
                             'ST_AsGeoJSON(geom)::json AS geo_json',
                             'concat(anglers.first_name, \' \', anglers.last_name) AS angler_name',
                             '"tags"."tag_no" AS tag_number',
                             'anglers.angler_id as angler_number'
                         ])
  end


  def fish_event_basins
    @items = helper_event_basin_data(geo_json_params(params))
  end

  def fish_event_heatmap
    @items = helper_event_heatmap(geo_json_params(params))
  end

  def species_counts
    clean_params = geo_json_params(params)
    klass = clean_params[:recapture] ? Recapture : Capture

    @items = klass.in_time_range(clean_params[:start_date], clean_params[:end_date])
                 .target_species_counts.to_a
                 .sort_by {|item| item.species_name == 'Other' ? -1 : item.species_count}.reverse

    @params = clean_params
  end
end

class RequestDownloadsController < ApplicationController

  skip_authorization_check

  before_action :set_request

  def get
    if @request.is_ready?
      cookies['fileDownload'] = 'true' # this cookie tells the jQuery.fileDownload action that there is a file download, which triggers the success callback
      send_file @request.filepath, filename: @request.filename
    else
      render json: {}, status: :accepted
    end
  end

  # simply checks to see if a files is ready for download
  def get_file_is_ready
    clean_params = params.permit(:token)

    # try to get the item requested by token
    item = RequestedDownload.where(token: clean_params[:token]).first

    render json: { ready: (!item.nil? and item.is_ready?) }
  end

  # updates a download object as to whether it needs to be sent via email
  def post_should_email
    @request.mark_for_emailing(request_params[:send_email].present?)
    render json: {}, status: :ok
  end

  private

  def request_params
    params.permit(:token, :send_email)
  end

  def set_request
    @request = RequestedDownload.where(token: request_params[:token]).first

    raise ActionController::RoutingError.new('Not Found') if @request.nil?
  end
end

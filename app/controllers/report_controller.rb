class ReportController < ApplicationController

  before_action do
   authorize! :report, :reports
 end

  include ReportDataHelper
  include GetCapturesModule
  include ReportModule
  include DownloadableActionable

  before_action :prune_params
  before_filter :get_section
  skip_before_filter :verify_authenticity_token

  REPORT_SECTIONS = {
      :angler_info => 'angler_info',
      :tagged_fish => 'tagged_fish',
      :recaptured_fish => 'recaptured_fish',
      :unused_kits => 'unused_kits',
      :days_at_large => 'days_at_large',
      :captures_by_angler => 'captures_by_angler',
      :recaptures_by_angler => 'recaptures_by_angler',
      :captures_lottery => 'captures_lottery',
      :data_check => 'data_check',
      :saltwater_series => 'saltwater_series',
      :custom_capture_query => 'custom_capture_query',
      :banquet_anglers => 'banquet_anglers',
      :sql_dump => 'sql_dump'
  }

  def reports
  end

  def angler_info
    respond_to do |format|
      format.json {
        token = download_angler_info(params)
        render json: { token: token }
      }
    end
  end

  def tagged_report
    respond_to do |format|
      format.js {
        @items = get_tagged_fish_list(params)
        render 'get_report_section'
      }
      format.json {
        token = download_tagged_fish_list(params)
        render json: { token: token }
      }
    end
  end

  def angler_captures

    respond_to do |format|
      format.js {
        @items = get_captures_by_angler_list(params)
        render 'get_report_section'
      }
      format.json {
        token = download_captures_by_angler_list(params)
        render json: { token: token }
      }
    end
  end

  def angler_recaptures

    respond_to do |format|
      format.js {
        @items = get_recaptures_by_angler_list(params)
        render 'get_report_section'
      }
      format.json {
        token = download_recaptures_by_angler_list(params)
        render json: { token: token }
      }
    end
  end

  def angler_events
    # raise params.inspect
    clean_params = params.permit(:recaptures, :angler_id, :start_date, :end_date, :count, :start, :length, :draw, :order => [:column, :dir])
    requested_recaptures = (params[:recaptures] == 'true')

    respond_to do |format|
      format.json {
        json_data = get_events_detail_by_angler_list_helper(clean_params, requested_recaptures)
        render json: json_data
      }
    end
  end

  def angler_unused_kits
    # raise params.inspect
    clean_params = params.permit(:angler_id, :start_date, :end_date, :start, :length, :draw, :order => [:column, :dir])

    respond_to do |format|
      format.js {
        @items = get_unused_kits_list(clean_params)
        render 'get_report_section'
      }
      format.json {
        token = download_unused_kits_list(params)
        render json: { token: token }
      }
    end
  end

  def banquet_anglers
    # raise params.inspect
    clean_params = params.permit(:start_date, :end_date, :start, :length, :draw, :order => [:column, :dir])

    respond_to do |format|
      format.js {
        @items = get_banquet_list(clean_params)
        render 'get_report_section'
      }
      format.json {
        token = download_banquet_list(params)
        render json: { token: token }
      }
    end
  end


  def days_at_large

    respond_to do |format|
      format.js {
        @items = get_days_at_large_list(params)
        render 'get_report_section'
      }
      format.json {
        token = download_days_at_large_list(params)
        render json: { token: token }
      }
    end
  end

  def random_event
    respond_to do |format|
      format.js {
        @items = get_random_event_list(params)
        render 'get_report_section'
      }
    end
  end

  def data_check
    cleaned_params = params.permit(:start_date,:end_date,:user_id)
    respond_to do |format|
      format.js {
        @items = get_data_check_list(cleaned_params)
      }
      format.json {
        token = download_data_check_list(cleaned_params)
        render json: { token: token }
      }
    end
  end

  #Saltwater series data
  def saltwater
    p = params[:custom_query]

    p[:search] = {
        captures: true,
        recaptures: false
    }

    p[:config] = {
        recaptures: true
    }

    p[:terms][:anglers] = %w(LAW9909 LAW9912 LAW9915).map { |t| { angler_id: t }  }

    # create the file name
    min = p[:terms][:capture_date][:min].blank? ? 'First' : p[:terms][:capture_date][:min]
    max = p[:terms][:capture_date][:max].blank? ? 'Last' : p[:terms][:capture_date][:max]
    filename = "saltwater_series_for_#{min}_to_#{max}.xlsx"

    # create the token
    token = get_custom_query_token(p, filename)

    # respond with a JSON request
    respond_to do |format|
      format.json {
        # send the token that can be claimed later
        render json: { token: token }
      }
    end # end respond_to
  end


  def custom
    p = params[:custom_query]

    # cleanup the options from the chosen multi select
    p[:terms][:species]                = p[:terms][:species].reject { |t| t.values.first.blank? }
    p[:terms][:basin]                  = p[:terms][:basin].reject { |t| t.values.first.blank? }
    p[:terms][:sub_basin]              = p[:terms][:sub_basin].reject { |t| t.values.first.blank? }
    p[:terms][:species]                = p[:terms][:species].reject { |t| t.values.first.blank? }
    p[:terms][:time_of_day_options]    = p[:terms][:time_of_day_options].reject { |t| t.values.first.blank? }
    p[:terms][:fish_condition_options] = p[:terms][:fish_condition_options].reject { |t| t.values.first.blank? }

    # create the file name
    # TODO need to check the params coming in
    # p[:terms][:anglers] = p[:terms][:anglers].map { |a| { angler_id: Angler.from_ambiguous_id(a).try(:id) }  }
    min = p[:terms][:capture_date][:min].blank? ? 'First' : p[:terms][:capture_date][:min]
    max = p[:terms][:capture_date][:max].blank? ? 'Last' : p[:terms][:capture_date][:max]
    filename = "Captures_for_#{min}_to_#{max}.xlsx"

    # create the token
    token = get_custom_query_token(p, filename)

    # respond with a JSON request
    respond_to do |format|
      format.json {
        # send the token that can be claimed later
        render json: { token: token }
      }
    end # end respond_to

  end

  def sql_dump
    if current_user.has_role? :admin
      respond_to do |format|
        format.json {
          token = download_sql_dump(params)
          render json: { token: token }
        }
      end
    else

    end
  end

  def sql_dump_csv
    if current_user.has_role? :admin
      respond_to do |format|
        format.json {
          token = download_sql_dump(params.merge(csv: true))
          render json: { token: token }
        }
      end
    else

    end
  end

  private

  def prune_params
    params[:start_date]  = params[:start_date].gsub(/__\/__\/____/, '').presence if params[:start_date].present?
    params[:end_date]    = params[:end_date].gsub(/__\/__\/____/, '').presence   if params[:end_date].present?
    params[:angler_id]   = params[:angler_id].presence

    params
  end

  def get_section
    @section = ReportController::REPORT_SECTIONS[params[:section].try(:to_sym) || :angler_info]
  end


  def get_custom_query_token(params, filename)
    # create the token
    token = generate_download_token

    # create the download item with the token, the filename, and the if of the user that created it
    RequestedDownload.create!(token: token, filename: filename, user_id: current_user.id)

    # start the job to create the file
    CustomQueryJob.perform_later(params, token)

    token
  end

end
# == Schema Information
# Schema version: 20160405165101
#
# Table name: species
#
#  id                 :integer          not null, primary key
#  common_name        :string(255)
#  species_code       :string(255)
#  position           :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  photo_file_name    :string(255)
#  photo_content_type :string(255)
#  photo_file_size    :integer
#  photo_updated_at   :datetime
#  proper_name        :string(255)
#  other_name         :string(255)
#  family             :string(255)
#  habitat            :text
#  description        :text
#  size               :text
#  food_value         :text
#  name               :string(255)
#  publish            :boolean          default(FALSE)
#  target             :boolean          default(FALSE)
#  ordinal_position   :integer
#

class SpeciesController < ApplicationController

  before_filter :authenticate_user!, :except => [:image]
  before_action :set_species,  except: [:index, :new, :create, :sort]

  # GET /species
  # GET /species.json
  def index
    authorize! :read, Species
  end

  # GET /species/1
  # GET /species/1.json
  def show
    authorize! :read, @species
  end

  # GET /species/new
  # GET /species/new.json
  def new
    authorize! :create, Species
    @species = Species.new
  end

  # GET /species/1/edit
  def edit
    authorize! :update, @species
  end

  # POST /species
  # POST /species.json
  def create
    authorize! :create, Species
    @species = Species.new(species_params)

    respond_to do |format|
      if @species.save
        format.html {
          redirect_to @species, notice: 'Species was successfully created.'
        }
        format.json { render json: {files: [@species.to_jq_upload]}, status: :created, location: @species }
        #format.json { render json: @species, status: :created, location: @species }
      else
        format.html { render action: "new" }
        format.json { render json: @species.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /species/1
  # PUT /species/1.json
  def update
    authorize! :update, @species
    respond_to do |format|
      if @species.update_attributes(species_params)
        format.html { redirect_to @species, notice: 'Species was successfully updated.' }
      else
        format.html { render action: 'edit' }
      end
    end
  end

  # DELETE /species/1
  # DELETE /species/1.json
  def destroy
    authorize! :destroy, @species
    params[:delete_image] ? @species.photo.destroy : @species.destroy

    respond_to do |format|
      format.html { redirect_to species_path }
      format.json { head :no_content }
    end
  end

  def update_published
    authorize! :update, @species
    @species.update_attributes(:publish => params[:publish])
  end

  def update_target
    authorize! :update, @species
    @species.update_attributes(target: params[:target])
  end

  def sort
    authorize! :update, @species
    @species = Species.all

    if params[:target].present?
      @species.each do |fish|
        if fish.position != 0 && fish.target
          fish.position = params["fish"].index(fish.id.to_s) + 1
          fish.save
        else
          fish.position = nil
          fish.save
        end
      end
    end

    if params[:position].present?
      @species.each do |fish|
        if fish.position != 0 && fish.publish
          fish.ordinal_position = params["fish"].index(fish.id.to_s) + 1
          fish.save
        else
          fish.ordinal_position = nil
          fish.save
        end
      end
    end


    render :nothing => true
  end

  def image
    authorize! :read, @species
    style = params[:style] ? params[:style] : 'original'
    send_file(@species.photo.path(style), {type: @species.photo_content_type, disposition: 'inline'})
  end

  private

  def set_species
    @species = Species.find(params[:id])
  end

  def species_params
  params.require(:species).permit(:common_name, :species_code, :position, :proper_name, :other_name, :family, :habitat,
                          :description, :size, :food_value, :name, :publish, :target, :ordinal_position, :photo)
  end
end

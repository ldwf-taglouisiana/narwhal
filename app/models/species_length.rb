# == Schema Information
# Schema version: 20150817164549
#
# Table name: species_lengths
#
#  id          :integer          not null, primary key
#  description :string(255)      not null
#  species_id  :integer          not null
#  position    :integer          not null
#  created_at  :datetime
#  updated_at  :datetime
#  min         :float
#  max         :float
#
# Indexes
#
#  idx_species_length_min_max  (min,max)
#
# Foreign Keys
#
#  species_lengths_species_fk  (species_id => species.id)
#

class SpeciesLength < ActiveRecord::Base
  belongs_to :species

  has_many :captures
  has_many :recaptures
  has_many :draft_captures

  scope :grouped_select, -> { joins(:species).order(:species_id, :position).select(['species_lengths.*', 'species.common_name as species_name']) }
end

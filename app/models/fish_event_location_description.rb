# == Schema Information
# Schema version: 20160111183154
#
# Table name: fish_event_location_descriptions
#
#  id          :integer          not null, primary key
#  description :text             not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_fish_event_location_descriptions_on_description  (description) UNIQUE
#

class FishEventLocationDescription < ActiveRecord::Base

  has_many :captures
  has_many :recaptures

  before_validation :clean_description

  validates_presence_of :description
  validates_uniqueness_of :description

  def self.clean(value)
    value.try(:gsub, /^[0-9]{4}\s|\n.*|\r.*|^\d.+?$/, '')
  end

  private

  def clean_description
    self.description = FishEventLocationDescription.clean(self.description)
  end
end

# == Schema Information
#
# Table name: notification_couriers
#
#  id         :integer          not null, primary key
#  courier    :string(255)      not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class NotificationCourier < ActiveRecord::Base
  has_many :user_courier

  # constants for the types of topics
  COURIERS = {
      :email => 'Email'
  }

  # dynamic methods to check what type of topic this is
  COURIERS.each do |key, value|
    define_method("#{key}?") { self.courier == value }
  end
end

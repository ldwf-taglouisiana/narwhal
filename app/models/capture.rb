# == Schema Information
# Schema version: 20160113180221
#
# Table name: captures
#
#  id                                 :integer          not null, primary key
#  tag_id                             :integer
#  capture_date                       :datetime         not null
#  species_id                         :integer
#  length                             :decimal(, )
#  location_description               :text
#  latitude                           :float
#  longitude                          :float
#  fish_condition_option_id           :integer
#  weight                             :float
#  comments                           :text
#  confirmed                          :boolean          default(FALSE)
#  verified                           :boolean          default(FALSE)
#  deleted                            :boolean
#  mailed_map                         :boolean
#  time_of_day_option_id              :integer
#  created_at                         :datetime         not null
#  updated_at                         :datetime         not null
#  geom                               :geometry
#  entered_gps_type                   :string(255)
#  user_capture                       :boolean          default(FALSE)
#  species_length_id                  :integer
#  basin_id                           :integer
#  sub_basin_id                       :integer
#  gps_format_id                      :integer
#  recent                             :boolean          default(FALSE), not null
#  search_vector                      :tsvector         not null
#  fish_event_location_description_id :integer
#  angler_id                          :integer          not null
#
# Indexes
#
#  cap_tag_fk                                  (tag_id) UNIQUE
#  captures_length_idx                         (length)
#  captures_tag_idx                            (tag_id) UNIQUE
#  idx_captures_created_at                     (created_at)
#  index_captures_geom                         (geom)
#  index_captures_on_fish_condition_option_id  (fish_condition_option_id)
#  index_captures_on_gps_format_id             (gps_format_id)
#  index_captures_on_recent                    (recent)
#  index_captures_on_search_vector             (search_vector)
#  index_captures_on_species_id                (species_id)
#  index_captures_on_species_length_id         (species_length_id)
#  index_captures_on_time_of_day_option_id     (time_of_day_option_id)
#
# Foreign Keys
#
#  cap_fishcond_fk                  (fish_condition_option_id => fish_condition_options.id)
#  cap_species_fk                   (species_id => species.id)
#  cap_time_fk                      (time_of_day_option_id => time_of_day_options.id)
#  captures_basin_id_fk             (basin_id => basins.gid)
#  captures_species_length_id_fkey  (species_length_id => species_lengths.id)
#  captures_sub_basin_id_fk         (sub_basin_id => "sub-basin".gid)
#  fk_captures_gps_type             (entered_gps_type => gps_formats.format_type)
#  fk_rails_64432fd973              (gps_format_id => gps_formats.id)
#  fk_rails_906df65cfe              (angler_id => anglers.id)
#

class Capture < ActiveRecord::Base

  include Captureable, FishEntryable, Datatableable, PgSearch, FishEventUtils, UserPermissible
  resourcify
  strip_attributes
  has_paper_trail


  has_one :presentable_capture, foreign_key: :id
  has_many :recaptures, -> { order(capture_date: :desc) }

  validates_uniqueness_of :tag_id, message: '^Capture already exists with that tag'
  validate :tag_assigned
  validate :angler_id_match_tag

  after_commit :update_archived, if: Proc.new { |capture| ArchivedCapture.where(id: capture.id).any? }
  after_commit :update_recaptures

  #They don't seem to want this right now so I'm just commenting out the trigger, the underlying system is still there
  #after_save :update_angler_incentive_item_requests

  before_create :set_recent

  before_destroy :unset_recapture_foreign_ky

  # the PgSearch scope to search through the tsvector column
  pg_search_scope :search,
                  against: :search_vector,
                  using: {
                      tsearch: {
                          dictionary: 'english',
                          tsvector_column: 'search_vector',
                          prefix: true
                      }
                  }

  # this will be the trigger used to update the tsvector column for a tag.
  trigger.before(:insert, :update).declare('angler record; tag record;') do
    <<-SQL
      -- get the associated angler for this tag
      select * into angler from anglers where id = new.angler_id;
      select * into tag from tags where id = new.tag_id;

      new.search_vector :=
        setweight(to_tsvector('pg_catalog.english', coalesce(tag.tag_no, '')), 'A')     ||
        setweight(to_tsvector('pg_catalog.english', coalesce(angler.angler_id, '')), 'B')  ||
        setweight(to_tsvector('pg_catalog.english', coalesce(angler.last_name, '')), 'C')  ||
        setweight(to_tsvector('pg_catalog.english', coalesce(angler.first_name, '')), 'C') ||
        setweight(to_tsvector('pg_catalog.english', coalesce(angler.email, '')), 'D')
    SQL
  end

  #####################################################
  ## Class methods
  #####################################################

  class << self
    def new_legacy_capture(params)
      Capture.new(params.except(:recapture_disposition_id, :recapture))
    end

    def datatable_list(options = {})
      verified = options[:verified]
      options = Datatableable::DATA_TABLE_OPTIONS.merge options
      table_name = PresentableCapture.table_name

      where_clause = verified ? "#{table_name}.verified IS TRUE" : "#{table_name}.verified IS FALSE"
      where_clause = [where_clause, options[:where]]
      where_clause << "id in (#{Capture.search(options[:search_term]).select(:id).to_sql})" unless options[:search_term].blank?
      where_clause = where_clause.compact.flatten.join(' AND ')

      # a list of sort columns that will correspond with the column index provided by the DataTable JS library
      columns = [
          "regexp_replace(#{table_name}.angler_number, E'[0-9]+', '') :sort_direction, regexp_replace(#{table_name}.angler_number, E'[^0-9]+', '', 'g')::bigint :sort_direction,  regexp_replace(#{table_name}.angler_number, E'^[A-Za-z]+[0-9]+', '') :sort_direction",
          "#{table_name}.capture_angler :sort_direction",
          "#{table_name}.tag_number :sort_direction",
          "#{table_name}.capture_date :sort_direction",
          "#{table_name}.species_name :sort_direction",
          "#{table_name}.length :sort_direction",
          "#{table_name}.latitude :sort_direction",
          "#{table_name}.longitude :sort_direction",
          "#{table_name}.fish_condition_option_id :sort_direction",
          "has_recapture :sort_direction",
          "#{table_name}.time_of_day_option_id :sort_direction",
          "#{table_name}.created_at :sort_direction",
          "has_images :sort_direction"
      ]

      image_count_sql = <<-SQL
        (
          SELECT
            count(*) > 0
          FROM
            fish_entry_photos
          WHERE
            fish_entry_photoable_type = 'Capture'
            AND
            fish_entry_photoable_id = #{table_name}.id
        ) as has_images
      SQL

      options = options.merge({
          where: where_clause,
          selects: [
              '*',
              "(#{table_name}.recapture_count > 0) as has_recapture",
              "#{table_name}.created_at as entered",
              image_count_sql
          ],
          order_column: columns,
          klass: (verified ? PresentableCapture : PresentableCapture.as_unverified)
      })

      options[:order_column_index] -= 1

      as_data_table_result(options)
    end
  end

  #####################################################
  ## Instance methods
  #####################################################

  def table_name
    'captures'
  end

  def sheet_row
    tag = self.tag
    angler = self.angler
    species = self.species
    species_length = self.species_length

    [
        angler.try(:angler_id),
        angler.try(:full_name),
        tag.try(:tag_number),
        capture_date.try(:strftime, '%m/%d/%Y'),
        species.try(:common_name),
        self.length,
        species_length.try(:description),
        self.location_description,
        self.comments,
        self.fish_condition_option.try(:fish_condition_option),
        self.time_of_day_option.try(:time_of_day_option),
        self.latitude_string,
        self.longitude_string,
        self.verified
    ]
  end

  def header_row
    [
        'Angler ID',
        'Angler Name',
        'Tag Number',
        'Capture Date',
        'Species',
        'Length',
        'Length Range',
        'Location Description',
        'Comments',
        'Fish Condition',
        'Time Of Day',
        'Latitude',
        'Longitude',
        'Verified'
    ]
  end

  private

  def unset_recapture_foreign_ky
    self.recaptures.each { |r| r.update_attribute(:capture_id, nil) }
  end

  def tag_assigned
    if Tag.where(angler_id: nil, id: self.tag_id).any?
      errors.add(:tag_id, '^Tag has not been assigned to anyone')
    end
  end

  def angler_id_match_tag
    if self.angler_id != Tag.where(id: self.tag_id).first.try(:angler_id)
      errors.add(:angler_id, '^Angler ID does not match assigned angler for the tag')
    end
  end

  def update_recaptures
    if self.tag_id_changed?
      self.recaptures.each{|r| r.update_attribute(:capture_id, nil)}
    end
    Recapture.where(tag_id: self.tag_id).where('capture_id != ? OR capture_id is NULL', self.id).each { |r| r.update_attribute(:capture_id, self.id) }
  end

  def update_archived
    RefreshArchivedCaptureJob.perform_if_needed
  end

  def set_recent
    self.recent = true
  end

  def update_angler_incentive_item_requests
    self.angler.create_capture_incentive_item_requests
  end

end

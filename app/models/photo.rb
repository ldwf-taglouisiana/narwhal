# == Schema Information
#
# Table name: photos
#
#  id                 :integer          not null, primary key
#  image_file_name    :string(255)
#  image_content_type :string(255)
#  image_file_size    :integer
#  image_updated_at   :datetime
#  imageable_id       :integer
#  imageable_type     :string(255)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

class Photo < ActiveRecord::Base

  belongs_to :imageable, :polymorphic => true
  before_create :randomize_file_name

  has_attached_file :image, {
      :styles => {:large => '1000x1000^', :medium => '800x800^', :small => '600x600^', :feature => '250x250>', :index_list => '124x>', :thumb => '80x80>'},
      :path => ':rails_root/storage/images/:style/:filename',
      :url => '/photos/:id/:hash.:extension?style=:style',
      :hash_secret => 'JJiHde4O9Qx3IpVR/C8d/PJE3LjTiL5By1xxpBxgLHUGJTcIYh9U/GSX7+HgdA170KhzkJuQ2889MufvWhfoWXrfnz0Yx/QKN45ZG3YwueL7ZUs/wpGnz5KLW4zSRgJTUrkaZy0IzGMO16rguTycD19xX7jMqa3KbMHiFjwj/dWZeLOWN65lYpe4+4Gyrd11izgyCsqe/mCvKCo0Jn16EiX2UDMXUYrGdHk1IwC7yNSvU76zdkKmDboa1IRDSC8dE4Z1u/d78iiH6bA+xaozS28Uj1SRPJFjwofV+hhQzjyjPgEG6KKU8kocdMlbSMQ+Qc91z9O5er85BH3TczLiyQ==',
      :convert_options => {all => '-colorspace RGB'}
  }

  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/

  private

  def randomize_file_name
    extension = File.extname(image_file_name).downcase
    extension = (extension.nil? or extension.empty?) ? '.png' : extension
    self.image.instance_write(:file_name, "#{SecureRandom.hex(16)}#{extension}")
  end
end

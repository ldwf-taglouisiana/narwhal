# == Schema Information
# Schema version: 20160113180221
#
# Table name: draft_captures
#
#  id                       :integer          not null, primary key
#  tag_number               :string(255)      not null
#  capture_date             :datetime         not null
#  species_id               :integer
#  length                   :float
#  location_description     :text
#  latitude                 :float
#  longitude                :float
#  fish_condition_id        :integer
#  weight                   :float
#  comments                 :text
#  recapture                :boolean          default(FALSE)
#  recapture_disposition_id :integer
#  time_of_day_id           :integer
#  entered_gps_type         :string(255)
#  error_json               :string(255)
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  should_save              :boolean          default(FALSE)
#  saved_at                 :datetime
#  species_length_id        :integer
#  geom                     :geometry(Point,4
#  uuid                     :string(255)
#  search_vector            :tsvector         not null
#  angler_id                :integer          not null
#
# Indexes
#
#  index_draft_captures_on_search_vector  (search_vector)
#  index_draft_captures_on_uuid           (uuid) UNIQUE
#
# Foreign Keys
#
#  draft_captures_species_length_id_fkey  (species_length_id => species_lengths.id)
#  fk_draft_captures_gps_type             (entered_gps_type => gps_formats.format_type)
#  fk_part_captures_fish_cond             (fish_condition_id => fish_condition_options.id)
#  fk_part_captures_recap_disp            (recapture_disposition_id => recapture_dispositions.id)
#  fk_part_captures_species_id            (species_id => species.id)
#  fk_part_captures_time_of_day           (time_of_day_id => time_of_day_options.id)
#  fk_rails_18ffac8d12                    (angler_id => anglers.id)
#

include ActionView::Helpers::DateHelper
require 'securerandom'

class DraftCapture < ActiveRecord::Base

  include FishEntryable, Datatableable, PgSearch, FishEventUtils,  UserPermissible
  has_paper_trail

  has_one :denormalized_draft_capture, foreign_key: :id
  belongs_to :time_of_day_option, foreign_key: :time_of_day_id
  belongs_to :recapture_disposition
  belongs_to :fish_condition_option, foreign_key: :fish_condition_id
  belongs_to :tag, foreign_key: :tag_number, primary_key: :tag_no
  belongs_to :gps_format, foreign_key: :entered_gps_type, class_name: 'GPSFormat', primary_key: :format_type

  before_validation :set_defaults

  after_save :check_for_errors
  after_save :update_saved_at

  validates :uuid, uniqueness: { scope: [:angler_id] }, unless: Proc.new { |b| b.uuid.blank? }

  # the PgSearch scope to search through the tsvector column
  pg_search_scope :search,
                  against: :search_vector,
                  using: {
                      tsearch: {
                          dictionary: 'english',
                          tsvector_column: 'search_vector',
                          prefix: true
                      }
                  }

  # this will be the trigger used to update the tsvector column for a tag.
  trigger.before(:insert, :update).declare('angler record;') do
    <<-SQL
      -- get the associated angler for this tag
      select * into angler from anglers where id = new.angler_id;

      new.search_vector :=
        setweight(to_tsvector('pg_catalog.english', coalesce(new.tag_number, '')), 'A') ||
        setweight(to_tsvector('pg_catalog.english', coalesce(angler.angler_id, '')), 'B')  ||
        setweight(to_tsvector('pg_catalog.english', coalesce(angler.last_name, '')), 'C')  ||
        setweight(to_tsvector('pg_catalog.english', coalesce(angler.first_name, '')), 'C') ||
        setweight(to_tsvector('pg_catalog.english', coalesce(angler.email, '')), 'D');
    SQL
  end

  #####################################################
  ## Class methods
  #####################################################

  class << self
    # Method to retrieve all draft captures that are ready to be moved to unverified captures.
    def check_for_expiration
      draft_captures = DraftCapture.where('(now() - saved_at) >= \'24 hours\'::interval and should_save is true')

      draft_captures.each do |dc|
        PaperTrail.whodunnit = User.where(angler_id: dc.angler_id).first.try(:id)
        dc.check_for_errors
        fish_event = dc.move_to_capture(false)
        PaperTrail.whodunnit = nil
      end
    end

    def dedupe_existing_entry(delete_duplicates = false)
      compare_attribute_list = %w(species_id latitude longitude capture_date entered_gps_type angler_id species_length_id length)

      duplicate_count = 0
      DraftCapture.where("error_json ilike '%capture already exists%'").each do |item|
        comparable_attrs = item.create_fish_entry.attributes.symbolize_keys.select_keys(*compare_attribute_list)
        duplicate_count = duplicate_count + 1 if item.class.where(comparable_attrs).any?
        item.destroy if delete_duplicates
      end

      puts "Duplicates found #{ 'and deleted ' if delete_duplicates }#{duplicate_count}"
    end

    def dedupe(angler_id)
      angler_id = Angler.from_ambiguous_id(angler_id).try(:angler_id)

      # find all models and group them on keys which should be common
      grouped = where(angler_id: angler_id).group_by{|model| [model.tag_number] }
      grouped.values.each do |duplicates|
        # the first one we want to keep right?
        first_one = duplicates.shift # or pop for last one
        # if there are any more left, they are duplicates
        # so delete all of them
        duplicates.each{|double| double.destroy} # duplicates can now be destroyed
      end
    end

    def datatable_list(options = {})
      options = Datatableable::DATA_TABLE_OPTIONS.merge options

      # a list of sort columns that will correspond with the column index provided by the DataTable JS library
      columns = [
          "regexp_replace(angler_number, E'[0-9]+', '') :sort_direction, regexp_replace(angler_number, E'[^0-9]+', '', 'g')::bigint :sort_direction,  regexp_replace(angler_number, E'^[A-Za-z]+[0-9]+', '') :sort_direction",
          'angler_full_name :sort_direction',
          'tag_number :sort_direction',
          'capture_date :sort_direction',
          'species_name :sort_direction',
          'species_length :sort_direction',
          'latitude :sort_direction',
          'longitude :sort_direction',
          'fish_condition_id :sort_direction',
          'recapture :sort_direction',
          'recapture_disposition_id :sort_direction',
          'created_at :sort_direction'
      ]
      where_clause = nil
      where_clause = options[:search_term].blank? ? nil : "id in (#{DraftCapture.search(options[:search_term]).select(:id).to_sql})" unless options[:search_term].blank?
      if options[:only_errors].present?
        where_clause = where_clause.nil? ? 'error_json IS NOT NULL AND error_json <> \'{"errors":[]}\'' : where_clause + ' AND error_json IS NOT NULL AND error_json <> \'{"errors":[]}\''
      end
      options = options.merge({
                                  order_column: columns,
                                  klass:  DenormalizedDraftCapture,
                                  includes: [:draft_capture],
                                  where: where_clause
                              })

      # offset the index, for the checkbox column
      options[:order_column_index] -= 1
      as_data_table_result(options)
    end

    #sometimes people use other people's tags
    def refresh_errors
      DraftCapture.all.each do |draft_capture|
        draft_capture.check_for_errors
      end
    end
  end

  #####################################################
  ## Instance methods
  #####################################################

  def table_name
    'draft_captures'
  end

  def status
    icon = ''
    message = ''

    if self.has_errors?
      icon = 'fa-warning'
      message = 'There are some errors with this draft'

      # there are no errors and is waiting for the 24 hours to end
    elsif should_save == true
      t = (Time.now - (self.saved_at + 24.hours))

      icon = 'fa-clock-o'
      message = "Draft is still editable for #{distance_of_time_in_words(t)}"

      # there are no errors and the user has saved for later
    elsif should_save == false
      icon = 'fa-save'
      message = 'No Errors. Draft has been saved for later'

    end

    {
        icon: icon,
        message: message
    }
  end

  def has_errors?
    self.parsed_json_errors.any?
  end

  def parsed_json_errors
    JSON.parse(error_json, symbolize_names: true)[:errors]
  end

  def create_fish_entry
    params = ActionController::Parameters.new(self.attributes).permit(:angler_id, :capture_date, :comments, :entered_gps_type, :fish_condition_id, :latitude, :longitude, :recapture, :recapture_disposition_id, :species_id, :species_length_id, :time_of_day_id, :length, :location_description)

    params[:tag_id] = Tag.with_number(self.tag_number.upcase).first.try(:id)
    params[:time_of_day_option_id] = params[:time_of_day_id]
    params[:fish_condition_option_id] = params[:fish_condition_id]
    params[:user_capture] = true

    params = params.except(:time_of_day_id, :recapture, :fish_condition_id)

    self.recapture ?  Recapture.new(params) :  Capture.new_legacy_capture(params)
  end

  # Method to move a valid draft capture into unverified captures.
  def move_to_capture(admin = false)
    # prevent if
    # - there are any errors
    # - the requester in not an admin and the user has indicated they want it converted
    return if self.has_errors? # or (admin != true and self.should_save)

    fish_entry = self.create_fish_entry
    fish_entry.verified = (admin.presence || false)

    # if self has no errors and the capture is saved properly
    if !self.has_errors? && fish_entry.save
      self.recapture ? Notification.build_recapture(fish_entry) : Notification.build_capture(fish_entry)

      # update the version meta data to inclide the draft capture id this entry came from.
      version = fish_entry.reload.versions.first
      meta = version.meta || {}
      meta[:draft_capture] = {}
      meta[:draft_capture][:id] = self.id

      version.update_attribute(:meta, meta)

      self.images.each do |image|
        photo = FishEntryPhoto.new(fish_entry_photoable: fish_entry)
        photo.image = image.image
        photo.created_at = image.created_at
        photo.save!
      end

      self.destroy

    else
      error_json = {errors: fish_entry.errors.full_messages.to_a}
      self.update_column(:error_json, error_json.to_json)

    end # end if

    fish_entry
  end # end move_to_capture

  def check_for_errors
    errors.clear

    errors.add(:latitude, 'cannot be blank') if self.latitude.blank?
    errors.add(:longitude, 'cannot be blank') if self.longitude.blank?

    # create params so we can see what errors might be generated in the target models
    params = ActionController::Parameters.new(self.attributes).permit(:angler_id, :capture_date, :comments, :entered_gps_type, :fish_condition_id, :latitude, :longitude, :recapture, :recapture_disposition_id, :species_id, :species_length_id, :time_of_day_id, :length, :location_description)
    params[:time_of_day_option_id] = params[:time_of_day_id]
    params[:fish_condition_option_id] = params[:fish_condition_id]
    params = params.except(:time_of_day_id, :recapture, :fish_condition_id)

    # create a target model instance
    capture = self.recapture ? Recapture.new(params) : Capture.new_legacy_capture(params)

    # set some of the attributes
    capture.tag = Tag.where(:tag_no => self.tag_number).first
    capture.verified = false
    capture.user_capture = true

    # if the model is not valid then add the generated errors
    unless capture.valid?
      capture.errors.each do |k, v|
        errors.add(k, v)
      end
    end

    # save the errors, if any
    Hash err = Hash.new
    err[:errors] = errors.to_a

    self.update_column(:error_json, err.to_json)
  end


  private

  def set_defaults
    self.uuid = self.uuid || SecureRandom.uuid
    self.should_save = true # TODO remove when save for later is re-enabled
    self.longitude = nil if self.longitude.try(:nan?)
    self.latitude  = nil if self.latitude.try(:nan?)

    true
  end

  def update_saved_at
    if self.saved_at.nil? and self.should_save and self.errors.empty?
      self.update_column :saved_at, Time.now
    end
  end

end

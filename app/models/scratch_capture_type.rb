# == Schema Information
# Schema version: 20180301154137
#
# Table name: scratch_capture_types
#
#  id                              :integer          not null, primary key
#  name                            :text             not null
#  displayable                     :boolean          not null
#  failure_scratch_capture_type_id :integer
#  created_at                      :datetime         not null
#  updated_at                      :datetime         not null
#
# Foreign Keys
#
#  scratch_capture_types_failure_scratch_capture_type_id_fkey  (failure_scratch_capture_type_id => scratch_capture_types.id)
#

class ScratchCaptureType < ActiveRecord::Base
  has_many :scratch_captures
  belongs_to :failure_scratch_capture_type, class_name: 'ScratchCaptureType'

  validates_presence_of :name, message: 'Must Have a Name'
  validates_inclusion_of :displayable, in: [true,false], message: 'Must Have a Display Value'

  def self.SUCCESSFUL_AUTOMATIC_CONVERSION
    self.where(name: 'Successful Automatic Conversion').first
  end

  def self.SUCCESSFUL_MANUAL_CONVERSION
    self.where(name: 'Successful Manual Conversion').first
  end

  def self.PENDING
    self.where(name: 'Pending').first
  end

end

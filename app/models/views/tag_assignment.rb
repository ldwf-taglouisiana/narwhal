# == Schema Information
# Schema version: 20160405165101
#
# Table name: tag_assignments
#
#  id             :integer
#  angler_id      :integer
#  angler_number  :text
#  tag_prefix     :text
#  assignment_at  :date
#  start_tag      :text
#  end_tag        :text
#  number_of_tags :integer
#  last_tag_used  :text
#  tag_numbers    :string           is an Array
#  tag_ids        :integer          is an Array
#  seq_id         :integer
#

class TagAssignment < ActiveRecord::Base
  include Datatableable, SqlView, UserPermissible

  belongs_to :angler

  scope :with_row_number, -> { select('*, row_number() OVER (PARTITION BY angler_id ORDER BY seq_id ASC) AS row_num') }
  scope :unused_kits, -> {
        joins("CROSS JOIN (SELECT id FROM (#{TagAssignment.with_row_number.to_sql}) sfsdfsfd WHERE row_num = 1) tag_assignments_rn")
        .where("#{TagAssignment.table_name}.id = tag_assignments_rn.id")
        .where(last_tag_used: nil)
  }

  class << self

    # get a paginated list of tags for the dataTables JS library
    def get_data_tables_list(options = {})
      options = Datatableable::DATA_TABLE_OPTIONS.merge options

      where = nil
      binds = []

      if options[:date][:month].presence
        date = Date.parse("1-#{options[:date][:month]}-#{options[:date][:year].presence || Time.now.year}")
        where = 'assignment_at <@ daterange(?,?, \'[]\')'
        binds = [date.beginning_of_month, date.end_of_month]
      end

      # a list of sort columns that will correspond with the column index provided by the DataTable JS library
      columns = [
          'angler_prefix :sort_direction, angler_mid :sort_direction, angler_suffix :sort_direction NULLS LAST',
          'angler_name :sort_direction',
          'assignment_at :sort_direction NULLS LAST',
          'start_tag :sort_direction',
          'end_tag :sort_direction',
          'number_of_tags :sort_direction',
          'last_tag_used :sort_direction NULLS LAST'
      ]

      options = options.merge({
                                  selects: [
                                      "tag_assignments.*",
                                      "regexp_replace(tag_assignments.angler_number, E'[0-9]+', '') as \"angler_prefix\"",
                                      "regexp_replace(tag_assignments.angler_number, E'[^0-9]+', '', 'g')::bigint as \"angler_mid\"",
                                      "regexp_replace(tag_assignments.angler_number, E'^[A-Za-z]+[0-9]+', '') as \"angler_suffix\"",
                                      "concat(anglers.first_name, ' ', anglers.last_name) AS \"angler_name\""
                                  ],
                                  joins: [:angler],
                                  where: where,
                                  binds: binds,
                                  order_column: columns
                              })

      options[:order_column_index]
      as_data_table_result(options)
    end

  end # end class << self
end
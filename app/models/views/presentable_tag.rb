# == Schema Information
# Schema version: 20160405165101
#
# Table name: presentable_tags
#
#  id              :integer          primary key
#  tag_no          :string(255)
#  tag_lot_id      :integer
#  tag_request_id  :integer
#  deleted         :boolean
#  unassign_option :integer
#  created_at      :datetime
#  updated_at      :datetime
#  old_anlger_id   :integer
#  prefix          :string(255)
#  active          :boolean
#  assigned_at     :datetime
#  recent          :boolean
#  search_vector   :tsvector
#  angler_id       :integer
#  tag_suffix      :integer
#  tag_prefix      :text
#  used            :boolean
#  used_at         :datetime
#  angler_name     :text
#  angler_number   :string(20)
#  seq_id          :integer
#

class PresentableTag < ActiveRecord::Base
  
  include PgSearch, SqlView, UserPermissible

  self.primary_key = 'id'

  def self.permissible_delegate_class
    Tag
  end

  def self.search(term)
    # we are building a SQL query with the AR query builder. PgSearch just adds a new query to the
    query = Tag.search(term).joins(:presentable_tag).where('"tags".id = "presentable_tags".id').select('"presentable_tags".*').to_sql
    self.find_by_sql(query)
  end
end

# == Schema Information
# Schema version: 20160113180221
#
# Table name: presentable_recaptures
#
#  user_capture             :boolean
#  id                       :integer          primary key
#  species_id               :integer
#  species_name             :string(255)
#  tag_id                   :integer
#  tag_number               :string(255)
#  recapture_angler_id      :integer
#  recapture_angler_number  :string(20)
#  recapture_angler_name    :text
#  capture_date             :datetime
#  date_string              :text
#  location                 :text
#  time_of_day              :string(255)
#  time_of_day_option_id    :integer
#  length                   :decimal(, )
#  length_range             :string(255)
#  recapture_disposition    :string(255)
#  fish_condition_option_id :integer
#  fish_condition           :string(255)
#  latitude                 :float
#  verified                 :boolean
#  longitude                :float
#  latitude_formatted       :text
#  longitude_formatted      :text
#  geom                     :geometry(Point,4
#  basin_id                 :integer
#  sub_basin_id             :integer
#  comments                 :text
#  location_description     :text
#  location_id              :integer
#  created_at               :datetime
#  updated_at               :datetime
#  capture_id               :integer
#  year_id                  :integer
#  recapture_number         :integer
#

class PresentableRecapture < ActiveRecord::Base

  include PgSearch, SqlView, FishEventUtils, UserPermissible

  def self.permissible_delegate_class
    Recapture
  end

  self.primary_key = 'id'

  has_one :recapture, foreign_key: :id

  scope :as_unverified, -> { where(verified: false) }

  def self.search(term, options = { verified: true })
    options = { verified: true }.merge(options)

    # we are building a SQL query with the AR query builder. PgSearch just adds a new query to the
    query = Recapture.search(term).joins(:presentable_recapture).where('"recaptures".id = "presentable_recaptures".id AND "presentable_recaptures".verified = ?', options[:verified]).select('"presentable_recaptures".*').to_sql
    self.find_by_sql(query)
  end

  def angler
    recapture.angler
  end

  def angler_id
    self.recapture_angler_id
  end

  def has_image?
    self.recapture.has_images?
  end
end

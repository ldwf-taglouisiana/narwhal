# == Schema Information
# Schema version: 20160113180221
#
# Table name: archived_captures
#
#  id                       :integer
#  species_id               :integer
#  species_name             :string(255)
#  tag_id                   :integer
#  tag_number               :string(255)
#  angler_id                :integer
#  angler_number            :string(20)
#  capture_angler           :text
#  capture_date             :datetime
#  date_string              :text
#  location                 :text
#  time_of_day_option_id    :integer
#  time_of_day              :string(255)
#  length                   :decimal(, )
#  length_range             :string(255)
#  fish_condition_option_id :integer
#  fish_condition           :string(255)
#  latitude                 :float
#  longitude                :float
#  latitude_formatted       :text
#  longitude_formatted      :text
#  entered_gps_type         :string(255)
#  geom                     :geometry
#  basin_id                 :integer
#  sub_basin_id             :integer
#  comments                 :text
#  user_capture             :boolean
#  verified                 :boolean
#  location_description     :text
#  location_id              :integer
#  created_at               :datetime
#  updated_at               :datetime
#  recapture_count          :integer
#  year_id                  :integer
#
# Indexes
#
#  idx_archived_captures_where_tag_year  (year_id,angler_id,tag_number,location_description,comments,time_of_day_option_id,fish_condition_option_id,length,species_id,capture_date,basin_id,sub_basin_id)
#  index_archived_captures_on_geom       (geom)
#  index_archived_captures_on_id         (id) UNIQUE
#  index_archived_captures_on_verified   (verified)
#

class ArchivedCapture < ActiveRecord::Base
  include SqlView

  belongs_to :basin
  belongs_to :sub_basin
  belongs_to :angler
  belongs_to :species
  belongs_to :time_of_day_option

  def to_builder
    Jbuilder.new do |json|
      json.(self, :id, :species_id, :species_name, :tag_id, :tag_number, :angler_id, :capture_angler, :capture_date, :date_string, :location, :time_of_day_option_id, :time_of_day, :length, :length_range, :fish_condition_option_id, :fish_condition, :latitude, :longitude, :comments, :location_description, :location_id, :recapture_count, :year_id)
    end
  end
end

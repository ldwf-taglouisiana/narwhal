# == Schema Information
# Schema version: 20160405165101
#
# Table name: archived_volunteer_hours
#
#  id                 :integer
#  angler_id          :integer
#  angler_number      :string(20)
#  angler_name        :text
#  verified           :boolean
#  ldwf_entered       :boolean
#  ldwf_edited        :boolean
#  total_time         :decimal(, )
#  start              :datetime
#  end                :datetime
#  recent             :boolean
#  created_at         :datetime
#  updated_at         :datetime
#  number_of_captures :integer
#
# Indexes
#
#  index_archived_volunteer_hours_on_angler_id           (angler_id)
#  index_archived_volunteer_hours_on_id                  (id) UNIQUE
#  index_archived_volunteer_hours_on_number_of_captures  (number_of_captures)
#  index_archived_volunteer_hours_on_recent              (recent)
#

class ArchivedVolunteerHour < ActiveRecord::Base
  include SqlView
end

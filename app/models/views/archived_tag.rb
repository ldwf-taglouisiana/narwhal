# == Schema Information
# Schema version: 20160405165101
#
# Table name: archived_tags
#
#  id              :integer          primary key
#  tag_no          :string(255)
#  tag_lot_id      :integer
#  tag_request_id  :integer
#  deleted         :boolean
#  unassign_option :integer
#  created_at      :datetime
#  updated_at      :datetime
#  old_anlger_id   :integer
#  prefix          :string(255)
#  active          :boolean
#  assigned_at     :datetime
#  recent          :boolean
#  search_vector   :tsvector
#  angler_id       :integer
#  tag_suffix      :integer
#  tag_prefix      :text
#  used            :boolean
#  used_at         :datetime
#  angler_name     :text
#  angler_number   :string(20)
#  seq_id          :integer
#
# Indexes
#
#  index_archived_tags_on_angler_id    (angler_id)
#  index_archived_tags_on_id           (id) UNIQUE
#  index_archived_tags_on_id_and_used  (id,used)
#  index_archived_tags_on_recent       (recent)
#  index_archived_tags_on_seq_id       (seq_id)
#  index_archived_tags_on_tag_no       (tag_no)
#  index_archived_tags_on_used_at      (used_at)
#

class ArchivedTag < ActiveRecord::Base
  include SqlView

  self.primary_key = :id

  has_one :tag, foreign_key: :id
end

# == Schema Information
# Schema version: 20180301154137
#
# Table name: presentable_angler_items
#
#  id            :integer          primary key
#  requested_at  :datetime
#  fulfilled_at  :datetime
#  created_at    :datetime
#  updated_at    :datetime
#  angler_name   :text
#  angler_number :string(20)
#  item_name     :text
#  item_type     :text
#  angler_id     :integer
#  user_id       :integer
#


class PresentableAnglerItem < ActiveRecord::Base

  include PgSearch, SqlView, UserPermissible

  def self.permissible_delegate_class
    AnglerItem
  end

  self.primary_key = 'id'

  private

  def readonly?
    true
  end
  
end

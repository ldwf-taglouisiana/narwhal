# == Schema Information
# Schema version: 20160405165101
#
# Table name: presentable_volunteer_hours
#
#  id                 :integer          primary key
#  angler_id          :integer
#  angler_number      :string(20)
#  angler_name        :text
#  verified           :boolean
#  ldwf_entered       :boolean
#  ldwf_edited        :boolean
#  total_time         :decimal(, )
#  start              :datetime
#  end                :datetime
#  recent             :boolean
#  created_at         :datetime
#  updated_at         :datetime
#  number_of_captures :integer
#

class PresentableVolunteerHour < ActiveRecord::Base
  include PgSearch, SqlView, UserPermissible

  belongs_to :angler

  def self.permissible_delegate_class
    VolunteerTimeLog
  end

  self.primary_key = 'id'

  def self.search(term)
    # we are building a SQL query with the AR query builder. PgSearch just adds a new query to the
    query = VolunteerTimeLog.search(term).includes(:angler).joins(:presentable_volunteer_hour).where('"volunteer_time_logs".id = "presentable_volunteer_hours".id').select('"presentable_volunteer_hours".*').to_sql
    self.find_by_sql(query)
  end

  def to_builder
    Jbuilder.new do |item|
      item.(self, *self.attributes.except('start', 'end', 'created_at', 'updated_at').keys)
      item.start_time self.start.strftime('%m/%d/%Y %I:%M %p')
      item.end_time self.end.strftime('%m/%d/%Y %I:%M %p')
      item.entered self.created_at.strftime('%m/%d/%Y %I:%M %p')
      item.updated self.updated_at.strftime('%m/%d/%Y %I:%M %p')
    end
  end

end

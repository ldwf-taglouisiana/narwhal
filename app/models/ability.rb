class Ability
  include CanCan::Ability

  def initialize(user)

    user ||= User.new # guest user (not logged in)

    if user.has_role? :admin
      can :manage, :all
      return
    end

    if user.has_role? :ldwf
      can :manage, :all
      cannot :assign_administrator_role, User
      return
    end

    if user.has_role? :volunteer
      can :read, User, verified: false
      can :update, User, verified: false
      can :update_assignment, Tag
      can :create, Angler
      can :create, Capture
      can :create, Recapture
      can :read, Capture
      can :read, Recapture
      can :read, DraftCapture
      can :read, Angler
      can :read, VolunteerTimeLog
      can :read, Tag
      can :read, UserTagRequest, fulfilled: false
      can :read, AnglerItem, fulfilled_at: nil
      can :mark_fulfilled, AnglerItem, fulfilled_at: nil
      return
    end
    
    if user.has_role? :angler

    end

  end
end

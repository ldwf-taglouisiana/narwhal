# == Schema Information
#
# Table name: announcements
#
#  id              :integer          not null, primary key
#  content         :text
#  created_at      :datetime
#  updated_at      :datetime
#  expiration_date :datetime
#

class Announcement < ActiveRecord::Base
  strip_attributes
end

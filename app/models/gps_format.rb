# == Schema Information
#
# Table name: gps_formats
#
#  id            :integer          not null, primary key
#  format_type   :string(255)      not null
#  format_string :text             not null
#  created_at    :datetime
#  updated_at    :datetime
#

class GPSFormat < ActiveRecord::Base
  self.table_name = 'gps_formats'

  DEGREES = self.where(format_type: 'D').first

  DEGREES_MINUTES = self.where(format_type: 'DM').first

  DEGREES_MINUTES_SECONDS = self.where(format_type: 'DMS').first
  
  NO_GPS = self.where(format_type: 'NOGPS').first

end

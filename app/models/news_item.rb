# == Schema Information
# Schema version: 20150817164549
#
# Table name: news_items
#
#  id                :integer          not null, primary key
#  user_id           :integer          not null
#  content           :text             not null
#  should_publish    :boolean          default(FALSE)
#  should_publish_at :datetime
#  title             :string(255)      not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#
# Foreign Keys
#
#  newsitem_user_fk  (user_id => users.id)
#

class NewsItem < ActiveRecord::Base
  belongs_to :user

  validates :should_publish_at, date: { on_or_after: Time.now }
  validates_presence_of :user, :content, :title
end

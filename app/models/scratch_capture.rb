# == Schema Information
# Schema version: 20180301154137
#
# Table name: scratch_captures
#
#  id                       :integer          not null, primary key
#  capture_date             :datetime
#  length                   :decimal(, )
#  location_description     :text
#  latitude                 :float
#  longitude                :float
#  comments                 :text
#  tag_id                   :integer
#  tag_number               :text
#  species_id               :integer
#  fish_condition_option_id :integer
#  recapture_disposition_id :integer
#  time_of_day_option_id    :integer
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  geom                     :geometry(Point,4
#  basin_id                 :integer
#  sub_basin_id             :integer
#  gps_format_id            :integer
#  angler_name              :text
#  angler_number            :text
#  angler_id                :integer
#  scratch_capture_type_id  :integer          not null
#  recapture_id             :integer
#
# Foreign Keys
#
#  scratch_captures_angler_id_fkey                 (angler_id => anglers.id)
#  scratch_captures_basin_id_fkey                  (basin_id => basins.gid)
#  scratch_captures_fish_condition_option_id_fkey  (fish_condition_option_id => fish_condition_options.id)
#  scratch_captures_gps_format_id_fkey             (gps_format_id => gps_formats.id)
#  scratch_captures_recapture_disposition_id_fkey  (recapture_disposition_id => recapture_dispositions.id)
#  scratch_captures_recapture_id_fkey              (recapture_id => recaptures.id)
#  scratch_captures_scratch_capture_type_id_fkey   (scratch_capture_type_id => scratch_capture_types.id)
#  scratch_captures_species_id_fkey                (species_id => species.id)
#  scratch_captures_sub_basin_id_fkey              (sub_basin_id => "sub-basin".gid)
#  scratch_captures_tag_id_fkey                    (tag_id => tags.id)
#  scratch_captures_time_of_day_option_id_fkey     (time_of_day_option_id => time_of_day_options.id)
#

class ScratchCapture < ActiveRecord::Base
  include UserPermissible, FishEntryable, Datatableable, FishEventUtils

  belongs_to :recapture_disposition
  belongs_to :time_of_day_option
  belongs_to :tag
  belongs_to :fish_condition_option
  belongs_to :basin
  belongs_to :sub_basin
  belongs_to :gps_format, class_name: 'GPSFormat'
  belongs_to :recapture
  belongs_to :scratch_capture_type
  belongs_to :angler

  before_save :default_type
  before_save :set_angler
  before_save :set_tag

  scope :displayables, -> { joins(:scratch_capture_type).where(scratch_capture_types: {displayable: true}) }

  def entered_gps_type
    return nil if self.gps_format.nil?
    self.gps_format.format_type
  end

  #expects type to be a valid ScratchCapture type
  def convert_to_recapture(type)
    raise ArgumentError,'Type must be set' if type.nil?
    raise ArgumentError, 'Type must be some Scratch Capture Type' unless type.instance_of?(ScratchCaptureType)
    errors = {errors: nil}

    # Pull all the attributes out that don't belong in recapture
    attributes = self.attributes.except(*%w(id created_at updated_at angler_name
                                            tag_number angler_number scratch_capture_type
                                            scratch_capture_type_id recapture_id)).map{|k,v| [k.to_sym, v]}.to_h
    new_recap = Recapture.new(attributes)
    #due to a validation in capturable we have to set this like so
    new_recap.entered_gps_type = self.gps_format.format_type
    new_recap.verified = true
    #if they don't have a valid angler we give them the placeholder
    new_recap.angler = new_recap.angler ? new_recap.angler : Angler.where(angler_id: 'PLC00000').first
    #if we don't know the capture date we infer that it's the day the cap was entered
    new_recap.capture_date = new_recap.capture_date ? new_recap.capture_date : new_recap.created_at
    #if we don't have the species we try to get the species from the original capture,
    #if it doesn't have a tag the conversion will fail anyway
    new_recap.species_id = new_recap.species_id ? new_recap.species_id : Capture.where(tag_id: new_recap.tag_id).first.try(:species_id)
    new_recap.capture = self.tag.try(:capture)
    self.recapture = new_recap
    self.scratch_capture_type = type
    if new_recap.valid? && self.valid?
      self.transaction do
        new_recap.save!
        self.save!
      end
    else
      self.recapture = nil
      self.scratch_capture_type = self.scratch_capture_type.failure_scratch_capture_type
      self.save!
      errors[:errors] = new_recap.errors.full_messages
    end
    errors

  rescue ActiveRecord::RecordInvalid => error
    logger.fatal 'Error Occured Converting to Recapture'
    logger.fatal error
  end

  def manual_conversion
    self.convert_to_recapture(ScratchCaptureType.SUCCESSFUL_MANUAL_CONVERSION)
  end

  def table_name
    'scratch_captures'
  end

  def latitude_string
    ActiveRecord::Base.connection.execute("SELECT format_latitude(geom, gps_formats.format_string) as item from #{self.table_name} left outer join gps_formats on #{self.table_name}.gps_format_id = gps_formats.id where #{self.table_name}.id = #{self.id}")[0]['item']
  end

  def longitude_string
    ActiveRecord::Base.connection.execute("SELECT format_longitude(geom, gps_formats.format_string) as item from #{self.table_name} LEFT OUTER JOIN gps_formats ON #{self.table_name}.gps_format_id = gps_formats.id where #{self.table_name}.id = #{self.id}")[0]['item']
  end

  class << self

    def check_for_expiration
      #Randy said it should be about 1 month till scratches auto convert to recaptures
      s_caps = ScratchCapture.where('(now() - saved_at) >= \'30 days\'::interval')
      s_caps.each do |cap|
        cap.convert_to_recapture(ScratchCaptureType.SUCCESSFUL_AUTOMATIC_CONVERSION)
      end
    end

    def datatable_list(options)
      options = Datatableable::DATA_TABLE_OPTIONS.merge options

      # a list of sort columns that will correspond with the column index provided by the DataTable JS library
      columns = [
        "regexp_replace(#{table_name}.angler_number, E'[0-9]+', '') :sort_direction, regexp_replace(#{table_name}.angler_number, E'[^0-9]+', '', 'g')::bigint :sort_direction,  regexp_replace(#{table_name}.angler_number, E'^[A-Za-z]+[0-9]+', '') :sort_direction",
        "#{table_name}.capture_angler :sort_direction",
        "#{table_name}.tag_number :sort_direction",
        "#{table_name}.capture_date :sort_direction",
        "#{table_name}.species_name :sort_direction",
        "#{table_name}.length :sort_direction",
        "#{table_name}.latitude :sort_direction",
        "#{table_name}.longitude :sort_direction",
        "#{table_name}.fish_condition_option_id :sort_direction",
        "has_recapture :sort_direction",
        "#{table_name}.time_of_day_option_id :sort_direction",
        "#{table_name}.created_at :sort_direction"
      ]

      options = options.merge({
                                order_column: columns,
                                klass: ScratchCapture
                              })

      options[:order_column_index] -= 1

      as_data_table_result(options)
    end

  end

  private
  def default_type
    self.scratch_capture_type ||= ScratchCaptureType.PENDING
  end

  def set_angler
    self.angler = Angler.where(angler_id: self.angler_number).first
  end

  def set_tag
    self.tag = Tag.where(tag_no: self.tag_number).first
  end

  def adjust_latitude_longitude
    if self.gps_format == GPSFormat::NO_GPS
      self.latitude = nil
      self.longitude = nil
    end

    if (!self.latitude.nil? and !self.latitude.nil?) and self.gps_format.nil?
      self.gps_format = GPSFormat::DEGREES
    end
  end

end

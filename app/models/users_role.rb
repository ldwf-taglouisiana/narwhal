# == Schema Information
# Schema version: 20150817164549
#
# Table name: users_roles
#
#  user_id :integer
#  role_id :integer
#
# Indexes
#
#  index_users_roles_on_user_id_and_role_id  (user_id,role_id)
#

class UsersRole < ActiveRecord::Base
  # -----------------------
  # THIS CLASS IS NOT BEING USED.  REFER TO RoleUsers
  # -----------------------

  belongs_to :user
  belongs_to :role

  validates_presence_of :user, :role
  validates_uniqueness_of :role, scope: [ :user ]
end

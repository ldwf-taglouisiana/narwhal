# == Schema Information
# Schema version: 20160801195545
#
# Table name: fish_entry_photos
#
#  id                        :integer          not null, primary key
#  fish_entry_photoable_type :string           not null
#  fish_entry_photoable_id   :integer          not null
#  image_file_name           :string
#  image_content_type        :string
#  image_file_size           :integer
#  image_updated_at          :datetime
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#

class FishEntryPhoto < ActiveRecord::Base
  belongs_to :fish_entry_photoable, polymorphic: true, touch: true

  has_attached_file :image,
                    styles: { medium: "300x300>", thumb: "200x200>" },
                    path: ':rails_root/storage/:class/:attachment/:id_partition/:style/:filename',
                    :url => '/:class/image/:id/:style/:filename',
                    # :hash_secret => 'rzw8vcUS6+9NyjGXEbB+K+vDUuJQFnrb+nMDwo5DmmUyYuCtUlned1bgzCDJN1ht4jtIoR++FbYZrktYT9O+JwZYy78zxIIi85s5izq1duBCrbKqXr5+aU',
                    :convert_options => { all: '-colorspace RGB'}

                    validates_attachment :image,
                       presence: true,
                       content_type: { content_type: ["image/jpeg", "image/gif", "image/png"] }

  before_save :fix_file_name

  private

  def fix_file_name
    extension = Rack::Mime::MIME_TYPES.invert[self.image_content_type]
    self.image_file_name = [File.basename(self.image_file_name), extension].join
  end
end

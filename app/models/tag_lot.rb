# == Schema Information
# Schema version: 20151014162425
#
# Table name: tag_lots
#
#  id              :integer          not null, primary key
#  tag_lot_number  :integer
#  manufacturer_id :integer
#  prefix          :string(255)
#  start_no        :integer
#  end_no          :integer
#  end_user_type   :integer
#  cost            :integer
#  date_entered    :datetime
#  ordered         :boolean
#  order_date      :datetime
#  received        :boolean
#  received_date   :datetime
#  deleted         :boolean
#  color           :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  user_id         :integer
#
# Foreign Keys
#
#  tl_enduser_fk  (end_user_type => tag_end_user_types.id)
#  tl_man_fk      (manufacturer_id => manufacturers.id)
#

class TagLot < ActiveRecord::Base
  include Datatableable, UserPermissible
  strip_attributes

  belongs_to :user
  belongs_to :manufacturer
  belongs_to :tag_end_user_type, foreign_key: 'end_user_type'
  belongs_to :tag_lot_color, foreign_key: 'color'

  validates_presence_of :prefix, :start_no, :end_no, :user

  validates :order_date, date: {
                    before: Proc.new { |t| Time.now },
                    before_message: 'Order date cannot be after today'
                }

  validates :received_date, date: {
                    before: Proc.new { |t| Time.now },
                    after:  Proc.new { |t| t.order_date },
                    before_message: 'Received date cannot be after today',
                    after_message: 'Received date cannot be before the order date'
                }
  validates :start_no, :end_no, overlap: {scope: 'prefix', message_title: 'Log time overlap', message_content: 'There is another entry for the angler that overlaps with this one.'}

  after_create :create_tags

  class << self
    # get a paginated list of anglers for the dataTables JS library
    def get_data_tables_list(options)
      options = Datatableable::DATA_TABLE_OPTIONS.merge options
      where_clause = 'manufacturer_id != 0'
      where_clause = [where_clause, options[:where]].compact.flatten.join(' AND ')



      # a list of sort columns that will correspond with the column index provided by the DataTable JS library
      columns = [
          'name1 :sort_direction',
          'lower(prefix) :sort_direction',
          'start_no :sort_direction',
          'end_no :sort_direction',
          'name2 :sort_direction',
          'color_name :sort_direction',
          'created_at :sort_direction',
          'ordered :sort_direction',
          'order_date :sort_direction',
          'received :sort_direction',
          'received_date :sort_direction'
      ]


      options = options.merge({
                                where: where_clause,
                                selects: ['tag_lots.*', 'manufacturers.name as name1', 'tag_lot_colors.color as color_name', 'tag_end_user_types.name as name2'],
                                joins: [
                                  'left join tag_end_user_types on tag_lots.end_user_type = tag_end_user_types.id',
                                  'left join manufacturers on tag_lots.manufacturer_id = manufacturers.id',
                                  'left join tag_lot_colors ON tag_lots.color = tag_lot_colors.id'
                                ],
                                order_column: columns,
                                klass: TagLot
                              })

      as_data_table_result(options)
    end
  end

  private

  def create_tags
    time_string = Time.now.iso8601

    data = (self.start_no..self.end_no).map { |value| ["#{self.prefix}#{value.to_s.rjust(6, '0')}", self.id, true, time_string, time_string] }
    copy_array_into_db("COPY tags (tag_no, tag_lot_id, recent, created_at, updated_at) FROM STDIN WITH CSV;", data)

    # go through the range of tag values, numerical components.
    data = Tag.where(tag_lot_id: self.id).pluck(:id).map { |tag_id| [Tag.name, tag_id, 'create', self.user_id, nil, 'create', time_string] }
    copy_array_into_db("COPY versions (item_type, item_id, event, whodunnit, object, activity_type, created_at) FROM STDIN WITH CSV;", data)
  end

  def copy_array_into_db(copy_command, items, conn = nil)
    raw = conn.nil? ? ActiveRecord::Base.connection.raw_connection : conn.raw_connection
    raw.exec copy_command

    begin
      items.each do |item|
        raw.put_copy_data item.to_csv
      end
    ensure
      raw.put_copy_end
      while res = raw.get_result do; end # very important to do this after a copy
    end
  end
end

# == Schema Information
#
# Table name: referrals
#
#  id          :integer          not null, primary key
#  referred_by :integer
#  last_name   :string(255)
#  first_name  :string(255)
#  street      :string(255)
#  suite       :string(255)
#  city        :string(255)
#  state       :integer
#  zip         :string(255)
#  phone       :string(255)
#  phone_type  :integer
#  email       :string(255)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Referral < ActiveRecord::Base
end

# == Schema Information
#
# Table name: species
#
#  id                 :integer          not null, primary key
#  common_name        :string(255)
#  species_code       :string(255)
#  position           :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  photo_file_name    :string(255)
#  photo_content_type :string(255)
#  photo_file_size    :integer
#  photo_updated_at   :datetime
#  proper_name        :string(255)
#  other_name         :string(255)
#  family             :string(255)
#  habitat            :text
#  description        :text
#  size               :text
#  food_value         :text
#  name               :string(255)
#  publish            :boolean          default(FALSE)
#  target             :boolean          default(FALSE)
#  ordinal_position   :integer
#

class Species < ActiveRecord::Base
  has_many :captures
  has_many :species_length

  default_scope { where.not(common_name: 'Unknown').order(common_name: :asc) }
  scope :ordinal, -> { where(publish: true).reorder(ordinal_position: :asc) }
  scope :target, -> { where(target: true, publish: true).reorder(position: :asc) }
  scope :not_target, -> { where(target: false).reorder(common_name: :asc) }

  validates_uniqueness_of :common_name

  has_attached_file :photo, {:styles => {:medium => '300x300^', :feature => '250x250>', :index_list => '124x>', :thumb => '80x80>'},
                             :default_url => '/assets/default_fish_original.png',
                             :path => ':rails_root/storage/images/:style/:basename.:extension',
                             :url => '/:class/image/:id/:basename.:extension?style=:style',
                             :hash_secret => 'emutlVkvC7POnMG6jfxdwHWgDvfOQcNWUj66QKW908B0kiRfBmk1/rzw8vcUS6+9NyjGXEbB+K+vDUuJQFnrb+nMDwo5DmmUyYuCtUlned1bgzCDJN1ht4jtIoR++FbYZrktYT9O+JwZYy78zxIIi85s5izq1duBCrbKqXr5+aU',
                             :convert_options => {all => '-colorspace RGB'}
                            }


  acts_as_list

  paginates_per 15

  include Rails.application.routes.url_helpers

  def to_jq_upload
    {
        "name" => read_attribute(:photo_file_name),
        "size" => read_attribute(:photo_file_size),
        "url" => photo.url(:thumb),
        "delete_url" => species_path(self, :delete_image => true),
        "delete_type" => "DELETE"
    }
  end
end

# == Schema Information
# Schema version: 20151207160422
#
# Table name: sub-basin
#
#  gid        :integer          not null, primary key
#  objectid   :decimal(10, )
#  area       :decimal(, )
#  perimeter  :decimal(, )
#  subseg02   :decimal(10, )
#  subseg02id :decimal(10, )
#  subsegment :string(15)
#  name       :string(71)
#  descriptio :string(140)
#  basin      :string(18)
#  shape_area :decimal(, )
#  shape_len  :decimal(, )
#  geom       :geometry(MultiPo
#  created_at :datetime         default(Tue, 01 Dec 2015 03:29:39 UTC +00:00), not null
#  updated_at :datetime         default(Tue, 01 Dec 2015 03:29:39 UTC +00:00), not null
#
# Indexes
#
#  idx_sub_basins_geom  (geom)
#

class SubBasin < ActiveRecord::Base
  self.table_name = 'sub-basin'
  self.primary_key = 'gid'
  belongs_to :basin, foreign_key: :basin, class_name: 'Basin', primary_key: :basin
end

module Datatableable
  extend ActiveSupport::Concern

  DATA_TABLE_OPTIONS = {
      limit: nil,
      offset: 0,
      order_column_index: 0,
      order_direction: 'asc',
      where: nil,
      joins: nil,
      includes: nil,
      selects: nil,
      binds: [],  #binds is always expected to be an array, even if it is only size 1
      order_column: [],
      draw: 0,
      klass: nil,
      search_term: nil,
      for_user: nil,
      scope: nil
  }

  class_methods do
    def as_data_table_result(options = {})
      options = DATA_TABLE_OPTIONS.deep_merge options
      options[:klass] = self if options[:klass].nil?

      sort_orders = {
          asc: 'ASC',
          desc: 'DESC'
      }
      # clean the sort direction, so we only use a valid sort direction
      options[:order_direction] = sort_orders[options[:order_direction].downcase.to_sym] || sort_orders[:desc]
      # sub out the :sort_direction key for the correct sort direction
      options[:order_column] =  options[:order_column].map { |t| t.gsub(/:sort_direction/, options[:order_direction])}
      # get the column to sort by
      order_column = options[:order_column][options[:order_column_index]]
      new_where = ([options[:where]] + options[:binds]).compact
      # fetch all the records using the passed in values
      items = check_scope(options[:klass],options[:scope]).accessible_by_user(options[:for_user],:read)
          .includes(options[:includes])
          .joins(options[:joins])
          .where(new_where)
          .reorder(order_column)
          .select(options[:selects])
          .offset(options[:offset])
          .limit(options[:limit])

      # get the count of the records using the where filter
      filtered_count = check_scope(options[:klass],options[:scope]).accessible_by_user(options[:for_user],:read)
                           .joins(options[:joins])
                           .where(new_where).except(:select).count

      # get the total count of all records
      total_count = check_scope(options[:klass],options[:scope]).accessible_by_user(options[:for_user],:read).except(:select).count

      # create the json object that datatables is expecting
      json = Jbuilder.new do |json|
        json.recordsTotal total_count

        json.recordsFiltered filtered_count
        json.set! :data do
          if items.first.respond_to?(:to_builder)
            json.array! items.collect { |t| t.to_builder.attributes! }
          else
            json.array! items
          end

        end
      end

      json.target!
    end

    private
    def check_scope(klass,scope)
      if !scope.nil? && klass.respond_to?(scope)
        klass.send(scope)
      else
        klass
      end

    end

  end


end

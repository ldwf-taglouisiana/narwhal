module UserPermissible

  extend ActiveSupport::Concern

  included do

    scope :accessible_by_user, -> (user, permission = :manage) { where(Ability.new(user).model_adapter(self._permissible_delegate,permission).conditions) }

    def self._permissible_delegate
      self.respond_to?(:permissible_delegate_class) ? self.permissible_delegate_class : self
    end

  end

end
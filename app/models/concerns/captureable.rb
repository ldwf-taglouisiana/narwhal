module Captureable
  extend ActiveSupport::Concern

  included do
    belongs_to :time_of_day_option
    belongs_to :tag
    belongs_to :fish_condition_option
    belongs_to :basin
    belongs_to :sub_basin
    belongs_to :gps_format, class_name: 'GPSFormat'
    belongs_to :fish_event_location_description

    has_many :photos

    scope :with_tag_number,        -> (tag_number) { joins(:tag).where(tags: { tag_no: tag_number }) }
    scope :in_time_range,          -> (start_time, end_time, scope = '[]') { where("\"#{self.table_name}\".\"capture_date\" <@ tstzrange(?,?,?)", start_time, end_time, scope) }
    scope :in_current_season,      -> { in_time_range( self.this_season[0], self.this_season[1]) }
    scope :in_last_season,         -> { in_time_range(self.last_season[0], self.last_season[1], '[)') }
    scope :target_species_counts,  -> { joins(:species).group('"species_name"').select('(CASE WHEN "species"."target" = true THEN "species"."common_name" ELSE \'Other\' END) as "species_name", count(*) as "species_count"') }
    scope :verified,               -> { where(verified: true) }

    before_validation :update_gps_format_id
    before_validation :check_verified_set
    before_validation :set_tag_id

    validates_presence_of :tag, message: '^Tag must exist'
    validates_presence_of :angler, message: '^Angler must exist'
    validates_presence_of :capture_date, message: '^Capture date cannot be blank'
    validates_presence_of :entered_gps_type, message: 'cannot be blank'
    validates_presence_of :gps_format_id, message: 'cannot be blank'
    validates_presence_of :species, message: 'cannot be blank'

    validates_format_of :latitude, with: /\A(\d(\.\d+)?|[1-8]\d(\.\d+)?|90(\.0+)?)\Z/, allow_nil: true, if: :has_gps?, message: '^Latitude is invalid.'
    validates_format_of :longitude, with: /\A([0](\.\d+)?|-\d(\.\d+)?|-[1-9]\d(\.\d+)?|-1[1-7]\d(\.\d+)?|-180(\.0+)?)\Z/, allow_nil: true, if: :has_gps?, message: '^Longitude is invalid.'

    validates :capture_date, date: {
        before:  Proc.new { |t| Time.now.end_of_day },
        after: Proc.new { |t| (Time.now - 6.years) },
        before_message: 'cannot be after today',
        after_message: 'cannot be before six years ago'
    }

    before_save :update_basins
    before_save :update_fish_event_location_description, unless: Proc.new { |t| t.location_description.blank? }

    after_commit :update_archived_tag, if: Proc.new { |t| t.changed.include?("tag_id") && t.tag.archived? }
  end

  class_methods do
    def last_season
      [SeasonsModule.last_season_start, SeasonsModule.last_season_end]
    end

    def this_season
      [SeasonsModule.season_start, SeasonsModule.season_end]
    end

    def target_species_counts_h
      target_species_counts.collect { |c| { name: c['species_name'],  count: c['species_count'] }}.sort! { |x, y| y[:name] == 'Other' ? -1 : y[:count] <=> x[:count] } || []
    end
  end

  def tag_number

    if @tag_number != self.tag.try(:tag_no)
      @tag_number = self.tag.try(:tag_no)
    end

    @tag_number
  end

  def tag_number=(str)
    @tag_number = str
    self.tag = Tag.with_number(str).first
  end

  def angler_name
    self.angler.try(:full_name)
  end

  def confirmed?
    self.user_capture ? self.verified : true
  end

  # @return [Object] Will be the active record model that self attempted to save. If the errors is empty then the save was successful
  def convert_to_other
    target_klass = self.is_a?(Capture) ? Recapture : Capture
    item = target_klass.new(self.attributes.except('id').select_keys(*target_klass.attribute_names))

    transaction do
      if item.save
        self.destroy!
      end
    end

    item
  end

  private

  def has_gps?
    self.entered_gps_type != 'NOGPS'
  end

  def update_basins
    if self.geom
      self.basin_id = Basin.where('ST_Within(ST_SetSRID(ST_MakePoint(?,?),4326),geom)', self.longitude, self.latitude).pluck(:gid).first
      self.sub_basin_id = SubBasin.where('ST_Within(ST_SetSRID(ST_MakePoint(?,?),4326),geom)', self.longitude, self.latitude).pluck(:gid).first
    end
  end

  def update_gps_format_id
    self.gps_format_id = GPSFormat.where(format_type: self.entered_gps_type).pluck(:id).first

    true # the true return is to indicate there was no error at this point
  end

  def check_verified_set
    self.verified = self.verified || false

    true # the true return is to indicate there was no error at this point
  end

  def set_tag_id
    self.tag = Tag.with_number(self.tag_number).first if self.tag.nil?

    true
  end

  def update_fish_event_location_description
    value = FishEventLocationDescription.clean(self.location_description)
    self.fish_event_location_description = FishEventLocationDescription.where(description: value).first_or_create!
  end

  def update_archived_tag
    RefreshArchivedTagsJob.perform_if_needed
  end
end
module FishEntryable
  extend ActiveSupport::Concern

  included do
    has_many :notifications, as: :notification_item, dependent: :destroy
    has_many :images, class_name: :FishEntryPhoto, as: :fish_entry_photoable, dependent: :destroy

    belongs_to :species_length
    belongs_to :angler
    belongs_to :species

    before_save :adjust_latitude_longitude, :nilify_zero
    after_save :update_geom
  end

  def latitude_string
    ActiveRecord::Base.connection.execute("SELECT format_latitude(geom, gps_formats.format_string) as item from #{self.table_name} LEFT OUTER JOIN gps_formats ON #{self.table_name}.entered_gps_type = gps_formats.format_type where #{self.table_name}.id = #{self.id}")[0]['item']
  end

  def longitude_string
    ActiveRecord::Base.connection.execute("SELECT format_longitude(geom, gps_formats.format_string) as item from #{self.table_name} LEFT OUTER JOIN gps_formats ON #{self.table_name}.entered_gps_type = gps_formats.format_type where #{self.table_name}.id = #{self.id}")[0]['item']
  end

  def has_images?
    self.images.any?
  end

  private

  def remove_associated_notifications
    self.notifications.destroy_all
  end

  def update_geom

    if self.latitude.nil? or self.longitude.nil?
      query = <<-SQL
          UPDATE #{self.table_name} set geom = NULL
          WHERE id = #{self.id}
      SQL

    else
      query = <<-SQL
          UPDATE #{self.table_name} set geom = ST_SetSRID(ST_MakePoint(longitude,latitude),4326)
          WHERE id = #{self.id}
      SQL

    end

    ActiveRecord::Base.connection.execute(query)
  end

  def nilify_zero
    self.attributes.keys.each {
        |attr| self[attr] = nil if self[attr] == 0
    }
  end

  def upcase_tag_number
    self.tag_number.upcase! if self.has_attribute?(:tag_number)
  end

  def clean_comments
    self.comments = self.comments.gsub(/Additional Comments/, '').strip if self.comments
  end

  def has_gps?
    self.entered_gps_type != 'NOGPS' if self.has_attribute?(:entered_gps_type)
  end

  def adjust_latitude_longitude
    return unless self.has_attribute?(:entered_gps_type)
    if self.entered_gps_type == 'NOGPS'
      self.latitude = nil
      self.longitude = nil
    end

    if (!self.latitude.nil? and !self.latitude.nil?) and self.entered_gps_type.nil?
      self.entered_gps_type = 'D'
    end
  end
end
module FishEventUtils
  extend ActiveSupport::Concern

  # included do
  #
  # end

  def obscure_location_from?(other_event)
=begin
  If the other event has the same angler as self, or if the other event has an angler id that starts with LAX or APP.
  LAX or APP ids are considered "recapture anglers", and location is considered public
=end

    # if either event has a "recapture" angler, then return true
    recapture_angler_regex = /^(LAX|APP)[0-9]+/

    # if the other event doesn't have an angelr or the anglers are the same
    if self.angler_id == other_event.angler_id || self.angler.try(:angler_id) =~ recapture_angler_regex
      return false
    end

    other_event.angler.try(:angler_id) !=~ recapture_angler_regex
  end

  def geo_json(opts = {})
    opts = {
        obscured: false, # if the result needs to be obscured to show the polygon segment,
        as_sql: false, # return the SQL string for the query, instead od the json string
        as_hash: false, # returns the result as a Ruby hash, instead of a string
    }.merge(opts)

    # Create the base query to get the item needed to get the geometry to convert to geojson
    query = self.class.where('A.id = ?', self.id)
    if opts[:obscured]
      query = query.joins(' A left join segments S on st_within(A.geom,S.geom)')
    else
      query = query.joins(' A') # alias the main table
    end

    query = query.select(
        [
            'A.*',
            opts[:obscured] ? 'st_geometryn(st_simplify(S.geom, 0.005), 1) as selected_geom' : 'A.geom as selected_geom'
        ]
    )

    # This will create a GeoJSON that conforms to the spec.
    # http://geojson.org/
    #
    # The "properties" json hash is where you can put arbitrary information about the item
    goe_json_query = <<-SQL
      SELECT row_to_json(T) as "json"
        FROM (
          SELECT 'Feature' AS type,
                 ST_AsGeoJSON(selected_geom)::json AS geometry,
                 (
                   SELECT row_to_json(B)::json
                   FROM (
                     SELECT R.id
                   ) B
                 ) AS properties
          FROM (
            #{query.to_sql}
          ) R
      ) T;
    SQL

    # conditionally run the query based on the options provided.
    query_result = opts[:as_sql] ? nil : self.class.connection.execute(goe_json_query)[0]['json']

    # return the resutl in the requested format
    if opts[:as_sql]
      goe_json_query
    elsif opts[:as_hash]
      JSON.parse(query_result)
    else
      query_result
    end
  end

  private

  def get_angler_id_value(fish_event)
    if fish_event.respond_to?(:angler)
      fish_event.send(:angler).try(:angler_id)
    elsif fish_event.respond_to?(:angler_number)
      fish_event.send(:angler_number)
    elsif fish_event.respond_to?(:recapture_angler_number)
      fish_event.send(:recapture_angler_number)
    else
      nil
    end
  end

end
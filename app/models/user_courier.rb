# == Schema Information
# Schema version: 20150817164549
#
# Table name: user_couriers
#
#  id                      :integer          not null, primary key
#  daily_digest            :boolean          default(FALSE)
#  user_id                 :integer          not null
#  notification_courier_id :integer          not null
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  notification_topic_id   :integer          not null
#
# Foreign Keys
#
#  ucourier_notcour_fk   (notification_courier_id => notification_couriers.id)
#  ucourier_nottopic_fk  (notification_topic_id => notification_topics.id)
#  ucourier_user_fk      (user_id => users.id)
#

class UserCourier < ActiveRecord::Base
  belongs_to :notification_courier
  belongs_to :user
  belongs_to :notification_topic

  validates_presence_of :user, :notification_courier, :notification_topic
  validates_uniqueness_of :notification_topic_id, scope: [:user_id, :notification_courier_id]
end

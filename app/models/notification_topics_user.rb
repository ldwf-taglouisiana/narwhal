# == Schema Information
# Schema version: 20150817164549
#
# Table name: notification_topics_users
#
#  notification_topic_id :integer
#  user_id               :integer
#
# Indexes
#
#  notifications_topic_user_idx  (notification_topic_id,user_id) UNIQUE
#
# Foreign Keys
#
#  nt_topic_fk  (notification_topic_id => notification_topics.id)
#  nt_user_fk   (user_id => users.id)
#

class NotificationTopicsUser < ActiveRecord::Base
  belongs_to :user
  belongs_to :notification_topic

  validates_presence_of :user, :notification_topic
  validates_uniqueness_of :notification_topic, scope: [:user]
end

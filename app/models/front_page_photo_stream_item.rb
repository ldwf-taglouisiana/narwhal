# == Schema Information
# Schema version: 20151026210053
#
# Table name: front_page_photo_stream_items
#
#  id         :integer          not null, primary key
#  featured   :boolean          default(FALSE)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class FrontPagePhotoStreamItem < ActiveRecord::Base
  has_one :photo, :as => :imageable, dependent: :destroy

  include Rails.application.routes.url_helpers

  attr_accessor :attachment64

  def api_image_url
    url = ""
    case Rails.env
      when 'production'
        url = "https://narwhal.taglouisiana.com/api/v1"
      when 'staging'
        url = "https://localhost:4444/api/v1"
      else
        url = "http://localhost:4000/api/v1"
    end

    "#{url}#{self.photo.image.url}".gsub(/\?/, '&').gsub(/%3F/, '?')
  end

  def image_name
    self.photo.image_file_name
  end

  def version
    self.created_at.utc.strftime('%Y-%m-%dT%M:%H:%S')
  end

  def to_jq_upload
    {
        "name" => self.photo.image_file_name,
        "size" => self.photo.image_file_size,
        "url" => self.photo.image.url(:small),
        "thumbnail_url" => self.photo.image.url(:thumb),
        "delete_url" => front_page_photo_stream_item_path(self),
        "delete_type" => "DELETE"
    }
  end


  private
  #
  # def save_attachment64
  #   File.open("tmp/reply.png", "wb") { |f| f.write(Datafy::decode_data_uri(attachment64)[0]) }
  #   self.attachment = File.open("tmp/reply.png", "r")
  # end

end

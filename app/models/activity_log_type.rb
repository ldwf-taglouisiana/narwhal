# == Schema Information
#
# Table name: activity_log_types
#
#  id            :integer          not null, primary key
#  activity_type :string(255)      not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class ActivityLogType < ActiveRecord::Base
  strip_attributes

  has_many :activity_logs

  # constants for the types
  TYPES = {
      :create_capture => 'create_capture',
      :update_capture => 'update_capture',
      :create_recapture => 'create_recapture',
      :update_recapture => 'update_recapture',
      :create_angler => 'create_angler',
      :update_angler => 'update_angler',
      :assign_tag => 'create_tag_assignment',
      :unassign_tag => 'create_tag_unassignment',
      :create_tag => 'create_tag',
      :update_tag => 'update_tag'
  }

  class << self
    def create_capture_type
      ActivityLogType.where(activity_type: TYPES[:create_capture]).first
    end

    def update_capture_type
      ActivityLogType.where(activity_type: TYPES[:update_capture]).first
    end

    def create_recapture_type
      ActivityLogType.where(activity_type: TYPES[:create_recapture]).first
    end

    def update_recapture_type
      ActivityLogType.where(activity_type: TYPES[:update_recapture]).first
    end

    def create_angler_type
      ActivityLogType.where(activity_type: TYPES[:create_angler]).first
    end

    def update_angler_type
      ActivityLogType.where(activity_type: TYPES[:update_angler]).first
    end

    def assign_tag_type
      ActivityLogType.where(activity_type: TYPES[:assign_tag]).first
    end

     def unassign_tag_type
      ActivityLogType.where(activity_type: TYPES[:unassign_tag]).first
     end

    def create_tag_type
      ActivityLogType.where(activity_type: TYPES[:create_tag]).first
    end

    def update_tag_type
      ActivityLogType.where(activity_type: TYPES[:update_tag]).first
    end

  end

  def description
    case self.activity_type
      when TYPES[:create_capture]
        'Created'
     when TYPES[:update_capture]
        'Updated'
     when TYPES[:create_recapture]
        'Created'
     when TYPES[:update_recapture]
        'Updated'
      when TYPES[:create_angler]
        'Created'
      when TYPES[:update_angler]
        'Updated'
      when TYPES[:assign_tag]
        'Assigned'
      when TYPES[:unassign_tag]
        'Unassigned'
      else
        'Not sure'
    end
  end

end

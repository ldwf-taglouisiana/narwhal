# == Schema Information
#
# Table name: shirt_size_options
#
#  id                :integer          not null, primary key
#  shirt_size_option :string(255)
#  item_ordinal_pos  :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class ShirtSizeOption < ActiveRecord::Base
end

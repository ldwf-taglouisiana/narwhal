# == Schema Information
# Schema version: 20150817164549
#
# Table name: notifications
#
#  id                     :integer          not null, primary key
#  initiator_id           :integer
#  checked                :boolean          default(FALSE)
#  notification_item_id   :integer          not null
#  notification_item_type :string(255)      not null
#  notification_topic_id  :integer          not null
#  user_id                :integer          not null
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#
# Foreign Keys
#
#  notification_initiator_fk  (initiator_id => users.id)
#  notification_topic_fk      (notification_topic_id => notification_topics.id)
#  notification_user_fk       (user_id => users.id)
#

class Notification < ActiveRecord::Base
  paginates_per 5

  belongs_to :user
  belongs_to :notification_topic
  belongs_to :notification_item, :polymorphic => true

  default_scope { includes(:notification_topic, :notification_item).order(created_at: :desc) }

  scope :active, -> { where(checked: false) }

  class << self
    # dynamic create the class methods to create notifications
    NotificationTopic::TOPICS.each do |key, value|
      define_method("build_#{key}") do |param|
        NotificationJob.perform_later(NotificationTopic.where(topic: value).first, param)
      end
    end
  end

  def kind
    if self.notification_item.nil?
      :unknown

    elsif self.notification_topic.capture?
      :capture

    elsif self.notification_topic.unregistered_recapture?
      :unregistered_recapture

    elsif self.notification_topic.recapture?
      :recapture

    elsif self.notification_topic.user_tag_request?
      :user_tag_request

    elsif self.notification_topic.user?
      :user

    end
  end

end

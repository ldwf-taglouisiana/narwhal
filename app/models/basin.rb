# == Schema Information
# Schema version: 20151207160422
#
# Table name: basins
#
#  gid        :integer          not null, primary key
#  objectid   :decimal(10, )
#  area       :decimal(, )
#  perimeter  :decimal(, )
#  subseg02   :decimal(10, )
#  subseg02id :decimal(10, )
#  subsegment :string(15)
#  name       :string(71)
#  descriptio :string(140)
#  basin      :string(18)
#  shape_area :decimal(, )
#  shape_len  :decimal(, )
#  geom       :geometry(MultiPo
#  created_at :datetime         default(Tue, 01 Dec 2015 03:29:39 UTC +00:00), not null
#  updated_at :datetime         default(Tue, 01 Dec 2015 03:29:39 UTC +00:00), not null
#
# Indexes
#
#  idx_basins_geom  (geom)
#

class Basin < ActiveRecord::Base
  self.table_name = 'basins'
  self.primary_key = 'gid'

  has_many :sub_basins, foreign_key: :basin, class_name: 'SubBasin', primary_key: :basin
  has_many :captures
  has_many :recaptures
end

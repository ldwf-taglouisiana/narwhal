# == Schema Information
# Schema version: 20160113180221
#
# Table name: user_tag_requests
#
#  id             :integer          not null, primary key
#  number_of_tags :integer          not null
#  fulfilled      :boolean          default(FALSE)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  tag_type       :string(255)      not null
#  uuid           :string
#  angler_id      :integer          not null
#
# Foreign Keys
#
#  fk_rails_e812ee6b36  (angler_id => anglers.id)
#

class UserTagRequest < ActiveRecord::Base
  include Datatableable, UserPermissible
  belongs_to :angler
  has_many :notifications, :as => :notification_item

  validates :uuid, uniqueness: { scope: :angler_id }, unless: Proc.new { |b| b.uuid.blank? }

  before_destroy :remove_associated_notifications

  class << self
    # get a paginated list of anglers for the dataTables JS library
    def get_data_tables_list(options)

      options = Datatableable::DATA_TABLE_OPTIONS.merge options
      #fulfilled = true or fulfilled = false is the where
      where_clause = options[:where]



      # a list of sort columns that will correspond with the column index provided by the DataTable JS library
      columns = [
        "regexp_replace(anglers.angler_id, E'[0-9]+', ''), regexp_replace(anglers.angler_id, E'[^0-9]+', '', 'g')::bigint :sort_direction,  regexp_replace(anglers.angler_id, E'^[A-Za-z]+[0-9]+', '') :sort_direction",
        'first_name :sort_direction',
        'lower(tag_type) :sort_direction',
        'number_of_tags :sort_direction',
        'created_at :sort_direction'
      ]


      options = options.merge({
                                where: where_clause,
                                selects: "user_tag_requests.*, concat(anglers.first_name,' ', anglers.last_name) as angler_name, anglers.angler_id as angler_number",
                                joins: [
                                  'left join anglers on user_tag_requests.angler_id = anglers.id'
                                ],
                                order_column: columns,
                                klass: UserTagRequest
                              })

      as_data_table_result(options)


    end
  end

  private

    def remove_associated_notifications
      self.notifications.destroy_all
    end
end

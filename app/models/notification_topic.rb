# == Schema Information
#
# Table name: notification_topics
#
#  id         :integer          not null, primary key
#  topic      :string(255)      not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class NotificationTopic < ActiveRecord::Base
  has_many :notifications
  has_many :user_couriers
  has_many :users, through: :user_couriers

  # constants for the types of topics
  TOPICS = {
      :capture => 'New user capture',
      :recapture => 'New user recapture',
      :unregistered_recapture => 'New unregistered recapture',
      :user_tag_request => 'New user tag request',
      :user => 'New user'
  }

  # dynamic methods to check what type of topic this is
  TOPICS.each do |key, value|
    define_method("#{key}?") { self.topic == value }
  end

end
# == Schema Information
# Schema version: 20170724200101
#
# Table name: anglers
#
#  id                         :integer          not null, primary key
#  lag_no                     :integer
#  law_no                     :integer
#  first_name                 :string(255)
#  last_name                  :string(255)
#  street                     :string(255)
#  suite                      :string(255)
#  city                       :string(255)
#  state                      :string(255)
#  zip                        :string(255)
#  phone_number_1             :string(255)
#  phone_number_1_type        :string(255)
#  phone_number_2             :string(255)
#  phone_number_2_type        :string(255)
#  phone_number_3             :string(255)
#  phone_number_3_type        :string(255)
#  phone_number_4             :string(255)
#  phone_number_4_type        :string(255)
#  email                      :string(255)
#  tag_end_user_type          :integer
#  shirt_size                 :integer
#  deleted                    :boolean
#  lax_no                     :integer
#  email_2                    :string(255)
#  user_name                  :string(255)
#  comments                   :text
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  angler_id                  :string(20)       not null
#  search_vector              :tsvector         not null
#  prefered_incentive_item_id :integer
#
# Indexes
#
#  angler_id_uniq                                           (id) UNIQUE
#  index_anglers_on_angler_id_and_first_name_and_last_name  (angler_id,first_name,last_name)
#  index_anglers_on_search_vector                           (search_vector)
#
# Foreign Keys
#
#  ang_shirt_fk                             (shirt_size => shirt_size_options.id)
#  ang_tag_fk                               (tag_end_user_type => tag_end_user_types.id)
#  anglers_prefered_incentive_item_id_fkey  (prefered_incentive_item_id => items.id)
#

class Angler < ActiveRecord::Base
  include Datatableable, PgSearch, UserPermissible
  strip_attributes
  has_paper_trail

  # this is a runtime variable we are using to store the current user for the updates on the model
  attr_accessor :current_user

  has_one :user
  has_many :volunteer_time_logs
  belongs_to :shirt_size_option, foreign_key: :shirt_size

  has_many :captures
  has_many :recaptures
  has_many :draft_captures
  has_many :tags
  has_many :tag_assignments

  has_many :presentable_captures
  has_many :presentable_recaptures, foreign_key: :recapture_angler_id
  has_many :presentable_tags

  has_many :angler_items


  scope :with_kit, -> { where('"anglers"."angler_id" !~* \'LAX\'') }
  scope :active_during, -> (start_time, end_time) { with_kit.joins(:captures).merge(Capture.in_time_range(start_time, end_time)).uniq }
  scope :currently_active, -> { active_during((Time.now - 30.days), Time.now) }
  scope :order_by_angler_id, -> (direction = :asc) { order("regexp_replace(angler_id, E'[0-9]+', '') #{direction}, regexp_replace(angler_id, E'[^0-9]+', '', 'g')::bigint #{direction},  regexp_replace(angler_id, E'^[A-Za-z]+[0-9]+', '') #{direction}") }
  scope :duplicates, -> { group('first_name, last_name, email').having('count(*) > 1').select('first_name, last_name, count(*) as "count"').order(count: :desc) }
  scope :banquet, -> (options = { start_date: nil, end_date: nil }) {
    options = { start_date: nil, end_date: nil }.merge options.symbolize_keys
    where("#{Angler.table_name}.id in (#{Capture.in_time_range(options[:start_date] || Capture.this_season.first, options[:end_date]).select(:angler_id).uniq.to_sql})")
  }

  before_validation :capitalize_angler_id

  validates_uniqueness_of :angler_id, case_sensitive: false, message: '^Angler ID has already been taken'

  after_commit :update_related_search_records, unless: Proc.new { |a| a.id_changed? }

  # the PgSearch scope to search through the tsvector column
  pg_search_scope :search,
                  against: :search_vector,
                  using: {
                      tsearch: {
                          dictionary: 'english',
                          tsvector_column: 'search_vector'
                      }
                  }

  # this will be the trigger used to update the tsvector column for a tag.
  trigger.before(:insert, :update) do
    <<-SQL
      new.search_vector :=
        setweight(to_tsvector('pg_catalog.english', coalesce(new.angler_id, '')), 'A')  ||
        setweight(to_tsvector('pg_catalog.english', coalesce(new.last_name, '')), 'B')  ||
        setweight(to_tsvector('pg_catalog.english', coalesce(new.first_name, '')), 'B') ||
        setweight(to_tsvector('pg_catalog.english', coalesce(new.email, '')), 'C')
    SQL
  end

  def is_app_angler?
    !self.angler_id.nil? && self.angler_id.starts_with?('APP')
  end

  ##############################################
  # Class Methods
  ##############################################

  class << self

    def TEMP_ANGLER
      Angler.find(-1)
    end
    # test written
    def builder (params)
      angler = Angler.new(params)

      if params[:angler_id].blank?
        num = self.connection.execute('SELECT nextval(\'recapture_angler_sequence\')').first["nextval"]
        angler.angler_id = 'LAX' + num
      end

      angler.angler_id = angler.angler_id.gsub(/\s+/, '')

      angler
    end

    # test written
    def new_recapture_angler (params)
      angler = Angler.new(params)
      num = self.connection.execute('SELECT nextval(\'recapture_angler_sequence\')').first["nextval"]
      angler.angler_id = 'LAX' + num

      angler
    end

    # test written
    def new_app_angler (params)
      angler = Angler.new(params)
      num = self.connection.execute('SELECT nextval(\'app_angler_sequence\')').first["nextval"]
      angler.angler_id = 'APP' + num
      angler
    end

    def from_ambiguous_id!(angler_id)
      result = nil

      # prep the destination_angler
      if angler_id.nil?
        # do nothing
      elsif angler_id.is_a? Angler
        result = angler_id
      elsif angler_id.to_i > 0 # check that the id is wrapped in a string
        result = Angler.where(id: angler_id.to_i).first
      elsif angler_id.is_a? String
        result = Angler.where(angler_id: angler_id).first
      else
        raise Exception.new('The destination_angler needs to be a String, Angler, or Integer')
      end

      result
    end

    def from_ambiguous_id(angler_id)
      result = nil

      begin
        result = self.from_ambiguous_id!(angler_id)
      rescue Exception => e

      end

      result
    end


    #takes advantage of the fact that nil is 0 in ruby, returns the next available negative number
    def first_free_negative_lag_no
      Angler.where("lag_no < 0").order(:lag_no).collect { |e| e.lag_no }.first.to_i - 1
    end

    # get a paginated list of anglers for the dataTables JS library
    def get_data_tables_list(options)
      options = Datatableable::DATA_TABLE_OPTIONS.merge options
      
      # a list of sort columns that will correspond with the column index provided by the DataTable JS library
      columns = [
          "regexp_replace(angler_id, E'[0-9]+', '') :sort_direction, regexp_replace(angler_id, E'[^0-9]+', '', 'g')::bigint :sort_direction,  regexp_replace(angler_id, E'^[A-Za-z]+[0-9]+', '') :sort_direction",
          'first_name :sort_direction',
          'last_name :sort_direction',
          'street :sort_direction',
          'suite :sort_direction',
          'city :sort_direction',
          'state :sort_direction',
          'zip :sort_direction',
          'phone_number_1 :sort_direction',
          'email :sort_direction',
          'created_at :sort_direction'
      ]

      options = options.merge({
                                order_column: columns,
                                klass: Angler
                              })

      options[:order_column_index] -= 1


      as_data_table_result(options)
    end

    def autocomplete(parameters)
      #TODO fix this string to do this replace for all non alphanumeric characters
      term      = parameters[:term].strip
                      .gsub(/[^\w\s'&]/,'') # sub out any non white listed characters
                      .gsub(/\s+/, ' ') # reduce any duplicate spaces
                      .gsub(/&/,'\u38') # convert ampersands to tsvector compatible characters

      options   = parameters[:options]
      limit     = parameters[:limit] || 15

      terms = [term]
      terms = term.split(/\s+/).reject{|t| t.blank?} if options[:cascade]
      terms = ActiveRecord::Base.sanitize(terms.map{|t| "#{t}:*" }.join('|'))

      ts_vector = [
          "setweight(to_tsvector('english', coalesce(first_name,'')), 'B')",
          "setweight(to_tsvector('english', coalesce(last_name, '')), 'C')",
          "setweight(to_tsvector('english', coalesce(angler_id, '')), 'A')"
      ].join(' || ')

      ts_vector = "(#{ts_vector})"

      self
          .joins("CROSS JOIN to_tsquery(#{terms}) query")
          .where("query @@ #{ts_vector}")
          .select("anglers.*, ts_rank_cd(#{ts_vector}, query, 32) AS rank")
          .order('rank DESC')
          .order_by_angler_id
          .limit(limit)
    end
  end # end of class definitions

  ##############################################
  # Instance Methods
  ##############################################

  # Used to create the string to use for the Dymo label.
  #
  # @return [String] A String with "\n" separating the lines of the address
  def postal_address
    line1 = [self.first_name, self.last_name].compact.reject(&:empty?).join(' ')
    line4 = [[self.city, self.state].join(', '), self.zip].compact.reject(&:empty?).join(' ')

    [line1, self.street, self.suite, line4].compact.reject(&:empty?).join("\n")
  end

  # Used to create the string to display in the drop down list
  # for the autocomplete returns
  def autocomplete_display
    "#{self.angler_id} - #{self.first_name} #{self.last_name}"
  end

  def autocomplete_stuff
    "#{self.angler_id} - #{self.first_name} #{self.last_name}"
  end

  def full_name(last_first = false)
    if last_first
      "#{self.last_name}, #{self.first_name}"
    else
      "#{self.first_name} #{self.last_name}"
    end
  end

  def has_valid_email?
    email != nil && email != '' && email != 'fishtagging@ccalouisiana.com' && email.match(/^[\w\.=-]+@[\w\.-]+\.[\w]{2,3}$/)
  end

  def to_builder
    Jbuilder.new do |angler|
      angler.id id
      angler.angler_id angler_id
      angler.first_name first_name
      angler.last_name last_name
      angler.street street
      angler.suite suite
      angler.city city
      angler.state state
      angler.zip zip
      angler.phone phone_number_1
      angler.email email
      angler.entered created_at.strftime('%m/%d/%Y')
    end
  end

  def shirt_size_name
    self.shirt_size_option.try(:shirt_size_option)
  end

  def data_count
    # get the list of tables that have an angler_id column, excluding the anglers table
    (query = '') << <<-SQL
      select table_name
      from information_schema.columns
      where table_schema not in
      ('information_schema','pg_catalog')
        and column_name = 'angler_id'
        AND table_name != 'captures_old'
        AND table_name != 'capture_duplicates'
        AND table_name != 'volunteer_time_logs_bad'
        and table_name != 'anglers';
    SQL

    connection = ActiveRecord::Base.connection
    table_list = connection.execute(query)

    # set up the arrays to hold transient data
    selects = []
    dynamic_params = []

    # create the inner queries to get the number of rows in each column that has the given angler_id
    table_list.each do |table|
      selects << "(SELECT count(*) FROM #{table['table_name']} WHERE angler_id = ?)"
      dynamic_params << self.id
    end

    (query = '') << <<-SQL
      select #{selects.join(' + ')} as "count";
    SQL

    # sanitize the SQL and execute it
    sql = ActiveRecord::Base.send(:sanitize_sql_array, [query] + dynamic_params)
    result = ActiveRecord::Base.connection.execute(sql)[0]

    # create the response hash
    result['count'].to_i
  end

  # check to see if there are any outstanding welcome item requests in the angler_item table
  def requested_welcome_package?
    self.angler_items.where(angler_item_type: AnglerItemType.welcome_package).any?
  end

  # check to see if there are any incentive item requests
  def requested_ten_capture_incentive_item?
    self.angler_items.where(angler_item_type: AnglerItemType.ten_capture_incentive_item).any?
  end

  def create_capture_incentive_item_requests
    if self.captures.verified.count >= 10 && !self.requested_ten_capture_incentive_item?
      AnglerItem.create(angler: self, requested_at: Time.now, item: Item.incentive_item, angler_item_type: AnglerItemType.ten_capture_incentive_item)
    end
  end

  def possible_duplicate_anglers
    Angler.where('(first_name ~* ? AND last_name ~* ?)', self.first_name.strip, self.last_name.strip)
  end



  private

  def update_related_search_records
    # we only do this if the specific attributes that affect search in other records.
    # we are also going to bootstrap off of the angler data migration tool, since it
    # already searches for any tables that have the angler id records in them.
    if (self.changed & %w(angler_id first_name last_name email)).any?
      AnglerDataMigrationModule::AnglerDataMigration.from_angler_to_angler!(self.id, self.id, current_user.try(:id))
    end
  end

  # test written
  def capitalize_angler_id
    self.angler_id = self.angler_id.upcase
  end

end

# == Schema Information
#
# Table name: user_device_types
#
#  id          :integer          not null, primary key
#  description :string(255)
#  created_at  :datetime
#  updated_at  :datetime
#

class UserDeviceType < ActiveRecord::Base
  belongs_to :user_device

  # constants for the types
  TYPES = {
      :ios => 'ios',
      :android => 'android',
  }

  class << self
    def ios_type
      UserDeviceType.where(description: TYPES[:ios]).first
    end

    def android_type
      UserDeviceType.where(description: TYPES[:android]).first
    end
  end

end

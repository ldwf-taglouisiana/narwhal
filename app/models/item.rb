# == Schema Information
# Schema version: 20170724200101
#
# Table name: items
#
#  id         :integer          not null, primary key
#  name       :text             not null
#  is_public  :boolean          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Item < ActiveRecord::Base
  has_many :angler_items

  class << self
    def tagging_kit
      self.where(name: 'Tagging Kit').first
    end

    def incentive_item
      self.where(name: 'Generic T-Shirt').first
    end
  end

end

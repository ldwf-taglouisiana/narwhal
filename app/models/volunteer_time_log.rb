# == Schema Information
# Schema version: 20160113180221
#
# Table name: volunteer_time_logs
#
#  id            :integer          not null, primary key
#  start         :datetime         not null
#  end           :datetime         not null
#  verified      :boolean          default(FALSE)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  ldwf_entered  :boolean          default(FALSE)
#  ldwf_edited   :boolean          default(FALSE)
#  uuid          :string
#  recent        :boolean          default(TRUE), not null
#  search_vector :tsvector         not null
#  angler_id     :integer          not null
#
# Indexes
#
#  index_volunteer_time_logs_on_recent         (recent)
#  index_volunteer_time_logs_on_search_vector  (search_vector)
#
# Foreign Keys
#
#  fk_rails_b894ce2542  (angler_id => anglers.id)
#

class VolunteerTimeLog < ActiveRecord::Base
  include PgSearch, Datatableable
  strip_attributes

  has_one :presentable_volunteer_hour, foreign_key: :id
  belongs_to :angler

  validates_presence_of :angler_id, :start, :end

  validates :uuid, uniqueness: { scope: :angler_id }, unless: Proc.new { |b| b.uuid.blank? }

  validates :start, date: {
                      before:  Proc.new { |t| t.end },
                      after: Proc.new { |t| (Time.now - 1.years) },
                      before_message: 'Start time cannot be after the end time',
                      after_message: 'Start time cannot be before a year ago'
                  }
  validates :end, date: {
                    before: Proc.new { |t| Time.now },
                    after:  Proc.new { |t| t.start },
                    before_message: 'End time cannot be after today',
                    after_message: 'End time cannot be before the start time'
                }

  validates :start, :end, overlap: {scope: 'angler_id', message_title: 'Log time overlap', message_content: 'There is another entry for the angler that overlaps with this one.'}


  after_save :update_archived, on: :update, unless: Proc.new { |tag| tag.recent }

  scope :with_capture_count, -> { joins() }

  # the PgSearch scope to search through the tsvector column
  pg_search_scope :search,
                  against: :search_vector,
                  using: {
                      tsearch: {
                          dictionary: 'english',
                          tsvector_column: 'search_vector'
                      }
                  }

  # this will be the trigger used to update the tsvector column for a tag.
  trigger.before(:insert, :update).declare('angler record;') do
    <<-SQL
      SELECT * INTO angler FROM anglers WHERE id = new.angler_id;
      new.search_vector :=
        setweight(to_tsvector('pg_catalog.english', coalesce(angler.angler_id, '')), 'A')  ||
        setweight(to_tsvector('pg_catalog.english', coalesce(angler.email, '')), 'B')      ||
        setweight(to_tsvector('pg_catalog.english', coalesce(angler.first_name, '')), 'C') ||
        setweight(to_tsvector('pg_catalog.english', coalesce(angler.last_name, '')), 'B');
    SQL
  end

  class << self
    def data_tables_json(options = {})
      all = options[:all]
      options = Datatableable::DATA_TABLE_OPTIONS.merge options

      if all
        where_clause = 'TRUE'
        min_captures = 0
      else
        where_clause = 'verified IS TRUE'
        min_captures = 1
      end

      where_clause = [where_clause,'number_of_captures >= COALESCE(?,0)', options[:where]].compact.join(' AND ')

      # a list of sort columns that will correspond with the column index provided by the DataTable JS library
      columns = [
        "regexp_replace(angler_number, E'[0-9]+', '') :sort_direction, regexp_replace(angler_number, E'[^0-9]+', '', 'g')::bigint :sort_direction,  regexp_replace(angler_number, E'^[A-Za-z]+[0-9]+', '') :sort_direction",
        'lower(angler_name) :sort_direction',
        '"start" :sort_direction',
        '"end" :sort_direction',
        'total_time :sort_direction',
        'verified :sort_direction',
        'created_at :sort_direction',
        'number_of_captures :sort_direction',
      ]

      options = options.merge({
                                where: where_clause,
                                binds: [min_captures],
                                order_column: columns,
                                klass: PresentableVolunteerHour
                              })

      as_data_table_result(options)

    end
  end

  private

  def update_archived
    RefreshArchivedVolunteerHoursJob.perform_if_needed
  end
end

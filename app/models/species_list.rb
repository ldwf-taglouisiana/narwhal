# == Schema Information
#
# Table name: species_lists
#
#  id         :integer          not null, primary key
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class SpeciesList < ActiveRecord::Base
  has_many :species #, :order => "position"
  # attr_accessible :title, :body
end

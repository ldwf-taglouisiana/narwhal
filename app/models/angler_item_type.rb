# == Schema Information
# Schema version: 20170724200101
#
# Table name: angler_item_types
#
#  id          :integer          not null, primary key
#  name        :text             not null
#  description :text             not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class AnglerItemType < ActiveRecord::Base
  has_many :angler_items

  class << self

    def welcome_package
      self.where(name: 'Welcome Package').first
    end

    def ten_capture_incentive_item
      self.where(name: '10-Capture Incentive Item').first
    end


  end

end

# == Schema Information
#
# Table name: time_of_day_options
#
#  id                 :integer          not null, primary key
#  time_of_day_option :string(255)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  start_time         :datetime
#  end_time           :datetime
#

class TimeOfDayOption < ActiveRecord::Base
  has_many :captures
  has_many :recaptures
  has_many :draft_captures

  default_scope { order(start_time: :asc) }
  
  def time_of_day
    "#{self.id} - #{self.time_of_day_option}"
  end
end

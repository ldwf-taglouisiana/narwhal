# == Schema Information
# Schema version: 20160405165101
#
# Table name: tags
#
#  id              :integer          not null, primary key
#  tag_no          :string(255)
#  tag_lot_id      :integer
#  tag_request_id  :integer
#  deleted         :boolean
#  unassign_option :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  old_anlger_id   :integer
#  prefix          :string(255)
#  active          :boolean          default(TRUE)
#  assigned_at     :datetime
#  recent          :boolean          default(TRUE), not null
#  search_vector   :tsvector         not null
#  angler_id       :integer
#
# Indexes
#
#  index_tags_on_angler_id      (angler_id)
#  index_tags_on_recent         (recent)
#  index_tags_on_search_vector  (search_vector)
#  index_tags_on_tag_lot_id     (tag_lot_id)
#  index_tags_on_tag_no         (tag_no) UNIQUE
#  tags_prefix_idx              (prefix)
#
# Foreign Keys
#
#  fk_rails_e4069f0b05  (angler_id => anglers.id)
#  tag_lot_fk           (tag_lot_id => tag_lots.id)
#

class Tag < ActiveRecord::Base
  include Datatableable, PgSearch
  strip_attributes
  has_paper_trail :meta => {:activity_type => :action_taken}

  has_one :capture
  has_many :recaptures
  has_one :presentable_tag, foreign_key: :id
  has_one :archived_tag, foreign_key: :id
  belongs_to :angler
  belongs_to :tag_lot

  before_validation :set_defaults

  validates_presence_of :tag_no
  validates_uniqueness_of :tag_no, case_sensitive: false

  after_save :update_archived, on: :update, unless: Proc.new { |tag| tag.recent }

  scope :with_number, -> (tag_number) { where(tag_no: tag_number).where.not(tag_no: nil) }
  scope :in_use, -> { joins(:recaptures, :capture).uniq }
  scope :for_use, -> (params = {}) { params[:recapture] ? for_recapture_use : for_capture_use }
  scope :for_capture_use, -> { where.not(angler_id: nil).joins('LEFT JOIN "captures" on "captures"."tag_id" = "tags"."id"').where(captures: {id: nil}).for_recapture_use }
  scope :for_recapture_use, -> { joins('LEFT JOIN "anglers" on "anglers"."id" = "tags"."angler_id"').where(active: true).where.not(id: -1) }
  scope :order_by_number, -> (direction = :asc) { order("substring(\"tags\".\"tag_no\" from '[a-zA-Z]+') #{direction}, substring(\"tags\".\"tag_no\" from '[0-9]+') #{direction}") }
  scope :with_number_range, -> (first_tag, last_tag) do
    cond = <<-SQL
      (substring(tags.tag_no from '[a-zA-Z]+') ILIKE substring(? from '[a-zA-Z]+'))
        AND
      (substring(tags.tag_no from '[0-9]+')::int between substring(? from '[0-9]+')::int and substring(? from '[0-9]+')::int)
    SQL

    where(cond, first_tag.upcase, first_tag.upcase, last_tag.upcase)
  end

  # the PgSearch scope to search through the tsvector column
  pg_search_scope :search,
                  against: :search_vector,
                  using: {
                      tsearch: {
                          dictionary: 'english',
                          tsvector_column: 'search_vector'
                      }
                  }

  # this will be the trigger used to update the tsvector column for a tag.
  trigger.before(:insert, :update).declare('angler record;') do
    <<-SQL
      -- get the associated angler for this tag
      select * into angler from anglers where id = new.angler_id;

      new.search_vector :=
          setweight(to_tsvector('pg_catalog.english', coalesce(new.tag_no,'')), 'A')    ||
          setweight(to_tsvector('pg_catalog.english', coalesce(angler.angler_id,'')), 'B')  ||
          setweight(to_tsvector('pg_catalog.english', coalesce(angler.last_name,'')), 'C') ||
          setweight(to_tsvector('pg_catalog.english', coalesce(angler.first_name,'')), 'C') ||
          setweight(to_tsvector('pg_catalog.english', coalesce(angler.email,'')), 'D');
    SQL
  end

  ##############################################
  # Class Methods
  ##############################################

  class << self

    def assign_tags(angler_id, start_tag, end_tag, current_user)

      start_s = start_tag
      end_s = end_tag

      #---------------------------------------------------------------------------------------
      # PREP SOME OF THE PARAMS TO ENSURE THAT WE HAVE THE PROPER TYPES FOR THE SQL

      # prep the angler_id
      angler_id = Angler.from_ambiguous_id(angler_id).try(:id)
      start_tag = Tag.from_ambiguous_id(start_tag).try(:tag_no)
      end_tag   = Tag.from_ambiguous_id(end_tag).try(:tag_no)

      #---------------------------------------------------------------------------------------

      time_string = Time.now.iso8601

      begin

        if start_tag.nil?
          raise StandardError.new("The specified start tag #{start_s.to_s} does not exist.")
        elsif end_tag.nil?
          raise StandardError.new("The specified end tag #{end_s.to_s} does not exist.")
        elsif angler_id != nil && Tag.where(tag_no: start_tag..end_tag).where.not(angler_id: nil).exists?
          raise StandardError.new("Some of the listed tags: #{start_tag} - #{end_tag} are still assigned to anglers")
        end

        Tag.transaction do
          action = angler_id.nil? ? 'unassignment' : 'assignment'
          assigned_at = angler_id.nil? ? nil : time_string

          query = <<-SQL
          -- set the versions
          INSERT into versions (item_id, item_type, event, whodunnit, created_at, activity_type, object_changes) (
              SELECT
                tags.id,
                'Tag',
                'update',
                ?,
                ?,
                ?,
                (
                  select
                    to_json(t)::jsonb
                  FROM (
                    select
                      ARRAY[angler_id, ?] as "angler_id",
                      ARRAY[updated_at, ?] as "updated_at",
                      ARRAY[assigned_at, ?] as "assigned_at"
                  )t
                )
              FROM
                tags
              WHERE (
                (substring(tags.tag_no from '[a-zA-Z]+') ILIKE substring(? from '[a-zA-Z]+'))
                  AND
                (substring(tags.tag_no from '[0-9]+')::int between substring(? from '[0-9]+')::int and substring(? from '[0-9]+')::int)
              )
            );

            -- update the tags table
            update tags
            set angler_id = ?, updated_at = ?, assigned_at = ?
            where (
              (substring(tags.tag_no from '[a-zA-Z]+') ILIKE substring(? from '[a-zA-Z]+'))
                AND
              (substring(tags.tag_no from '[0-9]+')::int between substring(? from '[0-9]+')::int and substring(? from '[0-9]+')::int) );
          SQL

          sanitized_sql = [query]
          sanitized_sql += [current_user.id, time_string, action, angler_id, time_string, assigned_at, start_tag, start_tag, end_tag] # versions params
          sanitized_sql += [angler_id, time_string, time_string, start_tag, start_tag, end_tag] # tag update params
          clean_sql = send(:sanitize_sql_array, sanitized_sql)

          connection.execute(clean_sql)

          # check if the tags were "archived" tags, if so, then need to refresh the materialized view
          if Tag.with_number_range(start_tag, end_tag).joins(:archived_tag).any?
            RefreshArchivedTagsJob.perform_if_needed
          end

        end
      rescue Exception => e
        raise Exception.new "Could not alter some of the tags: #{e.message}"
      end
    end

    def unassign_tags(start_tag, end_tag, current_user)
      self.assign_tags(nil, start_tag, end_tag, current_user)
    end

    # get a paginated list of tags for the dataTables JS library
    def get_data_tables_list(options = {})
      options = Datatableable::DATA_TABLE_OPTIONS.merge options

      # a list of sort columns that will correspond with the column index provided by the DataTable JS library
      columns = [
          'tag_no :sort_direction',
          "regexp_replace(angler_number, E'[0-9]+', '') :sort_direction NULLS LAST, regexp_replace(angler_number, E'[^0-9]+', '', 'g')::bigint :sort_direction NULLS LAST,  regexp_replace(angler_number, E'^[A-Za-z]+[0-9]+', '') :sort_direction NULLS LAST",
          'angler_name :sort_direction',
          'created_at :sort_direction',
          'assigned_at :sort_direction NULLS LAST',
          'used :sort_direction'
      ]

      options = options.merge({
                                  order_column: columns,
                                  klass: PresentableTag
                              })

      options[:order_column_index]
      as_data_table_result(options)
    end

    # Used the search for recaptures
    # @param [Hash] parameters The set of parameters from the autocomplete gem
    def autocomplete(parameters)
      term = parameters[:term].strip
      limit = parameters[:limit] || 15
      is_recapture = (parameters[:extra][:recapture] == '1')

      self
          .for_use(recapture: is_recapture)
          .where('tag_no ~* ?', term)
          .select(
              [
                  'tags.*',
                  'concat(anglers.first_name, \' \', anglers.last_name) as angler_name'
              ]
          )
          .order(tag_no: :asc)
          .limit(limit)
    end

    # @param [Hash] parameters The set of parameters from the autocomplete gem
    def autocomplete_any(parameters)
      self.autocomplete(parameters.deep_merge({extra: {recapture: '1'}}))
    end

    def from_ambiguous_id(tag_id)
      result = nil

      # prep the angler_id
      if tag_id.to_i > 0
        result = Tag.where(id: tag_id.to_i).first
      elsif tag_id.is_a?(String)
        result = Tag.where(tag_no: tag_id).first
        # don't do anything since we have the type we need
      elsif tag_id.is_a?(Tag)
        result = tag_id
      else
        raise Exception.new('The end_tag needs to be a String, Tag, or Integer')
      end

      result
    end
  end # end class << self

  ##############################################
  # Instance Methods
  ##############################################

  def tag_number
    self.tag_no
  end

  def angler_name
    self.angler.try(:full_name)
  end

  def autocomplete_stuff
    "#{self.tag_no}"
  end

  # Find the next capture in the tag number sequence after this tag number
  def get_next_capture
    next_tag = self.surrounding_tags.first
    next_tag.capture || next_tag.recaptures.first
  end

  # Find the previous tag in the sequence
  def get_previous_capture
    next_tag = self.surrounding_tags.last
    next_tag.capture || next_tag.recaptures.first
  end

  # gets the tags that precede and follow this tag in alpha-numeric sequence
  # the result is constructed such that it only returns 2 tags, the first will always
  # be the next tag in sequence and the last tag is the previous tag in sequence
  def surrounding_tags
    query = <<-SQL
      (
        -- get all the tags after self
        SELECT
          "tags".*
        FROM
          "tags"
          LEFT JOIN "captures" ON "captures"."tag_id" = "tags"."id"
          LEFT JOIN "recaptures" ON "recaptures"."tag_id" = "tags"."id"
        WHERE
          (tag_no > ?) -- tags after self
          AND
          (captures.id is not null OR recaptures.id is not null)
        ORDER BY
          tag_no ASC
        LIMIT 1
      )
      UNION
      (
        -- get all the tags before self
        SELECT
          "tags".*
        FROM
          "tags"
          LEFT JOIN "captures" ON "captures"."tag_id" = "tags"."id"
          LEFT JOIN "recaptures" ON "recaptures"."tag_id" = "tags"."id"
        WHERE
          (tag_no < ?) -- tags before self
          AND
          (captures.id is not null OR recaptures.id is not null)
        ORDER BY
          tag_no DESC
        LIMIT 1
      )
      -- only get the first row
      ORDER BY
        tag_no DESC
    SQL

    Tag.find_by_sql([query, self.tag_no, self.tag_no])
  end

  def assigned_on
    self.updated_at unless self.angler.nil?
  end

  def used?
    self.capture || self.recaptures.any?
  end

  def resent?
    self.recent
  end

  def archived?
    self.recent == false
  end

  def to_builder
    Jbuilder.new do |json|
      json.number self.tag_no
      json.angler_id self.angler.try(:angler_id)
      json.angler_name self.angler.try(:full_name)
      json.entered self.created_at.try(:in_time_zone, 'Central Time (US & Canada)').try(:strftime, '%m/%d/%Y')
      json.assigned self.assigned_on.try(:in_time_zone, 'Central Time (US & Canada)').try(:strftime, '%m/%d/%Y')
      json.active self.active
      json.used self.used?
    end
  end

  def action_taken
    if self.versions.empty?
      'create'
    elsif self.angler_id.nil?
      'unassignment'
    else
      'assignment'
    end
  end

  private

  def update_archived
    RefreshArchivedTagsJob.perform_if_needed
  end

  def set_defaults
    self.recent = true

    true
  end
end

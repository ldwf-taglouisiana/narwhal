# == Schema Information
# Schema version: 20180301154137
#
# Table name: angler_items
#
#  id                  :integer          not null, primary key
#  angler_id           :integer          not null
#  item_id             :integer          not null
#  angler_item_type_id :integer          not null
#  requested_at        :datetime
#  fulfilled_at        :datetime
#  comment             :text
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#
# Indexes
#
#  idx_angler_items_angler_id            (angler_id)
#  idx_angler_items_angler_item_type_id  (angler_item_type_id)
#  idx_angler_items_item_id              (item_id)
#
# Foreign Keys
#
#  angler_items_angler_id_fkey            (angler_id => anglers.id)
#  angler_items_angler_item_type_id_fkey  (angler_item_type_id => angler_item_types.id)
#  angler_items_item_id_fkey              (item_id => items.id)
#

class AnglerItem < ActiveRecord::Base
  include Datatableable, UserPermissible

  belongs_to :item 
  belongs_to :angler
  belongs_to :angler_item_type

  class << self
    def datatable_list(options = {})
      unsent = options[:unsent]
      where_clause = unsent ? 'presentable_angler_items.fulfilled_at is null' : nil
      options = Datatableable::DATA_TABLE_OPTIONS.merge options
      # a list of sort columns that will correspond with the column index provided by the DataTable JS library
      columns = [
        "regexp_replace(angler_number, E'[0-9]+', '') :sort_direction NULLS LAST, regexp_replace(angler_number, E'[^0-9]+', '', 'g')::bigint :sort_direction NULLS LAST,
         regexp_replace(angler_number, E'^[A-Za-z]+[0-9]+', '') :sort_direction NULLS LAST",
        'angler_name :sort_direction',
        'item_name :sort_direction',
        'item_type :sort_direction',
        'requested_at :sort_direction',
        'fulfilled_at :sort_direction NULLS FIRST'
      ]

      options = options.merge({
                                where: where_clause,
                                order_column: columns,
                                klass: PresentableAnglerItem
                              })

      options[:order_column_index] -= 1

      as_data_table_result(options)
    end
  end
  
end

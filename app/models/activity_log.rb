# == Schema Information
# Schema version: 20160405165101
#
# Table name: activity_logs
#
#  id                   :integer          not null, primary key
#  activity_log_type_id :integer          not null
#  old_item_id          :string(255)
#  user_id              :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  activity_item_id     :integer          not null
#  activity_item_type   :string(255)      not null
#
# Foreign Keys
#
#  alog_type_fk  (activity_log_type_id => activity_log_types.id)
#

class ActivityLog < ActiveRecord::Base
  strip_attributes

  belongs_to :user
  belongs_to :activity_log_type

  belongs_to :activity_item, polymorphic: true

  class << self

    def new_create_capture(user, item)
      ActivityLog.create(user: user, activity_item: item, activity_log_type: ActivityLogType.create_capture_type)
    end

    def new_update_capture(user, item)
      ActivityLog.create(user: user, activity_item: item, activity_log_type: ActivityLogType.update_capture_type)
    end

    def new_create_recapture(user, item)
      ActivityLog.create(user: user, activity_item: item, activity_log_type: ActivityLogType.create_recapture_type)
    end

    def new_update_recapture(user, item)
      ActivityLog.create(user: user, activity_item: item, activity_log_type: ActivityLogType.update_recapture_type)
    end

    def new_create_angler(user, item)
      ActivityLog.create(user: user, activity_item: item, activity_log_type: ActivityLogType.create_angler_type)
    end

    def new_update_angler(user, item)
      ActivityLog.create(user: user, activity_item: item, activity_log_type: ActivityLogType.update_angler_type)
    end

    def new_assign_tag(user, item)
      ActivityLog.create(user: user, activity_item: item, activity_log_type: ActivityLogType.assign_tag_type)
    end

    def new_unassign_tag(user, item)
      ActivityLog.create(user: user, activity_item: item, activity_log_type: ActivityLogType.unassign_tag_type)
    end

    def new_create_tag(user, item)
      ActivityLog.create(user: user, activity_item: item, activity_log_type: ActivityLogType.create_tag_type)
    end

    def new_update_tag(user, item)
      ActivityLog.create(user: user, activity_item: item, activity_log_type: ActivityLogType.update_tag_type)
    end

  end

end

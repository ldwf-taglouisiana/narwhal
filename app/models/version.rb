# == Schema Information
# Schema version: 20160405165101
#
# Table name: versions
#
#  id             :integer          not null, primary key
#  item_type      :string           not null
#  item_id        :integer          not null
#  event          :string           not null
#  whodunnit      :integer
#  object         :json
#  created_at     :datetime
#  activity_type  :text
#  object_changes :jsonb
#  meta           :jsonb
#
# Indexes
#
#  index_versions_on_item_type_and_item_id                   (item_type,item_id)
#  index_versions_on_whodunnit_and_created_at_and_item_type  (whodunnit,created_at,item_type)
#

class Version < PaperTrail::Version
  belongs_to :user, foreign_key: :whodunnit
end

# == Schema Information
# Schema version: 20150817164549
#
# Table name: roles
#
#  id            :integer          not null, primary key
#  name          :string(255)
#  resource_id   :integer
#  resource_type :string(255)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
# Indexes
#
#  index_roles_on_name                                    (name)
#  index_roles_on_name_and_resource_type_and_resource_id  (name,resource_type,resource_id)
#

class Role < ActiveRecord::Base
  scopify

  belongs_to :resource, :polymorphic => true

  has_many :roles_users
  has_many :user, through: :roles_users

  class << self
    def admin_role
      self.where(name: :admin).first
    end

    def ldwf_role
      self.where(name: :ldwf).first
    end

    def angler_role
      self.where(name: :angler).first
    end

    def volunteer_role
      self.where(name: :volunteer).first
    end
  end
end

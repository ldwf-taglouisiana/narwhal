# == Schema Information
#
# Table name: fish_condition_options
#
#  id                    :integer          not null, primary key
#  fish_condition_option :string(255)
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#

class FishConditionOption < ActiveRecord::Base

  def fish_condition
    "#{self.id} - #{self.fish_condition_option}"
  end
end

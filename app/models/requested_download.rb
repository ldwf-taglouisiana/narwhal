# == Schema Information
#
# Table name: requested_downloads
#
#  id           :integer          not null, primary key
#  token        :text             not null
#  filename     :text             not null
#  filepath     :text
#  should_email :boolean          default(FALSE)
#  created_at   :datetime
#  updated_at   :datetime
#  user_id      :integer          not null
#

class RequestedDownload < ActiveRecord::Base
  belongs_to :user

  before_validation :gen_token

  validates :token, presence: true
  validates :filename, presence: true
  validates :user_id, presence: true

  # when the record is deleted, ensure that the file is also deleted
  before_destroy :delete_file

  # clear all downloads after 24 hours
  def self.clear_expired_downloads
    expired_downloads = RequestedDownload.where('(now() - updated_at) >= \'24 hours\'::interval')
    expired_downloads.destroy_all
  end

  def mark_for_emailing(should_email = false)
    # update self with the new email params
    self.update_attributes({should_email: should_email})

    # send the email, will only do so if it is ready
    self.send_email
  end

  # check to see if the file has been created and is ready for download
  def is_ready?
    self.filepath != nil
  end

  # checks that the link to the file can be emailed
  def is_sendable?
    self.user_id != nil and self.should_email
  end

  # send an email to the user that requested the file, if it can be emailed
  def send_email
    if self.is_ready? and self.is_sendable?
      DownloadEmailJob.perform_later(self)
    end
  end

  def gen_token
    if self.token.nil?
      # create the token
      token = SecureRandom.hex(32)

      # if the token was already taken, then regenerate
      while RequestedDownload.where(token: token).count > 0
        token = SecureRandom.hex(32)
      end

     self.token = token
    end
  end

  private

  def delete_file
    File.delete(self.filepath) if self.filepath && File.exists?(self.filepath)
  end
end

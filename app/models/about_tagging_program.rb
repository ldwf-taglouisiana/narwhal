# == Schema Information
#
# Table name: about_tagging_programs
#
#  id          :integer          not null, primary key
#  description :text             not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class AboutTaggingProgram < ActiveRecord::Base
  strip_attributes
end

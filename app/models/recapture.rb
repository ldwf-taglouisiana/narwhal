# == Schema Information
# Schema version: 20160113180221
#
# Table name: recaptures
#
#  id                                 :integer          not null, primary key
#  capture_date                       :datetime         not null
#  length                             :decimal(, )
#  location_description               :text
#  latitude                           :float
#  longitude                          :float
#  comments                           :text
#  verified                           :boolean          default(FALSE)
#  user_capture                       :boolean          default(FALSE)
#  entered_gps_type                   :string(255)
#  tag_id                             :integer
#  species_id                         :integer
#  fish_condition_option_id           :integer
#  recapture_disposition_id           :integer
#  time_of_day_option_id              :integer
#  created_at                         :datetime         not null
#  updated_at                         :datetime         not null
#  geom                               :geometry(Point,4
#  species_length_id                  :integer
#  basin_id                           :integer
#  sub_basin_id                       :integer
#  gps_format_id                      :integer
#  capture_id                         :integer
#  recent                             :boolean          default(TRUE), not null
#  search_vector                      :tsvector         not null
#  fish_event_location_description_id :integer
#  angler_id                          :integer          not null
#
# Indexes
#
#  idx_recaptures_created_at                     (created_at)
#  index_recaptures_on_capture_id                (capture_id)
#  index_recaptures_on_fish_condition_option_id  (fish_condition_option_id)
#  index_recaptures_on_gps_format_id             (gps_format_id)
#  index_recaptures_on_recent                    (recent)
#  index_recaptures_on_search_vector             (search_vector)
#  index_recaptures_on_species_id                (species_id)
#  index_recaptures_on_species_length_id         (species_length_id)
#  index_recaptures_on_tag_id                    (tag_id)
#  index_recaptures_on_time_of_day_option_id     (time_of_day_option_id)
#  recaptures_date_idx                           (capture_date)
#  recaptures_geom_idx                           (geom)
#  recaptures_length_idx                         (length)
#  recaptures_tag_idx                            (tag_id)
#
# Foreign Keys
#
#  fk_rails_24510c6ae4         (capture_id => captures.id)
#  fk_rails_45858b5865         (gps_format_id => gps_formats.id)
#  fk_rails_782717f62d         (angler_id => anglers.id)
#  fk_recaptures_gps_type      (entered_gps_type => gps_formats.format_type)
#  recap_fish_cond_fk          (fish_condition_option_id => fish_condition_options.id)
#  recap_recap_dis_fk          (recapture_disposition_id => recapture_dispositions.id)
#  recap_species_fk            (species_id => species.id)
#  recap_tag_fk                (tag_id => tags.id)
#  recap_time_day_fk           (time_of_day_option_id => time_of_day_options.id)
#  recaptures_basin_id_fk      (basin_id => basins.gid)
#  recaptures_sub_basin_id_fk  (sub_basin_id => "sub-basin".gid)
#

class Recapture < ActiveRecord::Base

  include Captureable, FishEntryable, Datatableable, PgSearch, FishEventUtils, UserPermissible
  resourcify
  strip_attributes
  has_paper_trail

  has_one :presentable_recapture, foreign_key: :id
  belongs_to :recapture_disposition
  belongs_to :capture

  scope :with_capture_by, -> (angler) { joins(:capture).where(captures: { angler_id: angler.id }) }

  before_validation :set_defaults
  before_validation :set_capture_id

  validates_uniqueness_of :tag_id, scope: [:capture_date, :time_of_day_option_id, :angler_id, :species_id, :fish_condition_option_id, :latitude, :longitude, :entered_gps_type], case_sensitive: false, message: '^Duplicate Recapture'

  before_save :check_capture_species

  after_create :send_recapture_reports

  after_commit :touch_capture, if:  Proc.new { |recapture| recapture.capture_id.present? }
  after_commit :update_archived, if: Proc.new { |recapture| ArchivedRecapture.where(id: recapture.id).any? }

  # the PgSearch scope to search through the tsvector column
  pg_search_scope :search,
                  against: :search_vector,
                  using: {
                      tsearch: {
                          dictionary: 'english',
                          tsvector_column: 'search_vector',
                          prefix: true
                      }
                  }

  # this will be the trigger used to update the tsvector column for a tag.
  trigger.before(:insert, :update).declare('angler record; tag record;') do
    <<-SQL
      -- get the associated angler for this tag
      select * into angler from anglers where id = new.angler_id;
      select * into tag from tags where id = new.tag_id;

      new.search_vector :=
        setweight(to_tsvector('pg_catalog.english', coalesce(tag.tag_no, '')), 'A')     ||
        setweight(to_tsvector('pg_catalog.english', coalesce(angler.angler_id, '')), 'B')  ||
        setweight(to_tsvector('pg_catalog.english', coalesce(angler.last_name, '')), 'C')  ||
        setweight(to_tsvector('pg_catalog.english', coalesce(angler.first_name, '')), 'C') ||
        setweight(to_tsvector('pg_catalog.english', coalesce(angler.email, '')), 'D')
    SQL
  end

  #####################################################
  ## Class methods
  #####################################################

  class << self

    def datatable_list(options={})
      verified = options[:verified]
      options = Datatableable::DATA_TABLE_OPTIONS.merge options
      table_name = PresentableRecapture.table_name

      where_clause = verified ? "#{table_name}.verified IS TRUE" : "#{table_name}.verified IS FALSE"
      where_clause = [where_clause, options[:where]]
      where_clause << "id in (#{Recapture.search(options[:search_term]).select(:id).to_sql})" unless options[:search_term].blank?
      where_clause = where_clause.compact.join(' AND ')
      
      # a list of sort columns that will correspond with the column index provided by the DataTable JS library
      columns = [
        "regexp_replace(#{table_name}.recapture_angler_number, E'[0-9]+', '') :sort_direction, regexp_replace(#{table_name}.recapture_angler_number, E'[^0-9]+', '', 'g')::bigint :sort_direction,  regexp_replace(#{table_name}.recapture_angler_number, E'^[A-Za-z]+[0-9]+', '') :sort_direction",
        "#{table_name}.recapture_angler_name :sort_direction",
        "#{table_name}.tag_number :sort_direction",
        "#{table_name}.date_string :sort_direction",
        "#{table_name}.species_name :sort_direction",
        "#{table_name}.length :sort_direction",
        "#{table_name}.latitude :sort_direction",
        "#{table_name}.longitude :sort_direction",
        "#{table_name}.fish_condition_option_id :sort_direction",
        "#{table_name}.recapture_disposition :sort_direction",
        "#{table_name}.time_of_day_option_id :sort_direction",
        "#{table_name}.created_at :sort_direction"
      ]

      image_count_sql = <<-SQL
        (
          SELECT
            count(*) > 0
          FROM
            fish_entry_photos
          WHERE
            fish_entry_photoable_type = 'Recapture'
            AND
            fish_entry_photoable_id = #{table_name}.id
        ) as has_images
      SQL

      options = options.merge({
                                where: where_clause,
                                selects: ["#{table_name}.*", " to_char(#{table_name}.created_at, 'MM/DD/YYYY') as entered", image_count_sql],
                                order_column: columns,
                                klass: (verified ? PresentableRecapture : PresentableRecapture.as_unverified)
                              })

      options[:order_column_index] -= 1

      as_data_table_result(options)
    end

  end

  #####################################################
  ## Instance methods
  #####################################################


  def table_name
    'recaptures'
  end

  def other_events
    ([self.capture] + Recapture.where(tag_id: self.tag_id).where.not(id: self.id).order(capture_date: :desc)).compact
  end

  def days_since_original_capture
    return nil if self.capture.nil?
    ((self.capture_date - self.capture.capture_date)/1.day).round
  end

  def distance_from_original_capture
    capture = self.capture
    return nil if capture.nil? || capture.latitude.nil? || capture.longitude.nil? || self.latitude.nil? || self.longitude.nil?
    Geocoder::Calculations.distance_between([capture.latitude, capture.longitude], [self.latitude, self.longitude])
  end

  private

  def touch_capture
    self.capture.try(:touch)
  end

  def update_archived
    RefreshArchivedRecaptureJob.perform_if_needed

    if ArchivedCapture.where(tag_id: self.tag_id).any?
      RefreshArchivedCaptureJob.perform_if_needed
    end
  end

  def set_defaults
    self.recent = true

    true
  end

  def set_capture_id
    parent_capture = Capture.where(tag_id: self.tag_id).first
    self.capture_id = parent_capture.try(:id)

    true
  end

  def check_capture_species
    if related_capture = Capture.where(tag_id: self.tag_id).first
      errors.add(:base, 'Species differs from the initial capture') if related_capture.species_id != self.species_id
    end
  end

  def send_recapture_reports
    UserReportEmailJob.perform_later(self)
  end

end

# == Schema Information
#
# Table name: manufacturers
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  street     :string(255)
#  city       :string(255)
#  state      :string(255)
#  country    :string(255)
#  zip        :string(255)
#  contact    :string(255)
#  phone      :string(255)
#  fax        :string(255)
#  email      :string(255)
#  deleted    :boolean
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Manufacturer < ActiveRecord::Base
  has_many :tag_lots
end

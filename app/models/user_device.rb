# == Schema Information
# Schema version: 20150817164549
#
# Table name: user_devices
#
#  id                   :integer          not null, primary key
#  device_token         :string(255)      not null
#  user_id              :integer          not null
#  user_device_types_id :integer          not null
#  created_at           :datetime
#  updated_at           :datetime
#
# Indexes
#
#  index_user_devices_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_user_devices_device_type_id  (user_device_types_id => user_device_types.id)
#  fk_user_devices_user_id         (user_id => users.id)
#

class UserDevice < ActiveRecord::Base
  belongs_to :user
  has_one :user_device_type

  validates_presence_of :user, :device_token, :user_device_type
  validates_uniqueness_of :device_token
end

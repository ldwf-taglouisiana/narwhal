# == Schema Information
# Schema version: 20170724200101
#
# Table name: users
#
#  id                                  :integer          not null, primary key
#  email                               :string(255)      default(""), not null
#  encrypted_password                  :string(255)      default(""), not null
#  reset_password_token                :string(255)
#  reset_password_sent_at              :datetime
#  remember_created_at                 :datetime
#  sign_in_count                       :integer          default(0)
#  current_sign_in_at                  :datetime
#  last_sign_in_at                     :datetime
#  current_sign_in_ip                  :string(255)
#  last_sign_in_ip                     :string(255)
#  created_at                          :datetime         not null
#  updated_at                          :datetime         not null
#  first_name                          :string(255)      not null
#  last_name                           :string(255)      not null
#  authentication_token                :string(255)
#  verified                            :boolean          default(FALSE)
#  receive_daily_capture_email_summary :boolean          default(FALSE)
#  contacted                           :boolean          default(FALSE)
#  hidden                              :boolean          default(FALSE)
#  comments                            :text
#  search_vector                       :tsvector         not null
#  angler_id                           :integer
#  is_existing_angler                  :boolean
#
# Indexes
#
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#  index_users_on_search_vector         (search_vector)
#
# Foreign Keys
#
#  fk_rails_c2453f5ff0  (angler_id => anglers.id)
#

class User < ActiveRecord::Base

  include PgSearch
  include UserPermissible
  include Datatableable

  rolify

  devise :database_authenticatable,
         :registerable,
         :recoverable,
         :rememberable,
         :trackable,
         :validatable

  belongs_to :angler

  has_many :devices, dependent: :destroy
  has_many :user_devices, dependent: :destroy
  has_many :user_couriers, inverse_of: :user, dependent: :destroy
  has_many :news_items, dependent: :destroy
  has_many :requested_downloads, dependent: :destroy

  has_many :notifications, dependent: :destroy
  has_many :notification_topics_users, inverse_of: :user, dependent: :destroy
  has_many :notification_topics, through: :notification_topics_users

  has_many :roles_users, inverse_of: :user, dependent: :destroy
  has_many :roles, through: :roles_users

  has_many :versions, foreign_key: :whodunnit

  default_scope { order(last_name: :asc) }
  scope :with_logged_activity, -> { find_by_sql(<<-SQL
                                                  SELECT DISTINCT ON (U.id)
                                                    U.*
                                                  FROM (
                                                    SELECT DISTINCT ON (users.id)
                                                      users.*
                                                    FROM users
                                                      CROSS JOIN versions
                                                    WHERE users.id = versions.whodunnit
                                                    GROUP BY users.id
                                                  ) U INNER JOIN roles_users ON U.id = roles_users.user_id
                                                    LEFT JOIN roles ON roles_users.role_id = roles.id
                                                  WHERE
                                                    roles.name = 'admin'
                                                    OR
                                                    roles.name = 'ldwf'
                                                SQL
                                                )
                                  }
  scope :admin,       -> { joins(:roles).where(roles: { name: :admin}).uniq  }
  scope :ldwf,        -> { joins(:roles).where(roles: { name: :ldwf}).uniq  }
  scope :angler,      -> { joins(:roles).where(roles: { name: :angler}).uniq }
  scope :volunteer,   -> { joins(:roles).where(roles: { name: :volunteer}).uniq }
  scope :visible_app, -> { joins(:angler).includes(:roles).where('hidden = false AND "anglers"."angler_id" ~* \'APP\'') }
  scope :with_role_list, -> (options =  { with_search: false }) {
    # this adds the "roles_list" attribute to the user model.
    # we have an option to indicate this action is happening in a chaiend PgSearch scope, which adds the necessary
    # group by stuff
    joins('LEFT JOIN "roles_users" ON "roles_users"."user_id" = "users"."id" LEFT JOIN "roles" ON "roles"."id" = "roles_users"."role_id"')
        .group( options[:with_search] ? "\"users\".id, \"#{PgSearch::Configuration.alias('users')}\".rank, \"users\".last_name " : :id )
        .select("users.*,array_to_string(array_agg(roles.name), ',') AS roles_list")
  }

  validates_presence_of :first_name, :last_name, :email
  validates_uniqueness_of :email, :case_sensitive => false

  accepts_nested_attributes_for :roles_users, allow_destroy: true
  accepts_nested_attributes_for :notification_topics_users, allow_destroy: true
  accepts_nested_attributes_for :user_couriers, allow_destroy: true

  after_save :create_welcome_item_requests


  # the PgSearch scope to search through the tsvector column
  pg_search_scope :search,
                  against: :search_vector,
                  using: {
                      tsearch: {
                          dictionary: 'english',
                          tsvector_column: 'search_vector',
                          prefix: true

                  }
                  }

  # this will be the trigger used to update the tsvector column for a tag.
  trigger.before(:insert, :update).declare('angler record;') do
    <<-SQL
      SELECT * INTO angler FROM anglers WHERE id = new.angler_id;
      new.search_vector :=
        setweight(to_tsvector('pg_catalog.english', coalesce(new.email, '')), 'A')      ||
        setweight(to_tsvector('pg_catalog.english', coalesce(angler.angler_id, '')), 'A')  ||
        setweight(to_tsvector('pg_catalog.english', coalesce(new.last_name, '')), 'B')  ||
        setweight(to_tsvector('pg_catalog.english', coalesce(new.first_name, '')), 'B') ||
        setweight(to_tsvector('pg_catalog.english', coalesce(angler.email, '')), 'C')      ||
        setweight(to_tsvector('pg_catalog.english', coalesce(angler.last_name, '')), 'D')  ||
        setweight(to_tsvector('pg_catalog.english', coalesce(angler.first_name, '')), 'D');
    SQL
  end

  class << self

    def emails_for_daily_capture_summary
      (User.ldwf.where(receive_daily_capture_email_summary: true) && User.admin.where(receive_daily_capture_email_summary: true)).map {|u|  "#{u.first_name} #{u.last_name} <#{u.email}>" }
    end

    def index_list(user)
      user_list = {}
      user_list[:ldwf] = User.accessible_by_user(user,:read).includes(:angler).ldwf
      user_list[:anglers] = User.accessible_by_user(user,:read).includes(:angler).angler
      user_list[:unverified] = User.accessible_by_user(user,:read).includes(:angler).visible_app.where(verified: false)
      user_list[:admin] = User.accessible_by_user(user,:read).includes(:angler).admin
      user_list
    end

    def get_data_tables_list(options)
      where_clause = [options[:verified] ? "users.verified IS TRUE" : "users.verified IS FALSE"]
      where_clause << "users.id in (#{User.search(options[:search_term]).select(:id).to_sql})" unless options[:search_term].blank?
      where_clause = where_clause.compact.flatten.join(' AND ')

      options = Datatableable::DATA_TABLE_OPTIONS.merge options



      columns = [
          'users.first_name :sort_direction',
          'users.last_name :sort_direction',
          'users.email :sort_direction',
          "regexp_replace(anglers.angler_id, E'[0-9]+', '') :sort_direction, regexp_replace(anglers.angler_id, E'[^0-9]+', '', 'g')::bigint :sort_direction,  regexp_replace(anglers.angler_id, E'^[A-Za-z]+[0-9]+', '') :sort_direction",
          'users.created_at :sort_direction',
          'contacted :sort_direction'
      ]

      options = options.merge({ where: where_clause,
                                order_column: columns,
                                selects: [
                                    '*',
                                    "anglers.angler_id as angler_number",
                                    "anglers.id as angler_id",
                                    "users.id as user_id"
                                ],
                                klass: User
                              })
      options[:order_column_index] -= 1


      as_data_table_result(options)

    end

  end # end class << self

  # Setup accessible (or protected) attributes for your model

  def full_name
    [self.last_name,self.first_name].compact.join(', ')
  end

  def angler_info
    Angler.where(angler_id: self.angler_id).pluck(:first_name, :last_name, :email).first.join(' ')
  end

  def includes_notification_courier?(notification_courier_id, topic_id)
    self.user_couriers.map { |c| c.notification_courier.id == notification_courier_id and c.notification_topic_id == topic_id }.include?(true)
  end

  def add_notification_topic(topic)
    unless self.notification_topics.include?(topic)
      self.notification_topics << topic
    end
  end

  def remove_notification_topic(topic)
    self.notification_topics.destroy(topic)
  end

  def add_courier(courier_id, topic_id)
    unless self.includes_notification_courier?(courier_id, topic_id)
      self.user_couriers << UserCourier.create(:notification_courier_id => courier_id, :notification_topic_id => topic_id, :daily_digest => false, :user_id => self.id)
    end

    self.user_couriers
  end

  def remove_courier(courier_id, topic_id)
    self.user_couriers.where(:notification_courier_id => courier_id, :notification_topic_id => topic_id).destroy_all
  end

  # check to see if there are any outstanding welcome item requests in the angler_item table
  def requested_welcome_package?
    return false if self.angler.nil?
    self.angler.angler_items.where(angler_item_type: AnglerItemType.welcome_package).any?
  end

  private

  def create_welcome_item_requests
    # if he has a user, the user must be verified to get sent a kit
    # if the user is verified he must not already be in the program (this happens when an old angler joins via the web to track his stats)
      # he gets a new angler and then merges all his old info into that angler
    # finally if the angler already has an outstanding request for a tagging kit, don't add another one
    return if self.angler.nil?
    if (self.verified? && self.is_existing_angler == false) && !self.requested_welcome_package?
      AnglerItem.create(angler: self.angler, requested_at: Time.now, item: Item.tagging_kit, angler_item_type: AnglerItemType.welcome_package)
    end
  end

end

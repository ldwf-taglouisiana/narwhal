# == Schema Information
#
# Table name: marinas
#
#  id                      :integer          not null, primary key
#  site_no                 :integer
#  name                    :string(255)
#  closed                  :boolean          default(FALSE)
#  city                    :string(255)
#  parish                  :string(255)
#  publish                 :boolean          default(FALSE)
#  address                 :string(255)
#  latitude                :float
#  longitude               :float
#  web_address             :text
#  phone                   :string(255)
#  hours_of_operation      :string(255)
#  can_get_fishing_license :boolean
#  fee_amount              :string(255)
#  has_bait                :boolean          default(FALSE)
#  has_ice                 :boolean          default(FALSE)
#  has_food                :boolean          default(FALSE)
#  has_restrooms           :boolean          default(FALSE)
#  has_sewage_pump         :boolean          default(FALSE)
#  has_cert_scales         :boolean          default(FALSE)
#  facility_type           :string(255)
#  has_cleaning_station    :boolean          default(FALSE)
#  fuel_type               :string(255)
#  location_description    :text
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  deleted                 :boolean          default(FALSE)
#  photo_file_name         :string(255)
#  photo_content_type      :string(255)
#  photo_file_size         :integer
#  photo_updated_at        :datetime
#  comments                :text
#

class Marina < ActiveRecord::Base

  has_attached_file :photo, {:styles => {:medium => "300x300^", :thumb => "80x80^"},
                             :default_url => "default_marina_:style.png",
                             :path => ":rails_root/storage/images/:basename.:extension",
                             :url => "/:class/image/:id/:hash.:extension?style=:style",
                             :hash_secret => "emutlVkvC7POnMG6jfxdwHWgDvfOQcNWUj66QKW908B0kiRfBmk1/rzw8vcUS6+9NyjGXEbB+K+vDUuJQFnrb+nMDwo5DmmUyYuCtUlned1bgzCDJN1ht4jtIoR++FbYZrktYT9O+JwZYy78zxIIi85s5izq1duBCrbKqXr5+aU"}


  geocoded_by :address
  after_validation :geocode

  def photo_url(size = :medium)

    url = ''

    if self.photo.url(size).match('default')
      url = "/assets/#{self.photo.url(size)}"
    else
      url = "/#{self.photo.url(size)}"
    end

    return url
  end
end

# == Schema Information
#
# Table name: recapture_dispositions
#
#  id                    :integer          not null, primary key
#  recapture_disposition :string(255)
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#

class RecaptureDisposition < ActiveRecord::Base
  has_many :captures

  def select_display
    "#{self.id} - #{self.recapture_disposition}"
  end
end

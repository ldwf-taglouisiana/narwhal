# == Schema Information
# Schema version: 20151207160422
#
# Table name: roles_users
#
#  role_id :integer
#  user_id :integer
#  id      :integer          not null, primary key
#
# Indexes
#
#  ru_idx  (role_id,user_id) UNIQUE
#
# Foreign Keys
#
#  ru_roles_fk  (role_id => roles.id)
#  ru_user_fk   (user_id => users.id)
#

class RolesUser < ActiveRecord::Base
  belongs_to :user
  belongs_to :role

  validates_presence_of :user, :role
  validates_uniqueness_of :role_id, scope: [ :user_id ]
end

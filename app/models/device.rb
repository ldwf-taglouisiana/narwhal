# == Schema Information
# Schema version: 20150817164549
#
# Table name: devices
#
#  id                      :integer          not null, primary key
#  identifier              :string(255)      not null
#  user_id                 :integer          not null
#  update_captures         :boolean          default(FALSE)
#  update_tags             :boolean          default(FALSE)
#  update_partial_captures :boolean          default(FALSE)
#  update_species          :boolean          default(FALSE)
#  update_marinas          :boolean          default(FALSE)
#  last_synced             :datetime
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#
# Indexes
#
#  index_devices_on_identifier  (identifier) UNIQUE
#
# Foreign Keys
#
#  fk_devices_users  (user_id => users.id)
#

class Device < ActiveRecord::Base
  belongs_to :user

  validates_presence_of :user_id
  validates_uniqueness_of :identifier


  before_create :generate_identifier

  def generate_identifier
    # get a random identifier
    num = SecureRandom.hex(127)

    # if the identifier is already taken, then regenerate
    while Device.where(:identifier => num).count > 0 do
      num = SecureRandom.hex(127)
    end

    # save the new number this model
    self.identifier = num
  end
end

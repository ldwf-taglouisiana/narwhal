# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([√{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

['admin', 'ldwf', 'angler', 'ldwf_api'].each do |role|
  Role.where({:name => role}, :without_protection => true).first_or_create
end

ActivityLogType::TYPES.each do |key, value|
  ActivityLogType.where(:activity_type => value).first_or_create
end


UserDeviceType::TYPES.each do |key, value|
  UserDeviceType.where(:description => value).first_or_create
end


if Tag.where(:id => -1).count == 0
  Tag.create(:id => -1)
end

if AboutTaggingProgram.count == 0
  AboutTaggingProgram.create(:description => '')
end

if HowToTag.count == 0
  HowToTag.create(:description => '')
end

if Blurb.count == 0
  Blurb.create(:about_program => '', :capture_dashboard => '', :common_fish => '', :contact_info => '', :find_a_spot => '', :how_to => '', :licensing => '', :my_tags => '', :new_capture => '', :order_tags => '', :sign_up => '', :target_species => '', :volunteer_hours => '')
end

NotificationTopic::TOPICS.each do |key, value|
  NotificationTopic.where(:topic => value).first_or_create
end

NotificationCourier::COURIERS.each do |key, value|
  NotificationCourier.where(:courier => value).first_or_create
end

# add the default species lengths to the database
# speckled_trout = Species.where(id: 2).first # speckeled trout
# SpeciesLength.where(description: '< 12"'     , species: speckled_trout, position: 1).first_or_create
# SpeciesLength.where(description: '12" - 15"' , species: speckled_trout, position: 2).first_or_create
# SpeciesLength.where(description: '16" - 20"' , species: speckled_trout, position: 3).first_or_create
# SpeciesLength.where(description: '> 20"'     , species: speckled_trout, position: 4).first_or_create
#
# redfish = Species.where(id: 1).first # redfish
# SpeciesLength.where(description: '< 16"'     , species: redfish, position: 1).first_or_create
# SpeciesLength.where(description: '16" - 23"' , species: redfish, position: 2).first_or_create
# SpeciesLength.where(description: '24" - 27"' , species: redfish, position: 3).first_or_create
# SpeciesLength.where(description: '> 27"'     , species: redfish, position: 4).first_or_create
#
# yellowfin = Species.where(id: 55,).first
# SpeciesLength.where(description: '< 27"'     , species: yellowfin, position: 1).first_or_create
# SpeciesLength.where(description: '27" - 40"' , species: yellowfin, position: 2).first_or_create
# SpeciesLength.where(description: '40" - 51"' , species: yellowfin, position: 3).first_or_create
# SpeciesLength.where(description: '> 52"'     , species: yellowfin, position: 4).first_or_create

snapper = Species.where(id: 48,).first
SpeciesLength.where(description: '< 16"'     , species: snapper, position: 1).first_or_create
SpeciesLength.where(description: '16" - 23.99"' , species: snapper, position: 2).first_or_create
SpeciesLength.where(description: '24" - 27"' , species: snapper, position: 3).first_or_create
SpeciesLength.where(description: '> 27"'     , species: snapper, position: 4).first_or_create
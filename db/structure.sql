--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: btree_gist; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS btree_gist WITH SCHEMA public;


--
-- Name: EXTENSION btree_gist; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION btree_gist IS 'support for indexing common datatypes in GiST';


--
-- Name: pgcrypto; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA public;


--
-- Name: EXTENSION pgcrypto; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION pgcrypto IS 'cryptographic functions';


--
-- Name: postgis; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;


--
-- Name: EXTENSION postgis; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION postgis IS 'PostGIS geometry, geography, and raster spatial types and functions';


--
-- Name: uuid-ossp; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;


--
-- Name: EXTENSION "uuid-ossp"; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';


SET search_path = public, pg_catalog;

--
-- Name: anglers_before_insert_update_row_tr(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION anglers_before_insert_update_row_tr() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    new.search_vector :=
      setweight(to_tsvector('pg_catalog.english', coalesce(new.angler_id, '')), 'A')  ||
      setweight(to_tsvector('pg_catalog.english', coalesce(new.last_name, '')), 'B')  ||
      setweight(to_tsvector('pg_catalog.english', coalesce(new.first_name, '')), 'B') ||
      setweight(to_tsvector('pg_catalog.english', coalesce(new.email, '')), 'C');
    RETURN NEW;
END;
$$;


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: captures; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE captures (
    id integer NOT NULL,
    tag_id integer,
    capture_date timestamp with time zone NOT NULL,
    species_id integer,
    length numeric,
    location_description text,
    latitude double precision,
    longitude double precision,
    fish_condition_option_id integer,
    weight double precision,
    comments text,
    confirmed boolean DEFAULT false,
    verified boolean DEFAULT false,
    deleted boolean,
    mailed_map boolean,
    time_of_day_option_id integer,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    geom geometry,
    entered_gps_type character varying(255),
    user_capture boolean DEFAULT false,
    species_length_id integer,
    basin_id integer,
    sub_basin_id integer,
    gps_format_id bigint,
    recent boolean DEFAULT false NOT NULL,
    search_vector tsvector NOT NULL,
    fish_event_location_description_id integer,
    angler_id bigint NOT NULL
);


--
-- Name: capture_list(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION capture_list() RETURNS SETOF captures
    LANGUAGE sql
    AS $$
    SELECT captures.*
FROM 
captures 
, (
SELECT max(max_length) as max, min(min_length) as min
FROM (
  (SELECT max(length) as max_length, min(length) as min_length  FROM captures)
UNION ALL
  (SELECT max(length) as max_length, min(length) as min_length FROM recaptures)
) a
) l
WHERE (
(TRUE)
AND (          
length is NULL OR (length BETWEEN l.min AND l.max)
)
AND (
captures.species_id = ANY( VALUES (22),(39),(23),(82),(19),(85),(71),(45),(54),(40),(75),(34),(61),(62),(41),(70),(77),(24),(25),(69),(72),(20),(26),(67),(46),(53),(42),(65),(84),(68),(31),(32),(73),(47),(18),(56),(36),(80),(78),(37),(27),(48),(1),(30),(38),(83),(60),(74),(44),(50),(28),(33),(2),(76),(81),(79),(35),(52),(43),(57),(63),(49),(58),(29),(59),(64),(21),(55))
)
AND (
capture_date BETWEEN '1971-12-02 06:00:00.000000' AND '2014-12-18 00:00:00.000000'
)
);
$$;


--
-- Name: captures_before_insert_update_row_tr(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION captures_before_insert_update_row_tr() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    angler record;
    tag record;
BEGIN
          -- get the associated angler for this tag
          select * into angler from anglers where id = new.angler_id;
          select * into tag from tags where id = new.tag_id;
    
          new.search_vector :=
            setweight(to_tsvector('pg_catalog.english', coalesce(tag.tag_no, '')), 'A')     ||
            setweight(to_tsvector('pg_catalog.english', coalesce(angler.angler_id, '')), 'B')  ||
            setweight(to_tsvector('pg_catalog.english', coalesce(angler.last_name, '')), 'C')  ||
            setweight(to_tsvector('pg_catalog.english', coalesce(angler.first_name, '')), 'C') ||
            setweight(to_tsvector('pg_catalog.english', coalesce(angler.email, '')), 'D');
    RETURN NEW;
END;
$$;


--
-- Name: convert_id_to(integer, integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION convert_id_to(integer, integer) RETURNS void
    LANGUAGE sql
    AS $_$
	update captures 
		set angler_id = $2
		where angler_id = $1;
	update captures
                set captain_id = $2
                where captain_id = $1;
	update tags 
                set angler_id = $2
                where angler_id = $1;
	update  tag_requests
                set angler_id = $2
                where angler_id = $1;
$_$;


--
-- Name: date_display_tz(timestamp with time zone, text, text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION date_display_tz(param_dt timestamp with time zone, param_tz text, param_format text) RETURNS text
    LANGUAGE plpgsql
    AS $$
        DECLARE
          var_result varchar;
        BEGIN
          PERFORM set_config('timezone', param_tz, true);
          var_result := to_char(param_dt, param_format);
          RETURN var_result;
        END;
      $$;


--
-- Name: db_to_csv(text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION db_to_csv(path text) RETURNS void
    LANGUAGE plpgsql
    AS $$
declare
  tables RECORD;
  statement TEXT;
begin
  FOR tables IN 
    SELECT 
    (table_schema || '."' || table_name || '"') AS schema_table,
    table_name
    FROM information_schema.tables t INNER JOIN information_schema.schemata s 
    ON s.schema_name = t.table_schema 
    WHERE t.table_schema NOT IN ('pg_catalog', 'information_schema', 'configuration')
    AND t.table_type = 'BASE TABLE'
    AND ( NOT table_name ~* 'oauth') 
    ORDER BY schema_table
  LOOP
    statement := format('\COPY ' || tables.schema_table || ' TO ''' || path || '/' || tables.table_name || '.csv' ||''' DELIMITER '';'' CSV HEADER');
    EXECUTE statement;
  END LOOP;
  return;  
end;
$$;


--
-- Name: draft_captures_before_insert_update_row_tr(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION draft_captures_before_insert_update_row_tr() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    angler record;
BEGIN
          -- get the associated angler for this tag
          select * into angler from anglers where id = new.angler_id;
    
          new.search_vector :=
            setweight(to_tsvector('pg_catalog.english', coalesce(new.tag_number, '')), 'A') ||
            setweight(to_tsvector('pg_catalog.english', coalesce(angler.angler_id, '')), 'B')  ||
            setweight(to_tsvector('pg_catalog.english', coalesce(angler.last_name, '')), 'C')  ||
            setweight(to_tsvector('pg_catalog.english', coalesce(angler.first_name, '')), 'C') ||
            setweight(to_tsvector('pg_catalog.english', coalesce(angler.email, '')), 'D');
    RETURN NEW;
END;
$$;


--
-- Name: format_latitude(geometry, text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION format_latitude(geom geometry, format text) RETURNS text
    LANGUAGE sql
    AS $_$
          SELECT (split_part(regexp_replace(ST_AsLatLonText(geom, format), '(.+?)\s(-.*$)', '\1:\2'), ':', 1))
        $_$;


--
-- Name: format_longitude(geometry, text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION format_longitude(geom geometry, format text) RETURNS text
    LANGUAGE sql
    AS $_$
          SELECT (split_part(regexp_replace(ST_AsLatLonText(geom, format), '(.+?)\s(-.*$)', '\1:\2'), ':', 2))
        $_$;


--
-- Name: location_description_cleaner(text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION location_description_cleaner(item text) RETURNS text
    LANGUAGE sql
    AS $_$
	SELECT regexp_replace(item, '^[0-9]{4} |
.*|.*|^d.+?$', '', 'g')
$_$;


--
-- Name: rebuildmaterializedviews(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION rebuildmaterializedviews() RETURNS integer
    LANGUAGE sql
    AS $$
          select RefreshAllMaterializedViews();
          select 1;
      $$;


--
-- Name: recaptures_before_insert_update_row_tr(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION recaptures_before_insert_update_row_tr() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    angler record;
    tag record;
BEGIN
          -- get the associated angler for this tag
          select * into angler from anglers where id = new.angler_id;
          select * into tag from tags where id = new.tag_id;
    
          new.search_vector :=
            setweight(to_tsvector('pg_catalog.english', coalesce(tag.tag_no, '')), 'A')     ||
            setweight(to_tsvector('pg_catalog.english', coalesce(angler.angler_id, '')), 'B')  ||
            setweight(to_tsvector('pg_catalog.english', coalesce(angler.last_name, '')), 'C')  ||
            setweight(to_tsvector('pg_catalog.english', coalesce(angler.first_name, '')), 'C') ||
            setweight(to_tsvector('pg_catalog.english', coalesce(angler.email, '')), 'D');
    RETURN NEW;
END;
$$;


--
-- Name: refreshallmaterializedviews(text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION refreshallmaterializedviews(schema_arg text DEFAULT 'public'::text) RETURNS integer
    LANGUAGE plpgsql
    AS $$
        DECLARE
          r RECORD;
        BEGIN
          RAISE NOTICE 'Refreshing materialized view in schema %', schema_arg;
          FOR r IN SELECT matviewname FROM pg_matviews WHERE schemaname = schema_arg order by matviewname DESC
          LOOP
            RAISE NOTICE 'Refreshing %.%', schema_arg, r.matviewname;
            EXECUTE 'REFRESH MATERIALIZED VIEW CONCURRENTLY' || schema_arg || '.' || r.matviewname;
          END LOOP;

          RETURN 1;
        END
      $$;


--
-- Name: tags_before_insert_update_row_tr(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION tags_before_insert_update_row_tr() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    angler record;
BEGIN
          -- get the associated angler for this tag
          select * into angler from anglers where id = new.angler_id;
    
          new.search_vector :=
              setweight(to_tsvector('pg_catalog.english', coalesce(new.tag_no,'')), 'A')    ||
              setweight(to_tsvector('pg_catalog.english', coalesce(angler.angler_id,'')), 'B')  ||
              setweight(to_tsvector('pg_catalog.english', coalesce(angler.last_name,'')), 'C') ||
              setweight(to_tsvector('pg_catalog.english', coalesce(angler.first_name,'')), 'C') ||
              setweight(to_tsvector('pg_catalog.english', coalesce(angler.email,'')), 'D');
    RETURN NEW;
END;
$$;


--
-- Name: users_before_insert_update_row_tr(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION users_before_insert_update_row_tr() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    angler record;
BEGIN
    SELECT * INTO angler FROM anglers WHERE id = new.angler_id;
    new.search_vector :=
      setweight(to_tsvector('pg_catalog.english', coalesce(new.email, '')), 'A')      ||
      setweight(to_tsvector('pg_catalog.english', coalesce(angler.angler_id, '')), 'A')  ||
      setweight(to_tsvector('pg_catalog.english', coalesce(new.last_name, '')), 'B')  ||
      setweight(to_tsvector('pg_catalog.english', coalesce(new.first_name, '')), 'B') ||
      setweight(to_tsvector('pg_catalog.english', coalesce(angler.email, '')), 'C')      ||
      setweight(to_tsvector('pg_catalog.english', coalesce(angler.last_name, '')), 'D')  ||
      setweight(to_tsvector('pg_catalog.english', coalesce(angler.first_name, '')), 'D');
    RETURN NEW;
END;
$$;


--
-- Name: volunteer_time_logs_before_insert_update_row_tr(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION volunteer_time_logs_before_insert_update_row_tr() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    angler record;
BEGIN
    SELECT * INTO angler FROM anglers WHERE id = new.angler_id;
    new.search_vector :=
      setweight(to_tsvector('pg_catalog.english', coalesce(angler.angler_id, '')), 'A')  ||
      setweight(to_tsvector('pg_catalog.english', coalesce(angler.email, '')), 'B')      ||
      setweight(to_tsvector('pg_catalog.english', coalesce(angler.first_name, '')), 'C') ||
      setweight(to_tsvector('pg_catalog.english', coalesce(angler.last_name, '')), 'B');
    RETURN NEW;
END;
$$;


--
-- Name: year_to_int(timestamp with time zone); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION year_to_int(timestamp with time zone) RETURNS integer
    LANGUAGE sql IMMUTABLE
    AS $_$
          SELECT to_char($1, 'YYYY')::int
        $_$;


--
-- Name: about_tagging_programs; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE about_tagging_programs (
    id integer NOT NULL,
    description text NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);


--
-- Name: about_tagging_programs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE about_tagging_programs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: about_tagging_programs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE about_tagging_programs_id_seq OWNED BY about_tagging_programs.id;


--
-- Name: activity_log_types; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE activity_log_types (
    id integer NOT NULL,
    activity_type character varying(255) NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);


--
-- Name: activity_log_types_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE activity_log_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: activity_log_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE activity_log_types_id_seq OWNED BY activity_log_types.id;


--
-- Name: activity_logs; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE activity_logs (
    id integer NOT NULL,
    activity_log_type_id integer NOT NULL,
    old_item_id character varying(255),
    user_id integer,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    activity_item_id integer NOT NULL,
    activity_item_type character varying(255) NOT NULL
);


--
-- Name: activity_logs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE activity_logs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: activity_logs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE activity_logs_id_seq OWNED BY activity_logs.id;


--
-- Name: angler_item_types; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE angler_item_types (
    id bigint NOT NULL,
    name text NOT NULL,
    description text NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);


--
-- Name: angler_item_types_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE angler_item_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: angler_item_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE angler_item_types_id_seq OWNED BY angler_item_types.id;


--
-- Name: angler_items; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE angler_items (
    id bigint NOT NULL,
    angler_id bigint NOT NULL,
    item_id bigint NOT NULL,
    angler_item_type_id bigint NOT NULL,
    requested_at timestamp with time zone,
    fulfilled_at timestamp with time zone,
    comment text,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);


--
-- Name: angler_items_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE angler_items_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: angler_items_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE angler_items_id_seq OWNED BY angler_items.id;


--
-- Name: anglers; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE anglers (
    id integer NOT NULL,
    lag_no integer,
    law_no integer,
    first_name character varying(255),
    last_name character varying(255),
    street character varying(255),
    suite character varying(255),
    city character varying(255),
    state character varying(255),
    zip character varying(255),
    phone_number_1 character varying(255),
    phone_number_1_type character varying(255),
    phone_number_2 character varying(255),
    phone_number_2_type character varying(255),
    phone_number_3 character varying(255),
    phone_number_3_type character varying(255),
    phone_number_4 character varying(255),
    phone_number_4_type character varying(255),
    email character varying(255),
    tag_end_user_type integer,
    shirt_size integer,
    deleted boolean,
    lax_no integer,
    email_2 character varying(255),
    user_name character varying(255),
    comments text,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    angler_id character varying(20) NOT NULL,
    search_vector tsvector NOT NULL,
    prefered_incentive_item_id bigint
);


--
-- Name: anglers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE anglers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: anglers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE anglers_id_seq OWNED BY anglers.id;


--
-- Name: announcements; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE announcements (
    id integer NOT NULL,
    content text,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    expiration_date timestamp with time zone
);


--
-- Name: announcements_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE announcements_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: announcements_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE announcements_id_seq OWNED BY announcements.id;


--
-- Name: app_angler_sequence; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE app_angler_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: fish_condition_options; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE fish_condition_options (
    id integer NOT NULL,
    fish_condition_option character varying(255),
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);


--
-- Name: fish_event_location_descriptions; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE fish_event_location_descriptions (
    id integer NOT NULL,
    description text NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);


--
-- Name: gps_formats; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE gps_formats (
    id integer NOT NULL,
    format_type character varying(255) NOT NULL,
    format_string text NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone
);


--
-- Name: recaptures; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE recaptures (
    id integer NOT NULL,
    capture_date timestamp with time zone NOT NULL,
    length numeric,
    location_description text,
    latitude double precision,
    longitude double precision,
    comments text,
    verified boolean DEFAULT false,
    user_capture boolean DEFAULT false,
    entered_gps_type character varying(255),
    tag_id integer,
    species_id integer,
    fish_condition_option_id integer,
    recapture_disposition_id integer,
    time_of_day_option_id integer,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    geom geometry(Point,4326),
    species_length_id integer,
    basin_id integer,
    sub_basin_id integer,
    gps_format_id bigint,
    capture_id bigint,
    recent boolean DEFAULT true NOT NULL,
    search_vector tsvector NOT NULL,
    fish_event_location_description_id integer,
    angler_id bigint NOT NULL,
    angler_creation_token character varying
);


--
-- Name: species; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE species (
    id integer NOT NULL,
    common_name character varying(255),
    species_code character varying(255),
    "position" integer,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    photo_file_name character varying(255),
    photo_content_type character varying(255),
    photo_file_size integer,
    photo_updated_at timestamp with time zone,
    proper_name character varying(255),
    other_name character varying(255),
    family character varying(255),
    habitat text,
    description text,
    size text,
    food_value text,
    name character varying(255),
    publish boolean DEFAULT false,
    target boolean DEFAULT false,
    ordinal_position integer
);


--
-- Name: species_lengths; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE species_lengths (
    id integer NOT NULL,
    description character varying(255) NOT NULL,
    species_id integer NOT NULL,
    "position" integer NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    min double precision,
    max double precision
);


--
-- Name: tags; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE tags (
    id integer NOT NULL,
    tag_no character varying(255),
    tag_lot_id integer,
    tag_request_id integer,
    deleted boolean,
    unassign_option integer,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    old_anlger_id integer,
    prefix character varying(255),
    active boolean DEFAULT true,
    assigned_at timestamp with time zone,
    recent boolean DEFAULT true NOT NULL,
    search_vector tsvector NOT NULL,
    angler_id bigint
);


--
-- Name: time_of_day_options; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE time_of_day_options (
    id integer NOT NULL,
    time_of_day_option character varying(255),
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    start_time timestamp with time zone,
    end_time timestamp with time zone
);


--
-- Name: archived_captures; Type: MATERIALIZED VIEW; Schema: public; Owner: -; Tablespace: 
--

CREATE MATERIALIZED VIEW archived_captures AS
 SELECT a.id,
    a.species_id,
    species.common_name AS species_name,
    a.tag_id,
    tags.tag_no AS tag_number,
    a.angler_id,
    anglers.angler_id AS angler_number,
    concat(anglers.first_name, ' ', anglers.last_name) AS capture_angler,
    a.capture_date,
    to_char(a.capture_date, 'MM/DD/YYYY'::text) AS date_string,
    location_description_cleaner(a.location_description) AS location,
    a.time_of_day_option_id,
    time_of_day_options.time_of_day_option AS time_of_day,
    a.length,
    species_lengths.description AS length_range,
    a.fish_condition_option_id,
    fish_condition_options.fish_condition_option AS fish_condition,
    a.latitude,
    a.longitude,
    format_latitude(a.geom, gps_formats.format_string) AS latitude_formatted,
    format_longitude(a.geom, gps_formats.format_string) AS longitude_formatted,
    a.entered_gps_type,
    a.geom,
    a.basin_id,
    a.sub_basin_id,
    a.comments,
    a.user_capture,
    a.verified,
    a.location_description,
    fish_event_location_descriptions.id AS location_id,
    a.created_at,
    a.updated_at,
    ( SELECT count(*) AS count
           FROM recaptures
          WHERE (recaptures.capture_id = a.id)) AS recapture_count,
    year_to_int(a.capture_date) AS year_id
   FROM ((((((((captures a
     LEFT JOIN species ON ((a.species_id = species.id)))
     LEFT JOIN time_of_day_options ON ((a.time_of_day_option_id = time_of_day_options.id)))
     LEFT JOIN tags ON ((a.tag_id = tags.id)))
     LEFT JOIN fish_condition_options ON ((a.fish_condition_option_id = fish_condition_options.id)))
     LEFT JOIN anglers ON ((a.angler_id = anglers.id)))
     LEFT JOIN species_lengths ON ((a.species_length_id = species_lengths.id)))
     LEFT JOIN fish_event_location_descriptions ON ((a.fish_event_location_description_id = fish_event_location_descriptions.id)))
     LEFT JOIN gps_formats ON ((a.gps_format_id = gps_formats.id)))
  WHERE ((a.recent = false) AND (a.verified = true))
  WITH NO DATA;


--
-- Name: recapture_dispositions; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE recapture_dispositions (
    id integer NOT NULL,
    recapture_disposition character varying(255),
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);


--
-- Name: archived_recaptures; Type: MATERIALIZED VIEW; Schema: public; Owner: -; Tablespace: 
--

CREATE MATERIALIZED VIEW archived_recaptures AS
 SELECT a.user_capture,
    a.id,
    a.species_id,
    species.common_name AS species_name,
    a.tag_id,
    tags.tag_no AS tag_number,
    a.angler_id AS recapture_angler_id,
    anglers.angler_id AS recapture_angler_number,
    concat(anglers.first_name, ' ', anglers.last_name) AS recapture_angler_name,
    a.capture_date,
    to_char(((a.capture_date)::date)::timestamp with time zone, 'MM/DD/YYYY'::text) AS date_string,
    location_description_cleaner(a.location_description) AS location,
    time_of_day_options.time_of_day_option AS time_of_day,
    a.time_of_day_option_id,
    a.length,
    species_lengths.description AS length_range,
    recapture_dispositions.recapture_disposition,
    a.fish_condition_option_id,
    fish_condition_options.fish_condition_option AS fish_condition,
    a.latitude,
    a.verified,
    a.longitude,
    format_latitude(a.geom, gps_formats.format_string) AS latitude_formatted,
    format_longitude(a.geom, gps_formats.format_string) AS longitude_formatted,
    a.geom,
    a.basin_id,
    a.sub_basin_id,
    a.comments,
    a.location_description,
    fish_event_location_descriptions.id AS location_id,
    a.created_at,
    a.updated_at,
    a.capture_id,
    year_to_int(a.capture_date) AS year_id,
    row_number() OVER (PARTITION BY a.capture_id ORDER BY a.capture_date) AS recapture_number
   FROM (((((((((recaptures a
     LEFT JOIN species ON ((a.species_id = species.id)))
     LEFT JOIN time_of_day_options ON ((a.time_of_day_option_id = time_of_day_options.id)))
     LEFT JOIN tags ON ((a.tag_id = tags.id)))
     LEFT JOIN recapture_dispositions ON ((a.recapture_disposition_id = recapture_dispositions.id)))
     LEFT JOIN fish_condition_options ON ((a.fish_condition_option_id = fish_condition_options.id)))
     LEFT JOIN anglers ON ((a.angler_id = anglers.id)))
     LEFT JOIN species_lengths ON ((a.species_length_id = species_lengths.id)))
     LEFT JOIN fish_event_location_descriptions ON ((a.fish_event_location_description_id = fish_event_location_descriptions.id)))
     LEFT JOIN gps_formats ON ((a.gps_format_id = gps_formats.id)))
  WHERE ((a.recent = false) AND (a.verified = true))
  WITH NO DATA;


--
-- Name: volunteer_time_logs; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE volunteer_time_logs (
    id integer NOT NULL,
    start timestamp with time zone NOT NULL,
    "end" timestamp with time zone NOT NULL,
    verified boolean DEFAULT false,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    ldwf_entered boolean DEFAULT false,
    ldwf_edited boolean DEFAULT false,
    uuid character varying,
    recent boolean DEFAULT true NOT NULL,
    search_vector tsvector NOT NULL,
    angler_id bigint NOT NULL,
    CONSTRAINT chk_vol_time_log_start_end CHECK ((start < "end"))
);


--
-- Name: archived_volunteer_hours; Type: MATERIALIZED VIEW; Schema: public; Owner: -; Tablespace: 
--

CREATE MATERIALIZED VIEW archived_volunteer_hours AS
 WITH items AS (
         SELECT volunteer_time_logs_1.id,
            count(captures.id) AS number_of_captures
           FROM (volunteer_time_logs volunteer_time_logs_1
             JOIN captures ON ((captures.capture_date <@ tstzrange(volunteer_time_logs_1.start, volunteer_time_logs_1."end", '[]'::text))))
          WHERE (((volunteer_time_logs_1.recent = false) AND (captures.angler_id = volunteer_time_logs_1.angler_id)) AND (captures.verified IS TRUE))
          GROUP BY volunteer_time_logs_1.id
          ORDER BY count(captures.id) DESC
        )
 SELECT volunteer_time_logs.id,
    volunteer_time_logs.angler_id,
    anglers.angler_id AS angler_number,
    concat(anglers.last_name, ', ', anglers.first_name) AS angler_name,
    volunteer_time_logs.verified,
    volunteer_time_logs.ldwf_entered,
    volunteer_time_logs.ldwf_edited,
    round(((date_part('epoch'::text, (volunteer_time_logs."end" - volunteer_time_logs.start)) / (3600.0)::double precision))::numeric, 2) AS total_time,
    volunteer_time_logs.start,
    volunteer_time_logs."end",
    volunteer_time_logs.recent,
    volunteer_time_logs.created_at,
    volunteer_time_logs.updated_at,
    items.number_of_captures
   FROM ((items
     LEFT JOIN volunteer_time_logs ON ((items.id = volunteer_time_logs.id)))
     LEFT JOIN anglers ON ((volunteer_time_logs.angler_id = anglers.id)))
  WITH NO DATA;


--
-- Name: basins; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE basins (
    gid integer NOT NULL,
    objectid numeric(10,0),
    area numeric,
    perimeter numeric,
    subseg02 numeric(10,0),
    subseg02id numeric(10,0),
    subsegment character varying(15),
    name character varying(71),
    descriptio character varying(140),
    basin character varying(18),
    shape_area numeric,
    shape_len numeric,
    geom geometry(MultiPolygon),
    created_at timestamp with time zone DEFAULT '2015-12-01 03:29:39.137427+00'::timestamp with time zone NOT NULL,
    updated_at timestamp with time zone DEFAULT '2015-12-01 03:29:39.137427+00'::timestamp with time zone NOT NULL
);


--
-- Name: basins_gid_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE basins_gid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: basins_gid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE basins_gid_seq OWNED BY basins.gid;


--
-- Name: blurbs; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE blurbs (
    id integer NOT NULL,
    capture_dashboard text,
    new_capture text,
    my_tags text,
    order_tags text,
    find_a_spot text,
    common_fish text,
    target_species text,
    about_program text,
    sign_up text,
    how_to text,
    licensing text,
    volunteer_hours text,
    contact_info text,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    non_registered_capture text,
    login text,
    my_captures text,
    draft_captures text,
    account_info text,
    contact_us text,
    privacy_policy text,
    faq text
);


--
-- Name: blurbs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE blurbs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: blurbs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE blurbs_id_seq OWNED BY blurbs.id;


--
-- Name: capture_duplicates; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE capture_duplicates (
    id integer NOT NULL,
    angler_id character varying(20),
    tag_id integer,
    capture_date timestamp with time zone,
    species_id integer,
    length double precision,
    location_description text,
    latitude double precision,
    longitude double precision,
    fish_condition_option_id integer,
    weight double precision,
    comments text,
    confirmed boolean DEFAULT false,
    verified boolean DEFAULT false,
    deleted boolean,
    mailed_map boolean,
    recapture boolean DEFAULT false,
    recapture_disposition_id integer,
    time_of_day_option_id integer,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    geom geometry,
    entered_gps_type character varying(255),
    user_capture boolean
);


--
-- Name: captures_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE captures_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: captures_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE captures_id_seq OWNED BY captures.id;


--
-- Name: captures_old; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE captures_old (
    id integer NOT NULL,
    angler_id character varying(20),
    tag_id integer,
    capture_date timestamp with time zone,
    species_id integer,
    length double precision,
    location_description text,
    lat double precision,
    long double precision,
    fish_condition_id integer,
    weight double precision,
    comments text,
    confirmed boolean DEFAULT false,
    verified boolean DEFAULT false,
    deleted boolean,
    mailed_map boolean,
    recapture boolean DEFAULT false,
    recapture_disposition_id integer,
    time_of_day integer,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    geom geometry,
    entered_gps_type character varying(255),
    user_capture boolean
);


--
-- Name: captures_old_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE captures_old_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: captures_old_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE captures_old_id_seq OWNED BY captures_old.id;


--
-- Name: draft_captures; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE draft_captures (
    id integer NOT NULL,
    tag_number character varying(255) NOT NULL,
    capture_date timestamp with time zone NOT NULL,
    species_id integer,
    length double precision,
    location_description text,
    latitude double precision,
    longitude double precision,
    fish_condition_id integer,
    weight double precision,
    comments text,
    recapture boolean DEFAULT false,
    recapture_disposition_id integer,
    time_of_day_id integer,
    entered_gps_type character varying(255),
    error_json character varying(255),
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    should_save boolean DEFAULT false,
    saved_at timestamp with time zone,
    species_length_id integer,
    geom geometry(Point,4326),
    uuid character varying(255) DEFAULT uuid_generate_v4(),
    search_vector tsvector NOT NULL,
    angler_id bigint NOT NULL
);


--
-- Name: denormalized_draft_captures; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW denormalized_draft_captures AS
 SELECT draft_captures.id,
    draft_captures.tag_number,
    draft_captures.capture_date,
    draft_captures.species_id,
    draft_captures.length,
    draft_captures.location_description,
    draft_captures.latitude,
    draft_captures.longitude,
    draft_captures.fish_condition_id,
    draft_captures.weight,
    draft_captures.comments,
    draft_captures.recapture,
    draft_captures.recapture_disposition_id,
    draft_captures.time_of_day_id,
    draft_captures.entered_gps_type,
    draft_captures.error_json,
    draft_captures.created_at,
    draft_captures.updated_at,
    draft_captures.should_save,
    draft_captures.saved_at,
    draft_captures.species_length_id,
    draft_captures.geom,
    draft_captures.uuid,
    draft_captures.search_vector,
    draft_captures.angler_id,
    concat(anglers.first_name, ' ', anglers.last_name) AS angler_full_name,
    species.common_name AS species_name,
    anglers.angler_id AS angler_number,
    format_latitude(draft_captures.geom, gps_formats.format_string) AS latitude_formatted,
    format_longitude(draft_captures.geom, gps_formats.format_string) AS longitude_formatted,
        CASE
            WHEN (draft_captures.length IS NOT NULL) THEN ((draft_captures.length)::text)::character varying
            ELSE species_lengths.description
        END AS species_length
   FROM ((((draft_captures
     LEFT JOIN species ON ((draft_captures.species_id = species.id)))
     LEFT JOIN anglers ON ((draft_captures.angler_id = anglers.id)))
     LEFT JOIN species_lengths ON ((draft_captures.species_length_id = species_lengths.id)))
     LEFT JOIN gps_formats ON (((draft_captures.entered_gps_type)::text = (gps_formats.format_type)::text)));


--
-- Name: devices; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE devices (
    id integer NOT NULL,
    identifier character varying(255) NOT NULL,
    user_id integer NOT NULL,
    update_captures boolean DEFAULT false,
    update_tags boolean DEFAULT false,
    update_partial_captures boolean DEFAULT false,
    update_species boolean DEFAULT false,
    update_marinas boolean DEFAULT false,
    last_synced timestamp with time zone,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);


--
-- Name: devices_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE devices_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: devices_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE devices_id_seq OWNED BY devices.id;


--
-- Name: draft_captures_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE draft_captures_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: draft_captures_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE draft_captures_id_seq OWNED BY draft_captures.id;


--
-- Name: email_jobs; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE email_jobs (
    id integer NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone
);


--
-- Name: email_jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE email_jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: email_jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE email_jobs_id_seq OWNED BY email_jobs.id;


--
-- Name: fish_condition_options_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE fish_condition_options_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: fish_condition_options_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE fish_condition_options_id_seq OWNED BY fish_condition_options.id;


--
-- Name: fish_entry_photos; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE fish_entry_photos (
    id integer NOT NULL,
    fish_entry_photoable_type character varying NOT NULL,
    fish_entry_photoable_id bigint NOT NULL,
    image_file_name character varying,
    image_content_type character varying,
    image_file_size integer,
    image_updated_at timestamp with time zone,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);


--
-- Name: fish_entry_photos_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE fish_entry_photos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: fish_entry_photos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE fish_entry_photos_id_seq OWNED BY fish_entry_photos.id;


--
-- Name: fish_event_location_descriptions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE fish_event_location_descriptions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: fish_event_location_descriptions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE fish_event_location_descriptions_id_seq OWNED BY fish_event_location_descriptions.id;


--
-- Name: front_page_photo_stream_items; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE front_page_photo_stream_items (
    id integer NOT NULL,
    featured boolean DEFAULT false,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);


--
-- Name: front_page_photo_stream_items_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE front_page_photo_stream_items_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: front_page_photo_stream_items_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE front_page_photo_stream_items_id_seq OWNED BY front_page_photo_stream_items.id;


--
-- Name: gps_formats_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE gps_formats_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: gps_formats_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE gps_formats_id_seq OWNED BY gps_formats.id;


--
-- Name: how_to_tags; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE how_to_tags (
    id integer NOT NULL,
    description text,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);


--
-- Name: how_to_tags_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE how_to_tags_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: how_to_tags_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE how_to_tags_id_seq OWNED BY how_to_tags.id;


--
-- Name: items; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE items (
    id bigint NOT NULL,
    name text NOT NULL,
    is_public boolean NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);


--
-- Name: items_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE items_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: items_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE items_id_seq OWNED BY items.id;


--
-- Name: manufacturers; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE manufacturers (
    id integer NOT NULL,
    name character varying(255),
    street character varying(255),
    city character varying(255),
    state character varying(255),
    country character varying(255),
    zip character varying(255),
    contact character varying(255),
    phone character varying(255),
    fax character varying(255),
    email character varying(255),
    deleted boolean,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);


--
-- Name: manufacturers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE manufacturers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: manufacturers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE manufacturers_id_seq OWNED BY manufacturers.id;


--
-- Name: marinas; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE marinas (
    id integer NOT NULL,
    site_no integer,
    name character varying(255),
    closed boolean DEFAULT false,
    city character varying(255),
    parish character varying(255),
    publish boolean DEFAULT false,
    address character varying(255),
    latitude double precision,
    longitude double precision,
    web_address text,
    phone character varying(255),
    hours_of_operation character varying(255),
    can_get_fishing_license boolean,
    fee_amount character varying(255),
    has_bait boolean DEFAULT false,
    has_ice boolean DEFAULT false,
    has_food boolean DEFAULT false,
    has_restrooms boolean DEFAULT false,
    has_sewage_pump boolean DEFAULT false,
    has_cert_scales boolean DEFAULT false,
    facility_type character varying(255),
    has_cleaning_station boolean DEFAULT false,
    fuel_type character varying(255),
    location_description text,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    deleted boolean DEFAULT false,
    photo_file_name character varying(255),
    photo_content_type character varying(255),
    photo_file_size integer,
    photo_updated_at timestamp with time zone,
    comments text
);


--
-- Name: marinas_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE marinas_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: marinas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE marinas_id_seq OWNED BY marinas.id;


--
-- Name: news_items; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE news_items (
    id integer NOT NULL,
    user_id integer NOT NULL,
    content text NOT NULL,
    should_publish boolean DEFAULT false,
    should_publish_at timestamp with time zone,
    title character varying(255) NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);


--
-- Name: news_items_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE news_items_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: news_items_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE news_items_id_seq OWNED BY news_items.id;


--
-- Name: notification_couriers; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE notification_couriers (
    id integer NOT NULL,
    courier character varying(255) NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);


--
-- Name: notification_couriers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE notification_couriers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: notification_couriers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE notification_couriers_id_seq OWNED BY notification_couriers.id;


--
-- Name: notification_jobs; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE notification_jobs (
    id integer NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone
);


--
-- Name: notification_jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE notification_jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: notification_jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE notification_jobs_id_seq OWNED BY notification_jobs.id;


--
-- Name: notification_topics; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE notification_topics (
    id integer NOT NULL,
    topic character varying(255) NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);


--
-- Name: notification_topics_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE notification_topics_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: notification_topics_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE notification_topics_id_seq OWNED BY notification_topics.id;


--
-- Name: notification_topics_users; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE notification_topics_users (
    notification_topic_id integer,
    user_id integer
);


--
-- Name: notifications; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE notifications (
    id integer NOT NULL,
    initiator_id integer,
    checked boolean DEFAULT false,
    notification_item_id integer NOT NULL,
    notification_item_type character varying(255) NOT NULL,
    notification_topic_id integer NOT NULL,
    user_id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);


--
-- Name: notifications_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE notifications_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: notifications_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE notifications_id_seq OWNED BY notifications.id;


--
-- Name: oauth_access_grants; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE oauth_access_grants (
    id integer NOT NULL,
    resource_owner_id integer NOT NULL,
    application_id integer NOT NULL,
    token character varying(255) NOT NULL,
    expires_in integer NOT NULL,
    redirect_uri character varying(2048) NOT NULL,
    created_at timestamp with time zone NOT NULL,
    revoked_at timestamp with time zone,
    scopes character varying(255)
);


--
-- Name: oauth_access_grants_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE oauth_access_grants_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: oauth_access_grants_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE oauth_access_grants_id_seq OWNED BY oauth_access_grants.id;


--
-- Name: oauth_access_tokens; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE oauth_access_tokens (
    id integer NOT NULL,
    resource_owner_id integer,
    application_id integer NOT NULL,
    token character varying(255) NOT NULL,
    refresh_token character varying(255),
    expires_in integer,
    revoked_at timestamp with time zone,
    created_at timestamp with time zone NOT NULL,
    scopes character varying(255)
);


--
-- Name: oauth_access_tokens_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE oauth_access_tokens_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: oauth_access_tokens_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE oauth_access_tokens_id_seq OWNED BY oauth_access_tokens.id;


--
-- Name: oauth_applications; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE oauth_applications (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    uid character varying(255) NOT NULL,
    secret character varying(255) NOT NULL,
    redirect_uri character varying(2048) NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);


--
-- Name: oauth_applications_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE oauth_applications_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: oauth_applications_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE oauth_applications_id_seq OWNED BY oauth_applications.id;


--
-- Name: photos; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE photos (
    id integer NOT NULL,
    image_file_name character varying(255),
    image_content_type character varying(255),
    image_file_size integer,
    image_updated_at timestamp with time zone,
    imageable_id integer,
    imageable_type character varying(255),
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);


--
-- Name: photos_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE photos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: photos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE photos_id_seq OWNED BY photos.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE users (
    id integer NOT NULL,
    email character varying(255) DEFAULT ''::character varying NOT NULL,
    encrypted_password character varying(255) DEFAULT ''::character varying NOT NULL,
    reset_password_token character varying(255),
    reset_password_sent_at timestamp with time zone,
    remember_created_at timestamp with time zone,
    sign_in_count integer DEFAULT 0,
    current_sign_in_at timestamp with time zone,
    last_sign_in_at timestamp with time zone,
    current_sign_in_ip character varying(255),
    last_sign_in_ip character varying(255),
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    first_name character varying(255) NOT NULL,
    last_name character varying(255) NOT NULL,
    authentication_token character varying(255),
    verified boolean DEFAULT false,
    receive_daily_capture_email_summary boolean DEFAULT false,
    contacted boolean DEFAULT false,
    hidden boolean DEFAULT false,
    comments text,
    search_vector tsvector NOT NULL,
    angler_id bigint,
    is_existing_angler boolean
);


--
-- Name: presentable_angler_items; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW presentable_angler_items AS
 SELECT angler_items.id,
    angler_items.requested_at,
    angler_items.fulfilled_at,
    angler_items.created_at,
    angler_items.updated_at,
    concat(anglers.first_name, ' ', anglers.last_name) AS angler_name,
    anglers.angler_id AS angler_number,
    items.name AS item_name,
    angler_item_types.name AS item_type,
    angler_items.angler_id,
    users.id AS user_id
   FROM ((((angler_items
     JOIN anglers ON ((anglers.id = angler_items.angler_id)))
     JOIN items ON ((items.id = angler_items.item_id)))
     JOIN angler_item_types ON ((angler_item_types.id = angler_items.angler_item_type_id)))
     LEFT JOIN users ON ((users.angler_id = anglers.id)));


--
-- Name: recent_captures; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW recent_captures AS
 WITH r_captures AS (
         SELECT captures.id,
            captures.tag_id,
            captures.capture_date,
            captures.species_id,
            captures.length,
            captures.location_description,
            captures.latitude,
            captures.longitude,
            captures.fish_condition_option_id,
            captures.weight,
            captures.comments,
            captures.confirmed,
            captures.verified,
            captures.deleted,
            captures.mailed_map,
            captures.time_of_day_option_id,
            captures.created_at,
            captures.updated_at,
            captures.geom,
            captures.entered_gps_type,
            captures.user_capture,
            captures.species_length_id,
            captures.basin_id,
            captures.sub_basin_id,
            captures.gps_format_id,
            captures.recent,
            captures.search_vector,
            captures.fish_event_location_description_id,
            captures.angler_id
           FROM captures
          WHERE (captures.recent = true)
        )
 SELECT a.id,
    a.species_id,
    species.common_name AS species_name,
    a.tag_id,
    tags.tag_no AS tag_number,
    a.angler_id,
    anglers.angler_id AS angler_number,
    concat(anglers.first_name, ' ', anglers.last_name) AS capture_angler,
    a.capture_date,
    to_char(a.capture_date, 'MM/DD/YYYY'::text) AS date_string,
    location_description_cleaner(a.location_description) AS location,
    a.time_of_day_option_id,
    time_of_day_options.time_of_day_option AS time_of_day,
    a.length,
    species_lengths.description AS length_range,
    a.fish_condition_option_id,
    fish_condition_options.fish_condition_option AS fish_condition,
    a.latitude,
    a.longitude,
    format_latitude(a.geom, gps_formats.format_string) AS latitude_formatted,
    format_longitude(a.geom, gps_formats.format_string) AS longitude_formatted,
    a.entered_gps_type,
    a.geom,
    a.basin_id,
    a.sub_basin_id,
    a.comments,
    a.user_capture,
    a.verified,
    a.location_description,
    fish_event_location_descriptions.id AS location_id,
    a.created_at,
    a.updated_at,
    ( SELECT count(*) AS count
           FROM recaptures
          WHERE (recaptures.capture_id = a.id)) AS recapture_count,
    year_to_int(a.capture_date) AS year_id
   FROM ((((((((r_captures a
     LEFT JOIN species ON ((a.species_id = species.id)))
     LEFT JOIN time_of_day_options ON ((a.time_of_day_option_id = time_of_day_options.id)))
     LEFT JOIN tags ON ((a.tag_id = tags.id)))
     LEFT JOIN fish_condition_options ON ((a.fish_condition_option_id = fish_condition_options.id)))
     LEFT JOIN anglers ON ((a.angler_id = anglers.id)))
     LEFT JOIN species_lengths ON ((a.species_length_id = species_lengths.id)))
     LEFT JOIN fish_event_location_descriptions ON ((a.fish_event_location_description_id = fish_event_location_descriptions.id)))
     LEFT JOIN gps_formats ON ((a.gps_format_id = gps_formats.id)))
  WHERE ((a.recent = true) OR (a.verified = false));


--
-- Name: presentable_captures; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW presentable_captures AS
 SELECT recent_captures.id,
    recent_captures.species_id,
    recent_captures.species_name,
    recent_captures.tag_id,
    recent_captures.tag_number,
    recent_captures.angler_id,
    recent_captures.angler_number,
    recent_captures.capture_angler,
    recent_captures.capture_date,
    recent_captures.date_string,
    recent_captures.location,
    recent_captures.time_of_day_option_id,
    recent_captures.time_of_day,
    recent_captures.length,
    recent_captures.length_range,
    recent_captures.fish_condition_option_id,
    recent_captures.fish_condition,
    recent_captures.latitude,
    recent_captures.longitude,
    recent_captures.latitude_formatted,
    recent_captures.longitude_formatted,
    recent_captures.entered_gps_type,
    recent_captures.geom,
    recent_captures.basin_id,
    recent_captures.sub_basin_id,
    recent_captures.comments,
    recent_captures.user_capture,
    recent_captures.verified,
    recent_captures.location_description,
    recent_captures.location_id,
    recent_captures.created_at,
    recent_captures.updated_at,
    recent_captures.recapture_count,
    recent_captures.year_id
   FROM recent_captures
UNION ALL
 SELECT archived_captures.id,
    archived_captures.species_id,
    archived_captures.species_name,
    archived_captures.tag_id,
    archived_captures.tag_number,
    archived_captures.angler_id,
    archived_captures.angler_number,
    archived_captures.capture_angler,
    archived_captures.capture_date,
    archived_captures.date_string,
    archived_captures.location,
    archived_captures.time_of_day_option_id,
    archived_captures.time_of_day,
    archived_captures.length,
    archived_captures.length_range,
    archived_captures.fish_condition_option_id,
    archived_captures.fish_condition,
    archived_captures.latitude,
    archived_captures.longitude,
    archived_captures.latitude_formatted,
    archived_captures.longitude_formatted,
    archived_captures.entered_gps_type,
    archived_captures.geom,
    archived_captures.basin_id,
    archived_captures.sub_basin_id,
    archived_captures.comments,
    archived_captures.user_capture,
    archived_captures.verified,
    archived_captures.location_description,
    archived_captures.location_id,
    archived_captures.created_at,
    archived_captures.updated_at,
    archived_captures.recapture_count,
    archived_captures.year_id
   FROM archived_captures;


--
-- Name: recent_recaptures; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW recent_recaptures AS
 SELECT a.user_capture,
    a.id,
    a.species_id,
    species.common_name AS species_name,
    a.tag_id,
    tags.tag_no AS tag_number,
    a.angler_id AS recapture_angler_id,
    anglers.angler_id AS recapture_angler_number,
    concat(anglers.first_name, ' ', anglers.last_name) AS recapture_angler_name,
    a.capture_date,
    to_char(((a.capture_date)::date)::timestamp with time zone, 'MM/DD/YYYY'::text) AS date_string,
    location_description_cleaner(a.location_description) AS location,
    time_of_day_options.time_of_day_option AS time_of_day,
    a.time_of_day_option_id,
    a.length,
    species_lengths.description AS length_range,
    recapture_dispositions.recapture_disposition,
    a.fish_condition_option_id,
    fish_condition_options.fish_condition_option AS fish_condition,
    a.latitude,
    a.verified,
    a.longitude,
    format_latitude(a.geom, gps_formats.format_string) AS latitude_formatted,
    format_longitude(a.geom, gps_formats.format_string) AS longitude_formatted,
    a.geom,
    a.basin_id,
    a.sub_basin_id,
    a.comments,
    a.location_description,
    fish_event_location_descriptions.id AS location_id,
    a.created_at,
    a.updated_at,
    a.capture_id,
    year_to_int(a.capture_date) AS year_id,
    row_number() OVER (PARTITION BY a.capture_id ORDER BY a.capture_date) AS recapture_number
   FROM (((((((((recaptures a
     LEFT JOIN species ON ((a.species_id = species.id)))
     LEFT JOIN time_of_day_options ON ((a.time_of_day_option_id = time_of_day_options.id)))
     LEFT JOIN tags ON ((a.tag_id = tags.id)))
     LEFT JOIN recapture_dispositions ON ((a.recapture_disposition_id = recapture_dispositions.id)))
     LEFT JOIN fish_condition_options ON ((a.fish_condition_option_id = fish_condition_options.id)))
     LEFT JOIN anglers ON ((a.angler_id = anglers.id)))
     LEFT JOIN species_lengths ON ((a.species_length_id = species_lengths.id)))
     LEFT JOIN fish_event_location_descriptions ON ((a.fish_event_location_description_id = fish_event_location_descriptions.id)))
     LEFT JOIN gps_formats ON ((a.gps_format_id = gps_formats.id)))
  WHERE ((a.recent = true) OR (a.verified = false));


--
-- Name: presentable_recaptures; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW presentable_recaptures AS
 SELECT recent_recaptures.user_capture,
    recent_recaptures.id,
    recent_recaptures.species_id,
    recent_recaptures.species_name,
    recent_recaptures.tag_id,
    recent_recaptures.tag_number,
    recent_recaptures.recapture_angler_id,
    recent_recaptures.recapture_angler_number,
    recent_recaptures.recapture_angler_name,
    recent_recaptures.capture_date,
    recent_recaptures.date_string,
    recent_recaptures.location,
    recent_recaptures.time_of_day,
    recent_recaptures.time_of_day_option_id,
    recent_recaptures.length,
    recent_recaptures.length_range,
    recent_recaptures.recapture_disposition,
    recent_recaptures.fish_condition_option_id,
    recent_recaptures.fish_condition,
    recent_recaptures.latitude,
    recent_recaptures.verified,
    recent_recaptures.longitude,
    recent_recaptures.latitude_formatted,
    recent_recaptures.longitude_formatted,
    recent_recaptures.geom,
    recent_recaptures.basin_id,
    recent_recaptures.sub_basin_id,
    recent_recaptures.comments,
    recent_recaptures.location_description,
    recent_recaptures.location_id,
    recent_recaptures.created_at,
    recent_recaptures.updated_at,
    recent_recaptures.capture_id,
    recent_recaptures.year_id,
    recent_recaptures.recapture_number
   FROM recent_recaptures
UNION ALL
 SELECT archived_recaptures.user_capture,
    archived_recaptures.id,
    archived_recaptures.species_id,
    archived_recaptures.species_name,
    archived_recaptures.tag_id,
    archived_recaptures.tag_number,
    archived_recaptures.recapture_angler_id,
    archived_recaptures.recapture_angler_number,
    archived_recaptures.recapture_angler_name,
    archived_recaptures.capture_date,
    archived_recaptures.date_string,
    archived_recaptures.location,
    archived_recaptures.time_of_day,
    archived_recaptures.time_of_day_option_id,
    archived_recaptures.length,
    archived_recaptures.length_range,
    archived_recaptures.recapture_disposition,
    archived_recaptures.fish_condition_option_id,
    archived_recaptures.fish_condition,
    archived_recaptures.latitude,
    archived_recaptures.verified,
    archived_recaptures.longitude,
    archived_recaptures.latitude_formatted,
    archived_recaptures.longitude_formatted,
    archived_recaptures.geom,
    archived_recaptures.basin_id,
    archived_recaptures.sub_basin_id,
    archived_recaptures.comments,
    archived_recaptures.location_description,
    archived_recaptures.location_id,
    archived_recaptures.created_at,
    archived_recaptures.updated_at,
    archived_recaptures.capture_id,
    archived_recaptures.year_id,
    archived_recaptures.recapture_number
   FROM archived_recaptures;


--
-- Name: presentable_tags; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE presentable_tags (
    id integer,
    tag_no character varying(255),
    tag_lot_id integer,
    tag_request_id integer,
    deleted boolean,
    unassign_option integer,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    old_anlger_id integer,
    prefix character varying(255),
    active boolean,
    assigned_at timestamp with time zone,
    recent boolean,
    search_vector tsvector,
    angler_id bigint,
    tag_suffix bigint,
    tag_prefix text,
    used boolean,
    used_at timestamp with time zone,
    angler_name text,
    angler_number character varying(20),
    seq_id bigint
);

ALTER TABLE ONLY presentable_tags REPLICA IDENTITY NOTHING;


--
-- Name: recent_volunteer_hours; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW recent_volunteer_hours AS
 WITH items AS (
         SELECT volunteer_time_logs_1.id,
            count(captures.id) AS number_of_captures
           FROM (volunteer_time_logs volunteer_time_logs_1
             JOIN captures ON ((captures.capture_date <@ tstzrange(volunteer_time_logs_1.start, volunteer_time_logs_1."end", '[]'::text))))
          WHERE (((volunteer_time_logs_1.recent = true) AND (captures.angler_id = volunteer_time_logs_1.angler_id)) AND (captures.verified IS TRUE))
          GROUP BY volunteer_time_logs_1.id
          ORDER BY count(captures.id) DESC
        )
 SELECT volunteer_time_logs.id,
    volunteer_time_logs.angler_id,
    anglers.angler_id AS angler_number,
    concat(anglers.last_name, ', ', anglers.first_name) AS angler_name,
    volunteer_time_logs.verified,
    volunteer_time_logs.ldwf_entered,
    volunteer_time_logs.ldwf_edited,
    round(((date_part('epoch'::text, (volunteer_time_logs."end" - volunteer_time_logs.start)) / (3600.0)::double precision))::numeric, 2) AS total_time,
    volunteer_time_logs.start,
    volunteer_time_logs."end",
    volunteer_time_logs.recent,
    volunteer_time_logs.created_at,
    volunteer_time_logs.updated_at,
    items.number_of_captures
   FROM ((items
     LEFT JOIN volunteer_time_logs ON ((items.id = volunteer_time_logs.id)))
     LEFT JOIN anglers ON ((volunteer_time_logs.angler_id = anglers.id)));


--
-- Name: presentable_volunteer_hours; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW presentable_volunteer_hours AS
 SELECT recent_volunteer_hours.id,
    recent_volunteer_hours.angler_id,
    recent_volunteer_hours.angler_number,
    recent_volunteer_hours.angler_name,
    recent_volunteer_hours.verified,
    recent_volunteer_hours.ldwf_entered,
    recent_volunteer_hours.ldwf_edited,
    recent_volunteer_hours.total_time,
    recent_volunteer_hours.start,
    recent_volunteer_hours."end",
    recent_volunteer_hours.recent,
    recent_volunteer_hours.created_at,
    recent_volunteer_hours.updated_at,
    recent_volunteer_hours.number_of_captures
   FROM recent_volunteer_hours
UNION ALL
 SELECT archived_volunteer_hours.id,
    archived_volunteer_hours.angler_id,
    archived_volunteer_hours.angler_number,
    archived_volunteer_hours.angler_name,
    archived_volunteer_hours.verified,
    archived_volunteer_hours.ldwf_entered,
    archived_volunteer_hours.ldwf_edited,
    archived_volunteer_hours.total_time,
    archived_volunteer_hours.start,
    archived_volunteer_hours."end",
    archived_volunteer_hours.recent,
    archived_volunteer_hours.created_at,
    archived_volunteer_hours.updated_at,
    archived_volunteer_hours.number_of_captures
   FROM archived_volunteer_hours;


--
-- Name: recapture_angler_sequence; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE recapture_angler_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: recapture_dispositions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE recapture_dispositions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: recapture_dispositions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE recapture_dispositions_id_seq OWNED BY recapture_dispositions.id;


--
-- Name: recaptures_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE recaptures_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: recaptures_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE recaptures_id_seq OWNED BY recaptures.id;


--
-- Name: recent_tags; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE recent_tags (
    id integer,
    tag_no character varying(255),
    tag_lot_id integer,
    tag_request_id integer,
    deleted boolean,
    unassign_option integer,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    old_anlger_id integer,
    prefix character varying(255),
    active boolean,
    assigned_at timestamp with time zone,
    recent boolean,
    search_vector tsvector,
    angler_id bigint,
    tag_suffix bigint,
    tag_prefix text,
    used boolean,
    used_at timestamp with time zone,
    angler_name text,
    angler_number character varying(20),
    seq_id bigint
);

ALTER TABLE ONLY recent_tags REPLICA IDENTITY NOTHING;


--
-- Name: requested_downloads; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE requested_downloads (
    id integer NOT NULL,
    token text NOT NULL,
    filename text NOT NULL,
    filepath text,
    should_email boolean DEFAULT false,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    user_id bigint NOT NULL
);


--
-- Name: requested_downloads_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE requested_downloads_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: requested_downloads_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE requested_downloads_id_seq OWNED BY requested_downloads.id;


--
-- Name: roles; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE roles (
    id integer NOT NULL,
    name character varying(255),
    resource_id integer,
    resource_type character varying(255),
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);


--
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE roles_id_seq OWNED BY roles.id;


--
-- Name: roles_users; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE roles_users (
    role_id integer,
    user_id integer,
    id bigint NOT NULL
);


--
-- Name: roles_users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE roles_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: roles_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE roles_users_id_seq OWNED BY roles_users.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE schema_migrations (
    version character varying(255) NOT NULL
);


--
-- Name: scratch_capture_types; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE scratch_capture_types (
    id integer NOT NULL,
    name text NOT NULL,
    displayable boolean NOT NULL,
    failure_scratch_capture_type_id bigint,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);


--
-- Name: scratch_capture_types_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE scratch_capture_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: scratch_capture_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE scratch_capture_types_id_seq OWNED BY scratch_capture_types.id;


--
-- Name: scratch_captures; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE scratch_captures (
    id integer NOT NULL,
    capture_date timestamp with time zone,
    length numeric,
    location_description text,
    latitude double precision,
    longitude double precision,
    comments text,
    tag_id integer,
    tag_number text,
    species_id integer,
    fish_condition_option_id integer,
    recapture_disposition_id integer,
    time_of_day_option_id integer,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    geom geometry(Point,4326),
    basin_id integer,
    sub_basin_id integer,
    gps_format_id bigint,
    angler_name text,
    angler_number text,
    angler_id bigint,
    scratch_capture_type_id integer NOT NULL,
    recapture_id integer
);


--
-- Name: scratch_captures_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE scratch_captures_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: scratch_captures_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE scratch_captures_id_seq OWNED BY scratch_captures.id;


--
-- Name: segments; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE segments (
    gid integer NOT NULL,
    objectid numeric(10,0),
    area numeric,
    perimeter double precision,
    subseg02 double precision,
    subseg02id double precision,
    basin character varying(18),
    subbasin character varying(10),
    subbasin_n character varying(254),
    sgmnt_name character varying(165),
    segment character varying(10),
    geom geometry(MultiPolygon)
);


--
-- Name: segments_gid_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE segments_gid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: segments_gid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE segments_gid_seq OWNED BY segments.gid;


--
-- Name: shirt_size_options; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE shirt_size_options (
    id integer NOT NULL,
    shirt_size_option character varying(255),
    item_ordinal_pos integer,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);


--
-- Name: shirt_size_options_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE shirt_size_options_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: shirt_size_options_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE shirt_size_options_id_seq OWNED BY shirt_size_options.id;


--
-- Name: species_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE species_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: species_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE species_id_seq OWNED BY species.id;


--
-- Name: species_lengths_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE species_lengths_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: species_lengths_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE species_lengths_id_seq OWNED BY species_lengths.id;


--
-- Name: sub-basin; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "sub-basin" (
    gid integer NOT NULL,
    objectid numeric(10,0),
    area numeric,
    perimeter numeric,
    subseg02 numeric(10,0),
    subseg02id numeric(10,0),
    subsegment character varying(15),
    name character varying(71),
    descriptio character varying(140),
    basin character varying(18),
    shape_area numeric,
    shape_len numeric,
    geom geometry(MultiPolygon),
    created_at timestamp with time zone DEFAULT '2015-12-01 03:29:39.224486+00'::timestamp with time zone NOT NULL,
    updated_at timestamp with time zone DEFAULT '2015-12-01 03:29:39.224486+00'::timestamp with time zone NOT NULL
);


--
-- Name: sub-basin_gid_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "sub-basin_gid_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sub-basin_gid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "sub-basin_gid_seq" OWNED BY "sub-basin".gid;


--
-- Name: tag_assignments; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW tag_assignments AS
 SELECT min(a.id) AS id,
    min(a.angler_id) AS angler_id,
    min((a.angler_number)::text) AS angler_number,
    min(a.tag_prefix) AS tag_prefix,
    min((a.updated_at)::date) AS assignment_at,
    min((a.tag_no)::text) AS start_tag,
    max((a.tag_no)::text) AS end_tag,
    count(a.tag_no) AS number_of_tags,
    max((a.tag_no)::text) FILTER (WHERE (a.used = true)) AS last_tag_used,
    array_agg(a.tag_no) AS tag_numbers,
    array_agg(a.id) AS tag_ids,
    a.seq_id
   FROM presentable_tags a
  WHERE (a.angler_id IS NOT NULL)
  GROUP BY a.seq_id, a.angler_id
  ORDER BY min((a.updated_at)::date) DESC;


--
-- Name: tag_end_user_types; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE tag_end_user_types (
    id integer NOT NULL,
    name character varying(255),
    deleted boolean,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);


--
-- Name: tag_end_user_types_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE tag_end_user_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tag_end_user_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE tag_end_user_types_id_seq OWNED BY tag_end_user_types.id;


--
-- Name: tag_lot_colors; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE tag_lot_colors (
    id integer NOT NULL,
    color character varying(255),
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);


--
-- Name: tag_lot_colors_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE tag_lot_colors_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tag_lot_colors_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE tag_lot_colors_id_seq OWNED BY tag_lot_colors.id;


--
-- Name: tag_lots; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE tag_lots (
    id integer NOT NULL,
    tag_lot_number integer,
    manufacturer_id integer,
    prefix character varying(255),
    start_no integer,
    end_no integer,
    end_user_type integer,
    cost integer,
    date_entered timestamp with time zone,
    ordered boolean,
    order_date timestamp with time zone,
    received boolean,
    received_date timestamp with time zone,
    deleted boolean,
    color integer,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    user_id bigint
);


--
-- Name: tag_lots_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE tag_lots_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tag_lots_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE tag_lots_id_seq OWNED BY tag_lots.id;


--
-- Name: tags_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE tags_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tags_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE tags_id_seq OWNED BY tags.id;


--
-- Name: time_of_day_options_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE time_of_day_options_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: time_of_day_options_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE time_of_day_options_id_seq OWNED BY time_of_day_options.id;


--
-- Name: unassign_tag_options; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE unassign_tag_options (
    id integer NOT NULL,
    unassign_tag_option character varying(255),
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);


--
-- Name: unassign_tag_options_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE unassign_tag_options_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: unassign_tag_options_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE unassign_tag_options_id_seq OWNED BY unassign_tag_options.id;


--
-- Name: user_couriers; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE user_couriers (
    id integer NOT NULL,
    daily_digest boolean DEFAULT false,
    user_id integer NOT NULL,
    notification_courier_id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    notification_topic_id integer NOT NULL
);


--
-- Name: user_couriers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE user_couriers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: user_couriers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE user_couriers_id_seq OWNED BY user_couriers.id;


--
-- Name: user_device_types; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE user_device_types (
    id integer NOT NULL,
    description character varying(255),
    created_at timestamp with time zone,
    updated_at timestamp with time zone
);


--
-- Name: user_device_types_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE user_device_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: user_device_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE user_device_types_id_seq OWNED BY user_device_types.id;


--
-- Name: user_devices; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE user_devices (
    id integer NOT NULL,
    device_token character varying(255) NOT NULL,
    user_id integer NOT NULL,
    user_device_types_id integer NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone
);


--
-- Name: user_devices_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE user_devices_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: user_devices_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE user_devices_id_seq OWNED BY user_devices.id;


--
-- Name: user_tag_requests; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE user_tag_requests (
    id integer NOT NULL,
    number_of_tags integer NOT NULL,
    fulfilled boolean DEFAULT false,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    tag_type character varying(255) NOT NULL,
    uuid character varying,
    angler_id bigint NOT NULL
);


--
-- Name: user_tag_requests_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE user_tag_requests_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: user_tag_requests_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE user_tag_requests_id_seq OWNED BY user_tag_requests.id;


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: users_roles; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE users_roles (
    user_id integer,
    role_id integer
);


--
-- Name: versions; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE versions (
    id integer NOT NULL,
    item_type character varying NOT NULL,
    item_id integer NOT NULL,
    event character varying NOT NULL,
    whodunnit bigint,
    object json,
    created_at timestamp with time zone,
    activity_type text,
    object_changes jsonb,
    meta jsonb
);


--
-- Name: versions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE versions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: versions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE versions_id_seq OWNED BY versions.id;


--
-- Name: volunteer_time_logs_bad; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE volunteer_time_logs_bad (
    id integer,
    start timestamp with time zone,
    "end" timestamp with time zone,
    verified boolean,
    angler_id character varying(255),
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    ldwf_entered boolean,
    ldwf_edited boolean
);


--
-- Name: volunteer_time_logs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE volunteer_time_logs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: volunteer_time_logs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE volunteer_time_logs_id_seq OWNED BY volunteer_time_logs.id;


--
-- Name: zipcodes; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE zipcodes (
    id integer NOT NULL,
    latitude double precision,
    longitude double precision,
    geom geometry,
    CONSTRAINT enforce_dims_geom CHECK ((st_ndims(geom) = 2)),
    CONSTRAINT enforce_geotype_geom CHECK (((geometrytype(geom) = 'POINT'::text) OR (geom IS NULL))),
    CONSTRAINT enforce_srid_geom CHECK ((st_srid(geom) = 4326))
);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY about_tagging_programs ALTER COLUMN id SET DEFAULT nextval('about_tagging_programs_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY activity_log_types ALTER COLUMN id SET DEFAULT nextval('activity_log_types_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY activity_logs ALTER COLUMN id SET DEFAULT nextval('activity_logs_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY angler_item_types ALTER COLUMN id SET DEFAULT nextval('angler_item_types_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY angler_items ALTER COLUMN id SET DEFAULT nextval('angler_items_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY anglers ALTER COLUMN id SET DEFAULT nextval('anglers_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY announcements ALTER COLUMN id SET DEFAULT nextval('announcements_id_seq'::regclass);


--
-- Name: gid; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY basins ALTER COLUMN gid SET DEFAULT nextval('basins_gid_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY blurbs ALTER COLUMN id SET DEFAULT nextval('blurbs_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY captures ALTER COLUMN id SET DEFAULT nextval('captures_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY captures_old ALTER COLUMN id SET DEFAULT nextval('captures_old_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY devices ALTER COLUMN id SET DEFAULT nextval('devices_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY draft_captures ALTER COLUMN id SET DEFAULT nextval('draft_captures_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY email_jobs ALTER COLUMN id SET DEFAULT nextval('email_jobs_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY fish_condition_options ALTER COLUMN id SET DEFAULT nextval('fish_condition_options_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY fish_entry_photos ALTER COLUMN id SET DEFAULT nextval('fish_entry_photos_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY fish_event_location_descriptions ALTER COLUMN id SET DEFAULT nextval('fish_event_location_descriptions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY front_page_photo_stream_items ALTER COLUMN id SET DEFAULT nextval('front_page_photo_stream_items_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY gps_formats ALTER COLUMN id SET DEFAULT nextval('gps_formats_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY how_to_tags ALTER COLUMN id SET DEFAULT nextval('how_to_tags_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY items ALTER COLUMN id SET DEFAULT nextval('items_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY manufacturers ALTER COLUMN id SET DEFAULT nextval('manufacturers_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY marinas ALTER COLUMN id SET DEFAULT nextval('marinas_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY news_items ALTER COLUMN id SET DEFAULT nextval('news_items_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY notification_couriers ALTER COLUMN id SET DEFAULT nextval('notification_couriers_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY notification_jobs ALTER COLUMN id SET DEFAULT nextval('notification_jobs_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY notification_topics ALTER COLUMN id SET DEFAULT nextval('notification_topics_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY notifications ALTER COLUMN id SET DEFAULT nextval('notifications_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY oauth_access_grants ALTER COLUMN id SET DEFAULT nextval('oauth_access_grants_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY oauth_access_tokens ALTER COLUMN id SET DEFAULT nextval('oauth_access_tokens_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY oauth_applications ALTER COLUMN id SET DEFAULT nextval('oauth_applications_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY photos ALTER COLUMN id SET DEFAULT nextval('photos_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY recapture_dispositions ALTER COLUMN id SET DEFAULT nextval('recapture_dispositions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY recaptures ALTER COLUMN id SET DEFAULT nextval('recaptures_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY requested_downloads ALTER COLUMN id SET DEFAULT nextval('requested_downloads_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY roles ALTER COLUMN id SET DEFAULT nextval('roles_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY roles_users ALTER COLUMN id SET DEFAULT nextval('roles_users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY scratch_capture_types ALTER COLUMN id SET DEFAULT nextval('scratch_capture_types_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY scratch_captures ALTER COLUMN id SET DEFAULT nextval('scratch_captures_id_seq'::regclass);


--
-- Name: gid; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY segments ALTER COLUMN gid SET DEFAULT nextval('segments_gid_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY shirt_size_options ALTER COLUMN id SET DEFAULT nextval('shirt_size_options_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY species ALTER COLUMN id SET DEFAULT nextval('species_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY species_lengths ALTER COLUMN id SET DEFAULT nextval('species_lengths_id_seq'::regclass);


--
-- Name: gid; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "sub-basin" ALTER COLUMN gid SET DEFAULT nextval('"sub-basin_gid_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY tag_end_user_types ALTER COLUMN id SET DEFAULT nextval('tag_end_user_types_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY tag_lot_colors ALTER COLUMN id SET DEFAULT nextval('tag_lot_colors_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY tag_lots ALTER COLUMN id SET DEFAULT nextval('tag_lots_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY tags ALTER COLUMN id SET DEFAULT nextval('tags_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY time_of_day_options ALTER COLUMN id SET DEFAULT nextval('time_of_day_options_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY unassign_tag_options ALTER COLUMN id SET DEFAULT nextval('unassign_tag_options_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_couriers ALTER COLUMN id SET DEFAULT nextval('user_couriers_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_device_types ALTER COLUMN id SET DEFAULT nextval('user_device_types_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_devices ALTER COLUMN id SET DEFAULT nextval('user_devices_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_tag_requests ALTER COLUMN id SET DEFAULT nextval('user_tag_requests_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY versions ALTER COLUMN id SET DEFAULT nextval('versions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY volunteer_time_logs ALTER COLUMN id SET DEFAULT nextval('volunteer_time_logs_id_seq'::regclass);


--
-- Name: tags_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY tags
    ADD CONSTRAINT tags_pkey PRIMARY KEY (id);


--
-- Name: archived_tags; Type: MATERIALIZED VIEW; Schema: public; Owner: -; Tablespace: 
--

CREATE MATERIALIZED VIEW archived_tags AS
 WITH selected_tags AS (
         SELECT tags_1.id,
            tags_1.tag_no,
            tags_1.tag_lot_id,
            tags_1.tag_request_id,
            tags_1.deleted,
            tags_1.unassign_option,
            tags_1.created_at,
            tags_1.updated_at,
            tags_1.old_anlger_id,
            tags_1.prefix,
            tags_1.active,
            tags_1.assigned_at,
            tags_1.recent,
            tags_1.search_vector,
            tags_1.angler_id,
            ("substring"((tags_1.tag_no)::text, '[^A-Z]+'::text))::bigint AS tag_suffix,
            "substring"((tags_1.tag_no)::text, '^[A-Z]+'::text) AS tag_prefix,
            ((count(recaptures.id) + count(captures.id)) > 0) AS used,
            GREATEST(max(captures.updated_at), max(recaptures.updated_at)) AS used_at
           FROM ((tags tags_1
             LEFT JOIN recaptures ON ((tags_1.id = recaptures.tag_id)))
             LEFT JOIN captures ON ((tags_1.id = captures.tag_id)))
          WHERE (tags_1.recent = false)
          GROUP BY tags_1.id
        )
 SELECT tags.id,
    tags.tag_no,
    tags.tag_lot_id,
    tags.tag_request_id,
    tags.deleted,
    tags.unassign_option,
    tags.created_at,
    tags.updated_at,
    tags.old_anlger_id,
    tags.prefix,
    tags.active,
    tags.assigned_at,
    tags.recent,
    tags.search_vector,
    tags.angler_id,
    tags.tag_suffix,
    tags.tag_prefix,
    tags.used,
    tags.used_at,
    concat(anglers.first_name, ' ', anglers.last_name) AS angler_name,
    anglers.angler_id AS angler_number,
    (tags.tag_suffix - row_number() OVER (PARTITION BY tags.tag_prefix, tags.angler_id, (tags.updated_at)::date ORDER BY tags.tag_prefix, tags.tag_suffix, tags.angler_id)) AS seq_id
   FROM (selected_tags tags
     LEFT JOIN anglers ON ((tags.angler_id = anglers.id)))
  WITH NO DATA;


--
-- Name: about_tagging_programs_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY about_tagging_programs
    ADD CONSTRAINT about_tagging_programs_pkey PRIMARY KEY (id);


--
-- Name: activity_log_types_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY activity_log_types
    ADD CONSTRAINT activity_log_types_pkey PRIMARY KEY (id);


--
-- Name: activity_logs_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY activity_logs
    ADD CONSTRAINT activity_logs_pkey PRIMARY KEY (id);


--
-- Name: angler_id_uniq; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY anglers
    ADD CONSTRAINT angler_id_uniq UNIQUE (id);


--
-- Name: angler_item_types_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY angler_item_types
    ADD CONSTRAINT angler_item_types_pkey PRIMARY KEY (id);


--
-- Name: angler_items_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY angler_items
    ADD CONSTRAINT angler_items_pkey PRIMARY KEY (id);


--
-- Name: anglers_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY anglers
    ADD CONSTRAINT anglers_pkey PRIMARY KEY (id);


--
-- Name: announcements_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY announcements
    ADD CONSTRAINT announcements_pkey PRIMARY KEY (id);


--
-- Name: basins_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY basins
    ADD CONSTRAINT basins_pkey PRIMARY KEY (gid);


--
-- Name: blurbs_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY blurbs
    ADD CONSTRAINT blurbs_pkey PRIMARY KEY (id);


--
-- Name: cap_tag_fk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY captures
    ADD CONSTRAINT cap_tag_fk UNIQUE (tag_id);


--
-- Name: captures_duplicates_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY capture_duplicates
    ADD CONSTRAINT captures_duplicates_pkey PRIMARY KEY (id);


--
-- Name: captures_old_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY captures_old
    ADD CONSTRAINT captures_old_pkey PRIMARY KEY (id);


--
-- Name: captures_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY captures
    ADD CONSTRAINT captures_pkey PRIMARY KEY (id);


--
-- Name: devices_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY devices
    ADD CONSTRAINT devices_pkey PRIMARY KEY (id);


--
-- Name: draft_captures_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY draft_captures
    ADD CONSTRAINT draft_captures_pkey PRIMARY KEY (id);


--
-- Name: email_jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY email_jobs
    ADD CONSTRAINT email_jobs_pkey PRIMARY KEY (id);


--
-- Name: fish_condition_options_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY fish_condition_options
    ADD CONSTRAINT fish_condition_options_pkey PRIMARY KEY (id);


--
-- Name: fish_entry_photos_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY fish_entry_photos
    ADD CONSTRAINT fish_entry_photos_pkey PRIMARY KEY (id);


--
-- Name: fish_event_location_descriptions_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY fish_event_location_descriptions
    ADD CONSTRAINT fish_event_location_descriptions_pkey PRIMARY KEY (id);


--
-- Name: front_page_photo_stream_items_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY front_page_photo_stream_items
    ADD CONSTRAINT front_page_photo_stream_items_pkey PRIMARY KEY (id);


--
-- Name: gps_formats_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY gps_formats
    ADD CONSTRAINT gps_formats_pkey PRIMARY KEY (id);


--
-- Name: how_to_tags_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY how_to_tags
    ADD CONSTRAINT how_to_tags_pkey PRIMARY KEY (id);


--
-- Name: items_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY items
    ADD CONSTRAINT items_pkey PRIMARY KEY (id);


--
-- Name: manufacturers_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY manufacturers
    ADD CONSTRAINT manufacturers_pkey PRIMARY KEY (id);


--
-- Name: marinas_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY marinas
    ADD CONSTRAINT marinas_pkey PRIMARY KEY (id);


--
-- Name: news_items_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY news_items
    ADD CONSTRAINT news_items_pkey PRIMARY KEY (id);


--
-- Name: notification_couriers_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY notification_couriers
    ADD CONSTRAINT notification_couriers_pkey PRIMARY KEY (id);


--
-- Name: notification_jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY notification_jobs
    ADD CONSTRAINT notification_jobs_pkey PRIMARY KEY (id);


--
-- Name: notification_topics_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY notification_topics
    ADD CONSTRAINT notification_topics_pkey PRIMARY KEY (id);


--
-- Name: notifications_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY notifications
    ADD CONSTRAINT notifications_pkey PRIMARY KEY (id);


--
-- Name: oauth_access_grants_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY oauth_access_grants
    ADD CONSTRAINT oauth_access_grants_pkey PRIMARY KEY (id);


--
-- Name: oauth_access_tokens_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY oauth_access_tokens
    ADD CONSTRAINT oauth_access_tokens_pkey PRIMARY KEY (id);


--
-- Name: oauth_applications_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY oauth_applications
    ADD CONSTRAINT oauth_applications_pkey PRIMARY KEY (id);


--
-- Name: photos_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY photos
    ADD CONSTRAINT photos_pkey PRIMARY KEY (id);


--
-- Name: recapture_dispositions_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY recapture_dispositions
    ADD CONSTRAINT recapture_dispositions_pkey PRIMARY KEY (id);


--
-- Name: recaptures_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY recaptures
    ADD CONSTRAINT recaptures_pkey PRIMARY KEY (id);


--
-- Name: requested_downloads_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY requested_downloads
    ADD CONSTRAINT requested_downloads_pkey PRIMARY KEY (id);


--
-- Name: roles_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- Name: roles_users_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY roles_users
    ADD CONSTRAINT roles_users_pkey PRIMARY KEY (id);


--
-- Name: scratch_capture_types_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY scratch_capture_types
    ADD CONSTRAINT scratch_capture_types_pkey PRIMARY KEY (id);


--
-- Name: scratch_captures_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY scratch_captures
    ADD CONSTRAINT scratch_captures_pkey PRIMARY KEY (id);


--
-- Name: segments_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY segments
    ADD CONSTRAINT segments_pkey PRIMARY KEY (gid);


--
-- Name: shirt_size_options_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY shirt_size_options
    ADD CONSTRAINT shirt_size_options_pkey PRIMARY KEY (id);


--
-- Name: species_lengths_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY species_lengths
    ADD CONSTRAINT species_lengths_pkey PRIMARY KEY (id);


--
-- Name: species_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY species
    ADD CONSTRAINT species_pkey PRIMARY KEY (id);


--
-- Name: sub-basin_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "sub-basin"
    ADD CONSTRAINT "sub-basin_pkey" PRIMARY KEY (gid);


--
-- Name: tag_end_user_types_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY tag_end_user_types
    ADD CONSTRAINT tag_end_user_types_pkey PRIMARY KEY (id);


--
-- Name: tag_lot_colors_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY tag_lot_colors
    ADD CONSTRAINT tag_lot_colors_pkey PRIMARY KEY (id);


--
-- Name: tag_lots_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY tag_lots
    ADD CONSTRAINT tag_lots_pkey PRIMARY KEY (id);


--
-- Name: time_of_day_options_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY time_of_day_options
    ADD CONSTRAINT time_of_day_options_pkey PRIMARY KEY (id);


--
-- Name: unassign_tag_options_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY unassign_tag_options
    ADD CONSTRAINT unassign_tag_options_pkey PRIMARY KEY (id);


--
-- Name: user_couriers_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY user_couriers
    ADD CONSTRAINT user_couriers_pkey PRIMARY KEY (id);


--
-- Name: user_device_types_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY user_device_types
    ADD CONSTRAINT user_device_types_pkey PRIMARY KEY (id);


--
-- Name: user_devices_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY user_devices
    ADD CONSTRAINT user_devices_pkey PRIMARY KEY (id);


--
-- Name: user_tag_requests_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY user_tag_requests
    ADD CONSTRAINT user_tag_requests_pkey PRIMARY KEY (id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: versions_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY versions
    ADD CONSTRAINT versions_pkey PRIMARY KEY (id);


--
-- Name: volunteer_time_logs_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY volunteer_time_logs
    ADD CONSTRAINT volunteer_time_logs_pkey PRIMARY KEY (id);


--
-- Name: zipcodes_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY zipcodes
    ADD CONSTRAINT zipcodes_pkey PRIMARY KEY (id);


--
-- Name: captures_length_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX captures_length_idx ON captures USING btree (length);


--
-- Name: captures_tag_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX captures_tag_idx ON captures USING btree (tag_id);


--
-- Name: idx_angler_items_angler_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX idx_angler_items_angler_id ON angler_items USING btree (angler_id);


--
-- Name: idx_angler_items_angler_item_type_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX idx_angler_items_angler_item_type_id ON angler_items USING btree (angler_item_type_id);


--
-- Name: idx_angler_items_item_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX idx_angler_items_item_id ON angler_items USING btree (item_id);


--
-- Name: idx_archived_captures_where_tag_year; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX idx_archived_captures_where_tag_year ON archived_captures USING gist (year_id, angler_id, tag_number, location_description, comments, time_of_day_option_id, fish_condition_option_id, length, species_id, capture_date, basin_id, sub_basin_id);


--
-- Name: idx_archived_recaptures_where_tag_yer; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX idx_archived_recaptures_where_tag_yer ON archived_recaptures USING gist (year_id, recapture_angler_id, tag_number, location_description, comments, time_of_day_option_id, fish_condition_option_id, length, species_id, capture_date, basin_id, sub_basin_id);


--
-- Name: idx_basins_geom; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX idx_basins_geom ON basins USING gist (geom);


--
-- Name: idx_capture_old_angler_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX idx_capture_old_angler_id ON captures_old USING btree (angler_id);


--
-- Name: idx_capture_old_date; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX idx_capture_old_date ON captures_old USING btree (capture_date);


--
-- Name: idx_captures_created_at; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX idx_captures_created_at ON captures USING btree (created_at);


--
-- Name: idx_lower_angler_id_lower_firs_name_lower_last_name; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX idx_lower_angler_id_lower_firs_name_lower_last_name ON anglers USING btree (lower((angler_id)::text), lower((first_name)::text), lower((last_name)::text));


--
-- Name: idx_recaptures_created_at; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX idx_recaptures_created_at ON recaptures USING btree (created_at);


--
-- Name: idx_species_length_min_max; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX idx_species_length_min_max ON species_lengths USING btree (min, max);


--
-- Name: idx_sub_basins_geom; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX idx_sub_basins_geom ON "sub-basin" USING gist (geom);


--
-- Name: idx_tags_tag_no; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX idx_tags_tag_no ON tags USING btree (lower((tag_no)::text));


--
-- Name: index_anglers_on_angler_id_and_first_name_and_last_name; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_anglers_on_angler_id_and_first_name_and_last_name ON anglers USING btree (angler_id, first_name, last_name);


--
-- Name: index_anglers_on_search_vector; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_anglers_on_search_vector ON anglers USING gin (search_vector);


--
-- Name: index_archived_captures_on_geom; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_archived_captures_on_geom ON archived_captures USING btree (geom);


--
-- Name: index_archived_captures_on_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_archived_captures_on_id ON archived_captures USING btree (id);


--
-- Name: index_archived_captures_on_verified; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_archived_captures_on_verified ON archived_captures USING btree (verified);


--
-- Name: index_archived_recaptures_on_capture_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_archived_recaptures_on_capture_id ON archived_recaptures USING btree (capture_id);


--
-- Name: index_archived_recaptures_on_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_archived_recaptures_on_id ON archived_recaptures USING btree (id);


--
-- Name: index_archived_recaptures_on_verified; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_archived_recaptures_on_verified ON archived_recaptures USING btree (verified);


--
-- Name: index_archived_tags_on_angler_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_archived_tags_on_angler_id ON archived_tags USING btree (angler_id);


--
-- Name: index_archived_tags_on_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_archived_tags_on_id ON archived_tags USING btree (id);


--
-- Name: index_archived_tags_on_id_and_used; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_archived_tags_on_id_and_used ON archived_tags USING btree (id, used);


--
-- Name: index_archived_tags_on_recent; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_archived_tags_on_recent ON archived_tags USING btree (recent);


--
-- Name: index_archived_tags_on_seq_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_archived_tags_on_seq_id ON archived_tags USING btree (seq_id);


--
-- Name: index_archived_tags_on_tag_no; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_archived_tags_on_tag_no ON archived_tags USING btree (tag_no);


--
-- Name: index_archived_tags_on_used_at; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_archived_tags_on_used_at ON archived_tags USING btree (used_at);


--
-- Name: index_archived_volunteer_hours_on_angler_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_archived_volunteer_hours_on_angler_id ON archived_volunteer_hours USING btree (angler_id);


--
-- Name: index_archived_volunteer_hours_on_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_archived_volunteer_hours_on_id ON archived_volunteer_hours USING btree (id);


--
-- Name: index_archived_volunteer_hours_on_number_of_captures; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_archived_volunteer_hours_on_number_of_captures ON archived_volunteer_hours USING btree (number_of_captures);


--
-- Name: index_archived_volunteer_hours_on_recent; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_archived_volunteer_hours_on_recent ON archived_volunteer_hours USING btree (recent);


--
-- Name: index_captures_geom; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_captures_geom ON captures USING gist (geom);


--
-- Name: index_captures_old_geom; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_captures_old_geom ON captures_old USING gist (geom);


--
-- Name: index_captures_on_fish_condition_option_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_captures_on_fish_condition_option_id ON captures USING btree (fish_condition_option_id);


--
-- Name: index_captures_on_gps_format_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_captures_on_gps_format_id ON captures USING btree (gps_format_id);


--
-- Name: index_captures_on_recent; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_captures_on_recent ON captures USING btree (recent);


--
-- Name: index_captures_on_search_vector; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_captures_on_search_vector ON captures USING gin (search_vector);


--
-- Name: index_captures_on_species_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_captures_on_species_id ON captures USING btree (species_id);


--
-- Name: index_captures_on_species_length_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_captures_on_species_length_id ON captures USING btree (species_length_id);


--
-- Name: index_captures_on_time_of_day_option_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_captures_on_time_of_day_option_id ON captures USING btree (time_of_day_option_id);


--
-- Name: index_devices_on_identifier; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_devices_on_identifier ON devices USING btree (identifier);


--
-- Name: index_draft_captures_on_search_vector; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_draft_captures_on_search_vector ON draft_captures USING gin (search_vector);


--
-- Name: index_draft_captures_on_uuid; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_draft_captures_on_uuid ON draft_captures USING btree (uuid);


--
-- Name: index_fish_event_location_descriptions_on_description; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_fish_event_location_descriptions_on_description ON fish_event_location_descriptions USING btree (description);


--
-- Name: index_gps_formats_on_format_type; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_gps_formats_on_format_type ON gps_formats USING btree (format_type);


--
-- Name: index_oauth_access_grants_on_token; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_oauth_access_grants_on_token ON oauth_access_grants USING btree (token);


--
-- Name: index_oauth_access_tokens_on_refresh_token; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_oauth_access_tokens_on_refresh_token ON oauth_access_tokens USING btree (refresh_token);


--
-- Name: index_oauth_access_tokens_on_resource_owner_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_oauth_access_tokens_on_resource_owner_id ON oauth_access_tokens USING btree (resource_owner_id);


--
-- Name: index_oauth_access_tokens_on_token; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_oauth_access_tokens_on_token ON oauth_access_tokens USING btree (token);


--
-- Name: index_oauth_applications_on_uid; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_oauth_applications_on_uid ON oauth_applications USING btree (uid);


--
-- Name: index_recaptures_on_angler_creation_token; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_recaptures_on_angler_creation_token ON recaptures USING btree (angler_creation_token);


--
-- Name: index_recaptures_on_capture_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_recaptures_on_capture_id ON recaptures USING btree (capture_id);


--
-- Name: index_recaptures_on_fish_condition_option_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_recaptures_on_fish_condition_option_id ON recaptures USING btree (fish_condition_option_id);


--
-- Name: index_recaptures_on_gps_format_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_recaptures_on_gps_format_id ON recaptures USING btree (gps_format_id);


--
-- Name: index_recaptures_on_recent; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_recaptures_on_recent ON recaptures USING btree (recent);


--
-- Name: index_recaptures_on_search_vector; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_recaptures_on_search_vector ON recaptures USING gin (search_vector);


--
-- Name: index_recaptures_on_species_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_recaptures_on_species_id ON recaptures USING btree (species_id);


--
-- Name: index_recaptures_on_species_length_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_recaptures_on_species_length_id ON recaptures USING btree (species_length_id);


--
-- Name: index_recaptures_on_tag_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_recaptures_on_tag_id ON recaptures USING btree (tag_id);


--
-- Name: index_recaptures_on_time_of_day_option_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_recaptures_on_time_of_day_option_id ON recaptures USING btree (time_of_day_option_id);


--
-- Name: index_roles_on_name; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_roles_on_name ON roles USING btree (name);


--
-- Name: index_roles_on_name_and_resource_type_and_resource_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_roles_on_name_and_resource_type_and_resource_id ON roles USING btree (name, resource_type, resource_id);


--
-- Name: index_tags_on_angler_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_tags_on_angler_id ON tags USING btree (angler_id);


--
-- Name: index_tags_on_recent; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_tags_on_recent ON tags USING btree (recent);


--
-- Name: index_tags_on_search_vector; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_tags_on_search_vector ON tags USING gin (search_vector);


--
-- Name: index_tags_on_tag_lot_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_tags_on_tag_lot_id ON tags USING btree (tag_lot_id);


--
-- Name: index_tags_on_tag_no; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_tags_on_tag_no ON tags USING btree (tag_no);


--
-- Name: index_user_devices_on_user_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_user_devices_on_user_id ON user_devices USING btree (user_id);


--
-- Name: index_users_on_email; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_users_on_email ON users USING btree (email);


--
-- Name: index_users_on_reset_password_token; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_users_on_reset_password_token ON users USING btree (reset_password_token);


--
-- Name: index_users_on_search_vector; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_users_on_search_vector ON users USING gin (search_vector);


--
-- Name: index_users_roles_on_user_id_and_role_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_users_roles_on_user_id_and_role_id ON users_roles USING btree (user_id, role_id);


--
-- Name: index_versions_on_item_type_and_item_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_versions_on_item_type_and_item_id ON versions USING btree (item_type, item_id);


--
-- Name: index_versions_on_whodunnit_and_created_at_and_item_type; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_versions_on_whodunnit_and_created_at_and_item_type ON versions USING btree (whodunnit, created_at, item_type);


--
-- Name: index_volunteer_time_logs_on_recent; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_volunteer_time_logs_on_recent ON volunteer_time_logs USING btree (recent);


--
-- Name: index_volunteer_time_logs_on_search_vector; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_volunteer_time_logs_on_search_vector ON volunteer_time_logs USING gin (search_vector);


--
-- Name: notifications_topic_user_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX notifications_topic_user_idx ON notification_topics_users USING btree (notification_topic_id, user_id);


--
-- Name: recaptures_date_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX recaptures_date_idx ON recaptures USING btree (capture_date);


--
-- Name: recaptures_geom_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX recaptures_geom_idx ON recaptures USING btree (geom);


--
-- Name: recaptures_length_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX recaptures_length_idx ON recaptures USING btree (length);


--
-- Name: recaptures_tag_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX recaptures_tag_idx ON recaptures USING btree (tag_id);


--
-- Name: ru_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX ru_idx ON roles_users USING btree (role_id, user_id);


--
-- Name: tags_prefix_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX tags_prefix_idx ON tags USING btree (prefix);


--
-- Name: unique_schema_migrations; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX unique_schema_migrations ON schema_migrations USING btree (version);


--
-- Name: _RETURN; Type: RULE; Schema: public; Owner: -
--

CREATE RULE "_RETURN" AS
    ON SELECT TO recent_tags DO INSTEAD  WITH selected_tags AS (
         SELECT tags_1.id,
            tags_1.tag_no,
            tags_1.tag_lot_id,
            tags_1.tag_request_id,
            tags_1.deleted,
            tags_1.unassign_option,
            tags_1.created_at,
            tags_1.updated_at,
            tags_1.old_anlger_id,
            tags_1.prefix,
            tags_1.active,
            tags_1.assigned_at,
            tags_1.recent,
            tags_1.search_vector,
            tags_1.angler_id,
            ("substring"((tags_1.tag_no)::text, '[^A-Z]+'::text))::bigint AS tag_suffix,
            "substring"((tags_1.tag_no)::text, '^[A-Z]+'::text) AS tag_prefix,
            ((count(recaptures.id) + count(captures.id)) > 0) AS used,
            GREATEST(max(captures.updated_at), max(recaptures.updated_at)) AS used_at
           FROM ((tags tags_1
             LEFT JOIN recaptures ON ((tags_1.id = recaptures.tag_id)))
             LEFT JOIN captures ON ((tags_1.id = captures.tag_id)))
          WHERE (tags_1.recent = true)
          GROUP BY tags_1.id
        )
 SELECT tags.id,
    tags.tag_no,
    tags.tag_lot_id,
    tags.tag_request_id,
    tags.deleted,
    tags.unassign_option,
    tags.created_at,
    tags.updated_at,
    tags.old_anlger_id,
    tags.prefix,
    tags.active,
    tags.assigned_at,
    tags.recent,
    tags.search_vector,
    tags.angler_id,
    tags.tag_suffix,
    tags.tag_prefix,
    tags.used,
    tags.used_at,
    concat(anglers.first_name, ' ', anglers.last_name) AS angler_name,
    anglers.angler_id AS angler_number,
    ((-1) * (tags.tag_suffix - row_number() OVER (PARTITION BY tags.tag_prefix, tags.angler_id, (tags.updated_at)::date ORDER BY tags.tag_prefix, tags.tag_suffix, tags.angler_id))) AS seq_id
   FROM (selected_tags tags
     LEFT JOIN anglers ON ((tags.angler_id = anglers.id)));


--
-- Name: _RETURN; Type: RULE; Schema: public; Owner: -
--

CREATE RULE "_RETURN" AS
    ON SELECT TO presentable_tags DO INSTEAD  SELECT recent_tags.id,
    recent_tags.tag_no,
    recent_tags.tag_lot_id,
    recent_tags.tag_request_id,
    recent_tags.deleted,
    recent_tags.unassign_option,
    recent_tags.created_at,
    recent_tags.updated_at,
    recent_tags.old_anlger_id,
    recent_tags.prefix,
    recent_tags.active,
    recent_tags.assigned_at,
    recent_tags.recent,
    recent_tags.search_vector,
    recent_tags.angler_id,
    recent_tags.tag_suffix,
    recent_tags.tag_prefix,
    recent_tags.used,
    recent_tags.used_at,
    recent_tags.angler_name,
    recent_tags.angler_number,
    recent_tags.seq_id
   FROM recent_tags
UNION ALL
 SELECT archived_tags.id,
    archived_tags.tag_no,
    archived_tags.tag_lot_id,
    archived_tags.tag_request_id,
    archived_tags.deleted,
    archived_tags.unassign_option,
    archived_tags.created_at,
    archived_tags.updated_at,
    archived_tags.old_anlger_id,
    archived_tags.prefix,
    archived_tags.active,
    archived_tags.assigned_at,
    archived_tags.recent,
    archived_tags.search_vector,
    archived_tags.angler_id,
    archived_tags.tag_suffix,
    archived_tags.tag_prefix,
    archived_tags.used,
    archived_tags.used_at,
    archived_tags.angler_name,
    archived_tags.angler_number,
    archived_tags.seq_id
   FROM archived_tags;


--
-- Name: anglers_before_insert_update_row_tr; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER anglers_before_insert_update_row_tr BEFORE INSERT OR UPDATE ON anglers FOR EACH ROW EXECUTE PROCEDURE anglers_before_insert_update_row_tr();


--
-- Name: captures_before_insert_update_row_tr; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER captures_before_insert_update_row_tr BEFORE INSERT OR UPDATE ON captures FOR EACH ROW EXECUTE PROCEDURE captures_before_insert_update_row_tr();


--
-- Name: draft_captures_before_insert_update_row_tr; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER draft_captures_before_insert_update_row_tr BEFORE INSERT OR UPDATE ON draft_captures FOR EACH ROW EXECUTE PROCEDURE draft_captures_before_insert_update_row_tr();


--
-- Name: recaptures_before_insert_update_row_tr; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER recaptures_before_insert_update_row_tr BEFORE INSERT OR UPDATE ON recaptures FOR EACH ROW EXECUTE PROCEDURE recaptures_before_insert_update_row_tr();


--
-- Name: tags_before_insert_update_row_tr; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER tags_before_insert_update_row_tr BEFORE INSERT OR UPDATE ON tags FOR EACH ROW EXECUTE PROCEDURE tags_before_insert_update_row_tr();


--
-- Name: users_before_insert_update_row_tr; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER users_before_insert_update_row_tr BEFORE INSERT OR UPDATE ON users FOR EACH ROW EXECUTE PROCEDURE users_before_insert_update_row_tr();


--
-- Name: volunteer_time_logs_before_insert_update_row_tr; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER volunteer_time_logs_before_insert_update_row_tr BEFORE INSERT OR UPDATE ON volunteer_time_logs FOR EACH ROW EXECUTE PROCEDURE volunteer_time_logs_before_insert_update_row_tr();


--
-- Name: alog_type_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY activity_logs
    ADD CONSTRAINT alog_type_fk FOREIGN KEY (activity_log_type_id) REFERENCES activity_log_types(id);


--
-- Name: ang_shirt_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY anglers
    ADD CONSTRAINT ang_shirt_fk FOREIGN KEY (shirt_size) REFERENCES shirt_size_options(id);


--
-- Name: ang_tag_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY anglers
    ADD CONSTRAINT ang_tag_fk FOREIGN KEY (tag_end_user_type) REFERENCES tag_end_user_types(id);


--
-- Name: angler_items_angler_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY angler_items
    ADD CONSTRAINT angler_items_angler_id_fkey FOREIGN KEY (angler_id) REFERENCES anglers(id);


--
-- Name: angler_items_angler_item_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY angler_items
    ADD CONSTRAINT angler_items_angler_item_type_id_fkey FOREIGN KEY (angler_item_type_id) REFERENCES angler_item_types(id);


--
-- Name: angler_items_item_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY angler_items
    ADD CONSTRAINT angler_items_item_id_fkey FOREIGN KEY (item_id) REFERENCES items(id);


--
-- Name: anglers_prefered_incentive_item_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY anglers
    ADD CONSTRAINT anglers_prefered_incentive_item_id_fkey FOREIGN KEY (prefered_incentive_item_id) REFERENCES items(id);


--
-- Name: cap_fishcond_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY captures
    ADD CONSTRAINT cap_fishcond_fk FOREIGN KEY (fish_condition_option_id) REFERENCES fish_condition_options(id);


--
-- Name: cap_species_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY captures
    ADD CONSTRAINT cap_species_fk FOREIGN KEY (species_id) REFERENCES species(id);


--
-- Name: cap_time_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY captures
    ADD CONSTRAINT cap_time_fk FOREIGN KEY (time_of_day_option_id) REFERENCES time_of_day_options(id);


--
-- Name: captures_basin_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY captures
    ADD CONSTRAINT captures_basin_id_fk FOREIGN KEY (basin_id) REFERENCES basins(gid);


--
-- Name: captures_species_length_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY captures
    ADD CONSTRAINT captures_species_length_id_fkey FOREIGN KEY (species_length_id) REFERENCES species_lengths(id);


--
-- Name: captures_sub_basin_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY captures
    ADD CONSTRAINT captures_sub_basin_id_fk FOREIGN KEY (sub_basin_id) REFERENCES "sub-basin"(gid);


--
-- Name: draft_captures_species_length_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY draft_captures
    ADD CONSTRAINT draft_captures_species_length_id_fkey FOREIGN KEY (species_length_id) REFERENCES species_lengths(id);


--
-- Name: fk_captures_gps_type; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY captures
    ADD CONSTRAINT fk_captures_gps_type FOREIGN KEY (entered_gps_type) REFERENCES gps_formats(format_type);


--
-- Name: fk_devices_users; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY devices
    ADD CONSTRAINT fk_devices_users FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: fk_draft_captures_gps_type; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY draft_captures
    ADD CONSTRAINT fk_draft_captures_gps_type FOREIGN KEY (entered_gps_type) REFERENCES gps_formats(format_type);


--
-- Name: fk_part_captures_fish_cond; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY draft_captures
    ADD CONSTRAINT fk_part_captures_fish_cond FOREIGN KEY (fish_condition_id) REFERENCES fish_condition_options(id);


--
-- Name: fk_part_captures_recap_disp; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY draft_captures
    ADD CONSTRAINT fk_part_captures_recap_disp FOREIGN KEY (recapture_disposition_id) REFERENCES recapture_dispositions(id);


--
-- Name: fk_part_captures_species_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY draft_captures
    ADD CONSTRAINT fk_part_captures_species_id FOREIGN KEY (species_id) REFERENCES species(id);


--
-- Name: fk_part_captures_time_of_day; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY draft_captures
    ADD CONSTRAINT fk_part_captures_time_of_day FOREIGN KEY (time_of_day_id) REFERENCES time_of_day_options(id);


--
-- Name: fk_rails_18ffac8d12; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY draft_captures
    ADD CONSTRAINT fk_rails_18ffac8d12 FOREIGN KEY (angler_id) REFERENCES anglers(id);


--
-- Name: fk_rails_24510c6ae4; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY recaptures
    ADD CONSTRAINT fk_rails_24510c6ae4 FOREIGN KEY (capture_id) REFERENCES captures(id);


--
-- Name: fk_rails_45858b5865; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY recaptures
    ADD CONSTRAINT fk_rails_45858b5865 FOREIGN KEY (gps_format_id) REFERENCES gps_formats(id);


--
-- Name: fk_rails_64432fd973; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY captures
    ADD CONSTRAINT fk_rails_64432fd973 FOREIGN KEY (gps_format_id) REFERENCES gps_formats(id);


--
-- Name: fk_rails_782717f62d; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY recaptures
    ADD CONSTRAINT fk_rails_782717f62d FOREIGN KEY (angler_id) REFERENCES anglers(id);


--
-- Name: fk_rails_906df65cfe; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY captures
    ADD CONSTRAINT fk_rails_906df65cfe FOREIGN KEY (angler_id) REFERENCES anglers(id);


--
-- Name: fk_rails_b894ce2542; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY volunteer_time_logs
    ADD CONSTRAINT fk_rails_b894ce2542 FOREIGN KEY (angler_id) REFERENCES anglers(id);


--
-- Name: fk_rails_c2453f5ff0; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY users
    ADD CONSTRAINT fk_rails_c2453f5ff0 FOREIGN KEY (angler_id) REFERENCES anglers(id);


--
-- Name: fk_rails_e4069f0b05; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY tags
    ADD CONSTRAINT fk_rails_e4069f0b05 FOREIGN KEY (angler_id) REFERENCES anglers(id);


--
-- Name: fk_rails_e812ee6b36; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_tag_requests
    ADD CONSTRAINT fk_rails_e812ee6b36 FOREIGN KEY (angler_id) REFERENCES anglers(id);


--
-- Name: fk_recaptures_gps_type; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY recaptures
    ADD CONSTRAINT fk_recaptures_gps_type FOREIGN KEY (entered_gps_type) REFERENCES gps_formats(format_type);


--
-- Name: fk_user_devices_device_type_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_devices
    ADD CONSTRAINT fk_user_devices_device_type_id FOREIGN KEY (user_device_types_id) REFERENCES user_device_types(id) MATCH FULL;


--
-- Name: fk_user_devices_user_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_devices
    ADD CONSTRAINT fk_user_devices_user_id FOREIGN KEY (user_id) REFERENCES users(id) MATCH FULL;


--
-- Name: newsitem_user_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY news_items
    ADD CONSTRAINT newsitem_user_fk FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: notification_initiator_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY notifications
    ADD CONSTRAINT notification_initiator_fk FOREIGN KEY (initiator_id) REFERENCES users(id);


--
-- Name: notification_topic_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY notifications
    ADD CONSTRAINT notification_topic_fk FOREIGN KEY (notification_topic_id) REFERENCES notification_topics(id);


--
-- Name: notification_user_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY notifications
    ADD CONSTRAINT notification_user_fk FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: nt_topic_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY notification_topics_users
    ADD CONSTRAINT nt_topic_fk FOREIGN KEY (notification_topic_id) REFERENCES notification_topics(id);


--
-- Name: nt_user_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY notification_topics_users
    ADD CONSTRAINT nt_user_fk FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: recap_fish_cond_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY recaptures
    ADD CONSTRAINT recap_fish_cond_fk FOREIGN KEY (fish_condition_option_id) REFERENCES fish_condition_options(id);


--
-- Name: recap_recap_dis_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY recaptures
    ADD CONSTRAINT recap_recap_dis_fk FOREIGN KEY (recapture_disposition_id) REFERENCES recapture_dispositions(id);


--
-- Name: recap_species_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY recaptures
    ADD CONSTRAINT recap_species_fk FOREIGN KEY (species_id) REFERENCES species(id);


--
-- Name: recap_tag_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY recaptures
    ADD CONSTRAINT recap_tag_fk FOREIGN KEY (tag_id) REFERENCES tags(id);


--
-- Name: recap_time_day_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY recaptures
    ADD CONSTRAINT recap_time_day_fk FOREIGN KEY (time_of_day_option_id) REFERENCES time_of_day_options(id);


--
-- Name: recaptures_basin_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY recaptures
    ADD CONSTRAINT recaptures_basin_id_fk FOREIGN KEY (basin_id) REFERENCES basins(gid);


--
-- Name: recaptures_sub_basin_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY recaptures
    ADD CONSTRAINT recaptures_sub_basin_id_fk FOREIGN KEY (sub_basin_id) REFERENCES "sub-basin"(gid);


--
-- Name: ru_roles_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY roles_users
    ADD CONSTRAINT ru_roles_fk FOREIGN KEY (role_id) REFERENCES roles(id);


--
-- Name: ru_user_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY roles_users
    ADD CONSTRAINT ru_user_fk FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: scratch_capture_types_failure_scratch_capture_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY scratch_capture_types
    ADD CONSTRAINT scratch_capture_types_failure_scratch_capture_type_id_fkey FOREIGN KEY (failure_scratch_capture_type_id) REFERENCES scratch_capture_types(id);


--
-- Name: scratch_captures_angler_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY scratch_captures
    ADD CONSTRAINT scratch_captures_angler_id_fkey FOREIGN KEY (angler_id) REFERENCES anglers(id);


--
-- Name: scratch_captures_basin_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY scratch_captures
    ADD CONSTRAINT scratch_captures_basin_id_fkey FOREIGN KEY (basin_id) REFERENCES basins(gid);


--
-- Name: scratch_captures_fish_condition_option_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY scratch_captures
    ADD CONSTRAINT scratch_captures_fish_condition_option_id_fkey FOREIGN KEY (fish_condition_option_id) REFERENCES fish_condition_options(id);


--
-- Name: scratch_captures_gps_format_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY scratch_captures
    ADD CONSTRAINT scratch_captures_gps_format_id_fkey FOREIGN KEY (gps_format_id) REFERENCES gps_formats(id);


--
-- Name: scratch_captures_recapture_disposition_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY scratch_captures
    ADD CONSTRAINT scratch_captures_recapture_disposition_id_fkey FOREIGN KEY (recapture_disposition_id) REFERENCES recapture_dispositions(id);


--
-- Name: scratch_captures_recapture_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY scratch_captures
    ADD CONSTRAINT scratch_captures_recapture_id_fkey FOREIGN KEY (recapture_id) REFERENCES recaptures(id);


--
-- Name: scratch_captures_scratch_capture_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY scratch_captures
    ADD CONSTRAINT scratch_captures_scratch_capture_type_id_fkey FOREIGN KEY (scratch_capture_type_id) REFERENCES scratch_capture_types(id);


--
-- Name: scratch_captures_species_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY scratch_captures
    ADD CONSTRAINT scratch_captures_species_id_fkey FOREIGN KEY (species_id) REFERENCES species(id);


--
-- Name: scratch_captures_sub_basin_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY scratch_captures
    ADD CONSTRAINT scratch_captures_sub_basin_id_fkey FOREIGN KEY (sub_basin_id) REFERENCES "sub-basin"(gid);


--
-- Name: scratch_captures_tag_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY scratch_captures
    ADD CONSTRAINT scratch_captures_tag_id_fkey FOREIGN KEY (tag_id) REFERENCES tags(id);


--
-- Name: scratch_captures_time_of_day_option_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY scratch_captures
    ADD CONSTRAINT scratch_captures_time_of_day_option_id_fkey FOREIGN KEY (time_of_day_option_id) REFERENCES time_of_day_options(id);


--
-- Name: species_lengths_species_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY species_lengths
    ADD CONSTRAINT species_lengths_species_fk FOREIGN KEY (species_id) REFERENCES species(id);


--
-- Name: tag_lot_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY tags
    ADD CONSTRAINT tag_lot_fk FOREIGN KEY (tag_lot_id) REFERENCES tag_lots(id);


--
-- Name: tl_enduser_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY tag_lots
    ADD CONSTRAINT tl_enduser_fk FOREIGN KEY (end_user_type) REFERENCES tag_end_user_types(id);


--
-- Name: tl_man_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY tag_lots
    ADD CONSTRAINT tl_man_fk FOREIGN KEY (manufacturer_id) REFERENCES manufacturers(id);


--
-- Name: ucourier_notcour_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_couriers
    ADD CONSTRAINT ucourier_notcour_fk FOREIGN KEY (notification_courier_id) REFERENCES notification_couriers(id);


--
-- Name: ucourier_nottopic_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_couriers
    ADD CONSTRAINT ucourier_nottopic_fk FOREIGN KEY (notification_topic_id) REFERENCES notification_topics(id);


--
-- Name: ucourier_user_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_couriers
    ADD CONSTRAINT ucourier_user_fk FOREIGN KEY (user_id) REFERENCES users(id);


--
-- PostgreSQL database dump complete
--

SET search_path TO "$user",public;

INSERT INTO schema_migrations (version) VALUES ('20121011184916');

INSERT INTO schema_migrations (version) VALUES ('20121016031658');

INSERT INTO schema_migrations (version) VALUES ('20121016031730');

INSERT INTO schema_migrations (version) VALUES ('20121016031759');

INSERT INTO schema_migrations (version) VALUES ('20121016031850');

INSERT INTO schema_migrations (version) VALUES ('20121016031913');

INSERT INTO schema_migrations (version) VALUES ('20121016031943');

INSERT INTO schema_migrations (version) VALUES ('20121016032005');

INSERT INTO schema_migrations (version) VALUES ('20121016032037');

INSERT INTO schema_migrations (version) VALUES ('20121016032112');

INSERT INTO schema_migrations (version) VALUES ('20121016032200');

INSERT INTO schema_migrations (version) VALUES ('20121016032236');

INSERT INTO schema_migrations (version) VALUES ('20121016032308');

INSERT INTO schema_migrations (version) VALUES ('20121016032407');

INSERT INTO schema_migrations (version) VALUES ('20121016032502');

INSERT INTO schema_migrations (version) VALUES ('20121016032528');

INSERT INTO schema_migrations (version) VALUES ('20121016032549');

INSERT INTO schema_migrations (version) VALUES ('20121016032600');

INSERT INTO schema_migrations (version) VALUES ('20121016032644');

INSERT INTO schema_migrations (version) VALUES ('20121016032731');

INSERT INTO schema_migrations (version) VALUES ('20121016032750');

INSERT INTO schema_migrations (version) VALUES ('20121016033148');

INSERT INTO schema_migrations (version) VALUES ('20121016033324');

INSERT INTO schema_migrations (version) VALUES ('20121016033429');

INSERT INTO schema_migrations (version) VALUES ('20121016033520');

INSERT INTO schema_migrations (version) VALUES ('20121016034142');

INSERT INTO schema_migrations (version) VALUES ('20121016034240');

INSERT INTO schema_migrations (version) VALUES ('20121019004845');

INSERT INTO schema_migrations (version) VALUES ('20121025154215');

INSERT INTO schema_migrations (version) VALUES ('20121031213903');

INSERT INTO schema_migrations (version) VALUES ('20121115022641');

INSERT INTO schema_migrations (version) VALUES ('20121120154815');

INSERT INTO schema_migrations (version) VALUES ('20121218193102');

INSERT INTO schema_migrations (version) VALUES ('20121218195249');

INSERT INTO schema_migrations (version) VALUES ('20121218201319');

INSERT INTO schema_migrations (version) VALUES ('20121218204318');

INSERT INTO schema_migrations (version) VALUES ('20130107171735');

INSERT INTO schema_migrations (version) VALUES ('20130107172435');

INSERT INTO schema_migrations (version) VALUES ('20130119081736');

INSERT INTO schema_migrations (version) VALUES ('20130119081737');

INSERT INTO schema_migrations (version) VALUES ('20130119081738');

INSERT INTO schema_migrations (version) VALUES ('20130119081739');

INSERT INTO schema_migrations (version) VALUES ('20130119081740');

INSERT INTO schema_migrations (version) VALUES ('20130119081741');

INSERT INTO schema_migrations (version) VALUES ('20130119081742');

INSERT INTO schema_migrations (version) VALUES ('20130119081743');

INSERT INTO schema_migrations (version) VALUES ('20130122032311');

INSERT INTO schema_migrations (version) VALUES ('20130123040841');

INSERT INTO schema_migrations (version) VALUES ('20130130150800');

INSERT INTO schema_migrations (version) VALUES ('20130130195049');

INSERT INTO schema_migrations (version) VALUES ('20130202212614');

INSERT INTO schema_migrations (version) VALUES ('20130204035813');

INSERT INTO schema_migrations (version) VALUES ('20130206202857');

INSERT INTO schema_migrations (version) VALUES ('20130207200143');

INSERT INTO schema_migrations (version) VALUES ('20130207202604');

INSERT INTO schema_migrations (version) VALUES ('20130207221654');

INSERT INTO schema_migrations (version) VALUES ('20130207222612');

INSERT INTO schema_migrations (version) VALUES ('20130208020408');

INSERT INTO schema_migrations (version) VALUES ('20130220195605');

INSERT INTO schema_migrations (version) VALUES ('20130227152510');

INSERT INTO schema_migrations (version) VALUES ('20130304171755');

INSERT INTO schema_migrations (version) VALUES ('20130304182034');

INSERT INTO schema_migrations (version) VALUES ('20130305225054');

INSERT INTO schema_migrations (version) VALUES ('20130307161848');

INSERT INTO schema_migrations (version) VALUES ('20130310065309');

INSERT INTO schema_migrations (version) VALUES ('20130311211442');

INSERT INTO schema_migrations (version) VALUES ('20130315214641');

INSERT INTO schema_migrations (version) VALUES ('20130401152921');

INSERT INTO schema_migrations (version) VALUES ('20130517003138');

INSERT INTO schema_migrations (version) VALUES ('20130820184024');

INSERT INTO schema_migrations (version) VALUES ('20130821154819');

INSERT INTO schema_migrations (version) VALUES ('20130821163633');

INSERT INTO schema_migrations (version) VALUES ('20130822164339');

INSERT INTO schema_migrations (version) VALUES ('20130822184552');

INSERT INTO schema_migrations (version) VALUES ('20130829193027');

INSERT INTO schema_migrations (version) VALUES ('20130909205522');

INSERT INTO schema_migrations (version) VALUES ('20130909210940');

INSERT INTO schema_migrations (version) VALUES ('20130913165907');

INSERT INTO schema_migrations (version) VALUES ('20130917183145');

INSERT INTO schema_migrations (version) VALUES ('20130917185820');

INSERT INTO schema_migrations (version) VALUES ('20130925154519');

INSERT INTO schema_migrations (version) VALUES ('20131002162120');

INSERT INTO schema_migrations (version) VALUES ('20131004182709');

INSERT INTO schema_migrations (version) VALUES ('20131011151427');

INSERT INTO schema_migrations (version) VALUES ('20131025155723');

INSERT INTO schema_migrations (version) VALUES ('20131205200144');

INSERT INTO schema_migrations (version) VALUES ('20131218195450');

INSERT INTO schema_migrations (version) VALUES ('20140113171859');

INSERT INTO schema_migrations (version) VALUES ('20140204195548');

INSERT INTO schema_migrations (version) VALUES ('20140204201659');

INSERT INTO schema_migrations (version) VALUES ('20140207180251');

INSERT INTO schema_migrations (version) VALUES ('20140207184751');

INSERT INTO schema_migrations (version) VALUES ('20140218200956');

INSERT INTO schema_migrations (version) VALUES ('20140218201042');

INSERT INTO schema_migrations (version) VALUES ('20140218201425');

INSERT INTO schema_migrations (version) VALUES ('20140221202504');

INSERT INTO schema_migrations (version) VALUES ('20140224150603');

INSERT INTO schema_migrations (version) VALUES ('20140224155306');

INSERT INTO schema_migrations (version) VALUES ('20140224173737');

INSERT INTO schema_migrations (version) VALUES ('20140227220614');

INSERT INTO schema_migrations (version) VALUES ('20140227221551');

INSERT INTO schema_migrations (version) VALUES ('20140305182615');

INSERT INTO schema_migrations (version) VALUES ('20140305204804');

INSERT INTO schema_migrations (version) VALUES ('20140318154247');

INSERT INTO schema_migrations (version) VALUES ('20140318162932');

INSERT INTO schema_migrations (version) VALUES ('20140827174002');

INSERT INTO schema_migrations (version) VALUES ('20140905184955');

INSERT INTO schema_migrations (version) VALUES ('20140905185220');

INSERT INTO schema_migrations (version) VALUES ('20140908195534');

INSERT INTO schema_migrations (version) VALUES ('20140910192244');

INSERT INTO schema_migrations (version) VALUES ('20141003201732');

INSERT INTO schema_migrations (version) VALUES ('20141007184719');

INSERT INTO schema_migrations (version) VALUES ('20141007194315');

INSERT INTO schema_migrations (version) VALUES ('20141029200043');

INSERT INTO schema_migrations (version) VALUES ('20141030202241');

INSERT INTO schema_migrations (version) VALUES ('20150108214624');

INSERT INTO schema_migrations (version) VALUES ('20150109211345');

INSERT INTO schema_migrations (version) VALUES ('20150127201627');

INSERT INTO schema_migrations (version) VALUES ('20150211215558');

INSERT INTO schema_migrations (version) VALUES ('20150218193303');

INSERT INTO schema_migrations (version) VALUES ('20150227193027');

INSERT INTO schema_migrations (version) VALUES ('20150304172953');

INSERT INTO schema_migrations (version) VALUES ('20150304214856');

INSERT INTO schema_migrations (version) VALUES ('20150304223059');

INSERT INTO schema_migrations (version) VALUES ('20150306163237');

INSERT INTO schema_migrations (version) VALUES ('20150306171444');

INSERT INTO schema_migrations (version) VALUES ('20150306174342');

INSERT INTO schema_migrations (version) VALUES ('20150331204404');

INSERT INTO schema_migrations (version) VALUES ('20150408144743');

INSERT INTO schema_migrations (version) VALUES ('20150415201446');

INSERT INTO schema_migrations (version) VALUES ('20150521202255');

INSERT INTO schema_migrations (version) VALUES ('20150714162146');

INSERT INTO schema_migrations (version) VALUES ('20150715205450');

INSERT INTO schema_migrations (version) VALUES ('20150715215220');

INSERT INTO schema_migrations (version) VALUES ('20150716173128');

INSERT INTO schema_migrations (version) VALUES ('20150717162955');

INSERT INTO schema_migrations (version) VALUES ('20150720172206');

INSERT INTO schema_migrations (version) VALUES ('20150720174253');

INSERT INTO schema_migrations (version) VALUES ('20150805195852');

INSERT INTO schema_migrations (version) VALUES ('20150806210123');

INSERT INTO schema_migrations (version) VALUES ('20150814204724');

INSERT INTO schema_migrations (version) VALUES ('20150817164549');

INSERT INTO schema_migrations (version) VALUES ('20150820163443');

INSERT INTO schema_migrations (version) VALUES ('20150820164113');

INSERT INTO schema_migrations (version) VALUES ('20150820212602');

INSERT INTO schema_migrations (version) VALUES ('20150917203027');

INSERT INTO schema_migrations (version) VALUES ('20150928203659');

INSERT INTO schema_migrations (version) VALUES ('20151001195749');

INSERT INTO schema_migrations (version) VALUES ('20151001204655');

INSERT INTO schema_migrations (version) VALUES ('20151001214403');

INSERT INTO schema_migrations (version) VALUES ('20151005192155');

INSERT INTO schema_migrations (version) VALUES ('20151005211115');

INSERT INTO schema_migrations (version) VALUES ('20151008202002');

INSERT INTO schema_migrations (version) VALUES ('20151014162425');

INSERT INTO schema_migrations (version) VALUES ('20151026210053');

INSERT INTO schema_migrations (version) VALUES ('20151030170111');

INSERT INTO schema_migrations (version) VALUES ('20151104193742');

INSERT INTO schema_migrations (version) VALUES ('20151109202236');

INSERT INTO schema_migrations (version) VALUES ('20151117151225');

INSERT INTO schema_migrations (version) VALUES ('20151117164434');

INSERT INTO schema_migrations (version) VALUES ('20151117222959');

INSERT INTO schema_migrations (version) VALUES ('20151118215902');

INSERT INTO schema_migrations (version) VALUES ('20151120212208');

INSERT INTO schema_migrations (version) VALUES ('20151120212210');

INSERT INTO schema_migrations (version) VALUES ('20151125185644');

INSERT INTO schema_migrations (version) VALUES ('20151207160422');

INSERT INTO schema_migrations (version) VALUES ('20151209183047');

INSERT INTO schema_migrations (version) VALUES ('20151218153201');

INSERT INTO schema_migrations (version) VALUES ('20151218153417');

INSERT INTO schema_migrations (version) VALUES ('20151218160035');

INSERT INTO schema_migrations (version) VALUES ('20151218160107');

INSERT INTO schema_migrations (version) VALUES ('20160106205724');

INSERT INTO schema_migrations (version) VALUES ('20160106210014');

INSERT INTO schema_migrations (version) VALUES ('20160106210158');

INSERT INTO schema_migrations (version) VALUES ('20160106210219');

INSERT INTO schema_migrations (version) VALUES ('20160106211900');

INSERT INTO schema_migrations (version) VALUES ('20160106211908');

INSERT INTO schema_migrations (version) VALUES ('20160106211934');

INSERT INTO schema_migrations (version) VALUES ('20160106211952');

INSERT INTO schema_migrations (version) VALUES ('20160106214300');

INSERT INTO schema_migrations (version) VALUES ('20160106214302');

INSERT INTO schema_migrations (version) VALUES ('20160106214616');

INSERT INTO schema_migrations (version) VALUES ('20160106214641');

INSERT INTO schema_migrations (version) VALUES ('20160106221344');

INSERT INTO schema_migrations (version) VALUES ('20160106222147');

INSERT INTO schema_migrations (version) VALUES ('20160106222218');

INSERT INTO schema_migrations (version) VALUES ('20160106222638');

INSERT INTO schema_migrations (version) VALUES ('20160106222723');

INSERT INTO schema_migrations (version) VALUES ('20160108164042');

INSERT INTO schema_migrations (version) VALUES ('20160108165924');

INSERT INTO schema_migrations (version) VALUES ('20160108182622');

INSERT INTO schema_migrations (version) VALUES ('20160108182638');

INSERT INTO schema_migrations (version) VALUES ('20160108182652');

INSERT INTO schema_migrations (version) VALUES ('20160108182659');

INSERT INTO schema_migrations (version) VALUES ('20160108182713');

INSERT INTO schema_migrations (version) VALUES ('20160108191743');

INSERT INTO schema_migrations (version) VALUES ('20160108193531');

INSERT INTO schema_migrations (version) VALUES ('20160108193857');

INSERT INTO schema_migrations (version) VALUES ('20160108194224');

INSERT INTO schema_migrations (version) VALUES ('20160111183154');

INSERT INTO schema_migrations (version) VALUES ('20160111184049');

INSERT INTO schema_migrations (version) VALUES ('20160111190305');

INSERT INTO schema_migrations (version) VALUES ('20160111190321');

INSERT INTO schema_migrations (version) VALUES ('20160111190453');

INSERT INTO schema_migrations (version) VALUES ('20160111190502');

INSERT INTO schema_migrations (version) VALUES ('20160111190519');

INSERT INTO schema_migrations (version) VALUES ('20160111190520');

INSERT INTO schema_migrations (version) VALUES ('20160111200257');

INSERT INTO schema_migrations (version) VALUES ('20160111213004');

INSERT INTO schema_migrations (version) VALUES ('20160113172026');

INSERT INTO schema_migrations (version) VALUES ('20160113173007');

INSERT INTO schema_migrations (version) VALUES ('20160113173019');

INSERT INTO schema_migrations (version) VALUES ('20160113173030');

INSERT INTO schema_migrations (version) VALUES ('20160113173041');

INSERT INTO schema_migrations (version) VALUES ('20160113173050');

INSERT INTO schema_migrations (version) VALUES ('20160113173100');

INSERT INTO schema_migrations (version) VALUES ('20160113173130');

INSERT INTO schema_migrations (version) VALUES ('20160113173143');

INSERT INTO schema_migrations (version) VALUES ('20160113173154');

INSERT INTO schema_migrations (version) VALUES ('20160113173211');

INSERT INTO schema_migrations (version) VALUES ('20160113173222');

INSERT INTO schema_migrations (version) VALUES ('20160113173233');

INSERT INTO schema_migrations (version) VALUES ('20160113180221');

INSERT INTO schema_migrations (version) VALUES ('20160114184124');

INSERT INTO schema_migrations (version) VALUES ('20160114210831');

INSERT INTO schema_migrations (version) VALUES ('20160120184131');

INSERT INTO schema_migrations (version) VALUES ('20160121211238');

INSERT INTO schema_migrations (version) VALUES ('20160122193507');

INSERT INTO schema_migrations (version) VALUES ('20160125203359');

INSERT INTO schema_migrations (version) VALUES ('20160211211542');

INSERT INTO schema_migrations (version) VALUES ('20160304192122');

INSERT INTO schema_migrations (version) VALUES ('20160307153539');

INSERT INTO schema_migrations (version) VALUES ('20160405165101');

INSERT INTO schema_migrations (version) VALUES ('20160426211504');

INSERT INTO schema_migrations (version) VALUES ('20160801195545');

INSERT INTO schema_migrations (version) VALUES ('20170719163710');

INSERT INTO schema_migrations (version) VALUES ('20170724200101');

INSERT INTO schema_migrations (version) VALUES ('20170814202907');

INSERT INTO schema_migrations (version) VALUES ('20171024205347');

INSERT INTO schema_migrations (version) VALUES ('20180301154137');

INSERT INTO schema_migrations (version) VALUES ('20180621200813');


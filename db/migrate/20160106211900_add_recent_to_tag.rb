class AddRecentToTag < ActiveRecord::Migration
  def change
    add_column :tags, :recent, :boolean, null: false, default: true

    execute 'update tags set recent = false'
    execute <<-SQL
      update tags
      set recent = true
      where
        (angler_id IS NULL AND tags.updated_at > (now() - '60 days'::interval))
        OR tag_lot_id IN (SELECT id from tag_lots order by created_at DESC limit 3);
    SQL

    add_index :tags, :recent
  end
end

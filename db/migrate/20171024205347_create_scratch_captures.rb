class CreateScratchCaptures < ActiveRecord::Migration
  def up
    (query = '') << <<-SQL
      CREATE TABLE scratch_capture_types ( 
        id serial PRIMARY KEY,
        name text NOT NULL,
        displayable boolean NOT NULL,  
        failure_scratch_capture_type_id bigint references scratch_capture_types(id),
        created_at timestamp with time zone NOT NULL,
        updated_at timestamp with time zone NOT NULL
      );    

      CREATE TABLE scratch_captures (
        id serial PRIMARY KEY,
        capture_date timestamp with time zone,
        length numeric, 
        location_description text,
        latitude double precision,
        longitude double precision,
        comments text,  
        tag_id integer references tags(id),
        tag_number text,
        species_id integer references species(id),
        fish_condition_option_id integer references fish_condition_options(id),
        recapture_disposition_id integer references recapture_dispositions(id),
        time_of_day_option_id integer references time_of_day_options(id), 
        created_at timestamp with time zone NOT NULL,
        updated_at timestamp with time zone NOT NULL,
        geom geometry(Point,4326),
        basin_id integer references basins(gid),
        sub_basin_id integer references "sub-basin"(gid),
        gps_format_id bigint references gps_formats(id),  
        angler_name text,
        angler_number text,
        angler_id bigint references anglers(id),  
        scratch_capture_type_id integer references scratch_capture_types(id) NOT NULL,
        recapture_id integer references recaptures(id)  
      );  

    SQL

    ActiveRecord::Base.connection.execute(query)
    ScratchCaptureType.create!(name: 'Pending', displayable: true)
    fm = ScratchCaptureType.create!(name: 'Failed Manual Conversion', displayable: true)
    fa = ScratchCaptureType.create!(name: 'Failed Automatic Conversion', displayable: false)
    ScratchCaptureType.create!(name: 'Successful Automatic Conversion', displayable: false, failure_scratch_capture_type: fa)
    ScratchCaptureType.create!(name: 'Successful Manual Conversion', displayable: false, failure_scratch_capture_type: fm)

    #for angler we're opting to create a place holder so we don't have to worry about
    #bad data getting in and having a bunch of code that expect this to be here to break
    uk = Angler.create!(angler_id: 'PLC00000', first_name: 'Unknown', last_name: 'Scratch Angler')


    change_column_null :captures, :tag_id, true
    change_column_null :recaptures, :tag_id, true
  end

  def down
    drop_table :scratch_captures
    drop_table :scratch_capture_types
    Angler.where(angler_id: 'PLC00000', first_name: 'Unknown', last_name: 'Scratch Angler').first.destroy
    change_column_null :captures, :tag_id, false
    change_column_null :recaptures, :tag_id, false
  end
end

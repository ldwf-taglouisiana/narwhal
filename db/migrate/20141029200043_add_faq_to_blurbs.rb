class AddFaqToBlurbs < ActiveRecord::Migration
  def change
    add_column :blurbs, :faq, :text
  end
end

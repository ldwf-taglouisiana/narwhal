class UpdateArchivedVolunteerHoursToVersion3 < ActiveRecord::Migration
  def change
    create_view :archived_volunteer_hours,
                version: 3,
                materialized: true

    add_index :archived_volunteer_hours, :id, unique: true
    add_index :archived_volunteer_hours, :angler_id
    add_index :archived_volunteer_hours, :recent
  end
end

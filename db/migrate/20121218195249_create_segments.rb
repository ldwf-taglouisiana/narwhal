class CreateSegments < ActiveRecord::Migration
  def up
    create_table :segments do |t|
      t.integer :gid
      t.decimal :objectid
      t.decimal :area
      t.float :perimeter
      t.float :subseg02
      t.float :subseg02id
      t.string :basin
      t.string :subbasin
      t.string :subbasin_n
      t.string :sgmnt_name
      t.string :segment
      t.geometry :the_geom
    end

  end

  def down
  end
end

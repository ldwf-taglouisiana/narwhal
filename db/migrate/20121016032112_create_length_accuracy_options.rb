class CreateLengthAccuracyOptions < ActiveRecord::Migration
  def change
    create_table :length_accuracy_options do |t|
      t.string :length_accuracy_option

      t.timestamps
    end
  end
end

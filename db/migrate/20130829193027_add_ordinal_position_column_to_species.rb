class AddOrdinalPositionColumnToSpecies < ActiveRecord::Migration
  def change
   add_column :species, :ordinal_position, :integer
  end
end

class UpdateArchivedTagsToVersion3 < ActiveRecord::Migration
  def up
    # drop the existing view that has the union of the 2 partial views
    # tag_assignmetns depends on presentable_tags, so need to drop it first.
    drop_view :tag_assignments, revert_to_version: 2
    drop_view :presentable_tags, revert_to_version: 2

    # materialized views need to be dropped then recreated
    drop_view :archived_tags, revert_to_version: 2, materialized: true
    create_view :archived_tags, version: 3, materialized: true

    # add the indices back to the archived tags
    add_index :archived_tags, :id, unique: true
    add_index :archived_tags, :tag_no
    add_index :archived_tags, :angler_id
    add_index :archived_tags, :recent
    add_index :archived_tags, :seq_id
    add_index :archived_tags, [:id, :used]
    add_index :archived_tags, [:used_at]

    # updated teh recent_tags to the newest version
    update_view :recent_tags, version: 3, revert_to_version: 2

    # recreate the presentable_tags and tag_assignments
    create_view :presentable_tags, version: 2
    create_view :tag_assignments, version: 2
  end

  def down
    # drop the existing view that has the union of the 2 partial views
    # tag_assignmetns depends on presentable_tags, so need to drop it first.
    drop_view :tag_assignments, revert_to_version: 2
    drop_view :presentable_tags, revert_to_version: 2

    # materialized views need to be dropped then recreated
    drop_view :archived_tags, revert_to_version: 3, materialized: true
    create_view :archived_tags, version: 2, materialized: true

    # add the indices back to the archived tags
    add_index :archived_tags, :id, unique: true
    add_index :archived_tags, :tag_no
    add_index :archived_tags, :angler_id
    add_index :archived_tags, :recent
    add_index :archived_tags, :seq_id
    add_index :archived_tags, [:id, :used]

    # updated teh recent_tags to the newest version
    update_view :recent_tags, version: 2, revert_to_version: 3

    # recreate the presentable_tags and tag_assignments
    create_view :presentable_tags, version: 2
    create_view :tag_assignments, version: 2
  end
end

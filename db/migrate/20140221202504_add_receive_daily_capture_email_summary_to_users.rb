class AddReceiveDailyCaptureEmailSummaryToUsers < ActiveRecord::Migration
  def change
    add_column :users, :receive_daily_capture_email_summary, :boolean
  end
end

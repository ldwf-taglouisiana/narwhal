class UpdateTagsTableForAssignmentPurposes < ActiveRecord::Migration
  def self.up
    add_column :tags, :prefix, :string

    execute <<-SQL
      create index tags_prefix_idx on tags (prefix);
      create index tags_assignment_sort_idx on tags (angler_id, prefix, updated_at);
      update tags set prefix = substring(tag_no, '^[A-Z]+');
    SQL
  end

  def self.down
    execute <<-SQL
      DROP INDEX IF EXISTS tags_prefix_idx;
      DROP INDEX IF EXISTS tags_assignment_sort_idx;
    SQL

    remove_column :tags, :prefix
  end
end

class AddUserCaptureColumnToCapture < ActiveRecord::Migration
  def change
    add_column :captures, :user_capture, :boolean
  end
end

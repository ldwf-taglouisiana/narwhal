class CreateReferrals < ActiveRecord::Migration
  def change
    create_table :referrals do |t|
      t.integer :referred_by
      t.string :last_name
      t.string :first_name
      t.string :street
      t.string :suite
      t.string :city
      t.integer :state
      t.string :zip
      t.string :phone
      t.integer :phone_type
      t.string :email

      t.timestamps
    end
  end
end

class AddSavedAtToDraftCapture < ActiveRecord::Migration
  def change
    add_column :draft_captures, :saved_at, :datetime
  end
end

class AddIndexesToVersion < ActiveRecord::Migration
  def change
    add_index :versions, [:whodunnit, :created_at, :item_type]
  end
end

class CreateDraftCapturesTable < ActiveRecord::Migration
  def self.up
   create_table :draft_captures do |t|
     t.string   :angler_id, :null => false
     t.string   :tag_number, :null => false
     t.datetime :capture_date, :null => false
     t.integer  :species_id
     t.float    :length          
     t.text     :location_description
     t.float    :latitude
     t.float    :longitude
     t.integer  :fish_condition_id
     t.float    :weight          
     t.text     :comments        
     t.boolean  :recapture       
     t.integer  :recapture_disposition
     t.integer  :time_of_day_id
     t.string   :entered_gps_type
     t.string   :error_json

     t.timestamps
   end

    execute <<-SQL
      ALTER TABLE draft_captures
        ADD CONSTRAINT fk_part_captures_angler_id
        FOREIGN KEY (angler_id)
        REFERENCES anglers(angler_id)
    SQL

    execute <<-SQL
      ALTER TABLE draft_captures
        ADD CONSTRAINT fk_part_captures_species_id
        FOREIGN KEY (species_id)
        REFERENCES species(id)
    SQL

    execute <<-SQL
      ALTER TABLE draft_captures
        ADD CONSTRAINT fk_part_captures_fish_cond
        FOREIGN KEY (fish_condition_id)
        REFERENCES fish_condition_options(id)
    SQL

    execute <<-SQL
      ALTER TABLE draft_captures
        ADD CONSTRAINT fk_part_captures_recap_disp
        FOREIGN KEY (recapture_disposition)
        REFERENCES recapture_dispositions(id)
    SQL

    execute <<-SQL
      ALTER TABLE draft_captures
        ADD CONSTRAINT fk_part_captures_time_of_day
        FOREIGN KEY (time_of_day_id)
        REFERENCES time_of_day_options(id)
    SQL

    add_index :draft_captures, :angler_id

  end

  def self.down

    execute <<-SQL
      ALTER TABLE draft_captures
        DROP CONSTRAINT  fk_part_captures_angler_id
    SQL

    execute <<-SQL
      ALTER TABLE draft_captures
        DROP CONSTRAINT  fk_part_captures_species_id
    SQL

    execute <<-SQL
      ALTER TABLE draft_captures
        DROP CONSTRAINT  fk_part_captures_fish_cond
    SQL

    execute <<-SQL
      ALTER TABLE draft_captures
        DROP CONSTRAINT  fk_part_captures_recap_disp
    SQL

    execute <<-SQL
      ALTER TABLE draft_captures
        DROP CONSTRAINT  fk_part_captures_time_of_day
    SQL

    remove_index :draft_captures, :angler_id

    drop_table :draft_captures

  end
end

class UpdateRecentTagsToVersion2 < ActiveRecord::Migration
  def up
    create_view :recent_tags, version: 2
  end

  def down
    drop_view :recent_tags
  end
end

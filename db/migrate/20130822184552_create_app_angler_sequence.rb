class CreateAppAnglerSequence < ActiveRecord::Migration
  def up
    execute <<-SQL
      CREATE SEQUENCE app_angler_sequence;
      select setval('app_angler_sequence', (
        select max(regexp_replace(angler_id, '^[A-Z]+', '')::integer)
        from anglers
        where angler_id ilike '%APP%') + 1
      );
    SQL
  end

  def down
    execute <<-SQL
      DROP SEQUENCE app_angler_sequence;
    SQL
  end
end

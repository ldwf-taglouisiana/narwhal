class CreateFishTaggedOptions < ActiveRecord::Migration
  def change
    create_table :fish_tagged_options do |t|
      t.string :fish_tagged_option

      t.timestamps
    end
  end
end

class AddNeededConstraintsToDatabase < ActiveRecord::Migration
  def change

    # move the duplicate captures to a temp table to go through later
    execute <<-SQL
      CREATE TABLE capture_duplicates(
        id integer NOT NULL,
        angler_id character varying(20),
        tag_id integer,
        capture_date timestamp,
        species_id integer,
        length double precision,
        location_description text,
        latitude double precision,
        longitude double precision,
        fish_condition_option_id integer,
        weight double precision,
        comments text,
        confirmed boolean DEFAULT false,
        verified boolean DEFAULT false,
        deleted boolean,
        mailed_map boolean,
        recapture boolean DEFAULT false,
        recapture_disposition_id integer,
        time_of_day_option_id integer,
        created_at timestamp NOT NULL,
        updated_at timestamp NOT NULL,
        geom geometry,
        entered_gps_type character varying(255),
        user_capture boolean,
        CONSTRAINT captures_duplicates_pkey PRIMARY KEY (id)
      );

      insert into capture_duplicates (
        select * from captures where id in (
          select id from (
            SELECT captures.id, capture_date, tag_id, tag_no, recapture, ROW_NUMBER() OVER( PARTITION BY tag_id ) AS row
            FROM captures left join tags on captures.tag_id = tags.id
            order by tag_id
          ) dups
          where row > 1
          order by tag_id
        )
      );

      delete from captures where id in (
        select id from (
          SELECT captures.id, capture_date, tag_id, tag_no, recapture, ROW_NUMBER() OVER( PARTITION BY tag_id ) AS row
          FROM captures left join tags on captures.tag_id = tags.id
          order by tag_id
        ) dups
        where row > 1
        order by tag_id
      );
    SQL

    #  clean up some old data. Some items have to be deleted since the referenced item was deleted
    execute <<-SQL
      delete from activity_logs where activity_item_id is null;
      delete from activity_logs where id in ( select activity_logs.id from activity_logs left join users on activity_logs.user_id = users.id where users.first_name is null);
      delete from user_couriers where notification_courier_id is null;
      delete from user_couriers where notification_topic_id  is null;
      delete from user_tag_requests where angler_id = '' OR angler_id is null;
      delete from user_tag_requests where id in (select user_tag_requests.id from user_tag_requests left join anglers on user_tag_requests.angler_id = anglers.angler_id where anglers.id is null);
      delete from volunteer_time_logs where id in (select volunteer_time_logs.id from volunteer_time_logs left join anglers on volunteer_time_logs.angler_id = anglers.angler_id where anglers.id is null);

      update tag_lots set manufacturer_id = null where manufacturer_id = 0;
      update tags set angler_id = upper(angler_id);
      update tags set angler_id = null where angler_id = '';
      update users set angler_id = null where angler_id = '';
      update recaptures set angler_id = upper(angler_id);
    SQL

    execute <<-SQL

      -- about_tagging_program table
      ALTER TABLE about_tagging_programs
      ALTER COLUMN description SET NOT NULL;

      -- activity_log_types table
      ALTER TABLE activity_log_types
      ALTER COLUMN activity_type SET NOT NULL;

      -- activity_logs table
      ALTER TABLE activity_logs
      ALTER COLUMN activity_log_type_id SET NOT NULL;

      ALTER TABLE activity_logs
      ALTER COLUMN activity_item_id SET NOT NULL;

      ALTER TABLE activity_logs
      ALTER COLUMN activity_item_type SET NOT NULL;

      ALTER TABLE activity_logs
      ADD CONSTRAINT alog_type_fk
      FOREIGN KEY (activity_log_type_id)
      REFERENCES activity_log_types (id);

      ALTER TABLE activity_logs
      ADD CONSTRAINT alog_user_fk
      FOREIGN KEY (user_id)
      REFERENCES users (id);

      -- anglers table
      ALTER TABLE anglers
      ADD CONSTRAINT ang_shirt_fk
      FOREIGN KEY (shirt_size)
      REFERENCES shirt_size_options (id);

      ALTER TABLE anglers
      ADD CONSTRAINT ang_tag_fk
      FOREIGN KEY (tag_end_user_type)
      REFERENCES tag_end_user_types (id);

      -- captures table
      ALTER TABLE captures ALTER COLUMN user_capture SET DEFAULT FALSE;
      ALTER TABLE captures ALTER COLUMN verified SET DEFAULT FALSE;

      ALTER TABLE captures
      ALTER COLUMN capture_date SET NOT NULL;

      ALTER TABLE captures
      ADD CONSTRAINT cap_tag_fk
      UNIQUE (tag_id);

      ALTER TABLE captures
      ADD CONSTRAINT cap_angler_fk
      FOREIGN KEY (angler_id)
      REFERENCES anglers (angler_id);

      ALTER TABLE captures
      ADD CONSTRAINT cap_species_fk
      FOREIGN KEY (species_id)
      REFERENCES species (id);

      ALTER TABLE captures
      ADD CONSTRAINT cap_fishcond_fk
      FOREIGN KEY (fish_condition_option_id)
      REFERENCES fish_condition_options (id);

      ALTER TABLE captures
      ADD CONSTRAINT cap_time_fk
      FOREIGN KEY (time_of_day_option_id)
      REFERENCES time_of_day_options (id);

      CREATE UNIQUE INDEX captures_tag_idx ON captures (tag_id);

      -- notification_couriers
      ALTER TABLE notification_couriers
      ALTER COLUMN courier SET NOT NULL;

      -- notifications_topics
      ALTER TABLE notification_topics
      ALTER COLUMN topic SET NOT NULL;

      -- notifications_topics_users table
      ALTER TABLE notification_topics_users
      ADD CONSTRAINT nt_topic_fk
      FOREIGN KEY (notification_topic_id)
      REFERENCES notification_topics (id);

      ALTER TABLE notification_topics_users
      ADD CONSTRAINT nt_user_fk
      FOREIGN KEY (user_id)
      REFERENCES users (id);

      -- notifications
      ALTER TABLE notifications ALTER COLUMN checked SET DEFAULT FALSE;

      ALTER TABLE notifications
      ALTER COLUMN user_id SET NOT NULL;

      ALTER TABLE notifications
      ALTER COLUMN notification_item_id SET NOT NULL;

      ALTER TABLE notifications
      ALTER COLUMN notification_item_type SET NOT NULL;

      ALTER TABLE notifications
      ALTER COLUMN notification_topic_id SET NOT NULL;

      ALTER TABLE notifications
      ADD CONSTRAINT notification_initiator_fk
      FOREIGN KEY (initiator_id)
      REFERENCES users (id);

      ALTER TABLE notifications
      ADD CONSTRAINT notification_user_fk
      FOREIGN KEY (user_id)
      REFERENCES users (id);

      ALTER TABLE notifications
      ADD CONSTRAINT notification_topic_fk
      FOREIGN KEY (notification_topic_id)
      REFERENCES notification_topics (id);

      -- roles_users
      ALTER TABLE roles_users
      ADD CONSTRAINT ru_roles_fk
      FOREIGN KEY (role_id)
      REFERENCES roles (id);

      ALTER TABLE roles_users
      ADD CONSTRAINT ru_user_fk
      FOREIGN KEY (user_id)
      REFERENCES users (id);

      CREATE UNIQUE INDEX ru_idx ON roles_users (role_id, user_id);

      -- tag_lots table
      ALTER TABLE tag_lots
      ADD CONSTRAINT tl_man_fk
      FOREIGN KEY (manufacturer_id)
      REFERENCES manufacturers (id);

      ALTER TABLE tag_lots
      ADD CONSTRAINT tl_enduser_fk
      FOREIGN KEY (end_user_type)
      REFERENCES tag_end_user_types (id);

      -- tags table
      ALTER TABLE tags
      ADD CONSTRAINT tag_angler_fk
      FOREIGN KEY (angler_id)
      REFERENCES anglers (angler_id);

      ALTER TABLE tags
      ADD CONSTRAINT tag_lot_fk
      FOREIGN KEY (tag_lot_id)
      REFERENCES tag_lots (id);

      -- user_couriers
      ALTER TABLE user_couriers
      ALTER COLUMN notification_courier_id SET NOT NULL;

      ALTER TABLE user_couriers
      ALTER COLUMN notification_topic_id SET NOT NULL;

      ALTER TABLE user_couriers
      ALTER COLUMN user_id SET NOT NULL;

      ALTER TABLE user_couriers ALTER COLUMN daily_digest SET DEFAULT FALSE;

      ALTER TABLE user_couriers
      ADD CONSTRAINT ucourier_user_fk
      FOREIGN KEY (user_id)
      REFERENCES users (id);

      ALTER TABLE user_couriers
      ADD CONSTRAINT ucourier_notcour_fk
      FOREIGN KEY (notification_courier_id)
      REFERENCES notification_couriers (id);

      ALTER TABLE user_couriers
      ADD CONSTRAINT ucourier_nottopic_fk
      FOREIGN KEY (notification_topic_id)
      REFERENCES notification_topics (id);

      -- user_tag_requests
      ALTER TABLE user_tag_requests
      ALTER COLUMN angler_id SET NOT NULL;

      ALTER TABLE user_tag_requests
      ALTER COLUMN number_of_tags SET NOT NULL;

      ALTER TABLE user_tag_requests
      ALTER COLUMN tag_type SET NOT NULL;

      ALTER TABLE user_tag_requests ALTER COLUMN fulfilled SET DEFAULT FALSE;

      ALTER TABLE user_tag_requests
      ADD CONSTRAINT utagreq_angler_fk
      FOREIGN KEY (angler_id)
      REFERENCES anglers (angler_id);

      -- users table
      ALTER TABLE users
      ALTER COLUMN receive_daily_capture_email_summary SET DEFAULT FALSE;

      ALTER TABLE users
      ALTER COLUMN first_name SET NOT NULL;

      ALTER TABLE users
      ALTER COLUMN last_name SET NOT NULL;

      ALTER TABLE users
      ADD CONSTRAINT user_angler_fk
      FOREIGN KEY (angler_id)
      REFERENCES anglers (angler_id);

      -- volunteer_time_logs table
      ALTER TABLE volunteer_time_logs
      ALTER COLUMN start SET NOT NULL;

      ALTER TABLE volunteer_time_logs
      ALTER COLUMN "end" SET NOT NULL;

      ALTER TABLE volunteer_time_logs
      ALTER COLUMN verified SET DEFAULT FALSE;

      ALTER TABLE volunteer_time_logs
      ADD CONSTRAINT vtimelog_angler_fk
      FOREIGN KEY (angler_id)
      REFERENCES anglers (angler_id);

      -- news_items table
      ALTER TABLE news_items
      ALTER COLUMN title SET NOT NULL;

      ALTER TABLE news_items
      ALTER COLUMN content SET NOT NULL;

      ALTER TABLE news_items
      ALTER COLUMN created_at SET NOT NULL;

      ALTER TABLE news_items
      ALTER COLUMN updated_at SET NOT NULL;

      ALTER TABLE news_items
      ALTER COLUMN user_id SET NOT NULL;

      ALTER TABLE news_items
      ALTER COLUMN should_publish SET DEFAULT FALSE;

      ALTER TABLE news_items
      ADD CONSTRAINT newsitem_user_fk
      FOREIGN KEY (user_id)
      REFERENCES users (id);

      -- front_page_photo_stream_items table
      ALTER TABLE front_page_photo_stream_items
      ALTER COLUMN created_at SET NOT NULL;

      ALTER TABLE front_page_photo_stream_items
      ALTER COLUMN updated_at SET NOT NULL;

      ALTER TABLE front_page_photo_stream_items
      ALTER COLUMN featured SET DEFAULT FALSE;

      -- photos table
      ALTER TABLE photos
      ALTER COLUMN created_at SET NOT NULL;

      ALTER TABLE photos
      ALTER COLUMN updated_at SET NOT NULL;

      -- recaptures table
      CREATE INDEX recaptures_tag_idx ON recaptures (tag_id);
      CREATE INDEX recaptures_angler_idx ON recaptures (angler_id);
      CREATE INDEX recaptures_geom_idx ON recaptures (geom);
      CREATE INDEX recaptures_date_idx ON recaptures (capture_date);

      ALTER TABLE recaptures
      ALTER COLUMN created_at SET NOT NULL;

      ALTER TABLE recaptures
      ALTER COLUMN updated_at SET NOT NULL;

    SQL
  end
end

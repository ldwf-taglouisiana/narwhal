class CreateBaitTypeOptions < ActiveRecord::Migration
  def change
    create_table :bait_type_options do |t|
      t.string :bait_type_option

      t.timestamps
    end
  end
end

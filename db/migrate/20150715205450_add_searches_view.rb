class AddSearchesView < ActiveRecord::Migration
  def up
    execute <<-SQL
      CREATE MATERIALIZED VIEW searches AS

        ------------------------------------------------------------
        -- CAPTURES
        ------------------------------------------------------------

        -- CAPTURES : location_description
        SELECT
          md5(concat(searchable_id :: TEXT, searchable_type :: TEXT, searchable_attribute :: TEXT, term :: TEXT)) AS key,
          *
        FROM (
             SELECT
               captures.id                   AS searchable_id,
               'Capture'                     COLLATE "en_US" AS  searchable_type,
               'location_description'        COLLATE "en_US" AS  searchable_attribute,
               captures.location_description AS term
             FROM
               captures
             WHERE
               location_description IS NOT NULL AND trim(location_description) != ''
           ) t


        UNION

        -- CAPTURES : angler_id
        SELECT
          md5(concat(searchable_id :: TEXT, searchable_type :: TEXT, searchable_attribute :: TEXT, term :: TEXT)) AS key,
          *
        FROM (SELECT
              captures.id        AS searchable_id,
              'Capture'          COLLATE "en_US" AS  searchable_type,
              'angler_id'        COLLATE "en_US" AS  searchable_attribute,
              captures.angler_id AS term
            FROM captures
            WHERE angler_id IS NOT NULL

           ) t
        UNION

        -- CAPTURES -> TAGS : tag_no
        SELECT
          md5(concat(searchable_id :: TEXT, searchable_type :: TEXT, searchable_attribute :: TEXT, term :: TEXT)) AS key,
          *
        FROM (SELECT
              captures.id  AS searchable_id,
              'Capture'    COLLATE "en_US" AS  searchable_type,
              'tag_number' COLLATE "en_US" AS  searchable_attribute,
              tags.tag_no  AS term
            FROM captures
              INNER JOIN tags ON captures.tag_id = tags.id
            WHERE tag_no IS NOT NULL

           ) t
        UNION

        -- CAPTURES -> SPECIES : common_name
        SELECT
          md5(concat(searchable_id :: TEXT, searchable_type :: TEXT, searchable_attribute :: TEXT, term :: TEXT)) AS key,
          *
        FROM (SELECT
              captures.id         AS searchable_id,
              'Capture'           COLLATE "en_US" AS  searchable_type,
              'species_name'      COLLATE "en_US" AS  searchable_attribute,
              species.common_name AS term
            FROM captures
              INNER JOIN species ON captures.species_id = species.id

           ) t
        UNION

        -- CAPTURES -> ANGLERS : first_name
        SELECT
          md5(concat(searchable_id :: TEXT, searchable_type :: TEXT, searchable_attribute :: TEXT, term :: TEXT)) AS key,
          *
        FROM (SELECT
              captures.id         AS searchable_id,
              'Capture'           COLLATE "en_US" AS  searchable_type,
              'angler_first_name' COLLATE "en_US" AS  searchable_attribute,
              first_name          AS term
            FROM captures
              INNER JOIN anglers ON captures.angler_id = anglers.angler_id
            WHERE
              first_name IS NOT NULL AND trim(first_name) != ''

           ) t
        UNION

        -- CAPTURES -> ANGLERS : last_name
        SELECT
          md5(concat(searchable_id :: TEXT, searchable_type :: TEXT, searchable_attribute :: TEXT, term :: TEXT)) AS key,
          *
        FROM (SELECT
              captures.id        AS searchable_id,
              'Capture'          COLLATE "en_US" AS  searchable_type,
              'angler_last_name' COLLATE "en_US" AS  searchable_attribute,
              last_name          AS term
            FROM captures
              INNER JOIN anglers ON captures.angler_id = anglers.angler_id
            WHERE
              last_name IS NOT NULL AND trim(last_name) != ''

           ) t
        UNION

        ------------------------------------------------------------
          -- RECAPTURES
          ------------------------------------------------------------

          -- RECAPTURES : location_description
        SELECT
          md5(concat(searchable_id :: TEXT, searchable_type :: TEXT, searchable_attribute :: TEXT, term :: TEXT)) AS key,
          *
        FROM (SELECT
              recaptures.id                   AS searchable_id,
              'Recapture'                     COLLATE "en_US" AS  searchable_type,
              'location_description'          COLLATE "en_US" AS  searchable_attribute,
              recaptures.location_description AS term
            FROM recaptures
            WHERE
              location_description IS NOT NULL AND trim(location_description) != ''

           ) t
        UNION

        -- RECAPTURES : angler_id
        SELECT
          md5(concat(searchable_id :: TEXT, searchable_type :: TEXT, searchable_attribute :: TEXT, term :: TEXT)) AS key,
          *
        FROM (SELECT
              recaptures.id        AS searchable_id,
              'Recapture'          COLLATE "en_US" AS  searchable_type,
              'angler_id'          COLLATE "en_US" AS  searchable_attribute,
              recaptures.angler_id AS term
            FROM recaptures

           ) t
        UNION

        -- RECAPTURES -> TAGS : tag_no
        SELECT
          md5(concat(searchable_id :: TEXT, searchable_type :: TEXT, searchable_attribute :: TEXT, term :: TEXT)) AS key,
          *
        FROM (SELECT
              recaptures.id AS searchable_id,
              'Recapture'   COLLATE "en_US" AS  searchable_type,
              'tag_number'  COLLATE "en_US" AS  searchable_attribute,
              tags.tag_no   AS term
            FROM recaptures
              INNER JOIN tags ON recaptures.tag_id = tags.id

           ) t
        UNION

        -- RECAPTURES -> SPECIES : common_name
        SELECT
          md5(concat(searchable_id :: TEXT, searchable_type :: TEXT, searchable_attribute :: TEXT, term :: TEXT)) AS key,
          *
        FROM (SELECT
              recaptures.id       AS searchable_id,
              'Recapture'         COLLATE "en_US" AS  searchable_type,
              'species_name'      COLLATE "en_US" AS  searchable_attribute,
              species.common_name AS term
            FROM recaptures
              INNER JOIN species ON recaptures.species_id = species.id

           ) t
        UNION

        -- RECAPTURES -> ANGLERS : first_name
        SELECT
          md5(concat(searchable_id :: TEXT, searchable_type :: TEXT, searchable_attribute :: TEXT, term :: TEXT)) AS key,
          *
        FROM (SELECT
              recaptures.id       AS searchable_id,
              'Recapture'         COLLATE "en_US" AS  searchable_type,
              'angler_first_name' COLLATE "en_US" AS  searchable_attribute,
              first_name          AS term
            FROM recaptures
              INNER JOIN anglers ON recaptures.angler_id = anglers.angler_id
            WHERE
              first_name IS NOT NULL AND trim(first_name) != ''

           ) t
        UNION

        -- RECAPTURES -> ANGLERS : last_name
        SELECT
          md5(concat(searchable_id :: TEXT, searchable_type :: TEXT, searchable_attribute :: TEXT, term :: TEXT)) AS key,
          *
        FROM (SELECT
              recaptures.id      AS searchable_id,
              'Recapture'        COLLATE "en_US" AS  searchable_type,
              'angler_last_name' COLLATE "en_US" AS  searchable_attribute,
              last_name          AS term
            FROM recaptures
              INNER JOIN anglers ON recaptures.angler_id = anglers.angler_id
            WHERE
              last_name IS NOT NULL AND trim(last_name) != ''

           ) t
        UNION

        ------------------------------------------------------------
          -- DRAFT_CAPTURES
          ------------------------------------------------------------

          -- DRAFT_CAPTURES : location_description
        SELECT
          md5(concat(searchable_id :: TEXT, searchable_type :: TEXT, searchable_attribute :: TEXT, term :: TEXT)) AS key,
          *
        FROM (SELECT
              draft_captures.id                   AS searchable_id,
              'DraftCapture'                      COLLATE "en_US" AS  searchable_type,
              'location_description'              COLLATE "en_US" AS  searchable_attribute,
              draft_captures.location_description AS term
            FROM draft_captures
            WHERE
              location_description IS NOT NULL AND trim(location_description) != ''

           ) t
        UNION

        -- DRAFT_CAPTURES : comments
        SELECT
          md5(concat(searchable_id :: TEXT, searchable_type :: TEXT, searchable_attribute :: TEXT, term :: TEXT)) AS key,
          *
        FROM (SELECT
              draft_captures.id       AS searchable_id,
              'DraftCapture'          COLLATE "en_US" AS  searchable_type,
              'comments'              COLLATE "en_US" AS  searchable_attribute,
              draft_captures.comments AS term
            FROM draft_captures
            WHERE
              comments IS NOT NULL AND trim(comments) != ''

           ) t
        UNION

        -- DRAFT_CAPTURES : angler_id
        SELECT
          md5(concat(searchable_id :: TEXT, searchable_type :: TEXT, searchable_attribute :: TEXT, term :: TEXT)) AS key,
          *
        FROM (SELECT
              draft_captures.id        AS searchable_id,
              'DraftCapture'           COLLATE "en_US" AS  searchable_type,
              'angler_id'              COLLATE "en_US" AS  searchable_attribute,
              draft_captures.angler_id AS term
            FROM draft_captures


           ) t
        UNION

        -- DRAFT_CAPTURES : tag_number
        SELECT
          md5(concat(searchable_id :: TEXT, searchable_type :: TEXT, searchable_attribute :: TEXT, term :: TEXT)) AS key,
          *
        FROM (SELECT
              draft_captures.id         AS searchable_id,
              'DraftCapture'            COLLATE "en_US" AS  searchable_type,
              'tag_number'              COLLATE "en_US" AS  searchable_attribute,
              draft_captures.tag_number AS term
            FROM draft_captures

           ) t
        UNION

        -- DRAFT_CAPTURES -> SPECIES : common_name
        SELECT
          md5(concat(searchable_id :: TEXT, searchable_type :: TEXT, searchable_attribute :: TEXT, term :: TEXT)) AS key,
          *
        FROM (SELECT
              draft_captures.id   AS searchable_id,
              'DraftCapture'      COLLATE "en_US" AS  searchable_type,
              'species_name'      COLLATE "en_US" AS  searchable_attribute,
              species.common_name AS term
            FROM draft_captures
              INNER JOIN species ON draft_captures.species_id = species.id

           ) t
        UNION

        -- DRAFT_CAPTURES -> ANGLERS : first_name
        SELECT
          md5(concat(searchable_id :: TEXT, searchable_type :: TEXT, searchable_attribute :: TEXT, term :: TEXT)) AS key,
          *
        FROM (SELECT
              draft_captures.id   AS searchable_id,
              'DraftCapture'      COLLATE "en_US" AS  searchable_type,
              'angler_first_name' COLLATE "en_US" AS  searchable_attribute,
              first_name          AS term
            FROM draft_captures
              INNER JOIN anglers ON draft_captures.angler_id = anglers.angler_id
            WHERE
              first_name IS NOT NULL AND trim(first_name) != ''

           ) t
        UNION

        -- DRAFT_CAPTURES -> ANGLERS : last_name
        SELECT
          md5(concat(searchable_id :: TEXT, searchable_type :: TEXT, searchable_attribute :: TEXT, term :: TEXT)) AS key,
          *
        FROM (SELECT
              draft_captures.id  AS searchable_id,
              'DraftCapture'     COLLATE "en_US" AS  searchable_type,
              'angler_last_name' COLLATE "en_US" AS  searchable_attribute,
              last_name          AS term
            FROM draft_captures
              INNER JOIN anglers ON draft_captures.angler_id = anglers.angler_id
            WHERE
              last_name IS NOT NULL AND trim(last_name) != ''

           ) t
        UNION

        ------------------------------------------------------------
          -- TAGS
          ------------------------------------------------------------

          -- TAGS : tag_no
        SELECT
          md5(concat(searchable_id :: TEXT, searchable_type :: TEXT, searchable_attribute :: TEXT, term :: TEXT)) AS key,
          *
        FROM (SELECT
              tags.id      AS searchable_id,
              'Tag'        COLLATE "en_US" AS  searchable_type,
              'tag_number' COLLATE "en_US" AS  searchable_attribute,
              tags.tag_no  AS term
            FROM tags

           ) t
        UNION

        -- TAGS : angler_id
        SELECT
          md5(concat(searchable_id :: TEXT, searchable_type :: TEXT, searchable_attribute :: TEXT, term :: TEXT)) AS key,
          *
        FROM (SELECT
              tags.id        AS searchable_id,
              'Tag'          COLLATE "en_US" AS  searchable_type,
              'angler_id'    COLLATE "en_US" AS  searchable_attribute,
              tags.angler_id AS term
            FROM tags
            WHERE angler_id IS NOT NULL

           ) t
        UNION

        -- TAGS -> ANGLERS : first_name
        SELECT
          md5(concat(searchable_id :: TEXT, searchable_type :: TEXT, searchable_attribute :: TEXT, term :: TEXT)) AS key,
          *
        FROM (SELECT
              tags.id             AS searchable_id,
              'Tag'               COLLATE "en_US" AS  searchable_type,
              'angler_first_name' COLLATE "en_US" AS  searchable_attribute,
              first_name          AS term
            FROM tags
              INNER JOIN anglers ON tags.angler_id = anglers.angler_id

            WHERE
              first_name IS NOT NULL AND trim(first_name) != ''
           ) t
        UNION

        -- TAGS -> ANGLERS : first_name
        SELECT
          md5(concat(searchable_id :: TEXT, searchable_type :: TEXT, searchable_attribute :: TEXT, term :: TEXT)) AS key,
          *
        FROM (SELECT
              tags.id            AS searchable_id,
              'Tag'              COLLATE "en_US" AS  searchable_type,
              'angler_last_name' COLLATE "en_US" AS  searchable_attribute,
              last_name          AS term
            FROM tags
              INNER JOIN anglers ON tags.angler_id = anglers.angler_id
            WHERE
              last_name IS NOT NULL AND trim(last_name) != ''

           ) t
        UNION


        ------------------------------------------------------------
          -- ANGLERS
          ------------------------------------------------------------

          -- ANGLERS : angler_id
        SELECT
          md5(concat(searchable_id :: TEXT, searchable_type :: TEXT, searchable_attribute :: TEXT, term :: TEXT)) AS key,
          *
        FROM (SELECT
              anglers.id        AS searchable_id,
              'Angler'          COLLATE "en_US" AS  searchable_type,
              'angler_id'       COLLATE "en_US" AS  searchable_attribute,
              anglers.angler_id AS term
            FROM anglers

           ) t
        UNION

        -- ANGLERS : first_name
        SELECT
          md5(concat(searchable_id :: TEXT, searchable_type :: TEXT, searchable_attribute :: TEXT, term :: TEXT)) AS key,
          *
        FROM (SELECT
              anglers.id          AS searchable_id,
              'Angler'            COLLATE "en_US" AS  searchable_type,
              'angler_first_name' COLLATE "en_US" AS  searchable_attribute,
              anglers.first_name  AS term
            FROM anglers
            WHERE
              first_name IS NOT NULL AND trim(first_name) != ''

           ) t
        UNION

        -- ANGLERS : last_name
        SELECT
          md5(concat(searchable_id :: TEXT, searchable_type :: TEXT, searchable_attribute :: TEXT, term :: TEXT)) AS key,
          *
        FROM (SELECT
              anglers.id         AS searchable_id,
              'Angler'           COLLATE "en_US" AS  searchable_type,
              'angler_last_name' COLLATE "en_US" AS  searchable_attribute,
              anglers.last_name  AS term
            FROM anglers

            WHERE
              last_name IS NOT NULL AND trim(last_name) != ''
           ) t
        UNION

        ------------------------------------------------------------
          -- VOLUNTEER_TIME_LOGS
          ------------------------------------------------------------

          -- VOLUNTEER_TIME_LOGS : angler_id
        SELECT
          md5(concat(searchable_id :: TEXT, searchable_type :: TEXT, searchable_attribute :: TEXT, term :: TEXT)) AS key,
          *
        FROM (SELECT
              volunteer_time_logs.id        AS searchable_id,
              'VolunteerTimeLog'            COLLATE "en_US" AS  searchable_type,
              'angler_id'                   COLLATE "en_US" AS  searchable_attribute,
              volunteer_time_logs.angler_id AS term
            FROM volunteer_time_logs

           ) t
        UNION

        -- VOLUNTEER_TIME_LOGS -> ANGLERS : first_name
        SELECT
          md5(concat(searchable_id :: TEXT, searchable_type :: TEXT, searchable_attribute :: TEXT, term :: TEXT)) AS key,
          *
        FROM (SELECT
              volunteer_time_logs.id AS searchable_id,
              'VolunteerTimeLog'     COLLATE "en_US" AS  searchable_type,
              'angler_first_name'    COLLATE "en_US" AS  searchable_attribute,
              anglers.first_name     AS term
            FROM volunteer_time_logs
              INNER JOIN anglers ON volunteer_time_logs.angler_id = anglers.angler_id
            WHERE
              first_name IS NOT NULL AND trim(first_name) != ''

           ) t
        UNION

        -- VOLUNTEER_TIME_LOGS -> ANGLERS : last_name
        SELECT
          md5(concat(searchable_id :: TEXT, searchable_type :: TEXT, searchable_attribute :: TEXT, term :: TEXT)) AS key,
          *
        FROM (SELECT
              volunteer_time_logs.id AS searchable_id,
              'VolunteerTimeLog'     COLLATE "en_US" AS  searchable_type,
              'angler_last_name'     COLLATE "en_US" AS  searchable_attribute,
              anglers.last_name      AS term
            FROM volunteer_time_logs
              INNER JOIN anglers ON volunteer_time_logs.angler_id = anglers.angler_id
            WHERE
              last_name IS NOT NULL AND trim(last_name) != ''

           ) t
        UNION

        ------------------------------------------------------------
          -- USERS
          ------------------------------------------------------------

          -- USERS : angler_id
        SELECT
          md5(concat(searchable_id :: TEXT, searchable_type :: TEXT, searchable_attribute :: TEXT, term :: TEXT)) AS key,
          *
        FROM (SELECT
              users.id        AS searchable_id,
              'User'          COLLATE "en_US" AS  searchable_type,
              'angler_id'     COLLATE "en_US" AS  searchable_attribute,
              users.angler_id AS term
            FROM users
            WHERE angler_id IS NOT NULL
           ) t
        UNION

        -- USERS : email
        SELECT
          md5(concat(searchable_id :: TEXT, searchable_type :: TEXT, searchable_attribute :: TEXT, term :: TEXT)) AS key,
          *
        FROM (SELECT
              users.id    AS searchable_id,
              'User'      COLLATE "en_US" AS  searchable_type,
              'email'     COLLATE "en_US" AS  searchable_attribute,
              users.email AS term
            FROM users
            WHERE
              email IS NOT NULL AND trim(email) != ''

           ) t
        UNION

        -- USERS : first_name
        SELECT
          md5(concat(searchable_id :: TEXT, searchable_type :: TEXT, searchable_attribute :: TEXT, term :: TEXT)) AS key,
          *
        FROM (SELECT
              users.id          AS searchable_id,
              'User'            COLLATE "en_US" AS  searchable_type,
              'user_first_name' COLLATE "en_US" AS  searchable_attribute,
              users.first_name  AS term
            FROM users
            WHERE
              first_name IS NOT NULL AND trim(first_name) != ''

           ) t
        UNION

        -- USERS : last_name
        SELECT
          md5(concat(searchable_id :: TEXT, searchable_type :: TEXT, searchable_attribute :: TEXT, term :: TEXT)) AS key,
          *
        FROM (SELECT
              users.id         AS searchable_id,
              'User'           COLLATE "en_US" AS  searchable_type,
              'user_last_name' COLLATE "en_US" AS  searchable_attribute,
              users.last_name  AS term
            FROM users
            WHERE
              last_name IS NOT NULL AND trim(last_name) != ''

           ) t
        UNION

        -- USERS -> ANGLERS : first_name
        SELECT
          md5(concat(searchable_id :: TEXT, searchable_type :: TEXT, searchable_attribute :: TEXT, term :: TEXT)) AS key,
          *
        FROM (SELECT
              users.id            AS searchable_id,
              'User'              COLLATE "en_US" AS  searchable_type,
              'angler_first_name' COLLATE "en_US" AS  searchable_attribute,
              anglers.first_name  AS term
            FROM users
              INNER JOIN anglers ON users.angler_id = anglers.angler_id
            WHERE
              anglers.first_name IS NOT NULL AND trim(anglers.first_name) != ''

           ) t
        UNION

        -- USERS -> ANGLERS : last_name
        SELECT
          md5(concat(searchable_id :: TEXT, searchable_type :: TEXT, searchable_attribute :: TEXT, term :: TEXT)) AS key,
          *
        FROM (SELECT
              users.id           AS searchable_id,
              'User'             COLLATE "en_US" AS  searchable_type,
              'angler_last_name' COLLATE "en_US" AS  searchable_attribute,
              anglers.last_name  AS term
            FROM users
              INNER JOIN anglers ON users.angler_id = anglers.angler_id
            WHERE
              anglers.last_name IS NOT NULL AND trim(anglers.last_name) != ''
           ) t;


      CREATE UNIQUE INDEX idx_searches_primary_key ON searches (key);
      CREATE INDEX index_gin_searches_on_term ON searches USING GIN (to_tsvector('english', term));
      CREATE INDEX index_searches_on_searchable_type ON searches (searchable_type);
      CREATE INDEX index_searches_on_attribute ON searches (searchable_attribute);
      CREATE INDEX  index_searches_on_searchable_type_attribute ON searches (searchable_type, searchable_attribute);
    SQL
  end

  def down
    execute 'DROP MATERIALIZED VIEW searches;'
  end
end

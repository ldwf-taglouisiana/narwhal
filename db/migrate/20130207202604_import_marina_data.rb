class ImportMarinaData < ActiveRecord::Migration
  def up
    CSV.foreach("./import_files/marina_list.csv") do |row|
      record = Marina.new(
          :site_no => row[0],
          :name => row[1].split(' ').map { |w| w.capitalize }.join(' '),
          :closed => row[2],
          :city => row[3],
          :parish => row[4],
          :publish => row[5],
          :address => row[6],
          :latitude => row[7],
          :longitude => row[8],
          :web_address => row[9],
          :phone => row[10],
          :hours_of_operation => row[11],
          :can_get_fishing_license => row[12],
          :fee_amount => row[13],
          :has_bait => row[14],
          :has_ice => row[15],
          :has_food => row[16],
          :has_restrooms => row[17],
          :has_sewage_pump => row[18],
          :has_cert_scales => row[19],
          :facility_type => row[20],
          :has_cleaning_station => row[21],
          :fuel_type => row[22],
          :location_description => row[23]
      )
      record.save
    end
  end

  def down
    Marina.delete_all
  end
end

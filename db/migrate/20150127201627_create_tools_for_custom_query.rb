class CreateToolsForCustomQuery < ActiveRecord::Migration
  def change
    execute <<-SQL

-- add the extension that allows for mixed indices
CREATE EXTENSION btree_gist;

alter table captures alter column length type numeric;
alter table recaptures alter column length type numeric;

CREATE OR REPLACE FUNCTION year_to_int(timestamp) RETURNS integer AS $$
	select to_char($1, 'YYYY')::int
$$ LANGUAGE SQL immutable;

CREATE OR REPLACE FUNCTION location_description_cleaner(item text) RETURNS text AS $$
	SELECT regexp_replace(item, '^[0-9]{4}\s|\n.*|\r.*|^\d.+?$', '', 'g')
$$ LANGUAGE SQL ;

--
-- function to refresh all materialized views in a schema
--

CREATE OR REPLACE FUNCTION RefreshAllMaterializedViews(schema_arg TEXT DEFAULT 'public')
RETURNS INT AS $$
	DECLARE
		r RECORD;
	BEGIN
		RAISE NOTICE 'Refreshing materialized view in schema %', schema_arg;
		FOR r IN SELECT matviewname FROM pg_matviews WHERE schemaname = schema_arg order by matviewname DESC
		LOOP
			RAISE NOTICE 'Refreshing %.%', schema_arg, r.matviewname;
			EXECUTE 'REFRESH MATERIALIZED VIEW ' || schema_arg || '.' || r.matviewname;
		END LOOP;

		RETURN 1;
	END
$$ LANGUAGE plpgsql;

--
-- the materialized views for the custom query
--

CREATE MATERIALIZED VIEW locations AS (
	SELECT
		row_number() over () as "id",
		location as "location_description"
	FROM (
		SELECT
			location_description_cleaner(location_description) AS "location"
		FROM
			captures
		UNION
		SELECT
			location_description_cleaner(location_description) AS "location"
		FROM
			recaptures
	) a
);

CREATE INDEX idx_locations_id on locations (id);

---

CREATE MATERIALIZED VIEW denormalized_captures AS (
	SELECT
		a.id as "id",
		a.species_id                                			      AS "species_id",
		species.common_name                                     AS "species_name",
		a.tag_id                                                AS "tag_id",
		tags.tag_no                                             AS "tag_number",
		a.angler_id                                 			      AS "angler_id",
		concat(anglers.first_name, ' ', anglers.last_name)      AS "capture_angler",
		a.capture_date                                          AS "capture_date",
		to_char(a.capture_date, 'MM/DD/YYYY')                 	AS "date_string",
		location_description_cleaner(a.location_description)	  AS "location",
		a.time_of_day_option_id                      			      AS "time_of_day_option_id",
		time_of_day_options.time_of_day_option                  AS "time_of_day",
		a.length                                    		      	AS "length",
		species_lengths.description                             AS "length_range",
		a.fish_condition_option_id                				      AS "fish_condition_option_id",
		fish_condition_options.fish_condition_option            AS "fish_condition",
		a.latitude                                  		  	    AS "latitude",
		a.longitude                                 			      AS "longitude",
		a.comments                      						            AS "comments",
		a.location_description                      			      AS "location_description",
		locations.id 											                      AS "location_id",
		(SELECT count(*) FROM recaptures WHERE recaptures.tag_id = a.tag_id) AS "recapture_count",
		year_to_int(capture_date) 								              AS "year_id"
	FROM captures a
		LEFT JOIN species ON a.species_id = species.id
		LEFT JOIN time_of_day_options ON a.time_of_day_option_id = time_of_day_options.id
		LEFT JOIN tags ON a.tag_id = tags.id
		LEFT JOIN fish_condition_options ON a.fish_condition_option_id = fish_condition_options.id
		LEFT JOIN anglers ON a.angler_id = anglers.angler_id
		LEFT JOIN species_lengths ON a.species_length_id = species_lengths.id
		LEFT JOIN locations on location_description_cleaner(a.location_description) = locations.location_description
);

create index idx_denormalized_captures_where_tag on denormalized_captures USING GIST (angler_id, tag_number, location_description, comments, time_of_day_option_id, fish_condition_option_id, length, species_id, capture_date);
create index idx_denormalized_captures_where_tag_year on denormalized_captures USING GIST (year_id, angler_id, tag_number, location_description, comments, time_of_day_option_id, fish_condition_option_id, length, species_id, capture_date);

---

CREATE MATERIALIZED VIEW denormalized_recaptures AS (
	SELECT
		a.species_id                                          AS "species_id",
		species.common_name                                   AS "species_name",
		a.tag_id                                              AS "tag_id",
		a.angler_id                                           AS "recapture_angler_id",
		concat(anglers.first_name, ' ', anglers.last_name)    AS "recapture_angler_name",
		a.capture_date                                        AS "capture_date",
		to_char(a.capture_date :: DATE, 'MM/DD/YYYY')         AS "date_string",
		location_description_cleaner(a.location_description)  AS "location",
		time_of_day_options.time_of_day_option                AS "time_of_day",
		a.length                                              AS "length",
		species_lengths.description                           AS "length_range",
		recapture_dispositions.recapture_disposition          AS "recapture_disposition",
		fish_condition_options.fish_condition_option          AS "fish_condition",
		a.latitude                                            AS "latitude",
		a.longitude                                           AS "longitude",
		a.location_description                                AS "location_description",
		locations.id                                          AS "location_id",
		(SELECT id from captures where captures.tag_id = a.tag_id )           AS "capture_id",
		row_number() OVER (PARTITION BY a.tag_id ORDER BY a.capture_date ASC) AS "recapture_number"
	FROM recaptures a
		LEFT OUTER JOIN species ON a.species_id =  species.id
		LEFT OUTER JOIN time_of_day_options ON a.time_of_day_option_id = time_of_day_options.id
		LEFT OUTER JOIN tags ON a.tag_id = tags.id
		LEFT OUTER JOIN recapture_dispositions ON a.recapture_disposition_id = recapture_dispositions.id
		LEFT OUTER JOIN fish_condition_options ON a.fish_condition_option_id = fish_condition_options.id
		LEFT OUTER JOIN anglers ON a.angler_id = anglers.angler_id
		LEFT OUTER JOIN species_lengths ON a.species_length_id = species_lengths.id
		LEFT OUTER JOIN locations on location_description_cleaner(a.location_description) = locations.location_description
);

create index idx_denormalized_recaptures_capture_id on denormalized_recaptures(capture_id);

---

CREATE OR REPLACE FUNCTION RebuildMaterializedViews() RETURNS INT AS $$
		drop index idx_locations_id;
		drop index idx_denormalized_captures_where_tag;
		drop index idx_denormalized_captures_where_tag_year;
		drop index idx_denormalized_recaptures_capture_id;

		select RefreshAllMaterializedViews();

		CREATE INDEX idx_locations_id on locations (id);
		create index idx_denormalized_captures_where_tag on denormalized_captures USING GIST (angler_id, tag_number, location_description, comments, time_of_day_option_id, fish_condition_option_id, length, species_id, capture_date);
		create index idx_denormalized_captures_where_tag_year on denormalized_captures USING GIST (year_id, angler_id, tag_number, location_description, comments, time_of_day_option_id, fish_condition_option_id, length, species_id, capture_date);
		create index idx_denormalized_recaptures_capture_id on denormalized_recaptures(capture_id);

		select 1;
$$ LANGUAGE SQL ;

    SQL
  end
end

class CreateTagRequests < ActiveRecord::Migration
  def change
    create_table :tag_requests do |t|
      t.integer :angler_id
      t.integer :num_tags_requested
      t.datetime :date_requested
      t.boolean :fulfilled
      t.datetime :date_fulfilled
      t.boolean :deleted

      t.timestamps
    end
  end
end

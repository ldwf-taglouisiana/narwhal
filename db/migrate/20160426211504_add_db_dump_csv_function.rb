class AddDbDumpCsvFunction < ActiveRecord::Migration
  def change
    execute <<-SQL
      CREATE OR REPLACE FUNCTION db_to_csv(path TEXT) RETURNS void AS $$
      declare
        tables RECORD;
        statement TEXT;
      begin
        FOR tables IN
          SELECT
            (table_schema || '."' || table_name || '"') AS schema_table,
            table_name
          FROM information_schema.tables t INNER JOIN information_schema.schemata s
          ON s.schema_name = t.table_schema
          WHERE t.table_schema NOT IN ('pg_catalog', 'information_schema', 'configuration')
            AND t.table_type = 'BASE TABLE'
            AND ( NOT table_name ~* 'oauth')
          ORDER BY schema_table
        LOOP
          statement := 'COPY ' || tables.schema_table || ' TO ''' || path || '/' || tables.table_name || '.csv' ||''' DELIMITER '';'' CSV HEADER';
          EXECUTE statement;
        END LOOP;
        return;
      end;
      $$ LANGUAGE plpgsql;
    SQL
  end
end

class CreateFishEventLocationDescriptions < ActiveRecord::Migration
  def change
    create_table :fish_event_location_descriptions do |t|
      t.text :description, null: false
      t.timestamps null: false
    end

    execute <<-SQL
      INSERT INTO fish_event_location_descriptions (description, created_at, updated_at) (
        SELECT
          location_description_cleaner(location_description), now(), now()
        FROM
          captures
        WHERE captures.location_description IS NOT NULL
        UNION
        SELECT
          location_description_cleaner(location_description), now(), now()
        FROM
          recaptures
        WHERE recaptures.location_description IS NOT NULL
      )
    SQL

    add_index :fish_event_location_descriptions, :description, unique: true
  end
end

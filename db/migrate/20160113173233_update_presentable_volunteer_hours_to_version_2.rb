class UpdatePresentableVolunteerHoursToVersion2 < ActiveRecord::Migration
  def change
    create_view :presentable_volunteer_hours, version: 2
  end
end

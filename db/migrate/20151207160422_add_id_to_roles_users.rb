class AddIdToRolesUsers < ActiveRecord::Migration
  def up
    execute <<-SQL
      ALTER TABLE roles_users ADD COLUMN id BIGSERIAL PRIMARY KEY;
    SQL
  end

  def down
    remove_column :roles_users, :id, :bigint
  end
end

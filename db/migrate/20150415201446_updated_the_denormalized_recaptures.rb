class UpdatedTheDenormalizedRecaptures < ActiveRecord::Migration
  def up
    execute <<-SQL

      DROP materialized VIEW denormalized_recaptures;

      CREATE MATERIALIZED VIEW denormalized_recaptures AS
       SELECT
        a.species_id,
          species.common_name AS species_name,
          a.tag_id,
        tags.tag_no as "tag_number",
          a.angler_id AS recapture_angler_id,
          concat(anglers.first_name, ' ', anglers.last_name) AS recapture_angler_name,
          a.capture_date,
          to_char(((a.capture_date)::date)::timestamp with time zone, 'MM/DD/YYYY'::text) AS date_string,
          location_description_cleaner(a.location_description) AS location,
          a.time_of_day_option_id,
          time_of_day_options.time_of_day_option AS time_of_day,
          a.length,
          species_lengths.description AS length_range,
          recapture_dispositions.recapture_disposition,
          a.fish_condition_option_id,
          fish_condition_options.fish_condition_option AS fish_condition,
          a.latitude,
          a.longitude,
          a.comments,
          a.location_description,
          locations.id AS location_id,
          ( SELECT captures.id
                 FROM captures
                WHERE (captures.tag_id = a.tag_id)) AS capture_id,
          row_number() OVER (PARTITION BY a.tag_id ORDER BY a.capture_date) AS recapture_number,
          year_to_int(a.capture_date) as year_id
         FROM ((((((((recaptures a
           LEFT JOIN species ON ((a.species_id = species.id)))
           LEFT JOIN time_of_day_options ON ((a.time_of_day_option_id = time_of_day_options.id)))
           LEFT JOIN tags ON ((a.tag_id = tags.id)))
           LEFT JOIN recapture_dispositions ON ((a.recapture_disposition_id = recapture_dispositions.id)))
           LEFT JOIN fish_condition_options ON ((a.fish_condition_option_id = fish_condition_options.id)))
           LEFT JOIN anglers ON (((a.angler_id)::text = (anglers.angler_id)::text)))
           LEFT JOIN species_lengths ON ((a.species_length_id = species_lengths.id)))
           LEFT JOIN locations ON ((location_description_cleaner(a.location_description) = locations.location_description)))
      ;

      CREATE INDEX idx_denormalized_recaptures_capture_id ON denormalized_recaptures(capture_id);
      CREATE INDEX idx_denormalized_recaptures_where_tag ON denormalized_recaptures USING GIST (recapture_angler_id, tag_number, location_description, comments, time_of_day, fish_condition, length, species_id, capture_date);
      CREATE INDEX idx_denormalized_recaptures_where_tag_year ON denormalized_recaptures USING GIST (year_id, recapture_angler_id, tag_number, location_description, comments, time_of_day, fish_condition, length, species_id, capture_date);
    SQL
  end

  def down
    execute <<-SQL
      DROP materialized VIEW denormalized_recaptures;
    SQL
  end
end

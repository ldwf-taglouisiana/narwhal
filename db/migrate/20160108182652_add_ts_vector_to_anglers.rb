class AddTsVectorToAnglers < ActiveRecord::Migration
  def up
    # add the vector column to the tags table
    add_column :anglers, :search_vector, :tsvector

    # set the tsvector column for the tags
    execute <<-SQL
      UPDATE anglers A
      SET search_vector =
        setweight(to_tsvector('pg_catalog.english', coalesce(angler_id, '')), 'A')  ||
        setweight(to_tsvector('pg_catalog.english', coalesce(last_name, '')), 'B')  ||
        setweight(to_tsvector('pg_catalog.english', coalesce(first_name, '')), 'B') ||
        setweight(to_tsvector('pg_catalog.english', coalesce(email, '')), 'C')
    SQL

    # add the index, after population for faster pre-population
    add_index :anglers, :search_vector, using: :gin

    # disables nulls for the tsvector column
    change_column_null :anglers, :search_vector, false
  end

  def down
    remove_column :anglers, :search_vector, :tsvector
  end
end

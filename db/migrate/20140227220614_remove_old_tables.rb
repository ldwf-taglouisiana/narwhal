class RemoveOldTables < ActiveRecord::Migration
  def change
    execute <<-SQL
      drop table if exists captures_backup;
      drop table if exists captures_dups;
      drop table if exists bait_type_options;
      drop table if exists partial_captures;
      drop table if exists fish_tagged_options;
      drop table if exists girth_accuracy_options;
      drop table if exists tag_requests;
      drop table if exists transaction_logs;
      drop table if exists states;
      drop table if exists length_accuracy_options;
      drop table if exists hook_removed_options;
      drop table if exists phone_type_options;
      drop table if exists hook_type_options;
      drop table if exists hook_barbed_options;
      drop table if exists landing_net_used_options;
      drop table if exists species_lists;
      drop table if exists referrals;
      drop table if exists tag_lot_fixes;
    SQL
  end
end

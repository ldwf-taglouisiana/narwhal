class CreateActivityLogs < ActiveRecord::Migration
  def change
    create_table :activity_logs do |t|
      t.integer :activity_type
      t.string  :item_id
      t.integer :user_id

      t.timestamps
    end
  end
end

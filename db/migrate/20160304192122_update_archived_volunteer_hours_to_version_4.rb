class UpdateArchivedVolunteerHoursToVersion4 < ActiveRecord::Migration

  def up
    drop_view :presentable_volunteer_hours
    drop_view :archived_volunteer_hours, materialized: true

    update_view :recent_volunteer_hours, version: 4, revert_to_version: 3

    create_view :archived_volunteer_hours, version: 4, materialized: true
    create_view :presentable_volunteer_hours, version: 2
  end

  def down
    drop_view :presentable_volunteer_hours
    drop_view :archived_volunteer_hours, materialized: true

    update_view :recent_volunteer_hours, version: 3, revert_to_version: 4

    create_view :archived_volunteer_hours, version: 3, materialized: true
    create_view :presentable_volunteer_hours, version: 2


  end
end

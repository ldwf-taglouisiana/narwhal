class CreateRecaptureDispositions < ActiveRecord::Migration
  def change
    create_table :recapture_dispositions do |t|
      t.string :recapture_disposition

      t.timestamps
    end
  end
end

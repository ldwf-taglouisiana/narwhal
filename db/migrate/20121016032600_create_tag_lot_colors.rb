class CreateTagLotColors < ActiveRecord::Migration
  def change
    create_table :tag_lot_colors do |t|
      t.string :color

      t.timestamps
    end
  end
end

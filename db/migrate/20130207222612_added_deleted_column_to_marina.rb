class AddedDeletedColumnToMarina < ActiveRecord::Migration
  def up
    change_table :marinas do |t|
      t.boolean :deleted, :default => false
    end
  end

  def down
    remove_column :marinas, :deleted
  end
end

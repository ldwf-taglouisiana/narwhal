class CreateSpeciesLengths < ActiveRecord::Migration
  def self.up

    create_table :species_lengths do |t|
      t.column :description, :string
      t.column :species_id, :integer
      t.column :position, :integer

      t.timestamps
    end

    execute <<-SQL
      ALTER TABLE species_lengths
      ALTER COLUMN description SET NOT NULL;

      ALTER TABLE species_lengths
      ALTER COLUMN species_id SET NOT NULL;

      ALTER TABLE species_lengths
      ALTER COLUMN position SET NOT NULL;

      ALTER TABLE species_lengths
      ADD CONSTRAINT species_lengths_species_fk
      FOREIGN KEY (species_id)
      REFERENCES species (id);
    SQL
  end

  def self.down
    execute <<-SQL
      ALTER TABLE species_lengths
      DROP CONSTRAINT IF EXISTS species_lengths_species_fk;
    SQL

    drop_table :species_lengths
  end
end

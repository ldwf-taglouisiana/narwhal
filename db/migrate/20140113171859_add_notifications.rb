class AddNotifications < ActiveRecord::Migration
  def up
    create_table :notification_topics do |t|
      t.string :topic

      t.timestamps
    end

    create_table :notifications do |t|
      t.integer :initiator_id
      t.boolean :checked

      t.references :notification_item, :polymorphic => true
      t.references :notification_topic
      t.references :user

      t.timestamps
    end

    create_table :notification_topics_users, :id => false do |t|
      t.references :notification_topic, :user
    end

    add_index :notification_topics_users, [:notification_topic_id, :user_id], :name => 'notifications_topic_user_idx', :unique => true

    create_table :notification_couriers do |t|
      t.string :courier

      t.timestamps
    end

    create_table :user_couriers do |t|
      t.boolean :daily_digest

      t.references :user
      t.references :notification_courier

      t.timestamps
    end
  end

  def down
    drop_table :notifications
    drop_table :notification_topics
    drop_table :notification_topics_users
    drop_table :notification_couriers
    drop_table :user_couriers
  end
end

class PruneCapturesTable < ActiveRecord::Migration
  def up
    # create a backup of the captures table with all the legacy dat intact
    execute <<-SQL
      SELECT * INTO captures_backup FROM captures;
    SQL

    # if there is a legacy entry date, then move it into the created_at field
    execute <<-SQL
      update captures SET created_at = entry_date WHERE entry_date IS NOT NULL;
    SQL

    remove_column :captures, :captain_id
    remove_column :captures, :estimated_length
    remove_column :captures, :fish_tagged_in_id
    remove_column :captures, :landing_net_used_id
    remove_column :captures, :hook_type_id
    remove_column :captures, :bait_type_id
    remove_column :captures, :hook_removed_id
    remove_column :captures, :hook_barbed_id
    remove_column :captures, :estimated_weight
    remove_column :captures, :water_temperature
    remove_column :captures, :water_salinity
    remove_column :captures, :wind_direction_id
    remove_column :captures, :wind_speed
    remove_column :captures, :entry_date
    remove_column :captures, :see_card
    remove_column :captures, :old_angler_id
  end

  def down
    add_column :captures, :captain_id, :integer
    add_column :captures, :estimated_length, :float
    add_column :captures, :fish_tagged_in_id, :integer
    add_column :captures, :landing_net_used_id, :integer
    add_column :captures, :hook_type_id, :integer
    add_column :captures, :bait_type_id, :integer
    add_column :captures, :hook_removed_id, :integer
    add_column :captures, :hook_barbed_id, :integer
    add_column :captures, :estimated_weight, :float
    add_column :captures, :water_temperature, :float
    add_column :captures, :water_salinity, :float
    add_column :captures, :wind_direction_id, :integer
    add_column :captures, :wind_speed, :float
    add_column :captures, :entry_date, :datetime
    add_column :captures, :see_card, :boolean
    add_column :captures, :old_angler_id, :integer

    # if there is a legacy entry date, then move it into the created_at field
    execute <<-SQL
      UPDATE captures
      SET captain_id = B.captain_id,
          estimated_length = B.estimated_length,
          fish_tagged_in_id = B.fish_tagged_in_id,
          landing_net_used_id = B.landing_net_used_id,
          hook_type_id = B.hook_type_id,
          bait_type_id = B.bait_type_id,
          hook_removed_id = B.hook_removed_id,
          hook_barbed_id = B.hook_barbed_id,
          estimated_weight = B.estimated_weight,
          water_temperature = B.water_temperature,
          water_salinity = B.water_salinity,
          wind_direction_id = B.wind_direction_id,
          wind_speed = B.wind_speed,
          entry_date = B.entry_date,
          see_card = B.see_card,
          old_angler_id = B.old_angler_id
      FROM captures_backup B
      WHERE captures.id = B.id;
    SQL

    drop_table :captures_backup

  end
end

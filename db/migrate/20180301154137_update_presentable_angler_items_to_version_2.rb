class UpdatePresentableAnglerItemsToVersion2 < ActiveRecord::Migration
  def change
    update_view :presentable_angler_items, version: 2
  end
end
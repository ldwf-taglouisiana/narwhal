class AddMetaColumnToVersions < ActiveRecord::Migration
  def change
    add_column :versions, :meta, :jsonb
  end
end

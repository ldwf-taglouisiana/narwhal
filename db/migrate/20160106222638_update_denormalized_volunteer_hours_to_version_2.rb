class UpdateDenormalizedVolunteerHoursToVersion2 < ActiveRecord::Migration
  def change
    drop_view :presentable_volunteer_hours
    drop_view :archived_volunteer_hours, materialized: true
    create_view :archived_volunteer_hours,
      version: 2,
      materialized: true

    add_index :archived_volunteer_hours, :id, unique: true
    add_index :archived_volunteer_hours, :angler_id
    add_index :archived_volunteer_hours, :recent
  end
end

class FixTagVersionsCreations < ActiveRecord::Migration
  def up
    Version.where(item_type: 'Tag').where(activity_type: nil).where("created_at >= ?", Time.parse('2015-12-1')).where(event: 'create').each do |v|
      v.update_column(:activity_type, :create)
    end
  end

  def down
    # do nothing
  end
end

class AddColumnsToSpeciesTable < ActiveRecord::Migration
  def change
    add_column :species, :proper_name, :string
    add_column :species, :other_name, :string
    add_column :species, :family, :string
    add_column :species, :habitat, :text
    add_column :species, :description, :text
    add_column :species, :size, :text
    add_column :species, :food_value, :text
  end
end

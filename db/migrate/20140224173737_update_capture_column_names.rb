class UpdateCaptureColumnNames < ActiveRecord::Migration
  def change
    rename_column :captures, :lat, :latitude
    rename_column :captures, :long, :longitude

    rename_column :captures, :time_of_day, :time_of_day_option_id
    rename_column :captures, :fish_condition_id, :fish_condition_option_id
  end

  def up
    remove_column :captures, :recapture_disposition_id
    remove_column :captures, :recapture
    remove_column :captures, :deleted
    remove_column :captures, :mailed_map
    remove_column :captures, :confirmed

    add_column :captures, :deleted_at, :datetime
  end

  def down
    add_column :captures, :recapture_disposition_id, :integer
    add_column :captures, :recapture, :boolean
    add_column :captures, :deleted, :boolean
    add_column :captures, :mailed_map, :boolean
    add_column :captures, :confirmed, :boolean

    remove_column :captures, :deleted_at
  end
end

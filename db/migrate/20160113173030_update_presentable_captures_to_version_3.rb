class UpdatePresentableCapturesToVersion3 < ActiveRecord::Migration
  def change
    create_view :presentable_captures, version: 3
  end
end

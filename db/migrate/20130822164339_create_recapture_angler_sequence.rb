class CreateRecaptureAnglerSequence < ActiveRecord::Migration
  def up
    execute <<-SQL
      CREATE SEQUENCE recapture_angler_sequence;
      select setval('recapture_angler_sequence', (
        select max(regexp_replace(angler_id, '^[A-Z]+', '')::integer)
        from anglers
        where angler_id ilike '%LAX%') + 1
      );
    SQL
  end

  def down
    execute <<-SQL
      DROP SEQUENCE recapture_angler_sequence;
    SQL
  end
end

class AddUuidToDraftcapture < ActiveRecord::Migration
  def change
    add_column :draft_captures, :uuid, :string
    add_index :draft_captures, :uuid, :unique => true
  end
end

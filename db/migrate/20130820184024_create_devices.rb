class CreateDevices < ActiveRecord::Migration

  def self.up
    create_table :devices do |t|
      t.string :identifier, :null => false
      t.integer :user_id, :null => false
      t.boolean :update_captures, :default => false
      t.boolean :update_tags, :default => false
      t.boolean :update_partial_captures, :default => false
      t.boolean :update_species, :default => false
      t.boolean :update_marinas, :default => false

      t.datetime :last_synced

      t.timestamps
    end

    add_index :devices, :identifier, :unique => true

    # add the database level
    execute <<-SQL
          ALTER TABLE devices
            ADD CONSTRAINT fk_devices_users
            FOREIGN KEY (user_id)
            REFERENCES users(id)
    SQL
  end

  def self.down
    execute <<-SQL
          ALTER TABLE devices
            DROP CONSTRAINT fk_devices_users
    SQL

    remove_index :devices, :identifier

    drop_table :devices
  end
end

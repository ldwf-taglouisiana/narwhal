class RemoveTheOldLocationsTable < ActiveRecord::Migration
  def change
    drop_view :locations, materialized: true
  end
end

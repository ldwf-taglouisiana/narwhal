class CreateNotificationJob < ActiveRecord::Migration
  def change
    create_table :notification_jobs do |t|
      t.timestamps
    end
  end
end

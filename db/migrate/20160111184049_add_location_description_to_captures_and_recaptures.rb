class AddLocationDescriptionToCapturesAndRecaptures < ActiveRecord::Migration
  def up
    change_table :captures do |t|
      t.references :fish_event_location_description
    end

    execute <<-SQL
      UPDATE captures A
      SET fish_event_location_description_id = B.id
      FROM fish_event_location_descriptions AS B
      WHERE location_description_cleaner(A.location_description) = B.description;
    SQL

    change_table :recaptures do |t|
      t.references :fish_event_location_description
    end

    execute <<-SQL
      UPDATE recaptures A
      SET fish_event_location_description_id = B.id
      FROM fish_event_location_descriptions AS B
      WHERE location_description_cleaner(A.location_description) = B.description;
    SQL
  end

  def down
    remove_column :captures, :fish_event_location_description_id
    remove_column :recaptures, :fish_event_location_description_id
  end
end

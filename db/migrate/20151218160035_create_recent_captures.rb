class CreateRecentCaptures < ActiveRecord::Migration
  def change
    create_view :recent_captures
    add_index :captures, :recent
  end
end

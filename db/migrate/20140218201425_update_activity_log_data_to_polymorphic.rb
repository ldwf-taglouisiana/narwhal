class UpdateActivityLogDataToPolymorphic < ActiveRecord::Migration
  def change

    execute <<-SQL
      update activity_logs
      set activity_item_type =  (CASE
                                WHEN activity_type = #{ActivityLogType.create_capture_type.id} OR activity_type = #{ActivityLogType.update_capture_type.id}
                                  THEN 'Capture'
                                WHEN activity_type = #{ActivityLogType.create_angler_type.id} OR activity_type = #{ActivityLogType.update_angler_type.id}
                                  THEN 'Angler'
                                WHEN activity_type = #{ActivityLogType.assign_tag_type.id} OR activity_type = #{ActivityLogType.unassign_tag_type.id}
                                  THEN 'Tag'
                                END)
                                ,
          activity_item_id = (CASE
                                WHEN (substring(old_item_id, '[a-zA-Z]+') is not null)
                                  THEN (select id from anglers where angler_id = old_item_id)
                                ELSE
                                  old_item_id::integer
                                END)
    SQL

    rename_column :activity_logs, :activity_type, :activity_log_type_id
  end
end

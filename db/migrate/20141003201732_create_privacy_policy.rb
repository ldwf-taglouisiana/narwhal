class CreatePrivacyPolicy < ActiveRecord::Migration
  def up
    add_column :blurbs, :privacy_policy, :text
  end

  def down
    remove_column :blurbs, :privacy_policy
  end
end

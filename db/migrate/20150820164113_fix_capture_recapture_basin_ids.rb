class FixCaptureRecaptureBasinIds < ActiveRecord::Migration
  def change
	execute <<-SQL
	UPDATE captures set basin_id = basins.gid
        FROM basins
        WHERE captures.geom IS NOT NULL AND  captures.basin_id IS NULL  AND ST_Within(captures.geom,basins.geom);

        UPDATE captures set sub_basin_id = "sub-basin".gid
        FROM "sub-basin"
        WHERE captures.geom IS NOT NULL AND  captures.sub_basin_id IS NULL  AND ST_Within(captures.geom,"sub-basin".geom);

        UPDATE recaptures set basin_id = basins.gid
        FROM basins
        WHERE recaptures.geom IS NOT NULL AND  recaptures.basin_id IS NULL  AND ST_Within(recaptures.geom,basins.geom);

        UPDATE recaptures set sub_basin_id = "sub-basin".gid
        FROM "sub-basin"
        WHERE recaptures.geom IS NOT NULL AND recaptures.sub_basin_id IS NULL AND ST_Within(recaptures.geom,"sub-basin".geom);

	REFRESH MATERIALIZED VIEW CONCURRENTLY locations;
	REFRESH MATERIALIZED VIEW CONCURRENTLY denormalized_captures;
	REFRESH MATERIALIZED VIEW CONCURRENTLY denormalized_recaptures;
	SQL
  end
end

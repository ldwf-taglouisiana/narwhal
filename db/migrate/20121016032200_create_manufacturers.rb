class CreateManufacturers < ActiveRecord::Migration
  def change
    create_table :manufacturers do |t|
      t.string :name
      t.string :street
      t.string :city
      t.string :state
      t.string :country
      t.string :zip
      t.string :contact
      t.string :phone
      t.string :fax
      t.string :email
      t.boolean :deleted

      t.timestamps
    end
  end
end

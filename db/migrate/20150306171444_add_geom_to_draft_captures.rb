class AddGeomToDraftCaptures < ActiveRecord::Migration
  def change
    execute <<-SQL
      SELECT AddGeometryColumn ('public','draft_captures','geom',4326,'POINT',2);
      UPDATE draft_captures SET geom = ST_SetSRID(ST_MakePoint(longitude,latitude),4326);
    SQL
  end
end

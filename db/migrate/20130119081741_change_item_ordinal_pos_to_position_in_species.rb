class ChangeItemOrdinalPosToPositionInSpecies < ActiveRecord::Migration
  def up
    rename_column :species, :item_ordinal_pos, "position"
  end

  def down
    rename_column :species, "position", :item_ordinal_pos
  end
end

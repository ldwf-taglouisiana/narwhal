class UpdateRecentVolunteerHoursToVersion3 < ActiveRecord::Migration
  def change
    create_view :recent_volunteer_hours, version: 3
  end
end

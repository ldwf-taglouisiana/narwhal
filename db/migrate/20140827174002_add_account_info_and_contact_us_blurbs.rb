class AddAccountInfoAndContactUsBlurbs < ActiveRecord::Migration
  def change

    add_column :blurbs, :account_info, :text
    add_column :blurbs, :contact_us, :text
  end
end

class AddRecentToRecapture < ActiveRecord::Migration
  def change
    add_column :recaptures, :recent, :boolean, null: false, default: true
    add_index :recaptures, :recent

    execute "UPDATE recaptures SET recent = false WHERE created_at < '#{15.days.ago.iso8601}'"
  end
end

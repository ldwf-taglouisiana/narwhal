class UpdateRecentRecapturesToVersion4 < ActiveRecord::Migration
  def change
    create_view :recent_recaptures, version: 4
  end
end

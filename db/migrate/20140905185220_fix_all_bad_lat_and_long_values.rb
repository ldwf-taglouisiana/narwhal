class FixAllBadLatAndLongValues < ActiveRecord::Migration
  def change
    execute <<-SQL
      update captures set latitude = null, longitude = null, entered_gps_type = 'NOGPS', geom = null where latitude = 0 and longitude = 0;
      update captures set latitude = null, longitude = null, entered_gps_type = 'NOGPS', geom = null where entered_gps_type is null and latitude is null or longitude is null;

      update recaptures set latitude = null, longitude = null, entered_gps_type = 'NOGPS', geom = null where latitude = 0 and longitude = 0;
      update recaptures set latitude = null, longitude = null, entered_gps_type = 'NOGPS', geom = null where entered_gps_type is null and latitude is null or longitude is null;

      update captures set entered_gps_type = 'D' where entered_gps_type is null;
      update recaptures set entered_gps_type = 'D' where entered_gps_type is null;

      UPDATE captures set geom = ST_SetSRID(ST_MakePoint(longitude,latitude),4326) where entered_gps_type != 'NOGPS';
      UPDATE recaptures set geom = ST_SetSRID(ST_MakePoint(longitude,latitude),4326) where entered_gps_type != 'NOGPS';
    SQL
  end
end

class AddTimeStampsToBasinTables < ActiveRecord::Migration
  def change
    add_timestamps(:basins, default: Time.now)
    add_timestamps("sub-basin", default: Time.now)

    change_column_null :basins, :updated_at, false
    change_column_null :basins, :created_at, false

    change_column_null "sub-basin", :updated_at, false
    change_column_null "sub-basin", :created_at, false
  end
end

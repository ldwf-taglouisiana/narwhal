class FixThePolymorficKeyForUserTagRequestInNotifications < ActiveRecord::Migration
  def change
    execute "UPDATE notifications SET notification_item_type = 'UserTagRequest' WHERE notification_item_type = 'UserTagRequests'"
  end
end

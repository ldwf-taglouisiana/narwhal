class AddLdwfEnteredColumnToVolunteerTimeLog < ActiveRecord::Migration
  def change
    add_column :volunteer_time_logs, :ldwf_entered, :boolean, default: false
    add_column :volunteer_time_logs, :ldwf_edited, :boolean, default: false
  end
end

class CreateHookTypeOptions < ActiveRecord::Migration
  def change
    create_table :hook_type_options do |t|
      t.string :hook_type_option

      t.timestamps
    end
  end
end

# This migration was auto-generated via `rake db:generate_trigger_migration'.
# While you can edit this file, any changes you make to the definitions here
# will be undone by the next auto-generated trigger migration.

class CreateTriggerTagsInsertUpdate < ActiveRecord::Migration
  def up
    create_trigger("tags_before_insert_update_row_tr", :generated => true, :compatibility => 1).
        on("tags").
        before(:insert, :update).
        declare("angler record;") do
      <<-SQL_ACTIONS
      -- get the associated angler for this tag
      select * into angler from anglers where angler_id = new.angler_id;

      new.search_vector :=
          setweight(to_tsvector('pg_catalog.english', coalesce(new.tag_no,'')), 'A')    ||
          setweight(to_tsvector('pg_catalog.english', coalesce(new.angler_id,'')), 'B')  ||
          setweight(to_tsvector('pg_catalog.english', coalesce(angler.last_name,'')), 'C') ||
          setweight(to_tsvector('pg_catalog.english', coalesce(angler.first_name,'')), 'C') ||
          setweight(to_tsvector('pg_catalog.english', coalesce(angler.email,'')), 'D');
      SQL_ACTIONS
    end
  end

  def down
    drop_trigger("tags_before_insert_update_row_tr", "tags", :generated => true)
  end
end

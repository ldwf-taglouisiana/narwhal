class UpdateRecentCapturesToVersion2 < ActiveRecord::Migration
  def change
    update_view :recent_captures, version: 2, revert_to_version: 1
  end
end

class CreateEmailJob < ActiveRecord::Migration
  def change
    create_table :email_jobs do |t|
      t.timestamps
    end
  end
end

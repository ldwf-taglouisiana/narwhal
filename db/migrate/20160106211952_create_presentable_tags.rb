class CreatePresentableTags < ActiveRecord::Migration
  def change
    create_view :presentable_tags
  end
end

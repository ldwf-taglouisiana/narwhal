class CreateMarinas < ActiveRecord::Migration
  def change
    create_table :marinas do |t|
      t.integer :site_no
      t.string :name
      t.boolean :closed
      t.string :city
      t.string :parish
      t.boolean :publish
      t.string :address
      t.float :latitude
      t.float :longitude
      t.text :web_address
      t.string :phone
      t.string :hours_of_operation
      t.boolean :can_get_fishing_license
      t.string :fee_amount
      t.boolean :has_bait
      t.boolean :has_ice
      t.boolean :has_food
      t.boolean :has_restrooms
      t.boolean :has_sewage_pump
      t.boolean :has_cert_scales
      t.string :facility_type
      t.boolean :has_cleaning_station
      t.string :fuel_type
      t.text :location_description

      t.timestamps
    end
  end
end

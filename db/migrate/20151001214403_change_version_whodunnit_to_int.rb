class ChangeVersionWhodunnitToInt < ActiveRecord::Migration
  def change
    change_column :versions, :whodunnit, 'bigint USING CAST(whodunnit AS bigint)'
  end
end

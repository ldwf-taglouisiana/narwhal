class AddTsVectorToDraftCaptures < ActiveRecord::Migration
  def up
    # add the vector column to the tags table
    add_column :draft_captures, :search_vector, :tsvector

    # set the tsvector column for the tags
    execute <<-SQL
      UPDATE draft_captures A
      SET search_vector =
        setweight(to_tsvector('pg_catalog.english', coalesce(B.tag_number, '')), 'A') ||
        setweight(to_tsvector('pg_catalog.english', coalesce(B.angler_id, '')), 'B')  ||
        setweight(to_tsvector('pg_catalog.english', coalesce(C.last_name, '')), 'C')  ||
        setweight(to_tsvector('pg_catalog.english', coalesce(C.first_name, '')), 'C') ||
        setweight(to_tsvector('pg_catalog.english', coalesce(C.email, '')), 'D')
      FROM draft_captures B
        LEFT JOIN anglers C on B.angler_id = C.angler_id
      WHERE A.id = B.id;
    SQL

    # add the index, after population for faster pre-population
    add_index :draft_captures, :search_vector, using: :gin

    # disables nulls for the tsvector column
    change_column_null :draft_captures, :search_vector, false
  end

  def down
    remove_column :draft_captures, :search_vector, :tsvector
  end
end

class CreateNewsItems < ActiveRecord::Migration
  def change
    create_table :news_items do |t|
      t.belongs_to :user
      t.text :content
      t.boolean :should_publish
      t.datetime  :should_publish_at
      t.string :title

      t.timestamps
    end
  end
end

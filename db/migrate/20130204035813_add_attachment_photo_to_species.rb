class AddAttachmentPhotoToSpecies < ActiveRecord::Migration
  def self.up
    change_table :species do |t|
      t.attachment :photo
    end
  end

  def self.down
    drop_attached_file :species, :photo
  end
end

class CreateAboutTaggingPrograms < ActiveRecord::Migration
  def change
    create_table :about_tagging_programs do |t|
      t.text :description

      t.timestamps
    end
  end
end

class CreateCaptures < ActiveRecord::Migration
  def change
    create_table :captures do |t|
      t.integer :angler_id
      t.integer :captain_id
      t.integer :tag_id
      t.datetime :capture_date
      t.integer :species_id
      t.float :length
      t.float :estimated_length
      t.text :location_description
      t.float :lat
      t.float :long
      t.integer :fish_condition_id
      t.integer :fish_tagged_in_id
      t.integer :landing_net_used_id
      t.integer :hook_type_id
      t.integer :bait_type_id
      t.integer :hook_removed_id
      t.integer :hook_barbed_id
      t.float :weight
      t.float :estimated_weight
      t.float :water_temperature
      t.float :water_salinity
      t.float :wind_speed
      t.integer :wind_direction_id
      t.text :comments
      t.boolean :confirmed
      t.boolean :verified
      t.datetime :entry_date
      t.boolean :deleted
      t.boolean :mailed_map
      t.boolean :recapture
      t.integer :recapture_disposition
      t.boolean :see_card
      t.integer :time_of_day

      t.timestamps
    end
  end
end

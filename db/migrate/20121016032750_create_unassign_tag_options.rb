class CreateUnassignTagOptions < ActiveRecord::Migration
  def change
    create_table :unassign_tag_options do |t|
      t.string :unassign_tag_option

      t.timestamps
    end
  end
end

class CreateSpecies < ActiveRecord::Migration
  def change
    create_table :species do |t|
      t.string :common_name
      t.string :species_code
      t.integer :item_ordinal_pos

      t.timestamps
    end
  end
end

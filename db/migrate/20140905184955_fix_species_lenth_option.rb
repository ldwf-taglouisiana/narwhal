class FixSpeciesLenthOption < ActiveRecord::Migration
  def change
    speckled_trout = Species.where(id: 2).first
    SpeciesLength.where(description: '> 27"'     , species: speckled_trout, position: 4).each do |s|
      s.description = '> 20"'
      s.save!
    end
  end
end

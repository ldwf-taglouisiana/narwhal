class AddNonRegisteredCaptureToBlurbs < ActiveRecord::Migration
  def change
    add_column :blurbs, :non_registered_capture, :text
  end
end

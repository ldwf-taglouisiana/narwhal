class AddRecentToVolunteerHours < ActiveRecord::Migration
  def change
    add_column :volunteer_time_logs, :recent, :boolean, null: false, default: true
    add_index :volunteer_time_logs, :recent

    execute "UPDATE volunteer_time_logs SET recent = false WHERE created_at < '#{1.months.ago.iso8601}'"
  end
end

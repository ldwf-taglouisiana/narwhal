class CreateFishEntryPhotos < ActiveRecord::Migration
  def change
    create_table :fish_entry_photos do |t|
      t.string :fish_entry_photoable_type, null: false
      t.bigint :fish_entry_photoable_id, null: false
      t.attachment :image
      t.timestamps null: false
    end
  end
end

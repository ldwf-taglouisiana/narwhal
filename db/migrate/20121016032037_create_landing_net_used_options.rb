class CreateLandingNetUsedOptions < ActiveRecord::Migration
  def change
    create_table :landing_net_used_options do |t|
      t.string :landing_net_used_option

      t.timestamps
    end
  end
end

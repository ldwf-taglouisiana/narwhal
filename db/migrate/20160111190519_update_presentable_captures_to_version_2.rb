class UpdatePresentableCapturesToVersion2 < ActiveRecord::Migration
  def up
    create_view :presentable_captures, version: 2
  end

  def down
    drop_view :presentable_captures
  end
end

class ChangeStatesToString < ActiveRecord::Migration
  def up
    Angler.all.each do |angler|
      if angler.state && angler.state.to_i != 0
        angler.update_attributes(:state => State.find(angler.state.to_i).state_abbr) 
      end
    end

  end

  def down
    raise
  end
end

class ClearOutOldNotificaitonsForTheNewView < ActiveRecord::Migration
  def change
    Notification.all.update_all(checked: true)
  end
end

class AddTsVectorToCaptures < ActiveRecord::Migration
  def up
    # add the vector column to the tags table
    add_column :captures, :search_vector, :tsvector

    # set the tsvector column for the tags
    execute <<-SQL
      UPDATE captures A
      SET search_vector =
        setweight(to_tsvector('pg_catalog.english', coalesce(D.tag_no, '')), 'A')     ||
        setweight(to_tsvector('pg_catalog.english', coalesce(A.angler_id, '')), 'B')  ||
        setweight(to_tsvector('pg_catalog.english', coalesce(C.last_name, '')), 'C')  ||
        setweight(to_tsvector('pg_catalog.english', coalesce(C.first_name, '')), 'C') ||
        setweight(to_tsvector('pg_catalog.english', coalesce(C.email, '')), 'D')
      FROM captures B
        LEFT JOIN anglers C on B.angler_id = C.angler_id
        LEFT JOIN tags D on B.tag_id = D.id
      WHERE A.id = B.id;
    SQL

    # add the index, after population for faster pre-population
    add_index :captures, :search_vector, using: :gin

    # disables nulls for the tsvector column
    change_column_null :captures, :search_vector, false
  end

  def down
    remove_column :captures, :search_vector, :tsvector
  end
end

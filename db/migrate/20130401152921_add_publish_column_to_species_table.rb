class AddPublishColumnToSpeciesTable < ActiveRecord::Migration
  def change
    add_column :species, :publish, :boolean
    add_column :species, :target, :boolean
  end
end

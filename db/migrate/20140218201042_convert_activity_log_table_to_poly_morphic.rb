class ConvertActivityLogTableToPolyMorphic < ActiveRecord::Migration
  def change

    change_table :activity_logs do |t|
      t.references :activity_item, :polymorphic => true
    end

  end
end

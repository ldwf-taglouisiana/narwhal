class CreateAnglerItems < ActiveRecord::Migration


  def up
    (query = '') << <<-SQL
      CREATE TABLE items (
        id bigserial PRIMARY KEY,
        name text NOT NULL,
        is_public boolean NOT NULL,
        created_at timestamp with time zone NOT NULL,
        updated_at timestamp with time zone NOT NULL    
      ); 

      CREATE TABLE angler_item_types (
        id bigserial PRIMARY KEY,
        name text NOT NULL,
        description text NOT NULL,
        created_at timestamp with time zone NOT NULL,
        updated_at timestamp with time zone NOT NULL    
      );  
      INSERT INTO angler_item_types (name,description,created_at,updated_at) VALUES ('Welcome Package','For new anglers.',now(),now());
      INSERT INTO angler_item_types (name,description,created_at,updated_at) VALUES ('10-Capture Incentive Item','For anglers with 10 verified captures.',now(),now());

      CREATE TABLE angler_items (
        id bigserial PRIMARY KEY,
        angler_id bigint NOT NULL REFERENCES anglers (id),
        item_id bigint NOT NULL REFERENCES items (id),
        angler_item_type_id bigint NOT NULL REFERENCES angler_item_types (id),
        requested_at timestamp with time zone,
        fulfilled_at timestamp with time zone,
        comment text,
        created_at timestamp with time zone NOT NULL,
        updated_at timestamp with time zone NOT NULL    
      );              
      ALTER TABLE anglers ADD COLUMN prefered_incentive_item_id bigint REFERENCES items(id);

      CREATE INDEX idx_angler_items_angler_id ON angler_items(angler_id);
      CREATE INDEX idx_angler_items_item_id ON angler_items(item_id);
      CREATE INDEX idx_angler_items_angler_item_type_id ON angler_items(angler_item_type_id);
    SQL
    ActiveRecord::Base.connection.execute(query)
    Item.create(name: 'Tagging Kit', is_public: false)
    Item.create(name: 'Generic T-Shirt', is_public: false)
    ShirtSizeOption.all.each do |shirt|
      Item.create(name: "T-Shirt (#{shirt.shirt_size_option})", is_public: true)
    end

  end

  def down
    (query = '') << <<-SQL
      ALTER TABLE anglers DROP COLUMN prefered_incentive_item_id;
      DROP TABLE angler_items;
      DROP TABLE angler_item_types;
      DROP TABLE items;
    SQL
    ActiveRecord::Base.connection.execute(query)
  end
end

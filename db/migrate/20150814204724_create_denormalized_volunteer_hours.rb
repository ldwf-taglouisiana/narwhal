class CreateDenormalizedVolunteerHours < ActiveRecord::Migration
  def up
    execute <<-SQL
      CREATE VIEW denormalized_volunteer_hours as (

       SELECT
            A.id,
            A.angler_id,
            concat(B.last_name, ', ', B.first_name) as "angler_name",
            A.verified AS verified,
            A.ldwf_entered AS ldwf_entered,
            A.ldwf_edited AS ldwf_edited,
            (A.end - A.start) as total_time,
            A.start,
            A.end,
            A.created_at,
            (
              SELECT
                count(*)
              FROM
                captures
              WHERE
                captures.angler_id = A.angler_id
                AND
                captures.verified IS TRUE
                AND
                (
                  captures.capture_date <@ tstzrange(A.start,A.end,'[]')
                )
            ) as "number_of_captures"
          FROM
            volunteer_time_logs A LEFT JOIN anglers B ON A.angler_id = B.angler_id
       )
    SQL
  end

  def down
    execute 'DROP VIEW denormalized_volunteer_hours;'
  end
end

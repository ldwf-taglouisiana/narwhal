class AddAnglerIdIndexToTags < ActiveRecord::Migration
  def change
    add_index :tags, :angler_id
    add_index :tags, :tag_no, unique: true
    add_index :tags, :tag_lot_id
  end
end

class UpdateRecentRecapturesToVersion3 < ActiveRecord::Migration
  def change
    update_view :recent_recaptures, version: 3, revert_to_version: 2
  end
end

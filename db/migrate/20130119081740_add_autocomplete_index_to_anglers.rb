class AddAutocompleteIndexToAnglers < ActiveRecord::Migration
  def self.up
    add_index :anglers, [:angler_id, :first_name, :last_name]
  end

  def self.down
    remove_index :anglers, [:angler_id, :first_name, :last_name]
  end
end

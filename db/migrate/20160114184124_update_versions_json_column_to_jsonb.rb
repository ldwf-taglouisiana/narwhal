class UpdateVersionsJsonColumnToJsonb < ActiveRecord::Migration
  def up
    execute <<-SQL
      ALTER TABLE versions
        ALTER COLUMN object_changes
        SET DATA TYPE jsonb
        USING object_changes::jsonb;
    SQL
  end

  def down
    execute <<-SQL
      ALTER TABLE versions
        ALTER COLUMN object_changes
        SET DATA TYPE json
        USING object_changes::json;
    SQL
  end
end

class AddIndexToBasinSubbasin < ActiveRecord::Migration
  def up
    execute <<-SQL
      CREATE INDEX idx_basins_geom ON "basins" USING GIST (geom);
      CREATE INDEX idx_sub_basins_geom ON "sub-basin" USING GIST (geom);
    SQL
  end

  def down
    execute <<-SQL
      DROP INDEX idx_basins_geom;
      DROP INDEX idx_sub_basins_geom;
    SQL
  end
end

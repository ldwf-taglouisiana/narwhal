class UpdatePresentableRecapturesToVersion2 < ActiveRecord::Migration
  def up
    create_view :presentable_recaptures, version: 2
  end

  def down
    drop_view :presentable_recaptures
  end
end

class AddSaveColumnToDraftCaptures < ActiveRecord::Migration
  def change
    add_column :draft_captures, :should_save, :boolean, :default => false
  end
end

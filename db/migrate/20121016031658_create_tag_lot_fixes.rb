class CreateTagLotFixes < ActiveRecord::Migration
  def change
    create_table :tag_lot_fixes do |t|
      t.integer :tag_lot_id
      t.string :tag_no

      t.timestamps
    end
  end
end

class ChangeItemColumnActivityLog < ActiveRecord::Migration
  def change
    rename_column :activity_logs, :item_id, :old_item_id
  end
end

class AddBasinAndSubBasinToCapturesAndRecaptures < ActiveRecord::Migration
  def change
    add_column :captures, :basin_id, :integer
    add_column :captures, :sub_basin_id, :integer
    add_foreign_key :captures, :basins, primary_key: 'gid'
    add_foreign_key :captures, 'sub-basin', column:'sub_basin_id', primary_key: 'gid'

    add_column :recaptures, :basin_id, :integer
    add_column :recaptures, :sub_basin_id, :integer
    add_foreign_key :recaptures, :basins, primary_key: 'gid'
    add_foreign_key :recaptures, 'sub-basin', column: 'sub_basin_id', primary_key: 'gid'
  end
end

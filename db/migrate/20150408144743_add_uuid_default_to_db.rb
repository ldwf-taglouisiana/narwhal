class AddUuidDefaultToDb < ActiveRecord::Migration
  def change
    execute <<-SQL
      CREATE EXTENSION "uuid-ossp";
      ALTER TABLE draft_captures ALTER COLUMN uuid SET DEFAULT uuid_generate_v4();
      UPDATE draft_captures SET uuid = uuid_generate_v4() WHERE uuid IS NULL;
    SQL
  end
end

class CreateExistingAngler < ActiveRecord::Migration

  def up
    ActiveRecord::Base.connection.execute("ALTER TABLE users ADD COLUMN is_existing_angler boolean;")
  end

  def down
    ActiveRecord::Base.connection.execute("ALTER TABLE users DROP COLUMN is_existing_angler;")
  end

end

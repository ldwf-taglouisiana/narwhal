class CreatePresentableRecaptures < ActiveRecord::Migration
  def change
    create_view :presentable_recaptures
  end
end

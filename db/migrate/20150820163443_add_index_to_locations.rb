class AddIndexToLocations < ActiveRecord::Migration
  def change
	execute <<-SQL
		DROP INDEX idx_locations_id;
		CREATE UNIQUE INDEX idx_locations_id on locations (id);
	SQL
  end
end

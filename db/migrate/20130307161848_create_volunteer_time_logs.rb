class CreateVolunteerTimeLogs < ActiveRecord::Migration
  def change
    create_table :volunteer_time_logs do |t|
      t.datetime :start
      t.datetime :end
      t.boolean :verified
      t.string :angler_id

      t.timestamps
    end
  end
end

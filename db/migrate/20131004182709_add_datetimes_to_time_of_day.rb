class AddDatetimesToTimeOfDay < ActiveRecord::Migration
  def up
    add_column :time_of_day_options, :start_time, :datetime
    add_column :time_of_day_options, :end_time, :datetime

    start = Chronic.parse('12:01 AM')
    end_t = Chronic.parse('3:00 AM')

    vol = TimeOfDayOption.order('id asc').all
    vol.each do |option|
      option.start_time = start
      option.end_time = end_t
      option.save!
      start = start + (60 * 60 * 3)
      end_t = end_t + (60 * 60 * 3)
    end




  end
  def down
    remove_column :time_of_day_options, :start_time
    remove_column :time_of_day_options,:end_time
  end
end

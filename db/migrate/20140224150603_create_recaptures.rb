class CreateRecaptures < ActiveRecord::Migration
  def self.up
    create_table :recaptures do |t|
      t.datetime :capture_date, null: false
      t.float :length
      t.text :location_description
      t.float :latitude
      t.float :longitude
      t.text :comments
      t.boolean :verified, default: false
      t.boolean :user_capture, default: false
      t.string :entered_gps_type

      # this will be the reference to anglers, not using the anglers.id field
      t.string :angler_id, null: false

      t.references :tag
      t.references :species
      t.references :fish_condition_option, null: true
      t.references :recapture_disposition, null: true
      t.references :time_of_day_option, null: true

      t.timestamps
    end

    # add the geometry column
    execute <<-SQL
      SELECT AddGeometryColumn ('public','recaptures','geom',4326,'POINT',2);
    SQL

    # add the FK constraints
    execute <<-SQL
      ALTER TABLE recaptures
      ADD CONSTRAINT recap_angler_fk
      FOREIGN KEY (angler_id)
      REFERENCES anglers (angler_id);

      ALTER TABLE recaptures
      ADD CONSTRAINT recap_tag_fk
      FOREIGN KEY (tag_id)
      REFERENCES tags (id);

      ALTER TABLE recaptures
      ADD CONSTRAINT recap_species_fk
      FOREIGN KEY (species_id)
      REFERENCES species (id);

      ALTER TABLE recaptures
      ADD CONSTRAINT recap_fish_cond_fk
      FOREIGN KEY (fish_condition_option_id)
      REFERENCES fish_condition_options (id);

      ALTER TABLE recaptures
      ADD CONSTRAINT recap_recap_dis_fk
      FOREIGN KEY (recapture_disposition_id)
      REFERENCES recapture_dispositions (id);

      ALTER TABLE recaptures
      ADD CONSTRAINT recap_time_day_fk
      FOREIGN KEY (time_of_day_option_id)
      REFERENCES time_of_day_options (id);
    SQL

    # add the uniqueness constraints
    execute <<-SQL
      -- prevent duplicate captures
      ALTER TABLE recaptures
      ADD CONSTRAINT recap_unique_fk
      UNIQUE (tag_id, capture_date, time_of_day_option_id, angler_id, species_id, fish_condition_option_id, latitude, longitude, entered_gps_type)
    SQL


    add_index(:recaptures, :tag_id)

  end

  def self.down
    execute <<-SQL
      ALTER TABLE recaptures
      DROP CONSTRAINT recap_angler_fk;

      ALTER TABLE recaptures
      DROP CONSTRAINT recap_tag_fk;

      ALTER TABLE recaptures
      DROP CONSTRAINT recap_species_fk;

      ALTER TABLE recaptures
      DROP CONSTRAINT recap_fish_cond_fk;

      ALTER TABLE recaptures
      DROP CONSTRAINT recap_recap_dis_fk;

      ALTER TABLE recaptures
      DROP CONSTRAINT recap_time_day_fk;

      ALTER TABLE recaptures
      DROP CONSTRAINT recap_unique_fk;
    SQL

    remove_index(:recaptures, :name => 'index_recaptures_on_tag_id')

    drop_table :recaptures

  end
end

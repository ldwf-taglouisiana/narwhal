class CreateAnglers < ActiveRecord::Migration
  def change
    create_table :anglers do |t|
      t.integer :lag_no
      t.integer :law_no
      t.string :first_name
      t.string :last_name
      t.string :street
      t.string :suite
      t.string :city
      t.string :state
      t.string :zip
      t.string :phone_number_1
      t.integer :phone_number_1_type
      t.string :phone_number_2
      t.integer :phone_number_2_type
      t.string :phone_number_3
      t.integer :phone_number_3_type
      t.string :phone_number_4
      t.integer :phone_number_4_type
      t.string :email
      t.integer :tag_end_user_type
      t.integer :shirt_size
      t.boolean :deleted
      t.integer :lax_no
      t.string :email_2
      t.string :user_name
      t.text :comments

      t.timestamps
    end
  end
end

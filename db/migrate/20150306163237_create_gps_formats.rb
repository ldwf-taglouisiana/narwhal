class CreateGpsFormats < ActiveRecord::Migration
  def up

    # create the format table
    create_table :gps_formats do |t|
      t.string :format_type, null: false
      t.text :format_string, null: false

      t.timestamps
    end

    # add the unique index for the formats
    add_index(:gps_formats, :format_type, :unique => true)

    # add the formats
    GPSFormat.where(format_type: 'D', format_string: "D.DDDD" ).first_or_create!
    GPSFormat.where(format_type: 'DM', format_string: "D° M.MMM''" ).first_or_create!
    GPSFormat.where(format_type: 'DMS', format_string: "D° M'' S.SS\"" ).first_or_create!
    GPSFormat.where(format_type: 'NOGPS', format_string: "" ).first_or_create!

    # create the foreign keys for captures, recaptures, draft_captures
    execute <<-SQL
      ALTER TABLE captures ADD CONSTRAINT fk_captures_gps_type FOREIGN KEY (entered_gps_type) REFERENCES gps_formats (format_type);
      ALTER TABLE recaptures ADD CONSTRAINT fk_recaptures_gps_type FOREIGN KEY (entered_gps_type) REFERENCES gps_formats (format_type);
      ALTER TABLE draft_captures ADD CONSTRAINT fk_draft_captures_gps_type FOREIGN KEY (entered_gps_type) REFERENCES gps_formats (format_type);
    SQL
  end

  def down

    execute <<-SQL
      ALTER TABLE captures DROP CONSTRAINT fk_captures_gps_type;
      ALTER TABLE recaptures DROP CONSTRAINT fk_recaptures_gps_type;
      ALTER TABLE draft_captures DROP CONSTRAINT fk_draft_captures_gps_type;
    SQL

    remove_index :gps_formats, :format_type

    drop_table :gps_formats
  end
end

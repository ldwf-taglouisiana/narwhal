class UpdateRecentCapturesToVersion3 < ActiveRecord::Migration
  def change
    create_view :recent_captures, version: 3
  end
end

# This migration was auto-generated via `rake db:generate_trigger_migration'.
# While you can edit this file, any changes you make to the definitions here
# will be undone by the next auto-generated trigger migration.

class CreateTriggersMultipleTables < ActiveRecord::Migration
  def up
    create_trigger("users_before_insert_update_row_tr", :generated => true, :compatibility => 1).
        on("users").
        before(:insert, :update).
        declare("angler record;") do
      <<-SQL_ACTIONS
      SELECT * INTO angler FROM anglers WHERE angler_id = new.angler_id;
      new.search_vector :=
        setweight(to_tsvector('pg_catalog.english', coalesce(email, '')), 'A')      ||
        setweight(to_tsvector('pg_catalog.english', coalesce(angler_id, '')), 'A')  ||
        setweight(to_tsvector('pg_catalog.english', coalesce(last_name, '')), 'B')  ||
        setweight(to_tsvector('pg_catalog.english', coalesce(first_name, '')), 'B') ||
        setweight(to_tsvector('pg_catalog.english', coalesce(angler.email, '')), 'C')      ||
        setweight(to_tsvector('pg_catalog.english', coalesce(angler.angler_id, '')), 'C')  ||
        setweight(to_tsvector('pg_catalog.english', coalesce(angler.last_name, '')), 'D')  ||
        setweight(to_tsvector('pg_catalog.english', coalesce(angler.first_name, '')), 'D');
      SQL_ACTIONS
    end

    create_trigger("anglers_before_insert_update_row_tr", :generated => true, :compatibility => 1).
        on("anglers").
        before(:insert, :update) do
      <<-SQL_ACTIONS
      new.search_vector :=
        setweight(to_tsvector('pg_catalog.english', coalesce(angler_id, '')), 'A')  ||
        setweight(to_tsvector('pg_catalog.english', coalesce(last_name, '')), 'B')  ||
        setweight(to_tsvector('pg_catalog.english', coalesce(first_name, '')), 'B') ||
        setweight(to_tsvector('pg_catalog.english', coalesce(email, '')), 'C');
      SQL_ACTIONS
    end

    create_trigger("captures_before_insert_update_row_tr", :generated => true, :compatibility => 1).
        on("captures").
        before(:insert, :update).
        declare("angler record; tag record;") do
      <<-SQL_ACTIONS
      -- get the associated angler for this tag
      select * into angler from anglers where angler_id = new.angler_id;
      select * into tag from tags where id = new.tag_id;

      new.search_vector :=
        setweight(to_tsvector('pg_catalog.english', coalesce(tag.tag_no, '')), 'A')     ||
        setweight(to_tsvector('pg_catalog.english', coalesce(new.angler_id, '')), 'B')  ||
        setweight(to_tsvector('pg_catalog.english', coalesce(angler.last_name, '')), 'C')  ||
        setweight(to_tsvector('pg_catalog.english', coalesce(angler.first_name, '')), 'C') ||
        setweight(to_tsvector('pg_catalog.english', coalesce(angler.email, '')), 'D');
      SQL_ACTIONS
    end

    create_trigger("recaptures_before_insert_update_row_tr", :generated => true, :compatibility => 1).
        on("recaptures").
        before(:insert, :update).
        declare("angler record; tag record;") do
      <<-SQL_ACTIONS
      -- get the associated angler for this tag
      select * into angler from anglers where angler_id = new.angler_id;
      select * into tag from tags where id = new.tag_id;

      new.search_vector :=
        setweight(to_tsvector('pg_catalog.english', coalesce(tag.tag_no, '')), 'A')     ||
        setweight(to_tsvector('pg_catalog.english', coalesce(new.angler_id, '')), 'B')  ||
        setweight(to_tsvector('pg_catalog.english', coalesce(angler.last_name, '')), 'C')  ||
        setweight(to_tsvector('pg_catalog.english', coalesce(angler.first_name, '')), 'C') ||
        setweight(to_tsvector('pg_catalog.english', coalesce(angler.email, '')), 'D');
      SQL_ACTIONS
    end

    create_trigger("volunteer_time_logs_before_insert_update_row_tr", :generated => true, :compatibility => 1).
        on("volunteer_time_logs").
        before(:insert, :update).
        declare("angler record;") do
      <<-SQL_ACTIONS
      SELECT * INTO angler FROM anglers WHERE angler_id = new.angler_id;
      new.search_vector :=
        setweight(to_tsvector('pg_catalog.english', coalesce(new.angler_id, '')), 'A')  ||
        setweight(to_tsvector('pg_catalog.english', coalesce(angler.email, '')), 'B')      ||
        setweight(to_tsvector('pg_catalog.english', coalesce(angler.first_name, '')), 'C') ||
        setweight(to_tsvector('pg_catalog.english', coalesce(angler.last_name, '')), 'B');
      SQL_ACTIONS
    end
  end

  def down
    drop_trigger("users_before_insert_update_row_tr", "users", :generated => true)

    drop_trigger("anglers_before_insert_update_row_tr", "anglers", :generated => true)

    drop_trigger("captures_before_insert_update_row_tr", "captures", :generated => true)

    drop_trigger("recaptures_before_insert_update_row_tr", "recaptures", :generated => true)

    drop_trigger("volunteer_time_logs_before_insert_update_row_tr", "volunteer_time_logs", :generated => true)
  end
end

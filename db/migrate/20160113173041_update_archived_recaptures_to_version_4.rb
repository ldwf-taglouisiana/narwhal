class UpdateArchivedRecapturesToVersion4 < ActiveRecord::Migration
  def up
    create_view :archived_recaptures, version: 4, materialized: true

    add_index :archived_recaptures, :id, unique: true
    add_index :archived_recaptures, :capture_id
    add_index :archived_recaptures, :verified

    execute <<-SQL
      CREATE INDEX idx_archived_recaptures_where_tag_yer ON archived_recaptures USING GIST (year_id, recapture_angler_id, tag_number, location_description, comments, time_of_day_option_id, fish_condition_option_id, length, species_id, capture_date, basin_id, sub_basin_id);
    SQL
  end

  def down
    # drop_view :presentable_recaptures
    drop_view :archived_recaptures, revert_to_version: 2, materialized: true
    drop_view :recent_recaptures

    create_view :archived_recaptures, version: 3, materialized: true
    create_view :recent_recaptures, version: 3
    create_view :presentable_recaptures, version: 2
  end
end

class FixBadEntriesFromVolunteerTimeLogs < ActiveRecord::Migration
  def change
    execute <<-SQL
      -- move the bad entries to a temp table
      CREATE TABLE volunteer_time_logs_bad AS SELECT * FROM volunteer_time_logs B WHERE B.start >= B.end OR B.start < (now() -  '4 years'::interval) OR B.end < (now() -  '4 years'::interval);
      -- delete the bad entries from the main table
      DELETE  FROM volunteer_time_logs B WHERE B.start >= B.end OR B.start < (now() -  '4 years'::interval) OR B.end < (now() -  '4 years'::interval);
      -- ensure that the start time is less than the end time
      ALTER TABLE volunteer_time_logs ADD CONSTRAINT chk_vol_time_log_start_end CHECK("start" < "end");
      -- ensure that the times do not overlap for a given angler
      ALTER TABLE volunteer_time_logs ADD CONSTRAINT "vol_time_logs_no_overlapping_time_ranges" EXCLUDE USING gist (tstzrange(start, "end") WITH &&, angler_id WITH =);
    SQL
  end
end
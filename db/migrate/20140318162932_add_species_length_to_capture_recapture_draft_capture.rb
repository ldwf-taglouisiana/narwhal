class AddSpeciesLengthToCaptureRecaptureDraftCapture < ActiveRecord::Migration
  def change
    add_column :captures, :species_length_id, :integer
    add_column :recaptures, :species_length_id, :integer
    add_column :draft_captures, :species_length_id, :integer
  end
end

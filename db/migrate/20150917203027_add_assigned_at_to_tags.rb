class AddAssignedAtToTags < ActiveRecord::Migration
  def up
    add_column :tags, :assigned_at, :datetime

    execute <<-SQL
      UPDATE tags set assigned_at = updated_at where angler_id is not null;

      DROP MATERIALIZED VIEW denormalized_tags;

      CREATE MATERIALIZED VIEW denormalized_tags AS (
          WITH selected_tags AS (
          SELECT
            tags.*,
            (COUNT(recaptures.id) + COUNT(captures.id)) > 0      AS "used"
          FROM
            tags
            left join recaptures on tags.id = recaptures.tag_id
            left join captures on tags.id = captures.tag_id
          GROUP BY
            tags.id
           )
          SELECT
            tags.*,
            concat(anglers.first_name, ' ' , anglers.last_name) as angler_name,
            to_char(tags.created_at, 'MM/DD/YYYY') as entered

          FROM
            selected_tags tags
            left join anglers on tags.angler_id = anglers.angler_id

      );

      CREATE UNIQUE INDEX idx_denormalized_tag_id ON denormalized_tags (id);

    SQL
  end

  def down
    remove_column :tags, :assigned_at
    execute 'DROP MATERIALIZED VIEW denormalized_tags; '
  end
end

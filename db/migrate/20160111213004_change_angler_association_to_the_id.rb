class ChangeAnglerAssociationToTheId < ActiveRecord::Migration
  def up
    execute <<-SQL
      ALTER TABLE anglers DROP CONSTRAINT anglers_pkey CASCADE;
      ALTER TABLE anglers ADD PRIMARY KEY (id);

      DROP TRIGGER captures_before_insert_update_row_tr ON captures;
      DROP TRIGGER draft_captures_before_insert_update_row_tr ON draft_captures;
      DROP TRIGGER recaptures_before_insert_update_row_tr ON recaptures;
      DROP TRIGGER tags_before_insert_update_row_tr ON tags;
      DROP TRIGGER users_before_insert_update_row_tr ON users;
      DROP TRIGGER volunteer_time_logs_before_insert_update_row_tr ON volunteer_time_logs;
    SQL

    drop_view :presentable_captures
    drop_view :presentable_recaptures
    drop_view :presentable_tags
    drop_view :presentable_volunteer_hours

    drop_view :archived_captures, materialized: true
    drop_view :archived_recaptures, materialized: true
    drop_view :archived_tags, materialized: true
    drop_view :archived_volunteer_hours, materialized: true
    drop_view :searches, materialized: true

    drop_view :recent_captures
    drop_view :recent_recaptures
    drop_view :recent_tags
    drop_view :recent_volunteer_hours
    drop_view :tag_assignments

    table_names = %w(captures recaptures tags volunteer_time_logs draft_captures users user_tag_requests)

    # create the inner queries to get the number of rows in each column that has the given angler_id
    table_names.each do |table|
      table_sym = table.to_sym
      rename_column table_sym, :angler_id, :angler_number
      add_column table_sym, :angler_id, :bigint

      execute <<-SQL
        UPDATE #{table} A SET angler_id = anglers.id
        FROM anglers
        WHERE A.angler_number = anglers.angler_id
      SQL

      remove_column table_sym, :angler_number
      add_foreign_key table_sym, :anglers
      change_column_null(table_sym, :angler_id, false) unless (table_sym == :tags || table_sym == :users)
    end

    # need to go through and fix the angler_id values in the versions table.
    Version.where("object_changes::jsonb ? 'angler_id' AND item_type != 'Angler'").each do |v|
      json = v.object_changes
      json["angler_id"] = json["angler_id"].map { |t| Angler.where(angler_id: t).first.try(:id) }
      v.update_column(:object_changes, json)
    end
  end

  def down
    table_names = %w(captures recaptures tags volunteer_time_logs draft_captures users user_tag_requests)

    # create the inner queries to get the number of rows in each column that has the given angler_id
    table_names.each do |table|
      table_sym = table.to_sym
      rename_column table_sym, :angler_id, :angler_old
      add_column table_sym, :angler_id, :text

      execute <<-SQL
        UPDATE #{table} A SET angler_id = anglers.angler_id
        FROM anglers
        WHERE A.angler_old = anglers.id;
      SQL

      add_foreign_key table_sym, :anglers, primary_key: :angler_id
      change_column_null(table_sym, :angler_id, false) unless (table_sym == :tags || table_sym == :users)
      remove_column table_sym, :angler_old
    end

    create_trigger("users_before_insert_update_row_tr", :generated => true, :compatibility => 1).
        on("users").
        before(:insert, :update).
        declare("angler record;") do
      <<-SQL_ACTIONS
      SELECT * INTO angler FROM anglers WHERE angler_id = new.angler_id;
      new.search_vector :=
        setweight(to_tsvector('pg_catalog.english', coalesce(new.email, '')), 'A')      ||
        setweight(to_tsvector('pg_catalog.english', coalesce(new.angler_id, '')), 'A')  ||
        setweight(to_tsvector('pg_catalog.english', coalesce(new.last_name, '')), 'B')  ||
        setweight(to_tsvector('pg_catalog.english', coalesce(new.first_name, '')), 'B') ||
        setweight(to_tsvector('pg_catalog.english', coalesce(angler.email, '')), 'C')      ||
        setweight(to_tsvector('pg_catalog.english', coalesce(angler.angler_id, '')), 'C')  ||
        setweight(to_tsvector('pg_catalog.english', coalesce(angler.last_name, '')), 'D')  ||
        setweight(to_tsvector('pg_catalog.english', coalesce(angler.first_name, '')), 'D');
      SQL_ACTIONS
    end

    create_trigger("draft_captures_before_insert_update_row_tr", :generated => true, :compatibility => 1).
        on("draft_captures").
        before(:insert, :update).
        declare("angler record;") do
      <<-SQL_ACTIONS
      -- get the associated angler for this tag
      select * into angler from anglers where angler_id = new.angler_id;

      new.search_vector :=
        setweight(to_tsvector('pg_catalog.english', coalesce(new.tag_number, '')), 'A') ||
        setweight(to_tsvector('pg_catalog.english', coalesce(new.angler_id, '')), 'B')  ||
        setweight(to_tsvector('pg_catalog.english', coalesce(angler.last_name, '')), 'C')  ||
        setweight(to_tsvector('pg_catalog.english', coalesce(angler.first_name, '')), 'C') ||
        setweight(to_tsvector('pg_catalog.english', coalesce(angler.email, '')), 'D');
      SQL_ACTIONS
    end

    create_trigger("captures_before_insert_update_row_tr", :generated => true, :compatibility => 1).
        on("captures").
        before(:insert, :update).
        declare("angler record; tag record;") do
      <<-SQL_ACTIONS
      -- get the associated angler for this tag
      select * into angler from anglers where angler_id = new.angler_id;
      select * into tag from tags where id = new.tag_id;

      new.search_vector :=
        setweight(to_tsvector('pg_catalog.english', coalesce(tag.tag_no, '')), 'A')     ||
        setweight(to_tsvector('pg_catalog.english', coalesce(new.angler_id, '')), 'B')  ||
        setweight(to_tsvector('pg_catalog.english', coalesce(angler.last_name, '')), 'C')  ||
        setweight(to_tsvector('pg_catalog.english', coalesce(angler.first_name, '')), 'C') ||
        setweight(to_tsvector('pg_catalog.english', coalesce(angler.email, '')), 'D');
      SQL_ACTIONS
    end

    create_trigger("recaptures_before_insert_update_row_tr", :generated => true, :compatibility => 1).
        on("recaptures").
        before(:insert, :update).
        declare("angler record; tag record;") do
      <<-SQL_ACTIONS
      -- get the associated angler for this tag
      select * into angler from anglers where angler_id = new.angler_id;
      select * into tag from tags where id = new.tag_id;

      new.search_vector :=
        setweight(to_tsvector('pg_catalog.english', coalesce(tag.tag_no, '')), 'A')     ||
        setweight(to_tsvector('pg_catalog.english', coalesce(new.angler_id, '')), 'B')  ||
        setweight(to_tsvector('pg_catalog.english', coalesce(angler.last_name, '')), 'C')  ||
        setweight(to_tsvector('pg_catalog.english', coalesce(angler.first_name, '')), 'C') ||
        setweight(to_tsvector('pg_catalog.english', coalesce(angler.email, '')), 'D');
      SQL_ACTIONS
    end

    create_trigger("volunteer_time_logs_before_insert_update_row_tr", :generated => true, :compatibility => 1).
        on("volunteer_time_logs").
        before(:insert, :update).
        declare("angler record;") do
      <<-SQL_ACTIONS
      SELECT * INTO angler FROM anglers WHERE angler_id = new.angler_id;
      new.search_vector :=
        setweight(to_tsvector('pg_catalog.english', coalesce(new.angler_id, '')), 'A')  ||
        setweight(to_tsvector('pg_catalog.english', coalesce(angler.email, '')), 'B')      ||
        setweight(to_tsvector('pg_catalog.english', coalesce(angler.first_name, '')), 'C') ||
        setweight(to_tsvector('pg_catalog.english', coalesce(angler.last_name, '')), 'B');
      SQL_ACTIONS
    end

    create_trigger("tags_before_insert_update_row_tr", :generated => true, :compatibility => 1).
        on("tags").
        before(:insert, :update).
        declare("angler record;") do
      <<-SQL_ACTIONS
      -- get the associated angler for this tag
      select * into angler from anglers where angler_id = new.angler_id;

      new.search_vector :=
          setweight(to_tsvector('pg_catalog.english', coalesce(new.tag_no,'')), 'A')    ||
          setweight(to_tsvector('pg_catalog.english', coalesce(new.angler_id,'')), 'B')  ||
          setweight(to_tsvector('pg_catalog.english', coalesce(angler.last_name,'')), 'C') ||
          setweight(to_tsvector('pg_catalog.english', coalesce(angler.first_name,'')), 'C') ||
          setweight(to_tsvector('pg_catalog.english', coalesce(angler.email,'')), 'D');
      SQL_ACTIONS
    end
  end
end

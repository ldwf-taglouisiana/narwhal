class AddUuidToUserTagRequestAndVolunteerTimeLogs < ActiveRecord::Migration
  def change
	add_column :volunteer_time_logs, :uuid, :string
	add_column :user_tag_requests, :uuid, :string
	add_index :volunteer_time_logs, [:uuid, :angler_id], unique: true
	add_index :user_tag_requests, [:uuid, :angler_id], unique: true
  end
end

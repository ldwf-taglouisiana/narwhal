class AddColumnToUserCouriers < ActiveRecord::Migration
  def change
    add_column :user_couriers, :notification_topic_id, :integer
  end
end

class AddAttachmentPhotoToMarinas < ActiveRecord::Migration
  def self.up
    change_table :marinas do |t|
      t.attachment :photo
    end
  end

  def self.down
    drop_attached_file :marinas, :photo
  end
end

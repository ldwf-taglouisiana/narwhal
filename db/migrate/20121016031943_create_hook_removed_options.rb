class CreateHookRemovedOptions < ActiveRecord::Migration
  def change
    create_table :hook_removed_options do |t|
      t.string :hook_removed_option

      t.timestamps
    end
  end
end

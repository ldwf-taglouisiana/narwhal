class PopulateVersionsTable < ActiveRecord::Migration
  def up
    execute <<-SQL
      ALTER TABLE versions ADD COLUMN activity_type TEXT;
      ALTER TABLE versions ADD COLUMN object_changes JSON;

      ALTER TABLE versions ALTER COLUMN object TYPE JSON USING object::JSON;

      INSERT INTO versions (item_id, item_type, event, activity_type, whodunnit, created_at) (
        SELECT
          activity_logs.activity_item_id,
          activity_logs.activity_item_type,
          CASE
            WHEN activity_log_types.activity_type ~ 'unassignment'
            THEN
              'update'
            WHEN activity_log_types.activity_type ~ 'assignment'
            THEN
              'update'
            WHEN activity_log_types.activity_type ~ 'create'
            THEN
              'create'
            ELSE
            'update'
          END,
            CASE
            WHEN activity_log_types.activity_type ~ 'unassignment'
            THEN
              'unassignment'
            WHEN activity_log_types.activity_type ~ 'assignment'
            THEN
              'assignment'
            WHEN activity_log_types.activity_type ~ 'create'
            THEN
              'create'
            ELSE
            'update'
          END,
          activity_logs.user_id::TEXT,
          activity_logs.created_at
        FROM
          activity_logs
          LEFT JOIN activity_log_types ON activity_log_types.id = activity_logs.activity_log_type_id
        ORDER BY
          created_at ASC
      );
    SQL
  end

  def down
    # do nothing
  end
end

class AddGeomToCaptures < ActiveRecord::Migration
  def change    
    change_table :captures do |t|
      t.column :geom, :geometry
      t.integer :old_angler_id
      # t.index :index_captures_geom, :spatial => true
    end
  end
end

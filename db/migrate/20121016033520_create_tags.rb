class CreateTags < ActiveRecord::Migration
  def change
    create_table :tags do |t|
      t.integer :angler_id
      t.string :tag_no
      t.integer :tag_lot_id
      t.integer :tag_request_id
      t.boolean :deleted
      t.integer :unassign_option

      t.timestamps
    end
  end
end

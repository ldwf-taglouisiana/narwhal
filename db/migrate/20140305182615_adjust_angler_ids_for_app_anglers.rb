class AdjustAnglerIdsForAppAnglers < ActiveRecord::Migration
  def change

    execute <<-SQL
       update anglers set id = nextval('anglers_id_seq') where id is null;
       alter table anglers alter column id set not null;
       alter table  anglers add constraint angler_id_uniq unique (id);
    SQL
  end
end

class CreatePhotos < ActiveRecord::Migration
  def self.up
    drop_table :photos

    create_table :photos do |t|
      t.attachment :image
      t.references :imageable, polymorphic: true
      t.timestamps
    end
  end

  def self.down
    remove_attachment :photos, :image

    drop_table :photos
  end
end

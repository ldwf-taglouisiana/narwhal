class CreateFrontPagePhotoStreamItems < ActiveRecord::Migration
  def change
    create_table :front_page_photo_stream_items do |t|
      t.boolean :featured
      t.timestamps
    end
  end
end

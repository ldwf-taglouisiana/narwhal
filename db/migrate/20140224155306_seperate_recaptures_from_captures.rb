class SeperateRecapturesFromCaptures < ActiveRecord::Migration
  def self.up

    execute <<-SQL
      update captures
      set fish_condition_id = null
      where fish_condition_id = 0;

      update captures
      set time_of_day = null
      where time_of_day = 0;

      update captures
      set species_id = null
      where species_id = 0;

      update captures
      set verified = true
      where verified is null or created_at <= (now() - '7 days'::interval);
    SQL

    execute <<-SQL
      update activity_logs
      set activity_item_type = 'Recapture'
      where activity_item_type = 'Capture'
            AND activity_item_id in ( select id from captures where recapture is true)
    SQL

    execute <<-SQL
      INSERT INTO recaptures (capture_date, tag_id, time_of_day_option_id, angler_id, species_id, fish_condition_option_id, latitude, longitude, location_description, comments, entered_gps_type, user_capture, verified, geom, created_at, updated_at) (
        select  capture_date,
                tag_id,
                time_of_day,
                angler_id,
                species_id,
                fish_condition_id,
                lat,
                long,
                location_description,
                comments,
                entered_gps_type,
                user_capture,
                verified,
                geom,
                created_at,
                updated_at
        from captures
        where recapture is true
      );
    SQL

    execute <<-SQL
      delete from captures where recapture is true;
    SQL
  end

  def self.down

  end
end



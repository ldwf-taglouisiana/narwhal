class CreateDenormalizedTag < ActiveRecord::Migration
  def up
    execute <<-SQL
      ---

      CREATE MATERIALIZED VIEW denormalized_tags AS (
          WITH selected_tags AS (
          SELECT
            tags.*,
            (COUNT(recaptures.id) + COUNT(captures.id)) > 0      AS "used"
          FROM
            tags
            left join recaptures on tags.id = recaptures.tag_id
            left join captures on tags.id = captures.tag_id
          GROUP BY
            tags.id
           )
          SELECT
            tags.*,
            concat(anglers.first_name, ' ' , anglers.last_name) as angler_name,
            CASE WHEN tags.angler_id IS NULL THEN NULL ELSE to_char(tags.updated_at, 'MM/DD/YYYY') END as assigned_at_formated,
            CASE WHEN tags.angler_id IS NULL THEN NULL ELSE tags.updated_at END as assigned_at,
            to_char(tags.created_at, 'MM/DD/YYYY') as entered

          FROM
            selected_tags tags
            left join anglers on tags.angler_id = anglers.angler_id

      );

      CREATE UNIQUE INDEX idx_denormalized_tag_id ON denormalized_tags (id);

    SQL
  end

  def down
    execute "DROP MATERIALIZED VIEW denormalized_tags; "
    # do not need a down, since the up rebuilds anyways
  end
end

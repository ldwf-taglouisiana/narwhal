class UpdatePresentableRecapturesToVersion3 < ActiveRecord::Migration
  def change
    create_view :presentable_recaptures, version: 3
  end
end

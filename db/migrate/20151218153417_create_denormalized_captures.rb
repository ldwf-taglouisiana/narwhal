class CreateDenormalizedCaptures < ActiveRecord::Migration
  def change
    execute 'DROP MATERIALIZED VIEW IF EXISTS denormalized_captures;'
    create_view :archived_captures, materialized: true
    add_index :archived_captures, :id, unique: true
    add_index :archived_captures, :verified
    add_index :archived_captures, :geom

    execute <<-SQL
      CREATE INDEX idx_archived_captures_where_tag_year ON archived_captures USING GIST (year_id, angler_id, tag_number, location_description, comments, time_of_day_option_id, fish_condition_option_id, length, species_id, capture_date, basin_id, sub_basin_id);
    SQL
  end
end

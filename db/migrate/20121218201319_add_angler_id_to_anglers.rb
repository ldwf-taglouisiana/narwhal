class AddAnglerIdToAnglers < ActiveRecord::Migration
  execute "ALTER TABLE anglers DROP CONSTRAINT anglers_pkey"
  def change
    change_table :anglers do |t|
      t.string :angler_id
    end
  end
end

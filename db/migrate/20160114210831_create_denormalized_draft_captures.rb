class CreateDenormalizedDraftCaptures < ActiveRecord::Migration
  def change
    create_view :denormalized_draft_captures
  end
end

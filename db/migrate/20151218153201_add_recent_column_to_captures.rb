class AddRecentColumnToCaptures < ActiveRecord::Migration
  def change
    add_column :captures, :recent, :boolean, default: false, null: false
  end
end

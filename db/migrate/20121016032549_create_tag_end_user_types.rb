class CreateTagEndUserTypes < ActiveRecord::Migration
  def change
    create_table :tag_end_user_types do |t|
      t.string :name
      t.boolean :deleted

      t.timestamps
    end
  end
end

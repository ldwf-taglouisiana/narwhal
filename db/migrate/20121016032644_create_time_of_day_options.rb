class CreateTimeOfDayOptions < ActiveRecord::Migration
  def change
    create_table :time_of_day_options do |t|
      t.string :time_of_day_option

      t.timestamps
    end
  end
end

class CreateFishConditionOptions < ActiveRecord::Migration
  def change
    create_table :fish_condition_options do |t|
      t.string :fish_condition_option

      t.timestamps
    end
  end
end

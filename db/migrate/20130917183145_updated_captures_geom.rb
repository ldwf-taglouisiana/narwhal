class UpdatedCapturesGeom < ActiveRecord::Migration
  def up

    (query = '') << <<-SQL
      UPDATE captures set geom = ST_SetSRID(ST_MakePoint(long,lat),4326)
    SQL

    ActiveRecord::Base.connection.execute(query)
  end

  def down
  end
end

class AddContactedAndHiddenToUsers < ActiveRecord::Migration
  def change

    add_column :users, :contacted, :boolean, default: false
    add_column :users, :hidden, :boolean, default: false
    add_column :users, :comments, :text

  end
end

class AddUserIdToTagLotsTable < ActiveRecord::Migration
  def change
    add_column :tag_lots, :user_id, :bigint
  end
end

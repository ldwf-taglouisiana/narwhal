# This migration was auto-generated via `rake db:generate_trigger_migration'.
# While you can edit this file, any changes you make to the definitions here
# will be undone by the next auto-generated trigger migration.

class CreateTriggersUsersInsertUpdateOrAnglersInsertUpdate < ActiveRecord::Migration
  def up
    drop_trigger("users_before_insert_update_row_tr", "users", :generated => true)

    drop_trigger("anglers_before_insert_update_row_tr", "anglers", :generated => true)

    create_trigger("users_before_insert_update_row_tr", :generated => true, :compatibility => 1).
        on("users").
        before(:insert, :update).
        declare("angler record;") do
      <<-SQL_ACTIONS
      SELECT * INTO angler FROM anglers WHERE angler_id = new.angler_id;
      new.search_vector :=
        setweight(to_tsvector('pg_catalog.english', coalesce(new.email, '')), 'A')      ||
        setweight(to_tsvector('pg_catalog.english', coalesce(new.angler_id, '')), 'A')  ||
        setweight(to_tsvector('pg_catalog.english', coalesce(new.last_name, '')), 'B')  ||
        setweight(to_tsvector('pg_catalog.english', coalesce(new.first_name, '')), 'B') ||
        setweight(to_tsvector('pg_catalog.english', coalesce(angler.email, '')), 'C')      ||
        setweight(to_tsvector('pg_catalog.english', coalesce(angler.angler_id, '')), 'C')  ||
        setweight(to_tsvector('pg_catalog.english', coalesce(angler.last_name, '')), 'D')  ||
        setweight(to_tsvector('pg_catalog.english', coalesce(angler.first_name, '')), 'D');
      SQL_ACTIONS
    end

    create_trigger("anglers_before_insert_update_row_tr", :generated => true, :compatibility => 1).
        on("anglers").
        before(:insert, :update) do
      <<-SQL_ACTIONS
      new.search_vector :=
        setweight(to_tsvector('pg_catalog.english', coalesce(new.angler_id, '')), 'A')  ||
        setweight(to_tsvector('pg_catalog.english', coalesce(new.last_name, '')), 'B')  ||
        setweight(to_tsvector('pg_catalog.english', coalesce(new.first_name, '')), 'B') ||
        setweight(to_tsvector('pg_catalog.english', coalesce(new.email, '')), 'C');
      SQL_ACTIONS
    end
  end

  def down
    drop_trigger("users_before_insert_update_row_tr", "users", :generated => true)

    drop_trigger("anglers_before_insert_update_row_tr", "anglers", :generated => true)

    create_trigger("users_before_insert_update_row_tr", :generated => true, :compatibility => 1).
        on("users").
        before(:insert, :update).
        declare("angler record;") do
      <<-SQL_ACTIONS
      SELECT * INTO angler FROM anglers WHERE angler_id = new.angler_id;
      new.search_vector :=
        setweight(to_tsvector('pg_catalog.english', coalesce(email, '')), 'A')      ||
        setweight(to_tsvector('pg_catalog.english', coalesce(angler_id, '')), 'A')  ||
        setweight(to_tsvector('pg_catalog.english', coalesce(last_name, '')), 'B')  ||
        setweight(to_tsvector('pg_catalog.english', coalesce(first_name, '')), 'B') ||
        setweight(to_tsvector('pg_catalog.english', coalesce(angler.email, '')), 'C')      ||
        setweight(to_tsvector('pg_catalog.english', coalesce(angler.angler_id, '')), 'C')  ||
        setweight(to_tsvector('pg_catalog.english', coalesce(angler.last_name, '')), 'D')  ||
        setweight(to_tsvector('pg_catalog.english', coalesce(angler.first_name, '')), 'D');
      SQL_ACTIONS
    end

    create_trigger("anglers_before_insert_update_row_tr", :generated => true, :compatibility => 1).
        on("anglers").
        before(:insert, :update) do
      <<-SQL_ACTIONS
      new.search_vector :=
        setweight(to_tsvector('pg_catalog.english', coalesce(angler_id, '')), 'A')  ||
        setweight(to_tsvector('pg_catalog.english', coalesce(last_name, '')), 'B')  ||
        setweight(to_tsvector('pg_catalog.english', coalesce(first_name, '')), 'B') ||
        setweight(to_tsvector('pg_catalog.english', coalesce(email, '')), 'C');
      SQL_ACTIONS
    end
  end
end

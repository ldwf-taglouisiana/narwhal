class CreateGpsFormatSqlFunction < ActiveRecord::Migration
  def change
    execute <<-SQL
      CREATE OR REPLACE FUNCTION format_latitude(geom GEOMETRY, format text) RETURNS text AS
        $$
          SELECT (split_part(regexp_replace(ST_AsLatLonText(geom, format), '(.+?)\s(-.*$)', '\1:\2'), ':', 1))
        $$
      LANGUAGE SQL;

      CREATE OR REPLACE FUNCTION format_longitude(geom GEOMETRY, format text) RETURNS text AS
        $$
          SELECT (split_part(regexp_replace(ST_AsLatLonText(geom, format), '(.+?)\s(-.*$)', '\1:\2'), ':', 2))
        $$
      LANGUAGE SQL;
    SQL
  end
end

class Add < ActiveRecord::Migration
  def up
    add_column :blurbs, :my_captures, :text
    add_column :blurbs, :draft_captures, :text
  end

  def down
    remove_column :blurbs, :my_captures
    remove_column :blurbs, :draft_captures
  end
end

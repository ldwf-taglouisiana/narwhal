class AddTzFormatterFunction < ActiveRecord::Migration
  def change
    execute <<-SQL
      -- taken from: http://www.postgresonline.com/journal/archives/257-Working-with-Timezones.html
      CREATE OR REPLACE FUNCTION date_display_tz(param_dt timestamp with time zone, param_tz text, param_format text) RETURNS text AS
      $$
        DECLARE
          var_result varchar;
        BEGIN
          PERFORM set_config('timezone', param_tz, true);
          var_result := to_char(param_dt, param_format);
          RETURN var_result;
        END;
      $$ language plpgsql VOLATILE;
    SQL
  end
end

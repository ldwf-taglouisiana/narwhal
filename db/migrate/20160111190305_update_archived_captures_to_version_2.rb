class UpdateArchivedCapturesToVersion2 < ActiveRecord::Migration
  def up
    drop_view :presentable_captures
    drop_view :archived_captures, revert_to_version: 2, materialized: true
    create_view :archived_captures, version: 2, materialized: true

    add_index :archived_captures, :id, unique: true
    add_index :archived_captures, :verified
    add_index :archived_captures, :geom

    execute <<-SQL
      CREATE INDEX idx_archived_captures_where_tag_year ON archived_captures USING GIST (year_id, angler_id, tag_number, location_description, comments, time_of_day_option_id, fish_condition_option_id, length, species_id, capture_date, basin_id, sub_basin_id);
    SQL
  end

  def down
    # drop_view :presentable_captures
    drop_view :archived_captures, revert_to_version: 1, materialized: true
    drop_view :recent_captures

    create_view :archived_captures, version: 1, materialized: true
    create_view :recent_captures, version: 1
    create_view :presentable_captures, version: 1
  end
end

class CreateBlurbs < ActiveRecord::Migration
  def change
    create_table :blurbs do |t|
      t.text :capture_dashboard
      t.text :new_capture
      t.text :my_tags
      t.text :order_tags
      t.text :find_a_spot
      t.text :common_fish
      t.text :target_species
      t.text :about_program
      t.text :sign_up
      t.text :how_to
      t.text :licensing
      t.text :volunteer_hours
      t.text :contact_info

      t.timestamps
    end
  end
end

class UpdateRecentVolunteerHoursToVersion2 < ActiveRecord::Migration
  def change
    update_view :recent_volunteer_hours, version: 2, revert_to_version: 1
    create_view :presentable_volunteer_hours, version: 1
  end
end

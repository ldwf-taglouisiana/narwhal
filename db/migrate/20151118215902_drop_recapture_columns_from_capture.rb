class DropRecaptureColumnsFromCapture < ActiveRecord::Migration
  def change
    remove_column :captures, :recapture
    remove_column :captures, :recapture_disposition_id
  end
end

class FixCaseOfAnglerId < ActiveRecord::Migration
  def up
    connection = ActiveRecord::Base.connection();
    connection.execute("update anglers set angler_id = upper(angler_id)")
  end

  def down
  end
end

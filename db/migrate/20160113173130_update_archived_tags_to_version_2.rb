class UpdateArchivedTagsToVersion2 < ActiveRecord::Migration
  def up
    create_view :archived_tags, version: 2, materialized: true

    add_index :archived_tags, :id, unique: true
    add_index :archived_tags, :tag_no
    add_index :archived_tags, :angler_id
    add_index :archived_tags, :recent
    add_index :archived_tags, :seq_id
    add_index :archived_tags, [:id, :used]
  end

  def down
    drop_view :tag_assignments
    drop_view :presentable_tags
    drop_view :archived_tags, materialized: true
  end
end

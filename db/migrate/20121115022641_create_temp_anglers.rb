class CreateTempAnglers < ActiveRecord::Migration
  def change
    create_table :temp_anglers do |t|
      t.integer :angler_id
      t.integer :lag_no
    end
  end
end

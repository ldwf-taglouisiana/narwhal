class ChangePhoneNumberTypeAngler < ActiveRecord::Migration
  def up
    change_column(:anglers, :phone_number_1_type, :string)
    change_column(:anglers, :phone_number_2_type, :string)
    change_column(:anglers, :phone_number_3_type, :string)
    change_column(:anglers, :phone_number_4_type, :string)
  end

  def down
    change_column(:anglers, :phone_number_1_type, :integer)
    change_column(:anglers, :phone_number_2_type, :integer)
    change_column(:anglers, :phone_number_3_type, :integer)
    change_column(:anglers, :phone_number_4_type, :integer)
  end
end

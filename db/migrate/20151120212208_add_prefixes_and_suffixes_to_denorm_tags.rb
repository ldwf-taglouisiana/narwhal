class AddPrefixesAndSuffixesToDenormTags < ActiveRecord::Migration
  def change
    execute <<-SQL
      DROP MATERIALIZED VIEW IF EXISTS denormalized_tags;
      CREATE MATERIALIZED VIEW denormalized_tags AS (
      WITH selected_tags AS (
          SELECT
            tags.*,
            SUBSTRING(tags.tag_no FROM '[^A-Z]+') :: BIGINT    AS "tag_suffix",
            SUBSTRING(tags.tag_no FROM '^[A-Z]+')              AS "tag_prefix",
            (COUNT(recaptures.id) + COUNT(captures.id)) > 0 AS "used"
          FROM
            tags
            LEFT JOIN recaptures ON tags.id = recaptures.tag_id
            LEFT JOIN captures ON tags.id = captures.tag_id
          GROUP BY
            tags.id
      )
      SELECT
        tags.*,
        concat(anglers.first_name, ' ', anglers.last_name) AS angler_name,
        tag_suffix - row_number()
        OVER (
          PARTITION BY tag_prefix, tags.updated_at::DATE ORDER BY tag_prefix, tag_suffix
        ) as seqID
      FROM
        selected_tags tags
        LEFT JOIN anglers ON tags.angler_id = anglers.angler_id
    );

    CREATE UNIQUE INDEX idx_denormalized_tag_id ON denormalized_tags (id);
    CREATE UNIQUE INDEX idx_denormalized_tag_no ON denormalized_tags (tag_no);
    SQL
  end
end

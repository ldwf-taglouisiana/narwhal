class UpdatePresentableTagsToVersion2 < ActiveRecord::Migration
  def up
    create_view :presentable_tags, version: 2
  end

  def down

  end
end

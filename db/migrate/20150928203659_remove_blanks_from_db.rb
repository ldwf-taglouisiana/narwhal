class RemoveBlanksFromDb < ActiveRecord::Migration
  def up
    execute <<-SQL
      --update captures set angler_id = NULL where NULLIF(regexp_replace(angler_id, '\s', 'g')) = NULL;

      DO
      $$
      DECLARE
          row record;
      BEGIN
          FOR row IN select table_name, column_name, data_type from information_schema.columns where (data_type = 'character varying' or data_type = 'text') AND  ( table_name in (select tablename from pg_tables where schemaname = 'public')  AND table_name != 'spatial_ref_sys' AND table_name != 'gps_formats')
          LOOP
              EXECUTE 'UPDATE ' || quote_ident(row.table_name) || ' SET ' || quote_ident(row.column_name) || ' = NULL WHERE NULLIF(regexp_replace(' || quote_ident(row.column_name) || ', ' || '''\s''' || ',' || '''''' || ', ' || '''g''' || '), '''') IS NULL';
          END LOOP;
      END;
      $$;

    SQL
  end
end

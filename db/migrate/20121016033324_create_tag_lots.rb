class CreateTagLots < ActiveRecord::Migration
  def change
    create_table :tag_lots do |t|
      t.integer :tag_lot_number
      t.integer :manufacturer_id
      t.string :prefix
      t.integer :start_no
      t.integer :end_no
      t.integer :end_user_type
      t.integer :cost
      t.datetime :date_entered
      t.boolean :ordered
      t.datetime :order_date
      t.boolean :received
      t.datetime :received_date
      t.boolean :deleted
      t.integer :color

      t.timestamps
    end
  end
end

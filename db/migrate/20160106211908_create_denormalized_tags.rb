class CreateDenormalizedTags < ActiveRecord::Migration
  def change
    drop_view :tag_assignments, materialized: true

    execute 'DROP MATERIALIZED VIEW IF EXISTS denormalized_tags;'
    create_view :archived_tags, materialized: true

    add_index :archived_tags, :id, unique: true
    add_index :archived_tags, :tag_no
    add_index :archived_tags, :recent
  end
end

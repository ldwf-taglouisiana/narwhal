class CreateHookBarbedOptions < ActiveRecord::Migration
  def change
    create_table :hook_barbed_options do |t|
      t.string :hook_barbed_option

      t.timestamps
    end
  end
end

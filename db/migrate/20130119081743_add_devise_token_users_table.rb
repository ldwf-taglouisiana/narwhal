class AddDeviseTokenUsersTable < ActiveRecord::Migration
  def self.up
    add_column :users, :authentication_token, :string

#    change_table :users do |t|
#      t.token_authenticatable
#    end
  end

  def self.down
     remove_column :users, :authentication_token
#     t.remove :authentication_token
  end
end

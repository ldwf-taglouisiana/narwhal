class FixDuplicatesTags < ActiveRecord::Migration
  def change
    execute <<-SQL

      -- Find all the captures with the bad tags and set them to the first tag in the database
      with bad_tags as (
        SELECT id, tag_no,
          ROW_NUMBER() OVER (PARTITION BY tag_no ORDER BY tag_no) AS rnum
        FROM tags
        WHERE tag_no in (
          SELECT
            tag_no
          FROM (SELECT
                id,
                tag_no,
                created_at,
                ROW_NUMBER()
                OVER (PARTITION BY tag_no
                  ORDER BY tag_no, created_at) AS rnum
              FROM tags) t
          WHERE t.rnum > 1
        )
      )
      UPDATE captures A
      SET tag_id = C.id
      FROM bad_tags B, bad_tags C
      WHERE
        (A.tag_id = B.id OR A.tag_id = C.id)
        AND B.tag_no = C.tag_no
        AND B.rnum = 2
        AND C.rnum = 1
      ;

      -- Find all the recaptures with the bad tags and set them to the first tag in the database
      with bad_tags as (
        SELECT id, tag_no,
          ROW_NUMBER() OVER (PARTITION BY tag_no ORDER BY tag_no) AS rnum
        FROM tags
        WHERE tag_no in (
          SELECT
            tag_no
          FROM (SELECT
                id,
                tag_no,
                created_at,
                ROW_NUMBER()
                OVER (PARTITION BY tag_no
                  ORDER BY tag_no) AS rnum
              FROM tags) t
          WHERE t.rnum > 1
        )
      )
      UPDATE recaptures A
      SET tag_id = C.id
      FROM bad_tags B, bad_tags C
      WHERE
        (A.tag_id = B.id OR A.tag_id = C.id)
        AND B.tag_no = C.tag_no
        AND B.rnum = 2
        AND C.rnum = 1
      ;

      with bad_tags as (
        SELECT id, tag_no,
          ROW_NUMBER() OVER (PARTITION BY tag_no ORDER BY tag_no) AS rnum
        FROM tags
        WHERE tag_no in (
          SELECT
            tag_no
          FROM (SELECT
                id,
                tag_no,
                created_at,
                ROW_NUMBER()
                OVER (PARTITION BY tag_no
                  ORDER BY tag_no) AS rnum
              FROM tags) t
          WHERE t.rnum > 1
        )
      )
      delete from tags
      WHERE id in (
        SELECT id from bad_tags where rnum = 2
      );

      DROP INDEX IF EXISTS idx_tags_tag_no;
      CREATE UNIQUE INDEX idx_tags_tag_no ON tags (lower(tag_no));
    SQL
  end
end

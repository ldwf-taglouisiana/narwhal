class CreateDenormalizedVolunteerHoursScenic < ActiveRecord::Migration
  def change
    execute 'DROP VIEW IF EXISTS denormalized_volunteer_hours;'
    create_view :archived_volunteer_hours, materialized: true

    add_index :archived_volunteer_hours, :id, unique: true
    add_index :archived_volunteer_hours, :angler_id
    add_index :archived_volunteer_hours, :recent
  end
end

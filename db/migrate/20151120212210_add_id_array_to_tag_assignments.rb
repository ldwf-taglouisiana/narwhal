class AddIdArrayToTagAssignments < ActiveRecord::Migration
  def change
    execute <<-SQL
      DROP MATERIALIZED VIEW IF EXISTS tag_assignments;

      CREATE MATERIALIZED VIEW tag_assignments AS (
        SELECT
          md5(angler_id || min(tag_no) || max(tag_no) || to_char(assigned_at, 'YYYY-MM-DD')) AS "id",
          angler_id,
          tag_prefix,
          assigned_at AS "assignment_at",
          min(tag_no)                                                                AS start_tag,
          max(tag_no)                                                                AS end_tag,
          --then we form an array of the tags where each array is a sequential grouping of tags
          array_agg(tag_no)                                                          AS tag_numbers,
          array_agg(id)                                                              AS tag_ids,
          count(tag_no)                                                              AS "number_of_tags",
          (
            SELECT
              tags.tag_no
            FROM
              captures
              INNER JOIN tags ON captures.tag_id = tags.id
            WHERE
              captures.tag_id = ANY (array_agg(A.id))
            ORDER BY
              tag_no DESC
            LIMIT 1
          )                                                                              AS last_tag_used
        FROM denormalized_tags A
        GROUP BY
          angler_id,
          tag_prefix,
          assignment_at,
          --This group by is what makes it work
          seqID
        ORDER BY
          assignment_at DESC
      );

      CREATE UNIQUE INDEX idx_tag_assignments_id ON tag_assignments (id);
      CREATE INDEX idx_tag_assignments_angler_id ON tag_assignments (angler_id);
    SQL
  end
end

class ChangeTimestampsToHaveTimeZones < ActiveRecord::Migration
  def change
    execute <<-SQL
        -- the data types in the materialized views need to be updated as well
        -- we need to drop them first,then rebuild them later
        DROP materialized VIEW denormalized_captures;
        DROP materialized VIEW denormalized_recaptures;

        -- find all the columns that have timestamp without time zones
        -- and update them to have a timezone. This will modify the
        -- time zone to be in cluster time.
        DO
        $$
        DECLARE
            row record;
        BEGIN
            FOR row IN select table_name, column_name, data_type from information_schema.columns where data_type = 'timestamp without time zone'
            LOOP
                EXECUTE 'ALTER TABLE ' || quote_ident(row.table_name) || ' ALTER COLUMN ' || quote_ident(row.column_name) ||' SET DATA TYPE timestamp with time zone;';
            END LOOP;
        END;
        $$;

        -- drop the old function and replace with one that uses time zones
        DROP function year_to_int(timestamp );
        CREATE OR REPLACE FUNCTION year_to_int(timestamp with time zone) RETURNS integer AS $$
          SELECT to_char($1, 'YYYY')::int
        $$ LANGUAGE SQL immutable;

        -- rebuild the materialized views
        -- pulled from a previous migration
        CREATE MATERIALIZED VIEW denormalized_captures AS (
          SELECT
            a.id as "id",
            a.species_id                                			      AS "species_id",
            species.common_name                                     AS "species_name",
            a.tag_id                                                AS "tag_id",
            tags.tag_no                                             AS "tag_number",
            a.angler_id                                 			      AS "angler_id",
            concat(anglers.first_name, ' ', anglers.last_name)      AS "capture_angler",
            a.capture_date                                          AS "capture_date",
            to_char(a.capture_date, 'MM/DD/YYYY')                 	AS "date_string",
            location_description_cleaner(a.location_description)	  AS "location",
            a.time_of_day_option_id                      			      AS "time_of_day_option_id",
            time_of_day_options.time_of_day_option                  AS "time_of_day",
            a.length                                    		      	AS "length",
            species_lengths.description                             AS "length_range",
            a.fish_condition_option_id                				      AS "fish_condition_option_id",
            fish_condition_options.fish_condition_option            AS "fish_condition",
            a.latitude                                  		  	    AS "latitude",
            a.longitude                                 			      AS "longitude",
            a.comments                      						            AS "comments",
            a.location_description                      			      AS "location_description",
            locations.id 											                      AS "location_id",
            (SELECT count(*) FROM recaptures WHERE recaptures.tag_id = a.tag_id) AS "recapture_count",
            year_to_int(capture_date) 								              AS "year_id"
          FROM captures a
            LEFT JOIN species ON a.species_id = species.id
            LEFT JOIN time_of_day_options ON a.time_of_day_option_id = time_of_day_options.id
            LEFT JOIN tags ON a.tag_id = tags.id
            LEFT JOIN fish_condition_options ON a.fish_condition_option_id = fish_condition_options.id
            LEFT JOIN anglers ON a.angler_id = anglers.angler_id
            LEFT JOIN species_lengths ON a.species_length_id = species_lengths.id
            LEFT JOIN locations on location_description_cleaner(a.location_description) = locations.location_description
        );

        CREATE INDEX idx_denormalized_captures_where_tag ON denormalized_captures USING GIST (angler_id, tag_number, location_description, comments, time_of_day_option_id, fish_condition_option_id, length, species_id, capture_date);
        CREATE INDEX idx_denormalized_captures_where_tag_year ON denormalized_captures USING GIST (year_id, angler_id, tag_number, location_description, comments, time_of_day_option_id, fish_condition_option_id, length, species_id, capture_date);

        ---

        CREATE MATERIALIZED VIEW denormalized_recaptures AS (
          SELECT
            a.species_id                                          AS "species_id",
            species.common_name                                   AS "species_name",
            a.tag_id                                              AS "tag_id",
            a.angler_id                                           AS "recapture_angler_id",
            concat(anglers.first_name, ' ', anglers.last_name)    AS "recapture_angler_name",
            a.capture_date                                        AS "capture_date",
            to_char(a.capture_date :: DATE, 'MM/DD/YYYY')         AS "date_string",
            location_description_cleaner(a.location_description)  AS "location",
            time_of_day_options.time_of_day_option                AS "time_of_day",
            a.length                                              AS "length",
            species_lengths.description                           AS "length_range",
            recapture_dispositions.recapture_disposition          AS "recapture_disposition",
            fish_condition_options.fish_condition_option          AS "fish_condition",
            a.latitude                                            AS "latitude",
            a.longitude                                           AS "longitude",
            a.location_description                                AS "location_description",
            locations.id                                          AS "location_id",
            (SELECT id from captures where captures.tag_id = a.tag_id )           AS "capture_id",
            row_number() OVER (PARTITION BY a.tag_id ORDER BY a.capture_date ASC) AS "recapture_number"
          FROM recaptures a
            LEFT OUTER JOIN species ON a.species_id =  species.id
            LEFT OUTER JOIN time_of_day_options ON a.time_of_day_option_id = time_of_day_options.id
            LEFT OUTER JOIN tags ON a.tag_id = tags.id
            LEFT OUTER JOIN recapture_dispositions ON a.recapture_disposition_id = recapture_dispositions.id
            LEFT OUTER JOIN fish_condition_options ON a.fish_condition_option_id = fish_condition_options.id
            LEFT OUTER JOIN anglers ON a.angler_id = anglers.angler_id
            LEFT OUTER JOIN species_lengths ON a.species_length_id = species_lengths.id
            LEFT OUTER JOIN locations on location_description_cleaner(a.location_description) = locations.location_description
        );

        CREATE INDEX idx_denormalized_recaptures_capture_id ON denormalized_recaptures(capture_id);
    SQL
  end
end

class FixRecapturesFlagInDatabase < ActiveRecord::Migration
  def up
    connection = ActiveRecord::Base.connection();
    connection.execute("update captures as c set recapture = true from ( (select * from captures) except (select distinct on (tag_id) * from captures order by tag_id, capture_date asc)) f where c.id = f.id")
  end

  def down
  end
end

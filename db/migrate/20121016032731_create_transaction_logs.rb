class CreateTransactionLogs < ActiveRecord::Migration
  def change
    create_table :transaction_logs do |t|
      t.datetime :date_time
      t.string :username
      t.string :action
      t.string :data_set
      t.integer :record_id

      t.timestamps
    end
  end
end

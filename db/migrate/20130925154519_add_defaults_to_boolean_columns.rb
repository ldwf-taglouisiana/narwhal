class AddDefaultsToBooleanColumns < ActiveRecord::Migration
  def up
    change_column :species, :publish, :boolean, :default => false
    change_column :species, :target, :boolean, :default => false

    execute <<-SQL
      update species SET publish = false WHERE publish IS NULL;
      update species SET target = false WHERE target IS NULL;
    SQL

    change_column :captures, :verified, :boolean, :default => false
    change_column :captures, :confirmed, :boolean, :default => false
    change_column :captures, :recapture, :boolean, :default => false

    execute <<-SQL
      update captures SET verified = false WHERE verified IS NULL;
      update captures SET recapture = false WHERE recapture IS NULL;
      update captures SET confirmed = false WHERE confirmed IS NULL;
    SQL


    change_column :draft_captures, :recapture, :boolean, :default => false

    execute <<-SQL
      update draft_captures SET recapture = false WHERE recapture IS NULL;
    SQL

    change_column :marinas, :closed, :boolean, :default => false
    change_column :marinas, :publish, :boolean, :default => false
    change_column :marinas, :has_bait, :boolean, :default => false
    change_column :marinas, :has_ice, :boolean, :default => false
    change_column :marinas, :has_food, :boolean, :default => false
    change_column :marinas, :has_restrooms, :boolean, :default => false
    change_column :marinas, :has_sewage_pump, :boolean, :default => false
    change_column :marinas, :has_cert_scales, :boolean, :default => false
    change_column :marinas, :has_cleaning_station, :boolean, :default => false
    change_column :marinas, :has_cleaning_station, :boolean, :default => false

    execute <<-SQL
      update marinas SET closed = false WHERE closed IS NULL;
      update marinas SET publish = false WHERE publish IS NULL;
      update marinas SET has_bait = false WHERE has_bait IS NULL;
      update marinas SET has_ice = false WHERE has_ice IS NULL;
      update marinas SET has_food = false WHERE has_food IS NULL;
      update marinas SET has_restrooms = false WHERE has_restrooms IS NULL;
      update marinas SET has_sewage_pump = false WHERE has_sewage_pump IS NULL;
      update marinas SET has_cert_scales = false WHERE has_cert_scales IS NULL;
      update marinas SET has_cleaning_station = false WHERE has_cleaning_station IS NULL;
      update marinas SET has_cleaning_station = false WHERE has_cleaning_station IS NULL;
    SQL
  end

  def down
    change_column :species, :publish, :boolean, :default => nil
    change_column :species, :target, :boolean, :default => nil

  end
end

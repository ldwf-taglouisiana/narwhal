class UpdateRecentRecapturesToVersion2 < ActiveRecord::Migration
  def up
    update_view :recent_recaptures, version: 2, revert_to_version: 1
    create_view :presentable_recaptures, version: 1
  end

  def down
    drop_view :presentable_recaptures
  end
end

class CreateTagAssignments < ActiveRecord::Migration
  def up
   execute <<-SQL
    CREATE MATERIALIZED VIEW tag_assignments AS (
    WITH MarkedForGrouping AS (
      SELECT
        anglers.angler_id                     AS "angler_id",
        tags.id                               AS "tag_id",
        substring(tags.tag_no FROM '^[A-Z]+') AS "tag_prefix",
        tags.tag_no                           AS "tag_number",
        tags.assigned_at :: DATE              AS assignment_at,
        --This is where the magic is we're create a window based on the prefix of the tag, angler who they are assigned to and the day they were updated
        substring(tags.tag_no FROM '[^A-Z]+') :: BIGINT - row_number()
        OVER (PARTITION BY substring(tags.tag_no FROM '^[A-Z]+'), anglers.id, tags.updated_at :: DATE
          ORDER BY tags.tag_no)             AS seqID
      FROM
        tags
        INNER JOIN anglers ON tags.angler_id = anglers.angler_id
    )
    SELECT
      md5(angler_id || min(tag_number) || max(tag_number)  || to_char(assignment_at, 'YYYY-MM-DD')) AS "id",
      angler_id,
      tag_prefix,
      assignment_at,
      min(tag_number)                                                                AS start_tag,
      max(tag_number)                                                                AS end_tag,
      --then we form an array of the tags where each array is a sequential grouping of tags
      array_agg(tag_number)                                                          AS tag_numbers,
      count(tag_number)                                                              AS "number_of_tags",
      (
        SELECT
      tags.tag_no
        FROM
          captures
          INNER JOIN tags ON captures.tag_id = tags.id
        WHERE
          captures.tag_id = ANY (array_agg(MarkedForGrouping.tag_id))
        ORDER BY
          tag_no DESC
        LIMIT 1
      )                                                                              AS last_tag_used
    FROM MarkedForGrouping
    GROUP BY
      angler_id,
      tag_prefix,
      assignment_at,
      --This group by is what makes it work
      seqID
    ORDER BY
      assignment_at DESC
  );

  CREATE UNIQUE INDEX idx_tag_assignments_id ON tag_assignments (id);
  CREATE INDEX idx_tag_assignments_angler_id ON tag_assignments (angler_id);
  CREATE INDEX idx_tag_assignments_dae ON tag_assignments (assignment_at);
   SQL
  end

  def down
    execute <<-SQL
      DROP MATERIALIZED VIEW tag_assignments;
    SQL
  end
end

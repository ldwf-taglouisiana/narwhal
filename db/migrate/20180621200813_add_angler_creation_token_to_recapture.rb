class AddAnglerCreationTokenToRecapture < ActiveRecord::Migration
  def up
    add_column :recaptures, :angler_creation_token, :string
    add_index :recaptures, :angler_creation_token, unique: true
    Angler.create(id: -1, first_name: 'Temp', last_name: 'Angler', angler_id: 'TMP1')
  end

  def down
    remove_index :recaptures, :angler_creation_token
    remove_column :recaptures, :angler_creation_token
    Angler.find(-1).delete
  end
end

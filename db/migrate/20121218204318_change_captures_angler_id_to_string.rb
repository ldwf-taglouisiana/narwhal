class ChangeCapturesAnglerIdToString < ActiveRecord::Migration
  def up
    change_column :captures, :angler_id, :string
  end

  def down
    change_column :captures, :angler_id, :integer
  end
end

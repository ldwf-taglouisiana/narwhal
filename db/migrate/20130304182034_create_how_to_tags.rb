class CreateHowToTags < ActiveRecord::Migration
  def change
    create_table :how_to_tags do |t|
      t.text :description

      t.timestamps
    end
  end
end

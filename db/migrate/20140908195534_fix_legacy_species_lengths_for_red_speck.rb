class FixLegacySpeciesLengthsForRedSpeck < ActiveRecord::Migration
  def change
    
    add_column :species_lengths, :min, :float
    add_column :species_lengths, :max, :float
    
    speckled_trout = Species.where(id: 2).first
    redfish = Species.where(id: 1).first


    SpeciesLength.where(description: '< 12"'     , species: speckled_trout, position: 1).first.update_attributes({min: 0, max:12})
    SpeciesLength.where(description: '12" - 15"' , species: speckled_trout, position: 2).first.update_attributes({min: 12.0001, max:15})
    SpeciesLength.where(description: '16" - 20"' , species: speckled_trout, position: 3).first.update_attributes({min: 15.0001, max:20})
    SpeciesLength.where(description: '> 20"'     , species: speckled_trout, position: 4).first.update_attributes({min: 20.0001, max:2147483647})

    SpeciesLength.where(description: '< 16"'     , species: redfish, position: 1).first.update_attributes({min: 0, max:16})
    SpeciesLength.where(description: '16" - 23"' , species: redfish, position: 2).first.update_attributes({min: 16.0001, max:23})
    SpeciesLength.where(description: '24" - 27"' , species: redfish, position: 3).first.update_attributes({min: 24.0001, max:27})
    SpeciesLength.where(description: '> 27"'     , species: redfish, position: 4).first.update_attributes({min: 27.0001, max:2147483647})


    execute <<-SQL
      create index idx_species_length_min_max on species_lengths(min, max);

      update captures
      set species_length_id =
      (
        select id from species_lengths where species_lengths.species_id = captures.species_id AND (captures.length between species_lengths.min and species_lengths.max)
      )
      where captures.species_length_id is null;

      update recaptures
      set species_length_id =
      (
        select id from species_lengths where species_lengths.species_id = recaptures.species_id AND (recaptures.length between species_lengths.min and species_lengths.max)
      )
      where recaptures.species_length_id is null;
    SQL
    
  end
end

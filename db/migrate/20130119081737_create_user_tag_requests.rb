class CreateUserTagRequests < ActiveRecord::Migration
  def change
    create_table :user_tag_requests do |t|
      t.string :angler_id
      t.integer :number_of_tags
      t.boolean :fulfilled

      t.timestamps
    end
  end
end

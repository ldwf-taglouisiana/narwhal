class UpdateTagAssignmentsToVersion2 < ActiveRecord::Migration
  def up
    create_view :tag_assignments, version: 2
  end

  def down
    # drop_view :tag_assignments, revert_to_version: 1
  end
end

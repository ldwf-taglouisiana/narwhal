class CreatePresentableVolunteerHours < ActiveRecord::Migration
  def change
    create_view :presentable_volunteer_hours
  end
end

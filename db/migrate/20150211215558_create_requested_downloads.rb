class CreateRequestedDownloads < ActiveRecord::Migration
  def change
    create_table :requested_downloads do |t|
      t.text :token, unique: true, null: false
      t.text :filename, null: false
      t.text :filepath
      t.boolean :should_email, default: false

      t.timestamps
    end

    # add the user id column in SQL since rails does not support bigint
    execute 'ALTER TABLE requested_downloads ADD COLUMN user_id bigint'
    execute 'ALTER TABLE requested_downloads ALTER COLUMN user_id SET NOT NULL'
  end
end

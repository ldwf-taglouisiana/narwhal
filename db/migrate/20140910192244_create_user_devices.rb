class CreateUserDevices < ActiveRecord::Migration
  def change

    create_table :user_device_types do |t|
      t.string :description, :unique => true
      t.timestamps
    end

    create_table :user_devices do |t|
      t.string :device_token, :unique => true, :null => false

      t.references :user, :null => false
      t.references :user_device_types, :null => false
      t.timestamps
    end

    add_index :user_devices, :user_id

    execute <<-SQL
      alter table user_devices add constraint fk_user_devices_user_id foreign key (user_id) references users(id) match full;
      alter table user_devices add constraint fk_user_devices_device_type_id foreign key (user_device_types_id) references user_device_types(id) match full;
    SQL

  end
end

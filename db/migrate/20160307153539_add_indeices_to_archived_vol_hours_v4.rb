class AddIndeicesToArchivedVolHoursV4 < ActiveRecord::Migration
  def change
    add_index :archived_volunteer_hours, :angler_id
    add_index :archived_volunteer_hours, :recent
    add_index :archived_volunteer_hours, :number_of_captures
    add_index :archived_volunteer_hours, :id, unique: true
  end
end

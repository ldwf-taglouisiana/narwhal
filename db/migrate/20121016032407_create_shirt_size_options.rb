class CreateShirtSizeOptions < ActiveRecord::Migration
  def change
    create_table :shirt_size_options do |t|
      t.string :shirt_size_option
      t.integer :item_ordinal_pos

      t.timestamps
    end
  end
end

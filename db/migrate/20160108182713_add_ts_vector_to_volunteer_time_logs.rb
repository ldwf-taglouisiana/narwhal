class AddTsVectorToVolunteerTimeLogs < ActiveRecord::Migration
  def up
    # add the vector column to the tags table
    add_column :volunteer_time_logs, :search_vector, :tsvector

    # set the tsvector column for the tags
    execute <<-SQL
      UPDATE volunteer_time_logs A
      SET search_vector =
        setweight(to_tsvector('pg_catalog.english', coalesce(A.angler_id, '')), 'A')  ||
        setweight(to_tsvector('pg_catalog.english', coalesce(C.email, '')), 'B')      ||
        setweight(to_tsvector('pg_catalog.english', coalesce(C.first_name, '')), 'C') ||
        setweight(to_tsvector('pg_catalog.english', coalesce(C.last_name, '')), 'B')
      FROM volunteer_time_logs B
        LEFT JOIN anglers C on B.angler_id = C.angler_id
      WHERE A.id = B.id;
    SQL

    # add the index, after population for faster pre-population
    add_index :volunteer_time_logs, :search_vector, using: :gin

    # disables nulls for the tsvector column
    change_column_null :volunteer_time_logs, :search_vector, false
  end

  def down
    remove_column :volunteer_time_logs, :search_vector, :tsvector
  end
end

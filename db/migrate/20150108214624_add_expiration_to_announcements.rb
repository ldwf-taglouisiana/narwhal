class AddExpirationToAnnouncements < ActiveRecord::Migration
  def change
    add_column :announcements, :expiration_date, :datetime
  end
end

class CreateTagAssignmentsScenic < ActiveRecord::Migration
  def change
    execute 'DROP MATERIALIZED VIEW IF EXISTS tag_assignments;'
    create_view :tag_assignments
  end
end

class RenameCaptureRelatedColumns < ActiveRecord::Migration
  def up
    rename_column :draft_captures, :recapture_disposition, :recapture_disposition_id
    rename_column :captures, :recapture_disposition, :recapture_disposition_id
  end

  def down
    rename_column :draft_captures, :recapture_disposition_id, :recapture_disposition
    rename_column :captures, :recapture_disposition_id, :recapture_disposition
  end
end

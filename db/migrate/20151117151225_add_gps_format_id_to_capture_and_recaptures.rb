class AddGpsFormatIdToCaptureAndRecaptures < ActiveRecord::Migration
  def change
    add_column :captures, :gps_format_id, :bigint
    add_column :recaptures, :gps_format_id, :bigint

    add_foreign_key :captures, :gps_formats
    add_foreign_key :recaptures, :gps_formats

    add_index :captures, :gps_format_id
    add_index :recaptures, :gps_format_id

    execute <<-SQL
      UPDATE captures A SET gps_format_id = B.id
      FROM gps_formats B
      WHERE A.entered_gps_type = B.format_type;

      UPDATE recaptures A SET gps_format_id = B.id
      FROM gps_formats B
      WHERE A.entered_gps_type = B.format_type;
    SQL
  end
end

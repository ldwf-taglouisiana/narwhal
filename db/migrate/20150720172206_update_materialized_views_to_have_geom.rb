class UpdateMaterializedViewsToHaveGeom < ActiveRecord::Migration
  def up
    execute <<-SQL
      DROP MATERIALIZED VIEW denormalized_captures;
      DROP MATERIALIZED VIEW denormalized_recaptures;

      CREATE MATERIALIZED VIEW denormalized_captures AS (
        SELECT
          a.id                                                 AS "id",
          a.species_id                                         AS "species_id",
          species.common_name                                  AS "species_name",
          a.tag_id                                             AS "tag_id",
          tags.tag_no                                          AS "tag_number",
          a.angler_id                                          AS "angler_id",
          concat(anglers.first_name, ' ', anglers.last_name)   AS "capture_angler",
          a.capture_date                                       AS "capture_date",
          to_char(a.capture_date, 'MM/DD/YYYY')                AS "date_string",
          location_description_cleaner(a.location_description) AS "location",
          a.time_of_day_option_id                              AS "time_of_day_option_id",
          time_of_day_options.time_of_day_option               AS "time_of_day",
          a.length                                             AS "length",
          species_lengths.description                          AS "length_range",
          a.fish_condition_option_id                           AS "fish_condition_option_id",
          fish_condition_options.fish_condition_option         AS "fish_condition",
          a.latitude                                           AS "latitude",
          a.longitude                                          AS "longitude",
          format_latitude(a.geom, gps_formats.format_string)   AS "latitude_formatted",
          format_longitude(a.geom, gps_formats.format_string)  AS "longitude_formatted",
          CASE
          WHEN a.basin_id IS NULL
            THEN
              basins.gid
          ELSE
            NULL
          END                                                  AS "basin_id",
          CASE
          WHEN a.sub_basin_id IS NULL
            THEN
              sub_basins.gid
          ELSE
            NULL
          END                                                  AS "sub_basin_id",
          a.comments                                           AS "comments",
          a.user_capture                                       AS "user_capture",
          a.verified                                           AS "verified",
          a.location_description                               AS "location_description",
          locations.id                                         AS "location_id",
          a.created_at                                         AS "created_at",
          (
            SELECT
          count(*)
            FROM recaptures
            WHERE recaptures.tag_id = a.tag_id
          )                 									 AS "recapture_count",
          year_to_int(capture_date)                            AS "year_id"
        FROM captures a
          LEFT JOIN species ON a.species_id = species.id
          LEFT JOIN time_of_day_options ON a.time_of_day_option_id = time_of_day_options.id
          LEFT JOIN tags ON a.tag_id = tags.id
          LEFT JOIN fish_condition_options ON a.fish_condition_option_id = fish_condition_options.id
          LEFT JOIN anglers ON a.angler_id = anglers.angler_id
          LEFT JOIN species_lengths ON a.species_length_id = species_lengths.id
          LEFT JOIN locations ON location_description_cleaner(a.location_description) = locations.location_description
          LEFT JOIN gps_formats ON a.entered_gps_type = gps_formats.format_type
          LEFT OUTER JOIN basins ON ST_Contains(basins.geom, a.geom)
          LEFT OUTER JOIN "sub-basin" sub_basins ON ST_Contains(sub_basins.geom, a.geom)
      );

      ---

      CREATE MATERIALIZED VIEW denormalized_recaptures AS (
        SELECT
          a.id                                                 AS "id",
          a.species_id                                         AS "species_id",
          species.common_name                                  AS "species_name",
          a.tag_id                                             AS "tag_id",
          tags.tag_no                                          AS "tag_number",
          a.angler_id                                          AS "recapture_angler_id",
          concat(anglers.first_name, ' ', anglers.last_name)   AS "recapture_angler_name",
          a.capture_date                                       AS "capture_date",
          to_char(a.capture_date :: DATE, 'MM/DD/YYYY')        AS "date_string",
          location_description_cleaner(a.location_description) AS "location",
          time_of_day_options.time_of_day_option               AS "time_of_day",
          a.time_of_day_option_id                              AS "time_of_day_option_id",
          a.length                                             AS "length",
          species_lengths.description                          AS "length_range",
          recapture_dispositions.recapture_disposition         AS "recapture_disposition",
          a.fish_condition_option_id                           AS "fish_condition_option_id",
          fish_condition_options.fish_condition_option         AS "fish_condition",
          a.latitude                                           AS "latitude",
          a.verified                                           AS "verified",
          a.longitude                                          AS "longitude",
          format_latitude(a.geom, gps_formats.format_string)   AS "latitude_formatted",
          format_longitude(a.geom, gps_formats.format_string)  AS "longitude_formatted",
          CASE
          WHEN a.basin_id IS NULL
            THEN
              basins.gid
          ELSE
            NULL
          END                                                  AS "basin_id",
          CASE
          WHEN a.sub_basin_id IS NULL
            THEN
              sub_basins.gid
          ELSE
            NULL
          END                                                  AS "sub_basin_id",
          a.comments                                           AS "comments",
          a.location_description                               AS "location_description",
          locations.id                                         AS "location_id",
          a.created_at                                         AS "created_at",
          (
            SELECT
          id
            FROM captures
            WHERE captures.tag_id = a.tag_id
          )                   								 AS "capture_id",
          year_to_int(capture_date)                            AS "year_id",
          row_number()
          OVER (PARTITION BY a.tag_id
            ORDER BY a.capture_date ASC)                     AS "recapture_number"
        FROM recaptures a
          LEFT OUTER JOIN species ON a.species_id = species.id
          LEFT OUTER JOIN time_of_day_options ON a.time_of_day_option_id = time_of_day_options.id
          LEFT OUTER JOIN tags ON a.tag_id = tags.id
          LEFT OUTER JOIN recapture_dispositions ON a.recapture_disposition_id = recapture_dispositions.id
          LEFT OUTER JOIN fish_condition_options ON a.fish_condition_option_id = fish_condition_options.id
          LEFT OUTER JOIN anglers ON a.angler_id = anglers.angler_id
          LEFT OUTER JOIN species_lengths ON a.species_length_id = species_lengths.id
          LEFT OUTER JOIN locations
            ON location_description_cleaner(a.location_description) = locations.location_description
          LEFT OUTER JOIN gps_formats ON a.entered_gps_type = gps_formats.format_type
          LEFT OUTER JOIN basins ON ST_Contains(basins.geom, a.geom)
          LEFT OUTER JOIN "sub-basin" sub_basins ON ST_Contains(sub_basins.geom, a.geom)
      );

      CREATE UNIQUE INDEX idx_denormalized_captures_id ON denormalized_captures (id);
      CREATE UNIQUE INDEX idx_denormalized_recaptures_id ON denormalized_recaptures (id);

      CREATE INDEX idx_denormalized_captures_where_tag ON denormalized_captures USING GIST (angler_id, tag_number, location_description, comments, time_of_day_option_id, fish_condition_option_id, length, species_id, capture_date, basin_id, sub_basin_id);
      CREATE INDEX idx_denormalized_captures_where_tag_year ON denormalized_captures USING GIST (year_id, angler_id, tag_number, location_description, comments, time_of_day_option_id, fish_condition_option_id, length, species_id, capture_date, basin_id, sub_basin_id);

      CREATE INDEX idx_denormalized_recaptures_where_tag ON denormalized_recaptures USING GIST (recapture_angler_id, tag_number, location_description, comments, time_of_day_option_id, fish_condition_option_id, length, species_id, capture_date, basin_id, sub_basin_id);
      CREATE INDEX idx_denormalized_recaptures_where_tag_yer ON denormalized_recaptures USING GIST (year_id, recapture_angler_id, tag_number, location_description, comments, time_of_day_option_id, fish_condition_option_id, length, species_id, capture_date, basin_id, sub_basin_id);

      CREATE INDEX idx_denormalized_recaptures_capture_id ON denormalized_recaptures (capture_id);
    SQL
  end

  def down
    # do not need a down, since the up rebuilds anyways
  end
end

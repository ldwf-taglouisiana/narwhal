class CreateCapturesDupsTable < ActiveRecord::Migration
  def up
    (create_dups_table = '') << <<-eod
--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: captures_dups; Type: TABLE; Schema: public; Owner: postgres; Tablespace:
--

CREATE TABLE captures_dups (
    id integer NOT NULL,
    angler_id character varying(20),
    captain_id integer,
    tag_id integer,
    capture_date timestamp without time zone,
    species_id integer,
    length double precision,
    estimated_length double precision,
    location_description text,
    lat double precision,
    long double precision,
    fish_condition_id integer,
    fish_tagged_in_id integer,
    landing_net_used_id integer,
    hook_type_id integer,
    bait_type_id integer,
    hook_removed_id integer,
    hook_barbed_id integer,
    weight double precision,
    estimated_weight double precision,
    water_temperature double precision,
    water_salinity double precision,
    wind_speed double precision,
    wind_direction_id integer,
    comments text,
    confirmed boolean,
    verified boolean,
    entry_date timestamp without time zone,
    deleted boolean,
    mailed_map boolean,
    recapture boolean,
    recapture_disposition integer,
    see_card boolean,
    time_of_day integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    geom geometry,
    old_angler_id integer,
    entered_gps_type character varying(255),
    user_capture boolean
);


ALTER TABLE public.captures_dups OWNER TO postgres;

--
-- Name: captures_dups_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace:
--

ALTER TABLE ONLY captures_dups
    ADD CONSTRAINT captures_dups_pkey PRIMARY KEY (id);

    eod

    (move_dups = '') << <<-eod
INSERT INTO captures_dups (
  select * from captures where id in (
    select id from (
      SELECT id, ROW_NUMBER() OVER( PARTITION BY capture_date, recapture, tag_id, time_of_day, angler_id, species_id, fish_condition_id, lat, long, location_description, comments, entered_gps_type ) AS Row
      FROM captures
    ) dups
    where
    dups.Row > 1
  )
)
    eod

    (remove_dups = '') << <<-eod
DELETE from captures where id in (
 select id from (
    SELECT id, ROW_NUMBER() OVER( PARTITION BY capture_date, recapture, tag_id, time_of_day, angler_id, species_id, fish_condition_id, lat, long, location_description, comments, entered_gps_type ) AS Row
    FROM captures
  ) dups
  where
  dups.Row > 1
)
    eod

    Capture.transaction do
      ActiveRecord::Base.connection.execute(create_dups_table)
      ActiveRecord::Base.connection.execute(move_dups)
      ActiveRecord::Base.connection.execute(remove_dups)
    end
  end

  def down
  end
end

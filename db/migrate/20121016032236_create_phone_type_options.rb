class CreatePhoneTypeOptions < ActiveRecord::Migration
  def change
    create_table :phone_type_options do |t|
      t.string :phone_type_option

      t.timestamps
    end
  end
end

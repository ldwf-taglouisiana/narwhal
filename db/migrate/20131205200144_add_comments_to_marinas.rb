class AddCommentsToMarinas < ActiveRecord::Migration
  def change
    add_column :marinas, :comments, :text
  end
end

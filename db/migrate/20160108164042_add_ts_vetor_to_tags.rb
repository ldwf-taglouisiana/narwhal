class AddTsVetorToTags < ActiveRecord::Migration
  def up
    # add the vector column to the tags table
    add_column :tags, :search_vector, :tsvector

    # set the tsvector column for the tags
    execute <<-SQL
      UPDATE tags A
      SET search_vector =
        setweight(to_tsvector('pg_catalog.english', coalesce(A.tag_no, '')), 'A')     ||
        setweight(to_tsvector('pg_catalog.english', coalesce(A.angler_id, '')), 'B')  ||
        setweight(to_tsvector('pg_catalog.english', coalesce(C.last_name, '')), 'C')  ||
        setweight(to_tsvector('pg_catalog.english', coalesce(C.first_name, '')), 'C') ||
        setweight(to_tsvector('pg_catalog.english', coalesce(C.email, '')), 'D')
      FROM tags B LEFT JOIN anglers C on B.angler_id = C.angler_id
      WHERE A.id = B.id;
    SQL

    # add the index, after population for faster pre-population
    add_index :tags, :search_vector, using: :gin

    # disables nulls for the tsvector column
    change_column_null :tags, :search_vector, false
  end

  def down
    remove_column :tags, :search_vector, :tsvector
  end
end

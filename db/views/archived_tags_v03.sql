WITH selected_tags AS (
	SELECT
		tags.*,
		SUBSTRING(tags.tag_no FROM '[^A-Z]+')::BIGINT    AS "tag_suffix",
		SUBSTRING(tags.tag_no FROM '^[A-Z]+')              AS "tag_prefix",
		(COUNT(recaptures.id) + COUNT(captures.id)) > 0 AS "used",
		GREATEST(MAX(captures.updated_at), MAX(recaptures.updated_at)) as "used_at"
	FROM
		tags
		LEFT JOIN recaptures ON tags.id = recaptures.tag_id
		LEFT JOIN captures ON tags.id = captures.tag_id
	WHERE tags.recent = false
	GROUP BY
		tags.id
)
SELECT
	tags.*,
	concat(anglers.first_name, ' ', anglers.last_name) AS angler_name,
	anglers.angler_id as angler_number,
	tag_suffix - row_number()
	OVER (
		PARTITION BY tag_prefix, tags.angler_id, tags.updated_at::DATE ORDER BY tag_prefix, tag_suffix, tags.angler_id
	) as seq_id
FROM
	selected_tags tags
	LEFT JOIN anglers ON tags.angler_id = anglers.id
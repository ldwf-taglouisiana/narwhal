WITH items AS (
	SELECT
		volunteer_time_logs.id,
		count(captures.id) AS "number_of_captures"
	FROM
		volunteer_time_logs
		INNER JOIN captures
			ON captures.capture_date <@ tstzrange(volunteer_time_logs.start, volunteer_time_logs.end, '[]')
	WHERE
		volunteer_time_logs.recent = false
		AND
		captures.angler_id = volunteer_time_logs.angler_id
		AND
		captures.verified IS TRUE
	GROUP BY
		volunteer_time_logs.id
	ORDER BY
		number_of_captures DESC
)
SELECT
	volunteer_time_logs.id,
	volunteer_time_logs.angler_id,
	concat(anglers.last_name, ', ', anglers.first_name) as "angler_name",
	volunteer_time_logs.verified AS verified,
	volunteer_time_logs.ldwf_entered AS ldwf_entered,
	volunteer_time_logs.ldwf_edited AS ldwf_edited,
	(volunteer_time_logs.end - volunteer_time_logs.start) as total_time,
	volunteer_time_logs.start,
	volunteer_time_logs.end,
	volunteer_time_logs.recent,
	volunteer_time_logs.created_at,
	items.number_of_captures
FROM
	items
	LEFT JOIN volunteer_time_logs ON items.id = volunteer_time_logs.id
	LEFT JOIN anglers ON volunteer_time_logs.angler_id = anglers.angler_id
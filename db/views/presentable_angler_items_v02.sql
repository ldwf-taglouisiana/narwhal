SELECT
  angler_items.id                                    AS id,
  angler_items.requested_at                          AS requested_at,
  angler_items.fulfilled_at                          AS fulfilled_at,
  angler_items.created_at                            AS created_at,
  angler_items.updated_at                            AS updated_at,
  concat(anglers.first_name, ' ', anglers.last_name) AS angler_name,
  anglers.angler_id                                  AS angler_number,
  items.name                                         AS item_name,
  angler_item_types.name                             AS item_type,
  angler_items.angler_id                             AS angler_id,
  users.id                                           AS user_id
FROM
  angler_items
  INNER JOIN anglers ON anglers.id = angler_items.angler_id
  INNER JOIN items ON items.id = angler_items.item_id
  INNER JOIN angler_item_types ON angler_item_types.id = angler_items.angler_item_type_id
  LEFT JOIN  users ON users.angler_id = anglers.id
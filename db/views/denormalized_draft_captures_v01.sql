select
    draft_captures.*,
    concat(anglers.first_name, ' ', anglers.last_name) as angler_full_name,
    species.common_name as species_name,
    anglers.angler_id as angler_number,
    format_latitude(geom, gps_formats.format_string) as latitude_formatted,
    format_longitude(geom, gps_formats.format_string) as longitude_formatted,
    CASE
    WHEN draft_captures.length IS NOT NULL THEN
        draft_captures.length::text
    ELSE
        species_lengths.description
    END  AS "species_length"
from draft_captures
    LEFT OUTER JOIN "species" ON "draft_captures".species_id = "species".id
    LEFT OUTER JOIN "anglers" ON "draft_captures".angler_id = "anglers".id
    LEFT OUTER JOIN "species_lengths" ON "draft_captures".species_length_id = "species_lengths".id
    LEFT OUTER JOIN "gps_formats" ON "draft_captures".entered_gps_type = "gps_formats".format_type
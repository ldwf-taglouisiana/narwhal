SELECT
	min(A.id)                        AS "id",
	min(A.angler_id)             	 AS "angler_id",
	min(A.angler_number)             AS "angler_number",
	min(A.tag_prefix)                AS "tag_prefix",
	min(A.updated_at :: DATE)        AS "assignment_at",
	min(tag_no)                      AS start_tag,
	max(tag_no)                      AS end_tag,
	count(tag_no)                    AS "number_of_tags",
	max(A.tag_no)
		FILTER (WHERE A.used = TRUE) AS last_tag_used,
	array_agg(tag_no)                AS tag_numbers,
	array_agg(id)                    AS tag_ids     ,
	seq_id
FROM
	presentable_tags AS A
WHERE
	angler_id IS NOT NULL
GROUP BY
	seq_id,
	angler_id
ORDER BY
	assignment_at DESC
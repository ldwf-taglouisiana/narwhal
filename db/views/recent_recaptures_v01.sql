SELECT
	a.user_capture                                       AS "user_capture",
	a.id                                                 AS "id",
	a.species_id                                         AS "species_id",
	species.common_name                                  AS "species_name",
	a.tag_id                                             AS "tag_id",
	tags.tag_no                                          AS "tag_number",
	a.angler_id                                          AS "recapture_angler_id",
	concat(anglers.first_name, ' ', anglers.last_name)   AS "recapture_angler_name",
	a.capture_date                                       AS "capture_date",
	to_char(a.capture_date :: DATE, 'MM/DD/YYYY')        AS "date_string",
	location_description_cleaner(a.location_description) AS "location",
	time_of_day_options.time_of_day_option               AS "time_of_day",
	a.time_of_day_option_id                              AS "time_of_day_option_id",
	a.length                                             AS "length",
	species_lengths.description                          AS "length_range",
	recapture_dispositions.recapture_disposition         AS "recapture_disposition",
	a.fish_condition_option_id                           AS "fish_condition_option_id",
	fish_condition_options.fish_condition_option         AS "fish_condition",
	a.latitude                                           AS "latitude",
	a.verified                                           AS "verified",
	a.longitude                                          AS "longitude",
	format_latitude(a.geom, gps_formats.format_string)   AS "latitude_formatted",
	format_longitude(a.geom, gps_formats.format_string)  AS "longitude_formatted",
	a.geom                                               AS "geom",
	a.basin_id                                           AS "basin_id",
	a.sub_basin_id                                       AS "sub_basin_id",
	a.comments                                           AS "comments",
	a.location_description                               AS "location_description",
	locations.id                                         AS "location_id",
	a.created_at                                         AS "created_at",
	a.capture_id                                         AS "capture_id",
	year_to_int(capture_date)                            AS "year_id",
	row_number()
	OVER (
		PARTITION BY a.capture_id
		ORDER BY a.capture_date ASC
	)                                                    AS "recapture_number"
FROM recaptures a
	LEFT OUTER JOIN species ON a.species_id = species.id
	LEFT OUTER JOIN time_of_day_options ON a.time_of_day_option_id = time_of_day_options.id
	LEFT OUTER JOIN tags ON a.tag_id = tags.id
	LEFT OUTER JOIN recapture_dispositions ON a.recapture_disposition_id = recapture_dispositions.id
	LEFT OUTER JOIN fish_condition_options ON a.fish_condition_option_id = fish_condition_options.id
	LEFT OUTER JOIN anglers ON a.angler_id = anglers.angler_id
	LEFT OUTER JOIN species_lengths ON a.species_length_id = species_lengths.id
	LEFT OUTER JOIN locations ON location_description_cleaner(a.location_description) = locations.location_description
	LEFT OUTER JOIN gps_formats ON a.gps_format_id = gps_formats.id
WHERE
	a.recent = TRUE OR verified = FALSE
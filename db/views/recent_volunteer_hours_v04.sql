WITH items AS (
	SELECT
		volunteer_time_logs.id,
		count(captures.id) AS "number_of_captures"
	FROM
		volunteer_time_logs
		INNER JOIN captures
			ON captures.capture_date <@ tstzrange(volunteer_time_logs.start, volunteer_time_logs.end, '[]')
	WHERE
		volunteer_time_logs.recent = true
		AND
		captures.angler_id = volunteer_time_logs.angler_id
		AND
		captures.verified IS TRUE
	GROUP BY
		volunteer_time_logs.id
	ORDER BY
		number_of_captures DESC
)
SELECT
	volunteer_time_logs.id,
	volunteer_time_logs.angler_id,
	anglers.angler_id as angler_number,
	concat(anglers.last_name, ', ', anglers.first_name) as "angler_name",
	volunteer_time_logs.verified AS verified,
	volunteer_time_logs.ldwf_entered AS ldwf_entered,
	volunteer_time_logs.ldwf_edited AS ldwf_edited,
	round((extract(epoch from (volunteer_time_logs.end - volunteer_time_logs.start))/3600.0)::NUMERIC , 2) as total_time,
	volunteer_time_logs.start,
	volunteer_time_logs.end,
	volunteer_time_logs.recent,
	volunteer_time_logs.created_at,
	volunteer_time_logs.updated_at,
	items.number_of_captures
FROM
	items
	LEFT JOIN volunteer_time_logs ON items.id = volunteer_time_logs.id
	LEFT JOIN anglers ON volunteer_time_logs.angler_id = anglers.id
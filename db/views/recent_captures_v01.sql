WITH r_captures as (
	select * from captures where recent = true
)
SELECT
	a.id                                                 AS "id",
	a.species_id                                         AS "species_id",
	species.common_name                                  AS "species_name",
	a.tag_id                                             AS "tag_id",
	tags.tag_no                                          AS "tag_number",
	a.angler_id                                          AS "angler_id",
	concat(anglers.first_name, ' ', anglers.last_name)   AS "capture_angler",
	a.capture_date                                       AS "capture_date",
	to_char(a.capture_date, 'MM/DD/YYYY')                AS "date_string",
	location_description_cleaner(a.location_description) AS "location",
	a.time_of_day_option_id                              AS "time_of_day_option_id",
	time_of_day_options.time_of_day_option               AS "time_of_day",
	a.length                                             AS "length",
	species_lengths.description                          AS "length_range",
	a.fish_condition_option_id                           AS "fish_condition_option_id",
	fish_condition_options.fish_condition_option         AS "fish_condition",
	a.latitude                                           AS "latitude",
	a.longitude                                          AS "longitude",
	format_latitude(a.geom, gps_formats.format_string)   AS "latitude_formatted",
	format_longitude(a.geom, gps_formats.format_string)  AS "longitude_formatted",
	a.entered_gps_type                                   AS "entered_gps_type",
	a.geom                                               AS "geom",
	a.basin_id                                           AS "basin_id",
	a.sub_basin_id                                       AS "sub_basin_id",
	a.comments                                           AS "comments",
	a.user_capture                                       AS "user_capture",
	a.verified                                           AS "verified",
	a.location_description                               AS "location_description",
	locations.id                                         AS "location_id",
	a.created_at                                         AS "created_at",
	a.updated_at                                         AS "updated_at",
	(
		SELECT
			count(*)
		FROM recaptures
		WHERE recaptures.capture_id = a.id
	)                                                    AS "recapture_count",
	year_to_int(capture_date)                            AS "year_id"
FROM r_captures a
	LEFT JOIN species ON a.species_id = species.id
	LEFT JOIN time_of_day_options ON a.time_of_day_option_id = time_of_day_options.id
	LEFT JOIN tags ON a.tag_id = tags.id
	LEFT JOIN fish_condition_options ON a.fish_condition_option_id = fish_condition_options.id
	LEFT JOIN anglers ON a.angler_id = anglers.angler_id
	LEFT JOIN species_lengths ON a.species_length_id = species_lengths.id
	LEFT JOIN locations ON location_description_cleaner(a.location_description) = locations.location_description
	LEFT JOIN gps_formats ON a.gps_format_id = gps_formats.id
WHERE
	a.recent = TRUE OR a.verified = FALSE
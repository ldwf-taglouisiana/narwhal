# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140227221551) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "postgis"

  create_table "about_tagging_programs", force: true do |t|
    t.text     "description", null: false
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "activity_log_types", force: true do |t|
    t.string   "activity_type", null: false
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "activity_logs", force: true do |t|
    t.integer  "activity_log_type_id", null: false
    t.string   "old_item_id"
    t.integer  "user_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.integer  "activity_item_id",     null: false
    t.string   "activity_item_type",   null: false
  end

  create_table "anglers", id: false, force: true do |t|
    t.integer  "id",                             default: "nextval('anglers_id_seq'::regclass)"
    t.integer  "lag_no"
    t.integer  "law_no"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "street"
    t.string   "suite"
    t.string   "city"
    t.string   "state"
    t.string   "zip"
    t.string   "phone_number_1"
    t.string   "phone_number_1_type"
    t.string   "phone_number_2"
    t.string   "phone_number_2_type"
    t.string   "phone_number_3"
    t.string   "phone_number_3_type"
    t.string   "phone_number_4"
    t.string   "phone_number_4_type"
    t.string   "email"
    t.integer  "tag_end_user_type"
    t.integer  "shirt_size"
    t.boolean  "deleted"
    t.integer  "lax_no"
    t.string   "email_2"
    t.string   "user_name"
    t.text     "comments"
    t.datetime "created_at",                                                                     null: false
    t.datetime "updated_at",                                                                     null: false
    t.string   "angler_id",           limit: 20,                                                 null: false
  end

  add_index "anglers", ["angler_id", "first_name", "last_name"], :name => "index_anglers_on_angler_id_and_first_name_and_last_name"

  create_table "basins", primary_key: "gid", force: true do |t|
    t.decimal "objectid",                                              precision: 10, scale: 0
    t.decimal "area"
    t.decimal "perimeter"
    t.decimal "subseg02",                                              precision: 10, scale: 0
    t.decimal "subseg02id",                                            precision: 10, scale: 0
    t.string  "subsegment", limit: 15
    t.string  "name",       limit: 71
    t.string  "descriptio", limit: 140
    t.string  "basin",      limit: 18
    t.decimal "shape_area"
    t.decimal "shape_len"
    t.spatial "geom",       limit: {:srid=>0, :type=>"multi_polygon"}
  end

  create_table "blurbs", force: true do |t|
    t.text     "capture_dashboard"
    t.text     "new_capture"
    t.text     "my_tags"
    t.text     "order_tags"
    t.text     "find_a_spot"
    t.text     "common_fish"
    t.text     "target_species"
    t.text     "about_program"
    t.text     "sign_up"
    t.text     "how_to"
    t.text     "licensing"
    t.text     "volunteer_hours"
    t.text     "contact_info"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.text     "non_registered_capture"
    t.text     "login"
    t.text     "my_captures"
    t.text     "draft_captures"
  end

  create_table "capture_duplicates", id: false, force: true do |t|
    t.integer  "id",                                                                             null: false
    t.string   "angler_id",                limit: 20
    t.integer  "tag_id"
    t.datetime "capture_date"
    t.integer  "species_id"
    t.float    "length"
    t.text     "location_description"
    t.float    "latitude"
    t.float    "longitude"
    t.integer  "fish_condition_option_id"
    t.float    "weight"
    t.text     "comments"
    t.boolean  "confirmed",                                                      default: false
    t.boolean  "verified",                                                       default: false
    t.boolean  "deleted"
    t.boolean  "mailed_map"
    t.boolean  "recapture",                                                      default: false
    t.integer  "recapture_disposition_id"
    t.integer  "time_of_day_option_id"
    t.datetime "created_at",                                                                     null: false
    t.datetime "updated_at",                                                                     null: false
    t.spatial  "geom",                     limit: {:srid=>0, :type=>"geometry"}
    t.string   "entered_gps_type"
    t.boolean  "user_capture"
  end

  create_table "captures", force: true do |t|
    t.string   "angler_id",                limit: 20
    t.integer  "tag_id"
    t.datetime "capture_date",                                                                   null: false
    t.integer  "species_id"
    t.float    "length"
    t.text     "location_description"
    t.float    "latitude"
    t.float    "longitude"
    t.integer  "fish_condition_option_id"
    t.float    "weight"
    t.text     "comments"
    t.boolean  "confirmed",                                                      default: false
    t.boolean  "verified",                                                       default: false
    t.boolean  "deleted"
    t.boolean  "mailed_map"
    t.boolean  "recapture",                                                      default: false
    t.integer  "recapture_disposition_id"
    t.integer  "time_of_day_option_id"
    t.datetime "created_at",                                                                     null: false
    t.datetime "updated_at",                                                                     null: false
    t.spatial  "geom",                     limit: {:srid=>0, :type=>"geometry"}
    t.string   "entered_gps_type"
    t.boolean  "user_capture",                                                   default: false
  end

  add_index "captures", ["angler_id"], :name => "idx_capture_angler_id"
  add_index "captures", ["capture_date"], :name => "idx_capture_date"
  add_index "captures", ["geom"], :name => "index_captures_geom", :spatial => true
  add_index "captures", ["tag_id"], :name => "cap_tag_fk", :unique => true
  add_index "captures", ["tag_id"], :name => "captures_tag_idx", :unique => true

  create_table "devices", force: true do |t|
    t.string   "identifier",                              null: false
    t.integer  "user_id",                                 null: false
    t.boolean  "update_captures",         default: false
    t.boolean  "update_tags",             default: false
    t.boolean  "update_partial_captures", default: false
    t.boolean  "update_species",          default: false
    t.boolean  "update_marinas",          default: false
    t.datetime "last_synced"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
  end

  add_index "devices", ["identifier"], :name => "index_devices_on_identifier", :unique => true

  create_table "draft_captures", force: true do |t|
    t.string   "angler_id",                                null: false
    t.string   "tag_number",                               null: false
    t.datetime "capture_date",                             null: false
    t.integer  "species_id"
    t.float    "length"
    t.text     "location_description"
    t.float    "latitude"
    t.float    "longitude"
    t.integer  "fish_condition_id"
    t.float    "weight"
    t.text     "comments"
    t.boolean  "recapture",                default: false
    t.integer  "recapture_disposition_id"
    t.integer  "time_of_day_id"
    t.string   "entered_gps_type"
    t.string   "error_json"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.boolean  "should_save",              default: false
    t.datetime "saved_at"
  end

  add_index "draft_captures", ["angler_id"], :name => "index_draft_captures_on_angler_id"

  create_table "fish_condition_options", force: true do |t|
    t.string   "fish_condition_option"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  create_table "front_page_photo_stream_items", force: true do |t|
    t.boolean  "featured",   default: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "how_to_tags", force: true do |t|
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "manufacturers", force: true do |t|
    t.string   "name"
    t.string   "street"
    t.string   "city"
    t.string   "state"
    t.string   "country"
    t.string   "zip"
    t.string   "contact"
    t.string   "phone"
    t.string   "fax"
    t.string   "email"
    t.boolean  "deleted"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "marinas", force: true do |t|
    t.integer  "site_no"
    t.string   "name"
    t.boolean  "closed",                  default: false
    t.string   "city"
    t.string   "parish"
    t.boolean  "publish",                 default: false
    t.string   "address"
    t.float    "latitude"
    t.float    "longitude"
    t.text     "web_address"
    t.string   "phone"
    t.string   "hours_of_operation"
    t.boolean  "can_get_fishing_license"
    t.string   "fee_amount"
    t.boolean  "has_bait",                default: false
    t.boolean  "has_ice",                 default: false
    t.boolean  "has_food",                default: false
    t.boolean  "has_restrooms",           default: false
    t.boolean  "has_sewage_pump",         default: false
    t.boolean  "has_cert_scales",         default: false
    t.string   "facility_type"
    t.boolean  "has_cleaning_station",    default: false
    t.string   "fuel_type"
    t.text     "location_description"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.boolean  "deleted",                 default: false
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.text     "comments"
  end

  create_table "news_items", force: true do |t|
    t.integer  "user_id",                           null: false
    t.text     "content",                           null: false
    t.boolean  "should_publish",    default: false
    t.datetime "should_publish_at"
    t.string   "title",                             null: false
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  create_table "notification_couriers", force: true do |t|
    t.string   "courier",    null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "notification_topics", force: true do |t|
    t.string   "topic",      null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "notification_topics_users", id: false, force: true do |t|
    t.integer "notification_topic_id"
    t.integer "user_id"
  end

  add_index "notification_topics_users", ["notification_topic_id", "user_id"], :name => "notifications_topic_user_idx", :unique => true

  create_table "notifications", force: true do |t|
    t.integer  "initiator_id"
    t.boolean  "checked",                default: false
    t.integer  "notification_item_id",                   null: false
    t.string   "notification_item_type",                 null: false
    t.integer  "notification_topic_id",                  null: false
    t.integer  "user_id",                                null: false
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
  end

  create_table "oauth_access_grants", force: true do |t|
    t.integer  "resource_owner_id",              null: false
    t.integer  "application_id",                 null: false
    t.string   "token",                          null: false
    t.integer  "expires_in",                     null: false
    t.string   "redirect_uri",      limit: 2048, null: false
    t.datetime "created_at",                     null: false
    t.datetime "revoked_at"
    t.string   "scopes"
  end

  add_index "oauth_access_grants", ["token"], :name => "index_oauth_access_grants_on_token", :unique => true

  create_table "oauth_access_tokens", force: true do |t|
    t.integer  "resource_owner_id"
    t.integer  "application_id",    null: false
    t.string   "token",             null: false
    t.string   "refresh_token"
    t.integer  "expires_in"
    t.datetime "revoked_at"
    t.datetime "created_at",        null: false
    t.string   "scopes"
  end

  add_index "oauth_access_tokens", ["refresh_token"], :name => "index_oauth_access_tokens_on_refresh_token", :unique => true
  add_index "oauth_access_tokens", ["resource_owner_id"], :name => "index_oauth_access_tokens_on_resource_owner_id"
  add_index "oauth_access_tokens", ["token"], :name => "index_oauth_access_tokens_on_token", :unique => true

  create_table "oauth_applications", force: true do |t|
    t.string   "name",                      null: false
    t.string   "uid",                       null: false
    t.string   "secret",                    null: false
    t.string   "redirect_uri", limit: 2048, null: false
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "oauth_applications", ["uid"], :name => "index_oauth_applications_on_uid", :unique => true

  create_table "photos", force: true do |t|
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.integer  "imageable_id"
    t.string   "imageable_type"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  create_table "recapture_dispositions", force: true do |t|
    t.string   "recapture_disposition"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  create_table "recaptures", force: true do |t|
    t.datetime "capture_date",                                                                   null: false
    t.float    "length"
    t.text     "location_description"
    t.float    "latitude"
    t.float    "longitude"
    t.text     "comments"
    t.boolean  "verified",                                                       default: false
    t.boolean  "user_capture",                                                   default: false
    t.string   "entered_gps_type"
    t.string   "angler_id",                                                                      null: false
    t.integer  "tag_id"
    t.integer  "species_id"
    t.integer  "fish_condition_option_id"
    t.integer  "recapture_disposition_id"
    t.integer  "time_of_day_option_id"
    t.datetime "created_at",                                                                     null: false
    t.datetime "updated_at",                                                                     null: false
    t.spatial  "geom",                     limit: {:srid=>4326, :type=>"point"}
  end

  add_index "recaptures", ["angler_id"], :name => "recaptures_angler_idx"
  add_index "recaptures", ["capture_date"], :name => "recaptures_date_idx"
  add_index "recaptures", ["geom"], :name => "recaptures_geom_idx"
  add_index "recaptures", ["tag_id", "capture_date", "time_of_day_option_id", "angler_id", "species_id", "fish_condition_option_id", "latitude", "longitude", "entered_gps_type"], :name => "recap_unique_fk", :unique => true
  add_index "recaptures", ["tag_id"], :name => "index_recaptures_on_tag_id"
  add_index "recaptures", ["tag_id"], :name => "recaptures_tag_idx"

  create_table "roles", force: true do |t|
    t.string   "name"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "roles", ["name", "resource_type", "resource_id"], :name => "index_roles_on_name_and_resource_type_and_resource_id"
  add_index "roles", ["name"], :name => "index_roles_on_name"

  create_table "roles_users", id: false, force: true do |t|
    t.integer "role_id"
    t.integer "user_id"
  end

  add_index "roles_users", ["role_id", "user_id"], :name => "ru_idx", :unique => true

  create_table "segments", primary_key: "gid", force: true do |t|
    t.decimal "objectid",                                              precision: 10, scale: 0
    t.decimal "area"
    t.float   "perimeter"
    t.float   "subseg02"
    t.float   "subseg02id"
    t.string  "basin",      limit: 18
    t.string  "subbasin",   limit: 10
    t.string  "subbasin_n", limit: 254
    t.string  "sgmnt_name", limit: 165
    t.string  "segment",    limit: 10
    t.spatial "geom",       limit: {:srid=>0, :type=>"multi_polygon"}
  end

  create_table "shirt_size_options", force: true do |t|
    t.string   "shirt_size_option"
    t.integer  "item_ordinal_pos"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  create_table "species", force: true do |t|
    t.string   "common_name"
    t.string   "species_code"
    t.integer  "position"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.string   "proper_name"
    t.string   "other_name"
    t.string   "family"
    t.text     "habitat"
    t.text     "description"
    t.text     "size"
    t.text     "food_value"
    t.string   "name"
    t.boolean  "publish",            default: false
    t.boolean  "target",             default: false
    t.integer  "ordinal_position"
  end

  create_table "sub-basin", primary_key: "gid", force: true do |t|
    t.decimal "objectid",                                              precision: 10, scale: 0
    t.decimal "area"
    t.decimal "perimeter"
    t.decimal "subseg02",                                              precision: 10, scale: 0
    t.decimal "subseg02id",                                            precision: 10, scale: 0
    t.string  "subsegment", limit: 15
    t.string  "name",       limit: 71
    t.string  "descriptio", limit: 140
    t.string  "basin",      limit: 18
    t.decimal "shape_area"
    t.decimal "shape_len"
    t.spatial "geom",       limit: {:srid=>0, :type=>"multi_polygon"}
  end

  create_table "tag_end_user_types", force: true do |t|
    t.string   "name"
    t.boolean  "deleted"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tag_lot_colors", force: true do |t|
    t.string   "color"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tag_lots", force: true do |t|
    t.integer  "tag_lot_number"
    t.integer  "manufacturer_id"
    t.string   "prefix"
    t.integer  "start_no"
    t.integer  "end_no"
    t.integer  "end_user_type"
    t.integer  "cost"
    t.datetime "date_entered"
    t.boolean  "ordered"
    t.datetime "order_date"
    t.boolean  "received"
    t.datetime "received_date"
    t.boolean  "deleted"
    t.integer  "color"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "tags", force: true do |t|
    t.string   "angler_id",       limit: 20
    t.string   "tag_no"
    t.integer  "tag_lot_id"
    t.integer  "tag_request_id"
    t.boolean  "deleted"
    t.integer  "unassign_option"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.integer  "old_anlger_id"
  end

  add_index "tags", ["tag_no"], :name => "idx_tags_tag_no"

  create_table "time_of_day_options", force: true do |t|
    t.string   "time_of_day_option"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.datetime "start_time"
    t.datetime "end_time"
  end

  create_table "unassign_tag_options", force: true do |t|
    t.string   "unassign_tag_option"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  create_table "user_couriers", force: true do |t|
    t.boolean  "daily_digest",            default: false
    t.integer  "user_id",                                 null: false
    t.integer  "notification_courier_id",                 null: false
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.integer  "notification_topic_id",                   null: false
  end

  create_table "user_tag_requests", force: true do |t|
    t.string   "angler_id",                      null: false
    t.integer  "number_of_tags",                 null: false
    t.boolean  "fulfilled",      default: false
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.string   "tag_type",                       null: false
  end

  create_table "users", force: true do |t|
    t.string   "email",                               default: "",    null: false
    t.string   "encrypted_password",                  default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                       default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                                          null: false
    t.datetime "updated_at",                                          null: false
    t.string   "first_name",                                          null: false
    t.string   "last_name",                                           null: false
    t.string   "angler_id"
    t.string   "authentication_token"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.boolean  "verified",                            default: false
    t.boolean  "receive_daily_capture_email_summary", default: false
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

  create_table "users_roles", id: false, force: true do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "users_roles", ["user_id", "role_id"], :name => "index_users_roles_on_user_id_and_role_id"

  create_table "volunteer_time_logs", force: true do |t|
    t.datetime "start",                      null: false
    t.datetime "end",                        null: false
    t.boolean  "verified",   default: false
    t.string   "angler_id"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "zipcodes", id: false, force: true do |t|
    t.integer "id",                                              null: false
    t.float   "latitude"
    t.float   "longitude"
    t.spatial "geom",      limit: {:srid=>4326, :type=>"point"}
  end

end

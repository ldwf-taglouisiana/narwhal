# == Schema Information
# Schema version: 20160405165101
#
# Table name: tag_assignments
#
#  id             :integer
#  angler_id      :integer
#  angler_number  :text
#  tag_prefix     :text
#  assignment_at  :date
#  start_tag      :text
#  end_tag        :text
#  number_of_tags :integer
#  last_tag_used  :text
#  tag_numbers    :string           is an Array
#  tag_ids        :integer          is an Array
#  seq_id         :integer
#

FactoryGirl.define do
  factory :tag_assignment do
    
  end

end

# == Schema Information
# Schema version: 20160113180221
#
# Table name: recent_recaptures
#
#  user_capture             :boolean
#  id                       :integer
#  species_id               :integer
#  species_name             :string(255)
#  tag_id                   :integer
#  tag_number               :string(255)
#  recapture_angler_id      :integer
#  recapture_angler_number  :string(20)
#  recapture_angler_name    :text
#  capture_date             :datetime
#  date_string              :text
#  location                 :text
#  time_of_day              :string(255)
#  time_of_day_option_id    :integer
#  length                   :decimal(, )
#  length_range             :string(255)
#  recapture_disposition    :string(255)
#  fish_condition_option_id :integer
#  fish_condition           :string(255)
#  latitude                 :float
#  verified                 :boolean
#  longitude                :float
#  latitude_formatted       :text
#  longitude_formatted      :text
#  geom                     :geometry(Point,4
#  basin_id                 :integer
#  sub_basin_id             :integer
#  comments                 :text
#  location_description     :text
#  location_id              :integer
#  created_at               :datetime
#  updated_at               :datetime
#  capture_id               :integer
#  year_id                  :integer
#  recapture_number         :integer
#

FactoryGirl.define do
  factory :recent_recapture do
    
  end

end

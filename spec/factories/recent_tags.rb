# == Schema Information
# Schema version: 20160405165101
#
# Table name: recent_tags
#
#  id              :integer
#  tag_no          :string(255)
#  tag_lot_id      :integer
#  tag_request_id  :integer
#  deleted         :boolean
#  unassign_option :integer
#  created_at      :datetime
#  updated_at      :datetime
#  old_anlger_id   :integer
#  prefix          :string(255)
#  active          :boolean
#  assigned_at     :datetime
#  recent          :boolean
#  search_vector   :tsvector
#  angler_id       :integer
#  tag_suffix      :integer
#  tag_prefix      :text
#  used            :boolean
#  used_at         :datetime
#  angler_name     :text
#  angler_number   :string(20)
#  seq_id          :integer
#

FactoryGirl.define do
  factory :recent_tag do
    
  end

end

# == Schema Information
# Schema version: 20160111183154
#
# Table name: fish_event_location_descriptions
#
#  id          :integer          not null, primary key
#  description :text             not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_fish_event_location_descriptions_on_description  (description) UNIQUE
#

FactoryGirl.define do
  factory :fish_event_location_description do
    
  end

end

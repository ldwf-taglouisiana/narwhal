# == Schema Information
# Schema version: 20160405165101
#
# Table name: recent_volunteer_hours
#
#  id                 :integer
#  angler_id          :integer
#  angler_number      :string(20)
#  angler_name        :text
#  verified           :boolean
#  ldwf_entered       :boolean
#  ldwf_edited        :boolean
#  total_time         :decimal(, )
#  start              :datetime
#  end                :datetime
#  recent             :boolean
#  created_at         :datetime
#  updated_at         :datetime
#  number_of_captures :integer
#

FactoryGirl.define do
  factory :recent_volunteer_hour do
    
  end

end

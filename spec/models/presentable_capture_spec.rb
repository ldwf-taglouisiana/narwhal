# == Schema Information
# Schema version: 20160113180221
#
# Table name: presentable_captures
#
#  id                       :integer          primary key
#  species_id               :integer
#  species_name             :string(255)
#  tag_id                   :integer
#  tag_number               :string(255)
#  angler_id                :integer
#  angler_number            :string(20)
#  capture_angler           :text
#  capture_date             :datetime
#  date_string              :text
#  location                 :text
#  time_of_day_option_id    :integer
#  time_of_day              :string(255)
#  length                   :decimal(, )
#  length_range             :string(255)
#  fish_condition_option_id :integer
#  fish_condition           :string(255)
#  latitude                 :float
#  longitude                :float
#  latitude_formatted       :text
#  longitude_formatted      :text
#  entered_gps_type         :string(255)
#  geom                     :geometry
#  basin_id                 :integer
#  sub_basin_id             :integer
#  comments                 :text
#  user_capture             :boolean
#  verified                 :boolean
#  location_description     :text
#  location_id              :integer
#  created_at               :datetime
#  updated_at               :datetime
#  recapture_count          :integer
#  year_id                  :integer
#

require 'rails_helper'

RSpec.describe PresentableCapture, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end

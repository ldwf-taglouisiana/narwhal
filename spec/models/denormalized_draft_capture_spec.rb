# == Schema Information
# Schema version: 20160405165101
#
# Table name: denormalized_draft_captures
#
#  id                       :integer          primary key
#  tag_number               :string(255)
#  capture_date             :datetime
#  species_id               :integer
#  length                   :float
#  location_description     :text
#  latitude                 :float
#  longitude                :float
#  fish_condition_id        :integer
#  weight                   :float
#  comments                 :text
#  recapture                :boolean
#  recapture_disposition_id :integer
#  time_of_day_id           :integer
#  entered_gps_type         :string(255)
#  error_json               :string(255)
#  created_at               :datetime
#  updated_at               :datetime
#  should_save              :boolean
#  saved_at                 :datetime
#  species_length_id        :integer
#  geom                     :geometry(Point,4
#  uuid                     :string(255)
#  search_vector            :tsvector
#  angler_id                :integer
#  angler_full_name         :text
#  species_name             :string(255)
#  angler_number            :string(20)
#  latitude_formatted       :text
#  longitude_formatted      :text
#  species_length           :string
#

require 'rails_helper'

RSpec.describe DenormalizedDraftCapture, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end

# == Schema Information
# Schema version: 20170724200101
#
# Table name: anglers
#
#  id                         :integer          not null, primary key
#  lag_no                     :integer
#  law_no                     :integer
#  first_name                 :string(255)
#  last_name                  :string(255)
#  street                     :string(255)
#  suite                      :string(255)
#  city                       :string(255)
#  state                      :string(255)
#  zip                        :string(255)
#  phone_number_1             :string(255)
#  phone_number_1_type        :string(255)
#  phone_number_2             :string(255)
#  phone_number_2_type        :string(255)
#  phone_number_3             :string(255)
#  phone_number_3_type        :string(255)
#  phone_number_4             :string(255)
#  phone_number_4_type        :string(255)
#  email                      :string(255)
#  tag_end_user_type          :integer
#  shirt_size                 :integer
#  deleted                    :boolean
#  lax_no                     :integer
#  email_2                    :string(255)
#  user_name                  :string(255)
#  comments                   :text
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  angler_id                  :string(20)       not null
#  search_vector              :tsvector         not null
#  prefered_incentive_item_id :integer
#
# Indexes
#
#  angler_id_uniq                                           (id) UNIQUE
#  index_anglers_on_angler_id_and_first_name_and_last_name  (angler_id,first_name,last_name)
#  index_anglers_on_search_vector                           (search_vector)
#
# Foreign Keys
#
#  ang_shirt_fk                             (shirt_size => shirt_size_options.id)
#  ang_tag_fk                               (tag_end_user_type => tag_end_user_types.id)
#  anglers_prefered_incentive_item_id_fkey  (prefered_incentive_item_id => items.id)
#

require 'test_helper'

class AnglerTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  test 'angler creation' do
    angler = build(:angler,
                   first_name: 'Bob',
                   last_name: 'Smith',
                   angler_id: 'Test1',
                   email: 'test@email.com',
    )

    assert angler.save, angler.errors.full_messages.to_a.join(',')
  end

  test 'duplicate angler_id' do

    angler = create(:angler,
                   first_name: 'James',
                   last_name: 'Smith',
                   angler_id: 'Test1',
                   email: 'test1@email.com',
    )

    assert angler.save #save the first one

    angler = build(:angler,
                   first_name: 'Bob',
                   last_name: 'Smith',
                   angler_id: 'TEST1',
                   email: 'test2@email.com',
    )

    assert !angler.save # try to save another with same angler_id, all caps

    angler = build(:angler,
                   first_name: 'Bob',
                   last_name: 'Smith',
                   angler_id: 'test1',
                   email: 'test2@email.com',
    )

    assert !angler.save # try to save another with same angler_id, all lower
  end

  test 'capitalize angler_id' do
    angler = build(:angler,
                   first_name: 'Bob',
                   last_name: 'Smith',
                   angler_id: 'test2',
                   email: 'test@email.com',
    )

    assert angler.save, angler.errors.full_messages.to_a.join(',')

    assert angler.angler_id = 'TEST2'
  end

  test 'new app angler' do

    #add the needed sequence since rails does not add it for us
    ActiveRecord::Base.connection.execute(
        <<-SQL
        CREATE SEQUENCE app_angler_sequence;
        select setval('app_angler_sequence', (
          select max(regexp_replace(angler_id, '^[A-Z]+', '')::integer)
          from anglers
          where angler_id ilike '%APP%') + 1
        );
    SQL
    )

    angler = Angler.new_app_angler({first_name: 'Bob', last_name: 'Smith', email: 'test@email.com'})

    assert angler.angler_id = 'APP1'

    angler = Angler.new_app_angler({first_name: 'Bob', last_name: 'Smith', email: 'test@email.com'})

    assert angler.angler_id = 'APP2'
  end

  test 'new recapture angler' do
    #add the needed sequence since rails does not add it for us
    ActiveRecord::Base.connection.execute(
        <<-SQL
        CREATE SEQUENCE recapture_angler_sequence;
        select setval('recapture_angler_sequence', (
          select max(regexp_replace(angler_id, '^[A-Z]+', '')::integer)
          from anglers
          where angler_id ilike '%LAX%') + 1
        );
    SQL
    )

    angler = Angler.new_recapture_angler({first_name: 'Bob', last_name: 'Smith', email: 'test@email.com'})

    assert angler.angler_id = 'LAX1'

    angler = Angler.new_recapture_angler({first_name: 'Bob', last_name: 'Smith', email: 'test@email.com'})

    assert angler.angler_id = 'LAX2'
  end

end

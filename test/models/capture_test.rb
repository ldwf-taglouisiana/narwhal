# == Schema Information
# Schema version: 20160113180221
#
# Table name: captures
#
#  id                                 :integer          not null, primary key
#  tag_id                             :integer
#  capture_date                       :datetime         not null
#  species_id                         :integer
#  length                             :decimal(, )
#  location_description               :text
#  latitude                           :float
#  longitude                          :float
#  fish_condition_option_id           :integer
#  weight                             :float
#  comments                           :text
#  confirmed                          :boolean          default(FALSE)
#  verified                           :boolean          default(FALSE)
#  deleted                            :boolean
#  mailed_map                         :boolean
#  time_of_day_option_id              :integer
#  created_at                         :datetime         not null
#  updated_at                         :datetime         not null
#  geom                               :geometry
#  entered_gps_type                   :string(255)
#  user_capture                       :boolean          default(FALSE)
#  species_length_id                  :integer
#  basin_id                           :integer
#  sub_basin_id                       :integer
#  gps_format_id                      :integer
#  recent                             :boolean          default(FALSE), not null
#  search_vector                      :tsvector         not null
#  fish_event_location_description_id :integer
#  angler_id                          :integer          not null
#
# Indexes
#
#  cap_tag_fk                                  (tag_id) UNIQUE
#  captures_length_idx                         (length)
#  captures_tag_idx                            (tag_id) UNIQUE
#  idx_captures_created_at                     (created_at)
#  index_captures_geom                         (geom)
#  index_captures_on_fish_condition_option_id  (fish_condition_option_id)
#  index_captures_on_gps_format_id             (gps_format_id)
#  index_captures_on_recent                    (recent)
#  index_captures_on_search_vector             (search_vector)
#  index_captures_on_species_id                (species_id)
#  index_captures_on_species_length_id         (species_length_id)
#  index_captures_on_time_of_day_option_id     (time_of_day_option_id)
#
# Foreign Keys
#
#  cap_fishcond_fk                  (fish_condition_option_id => fish_condition_options.id)
#  cap_species_fk                   (species_id => species.id)
#  cap_time_fk                      (time_of_day_option_id => time_of_day_options.id)
#  captures_basin_id_fk             (basin_id => basins.gid)
#  captures_species_length_id_fkey  (species_length_id => species_lengths.id)
#  captures_sub_basin_id_fk         (sub_basin_id => "sub-basin".gid)
#  fk_captures_gps_type             (entered_gps_type => gps_formats.format_type)
#  fk_rails_64432fd973              (gps_format_id => gps_formats.id)
#  fk_rails_906df65cfe              (angler_id => anglers.id)
#

require 'test_helper'

class CaptureTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  test "duplicate capture" do
    t = Time.now
    t2 = t + 1.day

    tag = create(:tag)
    angler = create(:angler)
    angler2 = create(:angler, angler_id: 'TEST4343')


    c = build(:capture,
               capture_date: t,
               time_of_day_option_id: 2,
               angler_id: angler.angler_id,
               species_id: 5,
               fish_condition_option_id: 2,
               latitude: 30.456744654655465456,
               longitude: -90.435553454446453745,
               entered_gps_type: 'D',
               tag: tag
              )

    assert c.save

    # changed everything except for the tag
    c = build(:capture,
               capture_date: t2,
               time_of_day_option_id: 1,
               angler_id: angler2.angler_id,
               species_id: 6,
               fish_condition_option_id: 1,
               latitude: 30.8798,
               longitude: -90.7876,
               entered_gps_type: 'DM',
               tag: tag
              )

    assert !c.save, 'Saved a duplicate original capture'
  end

  test "valid latitude" do
    t = Time.now

    tag = create(:tag)
    angler = create(:angler)

    #duplicate recapture
    c = build(:capture,
              capture_date: t,
              time_of_day_option_id: 2,
              angler_id: angler.angler_id,
              species_id: 5,
              fish_condition_option_id: 2,
              latitude: 0,
              longitude: -90.435553454446453745,
              entered_gps_type: 'D',
              tag: tag
    )

    assert c.save, '0 case'

    c.latitude = 90

    assert c.save, '90 case'

    c.latitude = 45.43442344442434242434

    assert c.save, '0-90 case'

    c.latitude = 90.00000001

    assert !c.save, c.errors.full_messages.to_a.join(',') #'> 90 case'

    c.latitude = -0.00000001

    assert !c.save, '<0 case'

  end

  test "valid longitude" do
    t = Time.now

    tag = create(:tag)
    angler = create(:angler)

    #duplicate recapture
    c = build(:capture,
              capture_date: t,
              time_of_day_option_id: 2,
              angler_id: angler.angler_id,
              species_id: 5,
              fish_condition_option_id: 2,
              latitude: 35.00343,
              longitude: 0,
              entered_gps_type: 'D',
              tag: tag
    )

    assert c.save, '0 case'

    c.longitude = -0.0001

    assert c.save, '< 0 case'

    c.longitude = -180

    assert c.save, '-180 case'

    c.longitude = -45.43442344442434242434

    assert c.save, '-180 <-> 0 case'

    c.longitude = 0.000000001

    assert !c.save, '> 0 case'

  end

  test 'presence of tag' do
    t = Time.now
    angler = create(:angler)

    c = build(:capture,
              capture_date: t,
              time_of_day_option_id: 2,
              angler_id: angler.angler_id,
              species_id: 5,
              fish_condition_option_id: 2,
              latitude: 35.00343,
              longitude: -90.343,
              entered_gps_type: 'D'
    )

    assert !c.save

    assert c.errors.include?(:tag_id)
  end

  test 'presence of angler id' do
    t = Time.now
    tag = create(:tag)

    c = build(:capture,
              capture_date: t,
              time_of_day_option_id: 2,
              species_id: 5,
              fish_condition_option_id: 2,
              latitude: 35.00343,
              longitude: -90.343,
              entered_gps_type: 'D',
              tag: tag
    )

    assert !c.save  # no angler_id

    assert c.errors.include?(:angler_id)
  end

  test 'presence of capture_date' do
    tag = create(:tag)
    angler = create(:angler)

    c = build(:capture,
              time_of_day_option_id: 2,
              species_id: 5,
              fish_condition_option_id: 2,
              latitude: 35.00343,
              longitude: -90.343,
              entered_gps_type: 'D',
              tag: tag,
              angler_id: angler.angler_id
    )

    assert !c.save

    assert c.errors.include?(:capture_date)
  end

  test 'angler_id capitalization' do
    t = Time.now

    tag = create(:tag)
    angler = create(:angler, angler_id: 'test1')

    c = build(:capture,
              capture_date: t,
              time_of_day_option_id: 2,
              angler_id: angler.angler_id,
              species_id: 5,
              fish_condition_option_id: 2,
              latitude: 35.00343,
              longitude: -90.343,
              entered_gps_type: 'D',
              tag: tag
    )

    assert c.save # valid capture

    assert c.angler_id == 'TEST1'  # no capture date
  end

  test 'update geom' do
    t = Time.now

    tag = create(:tag)
    angler = create(:angler, angler_id: 'test1')

    #duplicate recapture
    c = build(:capture,
              capture_date: t,
              time_of_day_option_id: 2,
              angler_id: angler.angler_id,
              species_id: 5,
              fish_condition_option_id: 2,
              latitude: 35.00343,
              longitude: -90.343,
              entered_gps_type: 'D',
              tag: tag
    )

    assert c.save # valid capture

    #TODO add a method to get and parse the geom
    assert c.geom != nil, c.errors.full_messages.to_a.join(',')   # no capture date
  end

end

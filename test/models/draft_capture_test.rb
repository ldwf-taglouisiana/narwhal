# == Schema Information
# Schema version: 20160113180221
#
# Table name: draft_captures
#
#  id                       :integer          not null, primary key
#  tag_number               :string(255)      not null
#  capture_date             :datetime         not null
#  species_id               :integer
#  length                   :float
#  location_description     :text
#  latitude                 :float
#  longitude                :float
#  fish_condition_id        :integer
#  weight                   :float
#  comments                 :text
#  recapture                :boolean          default(FALSE)
#  recapture_disposition_id :integer
#  time_of_day_id           :integer
#  entered_gps_type         :string(255)
#  error_json               :string(255)
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  should_save              :boolean          default(FALSE)
#  saved_at                 :datetime
#  species_length_id        :integer
#  geom                     :geometry(Point,4
#  uuid                     :string(255)
#  search_vector            :tsvector         not null
#  angler_id                :integer          not null
#
# Indexes
#
#  index_draft_captures_on_search_vector  (search_vector)
#  index_draft_captures_on_uuid           (uuid) UNIQUE
#
# Foreign Keys
#
#  draft_captures_species_length_id_fkey  (species_length_id => species_lengths.id)
#  fk_draft_captures_gps_type             (entered_gps_type => gps_formats.format_type)
#  fk_part_captures_fish_cond             (fish_condition_id => fish_condition_options.id)
#  fk_part_captures_recap_disp            (recapture_disposition_id => recapture_dispositions.id)
#  fk_part_captures_species_id            (species_id => species.id)
#  fk_part_captures_time_of_day           (time_of_day_id => time_of_day_options.id)
#  fk_rails_18ffac8d12                    (angler_id => anglers.id)
#

require 'test_helper'

class DraftCaptureTest < ActiveSupport::TestCase
  test 'upcase tag number' do
    t = Time.now
    tag = 'test123'
    angler = create(:angler, angler_id: 'uno123')

    draft = build(:draft_capture,
                  angler_id: angler.angler_id,
                  capture_date: t,
                  tag_number: tag)

    assert draft.save

    assert draft.tag_number == draft.tag_number.upcase
  end

  test 'clean comments' do
    t = Time.now
    tag = create(:tag)
    angler = create(:angler, angler_id: 'uno123')


    draft = build(:draft_capture,
                  angler_id: angler.angler_id,
                  capture_date: t,
                  tag_number: tag.tag_no,
                  comments: 'sdhjhhhfsfjkh Additional Comments dfdfsfd')

    assert draft.save

    assert /Additional Comments/ !~ draft.comments
  end

end

# == Schema Information
# Schema version: 20160405165101
#
# Table name: tags
#
#  id              :integer          not null, primary key
#  tag_no          :string(255)
#  tag_lot_id      :integer
#  tag_request_id  :integer
#  deleted         :boolean
#  unassign_option :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  old_anlger_id   :integer
#  prefix          :string(255)
#  active          :boolean          default(TRUE)
#  assigned_at     :datetime
#  recent          :boolean          default(TRUE), not null
#  search_vector   :tsvector         not null
#  angler_id       :integer
#
# Indexes
#
#  index_tags_on_angler_id      (angler_id)
#  index_tags_on_recent         (recent)
#  index_tags_on_search_vector  (search_vector)
#  index_tags_on_tag_lot_id     (tag_lot_id)
#  index_tags_on_tag_no         (tag_no) UNIQUE
#  tags_prefix_idx              (prefix)
#
# Foreign Keys
#
#  fk_rails_e4069f0b05  (angler_id => anglers.id)
#  tag_lot_fk           (tag_lot_id => tag_lots.id)
#

require 'test_helper'

class TagsControllerTest < ActionController::TestCase
  setup do
    @tag = tags(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:tags)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create tag" do
    assert_difference('Tag.count') do
      post :create, tag: {  }
    end

    assert_redirected_to tag_path(assigns(:tag))
  end

  test "should show tag" do
    get :show, id: @tag
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @tag
    assert_response :success
  end

  test "should update tag" do
    patch :update, id: @tag, tag: {  }
    assert_redirected_to tag_path(assigns(:tag))
  end

  test "should destroy tag" do
    assert_difference('Tag.count', -1) do
      delete :destroy, id: @tag
    end

    assert_redirected_to tags_path
  end
end

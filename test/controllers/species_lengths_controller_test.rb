# == Schema Information
# Schema version: 20160405165101
#
# Table name: species_lengths
#
#  id          :integer          not null, primary key
#  description :string(255)      not null
#  species_id  :integer          not null
#  position    :integer          not null
#  created_at  :datetime
#  updated_at  :datetime
#  min         :float
#  max         :float
#
# Indexes
#
#  idx_species_length_min_max  (min,max)
#
# Foreign Keys
#
#  species_lengths_species_fk  (species_id => species.id)
#

require 'test_helper'

class SpeciesLengthsControllerTest < ActionController::TestCase
  setup do
    @species_length = species_lengths(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:species_lengths)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create species_length" do
    assert_difference('SpeciesLength.count') do
      post :create, species_length: { description: @species_length.description, species_id: @species_length.species_id }
    end

    assert_redirected_to species_length_path(assigns(:species_length))
  end

  test "should show species_length" do
    get :show, id: @species_length
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @species_length
    assert_response :success
  end

  test "should update species_length" do
    patch :update, id: @species_length, species_length: { description: @species_length.description, species_id: @species_length.species_id }
    assert_redirected_to species_length_path(assigns(:species_length))
  end

  test "should destroy species_length" do
    assert_difference('SpeciesLength.count', -1) do
      delete :destroy, id: @species_length
    end

    assert_redirected_to species_lengths_path
  end
end

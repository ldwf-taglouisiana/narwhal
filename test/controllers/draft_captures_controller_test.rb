# == Schema Information
# Schema version: 20160405165101
#
# Table name: draft_captures
#
#  id                       :integer          not null, primary key
#  tag_number               :string(255)      not null
#  capture_date             :datetime         not null
#  species_id               :integer
#  length                   :float
#  location_description     :text
#  latitude                 :float
#  longitude                :float
#  fish_condition_id        :integer
#  weight                   :float
#  comments                 :text
#  recapture                :boolean          default(FALSE)
#  recapture_disposition_id :integer
#  time_of_day_id           :integer
#  entered_gps_type         :string(255)
#  error_json               :string(255)
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  should_save              :boolean          default(FALSE)
#  saved_at                 :datetime
#  species_length_id        :integer
#  geom                     :geometry(Point,4
#  uuid                     :string(255)
#  search_vector            :tsvector         not null
#  angler_id                :integer          not null
#
# Indexes
#
#  index_draft_captures_on_search_vector  (search_vector)
#  index_draft_captures_on_uuid           (uuid) UNIQUE
#
# Foreign Keys
#
#  draft_captures_species_length_id_fkey  (species_length_id => species_lengths.id)
#  fk_draft_captures_gps_type             (entered_gps_type => gps_formats.format_type)
#  fk_part_captures_fish_cond             (fish_condition_id => fish_condition_options.id)
#  fk_part_captures_recap_disp            (recapture_disposition_id => recapture_dispositions.id)
#  fk_part_captures_species_id            (species_id => species.id)
#  fk_part_captures_time_of_day           (time_of_day_id => time_of_day_options.id)
#  fk_rails_18ffac8d12                    (angler_id => anglers.id)
#

require 'test_helper'

class DraftCapturesControllerTest < ActionController::TestCase
  setup do
    @draft_capture = draft_captures(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:draft_captures)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create draft_capture" do
    assert_difference('DraftCapture.count') do
      post :create, draft_capture: {  }
    end

    assert_redirected_to draft_capture_path(assigns(:draft_capture))
  end

  test "should show draft_capture" do
    get :show, id: @draft_capture
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @draft_capture
    assert_response :success
  end

  test "should update draft_capture" do
    patch :update, id: @draft_capture, draft_capture: {  }
    assert_redirected_to draft_capture_path(assigns(:draft_capture))
  end

  test "should destroy draft_capture" do
    assert_difference('DraftCapture.count', -1) do
      delete :destroy, id: @draft_capture
    end

    assert_redirected_to draft_captures_path
  end
end

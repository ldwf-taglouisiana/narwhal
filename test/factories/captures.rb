# == Schema Information
# Schema version: 20160113180221
#
# Table name: captures
#
#  id                                 :integer          not null, primary key
#  tag_id                             :integer
#  capture_date                       :datetime         not null
#  species_id                         :integer
#  length                             :decimal(, )
#  location_description               :text
#  latitude                           :float
#  longitude                          :float
#  fish_condition_option_id           :integer
#  weight                             :float
#  comments                           :text
#  confirmed                          :boolean          default(FALSE)
#  verified                           :boolean          default(FALSE)
#  deleted                            :boolean
#  mailed_map                         :boolean
#  time_of_day_option_id              :integer
#  created_at                         :datetime         not null
#  updated_at                         :datetime         not null
#  geom                               :geometry
#  entered_gps_type                   :string(255)
#  user_capture                       :boolean          default(FALSE)
#  species_length_id                  :integer
#  basin_id                           :integer
#  sub_basin_id                       :integer
#  gps_format_id                      :integer
#  recent                             :boolean          default(FALSE), not null
#  search_vector                      :tsvector         not null
#  fish_event_location_description_id :integer
#  angler_id                          :integer          not null
#
# Indexes
#
#  cap_tag_fk                                  (tag_id) UNIQUE
#  captures_length_idx                         (length)
#  captures_tag_idx                            (tag_id) UNIQUE
#  idx_captures_created_at                     (created_at)
#  index_captures_geom                         (geom)
#  index_captures_on_fish_condition_option_id  (fish_condition_option_id)
#  index_captures_on_gps_format_id             (gps_format_id)
#  index_captures_on_recent                    (recent)
#  index_captures_on_search_vector             (search_vector)
#  index_captures_on_species_id                (species_id)
#  index_captures_on_species_length_id         (species_length_id)
#  index_captures_on_time_of_day_option_id     (time_of_day_option_id)
#
# Foreign Keys
#
#  cap_fishcond_fk                  (fish_condition_option_id => fish_condition_options.id)
#  cap_species_fk                   (species_id => species.id)
#  cap_time_fk                      (time_of_day_option_id => time_of_day_options.id)
#  captures_basin_id_fk             (basin_id => basins.gid)
#  captures_species_length_id_fkey  (species_length_id => species_lengths.id)
#  captures_sub_basin_id_fk         (sub_basin_id => "sub-basin".gid)
#  fk_captures_gps_type             (entered_gps_type => gps_formats.format_type)
#  fk_rails_64432fd973              (gps_format_id => gps_formats.id)
#  fk_rails_906df65cfe              (angler_id => anglers.id)
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :capture do
  end
end

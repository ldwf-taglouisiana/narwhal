# == Schema Information
# Schema version: 20160113180221
#
# Table name: recaptures
#
#  id                                 :integer          not null, primary key
#  capture_date                       :datetime         not null
#  length                             :decimal(, )
#  location_description               :text
#  latitude                           :float
#  longitude                          :float
#  comments                           :text
#  verified                           :boolean          default(FALSE)
#  user_capture                       :boolean          default(FALSE)
#  entered_gps_type                   :string(255)
#  tag_id                             :integer
#  species_id                         :integer
#  fish_condition_option_id           :integer
#  recapture_disposition_id           :integer
#  time_of_day_option_id              :integer
#  created_at                         :datetime         not null
#  updated_at                         :datetime         not null
#  geom                               :geometry(Point,4
#  species_length_id                  :integer
#  basin_id                           :integer
#  sub_basin_id                       :integer
#  gps_format_id                      :integer
#  capture_id                         :integer
#  recent                             :boolean          default(TRUE), not null
#  search_vector                      :tsvector         not null
#  fish_event_location_description_id :integer
#  angler_id                          :integer          not null
#
# Indexes
#
#  idx_recaptures_created_at                     (created_at)
#  index_recaptures_on_capture_id                (capture_id)
#  index_recaptures_on_fish_condition_option_id  (fish_condition_option_id)
#  index_recaptures_on_gps_format_id             (gps_format_id)
#  index_recaptures_on_recent                    (recent)
#  index_recaptures_on_search_vector             (search_vector)
#  index_recaptures_on_species_id                (species_id)
#  index_recaptures_on_species_length_id         (species_length_id)
#  index_recaptures_on_tag_id                    (tag_id)
#  index_recaptures_on_time_of_day_option_id     (time_of_day_option_id)
#  recaptures_date_idx                           (capture_date)
#  recaptures_geom_idx                           (geom)
#  recaptures_length_idx                         (length)
#  recaptures_tag_idx                            (tag_id)
#
# Foreign Keys
#
#  fk_rails_24510c6ae4         (capture_id => captures.id)
#  fk_rails_45858b5865         (gps_format_id => gps_formats.id)
#  fk_rails_782717f62d         (angler_id => anglers.id)
#  fk_recaptures_gps_type      (entered_gps_type => gps_formats.format_type)
#  recap_fish_cond_fk          (fish_condition_option_id => fish_condition_options.id)
#  recap_recap_dis_fk          (recapture_disposition_id => recapture_dispositions.id)
#  recap_species_fk            (species_id => species.id)
#  recap_tag_fk                (tag_id => tags.id)
#  recap_time_day_fk           (time_of_day_option_id => time_of_day_options.id)
#  recaptures_basin_id_fk      (basin_id => basins.gid)
#  recaptures_sub_basin_id_fk  (sub_basin_id => "sub-basin".gid)
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :recapture do
  end
end

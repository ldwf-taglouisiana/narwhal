# == Schema Information
# Schema version: 20150817164549
#
# Table name: species_lengths
#
#  id          :integer          not null, primary key
#  description :string(255)      not null
#  species_id  :integer          not null
#  position    :integer          not null
#  created_at  :datetime
#  updated_at  :datetime
#  min         :float
#  max         :float
#
# Indexes
#
#  idx_species_length_min_max  (min,max)
#
# Foreign Keys
#
#  species_lengths_species_fk  (species_id => species.id)
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :species_length do
    description "MyString"
    species_id 1
  end
end

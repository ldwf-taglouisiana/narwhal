# == Schema Information
#
# Table name: requested_downloads
#
#  id           :integer          not null, primary key
#  token        :text             not null
#  filename     :text             not null
#  filepath     :text
#  should_email :boolean          default(FALSE)
#  created_at   :datetime
#  updated_at   :datetime
#  user_id      :integer          not null
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :requested_download do
    token "MyText"
    shouldEmail false
  end
end
